<?php
/// verifica se esta em manutenção
try{
    $sql="select * from tbl_suport where id=2";
    global $pdo;
    $manutencao=$pdo->prepare($sql);
    $manutencao->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$manutencao=$manutencao->fetch();
if ($manutencao['valor']==1) {
    if ( ($_SESSION['matriz']=="1") and ( $manutencao['apoio']==0 or $manutencao['apoio']==1 ) ){
        header("Location: {$env->env_url}mcu/manutencao.php");
        exit();
    }
    if ( ($_SESSION['matriz']=="2") and ( $manutencao['apoio']==0 or $manutencao['apoio']==2 ) ){
        header("Location: {$env->env_url}caf/manutencao.php");
        exit();
    }
    if ( ($_SESSION['matriz']=="3") and ( $manutencao['apoio']==0 or $manutencao['apoio']==3 ) ){
        header("Location: {$env->env_url}mpref/manutencao.php");
        exit();
    }
    if ( ($_SESSION['matriz']=="4") and ( $manutencao['apoio']==0 or $manutencao['apoio']==4 ) ){
        header("Location: {$env->env_url}smem/manutencao.php");
        exit();
    }
}
///