<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="usuarionew"){
    $nome=remover_caracter(ucwords(strtolower($_POST["nome"])));
    $status=$_POST["status"];
    $email=strtolower($_POST["email"]);
    $profissao=$_POST["profissao"];
    $nascimento=$_POST["nascimento"];
    $cpf = limpadocumento($_POST["cpf"]);
    $matriz=2;

    if(empty($nome) || empty($email) || empty($nascimento) || $nascimento==0 || empty($matriz)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
    }else{
        //valida email
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            //executa classe cadastro
            $conec= new Usuario();
            $conec=$conec->fncnewusuario($nome, $status, $email, $nascimento, $cpf, $profissao, $matriz);
            header("Location: index.php");
            exit();
        }else{
            //EMAIL ERRADO
            $_SESSION['fsh']=[
                "flash"=>"Preencha corretamente o email",
                "type"=>"warning",
            ];
        }
    }
}
