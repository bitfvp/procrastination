<?php

function fncbairrolist(){
    $sql = "SELECT * FROM mcu_bairros ORDER BY bairro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $bairrolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $bairrolista;
}

function fncgetbairro($id){
    $sql = "SELECT * FROM mcu_bairros WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getbairro = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getbairro;
}
