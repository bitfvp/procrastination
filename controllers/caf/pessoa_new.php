<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="pessoanew"){
    $id_prof=$_SESSION["id"];
    $nome=$_POST["nome"];
    $nome_social=$_POST["nome_social"];
    $sexo=$_POST["sexo"];
    $nascimento = $_POST["nascimento"];
    $cpf = limpadocumento($_POST["cpf"]);
    $rg = limpadocumento($_POST["rg"]);
    $uf_rg=$_POST["uf_rg"];
    $cn = $_POST["cn"];
    $ctps=$_POST["ctps"];
    $cod_familiar=$_POST["cod_familiar"];
    $endereco=$_POST["endereco"];
    $numero=$_POST["numero"];
    $bairro=$_POST["bairro"];
    $referencia=$_POST["referencia"];
    $telefone=$_POST["telefone"];
    $mae=$_POST["mae"];
    $pai=$_POST["pai"];
    $residencia=$_POST["residencia"];
    $escolaridade=$_POST["escolaridade"];
    $deficiencia=$_POST["deficiencia"];
    $deficiencia_desc=$_POST["deficiencia_desc"];
    $ser_quando_crescer=$_POST["ser_quando_crescer"];
    $sonho=$_POST["sonho"];
    $cor=$_POST["cor"];
    $musica=$_POST["musica"];
    $brincadeira=$_POST["brincadeira"];
    $personagem=$_POST["personagem"];
    $apadrinhamento=$_POST["apadrinhamento"];
    $uso_de_imagem=$_POST["uso_de_imagem"];

    if($nascimento==""){
        $nascimento="1000-01-01";
    }
    if($numero==""){
        $numero=0;
    }


    if($cpf!=""){
        if (validaCPF($cpf)==0){
            $cpf_de_boa="nao_esta_de_boa";
        }else{
            $cpf_de_boa="de_boa";
        }

    }else{
        $cpf_de_boa="de_boa";
    }

    //
    if(empty($nome)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }elseif ($cpf_de_boa!="de_boa"){
        $_SESSION['fsh']=[
            "flash"=>"CPF digitado é invalido!!",
            "type"=>"danger",
        ];
    } else{
        $salvar= new Pessoa();
        $salvar->fncpessoanew(
            $nome,
            $nome_social,
            $sexo,
            $nascimento,
            $cpf,
            $rg,
            $uf_rg,
            $cn,
            $ctps,
            $cod_familiar,
            $endereco,
            $numero,
            $bairro,
            $referencia,
            $telefone,
            $mae,
            $pai,
            $residencia,
            $escolaridade,
            $deficiencia,
            $deficiencia_desc,
            $ser_quando_crescer,
            $sonho,
            $cor,
            $musica,
            $brincadeira,
            $personagem,
            $apadrinhamento,
            $uso_de_imagem
            );
    }
}
?>