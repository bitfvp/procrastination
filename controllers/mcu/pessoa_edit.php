<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="pessoasave"){
    $id=$_POST["id"];
    $nome=$_POST["nome"];
    $nome_social=$_POST["nome_social"];
    $sexo=$_POST["sexo"];
    $nascimento = $_POST["nascimento"];
    $cpf = limpadocumento($_POST["cpf"]);
    $rg = limpadocumento($_POST["rg"]);
    $uf_rg=$_POST["uf_rg"];
    $nis = limpadocumento($_POST["nis"]);
    $ctps=$_POST["ctps"];
    $cod_familiar=$_POST["cod_familiar"];
    $responsavel_familiar=$_POST["responsavel_familiar"];
    $parentesco=$_POST["parentesco"];
    $endereco=$_POST["endereco"];
    $numero=$_POST["numero"];
    $bairro=$_POST["bairro"];
    $referencia=$_POST["referencia"];
    $telefone=$_POST["telefone"];
    $raca_cor=$_POST["raca_cor"];
    $agente_saude=$_POST["agente_saude"];
    $mae=$_POST["mae"];
    $pai=$_POST["pai"];
    $possui_veiculo=$_POST["possui_veiculo"];
    $paga_aluguel=$_POST["paga_aluguel"];
    $possui_propriedade=$_POST["possui_propriedade"];
    $alfabetizado=$_POST["alfabetizado"];
    $renda=$_POST["renda"];
    $bpc=$_POST["bpc"];
    $deficiencia=$_POST["deficiencia"];
    $deficiencia_desc=$_POST["deficiencia_desc"];
    $curso=$_POST["curso"];
    $horario=$_POST["horario"];

    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) || empty($nome)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }else{
                //executa classe cadastro

                $salvar= new Pessoa();
                $salvar->fncpessoaedit(
                $id,
                $nome,
                $nome_social,
                $sexo,
                $nascimento,
                $cpf,
                $rg,
                $uf_rg,
                $nis,
                $ctps,
                $cod_familiar,
                $responsavel_familiar,
                $parentesco,
                $endereco,
                $numero,
                $bairro,
                $referencia,
                $telefone,
                $raca_cor,
                $agente_saude,
                $mae,
                $pai,
                $possui_veiculo,
                $paga_aluguel,
                $possui_propriedade,
                $alfabetizado,
                $renda,
                $bpc,
                $deficiencia,
                $deficiencia_desc,
                $curso,
                $horario
                );
    }
}


//duplicatas
if($startactiona==1 && $aca=="nova_duplicata"){
    $pessoa=$_GET["id"];


    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($pessoa)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }else{
        //executa classe cadastro

        $salvar= new Pessoa();
        $salvar->fncnewduplicata($pessoa);
    }
}