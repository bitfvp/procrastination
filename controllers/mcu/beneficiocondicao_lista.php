<?php
function fncgetbecondicao($id){
    $sql = "SELECT * FROM mcu_beneficio_condicao WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getbecondicao = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getbecondicao;
}

function fncbecondicaolist(){
    $sql = "SELECT * FROM mcu_beneficio_condicao where status=1 ORDER BY condicao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $becondicaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $becondicaolista;
}

function fncbecondicaolist_todos(){
    $sql = "SELECT * FROM mcu_beneficio_condicao ORDER BY condicao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $becondicaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $becondicaolista;
}
