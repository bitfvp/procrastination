<?php
function fncgetbetipo($id){
    $sql = "SELECT * FROM mcu_beneficio_tipo WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getbetipo = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getbetipo;
}

function fncbetipolist(){
    $sql = "SELECT * FROM mcu_beneficio_tipo ORDER BY tipo";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $betipolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $betipolista;
}
