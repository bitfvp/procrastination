<?php
function fncmatrizlist(){
    $sql = "SELECT * FROM tbl_matriz ORDER BY matriz";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $matrizlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $matrizlista;
}

function fncgetmatriz($id){
    $sql = "SELECT * FROM tbl_matriz WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getmatriz = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getmatriz;
}
