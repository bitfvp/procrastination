<?php

function fncprofissaolist(){
    $sql = "SELECT * FROM tbl_profissao ORDER BY profissao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $profissaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $profissaolista;
}

function fncgetprofissao($id){
    $sql = "SELECT * FROM tbl_profissao WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getprofissao = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getprofissao;
}
?>
