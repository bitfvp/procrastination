<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="usuarionew"){
    $nome = remover_caracter(ucwords(strtolower($_POST["nome"])));
    $status = $_POST["status"];
    $nick = strtolower(limpadocumento($_POST["nick"]));
    $email = strtolower($_POST["email"]);
    $sexo = $_POST["sexo"];
    $nascimento = $_POST["nascimento"];
    if($nascimento==""){
        $nascimento="1000-01-01";
    }
    $mae = remover_caracter(ucwords(strtolower($_POST["mae"])));
    $pai = remover_caracter(ucwords(strtolower($_POST["pai"])));
    $cpf = limpadocumento($_POST["cpf"]);
    $rg = limpadocumento($_POST["rg"]);
    $rg_expedicao = $_POST["rg_expedicao"];
    if($rg_expedicao==""){
        $rg_expedicao="1000-01-01";
    }
    $rg_emissor = $_POST["rg_emissor"];
    $pis = limpadocumento($_POST["pis"]);
    $titulo = limpadocumento($_POST["titulo"]);
    $zona = limpadocumento($_POST["zona"]);
    $secao = limpadocumento($_POST["secao"]);
    $endereco=$_POST["endereco"];
    $bairro=$_POST["bairro"];
    $cep=limpadocumento($_POST["cep"]);
    $telefone1=$_POST["telefone1"];
    $telefone2=$_POST["telefone2"];
    $escolaridade=$_POST["escolaridade"];
    $estado_civil=$_POST["estado_civil"];
    $profissao=$_POST["profissao"];
    $setor=$_POST["setor"];
    $admissao=$_POST["admissao"];
    if($admissao==""){
        $admissao="1000-01-01";
    }
    $fim_exercicio=$_POST["fim_exercicio"];
    if($fim_exercicio==""){
        $fim_exercicio="1000-01-01";
    }
    $completo = $_POST["completo"];

    $matriz=4;

    if(empty($nome) || empty($email) || empty($nascimento) || $nascimento==0 || empty($matriz)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
    }else{
        //valida email
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            //executa classe cadastro
            $conec= new Usuario();
            $conec=$conec->fncnewusuario(
                $nome,
                $status,
                $nick,
                $email,
                $sexo,
                $nascimento,
                $mae,
                $pai,
                $cpf,
                $rg,
                $rg_expedicao,
                $rg_emissor,
                $pis,
                $titulo,
                $zona,
                $secao,
                $endereco,
                $bairro,
                $cep,
                $telefone1,
                $telefone2,
                $escolaridade,
                $estado_civil,
                $profissao,
                $setor,
                $admissao,
                $fim_exercicio,
                $completo,
                $matriz
            );
            header("Location: index.php");
            exit();
        }else{
            //EMAIL ERRADO
            $_SESSION['fsh']=[
                "flash"=>"Preencha corretamente o email",
                "type"=>"warning",
            ];
        }
    }
}
