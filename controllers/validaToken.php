<?php
//testa se ha as sessions com token
if (isset($_SESSION["tokenId"]) and isset($_SESSION["tokenTime"])) {
    //busca no banco tokens iguais
    $sql = "SELECT * FROM tbl_token WHERE token=? AND token_time=?";
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_SESSION["tokenId"]);
    $consulta->bindParam(2, $_SESSION["tokenTime"]);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $tli = $consulta->fetch();
    $tliCont = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    //renova session tokenid
    $_SESSION['tokenId'] = $tli['token'];
    //verifica se o banco retornou algo
    if ($tliCont != 0) {
        //pega mk do time atual
        $tm1 = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
        $tm2=$tm1;
        //testa se o valor no banco é maior que o session
        if (($tli['token_time'] > $tm2) and ($_SESSION['id']==$tli['user'])) {
            //novo tempo de validade pro token
            $newtm = $tm1 + 3600;
            //renova session tokentime
            $_SESSION['tokenTime'] = $newtm;
            $sql = "UPDATE tbl_token SET token_time=? WHERE token=?";
            $up = $pdo->prepare($sql);
            $up->bindParam(1, $newtm);
            $up->bindParam(2, $tli['token']);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
            $sql = null;
            $up = null;
            //hora de gerar as permissoes
            if (isset($_SESSION['id']) and isset($_SESSION['nome']) and isset($_SESSION['matriz']) and isset($_SESSION['logado'])){
                /////////////////////////////////////////////////////////////////////////////////
                if ($_SESSION['matriz']=="1"){
                    $sql = "select * from mcu_allow WHERE pessoa=?";
                }
                if ($_SESSION['matriz']=="2"){
                    $sql = "select * from caf_allow WHERE pessoa=?";
                }
                if ($_SESSION['matriz']=="3"){
                    $sql = "select * from mpref_allow WHERE pessoa=?";
                }
                if ($_SESSION['matriz']=="4"){
                    $sql = "select * from smem_allow WHERE pessoa=?";
                }
                $perm = $pdo->prepare($sql);
                $perm->bindParam(1, $_SESSION['id']);
                $perm->execute(); global $LQ; $LQ->fnclogquery($sql);
                $allow = $perm->fetch();
                $sql = null;
                $perm = null;
                global $allow;//consulte a tabela de apoio com as permissoes
                /////////////////////////////////////////////////////////////////////////////////////////

                ///
            }else{
                killSession();
            }

        } else {
            //se tempo espirar
            $_SESSION['fsh']=[
                "flash"=>"Muito tempo inativo!!",
                "type"=>"danger",
                "error"=>"Sua seção acabou",
            ];
            killSession();
        }

    } else {
        killSession();
    }
} else {
    killSession();
}
?>