<?php
$page="Error 404-".$env->env_titulo;
$css = "404";
include_once("{$env->env_root}includes/head.php");

echo"<META HTTP-EQUIV=REFRESH CONTENT = '15;URL={$env->env_url_mod}'>";
?>
<body>
<main class="content container-fluid">
    <h2 class="mt-3 ms1">A PÁGINA QUE VOCÊ SOLICITOU NÃO PODE SER ENCONTRADA...</h2>
    <div id="bloc404">
        <div class=" row d-flex justify-content-center align-items-center">
            <p class="four">4</p>
            <!-- Gif  ↓ -->
            <div class="travolta mx-5 d-flex justify-content-center align-items-center">
                <img src="<?php echo $env->env_estatico?>img/travolta-gif-repeat.gif" class="gif" alt="" title="">
            </div>
            <p class="four">4</p>
        </div>
        <p class="ms2 mt-4">
            Isso é mais informação do que você precisava... Click <a href="<?php echo $env->env_url_mod;?>">Aqui</a> e volte para a página principal.<br>

        </p>
    </div>
</main>
</body>
</html>
<script type="application/javascript">
    //
    var largeur = $('.travolta').width();
    $('.travolta').css('height', largeur);
    $(window).resize(function(){
        largeur = $('.travolta').width();
        $('.travolta').css({'height': largeur});
    });
</script>