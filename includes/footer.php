<?php
if (!isset($_SESSION["uptime"])) {
    if ($_ENV['ENVI'] == "p") {
        ////////////////////////
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.uptimerobot.com/v2/getMonitors",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 1,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "api_key=m779055716-5ec43a1f9e56cfdc61d2cee3&format=json&types=1&custom_uptime_ratios=30",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $monit1 = json_decode($response);
            $monit2 = $monit1->monitors;
            $monit3 = $monit2[0];
            $_SESSION["uptime"] = "<i title='analisado 24/7'>";
            $_SESSION["uptime"] .= number_format($monit3->custom_uptime_ratio, 2, ',', '.');
            $_SESSION["uptime"] .= "% de uptime";
            $_SESSION["uptime"] .= "</i>";
        }

    } else {
        $_SESSION["uptime"] = "00.00% de uptime";
    }
}
?>
<hr>

<footer>
    <div class="container text-center">
        <p>
            <i class="fa fa-paint-brush"></i>
            <strong class="text-danger">+</strong>
            <i class="fa fa-code"></i>
            <strong class="text-danger">+</strong>
            <i class="fa fa-cloud"></i>
            <a href="https://www.facebook.com/flavioworks" target="_blank" title="Construimos, testamos e implementamos em dias, não meses.">
                FlavioW<i class="fa fa-cogs"></i>rks
            </a>
            <?php echo number2roman('2016',true); ?> -
            <?php echo number2roman(date("Y"),true); ?>
        </p>
        <ul class="list-inline pt-0">
            <li class="list-inline-item">
                <?php if (isset($_SESSION["uptime"])){echo $_SESSION["uptime"];}  ?>
            </li>
            <li class="list-inline-item">
                <a href="?pg=Vtermosdeservico" target="_blank">Termos de serviço</a>
            </li>
        </ul>
    </div>
</footer>


<!--timeload-->
<div class="timeload" id="timeload">
    <?php
    //contagem de tempo de processamento, inicio em * index.php
    list($usec, $sec) = explode(" ", microtime());
    $script_end = (float) $sec + (float) $usec;
    $elapsed_time = round($script_end - $script_start, 5);
    $elapsed_time=number_format($elapsed_time, 3, ',', '.');

    $timeload = "A requisição foi processada em "
                . $elapsed_time
                ." segundos.. ";
                //. round(((memory_get_peak_usage(true) / 1024) / 1024). 2). " MBs";

    echo $timeload;
    
    ?>
</div>

<!-- esconde tempo de timeload -->
<script>
    $(function () {
        var fechadiv = setInterval(function () {
            $("#timeload").hide();
        }, 1500);
    });
</script>


<?php
//se não quizer que apareceça os onlines set a variavel $oculta_online
if (!isset($oculta_online)){
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="1") {
        include_once("{$env->env_root}/includes/mcu/online.php");
    }
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="2") {
        include_once("{$env->env_root}/includes/caf/online.php");
    }
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="3") {
        include_once("{$env->env_root}/includes/mpref/online.php");
    }
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="4") {
        include_once("{$env->env_root}/includes/smem/online.php");
    }
}
?>
