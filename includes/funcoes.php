<?php
/**
 * DIAS ENTRE 02 DATAS
 * @author Norberto ALcântara
 * @copyright (c) Célula Nerd, 2019
 *
 * @param type $data_inicial 2013-08-01
 * @param type $data_final 2013-08-16
 * @return type
 */
function diasDatas($data_inicial,$data_final) {
    $diferenca = strtotime($data_final) - strtotime($data_inicial);
    $dias = floor($diferenca / (60 * 60 * 24));
    return $dias;
}


///removetraçobarra
function limpadocumento($valor){
    $valor = trim($valor);
    $valor = str_replace(".", "", $valor);
    $valor = str_replace(",", "", $valor);
    $valor = str_replace("-", "", $valor);
    $valor = str_replace("/", "", $valor);
    $valor = str_replace("(", "", $valor);
    $valor = str_replace(")", "", $valor);
    $valor = str_replace("\"", "", $valor);
    $valor = str_replace("'", "", $valor);
    $valor = str_replace(";", "", $valor);
    $valor = str_replace(":", "", $valor);
    $valor = str_replace("#", "", $valor);
    $valor = str_replace("'", "", $valor);
    return $valor;
}

///////////////////////////////////////////////////////////////////////////////////////
//converte a data para o formato utilizado pelo MySQL
// tambem testa se a data é valida, retorna 0 se for falsa
//data
function data2banco ($data) {
    if ( ! $data ) return 0;
    $data=explode('/',$data);
    if (isset($data[2]) && isset($data[1]) && isset($data[0]) && $data[2]>=1920){
        $data=$data[2].'-'.$data[1].'-'.$data[0];
        return $data;
    }else{
        return 0;
    }

}
//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA
function dataBanco2data ($data) {
    if ( ! $data ) return 0;
    $data=explode('-',$data);
    $data=$data[2].'/'.$data[1].'/'.$data[0];
    return $data;
}
//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA / HORA
function datahoraBanco2data ($data) {
    if ( ! $data ) return 0;
    $data=explode(' ',$data);
    $d=$data[0];
    $h=$data[1];
    $d=explode('-',$d);
    $d=$d[2].'/'.$d[1].'/'.$d[0].' ';
    $data=$d.$h;
    return $data;
}

// DATA / HORA banco to data
function datahoraData2Banco ($data) {
    if ( ! $data ) return 0;
    $data=explode(' ',$data);
    $d=$data[0];
    $h=$data[1];
    $d=explode('/',$d);
    $d=$d[2].'-'.$d[1].'-'.$d[0].' ';
    $data=$d.$h;
    return $data;
}

//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA / date
function dataRetiraHora ($data) {
    if ( ! $data ) return 0;
    $data=explode(' ',$data);
    $d=$data[0];
    $d=explode('-',$d);
    $d=$d[2].'/'.$d[1].'/'.$d[0];
    $data=$d;
    return $data;
}
///////////////////////////////////////////////////////////////////////////////////////






// DATA now
function dataNow () {
    $data=date("Y-m-d H:i:s");
    return $data;
}


///////////////////////////////////////////////////////////////////////////////////////
// Saudação apartir da Hora do dia
function Comprimentar(){
    $hr = date(" H ");
    if($hr>=0 && $hr<6){
        return "Boa Noite ";
    }
    if($hr>=6 && $hr<12){
        return "Bom Dia ";
    }
    if($hr>=12 && $hr<=18){
        return "Boa Tarde ";
    }
        if($hr>=19 && $hr<=24){
        return "Boa Noite ";
    }
}
///////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////
//função simples de calculo de idade
function Calculo_Idade($data) {
    //Data atual
    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');
    $nascimento = explode('-', $data);
    $dianasc = ($nascimento[2]);
    $mesnasc = ($nascimento[1]);
    $anonasc = ($nascimento[0]);
    //Calculando sua idade
    $idade = $ano - $anonasc; // simples, ano- nascimento!
    if ($mes < $mesnasc) // se o mes é menor, só subtrair da idade
    {
        $idade--;
        return $idade;
    }
    elseif ($mes == $mesnasc && $dia <= $dianasc) // se esta no mes do aniversario mas não passou ou chegou a data, subtrai da idade
    {
        $idade--;
        return $idade;
    }
    else // ja fez aniversario no ano, tudo certo!
    {
        return $idade;
    }
}
///////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Função para remover acentos de uma string
//function remover_caracter($string) {
//    $string = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
//    return $string;
//}
function remover_caracter($texto){
    $troca = [
        'á' => 'a',
        'à' => 'a',
        'ã' => 'a',
        'â' => 'a',
        'é' => 'e',
        'ê' => 'e',
        'í' => 'i',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ú' => 'u',
        'ü' => 'u',
        'Á' => 'A',
        'À' => 'A',
        'Ã' => 'A',
        'Â' => 'A',
        'É' => 'E',
        'Ê' => 'E',
        'Í' => 'I',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ú' => 'U',
        'Ü' => 'U',
        'ç' => 'c',
        'Ç' => 'C',
        '\'' => '',
    ];
    return strtr($texto, $troca);
}
//////////////////////////////////////////////////////////////////////////////////////////////////
function Nascimentoverifica($data){
    if($data!="1900-01-01" and $data!="" and $data!="1000-01-01") {
        return dataBanco2data ($data);
    }else{
        $rt = "<i>Não preenchido</i>";
        return dataBanco2data ($data);
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////
///
///  //////////////////////////////////////////////////////////////////////////////////////////////////
function fncgetmes($data){

    if($data>=1 and $data<=12) {
        $numero_mes = $data * 1;
        $mes = array('', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        return $mes[$numero_mes];
    }else{
        return $data;
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * Validate a date
 *
 * @param    string    $data
 * @param    string    formato
 * @return    bool
 * NAO VI FUNCIONAR AINDA
 */
function validaData($data, $formato = 'DD/MM/AAAA') {
    switch($formato) {
        case 'DD-MM-AAAA':
        case 'DD/MM/AAAA':
            list($d, $m, $a) = preg_split('/[-./ ]/', $data);
            break;
        case 'AAAA/MM/DD':
        case 'AAAA-MM-DD':
            list($a, $m, $d) = preg_split('/[-./ ]/', $data);
            break;
        case 'AAAA/DD/MM':
        case 'AAAA-DD-MM':
            list($a, $d, $m) = preg_split('/[-./ ]/', $data);
            break;
        case 'MM-DD-AAAA':
        case 'MM/DD/AAAA':
            list($m, $d, $a) = preg_split('/[-./ ]/', $data);
            break;
        case 'AAAAMMDD':
            $a = substr($data, 0, 4);
            $m = substr($data, 4, 2);
            $d = substr($data, 6, 2);
            break;
        case 'AAAADDMM':
            $a = substr($data, 0, 4);
            $d = substr($data, 4, 2);
            $m = substr($data, 6, 2);
            break;
        default:
            throw new Exception( "Formato de data inválido");
            break;
    }
    return checkdate($m, $d, $a);
}


// retorna se já passou 24 horas
//retor 1 ainda nao passou
//retorna 0 já passou
function Expiradata($data,$periodo){
    //gera timestamp unix atual
    $now = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    //soma dias
    $dat = date('Y-m-d', strtotime("+{$periodo} days",strtotime($data)));
    $dd = explode("-", $dat);
    //gera timestamp unix
    $datacompara = mktime("08", "00", "01", $dd[1], $dd[2], $dd[0]);
    //compara se data atual é maior que atualização
    if ($now>$datacompara) {
        return 0;
    }else{
        return 1;
    }
}

function Expiradatahora($data,$periodo){
    //gera timestamp unix atual
    $now = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    //soma dias
    $dat = date('Y-m-d-H-i-s', strtotime("+{$periodo} days",strtotime($data)));
    $dd = explode("-", $dat);
    //gera timestamp unix
    $datacompara = mktime($dd['3'], $dd['4'], $dd['5'], $dd[1], $dd[2], $dd[0]);
    //compara se data atual é maior que atualização
    if ($now>$datacompara) {
        return 0;
    }else{
        return 1;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//emoji
function Smilify(&$subject, $ambiente)
{
    $smilies = array(

        ':confuso' => 'confuso',
        ':surpreso'  => 'surpreso',
        ':piscada' => 'piscada',
        ':lingua' => 'lingua',
        ':haha' => 'haha',
        ':legal' => 'legal',
        ':feliz' => 'feliz',
        ':sorrir' => 'feliz',
        ':sorriso' => 'feliz',
        ':triste' => 'triste',
        ':infeliz' => 'triste',
        ':anjo' => 'anjo',
        ':santinho' => 'santinho',
        ':bandido' => 'bandido',
        ':beijo' => 'beijo',
        ':choro' => 'choro',
        ':fogo' => 'fogo',
        ':mal' => 'mal',
        ':merda' => 'merda',
        ':coco' => 'merda',
        ':ninja' => 'ninja',
        ':porfavor' => 'porfavor',
        ':raiva' => 'raiva',
        ':rindo' => 'rindo',
        ':silencio' => 'silencio',
        ':sono' => 'sono',
        ':xingar' => 'xingar',
        ':bomba' => 'bomba',
        ':demonio' => 'demonio',
        ':dinheiro' => 'dinheiro',
        ':fantasma' => 'fantasma',
        ':susto' => 'susto',
        ':medo' => 'medo',
        ':amor' => 'amor',
        ':ok' => 'ok',
        ':joia' => 'ok',
        ':fine' => 'fine',
        ':network' => 'network',
    );

    $sizes = array(
        'haha' => 25,
        'legal' => 25,
        'haha' => 25,
        'confuso' => 25,
        'surpreso' => 25,
        'triste' => 25,
        'feliz' => 25,
        'lingua' => 25,
        'piscada' => 25,
        'anjo' => 25,
        'santinho' => 25,
        'bandido' => 25,
        'beijo' => 25,
        'choro' => 25,
        'fogo' => 25,
        'mal' => 25,
        'merda' => 25,
        'ninja' => 25,
        'porfavor' => 25,
        'raiva' => 25,
        'rindo' => 25,
        'silencio' => 25,
        'sono' => 25,
        'xingar' => 25,
        'bomba' => 25,
        'demonio' => 25,
        'dinheiro' => 25,
        'fantasma' => 25,
        'susto' => 25,
        'medo' => 25,
        'amor' => 25,
        'ok' => 25,
        'fine' => 25,
        'network' => 25,
    );

    $replace = array();
    foreach ($smilies as $smiley => $imgName)
    {
        $size = $sizes[$imgName];
        array_push($replace, '<img src="'.$ambiente.'public/img/emoji/'.$imgName.'.png" alt="'.$smiley.'" width="'.$size.'" height="'.$size.'" />');
    }
    $subject = str_replace(array_keys($smilies), $replace, $subject);
    return $subject;
}
////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////
//se mes tem 30 31 28 ou 29 dias
function UltimoDia($ano,$mes){
    if (((fmod($ano,4)==0) and (fmod($ano,100)!=0)) or (fmod($ano,400)==0)) {
        $dias_fevereiro = 29;
    } else {
        $dias_fevereiro = 28;
    }
    switch($mes) {
        case "01": return 31; break;
        case "02": return $dias_fevereiro; break;
        case "03": return 31; break;
        case "04": return 30; break;
        case "05": return 31; break;
        case "06": return 30; break;
        case "07": return 31; break;
        case "08": return 31; break;
        case "09": return 30; break;
        case "10": return 31; break;
        case "11": return 30; break;
        case "12": return 31; break;
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////
/// monta maskara
function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for($i = 0; $i<=strlen($mask)-1; $i++)
    {
        if($mask[$i] == '#')
        {
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }
        else
        {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }
    }
    return $maskared;
}
///use echo mask($cnpj,'##.###.###/####-##');


//valida cpf
function validaCPF($cpf) {

    // Extrai somente os números
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

    // Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return 0;
    }
    // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return 0;
    }
    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return 0;
        }
    }
    return true;
}

function roman2number($roman){
    $conv = array(
        array("letter" => 'I', "number" => 1),
        array("letter" => 'V', "number" => 5),
        array("letter" => 'X', "number" => 10),
        array("letter" => 'L', "number" => 50),
        array("letter" => 'C', "number" => 100),
        array("letter" => 'D', "number" => 500),
        array("letter" => 'M', "number" => 1000),
        array("letter" => 0, "number" => 0)
    );
    $arabic = 0;
    $state = 0;
    $sidx = 0;
    $len = strlen($roman);

    while ($len >= 0) {
        $i = 0;
        $sidx = $len;

        while ($conv[$i]['number'] > 0) {
            if (strtoupper(@$roman[$sidx]) == $conv[$i]['letter']) {
                if ($state > $conv[$i]['number']) {
                    $arabic -= $conv[$i]['number'];
                } else {
                    $arabic += $conv[$i]['number'];
                    $state = $conv[$i]['number'];
                }
            }
            $i++;
        }

        $len--;
    }

    return($arabic);
}


function number2roman($num,$isUpper=true) {
    $n = intval($num);
    $res = '';

    /*** roman_numerals array ***/
    $roman_numerals = array(
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1
    );

    foreach ($roman_numerals as $roman => $number)
    {
        /*** divide to get matches ***/
        $matches = intval($n / $number);

        /*** assign the roman char * $matches ***/
        $res .= str_repeat($roman, $matches);

        /*** substract from the number ***/
        $n = $n % $number;
    }

    /*** return the res ***/
    if($isUpper) return $res;
    else return strtolower($res);
}

//////////////////////////////////
//retorna endereco: cep, logradouro, complemento, bairro, localidade, uf, unidade, ibge, gia
function get_enderecobycep($cep){


    // formatar o cep removendo caracteres nao numericos
    $cep = preg_replace("/[^0-9]/", "", $cep);
    $url = "http://viacep.com.br/ws/$cep/xml/";

    $xml = simplexml_load_file($url);
    return $xml;
}

//////////
/// obter o id de um video no youtube
function getIDYouTube($url){
    parse_str( parse_url( $url, PHP_URL_QUERY ), $youtubeID );
    if (array_key_exists("v", $youtubeID))
    {
        $id = $youtubeID['v'];
        return $id;
    }else{
        $explode = explode("/", $url);
        return end($explode);
    }
}
//////////
/// obter o id de um video no youtube
///


//////////////
/// gera palavras
function GeraPalavra() {
    $vogais = array('a','e','i','o','u');
    $consoantes = array('b','c','d','f','g','h','nh','lh','ch','j','k','l','m','n','p','qu','r','rr','s','ss','t','v','w','x','y','z',);

    $palavra = '';
    $tamanho_palavra = rand(2,5);
    $contar_silabas = 0;
    while($contar_silabas < $tamanho_palavra){
        $vogal = $vogais[rand(0,count($vogais)-1)];
        $consoante = $consoantes[rand(0,count($consoantes)-1)];
        $silaba = $consoante.$vogal;
        $palavra .=$silaba;
        $contar_silabas++;
        unset($vogal,$consoante,$silaba);
    }
    return $palavra;
    unset($vogais,$consoantes,$palavra,$tamanho_palavra,$contar_silabas);
}



//////////////////////////////////
//criptografa para base64_encode
function get_criptografa64($text){
    $cript = base64_encode($text);
    return $cript;
}

//////////////////////////////////
//descriptografa para base64_encode
function get_descriptografa64($text){
    $cript = base64_decode($text);
    return $cript;
}