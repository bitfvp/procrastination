<!--modalmodalmsgs-->
<!-- Modal -->
<div class="modal flip-right-bounce" id="modalmsgs" tabindex="-1" role="dialog" aria-labelledby="modalmsgsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalmsgsLabel">Caixa de mensagens</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="mmsgs" class="modal-body">
            </div>

            <div class="modal-footer">
                <button id="chaticone2" class="btn btn-primary btn-sm fa fa-comment" data-dismiss="modal" aria-label="Close">
                    Nova mensagem
                </button>
            </div>
            <script>
                $('#chaticone2').on("click", function () {
                    $('#chaticone').hide("slow");
                    $('#cvp').removeClass("cvpout");
                    $('#cv').removeClass("cv_no_slide");
                    $('#cv').addClass("cv_slide");
                    $('#cvp_busca_pesquisa').focus();
                    var fechadialog = setTimeout(function () {
                        $('#cvp').addClass("cvpout");
                        $('#chaticone').show("slow");
                        $('#cv').removeClass("cv_slide");
                        $('#cv').addClass("cv_no_slide");
                    }, 10000);
                });
            </script>
        </div>
    </div>
</div>
<!--fim de modalmsgs-->

<?php
if (!isset($_SESSION['rankstaff'])) {
    $sql = "SELECT `id`,`cont_rank`,`nome` FROM `tbl_users` WHERE ((`cont_rank`<>0) and (`status`=1) and (`matriz`=3)) ORDER BY cont_rank DESC LIMIT 0,19";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $rankstaff = $consulta->fetchAll();
    $_SESSION['rankstaff'] = $rankstaff;
    $sql = null;
    $consulta = null;
}
$rankstaff=$_SESSION['rankstaff'];
?>
<!-- Modal pontos -->
<div class="modal fade" id="modalPontos" tabindex="-1" role="dialog" aria-labelledby="modalPontosLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body mb-0">
                <table class="table table-stripe  table-hover table-condensed">
                    <thead>
                    <tr>
                        <th  class=""></th>
                        <th class=''>Nome</th>
                        <th class="">Pontos</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $ccont=0;
                    foreach ($rankstaff as $rks){
                        $cont_rank=number_format($rks['cont_rank'], 0, '.', ',');
                        $ccont+=1;
                        echo "<tr>";
                        echo "<td  class='text-center'>";
                        if($ccont==1){
                            echo "<img src='";
                            echo $env->env_estatico;
                            echo "img/gold.png' alt=''>";
                            echo "</td>";
                            echo "<td >{$rks['nome']}</td>";
                            echo "<td>{$cont_rank}</td>";
                            echo "</tr>";
                        }
                        if($ccont==2){
                            echo "<img src='";
                            echo $env->env_estatico;
                            echo "img/silver.png' alt=''>";
                            echo "</td>";
                            echo "<td>{$rks['nome']}</td>";
                            echo "<td>{$cont_rank}</td>";
                            echo "</tr>";
                        }
                        if($ccont==3){
                            echo "<img src='";
                            echo $env->env_estatico;;
                            echo "img/bronze.png' alt=''>";
                            echo "</td>";
                            echo "<td>{$rks['nome']}</td>";
                            echo "<td>{$cont_rank}</td>";
                            echo "</tr>";
                        }
                        if($ccont>3){
                            echo $ccont;
                            echo "</td>";
                            echo "<td>{$rks['nome']}</td>";
                            echo "<td>{$cont_rank}</td>";
                            echo "</tr>";
                        }

                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--fim de modal pontos-->


<?php
if (!isset($_SESSION['user_cont_rank'])){
    try{
        $sql="select cont_rank from tbl_users WHERE id=? and matriz=1 limit 1";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindParam(1, $_SESSION['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $_SESSION['user_cont_rank'] = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
}


$ponto=$cont_rank=number_format($_SESSION['user_cont_rank'][0], 0, '.', ',');
$ponto .="&nbsp;Pontos";

if($contmsg!=0){
    $comp_msg="<span id='msgico' class='pulse-slow fa fa-envelope'>&nbsp;</span>";
}else{
    $comp_msg="<span id='msgico' class='fa fa-address-book'>&nbsp;</span>";
}
$primeiroNome = explode(" ", $_SESSION["nome"]);
$comp_msg.="Olá ";
$comp_msg.=$primeiroNome[0]; // Fulano
$comp_msg.=", ".Comprimentar();
?>

<!-- Modal temas -->
<div class="modal dance" id="modaltemas" tabindex="-1" role="dialog" aria-labelledby="modaltemasLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body mb-0">
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=1">
                            Original&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=2">
                            <i class="fa fa-tint"></i>&nbsp;Celeste
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=3">
                            Universo&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=4">
                            <i class="fa fa-tint"></i>&nbsp;Cibernético
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=5">
                            Night&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=6">
                            <i class="fa fa-tint"></i>&nbsp;Plano
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=7">
                            Diário&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=8">
                            <i class="fa fa-tint"></i>&nbsp;Solar
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=9">
                            Material design&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=10">
                            <i class="fa fa-tint"></i>&nbsp;Hortelã
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=11">
                            Vibrante&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=12">
                            <i class="fa fa-tint"></i>&nbsp;Cimento
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=13">
                            Simples&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=14">
                            <i class="fa fa-tint"></i>&nbsp;Cartum
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=15">
                            Ardósia&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=16">
                            <i class="fa fa-tint"></i>&nbsp;Nobre
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=17">
                            Laboratório&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=18">
                            <i class="fa fa-tint"></i>&nbsp;Super herói
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <a href ="?aca=changetheme&t=19">
                            Re-Unido&nbsp;<i class="fa fa-tint float-right"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href ="?aca=changetheme&t=20">
                            <i class="fa fa-tint"></i>&nbsp;Tibete
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<!--fim de modal temas-->