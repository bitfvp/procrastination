<?php
if ($allow["allow_8"]==1){

    echo "<a href='".$env->env_url."mcu/agenda' target='_blank' >"
        ."<span id='agendaicone' class='fa-stack' data-toggle='popover' data-placement='bottom' data-trigger='focus' data-content='Contatos'>"
        ."<i class='fa fa-circle fa-stack-2x text-light'></i>"
        ."<i class='fas fa-phone fa-stack-1x text-dark'></i>"


        ."</span>"
        ."</a>";
}
if ($allow["allow_75"]==1){
    echo "<a href='".$env->env_url."mcu/f' target='_blank' >"
        ."<span id='filaicone' class='fa-stack ' title='Fila'>"
        ."<i class='fa fa-circle fa-stack-2x text-light'></i>"
        ."<i class='fa fa-users fa-stack-1x text-dark'></i>"

        ."</span>"
        ."</a>";
}
?>

<!--chama msg recebida-->
<script type="text/javascript">
    $.ajaxSetup({cache: false});
    $(document).ready(function () {
        $('#mmsgs').load('<?php echo $env->env_url;?>includes/mcu/alerta_msgs.php');
    });
    $(document).ready(function () {
        setInterval(function () {
            $('#mmsgs').load('<?php echo $env->env_url;?>includes/mcu/alerta_msgs.php')
        }, 50000);
    });
</script>

<?php
if (isset($_POST['troca_chat_val']) and $_POST['troca_chat_val']>0 and $_POST['troca_chat_val']!=$_SESSION['id']){
    $_SESSION['id_chat']=$_POST['troca_chat_val'];
    //esse script nao deixa repetir o post
    echo "<script>";
    echo "if (window.history.replaceState) {";
    echo "window.history.replaceState (null, null, window.location.href);";
    echo "}";
    echo "</script>";
}
if ($env->env_url=="https://syssocial.net/"){
    $URL_ATUAL= "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}else{
    $URL_ATUAL= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}




                    if (isset($_SESSION['id_chat']) and is_numeric($_SESSION['id_chat']) and $_SESSION['id_chat']>0){
                      $chat_nome=explode(' ',fncgetusuario($_SESSION['id_chat'])['nome']);
                        $chat_nome=$chat_nome[0]." ".end($chat_nome);
                        ?>
                        <!--começa novo messenger-->
                        <div id="cv" class="cv cv_no_slide">
                            <div class="cv_todo">
                                <div class="cv_topo">
                                    <span class="cv_titulo">
                                        <?php echo $chat_nome;?>
                                        <button id="btn_clip" class="btn btn-sm"><i class="fa fa-paperclip text-light"></i></button>
                                        <button id="btn_emoji" class="btn btn-sm"><i class="fa fa-smile text-light"></i></button>
                                    </span>
                                    <button class="cv_topo_times" id="cv_topo_times">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                    <script>
                                        $('#cv_topo_times').click( function(){
                                            $.ajax({
                                                <?php //Para aonde vai a requisição (um outro arquivo)?>
                                                url: '<?php echo $env->env_url;?>includes/mcu/chat_sair_conversa.php',
                                                <?php //Os dados que devem ser passados (adicione um id ao input e coloque no lugar de #input)?>
                                                data: {ts: $('#input').val()},
                                                <?php //Como vai ser passados os dados (o mesmo method do form)?>
                                                method: 'POST',
                                            }).done(function(data) {
                                                <?php //Quando finalizar a requisição com sucesso?>
                                                <?php // alert('Concluído com êxito');?>
                                                $(location).attr('href', '<?php echo "$URL_ATUAL"; ?>');
                                            }).fail(function(error) {
                                                <?php //Quando finalizar a requisição com falha?>
                                                alert('Erro: ' + error);
                                            });

                                        });
                                    </script>
                                </div>
                                <div class="cv_emojis" id="cv_emojis" style="display: none;">
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('amor')"><img src="<?php echo $env->env_estatico; ?>img/emoji/amor.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('anjo')"><img src="<?php echo $env->env_estatico; ?>img/emoji/anjo.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('bandido')"><img src="<?php echo $env->env_estatico; ?>img/emoji/bandido.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('beijo')"><img src="<?php echo $env->env_estatico; ?>img/emoji/beijo.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('bomba')"><img src="<?php echo $env->env_estatico; ?>img/emoji/bomba.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('choro')"><img src="<?php echo $env->env_estatico; ?>img/emoji/choro.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('confuso')"><img src="<?php echo $env->env_estatico; ?>img/emoji/confuso.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('demonio')"><img src="<?php echo $env->env_estatico; ?>img/emoji/demonio.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('dinheiro')"><img src="<?php echo $env->env_estatico; ?>img/emoji/dinheiro.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('fantasma')"><img src="<?php echo $env->env_estatico; ?>img/emoji/fantasma.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('feliz')"><img src="<?php echo $env->env_estatico; ?>img/emoji/feliz.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('fine')"><img src="<?php echo $env->env_estatico; ?>img/emoji/fine.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('fogo')"><img src="<?php echo $env->env_estatico; ?>img/emoji/fogo.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('haha')"><img src="<?php echo $env->env_estatico; ?>img/emoji/haha.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('legal')"><img src="<?php echo $env->env_estatico; ?>img/emoji/legal.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('lingua')"><img src="<?php echo $env->env_estatico; ?>img/emoji/lingua.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('mal')"><img src="<?php echo $env->env_estatico; ?>img/emoji/mal.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('medo')"><img src="<?php echo $env->env_estatico; ?>img/emoji/medo.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('merda')"><img src="<?php echo $env->env_estatico; ?>img/emoji/merda.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('network')"><img src="<?php echo $env->env_estatico; ?>img/emoji/network.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('ninja')"><img src="<?php echo $env->env_estatico; ?>img/emoji/ninja.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('ok')"><img src="<?php echo $env->env_estatico; ?>img/emoji/ok.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('piscada')"><img src="<?php echo $env->env_estatico; ?>img/emoji/piscada.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('porfavor')"><img src="<?php echo $env->env_estatico; ?>img/emoji/porfavor.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('raiva')"><img src="<?php echo $env->env_estatico; ?>img/emoji/raiva.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('rindo')"><img src="<?php echo $env->env_estatico; ?>img/emoji/rindo.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('santinho')"><img src="<?php echo $env->env_estatico; ?>img/emoji/santinho.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('silencio')"><img src="<?php echo $env->env_estatico; ?>img/emoji/silencio.png" alt=""></button>
                                    <br>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('sono')"><img src="<?php echo $env->env_estatico; ?>img/emoji/sono.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('surpreso')"><img src="<?php echo $env->env_estatico; ?>img/emoji/surpreso.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('susto')"><img src="<?php echo $env->env_estatico; ?>img/emoji/susto.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('triste')"><img src="<?php echo $env->env_estatico; ?>img/emoji/triste.png" alt=""></button>
                                    <button id="emoji" class="btn btn-sm m-1" onclick="isereemoji('xingar')"><img src="<?php echo $env->env_estatico; ?>img/emoji/xingar.png" alt=""></button>
                                </div>
                                <div class="cv_msgs" id="cv_msgs"></div>
                                <div class="cv_footer">
                                    <form action="<?php echo $URL_ATUAL;?>" class="cv_footer_campo" id="form-message" method="post" enctype="multipart/form-data">
                                        <input type='hidden' name='chat_envia_msg' value='<?php echo $_SESSION['id_chat'];?>' />
                                        <input id="esc_texto" name="esc_texto" type="text" autofocus autocomplete="off" placeholder="Digite sua mensagem..."/>
                                        <div class="form-group" id="esc_arquivo" style="display: none;">
                                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="esc_arquivo[]" value="" multiple>
                                        </div>
                                        <button id="sendMessage"><i class="fa fa-play text-dark"></i></button>
                                    </form>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $.ajaxSetup({cache: false});
                                $(document).ready(function () {
                                    $('#cv_msgs').load('<?php echo $env->env_url;?>includes/mcu/chat_msg_corpo.php?folder=<?php echo $env->env_nivel_folder;?>');
                                });
                                $(document).ready(function () {
                                    setInterval(function () {
                                        $('#cv_msgs').load('<?php echo $env->env_url;?>includes/mcu/chat_msg_corpo.php?folder=<?php echo $env->env_nivel_folder;?>')
                                    }, 30000);
                                });

                                function escondeemoji() {
                                    $("#cv_msgs").toggle();
                                    $("#cv_emojis").toggle();
                                }
                                $(document).ready(function () {
                                    $("#btn_emoji").click(escondeemoji);
                                })
                                function escondefile() {
                                    $("#esc_texto").toggle();
                                    $("#esc_arquivo").toggle();
                                }
                                $(document).ready(function () {
                                    $("#btn_clip").click(escondefile);
                                });

                                function isereemoji(emoji) {
                                    var str = $("#esc_texto").val();
                                    $("#cv_msgs").toggle();
                                    $("#cv_emojis").toggle();
                                    $('#esc_texto').val(str+" :"+emoji);
                                    $('#esc_texto').focus();
                                }
                                $( document ).ready(function() {
                                    var rola = setTimeout(function () {
                                        $('.cv_msgs').scrollTop(12000);
                                    }, 250);
                                });
                            </script>
                        </div>

                        <?php
                    }
                    ?>


<!--começa novo messenger-->
<div class="cvp cvpout" id="cvp">
    <div class="cvp_todo">
        <div class="cvp_busca" id="cvp_busca">
            <input type="text" class="cvp_busca_pesquisa" id="cvp_busca_pesquisa" autocomplete="off" placeholder="Pesquisar contato">
        </div>
        <div class="cvp_corpo" id="cvp_corpo"></div>
    </div>
</div>

<span id='chaticone' class='fa-stack fa-2x' title='Online no momento<?php echo $env->env_nivel_folder;?>' >
    <i class='fa fa-circle fa-stack-2x text-light'></i>
    <i class='fas fa-comment fa-stack-1x text-dark'></i>
</span>

<script type="text/javascript">
    $(document).ready(function () {
        $('#cvp_busca input[type="text"]').on("keyup", function () {
            /* Get input value on change */
            var inputVal = $(this).val();
            var urlref = '<?php echo $URL_ATUAL;?>';
            if (inputVal.length) {
                $.get("<?php echo $env->env_url;?>includes/mcu/chat_busca_contato.php", {term: inputVal, urltransf: urlref}).done(function (data) {
                    // Display the returned data in browser
                    //alert('aaaa');
                    $('#cvp_corpo').html(data);
                });
            } else {
                $('#cvp_corpo').html('<strong class="text-center text-info"><i class="fa fa-keyboard"></i> Volte a digitar...</strong>');
            }
        });
    });
    $.ajaxSetup({cache: false});
    $(document).ready(function () {
        var urlref = '<?php echo $URL_ATUAL;?>';
            $.get("<?php echo $env->env_url;?>includes/mcu/chat_busca_contato.php", {urltransf: urlref}).done(function (data) {
                // Display the returned data in browser
                //alert('aaaa');
                $('#cvp_corpo').html(data);
            });
    });

    $('#chaticone').on("click", function () {
        $('#chaticone').hide("slow");
        $('#cvp').removeClass("cvpout");
        $('#cv').removeClass("cv_no_slide");
        $('#cv').addClass("cv_slide");
        $('#cvp_busca_pesquisa').focus();
        var fechadialog = setTimeout(function () {
            $('#cvp').addClass("cvpout");
            $('#chaticone').show("slow");
            $('#cv').removeClass("cv_slide");
            $('#cv').addClass("cv_no_slide");
        }, 10000);
    });
</script>