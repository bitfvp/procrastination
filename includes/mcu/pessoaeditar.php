<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
    <div class="row">
            <div class="col-md-6">
                <?php
                if ($allow["allow_4"]==1){ ?>
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
                    <?php
                }
                ?>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
            <label for="nome">NOME</label><input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
        </div>
        <script type="text/javascript">
            $(function(){
                var campo = $("#nome");
                campo.keyup(function(e){
                    e.preventDefault();
                    campo.val($(this).val().toLowerCase());
                });
            });

        </script>
        <div class="col-md-3">
            <label for="nome_social">NOME SOCIAL</label>
            <input autocomplete="off" id="nome_social" type="text" class="form-control" name="nome_social" value="<?php echo $pessoa['nome_social']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="sexo">SEXO</label>
            <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['sexo'];
                } ?>">
                    <?php
                    if ($pessoa['sexo'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['sexo'] == 1) {
                        echo "Feminino";
                    }
                    if ($pessoa['sexo'] == 2) {
                        echo "Masculino";
                    }
                    if ($pessoa['sexo'] == 3) {
                        echo "Indefinido";
                    }
                    ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Feminino</option>
                <option value="2">Masculino</option>
                <option value="3">Indefinido</option>
            </select>

        </div>
        <div class="col-md-3">
            <label for="nascimento">NASCIMENTO</label>
            <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for="cpf">CPF</label>
            <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#cpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
        </div>
        <div class="col-md-3">
            <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                             value="<?php echo $pessoa['rg']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#rg').mask('00.000.000.000', {reverse: true});
                });
            </script>
        </div>
        <div class="col-md-2">
            <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                   value="<?php echo $pessoa['uf_rg']; ?>"/>
        </div>
        <div class="col-md-3">
            <label for="nis">NIS</label><input autocomplete="off" id="nis" type="text" class="form-control" name="nis"
                                               value="<?php echo $pessoa['nis']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#nis').mask('000.000.000-00', {reverse: false});
                });
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label for="ctps">CARTEIRA DE TRABALHO</label>
            <input autocomplete="off" id="ctps" type="text" class="form-control" name="ctps" value="<?php echo $pessoa['ctps']; ?>"/>
        </div>
        <div class="col-md-3">
            <label for="cod_familiar">CÓDIGO FAMÍLIAR</label>
            <input autocomplete="off" id="cod_familiar" type="text" class="form-control" name="cod_familiar" value="<?php echo $pessoa['cod_familiar']; ?>"/>
        </div>
        <div class="col-md-3">
            <label for="responsavel_familiar">RESPONSÁVEL FAMILIAR</label>
            <select name="responsavel_familiar" id="responsavel_familiar" class="form-control">
                // vamos criar a visualização de rf
                <option selected="" value="<?php if ($pessoa['responsavel_familiar'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['responsavel_familiar'];
                } ?>">
                    <?php
                    if ($pessoa['responsavel_familiar'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['responsavel_familiar'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
        <div class="col-md-3">
            <label for="parentesco">PARENTESCO</label>
            <select name="parentesco" id="parentesco" class="form-control">
                // vamos criar a visualização de parentesco
                <option selected="" value="<?php if ($pessoa['parentesco'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['parentesco'];
                } ?>">
                    <?php
                    if (($pessoa['parentesco'] == 0) or ($pessoa['parentesco'] == "")) {
                        echo "Selecione...";
                    }
                    if ($pessoa['parentesco'] == 1) {
                        echo "Pessoa Responsavel pela Unidade Familiar";
                    }
                    if ($pessoa['parentesco'] == 2) {
                        echo "Conjuge ou companheiro(a)";
                    }
                    if ($pessoa['parentesco'] == 3) {
                        echo "Filho(a)";
                    }
                    if ($pessoa['parentesco'] == 4) {
                        echo "Enteado(a)";
                    }
                    if ($pessoa['parentesco'] == 5) {
                        echo "Neto(a) ou bisneto(a)";
                    }
                    if ($pessoa['parentesco'] == 6) {
                        echo "Pai ou mae";
                    }
                    if ($pessoa['parentesco'] == 7) {
                        echo "Sogro(a)";
                    }
                    if ($pessoa['parentesco'] == 8) {
                        echo "Irmao ou irma";
                    }
                    if ($pessoa['parentesco'] == 9) {
                        echo "Genro ou nora";
                    }
                    if ($pessoa['parentesco'] == 10) {
                        echo "Outro";
                    } ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Pessoa Responsavel pela Unidade Familiar</option>
                <option value="2">Conjuge ou companheiro(a)</option>
                <option value="3">Filho(a)</option>
                <option value="4">Enteado(a)</option>
                <option value="5">Neto(a) ou bisneto(a)</option>
                <option value="6">Pai ou mãe</option>
                <option value="7">Sogro(a)</option>
                <option value="8">Irmao ou irma</option>
                <option value="9">Genro ou nora</option>
                <option value="10">Outro</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <label for="pai">PAI</label>
            <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $pessoa['pai']; ?>"/>
        </div>
        <div class="col-md-5">
            <label for="mae">MÃE</label>
            <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $pessoa['mae']; ?>"/>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <label for="endereco">ENDEREÇO</label>
            <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="numero">NÚMERO</label>
            <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="bairro">BAIRRO</label>
            <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php
                $bairroid = $pessoa['bairro'];
                $getbairro=fncgetbairro($bairroid);
                ?>
                <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $pessoa['bairro']; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                foreach (fncbairrolist() as $item) {
                    ?>
                <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                    <?php echo $item['bairro']; ?>
                </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-md-1">
            <?php
            if ($allow["allow_5"]==1){
            $tempcaminho = $env->env_url;
            switch ($_SESSION['matriz']){
                case 1:
                    $tempcaminho.= "mcu/?pg=Vbl";
                    break;
                case 2;
                    $tempcaminho.= "caf/?pg=Vbl";
                    break;
            }
            ?>
            <label for="cad"><h6>CAD. BAIRRO</h6><a href="<?php echo $tempcaminho;?>" target="_blank"><i class="fa fa-plus"></i></a></label>
            <?php }?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label for="referencia">REFERÊNCIA</label>
            <input autocomplete="off" id="referencia" type="text" class="form-control" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label for="telefone">TELEFONE</label>
            <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                });
            </script>
        </div>

        <div class="col-md-3">
            <label for="raca_cor">RAÇA/COR</label>
            <select name="raca_cor" id="raca_cor" class="form-control">
                // vamos criar a visualização de cor-raça
                <option selected="" value="<?php if ($pessoa['raca_cor'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['raca_cor'];
                } ?>">
                    <?php
                    if (($pessoa['raca_cor'] == 0) or ($pessoa['raca_cor'] == "")) {
                        echo "Selecione...";
                    }
                    if ($pessoa['raca_cor'] == 1) {
                        echo "Branca";
                    }
                    if ($pessoa['raca_cor'] == 2) {
                        echo "Negra";
                    }
                    if ($pessoa['raca_cor'] == 3) {
                        echo "Amarela";
                    }
                    if ($pessoa['raca_cor'] == 4) {
                        echo "Parda";
                    }
                    if ($pessoa['raca_cor'] == 5) {
                        echo "Indigena";
                    } ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Branca</option>
                <option value="2">Negra</option>
                <option value="3">Amarela</option>
                <option value="4">Parda(a)</option>
                <option value="5">Indigena</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="agente_saude">AGENTE DE SAÚDE</label>
            <input autocomplete="off" id="agente_saude" type="text" class="form-control" name="agente_saude" value="<?php echo $pessoa['agente_saude']; ?>"/>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-md-4">
            <label for="possui_veiculo">POSSUI VEÍCULO</label>
            <select name="possui_veiculo" id="possui_veiculo" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['possui_veiculo'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['possui_veiculo'];
                } ?>">
                    <?php
                    if ($pessoa['possui_veiculo'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['possui_veiculo'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="paga_aluguel">PAGA ALUGUEL</label>
            <select name="paga_aluguel" id="paga_aluguel" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['paga_aluguel'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['paga_aluguel'];
                } ?>">
                    <?php
                    if ($pessoa['paga_aluguel'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['paga_aluguel'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
        <div class="col-md-4">
            <label for="possui_propriedade">POSSUI PROPRIEDADE</label>
            <select name="possui_propriedade" id="possui_propriedade" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['possui_propriedade'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['possui_propriedade'];
                } ?>">
                    <?php
                    if ($pessoa['possui_propriedade'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['possui_propriedade'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <label for="alfabetizado">ALFABETIZADO</label>
            <select name="alfabetizado" id="alfabetizado" class="form-control">// vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['alfabetizado'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['alfabetizado'];
                } ?>">
                    <?php
                    if ($pessoa['alfabetizado'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['alfabetizado'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="renda">RENDA MENSAL</label>
            <?php
            if ($pessoa['renda'] == "") {
                $z = 0;
            } else {
                $z = $pessoa['renda'];
            }
            ?>
            <input autocomplete="off" id="renda" type="number" class="form-control" name="renda" value="<?php echo $z; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="bpc">BPC</label>
            <select name="bpc" id="bpc" class="form-control">
                // vamos criar a visualização
                <?php
                if ($pessoa['bpc'] == "") {
                    $z = 0;
                } else {
                    $z = $pessoa['bpc'];
                }
                ?>
                <option selected="" value="<?php echo $z; ?>">
                    <?php
                    if ($pessoa['bpc'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['bpc'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
    </div>
<?php
if ($allow["allow_65"]!=1){
    $mostrar_telecentro="d-none";
}
?>
    <hr class="<?php echo $mostrar_telecentro;?>">
    <div class="row <?php echo $mostrar_telecentro;?>">
        <div class="col-md-5">
            <label for="curso">CURSO</label>
            <select name="curso" id="curso" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['curso'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['curso'];
                } ?>">
                    <?php
                    if ($pessoa['curso'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['curso'] == 1) {
                        echo "Cursando";
                    }
                    if ($pessoa['curso'] == 2) {
                        echo "Encerrado";
                    }
                    if ($pessoa['curso'] == 3) {
                        echo "Abandonado";
                    }
                    if ($pessoa['curso'] == 4) {
                        echo "Fila de Espera";
                    }
                    ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Cursando</option>
                <option value="2">Encerrado</option>
                <option value="3">Abandonado</option>
                <option value="4">Fila de Espera</option>
            </select>
        </div>
        <div class="col-md-5">
            <label for="horario">HORÁRIO</label>
            <select name="horario" id="horario" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['horario'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['horario'];
                } ?>">
                    <?php
                    if ($pessoa['horario'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['horario'] == 1) {
                        echo "2ª 4ª 6ª 13:30 as 15:00";
                    }
                    if ($pessoa['horario'] == 2) {
                        echo "2ª 4ª 6ª 15:00 as 16:00";
                    }
                    if ($pessoa['horario'] == 3) {
                        echo "3ª 5ª 13:00 as 15:00";
                    }
                    if ($pessoa['horario'] == 4) {
                        echo "3ª 5ª 15:00 as 17:00";
                    }
                    ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">2ª 4ª 6ª 13:30 as 15:00</option>
                <option value="2">2ª 4ª 6ª 15:00 as 16:00</option>
                <option value="3">3ª 5ª 13:00 as 15:00</option>
                <option value="4">3ª 5ª 15:00 as 17:00</option>
            </select>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label for="deficiencia">POSSUI DEFICIÊNCIA</label>
            <select name="deficiencia" id="deficiencia" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['deficiencia'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['deficiencia'];
                } ?>">
                    <?php
                    if ($pessoa['deficiencia'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['deficiencia'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>

        <div class="col-md-6">
            <label for="deficiencia_desc">DESCRIÇÃO DA DEFICIÊNCIA</label>
            <input autocomplete="off" id="deficiencia_desc" type="text" class="form-control" name="deficiencia_desc" value="<?php echo $pessoa['deficiencia_desc']; ?>"/>
        </div>
    </div>
    </form>
</main>
