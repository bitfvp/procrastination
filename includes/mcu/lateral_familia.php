<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#serch1 input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings(".result");
            if (inputVal.length) {
                $.get("<?php echo $env->env_url;?>includes/mcu/pessoa_add_cf.php", {term: inputVal,cf: "<?php echo $pessoa['cod_familiar'];?>", id: "<?php echo $pessoa['id'];?>", pg: "<?php echo $_GET['pg'];?>"} ).done(function (data) {
                    // Display the returned data in browser
                    resultDropdown.html(data);
                });
            } else {
                resultDropdown.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".result p", function () {
            $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
            $(this).parent(".result").empty();
        });
    });
</script>
<!--popover em hover-->
<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>
<section class="sidebar-offcanvas" id="sidebarf">
    <?php
    $pestemp=$pessoa['cod_familiar'];
    if(isset($pestemp) and $pestemp!=null and $pestemp!=0) {
        //existe um id e se ele é numérico
            $sql = "SELECT * FROM mcu_pessoas WHERE cod_familiar=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $pestemp);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $cfpessoa = $consulta->fetchall();
            $sql=null;
            $consulta=null;
    ?>
    <div class="list-group" id="serch1">
        <h3>
            <a href="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>">Família</a>

            <?php
            if (isset($protecaobasica) and $protecaobasica==1){ ?>
            <a href="index.php?pg=Vfamilia&id=<?php echo $_GET['id'];?>" class="float-right fas fa-list m-1"></a>
            <i class="float-right fas fa-question m-1" href="#" class="" data-toggle="modal" data-target="#modalavaliacao"></i>

                <div class="modal show bd-example-modal-lg" id="modalavaliacao" tabindex="-1" role="dialog" aria-labelledby="modalavaliacao" aria-hidden="true">
                    <div class="modal-dialog modal-lg bg-dark" role="document">
                        <div class="modal-content">
                            <div class="modal-header pl-5">
                                <h4 class="modal-title">
                                    <i class="fas fa-users"></i> Avaliação da familia</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>


                            <style type="text/css">
                                input[type=radio]{
                                    transform:scale(1.5);
                                }

                                .form-check label:last-child {
                                    transform:scale(1.0);
                                }
                            </style>
                            <div id="" class="modal-body pl-5">
                                <H5>
                                    <form action="index.php?pg=<?php echo $_GET['pg'];?>&id=<?php echo $_GET['id'];?>&cf=<?php echo $pessoa['cod_familiar'];?>&aca=newavaliacao" method="post">

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="aa" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="aa" class="mb-1">ACOMPANHADA POR MIM</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="a1" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="a1" class="mb-1">ACOMPANHADA PELO PAIF</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="a2" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="a2" class="mb-1">NOVA FAMÍLIA ACOMPANHADA</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b1" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b1" class="mb-1">FAMÍLIA EM SITUAÇÃO DE EXTREMA POBREZA</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b2" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b2" class="mb-1">FAMÍLIA BENEFICIÁRIA NO PROGRAMA BOLSA FAMÍLIA</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b3" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b3" class="mb-1">BOLSA FAMÍLIA EM DESCUMPRIMENTO DE CONDICIONALIDADES</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b4" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b4" class="mb-1">MEMBROS BENEFICIÁRIOS DO BPC</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b5" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b5" class="mb-1">CRIANÇA EM SITUAÇÃO DE TRABALHO INFANTIL</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2 mb-1">
                                                <input name="b6" type="checkbox" data-toggle="toggle" data-on="<i class='far fa-thumbs-up'></i>SIM" data-off="<i class='far fa-thumbs-down'></i>NÃO" data-onstyle="success" data-offstyle="info" data-width="100%">
                                            </div>
                                            <div class="col-md-10 mb-1 pl-0">
                                                <label for="b6" class="mb-1">CRIANÇAS EM SERVIÇO DE ACOLHIMENTO</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 mb-1">
                                                <input type="submit" class="btn btn-block btn-dark" value="SALVAR">
                                            </div>
                                        </div>
                                    </form>
                                </H5>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    // $('#modalavaliacao').modal('toggle');
                </script>


            <?php } ?>

        </h3>
        <?php
        foreach ($cfpessoa as $cfp){

            echo "<div class='list-group-item'>";
            $nome = explode(" ", $cfp['nome']);

//            echo "<a href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}' class='badge badge-danger float-right'><span class='fa fa-times'></span></a>";

            echo "<div class='dropdown show float-right dropleft'>"
                ."<a class='badge badge-danger btn-sm' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"
                ."<span class='fa fa-times'></span>"
                ."</a>"
                ."<div class='dropdown-menu left' aria-labelledby='dropdownMenuLink'>"
                ."<a class='dropdown-item' href='#'>Manter como está</a>"
                ."<a class='dropdown-item bg-danger' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}'>Remover da família</a>"
                ."</div>"
                ."</div>";

            echo "<a href='index.php?pg=Vpessoa&id={$cfp['id']}' "
                ."class='' "
                ."data-toggle='popover' "
                ."data-placement='top' "
                ."title='{$cfp['nome']}' "
                ."data-html='true' "
                ."data-trigger='hover' "
                ."data-content='"
                .dataBanco2data ($cfp['nascimento']). " idade:" .Calculo_Idade($cfp['nascimento'])." ano(s)<br>"
                ."CPF:".$cfp['cpf']."<br>"
                ."RG:".$cfp['rg']."<br>"
                ."'>";

            echo "    {$nome[0]} </a>";

            include_once("includes/possui_registro.php");
            fncpossui_registro($cfp['id']);

            echo "</div>";
        }
        ?>
        <input type="text" class="form-control input-sm" id="admembro" placeholder="Adicionar membro da família">
        <div class="result"></div>
    </div>
    <?php
    }else{
        echo "<a href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=novocf' class='btn btn-warning btn-block btn-lg mt-3'>Gerar família</a>";
    }

    ?>
</section>