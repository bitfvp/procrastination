<!--chama contatos online-->
<script type="text/javascript">
    $.ajaxSetup({cache: false});
    $(document).ready(function () {
        $('#online').load('<?php echo $env->env_url;?>includes/caf/online.php');
    });
    $(document).ready(function () {
        setInterval(function () {
            $('#online').load('<?php echo $env->env_url;?>includes/caf/online.php')
        }, 50000);
    });
</script>
<div id="usericone" class="fa fa-comment fa-2x text-success"></div>
<div id="online"></div>

<script>
    $('#usericone').on("click", function () {
        $('#online').show("slow");
        $('#usericone').hide("slow");
        var fechadialog = setTimeout(function () {
            $('#online').hide("slow");
            $('#usericone').show("slow");
        }, 15000);
    });

    $('#online').on("click", function () {
        $('#online').hide();
        $('#usericone').show("slow");
    });
</script>

<!--chama msg recebida-->
<script type="text/javascript">
    $.ajaxSetup({cache: false});
    $(document).ready(function () {
        $('#mmsgs').load('<?php echo $env->env_url;?>includes/caf/alerta_msgs.php');
    });
    $(document).ready(function () {
        setInterval(function () {
            $('#mmsgs').load('<?php echo $env->env_url;?>includes/caf/alerta_msgs.php')
        }, 50000);
    });
</script>