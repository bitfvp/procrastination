<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
    <div class="row">
            <div class="col-md-6">
                <?php
                if ($allow["admin"]==1){ ?>
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
                    <?php
                }
                ?>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
            <label for="nome">NOME</label><input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
        </div>
        <div class="col-md-3">
            <label for="nome_social">NOME SOCIAL</label>
            <input autocomplete="off" id="nome_social" type="text" class="form-control" name="nome_social" value="<?php echo $pessoa['nome_social']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="sexo">SEXO</label>
            <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['sexo'];
                } ?>">
                    <?php
                    if ($pessoa['sexo'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['sexo'] == 1) {
                        echo "Feminino";
                    }
                    if ($pessoa['sexo'] == 2) {
                        echo "Masculino";
                    }
                    if ($pessoa['sexo'] == 3) {
                        echo "Indefinido";
                    }
                    ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Feminino</option>
                <option value="2">Masculino</option>
                <option value="3">Indefinido</option>
            </select>

        </div>
        <div class="col-md-3">
            <label for="nascimento">NASCIMENTO</label>
            <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label for="cpf">CPF</label>
            <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#cpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
        </div>
        <div class="col-md-5">
            <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                             value="<?php echo $pessoa['rg']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#rg').mask('00.000.000.000', {reverse: true});
                });
            </script>
        </div>
        <div class="col-md-2">
            <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                   value="<?php echo $pessoa['uf_rg']; ?>"/>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <label for="cn">CERTIDÃO DE NASCIMENTO</label>
            <input autocomplete="off" id="cn" type="text" class="form-control" name="cn" value="<?php echo $pessoa['cn']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="ctps">CARTEIRA DE TRABALHO</label>
            <input autocomplete="off" id="ctps" type="text" class="form-control" name="ctps" value="<?php echo $pessoa['ctps']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="cod_familiar">CÓDIGO FAMILIAR</label>
            <input autocomplete="off" id="cod_familiar" type="text" class="form-control" name="cod_familiar" value="<?php echo $pessoa['cod_familiar']; ?>"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for="pai">PAI</label>
            <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $pessoa['pai']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="mae">MÃE</label>
            <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $pessoa['mae']; ?>"/>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-5">
            <label for="endereco">ENDEREÇO</label>
            <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="numero">NÚMERO</label>
            <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="bairro">BAIRRO</label>
            <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php
                $bairroid = $pessoa['bairro'];
                $getbairro=fncgetbairro($bairroid);
                ?>
                <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $pessoa['bairro']; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                foreach (fncbairrolist() as $item) {
                    ?>
                <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                    <?php echo $item['bairro']; ?>
                </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-md-1">
            <?php
            if ($allow["allow_5"]==1){
            $tempcaminho = $env->env_url;
            switch ($_SESSION['matriz']){
                case 1:
                    $tempcaminho.= "mcu/?pg=Vbl";
                    break;
                case 2;
                    $tempcaminho.= "caf/?pg=Vbl";
                    break;
            }
            ?>
            <label for="cad"><h6>CAD. BAIRRO</h6><a href="<?php echo $tempcaminho;?>" target="_blank"><i class="fa fa-plus"></i></a></label>
            <?php }?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <label for="referencia">REFERÊNCIA</label>
            <input autocomplete="off" id="referencia" type="text" class="form-control" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
        </div>
        <div class="col-md-5">
            <label for="telefone">TELEFONE</label>
            <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                });
            </script>
        </div>
    </div>
        <hr>
    <div class="row">
        <div class="col-md-4">
            <label for="escolaridade">ESCOLARIDADE</label>
            <select name="escolaridade" id="escolaridade" class="form-control">// vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['escolaridade'] == "") {
                                                    $z = 0;
                                                    echo $z;
                                                } else {
                                                    echo $pessoa['escolaridade'];
                                                } ?>">
                    <?php
                    switch ($pessoa['escolaridade']){
                        case 0:
                            echo "indefinido";
                            break;
                        case 1:
                            echo "Não alfabetizado";
                            break;
                        case 2:
                            echo "fundamental";
                            break;
                        case 3:
                            echo "medio";
                            break;
                    }
                    ?>
                </option>
                <option value="0">indefinido</option>
                <option value="1">Não alfabetizado</option>
                <option value="2">fundamental</option>
                <option value="3">medio</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="residencia">TIPO DE REDIDÊNCIA</label>
            <select name="residencia" id="residencia" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if (isset($pessoa['residencia']) and $pessoa['residencia']>0 and $pessoa['residencia']<3){echo $pessoa['residencia'];}else{echo 0;} ?>">
                    <?php
                    if (isset($pessoa['residencia'])){
                        if ($pessoa['residencia']==0){
                            echo "Selecione";
                        }
                        if ($pessoa['residencia']==1){
                            echo "Própria";
                        }
                        if ($pessoa['residencia']==2){
                            echo "Alugada";
                        }
                    }else{
                        echo "Selecione";
                    }
                    ?>
                </option>
                <option value="1">Própria </option>
                <option value="2">Alugada</option>
            </select>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label for="deficiencia">POSSUI DEFICIÊNCIA</label>
            <select name="deficiencia" id="deficiencia" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['deficiencia'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['deficiencia'];
                } ?>">
                    <?php
                    if ($pessoa['deficiencia'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['deficiencia'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>

        <div class="col-md-6">
            <label for="deficiencia_desc">DESCRIÇÃO DA DEFICIÊNCIA</label>
            <input autocomplete="off" id="deficiencia_desc" type="text" class="form-control" name="deficiencia_desc" value="<?php echo $pessoa['deficiencia_desc']; ?>"/>
        </div>
    </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <label for="ser_quando_crescer">ser_quando_crescer</label>
                <input autocomplete="off" id="ser_quando_crescer" type="text" class="form-control" name="ser_quando_crescer" value="<?php echo $pessoa['ser_quando_crescer']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="sonho">sonho</label>
                <input autocomplete="off" id="sonho" type="text" class="form-control" name="sonho" value="<?php echo $pessoa['sonho']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="cor">cor</label>
                <input autocomplete="off" id="cor" type="text" class="form-control" name="cor" value="<?php echo $pessoa['cor']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="musica">musica</label>
                <input autocomplete="off" id="musica" type="text" class="form-control" name="musica" value="<?php echo $pessoa['musica']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="brincadeira">brincadeira</label>
                <input autocomplete="off" id="brincadeira" type="text" class="form-control" name="brincadeira" value="<?php echo $pessoa['brincadeira']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="personagem">personagem</label>
                <input autocomplete="off" id="personagem" type="text" class="form-control" name="personagem" value="<?php echo $pessoa['personagem']; ?>"/>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label for="apadrinhamento">apadrinhamento</label>
                <select name="apadrinhamento" id="apadrinhamento" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($pessoa['apadrinhamento'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['apadrinhamento'];
                    } ?>">
                        <?php
                        switch ($pessoa['apadrinhamento']){
                            case 0:
                                echo "Não disponivel";
                                break;
                            case 1:
                                echo "disponivel";
                                break;
                            case 2:
                                echo "apadrinhada";
                                break;
                        }
                        ?>
                    </option>
                    <option value="0">Não disponivel</option>
                    <option value="1">disponivel</option>
                    <option value="2">apadrinhada</option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="uso_de_imagem">uso_de_imagem</label>
                <select name="uso_de_imagem" id="uso_de_imagem" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($pessoa['uso_de_imagem'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['uso_de_imagem'];
                    } ?>">
                        <?php
                        if ($pessoa['uso_de_imagem'] == 0) {
                            echo "Não";
                        }
                        if ($pessoa['uso_de_imagem'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>

            </div>
        </div>

    </form>
</main>
