<script type="text/javascript">
    $(document).ready(function () {
        $('#serch1 input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings(".result");
            if (inputVal.length) {
                $.get("<?php echo $env->env_url;?>includes/caf/pessoa_add_cf.php", {term: inputVal,cf: "<?php echo $pessoa['cod_familiar'];?>", id: "<?php echo $pessoa['id'];?>", pg: "<?php echo $_GET['pg'];?>"} ).done(function (data) {
                    // Display the returned data in browser
                    resultDropdown.html(data);
                });
            } else {
                resultDropdown.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".result p", function () {
            $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
            $(this).parent(".result").empty();
        });
    });
</script>
<section class="sidebar-offcanvas" id="sidebarf">
    <?php
    $pestemp=$pessoa['cod_familiar'];
    if(isset($pestemp) and $pestemp!=null and $pestemp!=0) {
        //existe um id e se ele é numérico
            $sql = "SELECT * FROM caf_pessoas WHERE cod_familiar=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $pestemp);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $cfpessoa = $consulta->fetchall();
            $sql=null;
            $consulta=null;
    ?>
    <div class="list-group" id="serch1">
        <h3><a href="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>">Família</a></h3>
        <?php
        foreach ($cfpessoa as $cfp){

            echo "<div class='list-group-item'>";
            include_once("includes/possui_registro.php");
            fncpossui_registro($cfp['id']);
            echo "<a href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}' class='badge badge-danger float-right'><span class='fa fa-times'></span></a>";
            echo "<a href='index.php?pg=Vpessoa&id={$cfp['id']}'>    {$cfp['nome']}</a>";
            echo "</div>";
        }
        ?>
        <input type="text" class="form-control input-sm" id="admembro" placeholder="Adicionar membro da família">
        <div class="result"></div>
    </div>
    <?php
    }else{
        echo "<a href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=novocf' class='btn btn-warning btn-block btn-lg mt-3'>Gerar família</a>";
    }

    ?>
</section>