<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />

            <div class="dropdown dropdown-lg">
                <button class="btn btn-outline-info btn-lg" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-plus"></i></button>

                <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <div class="form-group">
                        <label for="contain">CPF</label>
                        <input type="search" autocomplete="off" class="form-control" placeholder="Buscar por CPF..." name="scb" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="contain">RG</label>
                        <input type="search" autocomplete="off" class="form-control" placeholder="Buscar por RG..." name="scc" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>"/>
                    </div>
                </div>
            </div>

        </div>
    </form>
        <?php
        if ($allow["admin"]==1){ ?>
            <a href="index.php?pg=Vpessoaeditar" class="btn btn-info btn-lg btn-block col-md-6 float-right">
                NOVO CADASTRO
            </a>
            <?php
        }
        ?>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
</div>