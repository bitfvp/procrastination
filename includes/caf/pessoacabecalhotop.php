<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
  <h4 class="ml-3">DADOS DO USUÁRIO</h4>
  <blockquote class="blockquote blockquote-info">
  <header>
      NOME:
      <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
  </header>
      <h6>
          NOME SOCIAL:
          <strong class="text-info"><?php echo $pessoa['nome_social']; ?>&nbsp;&nbsp;</strong>
          SEXO:
          <?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?>
          NASCIMENTO:
          <strong class="text-info"><?php
              if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['nascimento']);
                  echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
          <hr>
        
          CPF:
            <strong class="text-info"><?php
            if($pessoa['cpf']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['cpf'],'###.###.###-##');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>
          RG:
            <strong class="text-info"><?php
            if($pessoa['rg']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['rg'],'###.###.###');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>
          UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong>

          MÃE:
          <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;
          </strong>

          PAI:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>
          <hr>

          ENDEREÇO:
          <strong class="text-info"><?php
            if($pessoa['endereco']!=""){
                echo "<span class='azul'>";
                echo $pessoa['endereco'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>&nbsp;&nbsp;
            
          NÚMERO:
            <strong class="text-info"><?php
                if ($pessoa['numero']==0){
                    echo "<span class='text-info'>";
                    echo "s/n";
                    echo "</span>";
                }else{
                    echo "<span class='text-info'>";
                    echo $pessoa['numero'];
                    echo "</span>";
                }
            ?></strong>&nbsp;&nbsp;
        
          BAIRRO:
          <strong class="text-info"><?php
            if ($pessoa['bairro'] != "0") {
                $cadbairro=fncgetbairro($pessoa['bairro']);
                echo $cadbairro['bairro'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>
          </strong>&nbsp;&nbsp;

          REFERÊNCIA:
            <strong class="text-info"><?php
            if($pessoa['referencia']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['referencia'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>&nbsp;
        
          TELEFONE:
            <strong class="text-info"><?php
            if($pessoa['telefone']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['telefone'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>
          <hr>
          SITUAÇÃO DA RESIDÊNCIA:
            <strong class="text-info"><?php
                if($pessoa['residencia']==0){echo"Selecione...";}
                if($pessoa['residencia']==1){echo"Própria";}
                if($pessoa['residencia']==2){echo"Alugada";} ?>&nbsp;&nbsp;
            </strong>

          ESCOLARIDADE:
          <strong class="text-info"><?php
              switch ($pessoa['escolaridade']){
                  case 0:
                      echo "indefinido";
                      break;
                  case 1:
                      echo "Não alfabetizado";
                      break;
                  case 2:
                      echo "fundamental";
                      break;
                  case 3:
                      echo "medio";
                      break;
              }
              ?>&nbsp;&nbsp;</strong>

          POSSUI DEFICIÊNCIA:
          <strong class="text-info"><?php
              if($pessoa['deficiencia']==0){echo"Não";}
              if($pessoa['deficiencia']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

          DESCRIÇÃO DA DEFICIÊNCIA:
          <strong class="text-info"><?php
              if($pessoa['deficiencia_desc']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['deficiencia_desc'];
                  echo "</span>";
              }else{
              }
              ?></strong>
          <hr>
          ser_quando_crescer:<strong class="text-info"><?php echo $pessoa['ser_quando_crescer']; ?>&nbsp;&nbsp;</strong>
          sonho:<strong class="text-info"><?php echo $pessoa['sonho']; ?>&nbsp;&nbsp;</strong>
          cor:<strong class="text-info"><?php echo $pessoa['cor']; ?>&nbsp;&nbsp;</strong>
          musica:<strong class="text-info"><?php echo $pessoa['musica']; ?>&nbsp;&nbsp;</strong>
          brincadeira:<strong class="text-info"><?php echo $pessoa['brincadeira']; ?>&nbsp;&nbsp;</strong>
          personagem:<strong class="text-info"><?php echo $pessoa['personagem']; ?>&nbsp;&nbsp;</strong>

          apadrinhamento:
          <strong class="text-info"><?php
              if($pessoa['apadrinhamento']==0){echo"indisponivel";}
              if($pessoa['apadrinhamento']==1){echo"disponivel";}
              if($pessoa['apadrinhamento']==2){echo"apadrinhada";}?>&nbsp;&nbsp;</strong>

          uso_de_imagem:
          <strong class="text-info"><?php
              if($pessoa['uso_de_imagem']==0){echo"Não";}
              if($pessoa['uso_de_imagem']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>



    </h6>
    <footer class="blockquote-footer">
            Mantenha atualizado</strong>&nbsp;&nbsp;
    </footer>
  </blockquote>
</div>
<?php
if (!$pessoa['status']){
?>
    <a href='index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>&aca=pessoaativar' class='btn btn-outline-primary btn-block'>Habilitar para oficinas</a>
    <?php
}
?>

<?php if ($allow["admin"]==1){ ?>
        <a class="btn btn-success btn-block" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR PESSOA
        </a>
    <?php }?>
