<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-danger text-light">
                    30 Usuários mais ativos
                </div>
                <div class="card-body">
                    <table class="table table-hover table-sm">
                        <tbody>
                        <?php
                        $ccont=0;
                        foreach (fnc_top_pessoa() as $rk){
                            $ccont+=1;

                            if($ccont==1){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/gold.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont==2){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/silver.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont==3){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/bronze.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont>3){
                                echo "<tr>";
                                echo "<td  class='text-center'>";
                                echo $ccont;
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- /..col-md-5 -->

        <div class="col-md-7">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Informações
                </div>
                <div class="card-body">
                    <?php
                    $sql = "SELECT `id` FROM `caf_pessoas` WHERE `cont_rank`<>0 ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $quantcadastros = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    $sql = "SELECT `id` FROM `caf_beneficio` ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $quantben = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    $sql = "SELECT `id` FROM `caf_at` ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $quant_at = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    $sql = "SELECT `id` FROM `tbl_token` ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $quantacessos = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    $sql = "SELECT `id` FROM `tbl_token` WHERE user={$_SESSION['id']}";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $quantmeusacessos = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    ?>
                    <table class="table table-hover table-sm">
                        <tr>
                            </i>
                            <td>Cadastros ativos <i class="small text-info">(após 22/08/2018)</i></td>
                            <td><?php echo $quantcadastros;?></td>
                        </tr>
                        <tr>
                            <td>Benefícios <i class="small text-info">(após 22/08/2016)</i></td>
                            <td><?php echo $quantben;?></td>
                        </tr>
                        <tr>
                            <td>Atividades com usuário <i class="small text-info">(após 22/08/2016)</i></td>
                            <td><?php echo $quant_at;?></td>
                        </tr>
                        <tr>
                            <td>Acessos ao Syssocial <i class="small text-info">(após 23/07/2018)</i></td>
                            <td><?php echo $quantacessos;?></td>
                        </tr>
                        <tr>
                            <td>Meus acessos <i class="small text-info">(após 23/07/2018)</i></td>
                            <td><?php echo $quantmeusacessos;?></td>
                        </tr>
                    </table>
                </div>
            </div>


            <script type="text/javascript">
                $.ajaxSetup({ cache: false });
                $(document).ready(function() {
                    $('#conteudo').load('../includes/monitorlog.php');
                });
                $(document).ready(function() {
                    setInterval(function () {
                        $('#conteudo').load('../includes/monitorlog.php')
                    }, 30000);
                });
            </script>
            <div class="card mt-2">
                <div class="card-header bg-primary text-light">
                    Últimos acontecimentos
                </div>
                <div id="conteudo" class="card-body">
                    <!-- Conteudo via js-->
                </div>
            </div>

        </div> <!-- /..col-md-7 -->

    </div> <!-- /.row -->


</main><!--fim de conteiner-->