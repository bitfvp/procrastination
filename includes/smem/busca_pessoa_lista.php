<div class="container-fluid">
<table class="table table-striped table-hover table-sm table-responsive-md">
    <thead>
    <tr>
        <th scope="row" colspan="3">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm mb-0">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                    }
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$proximo}'>Próximo →</a></li>";
                    }
                    ?>
                </ul>
            </nav>
        </th>
        <th colspan="3" class="text-info text-right"><?php echo $tr;?> pessoa(s) listada(s)</th>
    </tr>
    </thead>
    <thead class="thead-dark">
    <tr>
        <th scope="col">NOME</th>
        <th scope="col">NASCIMENTO</th>
        <th scope="col">CPF</th>
        <th scope="col"><?php if (isset($_GET['endereco']) and $_GET['endereco']!=""){echo "ENDEREÇO";}else{echo "RG";}?></th>
        <th scope="col">BAIRRO</th>
        <th scope="col">AÇÕES</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th scope="row" colspan="3">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                    }
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$proximo}'>Próximo →</a></li>";
                    }
                    ?>
                </ul>
            </nav>
        </th>
        <th colspan="3" class="text-info text-right"><?php echo $tr;?> pessoa(s) listada(s)</th>
    </tr>
    </tfoot>

    <?php
    if(isset($_GET['sca']) and $_GET['sca']!="") {
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
    }
    if (isset($_GET['nascimento']) and $_GET['nascimento'] != "") {
        $stnascimento = strtoupper($_GET['nascimento']);
        define('CSNASCIMENTO', $stnascimento);//TESTE
    }
    if (isset($_GET['cpf']) and $_GET['cpf'] != "") {
        $stcpf = strtoupper($_GET['cpf']);
        define('CSCPF', $stcpf);//TESTE
    }
    if (isset($_GET['rg']) and $_GET['rg'] != "") {
        $strg = strtoupper($_GET['rg']);
        define('CSRG', $strg);//TESTE
    }
    if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
        $stendereco = strtoupper($_GET['endereco']);
        define('CSENDERECO', $stendereco);//TESTE
    }
    if (isset($_GET['numero']) and $_GET['numero'] != "") {
        $stnumero = strtoupper($_GET['numero']);
        define('CSNUMERO', $stnumero);//TESTE
    }
    if (isset($_GET['bairro']) and $_GET['bairro'] != "") {
        $stbairro = strtoupper($_GET['bairro']);
        define('CSBAIRRO', $stbairro);//TESTE
    }
    if (isset($_GET['genero']) and $_GET['genero'] != "") {
        $stgenero = strtoupper($_GET['genero']);
        define('CSGENERO', $stgenero);//TESTE
    }

    // vamos criar a visualização
    while ($dados =$limite->fetch()){
    $id = $dados["id"];
    $nome = strtoupper($dados["nome"]);
    $nascimento = dataBanco2data ($dados["nascimento"]);
    $idade = Calculo_Idade($dados["nascimento"]);
    $cpf = $dados["cpf"];
    $rg = $dados["rg"];
    $endereco = $dados["endereco"];
    $numero = $dados["numero"];
    $cod_familiar=$dados["cod_familiar"];
    $bairro = $dados["bairro"];

    ?>
    <tbody>
    <tr>
        <th scope="row" id="<?php echo $id;  ?>">
            <i class="click-to-copy">
                <span class="click-to-copy-text fas fa-hashtag small"><i class="d-none"><?php echo $id; ?></i></span>
                <span class="click-to-copy-label d-none text-warning"></span>
            </i>
            <i class="click-to-copy">
                <span class="click-to-copy-text fas fa-at small"><i class="d-none"><?php echo ucwords(strtolower($nome)); ?></i></span>
                <span class="click-to-copy-label d-none text-warning"></span>
            </i>
            <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                <?php

                if(isset($_GET['sca']) and $_GET['sca']!="") {
                    $sta = CSA;
                    $nnn = $nome;
                    $nn = explode(CSA, $nnn);
                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                    echo $n;
                }else{
                    echo $nome;
                }
                ?>
            </a>
            <?php
            if ($allow["admin"]==1 and $env->env_mod_nome==" Proteção Basica "){
                echo "<a href='index.php?pg=Vbe&id={$id}' class='fas fa-hands-helping float-right text-success'></a> ";
                echo "<a href='index.php?pg=Vat&id={$id}' class='fas fa-quote-left float-right text-success'></a>";
            }
            ?>
        </th>
        <td style="white-space: nowrap;">
            <?php
            if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                echo $nascimento. "<span class='badge badge-info ml-1 text-center' title='idade'>" . $idade. "</span>";
            }else{
                echo "<span class='text-danger'>--/--/----</span>";
            }
            ?>
        </td>
        <td  style="white-space: nowrap;">
            <?php
            if($cpf!="0" and $cpf!="") {
                if (isset($_GET['cpf']) and $_GET['cpf'] != "") {
                    $stcpf = CSCPF;
                    $ccc = $cpf;
                    $cc = explode(CSCPF, $ccc);
                    $c = implode("<span class='text-danger'>{$stcpf}</span>", $cc);
                    echo $c;
                } else {
                    echo mask($cpf,'###.###.###-##');
                }
            }else{
                echo "----";
            }
            ?></td>
        <td>
            <?php if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
                if($endereco!="0" and $endereco!="") {
                    if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
                        $stc = CSENDERECO;
                        $rrr = $endereco;
                        $rr = explode(CSENDERECO, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $endereco;
                    }
                }else{
                    echo "<i>-- </i>";
                }
                echo "/";
                if($numero!="0" and $numero!="") {
                    if (isset($_GET['numero']) and $_GET['numero'] != "") {
                        $stc = CSNUMERO;
                        $rrr = $numero;
                        $rr = explode(CSNUMERO, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $numero;
                    }
                }else{
                    echo "<i>s/n </i>";
                }
            } else {
                if($rg!="0" and $rg!="") {
                    if (isset($_GET['rg']) and $_GET['rg'] != "") {
                        $stc = CSRG;
                        $rrr = $rg;
                        $rr = explode(CSRG, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $rg;
                    }
                }else{
                    echo "---";
                }
            }

            ?></td>
        <td class="small">
            <?php
            if ($bairro != "0") {
                $cadbairro=fncgetbairro($bairro);
                echo $cadbairro['bairro'];
            } else {
                echo "<span class='text-warning'>-----</span>";
            }
            ?>
        </td>
        <td>
            <?php
            if ($allow["allow_4"]==1){ ?>
                <div class="row">
                    <div class="col-6 p-1">
                        <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa" class="btn btn-sm btn-outline-primary fa fa-pen float-right"></a>
                    </div>
                    <div class="col-6 p-1">

                        <?php
                        try {
                            $sql = "select count(mcu_pessoas_duplicadas.id) from mcu_pessoas_duplicadas where pessoa=? and verificada=0 ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1, $id);
                            $consulta->execute();
                        } catch (PDOException $error_msg) {
                            echo 'Erroff' . $error_msg->getMessage();
                        }
                        $valor = $consulta->fetch();
                        if ($valor[0] != 0) {
                            echo "<h6 class='text-danger fas fa-clone small'>Em análise</h6>";
                        }else{
                            echo "<a href='index.php?pg=Vbusca&id=".$id."&aca=nova_duplicata&sca=".$_GET['sca']."&scb=".$_GET['scb']."&scc=".$_GET['scc']."' class='btn btn-sm btn-outline-success fas fa-clone text-success float-left' title='Denunciar cadastro duplicado'></a>";
                        }
                        ?>
                    </div>
                </div>





                <?php
            }else{
                echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
            }
            ?>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>
</div>
<script type="application/javascript">
    function copyToClipboard(element) {
        const tempElement = document.createElement("input");
        document.body.appendChild(tempElement);
        if (element.textContent) {
            tempElement.value = element.textContent;
            tempElement.select();
            document.execCommand("copy");
            tempElement.remove();
        }
    }

    const clickToCopyElements = document.getElementsByClassName("click-to-copy");

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < clickToCopyElements.length; i++) {
        const clickToCopyElement = clickToCopyElements[i];
        const textElement = clickToCopyElement.querySelector(".click-to-copy-text");
        const copyLabel = clickToCopyElement.querySelector(".click-to-copy-label");
        if (textElement && copyLabel) {
            textElement.addEventListener("mouseover", () => {
                copyLabel.classList.remove("d-none");
            });
            textElement.addEventListener("mouseout", () => {
                copyLabel.classList.add("d-none");
            });
            textElement.addEventListener("click", () => {
                copyToClipboard(textElement);
                copyLabel.textContent = "Copiado";
                //copyLabel.classList.add("d-none");
                setTimeout(() => copyLabel.textContent = "", 850);
            });
        }
    }
</script>