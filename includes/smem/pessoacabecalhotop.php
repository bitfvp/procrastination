<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
  <h3 class="ml-3">DADOS DO USUÁRIO</h3>
  <blockquote class="blockquote blockquote-info">
  <header>
      NOME:
      <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
  </header>
      <h6>
          NOME SOCIAL:
          <strong class="text-info"><?php echo $pessoa['nome_social']; ?>&nbsp;&nbsp;</strong>
          SEXO:
          <strong class="text-info"><?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?></strong>
          NASCIMENTO:
          <strong class="text-info"><?php
              if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['nascimento']);
                  echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
          <hr>
        
          CPF:
            <strong class="text-info"><?php
            if($pessoa['cpf']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['cpf'],'###.###.###-##');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>
          RG:
            <strong class="text-info"><?php
            if($pessoa['rg']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['rg'],'###.###.###');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>
          UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong>

          NIS:
          <strong class="text-info"><?php
              if($pessoa['nis']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['nis'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          MÃE:
          <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;
          </strong>

          PAI:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>
          <hr>

          ENDEREÇO:
          <strong class="text-info"><?php
            if($pessoa['endereco']!=""){
                echo "<span class='azul'>";
                echo $pessoa['endereco'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>&nbsp;&nbsp;
            
          NÚMERO:
            <strong class="text-info"><?php
                if ($pessoa['numero']==0){
                    echo "<span class='text-info'>";
                    echo "s/n";
                    echo "</span>";
                }else{
                    echo "<span class='text-info'>";
                    echo $pessoa['numero'];
                    echo "</span>";
                }
            ?></strong>&nbsp;&nbsp;
        
          BAIRRO:
          <strong class="text-info"><?php
            if ($pessoa['bairro'] != "0") {
                $cadbairro=fncgetbairro($pessoa['bairro']);
                echo $cadbairro['bairro'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>
          </strong>&nbsp;&nbsp;

          REFERÊNCIA:
            <strong class="text-info"><?php
            if($pessoa['referencia']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['referencia'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>&nbsp;
        
          TELEFONE:
            <strong class="text-info"><?php
            if($pessoa['telefone']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['telefone'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?></strong>

          AGENTE SAÚDE:
          <strong class="text-info"><?php
              if($pessoa['agente_saude']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['agente_saude'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>
          <hr>
          PARENTESCO:
            <strong class="text-info"><?php
                if(($pessoa['parentesco']==0)or($pessoa['parentesco']=="")){echo"Selecione...";}
                if($pessoa['parentesco']==1){echo"Pessoa Responsavel pela Unidade Familiar";}
                if($pessoa['parentesco']==2){echo"Conjuge ou companheiro(a)";}
                if($pessoa['parentesco']==3){echo"Filho(a)";}
                if($pessoa['parentesco']==4){echo"Enteado(a)";}
                if($pessoa['parentesco']==5){echo"Neto(a) ou bisneto(a)";}
                if($pessoa['parentesco']==6){echo"Pai ou mae";}
                if($pessoa['parentesco']==7){echo"Sogro(a)";}
                if($pessoa['parentesco']==8){echo"Irmao ou irma";}
                if($pessoa['parentesco']==9){echo"Genro ou nora";}
                if($pessoa['parentesco']==10){echo"Outro";}
                ?>&nbsp;&nbsp;
            </strong>
          RAÇA/COR:
            <strong class="text-info"><?php
                if(($pessoa['raca_cor']==0)or($pessoa['raca_cor']=="")){echo"Selecione...";}
                if($pessoa['raca_cor']==1){echo"Branca";}
                if($pessoa['raca_cor']==2){echo"Negra";}
                if($pessoa['raca_cor']==3){echo"Amarela";}
                if($pessoa['raca_cor']==4){echo"Parda";}
                if($pessoa['raca_cor']==5){echo"Indigena";} ?>&nbsp;&nbsp;
            </strong>
    
          POSSUI VEICULO:
            <strong class="text-info"><?php
                if($pessoa['possui_veiculo']==0){echo"Não";}
                if($pessoa['possui_veiculo']==1){echo"Sim";} ?>&nbsp;&nbsp;
            </strong>

          PAGA ALUGUEL:
            <strong class="text-info"><?php
                if($pessoa['paga_aluguel']==0){echo"Não";}
                if($pessoa['paga_aluguel']==1){echo"Sim";} ?>&nbsp;&nbsp;
            </strong>

          POSSUI PROPRIEDADE:
            <strong class="text-info"><?php
                if($pessoa['possui_propriedade']==0){echo"Não";}
                if($pessoa['possui_propriedade']==1){echo"Sim";} ?>&nbsp;&nbsp;
            </strong>

          ALFABETIZADO:
          <strong class="text-info"><?php
              if($pessoa['alfabetizado']==0){echo"Não";}
              if($pessoa['alfabetizado']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

          RENDA MENSAL:<strong class="text-info"><?php echo"R$ "; if($pessoa['renda']==""){$z=0; echo $z;}else{ echo $pessoa['renda'];} ?>&nbsp;&nbsp;</strong>


          POSSUI DEFICIÊNCIA:
          <strong class="text-info"><?php
              if($pessoa['deficiencia']==0){echo"Não";}
              if($pessoa['deficiencia']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

          DESCRIÇÃO DA DEFICIÊNCIA:
          <strong class="text-info"><?php
              if($pessoa['deficiencia_desc']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['deficiencia_desc'];
                  echo "</span>";
              }else{
              }
              ?></strong>
          <hr />
          <?php
          function onde_passou($pessoa){
              $result="";


              //geral
              try {
                  $sql = "select count(smem_at.id) from smem_at where pessoa=? and tipo=1 ";
                  global $pdo;
                  $consulta = $pdo->prepare($sql);
                  $consulta->bindParam(1, $pessoa);
                  $consulta->execute();
              } catch (PDOException $error_msg) {
                  echo 'Erroff' . $error_msg->getMessage();
              }
              $valor = $consulta->fetch();
              if ($valor[0] != 0) {
                  $result .= "<i class='badge badge-info badge-pill fas fa-quote-left'> Geral</i> ";
              }


              return $result;
          }
          echo onde_passou($_GET['id']);
          ?>
    </h6>
    <footer class="blockquote-footer">
            Mantenha atualizado</strong>&nbsp;&nbsp;
    </footer>

        <!--      campanha pra denunciar duplicatas-->
        <!--      /////////////////////////-->
        <?php
        try {
            $sql = "select count(mcu_pessoas_duplicadas.id) from mcu_pessoas_duplicadas where pessoa=? and verificada=0 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
        } catch (PDOException $error_msg) {
            echo 'Erroff' . $error_msg->getMessage();
        }
        $valor = $consulta->fetch();
        if ($valor[0] != 0) {
            echo "<h6 class='text-danger fas fa-clone'>Esse cadastro já foi denunciado como duplicata e está em análise</h6>";
        }else{
            echo "<a href='index.php?pg=Vpessoa&id=".$_GET['id']."&aca=nova_duplicata' class='btn btn-sm btn-outline-info fas fa-clone'>Denunciar cadastro duplicado</a>";
        }
        ?>

      <!--      /////////////////////////-->
      <!--      campanha pra denunciar duplicatas-->

  </blockquote>
</div>

<?php if ($allow["allow_4"]==1){ ?>
        <a class="btn btn-success btn-block" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR PESSOA
        </a>
    <?php }?>
