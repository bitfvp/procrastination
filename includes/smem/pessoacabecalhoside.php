<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "erro";
}
?>
<div class="container-fluid">
  <h5 class="ml-3">DADOS DO USUÁRIO</h5>
    <blockquote class="blockquote blockquote-info">
    <?php if ($allow["allow_4"]==1){ ?>
          <a class="btn btn-success btn-block mb-2" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR PESSOA
        </a>
    <?php }?>
        <h5>NOME:
            <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong></h5>
        <hr>
        <h5>NOME SOCIAL:
            <strong class="text-info"><?php echo $pessoa['nome_social']; ?>&nbsp;&nbsp;</strong></h5>
        <h5>SEXO:
            <strong class="text-info"><?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?></strong>
        </h5>
        <h5>NASCIMENTO:
          <strong class="text-info"><?php
              if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['nascimento']);
                  echo "<br><i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
        </h5>
        <h5>CPF:
            <strong class="text-info"><?php
            if($pessoa['cpf']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['cpf'],'###.###.###-##');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
            </h5>
        <h5>RG:
            <strong class="text-info"><?php
            if($pessoa['rg']!="") {
                echo "<span class='text-info'>";
                echo mask($pessoa['rg'],'###.###.###');
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
        </h5>
        <h5>UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong></h5>
        <h5>NIS:
          <strong class="text-info"><?php
              if($pessoa['nis']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['nis'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
        </h5>
        <h5>MÃE:
          <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;
          </strong>
        </h5>
        <h5>PAI:
            <strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong></h5>
        <hr>
        <h5>ENDEREÇO:
        <strong class="text-info"><?php
            if($pessoa['endereco']!=""){
                echo "<span class='text-info'>";
                echo $pessoa['endereco'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
        ?>&nbsp;&nbsp;</strong>
        </h5>
        <h5>NÚMERO:
            <strong class="text-info"><?php
                if ($pessoa['numero']==0){
                    echo "<span class='text-info'>";
                    echo "s/n";
                    echo "</span>";
                }else{
                    echo "<span class='text-info'>";
                    echo $pessoa['numero'];
                    echo "</span>";
                }
            ?>&nbsp;&nbsp;</strong>
        </h5>
        <h5>BAIRRO:
            <strong class="text-info"><?php
                if ($pessoa['bairro'] != "0") {
                echo fncgetbairro($pessoa['bairro'])['bairro'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>&nbsp;&nbsp;</strong>
        </h5>
        <h5>REFERÊNCIA:
            <strong class="text-info"><?php
                if($pessoa['referencia']!="") {
                    echo "<span class='text-info'>";
                    echo $pessoa['referencia'];
                    echo "</span>";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>&nbsp;</strong>
        </h5>
        <h5>TELEFONE:
            <strong class="text-info"><?php
            if($pessoa['telefone']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['telefone'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
        </h5>
        <hr>
        <h5>POSSUI DEFICIÊNCIA:

            <strong class="text-info"><?php
            if($pessoa['deficiencia']==0){echo"Não";}
            if($pessoa['deficiencia']==1){echo"Sim";} ?>&nbsp;&nbsp;
            </strong>
        </h5>
        <h5>DESCRIÇÃO DA DEFICIÊNCIA:
            <strong class="text-info"><?php
                if($pessoa['deficiencia_desc']!="") {
                    echo "<span class='text-info'>";
                    echo $pessoa['deficiencia_desc'];
                    echo "</span>";
                } ?>
            </strong>
        </h5>
        <footer class="blockquote-footer">Favor, mantenha atualizado</footer>
    </blockquote>
</div>
