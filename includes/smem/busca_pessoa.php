<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input style="text-transform:lowercase;" type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control " placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <div class="dropdown dropdown-lg">

                <button class="btn btn-outline-info btn-lg" type="button" data-toggle="modal" data-target="#modalserchadvanced" aria-expanded="false">
                    <i class="fa fa-plus"></i>
                </button>

                <!-- Modal modalserchadvanced -->
                <div class="modal fade" id="modalserchadvanced" tabindex="-1" role="dialog" aria-labelledby="modalPontosLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body mb-0">
                                <form action="#">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="row">
                                                    <div class="col-6 mb-3">
                                                        <label for="nascimento">Buscar por data de nascimento</label>
                                                        <input type="date" autocomplete="off" class="form-control" id="nascimento" name="nascimento" placeholder="" value="<?php if (isset($_GET['nascimento'])) {echo $_GET['nascimento'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 mb-3">
                                                        <label for="cpf">Buscar por CPF</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="cpf" name="cpf" placeholder="" value="<?php if (isset($_GET['cpf'])) {echo $_GET['cpf'];} ?>">
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <label for="rg">Buscar por número de identidade</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="rg" name="rg" placeholder="" value="<?php if (isset($_GET['rg'])) {echo $_GET['rg'];} ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-9 mb-3">
                                                        <label for="endereco">Buscar por rua</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="endereco" name="endereco" placeholder="" value="<?php if (isset($_GET['endereco'])) {echo $_GET['endereco'];} ?>">
                                                    </div>
                                                    <div class="col-3 mb-3">
                                                        <label for="numero">Número</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="numero" name="numero" placeholder="" value="<?php if (isset($_GET['numero'])) {echo $_GET['numero'];} ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12 mb-3">
                                                        <label for="bairro">Buscar por bairro</label>
                                                        <select name="bairro" id="bairro" class="form-control">
                                                            <?php
                                                            if (isset($_GET['bairro']) and is_numeric($_GET['bairro'])){
                                                                $getbairro=fncgetbairro($_GET['bairro']);
                                                                echo "<option value='{$getbairro['id']}'>{$getbairro['bairro']}</option>";
                                                            }else{
                                                                echo "<option value='0'></option>\n";
                                                            }

                                                            foreach (fncbairrolist() as $item) {
                                                                echo "<option value='{$item['id']}'>{$item['bairro']}</option>\n";
                                                            }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-3">
                                                <h4 class="mb-3">Gênero</h4>
                                                <div class="d-block my-3">
                                                    <div class="custom-control custom-radio">
                                                        <input id="todos" name="genero" type="radio" class="custom-control-input" value="0" <?php echo $_GET['genero']==0 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="todos"><i class="fas fa-transgender fa-2x"></i>Todos</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input id="feminino" name="genero" type="radio" class="custom-control-input" value="1" <?php echo $_GET['genero']==1 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="feminino"><i class="fas fa-venus fa-2x"></i>Feminino</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input id="masculino" name="genero" type="radio" class="custom-control-input" value="2" <?php echo $_GET['genero']==2 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="masculino"><i class="fas fa-mars fa-2x"></i>Masculino</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div><!--row-->
                                        <div class="row">
                                            <div class="col-8">
                                                <button type="submit" class="btn btn-lg btn-outline-success btn-block float-left fa fa-search"> BUSCAR</button>
                                            </div>
                                            <div class="col-4">
                                                <a href="index.php?pg=Vbusca" class="btn btn-outline-dark btn-block float-right">LIMPAR BUSCA</a>
                                            </div>
                                        </div>
                                    </div><!--conteiner fluid-->

                                </form>
                            </div><!--body-->
                        </div>
                    </div>
                </div>
                <!--fim de modal modalserchadvanced-->

            </div>


        </div>
    </form>
        <?php
        if ($allow["allow_3"]==1){ ?>
            <a href="index.php?pg=Vpessoaeditar" class="btn btn-info btn-lg btn-block col-md-6 float-right">
                NOVO CADASTRO
            </a>
            <?php
        }
        ?>
</div>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
