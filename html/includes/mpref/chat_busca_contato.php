<?php
//ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}


require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

$url_atual=$_REQUEST['urltransf'];
//$url_atual=str_replace("http","https",$url_atual);

if(isset($_REQUEST['term'])){
    // Prepare a select statement
    $sca = $_REQUEST['term'];
    $sql = "SELECT * FROM tbl_users WHERE nome LIKE '%$sca%' AND `status`=1 AND `matriz`=3 order by nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $lipessoa = $consulta->fetchall();
    $contpessoa = $consulta->rowCount();
    $sql=null;
    $consulta=null;

    if($contpessoa > 0){
        // Fetch result rows as an associative array
        foreach ($lipessoa as $pe){
            $nome = explode(' ', $pe['nome']);
            $nome = $nome[0]." ".end($nome);
            echo "<form action='{$url_atual}' method='POST'><input type='hidden' name='troca_chat_val' value='{$pe['id']}' /><button class='cvp_contato_off' name='troca_chat'><span></span>{$nome}</button></form>";
        }
    } else{
        echo "<small class='text-center text-warning'><i class='fa fa-meh'></i> Sem combinações encontradas, confira a grafia</small>";
    }


}else{

    // Prepare a select statement
    $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    $tm2 = $tml+4000;
    $tml+=3000;
    try {
        $sql = "SELECT "
            ."tbl_token.id, tbl_token.token_time, tbl_users.nome, tbl_token.user AS id_prof "
            ."FROM tbl_users INNER JOIN tbl_token ON tbl_users.id = tbl_token.user WHERE (((tbl_token.token_time)>?) and ((tbl_token.token_time)<?) and ((tbl_users.matriz)=1)) ORDER BY tbl_users.nome";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$tml);
        $consulta->bindParam(2,$tm2);
        $consulta->execute();
    } catch (PDOException $error) {
        echo 'erro ao executar:' . $error->getMessage();
    }
    $pessoas = $consulta->fetchall();
    $pessoascont = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    $verif=0;


if ($pessoascont>1){
    foreach ($pessoas as $pessoa){
        if ($verif!=$pessoa[3]){
            $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
            $newtm =$pessoa[1]-$tml;
            if ($newtm<0){$newtm=0;}
            $newtm=gmdate("H:i:s", $newtm);

            //link pra msg que abre em novo painel
            if ($pessoa[3]!=$_SESSION['id']){
                $nome = explode(' ', $pessoa['nome']);
                $nome = $nome[0]." ".end($nome);
                echo "<form action='{$url_atual}' method='POST'><input type='hidden' name='troca_chat_val' value='{$pessoa['id_prof']}' /><button class='cvp_contato_on' name='troca_chat'><span></span>{$nome}</button></form>";
            }else{
//                    não mostrar o proprio user
            }

            $verif=$pessoa[3];
        }
    }
}else{
    echo "<small class='text-center text-info'><i class='fa fa-grin-beam-sweat'></i> Apenas você está online</small>";
}

}