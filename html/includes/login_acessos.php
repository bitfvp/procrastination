<?php
//ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}


require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

$sql = "SELECT Max(id) FROM tbl_token";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
$mxv = $consulta->fetch();
$sql = null;
$consulta = null;
echo $mxv[0];

