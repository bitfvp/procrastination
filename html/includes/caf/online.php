<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}


require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

// Prepare a select statement
$tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
$tm2 = $tml+4000;
$tml+=3000;
try {
    $sql = "SELECT "
        ."tbl_token.id, tbl_token.token_time, tbl_users.nome, tbl_token.user AS id_prof "
        ."FROM tbl_users INNER JOIN tbl_token ON tbl_users.id = tbl_token.user WHERE (((tbl_token.token_time)>?) and ((tbl_token.token_time)<?) and ((tbl_users.matriz)=2)) ORDER BY tbl_users.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$tml);
    $consulta->bindParam(2,$tm2);
    $consulta->execute();
} catch (PDOException $error) {
    echo 'erro ao executar:' . $error->getMessage();
}
$frase = $consulta->fetchall();
$contcidade = $consulta->rowCount();
$sql=null;
$consulta=null;
$verif=0;
?>
<section class="sidebar-offcanvas" id="sidebar">
    <div class="list-group text-left">
        <?php
        foreach ($frase as $tx){
            if ($verif!=$tx[3]){
                $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                $newtm =$tx[1]-$tml;
                if ($newtm<0){$newtm=0;}
                $newtm=gmdate("H:i:s", $newtm);

                //link pra msg que abre em novo painel
                if ($tx[3]!=$_SESSION['id']){
                    echo "<a href='{$realurl}caf/?pg=Vmsg&id={$tx[3]}' target=\"_blank\" onclick=\"window.open(this.href, this.target, 'width=500,height=650'); return false;\" ";
                    echo " target='_blank' onclick='window.open(this.href, this.target, 'width=500,height=650'); return false;'";
                }else{
                    echo "<a href='#' ";
                }
                echo " class='list-group-item text-info'  id='listonline' ><i class='fa fa-user-circle text-success'></i> ";
                echo $tx[2];
                echo "</a>";

                $verif=$tx[3];
            }
        }
        ?>
        </div>
    </section>
