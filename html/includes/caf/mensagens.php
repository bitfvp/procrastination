<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

/////////////////////////////////
$out_id = $_GET['out_id'];
try {
    $sql = "SELECT caf_msgs.id, caf_msgs.data, tbl_users.nome AS remetente, caf_msgs.destinatario, caf_msgs.lida, caf_msgs.remetente AS remetente_filtro, caf_msgs.conteudo\n"
        . "FROM tbl_users INNER JOIN caf_msgs ON tbl_users.id = caf_msgs.remetente\n"
        . "WHERE (((caf_msgs.destinatario)=:destinatario) AND ((caf_msgs.remetente)=:remetente)) OR (((caf_msgs.destinatario)=:remetente) AND ((caf_msgs.remetente)=:destinatario))\n"
        . "ORDER BY caf_msgs.data desc limit 0,25";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(':destinatario', $out_id);
    $consulta->bindValue(':remetente', $_SESSION['id']);
    $consulta->execute();
} catch (PDOException $error) {
    echo 'erro ao executar:' . $error->getMessage();
}
$msgss = array_reverse($consulta->fetchall());
$sql = null;
$consulta = null;

        foreach ($msgss as $ms) {
            if ($ms['destinatario'] > $ms['remetente_filtro']) {
                $clas_left_right = "left";
            } else {
                $clas_left_right = "right";
            }
            echo "<li class='message {$clas_left_right} appeared'>";
            echo "<div class='avatar'>";

            $prinome = explode(" ", $ms['remetente']);
            $priletra = str_split($prinome[0], 3);
            echo $priletra[0];

            echo "</div>";
            echo "<div class='text_wrapper'>";
            echo "<div class='text'>";
            echo "<strong class'text-info'>";
            echo Smilify($ms['conteudo'],$_ENV['ENV_URL']);
            echo "</strong>";

            echo "<br><strong>";
            $primeiroNome = explode(" ", $ms['remetente']);
            echo "</strong><i class='fa fa-clock'></i> ";
            echo datahoraBanco2data($ms['data']);


            if ($ms['lida'] == "1") {
                echo "  <i class='fa fa-check-double text-success fa-'></i>&nbsp;visualizado";

            } else {
                echo "  <span class=' fa fa-check text-danger'>&nbsp;não visualizado</span>";
            }

            echo "</div>";
            echo "<hr class='m-0' style='border-color: #ccc'>";

            if ($ms['id'] != 0) {
                $files = glob("../../dados/caf/chat/" . $ms['id'] . "/*.*");
                for ($i = 0; $i < count($files); $i++) {
                    $num = $files[$i];
                    $extencao = explode(".", $num);
                    //ultima posicao do array
                    $ultimo = end($extencao);
                    //
                    $co= explode("/", $num);
                    $num=$co[1]."/".$co[2]."/".$co[3]."/".$co[4]."/".$co[5]."/".$co[6];
                    //
                    switch ($ultimo) {
                        case "docx":
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href=" . $num . " target='_blank' class='img-thumbnail'>"
                                ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/docx.png alt='...' class='img-fluid'>"
                                ."</a>"
                                ."</div>"
                                ."</div>";
                            break;

                        case "doc":
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href=" . $num . " target='_blank' class='img-thumbnail'>"
                                ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/doc.png alt='...' class='img-fluid'>"
                                ."</a>"
                                ."</div>"
                                ."</div>";
                            break;

                        case "xls":
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href=" . $num . " target='_blank' class='thumbnail'>"
                                ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/xls.png alt='...' class='img-fluid'>"
                                ."</a>"
                                ."</div>"
                                ."</div>";
                            break;

                        case "xlsx":
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href=" . $num . " target='_blank' class='img-thumbnail'>"
                                ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/xls.png alt='...' class='img-fluid'>"
                                ."</a>"
                                ."</div>"
                                ."</div>";
                            break;

                        case "pdf":
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href=" . $num . " target='_blank' class='img-thumbnail'>"
                                ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/pdf.png alt='...' class='img-fluid'>"
                                ."</a>"
                                ."</div>"
                                ."</div>";
                            break;

                        default:
                            echo "<div class='row'>";
                            echo "<div class='col-md-4'>"
                                ."<a href='" .$num . "' target='_blank' class='img-thumbnail'>"
                                ."<img src='" .$num . "' alt='++' class='img-fluid'>"
                                ."</a>"
                                ."</div>"."</div>";
                            break;
                    }


                }
            }
            echo "</div>";
            echo "</li>";
        }

try {
    $sql = "UPDATE caf_msgs ";
    $sql .= "SET lida='1'";
    $sql .= " WHERE ((caf_msgs.destinatario)=:destinatario AND (caf_msgs.remetente)=:remetente ) ";

    global $pdo;
    $atualiza = $pdo->prepare($sql);
    $atualiza->bindValue(':destinatario', $_SESSION['id']);
    $atualiza->bindValue(':remetente', $out_id);
    $atualiza->execute();

} catch (PDOException $error_msg) {
    echo 'Erro' . $error_msg->getMessage();
}
/// /////////////////////////////////
?>
<script>
    (function () {
        $('.messages').animate({scrollTop: 9999999}, 500);

    }.call(this));
</script>
