<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


try {
    $sql = "SELECT smem_msgs.id, smem_msgs.data, tbl_users.nome AS remetente, smem_msgs.destinatario, smem_msgs.lida, smem_msgs.remetente AS remetente_filtro\n"
        . "FROM tbl_users INNER JOIN smem_msgs ON tbl_users.id = smem_msgs.remetente\n"
        . "WHERE (((smem_msgs.destinatario)=:user) AND ((smem_msgs.lida)=0))\n"
        . "ORDER BY smem_msgs.data";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":user", $_SESSION['id']);
    $consulta->execute();
    $msgslista = $consulta->fetchAll();
    $contmsg=$consulta->rowCount();
    $sql=null;
    $consulta=null;
} catch (PDOException $error) {
    echo 'erro ao executar:' . $error->getMessage();
}
if ($contmsg!=0) {
?>
    <a href=""></a>
    <script>
        $.notify({
            // options
            icon: 'fa fa-envelope',
            title: '',
            message: 'Você tem uma nova mensagem<a href=\'#\' onclick=\'msgoff()\' data-toggle=\'modal\' data-target=\'#modalmsgs\'> Abra agora!!</a>',
            url: '',
            target: ''
        }, {
            // settings
            type: "info",
            allow_dismiss: true,
            newest_on_top: true,
            showProgressbar: false,
            placement: {
                from: "top",
                align: "right"
            },
            offset: {
                x: 20,
                y: 100
            },
            spacing: 10,
            z_index: 1031,
            delay: 10000,//10 segundos
            timer: 1000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    </script>
    <?php
    foreach ($msgslista as $msg) {
        echo "<form action='#' method='POST' class=''>"
            ."<input type='hidden' name='troca_chat_val' value='{$msg['remetente_filtro']}' />"
            ."<button class='border-0' name='troca_chat'>"
            ."<i class='fa fa-envelope mr-2 text-info'>  "
            .$msg['remetente']
            ."</i>"
            ."<i class='float-right'>"
            .datahorabanco2data($msg['data'])
            ."</i>"
            ."</button>"
            ."</form>";
    }
}else{
    echo "<h5 class='text-muted'>";
    echo "Não há mensagens novas para você <i class='fa fa-grin-beam-sweat'></i>";
    echo "</h5>";
} ?>