<?php
//ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}


require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

$out_id = $_SESSION['id_chat'];
$folder = $_GET['folder'];
try {
    $sql = "SELECT \n"
        . "smem_msgs.id,smem_msgs.`data`,smem_msgs.remetente,smem_msgs.destinatario,smem_msgs.lida,smem_msgs.conteudo \n"
        . "FROM smem_msgs WHERE \n"
        . "(((smem_msgs.destinatario) =:destinatario) \n"
        . "AND ((smem_msgs.remetente) =:remetente)) \n"
        . "OR \n"
        . "(((smem_msgs.destinatario) =:remetente) \n"
        . "AND ((smem_msgs.remetente) =:destinatario)) \n"
        . "ORDER BY smem_msgs.data desc limit 0,25";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(':destinatario', $out_id);
    $consulta->bindValue(':remetente', $_SESSION['id']);
    $consulta->execute();
} catch (PDOException $error) {
    echo 'erro ao executar:' . $error->getMessage();
}
$msgss = array_reverse($consulta->fetchall());
$sql = null;
$consulta = null;

foreach ($msgss as $ms) {
    if ($ms['remetente']==$_SESSION['id']) {
        $posicao = "cv_msg_enviada";
    } else {
        $posicao = "cv_msg_recebida";
    }

    if ($ms['lida'] == "1") {
        $lida="fa fa-check-double text-success";
    } else {
        $lida="fa fa-check text-danger";
    }
    echo "<div class='{$posicao}'><strong>";
    echo Smilify($ms['conteudo'],$_ENV['ENV_URL']);
    echo "</strong><br><i class='{$lida}'>".datahoraBanco2data($ms['data'])."</i><br>";

    if ($ms['id'] != 0) {
        $files = glob("../../dados/smem/chat/" . $ms['id'] . "/*.*");
        for ($i = 0; $i < count($files); $i++) {
            $num = $files[$i];
            $extencao = explode(".", $num);
            //ultima posicao do array
            $extencao = end($extencao);
            //
            $co= explode("/", $num);
            if ($folder=="../"){
                $num=$co[1]."/".$co[2]."/".$co[3]."/".$co[4]."/".$co[5]."/".$co[6];
            }
            if ($folder=="../../"){
                $num=$co[0]."/".$co[1]."/".$co[2]."/".$co[3]."/".$co[4]."/".$co[5]."/".$co[6];
            }
            //
            switch ($extencao) {
                case "docx":
                    echo "<a href=" . $num . " target='_blank' class=''>"
                        ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/docx.png alt='...' class='img-fluid'>"
                        ."</a>";
                    break;

                case "doc":
                    echo "<a href=" . $num . " target='_blank' class=''>"
                        ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/doc.png alt='...' class='img-fluid'>"
                        ."</a>";
                    break;

                case "xls":
                    echo "<a href=" . $num . " target='_blank' class=''>"
                        ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/xls.png alt='...' class='img-fluid'>"
                        ."</a>";
                    break;

                case "xlsx":
                    echo "<a  href=" . $num . " target='_blank' class=''>"
                        ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/xls.png alt='...' class='img-fluid'>"
                        ."</a>";
                    break;

                case "pdf":
                    echo "<a href=" . $num . " target='_blank' class=''>"
                        ."<img src=" . $_ENV['ENV_URL'] . $_ENV['ENV_ESTATICO'] . "img/pdf.png alt='...' class='img-fluid'>"
                        ."</a>";
                    break;

                default:
                   echo "<a href='" .$num . "' target='_blank' class=''>"
                        ."<img src='" .$num . "' alt='++' class='img-fluid mb-1'>"
                        ."</a>";
                    break;
            }


        }
    }
    echo "</div>";

}//fim de foreach

try {
    $sql = "UPDATE smem_msgs ";
    $sql .= "SET lida='1'";
    $sql .= " WHERE ((smem_msgs.destinatario)=:destinatario AND (smem_msgs.remetente)=:remetente ) ";

    global $pdo;
    $atualiza = $pdo->prepare($sql);
    $atualiza->bindValue(':destinatario', $_SESSION['id']);
    $atualiza->bindValue(':remetente', $out_id);
    $atualiza->execute();

} catch (PDOException $error_msg) {
    echo 'Erro' . $error_msg->getMessage();
}