<?php
//testa se ha as sessions com token
if (isset($_SESSION["lm_tokenId"]) and isset($_SESSION["lm_tokenTime"])) {
    //busca no banco tokens iguais
    $sql = "SELECT * FROM lm_token WHERE token=? AND token_time=?";
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_SESSION["lm_tokenId"]);
    $consulta->bindParam(2, $_SESSION["lm_tokenTime"]);
    $consulta->execute();
    $tli = $consulta->fetch();
    $tliCont = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    //renova session tokenid
    $_SESSION['lm_tokenId'] = $tli['token'];
    //verifica se o banco retornou algo
    if ($tliCont != 0) {
        //pega mk do time atual
        $tm1 = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
        $tm2=$tm1;
        //testa se o valor no banco é maior que o session
        if (($tli['token_time'] > $tm2) and ($_SESSION['lm_id']==$tli['user'])) {
            //novo tempo de validade pro token
            $newtm = $tm1 + 3600;
            //renova session tokentime
            $_SESSION['lm_tokenTime'] = $newtm;
            $sql = "UPDATE lm_token SET token_time=? WHERE token=?";
            $up = $pdo->prepare($sql);
            $up->bindParam(1, $newtm);
            $up->bindParam(2, $tli['token']);
            $up->execute();
            $sql = null;
            $up = null;
                /////////////////////////////////////////////////////////////////////////////////////////
            }else{
                killSession();
            }

        } else {
            //se tempo espirar
            $_SESSION['fsh']=[
                "flash"=>"Muito tempo inativo!!",
                "type"=>"danger",
                "error"=>"Sua seção acabou",
            ];
            killSession();
        }

} else {
    killSession();
}
?>