<?php

function fncgetuser($id){
    $sql = "SELECT * FROM lm_users WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getgui = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getgui;
}
