<?php
function fncguildalista(){
    $sql = "SELECT * FROM lm_guildas where status=1 ORDER BY abreviacao ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $guilista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $guilista;
}

function fncgetguilda($id){
    $sql = "SELECT * FROM lm_guildas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getgui = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getgui;
}
