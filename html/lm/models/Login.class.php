<?php
class Login{
    public function fnclogar($usuario, $senha, $dispositivo, $sistema_operacional, $navegador ){

        //verifica se o banco tem o usuario e senha
	    try{
            $sql="select * from lm_users WHERE usuario=? AND senha=? limit 1";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $usuario);
            $consulta->bindParam(2, $senha);
            $consulta->execute();
	    }catch ( PDOException $error_msg){
	        echo 'Erroff'. $error_msg->getMessage();
        }
        //verifica se existe >0 registros
        if( $consulta->rowCount()!=0){
			$dados=$consulta->fetch();
			//verifica se esta ativo
//			if($dados['status']==1){
                $_SESSION['lm_id']=$dados['id'];
				$_SESSION['lm_nome']=$dados['nome'];
                $_SESSION['lm_usuario']=$dados['usuario'];
                $_SESSION['lm_guilda']=$dados['guilda'];
                $_SESSION['lm_nivel']=$dados['nivel'];
                $_SESSION['lm_status']=$dados['status'];
                $_SESSION['lm_logado']="1";
				$log=1;

				//seta o token
                $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                $tm2=$tml;
                $newtm = $tm2+"3600";
                $lm_tokenId = sha1($tm2);
                $_SESSION['lm_tokenId'] = $lm_tokenId;
                $_SESSION['lm_tokenTime'] = $newtm;


                $sql = "insert into lm_token ";
                $sql .= "(id, token, token_time, user, dispositivo, sistema_operacional, navegador) values(NULL, :token, :time, :user, :dispositivo, :sistema_operacional, :navegador) ";
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":token", $lm_tokenId);
                $insere->bindValue(":time", $newtm);
                $insere->bindValue(":dispositivo", $dispositivo);
                $insere->bindValue(":sistema_operacional", $sistema_operacional);
                $insere->bindValue(":navegador", $navegador);
                $insere->bindValue(":user", $dados['id']);
                $insere->execute();
                $sql = null;
                $consulta = null;

//			}else{
//                $_SESSION['fsh']=[
//			        "flash"=>"Aguarde nossa Aprovação",
//                    "type"=>"info",
//                    ];
//			}
		}
		if(isset($log)){
		    $sorte=rand(1,10);
            switch ($sorte){
                case 1:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo ao sistema que você nunca soube que queria",
                        "type"=>"success",
                    ];
                    break;
                case 2:
                    $_SESSION['fsh']=[
                        "flash"=>"olá, preparamos tudo para você ter uma ótima experiência",
                        "type"=>"success",
                    ];
                    break;
                case 3:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, estamos cada vez melhor graças a sua ajuda",
                        "type"=>"success",
                    ];
                    break;
                case 4:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, se estava bom ontem, hoje está melhor",
                        "type"=>"success",
                    ];
                    break;

                case 6:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, que bom que está com a gente",
                        "type"=>"success",
                    ];
                    break;
                case 8:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, estamos prontos pra começar",
                        "type"=>"success",
                    ];
                    break;
                case 9:
                    $_SESSION['fsh']=[
                        "flash"=>"Apenas que… Busquem conhecimento.",
                        "type"=>"success",
                    ];
                    break;
                case 10:
                    $_SESSION['fsh']=[
                        "flash"=>"Apenas que… Busquem conhecimento!",
                        "type"=>"success",
                    ];
                    break;
                default:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo",
                        "type"=>"success",
                    ];
                    break;
            }


		}else{
			if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops, erro ao entrar, digite usuario e senha corretamente!",
                    "type"=>"danger",
                ];
			}
		}

		
	}

}