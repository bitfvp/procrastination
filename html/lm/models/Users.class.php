<?php 
class Users{
	public function fncnewuser($nome, $usuario, $senha){
		//valida se ja ha um usuario cadastrado

			//inserção no banco
            try{
                $sql="INSERT INTO lm_users(nome,usuario,senha,guilda,nivel,status)VALUES(:nome,:usuario,:senha,0,1,0)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":usuario", $usuario);
                $insere->bindValue(":senha", $senha);
                $insere->execute();

            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

			if(isset($insere)){

                $_SESSION['fsh']=[
                    "flash"=>"Cadastro realizado com sucesso!!",
                    "type"=>"success",
                ];
                
			}else{
				if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];
				}
			}
	}
//==================================================================================================


//==================================================================================================
    public function fncselectguilda($user, $guilda)
    {
        try{
            $sql = "UPDATE lm_users SET guilda = :guilda WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":guilda", $guilda);
            $insere2->bindValue(":id", $user);
            $insere2->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        $_SESSION['lm_guilda']=$guilda;

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

//==================================================================================================
//==================================================================================================
    public function fncsairguilda($user)
    {
        try{
            $sql = "UPDATE lm_users SET guilda = 0 and nivel=1 WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":id", $user);
            $insere2->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        $_SESSION['lm_guilda']=0;

        if(isset($insere2)){
            /////////////////////////////////////////////////////

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
//=========================================================================================
    public function fncusuarioedit($pessoa,
                                   $nome,
                                   $status,
                                   $email,
                                   $nascimento,
                                   $cpf,
                                   $profissao,
                                   $matriz)
    {
        try{
            $sql = "UPDATE tbl_users SET nome = :nome, status = :status, email = :email, nascimento = :nascimento, cpf = :cpf, profissao = :profissao, matriz = :matriz WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":nome", $nome);
            $insere2->bindValue(":status", $status);
            $insere2->bindValue(":email", $email);
            $insere2->bindValue(":nascimento", $nascimento);
            $insere2->bindValue(":cpf", $cpf);
            $insere2->bindValue(":profissao", $profissao);
            $insere2->bindValue(":matriz", $matriz);
            $insere2->bindValue(":id", $pessoa);
            $insere2->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
/// //==================================================================================================

//=========================================================================================
    public function fncnewsenha($id,$senha){
        //tratamento das variaveis
        $senha=sha1($senha."1010011010");

        //atualização no banco
        try{
            $sql="UPDATE lm_users SET senha=:senha ";
            $sql.="WHERE id=:id";
            global $pdo;
            $at=$pdo->prepare($sql);
            $at->bindValue(":id", $id);
            $at->bindValue(":senha", $senha);
            $at->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if(isset($at)){
            $_SESSION['fsh']=[
                "flash"=>"Atualização de senha realizado com sucesso!!",
                "type"=>"success",
                "error"=>"No proximo login use a nova senha cadastrada",
            ];
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
//==================================================================================================

//=========================================================================================
    public function fncresetsenha($id){
        //inserção no banco
        try{
            $sql = "UPDATE tbl_users SET senha = :novasenha WHERE id = :id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":novasenha", "c8b27e713ebfe1e38f991c4462360a9b11242db9");
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Senha resetada com sucesso!! ",
                "type"=>"success",
                "error"=>"<h2>Nova senha: 12345678</h2>"
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador ",
                    "type"=>"danger",
                ];

            }
        }
    }
    //==================================================================================================

//=========================================================================================
}
?>