<?php
class Lancamento
{
    public function fnclancamentonew($usuario,$guilda,$comum,$incomum,$raro,$chaves,$Upin)
    {
        $data=date('Y'."/".'m'."/".'d');

        try {
            $sql = "INSERT INTO lm_lancamentos ";
            $sql .= "(id, usuario, guilda, data, comum, incomum, raro, chaves)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :usuario, :guilda, :data, :comum, :incomum, :raro, :chaves)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":usuario", $usuario);
            $insere->bindValue(":guilda", $guilda);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":comum", $comum);
            $insere->bindValue(":incomum", $incomum);
            $insere->bindValue(":raro", $raro);
            $insere->bindValue(":chaves", $chaves);
            $insere->execute();
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($insere)) {
            //foto
            $sql = "SELECT Max(id) FROM lm_lancamentos";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $mxv = $consulta->fetch();
            $sql = null;
            $consulta = null;
            $maxv = $mxv[0];
            // verifica se foi enviado um arquivo
            $fillle=$_FILES['arquivo']['name'];
            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
                if (is_dir("../dados/lm/lancamentos/" . $maxv . '/')) {} else {mkdir("../dados/lm/lancamentos/" . $maxv . '/');}
                // verifica se foi enviado um arquivo
                $Upin->get(
                    '../dados/lm/lancamentos/'.$maxv.'/', //Pasta de uploads (previamente criada)
                    $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
                    10, //Tamanho máximo
                    "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
                    "arquivo", //Atributo name do input file
                    1 //Mudar o nome? 1 = sim, 0 = não
                );
                $Upin->run();
            }
            //começa alteração atendido pelo cras
            //

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vhome");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new


}//fim da classe

?>