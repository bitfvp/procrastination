<!DOCTYPE html>
<html lang="pt-br">

<head>
    <script>
      // adiciona via js um favicom
        function changeFavicon(src) {
            var link = document.createElement('link'),
                oldLink = document.getElementById('dynamic-favicon');
            link.id = 'dynamic-favicon';
            link.rel = 'shortcut icon';
            link.href = src;
            if (oldLink) {
                document.head.removeChild(oldLink);
            }
            document.head.appendChild(link);
        }
        changeFavicon("<?php echo $env->env_url;?>favicon32.ico");
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <META NAME="author" CONTENT="Works">
    <META NAME="description" CONTENT="Flavioworks flavio lordsmobile ">
    <META NAME="keywords" CONTENT="lordsmobile flavioworks flavio works">

    <title><?php echo $page; ?></title>

    <?php include_once("includes/style.php");?>
</head>
<body>
<?php
if (isset($_SESSION['fsh']) and $_SESSION['fsh']!=null) {
    $fsh=$_SESSION['fsh'];

    switch ($fsh['type']) {
        case "success":
            $fsh_ico="fa fa-smile";
            if (isset($fsh['error'])){
                $fsh['flash'].="<br>".$fsh['error'];
            }
            $timeinfo=5000;
            break;
        case "info":
            $fsh_ico="fa fa-info";
            if (isset($fsh['error'])){
                $fsh['flash'].="<br>".$fsh['error'];
            }
            $timeinfo=5000;
            break;
        case "warning":
            $fsh_ico="fa fa-exclamation-triangle";
            if (isset($fsh['error'])){
                $fsh['flash'].="<br>".$fsh['error'];
            }
            $timeinfo=10000;
            break;
        case "danger":
            $fsh_ico="fa fa-dizzy";
            if (isset($fsh['error'])){
                $fsh['flash'].="<br>".$fsh['error'];
            }
            $timeinfo=10000;
            break;
    }
    ?>
        <script>
            $.notify({
                // options
                icon: '<?php echo $fsh_ico;?>',
                title: '',
                message: '<?php echo $fsh['flash'];?>',
                url: '',
                target: ''
            }, {
                // settings
                type: "<?php echo $fsh['type'];?>",
                allow_dismiss: true,
                newest_on_top: true,
                showProgressbar: false,
                placement: {
                    from: "bottom",
                    align: "center"
                },
                offset: {
                    x: 20,
                    y: 100
                },
                spacing: 10,
                z_index: 1031,
                delay: <?php echo $timeinfo;?>,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });
        </script>
    <?php
}
    if (isset($fsh['pointofview']) and $fsh['pointofview']!=0){
        switch ($fsh['pointofview']){
            case 1:
                $pv="pointofview";
                break;
            case 2:
                $pv="pointofview2";
                break;
            case 3:
                $pv="pointofview3";
                break;
            case 4:
                $pv="pointofview4";
                break;
            default:
                $pv="pointofview";
                break;
        }
        ?>
<script>
    $(document).ready(function() {
        window.location.href='#<?php echo $pv;?>';
    });
</script>
<?php }
$_SESSION['fsh']=null;
unset($_SESSION['fsh']);