<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();

////classe para debugar e salvar sqls ao banco de dados
//include_once("{$env->env_root}models/LogQuery.class.php");//classe de log de query
//$LQ = new LogQuery();



//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

////login
/// classe para login
include_once("models/Login.class.php");
///controle para login
include_once("controllers/login.php");

//logout
//controle pra logout
include_once("controllers/logout.php");

//validação de sessoes ativas
//inclui um controle que valida o hash e renova o mesmo se tiver tudo certo
include_once("controllers/validaToken.php");


/* inicio do Bloco dedidado*/
//registro
include_once("models/Users.class.php");
include_once("controllers/registro.php");
include_once("controllers/user_lista.php");

include_once("controllers/guilda_lista.php");

include_once("models/Lancamento.class.php");
include_once("controllers/lancamentos.php");
/* fim do bloco dedicado*/


//metodo de checar usuario, confirma se a session que vincula o login ao hash de sessao no banco de dados esta ativa
include_once("controllers/confirmUser.php");