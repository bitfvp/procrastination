<nav class="navbar navbar-expand-md navbar-dark bg-dark sticky-top mb-2">
    <a class="navbar-brand" href="index.php">
        <?php
        echo strtoupper(fncgetguilda($_SESSION['lm_guilda'])['abreviacao']);
        ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="true" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse" id="navbarsExample04" style="">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=Vmembros">Membros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=Vmeus">Meus Lançamentos</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <?php
                    $primeiroNome = explode(" ", $_SESSION["lm_nome"]);
                    $comp_msg.="Olá ";
                    $comp_msg.=$primeiroNome[0]; // Fulano
                    $comp_msg.=", ".Comprimentar();
                    echo $comp_msg;
                    ?>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                    <a class="dropdown-item" href ="index.php?pg=Vhome&aca=sairguilda"><i class="fa fa-times"></i>&nbsp;Sair da Guilda</a>
                    <a class="dropdown-item" href ="index.php?pg=Vtroca_senha"><i class="fa fa-key"></i>&nbsp;Alterar Senha</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");