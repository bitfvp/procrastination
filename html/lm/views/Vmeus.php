<?php
$page="".$env->env_titulo;
$css="padrao";
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(!isset($_SESSION['lm_guilda']) or $_SESSION['lm_guilda']==0){
    header("Location: {$env->env_url}lm/index.php?pg=Vguildaselect");
    exit();
}

include_once("includes/head.php");
include_once("includes/topo.php");
?>

<main class="container-fluid">

    <?php
    $sql = "SELECT * FROM lm_lancamentos where guilda=? and usuario=? order by data_hora desc limit 0,15";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$_SESSION['lm_guilda']);
    $consulta->bindParam(2,$_SESSION['lm_id']);
    $consulta->execute();
    $llista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

    ?>
    <div class="" id="">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th><div class="vertical">Data</div></th>
                <th><div class="vertical">Comum</div></th>
                <th><div class="vertical">Incomum</div></th>
                <th><div class="vertical">Raro</div></th>
                <th><div class="vertical">Chaves</div></th>
            </tr>
            </thead>
            <tbody>
            <?php
            // vamos criar a visualização
            foreach ($llista as $dados){
            ?>
            <tr  data-toggle="collapse" data-target="#accordion<?php echo $dados['id']; ?>" class="clickable">
                <td>
                    <?php
                    echo datahoraBanco2data($dados['data']);
                    ?>
                </td>

                <td class="text-muted">
                    <?php
                    echo $dados['comum'];
                    ?>
                </td>
                <td class="text-success">
                    <?php
                    echo $dados['incomum'];
                    ?>
                </td>
                <td class="text-info">
                    <?php
                    echo $dados['raro'];
                    ?>
                </td>
                <td class="bg-dark text-light">
                    <?php
                    echo $dados['chaves'];
                    ?>
                </td>
            </tr>
                <tr id="accordion<?php echo $dados['id']; ?>" class="collapse">
                    <td colspan="5">
                        <?php
                        if ($dados['id'] != 0) {
                            $files = glob("../dados/lm/lancamentos/" . $dados['id'] . "/*.*");
                            for ($i = 0; $i < count($files); $i++) {
                                $num = $files[$i];
                                $extencao = explode(".", $num);
                                //ultima posicao do array
                                $ultimo = end($extencao);
                                switch ($ultimo) {
                                    case "docx":
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank'>";
                                        echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                        break;

                                    case "doc":
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank'>";
                                        echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                        break;

                                    case "xls":
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank'>";
                                        echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";

                                        echo "</div>";
                                        echo "</div>";
                                        break;

                                    case "xlsx":
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank'>";
                                        echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                        break;

                                    case "pdf":
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank'>";
                                        echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                        break;

                                    default:
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href=" . $num . " target='_blank' >";
                                        echo "<img src=" . $num . " alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                        break;
                                }
                            }
                        }//fim de foto
                        ?>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>


</main>



<?php include_once("includes/footer.php"); ?>
</body>

</html>