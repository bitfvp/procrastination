<?php
$page="Login-".$env->env_titulo;
$css="padrao";
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(!isset($_SESSION['lm_guilda']) or $_SESSION['lm_guilda']==0){
    header("Location: {$env->env_url}lm/index.php?pg=Vguildaselect");
    exit();
}
include_once("includes/head.php");
include_once("includes/topo.php");
?>

<div class="text-center">

    <form class="form-signin" action="index.php?pg=Vlancar&aca=lancar" method="post"  enctype="multipart/form-data">
        <h1 class="h2 mb-3 font-weight-normal">
           Lançar
        </h1>

        <input type="file" class="form-control mt-1" id="arquivo" name="arquivo[]" value="" multiple>

        <label for="comum" class="h3">Comum
            <div class="qty mt-2">
                <span class="minus bg-dark" id="minuscomum">-</span>
                <input type="number" class="count" id="comum" name="comum" value="0">
                <span class="plus bg-dark" id="pluscomum">+</span>
            </div>
        </label>

        <label for="incomum" class="h3 text-success">Incomum
            <div class="qty mt-2">
                <span class="minus bg-dark" id="minusincomum">-</span>
                <input type="number" class="count" id="incomum" name="incomum" value="0">
                <span class="plus bg-dark" id="plusincomum">+</span>
            </div>
        </label>

        <label for="raro" class="h3 text-info">Raro
            <div class="qty mt-2">
                <span class="minus bg-dark" id="minusraro">-</span>
                <input type="number" class="count" id="raro" name="raro" value="0">
                <span class="plus bg-dark" id="plusraro">+</span>
            </div>
        </label>

        <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="Salvar">

    </form>

</div> <!-- /container -->

<?php include_once("includes/footer.php"); ?>

</body>
</html>