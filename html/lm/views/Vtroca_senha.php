<?php
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(!isset($_SESSION['lm_guilda']) or $_SESSION['lm_guilda']==0){
    header("Location: {$env->env_url}lm/index.php?pg=Vguildaselect");
    exit();
}

$page="Alterar Senha-".$env->env_titulo;
$css="padrao";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-info text-light">
                Alteração de senha de acesso
            </div>
            <div class="card-body">
                <form action="?pg=Vtroca_senha&aca=novasenha" method="post">
                    <div class="form-group">
                        <label for="senha">Nova senha</label>
                        <input type="password" class="form-control" name="newpass"  autocomplete="off" placeholder="Digite a nova senha com pelo menos 8 caracteres" autofocus minlength="8" maxlength="30"/>
                    </div>
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR NOVA SENHA"/>
                </form>
            </div>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>