<?php
$page="".$env->env_titulo;
$css="padrao";
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(!isset($_SESSION['lm_guilda']) or $_SESSION['lm_guilda']==0){
    header("Location: {$env->env_url}lm/index.php?pg=Vguildaselect");
    exit();
}

include_once("includes/head.php");
include_once("includes/topo.php");
?>

<main class="container-fluid">

    <?php
    $sql = "SELECT * FROM lm_users where guilda=? ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$_SESSION['lm_guilda']);
    $consulta->execute();
    $llista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

    ?>
    <div class="" id="">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th><div class="vertical">Usuario</div></th>
                <th><div class="vertical">Nome</div></th>
                <th><div class="vertical">Remover</div></th>
            </tr>
            </thead>
            <tbody>
            <?php
            // vamos criar a visualização
            foreach ($llista as $dados){
            ?>
            <tr>
                <td>
                    <a href="index.php?pg=Vmembro&id=<?php echo $dados['id']; ?>">
                        <?php
                        echo $dados['usuario'];
                        ?>
                    </a>
                </td>

                <td class="text-muted">
                    <?php
                    echo $dados['nome'];
                    ?>
                </td>
                <td class="bg-dark text-light">
                    <a href="" class="btn btn-danger btn-sm" ><i class="fas fa-times" ></i></a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>


</main>



<?php include_once("includes/footer.php"); ?>
</body>

</html>