<?php
$page="Login-".$env->env_titulo;
$css="padrao";
if(isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/");
    exit();
}
include_once("includes/head.php");
?>


<div class="text-center">

    <form class="form-signin" action="index.php?pg=Vregistro&aca=registrar" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Preencha Seus Dados</h1>

        <label for="nome" class="sr-only">Nome</label>
        <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome" required autofocus maxlength="50">



        <script type="text/javaScript">
            function Trim(str){
                return str.replace(/^\s+|\s+$/g,"");
            }
        </script>

        <label for="usuario" class="sr-only">Usuario</label>
        <input type="text" name="usuario" id="usuario" class="form-control mt-1" placeholder="Usuario" required autofocus onkeyup="this.value = Trim( this.value )" maxlength="30">

        <label for="senha" class="sr-only">Senha</label>
        <input type="password" name="senha" id="senha" class="form-control mt-1" placeholder="Senha" required>

        <button class="btn btn-lg btn-success btn-block mt-2" type="submit">Salvar</button>

        <p class="mt-5 mb-3 text-muted">Ou se já tem o cadastro <br><a href="index.php?pg=Vlogin">Entrar</a></p>
    </form>

</div> <!-- /container -->

<?php include_once("includes/footer.php"); ?>

</body>
</html>