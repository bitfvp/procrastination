<?php
$page="Login-".$env->env_titulo;
$css="padrao";
if(isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/");
    exit();
}
include_once("includes/head.php");
?>


<div class="text-center">

    <form class="form-signin" action="index.php?pg=Vlogin&aca=logar" method="post">
        <h1 class="h2 mb-3 font-weight-normal">Monitor de caça Lords Mobile</h1>

        <script type="text/javaScript">
            function Trim(str){
                return str.replace(/^\s+|\s+$/g,"");
            }
        </script>

        <label for="usuario" class="sr-only">Usuario</label>
        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" required autofocus onkeyup="this.value = Trim( this.value )" maxlength="30">

        <label for="senha" class="sr-only">Senha</label>
        <input type="password" name="senha" id="senha" class="form-control mt-1" placeholder="Senha" required>

        <button class="btn btn-lg btn-primary btn-block mt-2" type="submit">Entrar</button>

        <p class="mt-5 mb-3 text-muted">Se ainda não tem o cadastro <br><a href="index.php?pg=Vregistro">Registre-se</a></p>
    </form>

</div> <!-- /container -->

<?php include_once("includes/footer.php"); ?>

</body>
</html>