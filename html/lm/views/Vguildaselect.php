<?php
$page="Login-".$env->env_titulo;
$css="padrao";
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(isset($_SESSION['lm_guilda']) and $_SESSION['lm_guilda']!=0){
    header("Location: {$env->env_url}lm/index.php");
    exit();
}

include_once("includes/head.php");
?>


<div class="text-center">

    <form class="form-signin" action="index.php?pg=Vguildaselect&aca=selectguilda" method="post">
        <h1 class="h2 mb-3 font-weight-normal">Selecione sua guilda</h1>

        <label for="guilda" class="sr-only">Guilda</label>
        <select name="guilda" id="guilda" class="form-control"  required autofocus >
            // vamos criar a visualização
            <option selected="" value="0">Selecione...</option>
            <?php
            foreach (fncguildalista() as $ativ) {
                ?>
                <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['guilda']; ?></option>
                <?php
            }
            ?>
        </select>

        <button class="btn btn-lg btn-success btn-block mt-2" type="submit">Salvar</button>

    </form>

</div> <!-- /container -->

<?php include_once("includes/footer.php"); ?>

</body>
</html>