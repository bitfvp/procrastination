<?php
$page="".$env->env_titulo;
$css="padrao";
if(!isset($_SESSION['lm_logado'])){
    header("Location: {$env->env_url}lm/index.php?pg=Vlogin");
    exit();
}

if(!isset($_SESSION['lm_guilda']) or $_SESSION['lm_guilda']==0){
    header("Location: {$env->env_url}lm/index.php?pg=Vguildaselect");
    exit();
}

include_once("includes/head.php");
include_once("includes/topo.php");
?>

<main class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <a href="index.php?pg=Vlancar" class="btn btn-block btn-success">Fazer Lançamento</a>
        </div>
        <div class="col-md-12 mt-2">
            <a class="btn btn-block btn-primary"  data-toggle="collapse" href="#ultimos" role="button" aria-expanded="false" aria-controls="ultimos">
                Mostrar Ultimos
            </a>
        </div>
    </div>



    <?php
    $sql = "SELECT * FROM lm_lancamentos where guilda=? ORDER BY data_hora desc limit 0,20";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$_SESSION['lm_guilda']);
    $consulta->execute();
    $llista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

    ?>
    <div class="collapse" id="ultimos">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th><div class="vertical">Usuario</div></th>
                <th><div class="vertical">Comum</div></th>
                <th><div class="vertical">Incomum</div></th>
                <th><div class="vertical">Raro</div></th>
                <th><div class="vertical">Chaves</div></th>
            </tr>
            </thead>
            <tbody>
            <?php
            // vamos criar a visualização
            foreach ($llista as $dados){
            ?>
            <tr data-toggle="tooltip" data-placement="top" title="<?php echo datahoraBanco2data($dados['data_hora']);?>">
                <td>
                    <a href="index.php?pg=Vmembro&id=<?php echo $dados['usuario']; ?>">
                        <?php
                        echo fncgetuser($dados['usuario'])['usuario'];
                        ?>
                    </a>
                </td>

                <td class="text-muted">
                    <?php
                    echo $dados['comum'];
                    ?>
                </td>
                <td class="text-success">
                    <?php
                    echo $dados['incomum'];
                    ?>
                </td>
                <td class="text-info">
                    <?php
                    echo $dados['raro'];
                    ?>
                </td>
                <td class="bg-dark text-light">
                    <?php
                    echo $dados['chaves'];
                    ?>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="curve_chart1" class="pr-2"></div>
        </div>
    </div>




</main>



<?php include_once("includes/footer.php"); ?>
</body>

</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMUM', 'INCOMUM', 'RARO', 'CHAVES'],
            <?php
            // Recebe
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-1 AND CURRENT_DATE()-0 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
                foreach ($graf as $gf) {
                 if (!is_null($gf[0])){
                     echo "['hoje', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                 }
                }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-2 AND CURRENT_DATE()-1 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['ontem', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-3 AND CURRENT_DATE()-2 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['à 2 dias', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-4 AND CURRENT_DATE()-3 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['à 3 dias', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-5 AND CURRENT_DATE()-4 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['à 4 dias', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-6 AND CURRENT_DATE()-5 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['à 5 dias', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            $sql = "SELECT SUM(comum) as comum, SUM(incomum) as incomum, SUM(raro) as raro, SUM(chaves) as chaves  FROM lm_lancamentos WHERE guilda=? and data BETWEEN CURRENT_DATE()-7 AND CURRENT_DATE()-6 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_SESSION['lm_guilda']);
            $consulta->execute();
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                if (!is_null($gf[0])){
                    echo "['à 6 dias', " . $gf['comum'] . ", " . $gf['incomum'] . ", " . $gf['raro'] . ", " . $gf['chaves'] . "]," . "\n";
                }
            }
            ?>
        ]);

        var options = {
            title: 'Semana',
            curveType: 'function',
            chartArea: { left: 10, top: 20}
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart1'));

        chart.draw(data, options);
    }
</script>