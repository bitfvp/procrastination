<?php

//////// insere modal pedindo pra corrigir dados
if ($dados_incompletos==0){ ?>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="dados_encompletos" tabindex="-1" role="dialog" aria-labelledby="modaldadosLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="info-corpo" class="modal-body">
                    <h1 class="text-center">Olá, alteramos as políticas de controle e gestão de dados do sistema.</h1>
                    <h2 class="text-center text-danger">Precisamos que você revise seus dados e complete o que falta de informação.</h2>
                    <h3 class="text-center">Você pode colocar informações por etapas ou editar as informações que se encontrem incorretas.</h3>
                    <h6 class="text-center text-info">Ao final do processo salve informando que o cadastro está completo (<i class="text-warning">Este Cadastro Está Completo:SIM</i>).</h6>
                    <h1 class="text-center"><a href="index.php?pg=Vtroca_dados" class="btn btn-outline-success fas fa-arrow-right">Revisar Dados Pessoais</a></h1>
                </div>
            </div>
        </div>
    </div>

    <script>
        // $('#dados_encompletos').modal('toggle');
        setTimeout(function() {
            $('#dados_encompletos').modal('toggle');
        }, 1000);
    </script>
    <!--fim de modalmsgs-->

<?php }
//exit();




$dia=date("d");
$mes=date("m");

$sql = "SELECT * FROM tbl_users WHERE status=1 and matriz=? and `nascimento` LIKE '%-{$mes}-{$dia}%' ORDER BY nome";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$_SESSION['matriz']);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$aniversarianteslista = $consulta->fetchAll();
$contaniversariantes = $consulta->rowCount();
$sql=null;
$consulta=null;

$meuaniversario=0;
foreach ($aniversarianteslista as $an){
    if ($an['id']==$_SESSION['id']){
        $meuaniversario++;
    }
}
///////////////////////////////////////////////////////
if ($contaniversariantes>0){
    if ($meuaniversario > 0){
        $aniver=fncgetusuario($_SESSION['id']);
        $primeiroNome = explode(" ", $_SESSION["nome"]);
        ?>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="aniversario" tabindex="-1" role="dialog" aria-labelledby="modalaniversarioLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div id="mmsgs" class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                        <h1 class="text-center text-danger">Ei você!! Pensou que não sabiamos??</h1>
                        <h1 class="text-center">A gente lembrou <i class="fa fa-smile-wink"></i></h1>
                        <h1 class="text-center text-warning"><i class="fa fa-birthday-cake fa-3x pulse"></i></h1>
                        <h1 class="text-center"><?php echo Calculo_Idade($aniver['nascimento'])+1;?> aninhos</h1>
                        <h1 class="text-center"><strong>Feliz Aniversário <?php echo $primeiroNome[0];?></strong></h1>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#aniversario').modal('toggle');
        </script>
        <!--fim de modalmsgs-->


    <?php
    }else{
        ?>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="aniversario" tabindex="-1" role="dialog" aria-labelledby="modalaniversarioLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg bg-dark" role="document">
                <div class="modal-content">
                    <div id="mmsgs" class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                        <h1 class="text-center text-info"><i class="fa fa-surprise"></i> Você sabia??</h1>
                        <h2 class="text-center">Tem amigo fazendo aniversário hoje</h2>

                        <?php
                        foreach ($aniversarianteslista as $parabens){
                            $idade=Calculo_Idade($parabens['nascimento'])+1;
                            echo "<h5 class='text-center'>".$parabens['nome']. " (".fncgetprofissao($parabens['profissao'])['profissao'].") <i class='fa fa-birthday-cake text-danger pulse-slow'></i> ".$idade." anos</h5>";
                        }
                        ?>
                        <h1 class="text-center text-info">Aproveite para dar os parabéns <i class="fa fa-grin"></i></h1>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#aniversario').modal('toggle');
        </script>
        <!--fim de modalmsgs-->

        <?php
    }
}else{
    ///modal de msg
    $sql = "SELECT * FROM smem_alertas WHERE status=1 and id=1";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $alerta = $consulta->fetch();
    $contador = $consulta->rowCount();
    $sql=null;
    $consulta=null;

    if ($contador!=0 and $_SESSION['nlog']==1){ ?>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="alerta" tabindex="-1" role="dialog" aria-labelledby="modalalertaLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg bg-dark" role="document">
                <div class="modal-content">
                    <div id="mmsgs" class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                        </button>
                        <h1 class="text-center text-warning"><i class="fa fa-exclamation-triangle"></i> Atenção</h1>
                        <h4 class="text-center"><?php echo $alerta['conteudo'];?></h4>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <?php
                                if ($alerta['imagem']!=""){
                                    echo "<img src='../dados/smem/alertas/".$alerta['imagem']."' alt='' class='img-thumbnail img-fluid'>";
                                }
                                ?>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#alerta').modal('toggle');
        </script>
        <!--fim de modalmsgs-->
        <?php
    }
}







