<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">
    <a class="navbar-brand d-none d-xl-block" href="http://www.manhuacu.mg.gov.br/diario-eletronico" target="_blank" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="click e leia o diário oficial de hoje!">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="<?php echo $env->env__nome; ?>">
    </a>
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/syssocial2.png" alt="<?php echo $env->env_nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">

            </ul>

            <?php
            include_once("{$env->env_root}includes/smem/modal_msg_pontos.php");
            ?>
            <script type="text/javascript">
                function msgoff() {
                    var mico = document.getElementById("msgico");
                    mico.className='fa fa-address-book';
                }
            </script>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div id="qqq" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalPontos">
                            <i class="fa fa-star"></i>
                            <?php echo $ponto; ?>
                        </a>
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <a class="dropdown-item" href ="index.php?pg=Vtroca_dados"><i class="far fa-address-book"></i>&nbsp;Dados Pessoais</a>
                        <a class="dropdown-item" href ="index.php?pg=Vtroca_senha"><i class="fa fa-key"></i>&nbsp;Alterar Senha</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a></li>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
<?php if (true===false){?>
    <a class="navbar-brand d-none d-xl-block" data-toggle="tooltip" data-trigger="click" data-html="true" title="Grande dia! <i class='fas fa-thumbs-up text-warning'></i>">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/patriaamada.png" alt="<?php echo $env->env__nome; ?>">
    </a>
<?php } ?>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <?php
    $_SESSION['nlog']=$_SESSION['nlog']+1;
    if ($_SESSION['nlog']==1){
        ?>
        <script>
            $(function () {
                $('[data-toggle="popover"]').popover();
                $('[data-toggle="popover"]').popover('show');
            })
        </script>
        <?php
    }
    ?>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");