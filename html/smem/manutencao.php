<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


///
try{
    $sql="select * from tbl_suport where id=2";
    global $pdo;
    $manutencao=$pdo->prepare($sql);
    $manutencao->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$manutencao=$manutencao->fetch();
if ($manutencao['apoio']!=1) {
    if ($manutencao['valor'] == 0) {
        header("Location: {$_ENV['ENV_URL']}?pg=Vlogin");
        exit();
    }else{
        if ($manutencao['apoio']!=0){
            header("Location: {$_ENV['ENV_URL']}?pg=Vlogin");
            exit();
        }
    }
}
echo"<META HTTP-EQUIV=REFRESH CONTENT = '600;URL=index.php'>";
?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<style>
    #imagemanu{
        text-align: center;
    }
    #imagemanu img{
        width: 40%;
        /*max-height: 95%;*/
    }
        #titulo{
            color: #000000;
            font-family: sans-serif;
            font-weight: 700;
            font-size: 40px;
            margin-top: 5px;
            margin-bottom: 0;
            word-break: break-word;
            display: block;
            text-align: center;
        }

        #titulo span{
            font-weight: 500;
            font-size: 20px;
            color: #000000;
            margin-top: 30px;
        }

        #titulo h3{
            font-size: 1.17em;
            font-weight: bold;
        }
        #informe{
            display: block;
            text-align: center;
            margin-bottom: 10px;
        }
        #informe p{
            color: #000000;
            font-family: "Poppins", sans-serif;
            font-weight: 400;
            font-size: 1.17em;
            margin-top: 0;
            margin-bottom: 10px;

            line-height: 110%;
        }
        #informe span{
            color: #000000;
            font-weight: 700;
            font-size: 1.50em;
            margin-top: 5px;
            margin-bottom: 10px;
            line-height: 110%;
        }
</style>
<div class="container">
    <div class="row">
        <div class="col-12" id="imagemanu">
            <img src="../public/img/bender.png" alt="maintenance">
        </div>
        <div class="col-12" id="titulo">
            <span>VOLTAREMOS EM BREVE</span>
            <h3 class="pp-infobox-title">O sistema está em manutenção!</h3>
        </div>
        <div class="col-12" id="informe">
            <p>Estamos temporariamente indisponível devido a uma manutenção não planejada.<br>Esperamos concluir a manutenção até meia-noite.<br>Pedimos desculpas por qualquer inconveniente.</p>
            <span class="title-text pp-primary-title">Nos contate por aqui</span>
        </div>
        <div class="offset-2"></div>
        <div class="col-4 text-right">
            <span class="fa-stack text-success" style="vertical-align: top;">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fab fa-whatsapp fa-stack-1x"></i>
            </span>
            <strong>(33)98461-7836</strong>
        </div>
        <div class="col-4 text-left">
            <span class="fa-stack text-info" style="vertical-align: top;">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fas fa-envelope-open-text fa-stack-1x"></i>
            </span>
            <strong>flavioworks@live.com</strong>
        </div>
    </div>
</div>