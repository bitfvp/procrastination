<?php
class Atividadelista{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncatividadelistanew($atividade,$ge){

            //inserção no banco
            try{
                $sql="INSERT INTO smem_at_atlista ";
                $sql.="(id, atividade, ge)";
                $sql.=" VALUES ";
                $sql.="(NULL, :atividade, :ge)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":atividade", $atividade);
                $insere->bindValue(":ge", $ge);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vat_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncatividadelistaedit($id,$atividade,$ge){

        //inserção no banco
        try{
            $sql="UPDATE smem_at_atlista SET atividade=:atividade, ge=:ge WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":atividade", $atividade);
            $insere->bindValue(":ge", $ge);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vat_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>