<?php
function fnc_atlista(){
    $sql = "SELECT * FROM smem_at_atlista where ge=1 ORDER BY atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $atlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $atlista;
}

function fncget_atlista($id){
    $sql = "SELECT * FROM smem_at_atlista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getat = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getat;
}
