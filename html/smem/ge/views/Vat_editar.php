<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        //validação das permissoes
//        if ($allow["admin"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Editar atividade-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="atividadelistasave";
    $atividade=fncgetatividade($_GET['id']);
}else{
    $a="atividadelistanew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vat_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de atividade</h3>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $atividade['id']; ?>"/>
                <label for="atividade">ATIVIDADE:</label>
                <input autocomplete="off" id="atividade" placeholder="atividade" type="text" class="form-control" name="atividade" value="<?php echo $atividade['atividade']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="ge">GE</label>
                <select name="ge" id="ge" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['ge'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['ge'];
                    } ?>">
                        <?php
                        if ($atividade['ge'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['ge'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>


            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>