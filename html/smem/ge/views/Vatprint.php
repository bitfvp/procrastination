<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if (($allow["allow_10"]!=1) and ($allow["allow_18"]!=1) and ($allow["allow_26"]!=1)){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Pasta do Usuário-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<style media=all>
    .hr {
        border-color: #000;
    }
</style>
<main class="container">
    <hr class="hr mb-2 mt-4">
    <h4 class="text-center"><strong>RELATÓRIO GERAL DE ATIVIDADES COM O USUÁRIO</strong></h4>
    <h5 class="mt-4"><strong> I - DADOS PESSOAIS DO USUÁRIO</strong></h5>
    <h6 class="float-left"><strong>Nome: </strong><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Sexo: </strong><?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Nascimento: </strong><?php echo Nascimentoverifica($pessoa['nascimento']);?>&nbsp;&nbsp;&nbsp;
        <?php echo Calculo_Idade($pessoa['nascimento'])." anos";?>&nbsp;&nbsp;&nbsp;
    </h6>
    <h6 class="float-left"><strong>CPF: </strong><?php echo mask($pessoa['cpf'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>RG: </strong><?php echo mask($pessoa['rg'],"###.###.###"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>UF: </strong><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>NIS: </strong><?php echo mask($pessoa['nis'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Pai: </strong><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Mãe: </strong><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;&nbsp;</h6>

    <h5 class="mt-4"><strong> II - ENDEREÇO DO USUÁRIO</strong></h5>
    <h6 class="float-left"><strong>Rua: </strong><?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Nº: </strong><?php echo $pessoa['numero']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Bairro: </strong><?php echo fncgetbairro($pessoa['bairro'])['bairro']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Referência: </strong><?php echo $pessoa['referencia']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Telefone: </strong><?php echo $pessoa['telefone']; ?>&nbsp;&nbsp;&nbsp;</h6>

    <h5 class="mt-4"><strong> III - COMPONENTES DA FAMÍLIA</strong></h5>
    <?php
    $pestemp=$pessoa['cod_familiar'];
    if(isset($pestemp) and $pestemp!=null and $pestemp!=0) {
        //existe um id e se ele é numérico
        $sql = "SELECT * FROM mcu_pessoas WHERE cod_familiar=? order by nome";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $pestemp);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $cfpessoa = $consulta->fetchall();
        $sql = null;
        $consulta = null;

        foreach ($cfpessoa as $item) {
            ?>
            <div class="row border border-dark mx-1 mb-1 px-2 pt-2 pb-0">
                <h6 class="float-left small"><strong>Nome : </strong><?php echo $item['nome']; ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>Sexo: </strong><?php echo fncgetsexo($item['sexo'])['sexo']; ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="small"><strong>Nascimento: </strong><?php echo Nascimentoverifica($item['nascimento']);?>&nbsp;&nbsp;&nbsp;
                    <?php echo Calculo_Idade($item['nascimento'])." anos";?>&nbsp;&nbsp;&nbsp;
                </h6>
                <h6 class="float-left small"><strong>CPF: </strong><?php echo mask($item['cpf'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>RG: </strong><?php echo mask($item['rg'],"###.###.###"); ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>UF: </strong><?php echo $item['uf_rg']; ?>&nbsp;&nbsp;&nbsp;</h6>
            </div>
            <?php
        }
    }
    ?>

    <h5 class="mt-4"><strong> IV - HISTÓRICO DE ATIVIDADES RELACIONADAS AO USUÁRIO</strong></h5>
    <?php
    if (isset($_GET['id'])) {
        $at_idpessoa = $_GET['id'];
        if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
            $sql = "SELECT * \n"
                . "FROM smem_at \n"
                . "WHERE (pessoa=? and status='1')\n"
                . "ORDER BY data DESC, id DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $at_idpessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $ativi = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
        }
    }else{
        echo "erro, contate o admin";
        exit();
    }

    foreach ($ativi as $at) {
        $autoriza_ver=0;
        switch ($at['tipo']){
            case 1:
                if ($allow["allow_10"]==1){$autoriza_ver=1;}
                break;
            case 2:
                if ($allow["allow_18"]==1){$autoriza_ver=1;}
                break;
            case 3:
                if ($allow["allow_26"]==1){$autoriza_ver=1;}
                break;
            case 4:
                if ($allow["allow_42"]==1){$autoriza_ver=1;}
                break;
            default:
                $autoriza_ver=0;
                break;
        }

        if($at['restricao']=="1" or $autoriza_ver!=1){
            if($at['profissional']==$_SESSION['id']){
                $desc = "<i class='fa fa-user-secret'></i> ".$at['descricao'];
            }else{
                $desc = "<i class='fa fa-user-secret'> Confidencial - contate o profissional responsável por essa atividade...</i>";
            }
        }else{
            $desc = $at['descricao'];
        }
        ?>
        <div class="row border border-dark mx-1 mb-1 px-2 pt-2 pb-0">
        <p>
            <i class="fa fa-quote-left fa-sm "></i>
            <strong><?php echo $desc; ?></strong>
            <i class="fa fa-quote-right fa-sm"></i>
        </p>&nbsp;&nbsp;
        <p class="small">
            <strong ><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
            Tipo de atividade:<strong><?php echo fncget_atlista($at['atividade'])['atividade']; ?>&nbsp&nbsp</strong>
            Profissional:<strong>
                <?php
                $us = fncgetusuario($at['profissional']);
                echo $us['nome'];
                echo " (" . fncgetprofissao($us['profissao'])['profissao'] . ")";
                ?>
            </strong>
        </p>
        <?php
        if ($at['id'] != 0) {
            $sql = "SELECT * FROM `smem_at_dados` where atividade='{$at['id']}' ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $dados = $consulta->fetchAll();//$total[0]
            $sql = null;
            $consulta = null;

            foreach ($dados as $dado){
                switch ($dado['extensao']) {
                    case "docx":
                        $infoo="Há um arquivo word em anexo";
                        break;

                    case "doc":
                        $infoo="Há um arquivo word em anexo ";
                        break;

                    case "xls":
                        $infoo="Há um arquivo excel em anexo";
                        break;

                    case "xlsx":
                        $infoo="Há um arquivo excel em anexo ";
                        break;

                    case "pdf":
                        $infoo="Há um arquivo pdf em anexo";
                        break;

                    default:
                        $infoo="Há um arquivo de imagem em anexo";
                        break;
                }
                //
                echo "<div class='row'>";
                echo "<div class='col-md-10'>";
                echo "<h5 class='text-danger'>".$infoo."</h5>";
                echo "</div>";
                echo "</div>";
                //
            }
        } ?>
        </div><?php
    }?>

    <h5 class="text-center mt-4"><strong>INFORMAÇÕES RETIRADAS DO SYSSOCIAL</strong></h5>
    <h5 class="text-center"><strong>ÁS <?php echo date("H:i:s");?> DE <?php echo date("d/m/Y");?></strong></h5>
    <h5 class="text-center"><strong>PELO PROFISSIONAL: <?php echo $_SESSION['nome'];?></strong></h5>

</main>
</body>
</html>