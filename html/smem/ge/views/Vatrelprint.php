<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_10"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}

$page="Meu relatório de atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$profi=$_SESSION['id'];
    $sql = "SELECT smem_at.id, tbl_users.nome AS prof, mcu_pessoas.nome AS pessoa, smem_at_atlista.atividade, smem_at.data, smem_at.descricao, smem_at.profissional\n"
        . "FROM mcu_pessoas INNER JOIN (tbl_users INNER JOIN (smem_at_atlista INNER JOIN smem_at ON smem_at_atlista.id = smem_at.atividade) ON tbl_users.id = smem_at.profissional) ON mcu_pessoas.id = smem_at.pessoa\n"
        . "WHERE (((smem_at.data)>=:inicial) And ((smem_at.data)<=:final) AND ((smem_at.profissional)=:profi) And ((smem_at.tipo)=1) and smem_at.status=1)\n"
        . "ORDER BY tbl_users.nome, smem_at_atlista.atividade, mcu_pessoas.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    $usuario=fncgetusuario($_SESSION['id']);
?>
<div class="container-fluid">
    <h3>Meu relatório de atividades</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
    <h5>Profissional:<?php echo $usuario['nome']." ( ".fncgetprofissao($usuario['profissao'])['profissao'];?> )</h5>
    <?php
    $acont=0;
    $a=$ppp="a";
    echo "<table class='table table-striped table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>PESSOA</td>";
    echo "<td>DATA</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach($ati as $at){

        if(((isset($ppp))and ($ppp!=$at['prof']) and ($acont>0))or((isset($a))and ($a!=$at['atividade'])) and ($acont>0)){

            echo "<tr>";
            echo "<td colspan='2' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
            echo "</tr>";
            $acont=0;
            if ((isset($ppp))and ($ppp!=$at['prof'])){
                echo "<tr>";
                echo "<td colspan='2' class='text-right font-weight-bold'>Total ".$tcont."</td>";
                echo "</tr>";
                $tcont=0;
            }

        }
        if(((isset($ppp))and ($ppp!=$at['prof']))or((isset($a))and ($a!=$at['atividade']))){

            echo "<tr>";
            echo "<td colspan='2' class='font-weight-bold'>";
            echo $at['atividade'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $tcont++;
        $ppp=$at['prof'];
        $a=$at['atividade'];
    }
    echo "<tr>";
    echo "<td colspan='2' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
    echo "</tr>";
    $acont=0;
    echo "<tr>";
    echo "<td colspan='2' class='text-right font-weight-bold'>Total ".$tcont."</td>";
    echo "</tr>";
    $tcont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
<fieldset>
    <div id="piechart" style="width: auto; height: 900px;"></div>
</fieldset>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['Atividade', 'Quant'],
            <?php
            // Recebe
            $inicial = $_POST['data_inicial']." 00:00:01";
            $final = $_POST['data_final']." 23:59:59";
            $profi=$_SESSION['id'];
                $sql = "SELECT\n"
                    . "Count(smem_at.id) AS contadora,\n"
                    . "smem_at_atlista.atividade AS atv\n"
                    . "FROM\n"
                    . "smem_at\n"
                    . "INNER JOIN smem_at_atlista ON smem_at_atlista.id = smem_at.atividade\n"
                    . "WHERE\n"
                    . "smem_at.`data` >= :inicial AND\n"
                    . "smem_at.`data` <= :final AND\n"
                    . "smem_at.profissional = :prof AND smem_at.tipo=1  and smem_at.status=1 \n"
                    . "GROUP BY\n"
                    . "smem_at.atividade\n"
                    . "ORDER BY\n"
                    . "contadora desc";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->bindValue(":prof", $profi);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;

            foreach ($graf as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['atv'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);

        //opções para o gráfico pizza
        var options3 = {
            title: 'Gráfico de atividades por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>