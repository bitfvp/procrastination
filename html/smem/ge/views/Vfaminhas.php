<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"]!=4) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_9"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
        }
    }
}

$page = "Meus Acompanhamentos-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Recebe
                // Captura os dados do cliente solicitado
                $sql = "SELECT * \n"
                    . "FROM mcu_pb_avaliacao \n"
                    . "WHERE ((profissional=?) and (status=1))\n"
                    . "ORDER BY data_ts DESC LIMIT 0,1000";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $_SESSION['id']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mat = $consulta->fetchAll();
                $contar = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Famílias em acompanhamentos - { <?php echo $contar ?> }
                </div>
                <!--popover em hover-->
                <script>
                    $(document).ready(function(){
                        $('[data-toggle="popover"]').popover();
                    });
                </script>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach ($mat as $at) {
                            $status="";
                            if ($at['status']==0){
                                $status="bg-danger-light ";
                            }else{
                                $cor="info";
                                $status=" ";
                            }
                            ?>
                            <hr>
                            <div class="row <?php echo $status; ?>">
                                <div class="col-md-7">
                                    <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                        <?php
                                        if ($at['aa']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PROFISSIONAL QUE FEZ O REGISTRO</strong><br>";
                                        }
                                        if ($at['a1']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PAIF</strong><br>";
                                        }
                                        if ($at['a2']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>É UMA NOVA FAMÍLIA ACOMPANHADA</strong><br>";
                                        }
                                        if ($at['b1']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA ESTÁ EM SITUAÇÃO DE EXTREMA POBREZA</strong><br>";
                                        }
                                        if ($at['b2']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA É BENEFICIÁRIA NO PROGRAMA BOLSA FAMÍLIA</strong><br>";
                                        }
                                        if ($at['b3']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>POSSUI BOLSA FAMÍLIA MAS ESTÁ EM DESCUMPRIMENTO DE CONDICIONALIDADES</strong><br>";
                                        }
                                        if ($at['b4']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI MEMBROS BENEFICIÁRIOS DO BPC</strong><br>";
                                        }
                                        if ($at['b5']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SITUAÇÃO DE TRABALHO INFANTIL</strong><br>";
                                        }
                                        if ($at['b6']==1){
                                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SERVIÇO DE ACOLHIMENTO</strong><br>";
                                        }
                                        ?>
                                        <br>
                                        LANÇADA EM:<strong class="text-info" title=""><?php echo dataBanco2data($at['data_inicial']); ?></strong>
                                        VALIDO ATÉ:<strong class="text-info" title="">
                                            <?php
                                            echo dataBanco2data($at['data_final']); ?>
                                        </strong>
                                    </blockquote>
                                </div>
                                <div class="col-md-5">
                                    <?php
                                    $sql = "SELECT * FROM mcu_pessoas WHERE cod_familiar=?";
                                    global $pdo;
                                    $consulta = $pdo->prepare($sql);
                                    $consulta->bindParam(1, $at['cf']);
                                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                    $cfpessoa = $consulta->fetchall();
                                    $sql=null;
                                    $consulta=null;

                                    foreach ($cfpessoa as $cfp){

                                        echo "<div class='list-group-item'>\n";
                                        $nome = explode(" ", $cfp['nome']);

                                        echo "<div class='dropdown show float-right dropleft'>\n"
                                            ."<a class='badge badge-danger btn-sm' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>\n"
                                            ."<span class='fa fa-times'></span>\n"
                                            ."</a>\n"
                                            ."<div class='dropdown-menu left' aria-labelledby='dropdownMenuLink'>\n"
                                            ."<a class='dropdown-item' href='#'>Manter como está</a>\n"
                                            ."<a class='dropdown-item bg-danger' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}'>Remover da família</a>\n"
                                            ."</div>\n"
                                            ."</div>\n";

                                        echo "<a href='index.php?pg=Vpessoa&id={$cfp['id']}' "
                                            ."class='' "
                                            ."data-toggle='popover' "
                                            ."data-placement='top' "
                                            ."title='{$cfp['nome']}' "
                                            ."data-html='true' "
                                            ."data-trigger='hover' "
                                            ."data-content='"
                                            .dataBanco2data ($cfp['nascimento']). " idade:" .Calculo_Idade($cfp['nascimento'])." ano(s)<br>"
                                            ."CPF:".$cfp['cpf']."<br>"
                                            ."RG:".$cfp['rg']."<br>"
                                            ."' >\n";

                                        echo "    {$nome[0]} </a>\n";
//                                        echo "    {$cfp['nome']} </a>";

                                        echo "</div>\n";
                                    }
                                    ?>
                                </div>
                            </div>
                            <br>
                        <?php } ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>