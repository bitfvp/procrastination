<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}
$page="Pasta do usuário-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-9">

            <?php
            // Recebe
                if (!empty($pessoa['cod_familiar'])) {
                    // Captura os dados do cliente solicitado
                        $sql = "SELECT * \n"
                        . "FROM mcu_pb_avaliacao \n"
                        . "WHERE (((mcu_pb_avaliacao.cf)=?))\n"
                        . "ORDER BY mcu_pb_avaliacao.data_ts DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $pessoa['cod_familiar']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $atividades = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Avaliações da família
                </div>
                <div class="card-body">
                    <!--apenas pra funcao funcionar e o botao ficar translucido-->
                        <?php
                        foreach ($atividades as $at) {
                            $status="";
                            if ($at['status']==0){
                                $status="bg-danger-light ";
                                $cor="dark";
                            }else{
                                $status=" ";
                                $cor="info";
                            }
                            ?>
                    <h6>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $cor." ".$status; ?>">
                                    <?php
                                    if ($at['aa']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PROFISSIONAL QUE FEZ O REGISTRO</strong><br>";
                                    }
                                    if ($at['a1']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PAIF</strong><br>";
                                    }
                                    if ($at['a2']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>É UMA NOVA FAMÍLIA ACOMPANHADA</strong><br>";
                                    }
                                    if ($at['b1']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA ESTÁ EM SITUAÇÃO DE EXTREMA POBREZA</strong><br>";
                                    }
                                    if ($at['b2']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA É BENEFICIÁRIA NO PROGRAMA BOLSA FAMÍLIA</strong><br>";
                                    }
                                    if ($at['b3']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>POSSUI BOLSA FAMÍLIA MAS ESTÁ EM DESCUMPRIMENTO DE CONDICIONALIDADES</strong><br>";
                                    }
                                    if ($at['b4']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI MEMBROS BENEFICIÁRIOS DO BPC</strong><br>";
                                    }
                                    if ($at['b5']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SITUAÇÃO DE TRABALHO INFANTIL</strong><br>";
                                    }
                                    if ($at['b6']==1){
                                        echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SERVIÇO DE ACOLHIMENTO</strong><br>";
                                    }
                                    ?>
                                <br>
                                LANÇADA EM:<strong class="text-info" title=""><?php echo dataBanco2data($at['data_inicial']); ?></strong>
                                VALIDO ATÉ:<strong class="text-info" title="">
                                    <?php
                                    echo dataBanco2data($at['data_final']); ?>
                                </strong>
<!--                                <span class="badge badge-pill badge-warning float-right"><strong>--><?php //echo $at['id']; ?><!--</strong></span>-->
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                    </h6>
                        <?php
                        }
                        ?>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>