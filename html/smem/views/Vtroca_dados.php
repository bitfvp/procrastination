<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}
$page="Alterar Dados-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

    $a="usuariosave2";
    $us_er=fncgetusuario($_SESSION['id']);
?>


<main class="container">

    <form class="frmgrid" action="index.php?pg=Vtroca_dados&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="col">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block" />
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <label  class="x-small" for="status">Este Cadastro Está Completo:</label>
                <select name="completo" id="completo" class="form-control" >
                    // vamos criar a visualização de rf
                    <option selected="" value="<?php if($us_er['completo']==""){$z=0; echo $z;}else{ echo $us_er['completo'];} ?>">
                        <?php
                        if($us_er['completo']==0){echo"Não";}
                        if($us_er['completo']==1){echo"Sim";} ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
                <hr class="bg-danger">
            </div>

        </div>

        <div class="row">

            <div class="col-md-4">
                <label  class="large" for="nome">Nome:</label>
                <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required/>
            </div>


            <div class="col-md-3">
                <label  class="large" for="">Sexo:</label>
                <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                    <option selected="" value="<?php if ($us_er['sexo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $us_er['sexo'];
                    } ?>">
                        <?php
                        if ($us_er['sexo'] == 0) {
                            echo "Selecione...";
                        }
                        if ($us_er['sexo'] == 1) {
                            echo "Feminino";
                        }
                        if ($us_er['sexo'] == 2) {
                            echo "Masculino";
                        }
                        if ($us_er['sexo'] == 3) {
                            echo "Indefinido";
                        }
                        ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Feminino</option>
                    <option value="2">Masculino</option>
                    <option value="3">Indefinido</option>
                </select>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">Nascimento:</label>
                <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required/>
            </div>

            <div class="col-md-4">
                <label class="large" for="">Pai:</label>
                <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $us_er['pai']; ?>"/>
            </div>

            <div class="col-md-4">
                <label class="large" for="">Mãe:</label>
                <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $us_er['mae']; ?>"/>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">CPF:</label>
                <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label for="rg">RG:</label>
                <input autocomplete="off" id="rg" type="text" class="form-control" name="rg" value="<?php echo $us_er['rg']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#rg').mask('00.000.000.000', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label class="large" for="">Data de Expedição:</label>
                <input name="rg_expedicao" id="rg_expedicao" type="date" class="form-control" value="<?php echo $us_er['rg_expedicao']; ?>"/>
            </div>

            <div class="col-md-2">
                <label class="large" for="">Orgão Emissor:</label>
                <input autocomplete="off" id="rg_emissor" type="text" class="form-control" name="rg_emissor" value="<?php echo $us_er['rg_emissor']; ?>"/>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">PIS/PASEP:</label>
                <input name="pis" id="pis" type="text" class="form-control" value="<?php echo $us_er['pis']; ?>" />
                <script>
                    $(document).ready(function(){
                        $('#pis').mask('000.00000.00-0', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label  class="large" for="">Título de Eleitor:</label>
                <input name="titulo" id="titulo" type="text" class="form-control" value="<?php echo $us_er['titulo']; ?>" />
                <script>
                    $(document).ready(function(){
                        $('#titulo').mask('000000000000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label class="large" for="">Zona Eleitoral:</label>
                <input autocomplete="off" id="zona" type="text" class="form-control" name="zona" value="<?php echo $us_er['zona']; ?>"/>
            </div>

            <div class="col-md-3">
                <label class="large" for="">Seção Eleitoral:</label>
                <input autocomplete="off" id="secao" type="text" class="form-control" name="secao" value="<?php echo $us_er['secao']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="endereco">Endereço:</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $us_er['endereco']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="bairro">Bairro:</label>
                <input autocomplete="off" id="bairro" type="text" class="form-control" name="bairro" value="<?php echo $us_er['bairro']; ?>"/>
            </div>

            <div class="col-md-2">
                <label  class="large" for="">CEP:</label>
                <input name="cep" id="cep" type="text" class="form-control" value="<?php echo $us_er['cep']; ?>" />
                <script>
                    $(document).ready(function(){
                        $('#cep').mask('00000-000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-5">
                <label for="telefone1">Telefone:</label>
                <input autocomplete="off" id="telefone1" type="tel" class="form-control" name="telefone1" value="<?php echo $us_er['telefone1']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone1').mask('(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-5">
                <label for="telefone2">Telefone(Opcional):</label>
                <input autocomplete="off" id="telefone2" type="tel" class="form-control" name="telefone2" value="<?php echo $us_er['telefone2']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone2').mask('(00)0000-0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-5">
                <label for="escolaridade">Escolaridade:</label>
                <input autocomplete="off" id="escolaridade" type="text" class="form-control" name="escolaridade" value="<?php echo $us_er['escolaridade']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="estado_civil">Estado Civil:</label>
                <input autocomplete="off" id="estado_civil" type="text" class="form-control" name="estado_civil" value="<?php echo $us_er['estado_civil']; ?>"/>
            </div>

            <div class="col-md-3">
                <label>Cargo:</label>
                <select name="profissao" id="profissao" class="form-control" required>
                    <?php
                    $getprofissao=fncgetprofissao($us_er['profissao']);
                    ?>
                    <option selected="" value="<?php echo $us_er['profissao']; ?>">
                        <?php echo $getprofissao['profissao'];?>
                    </option>
                    <?php
                    $profissaolista=fncprofissaolist();
                    foreach ($profissaolista as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['profissao'];?>" value="<?php echo $item['id'];?>">
                            <?php echo $item['profissao']; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-3">
                <label for="setor">Setor Pertencente:</label>
                <input autocomplete="off" id="setor" type="text" class="form-control" name="setor" value="<?php echo $us_er['setor']; ?>"/>
            </div>

            <div class="col-md-3">
                <label class="large" for="">Data da Admissão:</label>
                <input name="admissao" id="admissao" type="date" class="form-control" value="<?php echo $us_er['admissao']; ?>"/>
            </div>

            <div class="col-md-3">
                <label class="large" for="">Fim do Exercício:</label>
                <input name="fim_exercicio" id="fim_exercicio" type="date" class="form-control" value="<?php echo $us_er['fim_exercicio']; ?>"/>
            </div>


        </div>

    </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>