<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}
////////////////////////////////////////////////////////////////////////////////////////////

$nn=$_GET['term'];
$pp=$_GET['pessoa'];

    $sql = "SELECT COUNT(`id`) FROM tbl_users WHERE nick='{$nn}' and id<>'{$pp}' ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $lipessoa = $consulta->fetch();
    $lipessoa = $lipessoa[0];
    $sql=null;
    $consulta=null;

    if ($lipessoa>0){
       echo "<br><i class='badge badge-danger'>Já existe</i>";
    }else{
        echo "<br><i class='badge badge-success'>Disponível</i>";
    }

