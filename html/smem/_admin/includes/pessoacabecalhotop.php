<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetusuario($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
  <h3>DADOS DO PROFISSIONAL</h3>
  <blockquote class="blockquote blockquote-info">
  <header>
      NOME:
      <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
  </header>
      <h6>
          NICK:
          <strong class="text-info"><?php echo $pessoa['nick']; ?>&nbsp;&nbsp;</strong>
          E-MAIL:
          <strong class="text-info"><?php echo $pessoa['email']; ?>&nbsp;&nbsp;</strong>
          SEXO:
          <strong class="text-info"><?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?></strong>
          NASCIMENTO:
          <strong class="text-info"><?php
              if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['nascimento']);
                  echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
          PAI:
          <strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>
          MÃE:
          <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;</strong>
          CPF:
          <strong class="text-info"><?php
              if($pessoa['cpf']!="") {
                  echo "<span class='text-info'>";
                  echo mask($pessoa['cpf'],'###.###.###-##');
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>
          RG:
          <strong class="text-info"><?php
              if($pessoa['rg']!="") {
                  echo "<span class='text-info'>";
                  echo mask($pessoa['rg'],'###.###.###');
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          DATA DE EXPEDIÇÃO:
          <strong class="text-info"><?php
              if($pessoa['rg_expedicao']!="1900-01-01" and $pessoa['rg_expedicao']!="" and $pessoa['rg_expedicao']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['rg_expedicao']);
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>

          ORGÃO EMISSOR:
          <strong class="text-info"><?php echo $pessoa['rg_emissor']; ?>&nbsp;&nbsp;</strong>

          PIS/PASEP:
          <strong class="text-info"><?php
              if($pessoa['pis']!="") {
                  echo "<span class='text-info'>";
                  echo mask($pessoa['pis'],'###.#####-##-#');
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          Titulo de Eleitor:
          <strong class="text-info"><?php echo $pessoa['titulo']; ?>&nbsp;&nbsp;</strong>

          Zona Eleitoral:
          <strong class="text-info"><?php echo $pessoa['zona']; ?>&nbsp;&nbsp;</strong>

          Seção Eleitoral:
          <strong class="text-info"><?php echo $pessoa['zona']; ?>&nbsp;&nbsp;</strong>

          ENDEREÇO:
          <strong class="text-info"><?php
              if($pessoa['endereco']!=""){
                  echo "<span class='azul'>";
                  echo $pessoa['endereco'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>&nbsp;&nbsp;

          BAIRRO:
          <strong class="text-info"><?php
              if($pessoa['bairro']!=""){
                  echo "<span class='azul'>";
                  echo $pessoa['bairro'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>&nbsp;&nbsp;

          CEP:
          <strong class="text-info"><?php
              if($pessoa['cep']!="") {
                  echo "<span class='text-info'>";
                  echo mask($pessoa['cep'],'#####-###');
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          MUNICÍPIO:
          <strong class="text-info"><?php
              if($pessoa['cep']!="") {
                  $municipio=get_enderecobycep($pessoa['cep']);
                  echo "<span class='text-info'>";
                  echo $municipio->localidade;
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          TELEFONE:
          <strong class="text-info"><?php
              if($pessoa['telefone1']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['telefone1'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          TELEFONE SECUNDÁRIO:
          <strong class="text-info"><?php
              if($pessoa['telefone2']!="") {
                  echo "<span class='text-info'>";
                  echo $pessoa['telefone2'];
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?></strong>

          escolaridade:
          <strong class="text-info"><?php echo $pessoa['escolaridade']; ?>&nbsp;&nbsp;</strong>

          Estado Civil:
          <strong class="text-info"><?php echo $pessoa['estado_civil']; ?>&nbsp;&nbsp;</strong>

          PROFISSÃO:
          <strong class="text-info"><?php
            if ($pessoa['profissao'] != "0") {
                echo fncgetprofissao($pessoa['profissao'])['profissao'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>
          </strong>&nbsp;&nbsp;

          Setor Pertencente:
          <strong class="text-info"><?php echo $pessoa['setor']; ?>&nbsp;&nbsp;</strong>

          Data da Admissão:
          <strong class="text-info"><?php echo dataBanco2data ($pessoa['admissao']); ?>&nbsp;&nbsp;</strong>

          Fim do Exercicio:
          <strong class="text-info"><?php echo dataBanco2data ($pessoa['fim_exercicio']); ?>&nbsp;&nbsp;</strong>



    </h6>
    <footer class="blockquote-footer">
            Mantenha atualizado</strong>&nbsp;&nbsp;
    </footer>
  </blockquote>
</div>
        <a class="btn btn-success btn-block" href="?pg=Valloweditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR USUÁRIO
        </a>
