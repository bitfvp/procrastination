<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=4){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Contato-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="contatosave";
    $contato=fncgetcontato($_GET['id']);
}else{
    $a="contatonew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vhome&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $contato['id']; ?>"/>
                <label for="nome">Nome</label><input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $contato['nome']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="telefone">Numero de Telefone</label>
                <input minlength="10" min="10" autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $contato['telefone']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#telefone').mask('(00)00000-0000', {reverse: false});
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>