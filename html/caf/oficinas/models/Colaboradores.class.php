<?php
class Livro{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnclivronew($livro,$registro,$acervo,$autor,$classificacao){
        $livro=str_replace("#", "", $livro);
            //inserção no banco
            try{
                $sql="INSERT INTO mcu_biblioteca_livro ";
                $sql.="(id, livro, registro, acervo, autor, classificacao)";
                $sql.=" VALUES ";
                $sql.="(NULL, :livro, :registro, :acervo, :autor, :classificacao)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":livro", $livro);
                $insere->bindValue(":registro", $registro);
                $insere->bindValue(":acervo", $acervo);
                $insere->bindValue(":autor", $autor);
                $insere->bindValue(":classificacao", $classificacao);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vlivro_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
        
    }

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnclivroedit($id,$livro,$registro,$acervo,$autor,$classificacao){
        $livro=str_replace("#", "", $livro);
        //inserção no banco
        try{
            $sql="UPDATE mcu_biblioteca_livro SET livro=:livro, registro=:registro,  acervo=:acervo, autor=:autor, classificacao=:classificacao WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":livro", $livro);
            $up->bindValue(":registro", $registro);
            $up->bindValue(":acervo", $acervo);
            $up->bindValue(":autor", $autor);
            $up->bindValue(":classificacao", $classificacao);
            $up->bindValue(":id", $id);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro errado '. $error_msg->getMessage();
        }

        if(isset($up)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vlivro_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>