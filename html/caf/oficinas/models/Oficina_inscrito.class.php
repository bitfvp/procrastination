<?php

class Oficina_inscrito
{

    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function addinscrito($cod_oficina,$cod_pessoa,$cod_usuario)
    {

        //ver se ha ja ha a inscricao
        try{
            $sql="SELECT id FROM ";
            $sql.="caf_oficina_inscrito";
            $sql.=" WHERE cod_oficina=:cod_oficina and cod_pessoa=:cod_pessoa";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":cod_oficina", $cod_oficina);
            $consulta->bindValue(":cod_pessoa", $cod_pessoa);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();

        if ($contar==0) {
            try {
                $sql = "INSERT INTO caf_oficina_inscrito (`id`, `cod_oficina`, `cod_pessoa`, `cod_usuario`)"
                    . " VALUES(NULL, :cod_oficina, :cod_pessoa, :cod_usuario)";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":cod_oficina", $cod_oficina);
                $insere->bindValue(":cod_pessoa", $cod_pessoa);
                $insere->bindValue(":cod_usuario", $cod_usuario);
                $insere->execute();
            } catch (PDOException $error_msg) {
                echo 'Erroff' . $error_msg->getMessage();
            }

            if (isset($insere)) {
                //////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////////////

                $_SESSION['fsh']=[
                    "flash"=>"Adicionada a Oficina!!",
                    "type"=>"success",
                ];
                header("Location: index.php?pg=Voficina&id={$_GET['id']}");
                exit();

            } else {
                if (empty($_SESSION['fsh'])) {
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];

                }
            }

        }else{
            $_SESSION['fsh']=[
                "flash"=>"Essa pessoa já esta inscrita na oficina!!",
                "type"=>"danger",
            ];
            header("Location: index.php?pg=Voficina&id={$_GET['id']}");
            exit();
        }

    }



    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncexcinscrito($id_inscricao)
    {

        try {
            $sql = "DELETE FROM `caf_oficina_inscrito` WHERE id = :id ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":id", $id_inscricao);
            $exclui->execute();
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        if (isset($exclui)) {
            //////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Membro Removido!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Voficina&id={$_GET['id']}");
            exit();

        } else {
            if (empty($_SESSION['fsh'])) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}

?>