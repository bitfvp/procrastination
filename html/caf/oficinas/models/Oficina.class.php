<?php 
class Oficina{
    public function fncoficinanew($status, $oficina, $oficineiro, $cod_usuario){

            //inserção no banco
            try{
                $sql="INSERT INTO caf_oficina (`id`, `status`, `oficina`, `oficineiro`, `cod_usuario`)"
                                ." VALUES(NULL, :status, :oficina, :oficineiro, :cod_usuario)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":status",$status);
                $insere->bindValue(":oficina",$oficina);
                $insere->bindValue(":oficineiro",$oficineiro);
                $insere->bindValue(":cod_usuario",$cod_usuario);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="caf_oficina";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;
            $maid=$mid[0];
            header("Location: index.php?pg=Voficina&id={$maid}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncoficinaedit($id, $status, $oficina, $oficineiro){
        try{
            $sql="SELECT id FROM ";
            $sql.="caf_oficina";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //inserção no banco
            try{
                $sql="UPDATE caf_oficina SET "
                    ."oficina=:oficina, oficineiro=:oficineiro, "
                    ."status=:status "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":oficina",$oficina);
                $update->bindValue(":oficineiro",$oficineiro);
                $update->bindValue(":status",$status);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa oficina cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Voficina&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao


}//fim class
?>