<div class="card mb-2 mt-3">
    <div class="card-header bg-info text-light">
        Envio de fotos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Vpessoa&aca=novafoto&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="titular" type="file" class="custom-file-input" name="titular[]" value="" multiple/>
                        <label class="custom-file-label" for="titular">Selecione as fotos...</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="ENVIAR FOTOS"/>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Envio de arquivos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Vpessoa&aca=novoarquivo&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="arquivos" type="file" class="custom-file-input" name="arquivos[]" value="" multiple/>
                        <label class="custom-file-label" for="arquivos">Selecione os arquivos...</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="ENVIAR ARQUIVOS"/>
                </div>
            </div>
        </form>
    </div>
</div>