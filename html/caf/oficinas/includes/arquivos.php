<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Arquivos de documentos
    </div>
    <div class="card-body">
            <blockquote class="blockquote blockquote-info">
                      <?php
                      if ($pessoa['id']!=0){
                          ?>
                            <div class="row">
                              <?php
                              $files = glob("../../dados/caf/pessoas/" . $pessoa['id'] . "/arquivos/*.*");
                              for ($i = 0; $i < count($files); $i++) {
                                  $num = $files[$i];
                                  echo "<div class='col-md-6'>
                                <a href=" . $num . " target='_blank'>
                                    <img src=" . $num . " alt='...' class='img-fluid img-thumbnail'>
                                </a>
                            </div>";
                              }
                              ?>
                          </div>
                          <?php
                      }
                      ?>
                <footer class="blockquote-footer text-danger">
                    Click no arquivo pra acessar
                </footer>
            </blockquote>
        </div>
    </div>