<?php
include_once ("{$env->env_root}includes/caf/busca_pessoa.php");
?>

<div class="container">
    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">NASCIMENTO</th>
            <th scope="col">CPF</th>
            <th scope="col">RG</th>
            <th scope="col">Status</th>
            <th scope="col">EDITAR</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $todos->rowCount();?> pessoa(s) listada(s)</th>
        </tr>
        </tfoot>

        <?php
        if(isset($_GET['sca']) and $_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        if (isset($_GET['scb']) and $_GET['scb'] != "") {
            $stb = strtoupper($_GET['scb']);
            define('CSB', $stb);//TESTE
        }
        if (isset($_GET['scc']) and $_GET['scc'] != "") {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);//TESTE
        }
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
        $id = $dados["id"];
        $nome = strtoupper($dados["nome"]);
        $nascimento = dataBanco2data ($dados["nascimento"]);
        $cpf = $dados["cpf"];
        $rg = $dados["rg"];
        $cod_familiar=$dados["cod_familiar"];
        if ($dados["status"]==1){
            $status = "<strong class='text-success'>Apto para oficinas</strong>";
        }else{
            $status = "<strong class='text-danger'>Não relacionado</strong>";
        }
        ?>
        <tbody>
        <tr>
            <th scope="row" id="<?php echo $id;  ?>">
                <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                    <?php
                    if(isset($_GET['sca']) and $_GET['sca']!="") {
                        $sta = CSA;
                        $nnn = $nome;
                        $nn = explode(CSA, $nnn);
                        $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                        echo $n;
                    }else{
                        echo $nome;
                    }
                    ?>
                </a>
            </th>
            <td>
                <?php
                if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                    echo $nascimento;
                }else{
                    echo "<span class='text-danger'>--/--/----</span>";
                }
                ?>
            </td>
            <td>
                <?php
                if($cpf!="0" and $cpf!="") {
                    if (isset($_GET['scb']) and $_GET['scb'] != "") {
                        $stb = CSB;
                        $ccc = $cpf;
                        $cc = explode(CSB, $ccc);
                        $c = implode("<span class='text-danger'>{$stb}</span>", $cc);
                        echo $c;
                    } else {
                        echo mask($cpf,'###.###.###_##');
                    }
                }else{
                    echo "--- --- --- --";
                }
                ?></td>
            <td>
                <?php
                if($rg!="0" and $rg!="") {
                    if (isset($_GET['scc']) and $_GET['scc'] != "") {
                        $stc = CSC;
                        $rrr = $rg;
                        $rr = explode(CSC, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $rg;
                    }
                }else{
                    echo "--- --- ---";
                }
                ?></td>
            <td>
                <?php
                    echo $status;
                ?>
            </td>
            <td>
                <?php
                if ($allow["admin"]==1){ ?>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa">
                        Alterar
                    </a>
                    <?php
                }else{
                    echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                }
                ?>
            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</div>
