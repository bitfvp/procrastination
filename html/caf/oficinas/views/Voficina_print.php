<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Oficina-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");


if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $oficina=fncgetoficina($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">

    <h3><?php echo $oficina['oficina'];?></h3>
    Oficineiro:<strong><?php echo $oficina['oficineiro'];?></strong><br>
    Cadastrada em:<strong><?php echo dataRetiraHora($oficina['data_cadastro']);?></strong><br>
    Cadastrada por:<strong><?php echo fncgetusuario($oficina['cod_usuario'])['nome'];?></strong><br><br>
    Status:<strong>
        <?php
        if ($oficina['status']==1){
            echo "<strong class='green-text'>Oficina Ativa</strong>";
        }else{
            echo "<strong class='red-text'>Oficina Desativada</strong>";
        }
        ?>
    </strong>

    <hr>
    <?php
    $sql = "SELECT * "
        ."FROM "
        ."caf_oficina_inscrito "
        ."INNER JOIN caf_pessoas ON caf_pessoas.id = caf_oficina_inscrito.cod_pessoa "
        ."WHERE cod_oficina=? "
        ."ORDER BY "
        ."caf_pessoas.nome ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $oficina['id']);
    $consulta->execute();
    $inscritos = $consulta->fetchall();
    $inscritos_count = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    ?>

    <h6><?php echo $inscritos_count." inscrito(s) nessa oficina";?></h6>
    <ul>
        <?php
        $c=1;
        foreach ($inscritos as $item){
            //
            echo "<li>";
            echo $c++."&nbsp;".fncgetpessoa($item['cod_pessoa'])['nome'];
            echo "&nbsp;&nbsp;";
            echo dataBanco2data(fncgetpessoa($item['cod_pessoa'])['nascimento']);
            echo "</li>";
        }
        ?>
    </ul>

</main>
</body>
</html>