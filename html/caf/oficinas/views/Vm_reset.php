<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Crianças com cadastro ativo-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
    <main class="container">
        <div class="row">
            <div class="col-md-8 offset-2">
                <h3 class="text-danger">Desativar Cadastros para Inicio de temporada</h3>
                <h5>Na data estipulada pela cordenação, todos os cadastros de crianças e adolescentes será desativado e aguardaram atualização e re-matricula para voltar a frequentar as oficinas.  </h5>
                <h6 class="text-info">Deseja Desativar os cadastros agora?</h6>
                <a class="btn btn-lg btn-success" href="index.php">Cancelar</a>
                <div class="dropdown show float-left pr-1">
                    <a class="btn btn-lg btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Desativar cadastros
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Não</a>
                        <a class="dropdown-item bg-danger" href="index.php?aca=cadastrosdesativar">Desativar Realmente??</a>
                    </div>
                </div>
            </div>
        </div>
    </main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>