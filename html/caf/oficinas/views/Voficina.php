<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Oficina-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $oficina=fncgetoficina($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-info text-light">
                    <?php echo $oficina['oficina'];?>
                </div>
                <div class="card-body">
                    Oficineiro: <strong><?php echo $oficina['oficineiro'];?></strong><br>
                    Cadastrada em: <strong><?php echo dataRetiraHora($oficina['data_cadastro']);?></strong><br>
                    Cadastrada por: <strong><?php echo fncgetusuario($oficina['cod_usuario'])['nome'];?></strong><br><br>
                    Status: <strong>
                        <?php
                        if ($oficina['status']==1){
                            echo "<strong class='text-success'>Oficina Ativa</strong>";
                        }else{
                            echo "<strong class='text-danger'>Oficina Desativada</strong>";
                        }
                        ?>
                    </strong>
                    <a class="btn btn-outline-primary btn-block" href="index.php?pg=Voficina_print&id=<?php echo $_GET['id'];?>" target="_blank" title="">
                        IMPRIMIR OFICINA
                    </a>
                    <a class="btn btn-success btn-block" href="index.php?pg=Voficina_editar&id=<?php echo $oficina['id']?>" title="Alterar dados da oficina">
                        EDITAR OFICINA
                    </a>
                </div>
            </div>
        </div>

        <?php
        $sql = "SELECT * FROM caf_oficina_inscrito WHERE cod_oficina=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $oficina['id']);
        $consulta->execute();
        $inscritos = $consulta->fetchall();
        $inscritos_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        ?>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#serch1 input[type="text"]').on("keyup input", function () {
                    /* Get input value on change */
                    var inputVal = $(this).val();
                    var resultDropdown = $(this).siblings(".result");
                    if (inputVal.length) {
                        $.get("<?php echo $env->env_url_mod;?>includes/oficina_add_inscrito.php", {term: inputVal,cod_oficina: "<?php echo $oficina['id'];?>", pg: "<?php echo $_GET['pg'];?>"} ).done(function (data) {
                            // Display the returned data in browser
                            resultDropdown.html(data);
                        });
                    } else {
                        resultDropdown.empty();
                    }
                });

                // Set search input value on click of result item
                $(document).on("click", ".result p", function () {
                    $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
                    $(this).parent(".result").empty();
                });
            });
        </script>

        <div class="col-md-7" id="serch1">
            <?php
            if ($oficina['status']==1){
            echo "<input type='text' class='form-control input-sm' id='admembro' placeholder='Adicionar Criança na Oficina'>";
            }
            ?>
            <div class="result mt-2"></div>
            <h6 class="text-info mt-2">
                <?php echo $inscritos_count." inscrito(s) nessa oficina";?>
            </h6>
            <div class="list-group">
                <?php
                foreach ($inscritos as $item){
                    //
                    echo "<div class='list-group-item'>";
                    echo "<a href='index.php?pg=Vpessoa&id=".fncgetpessoa($item['cod_pessoa'])['id']."'>".fncgetpessoa($item['cod_pessoa'])['nome']."</a>";
                    if ($oficina['status']==1) {
                        echo "<a class='badge badge-danger float-right' data-position='top' data-tooltip='Remover pessoa da oficina' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=excloficina&membro={$item['id']}'>";
                        echo "<span class='fa fa-times'>";
                        echo "</a>";
                    }
                    echo "</div>";
                }
                ?>
            </div>
        </div>

    </div>
</main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>