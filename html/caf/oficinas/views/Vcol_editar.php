<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar Livro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
    <div class="row">
            <div class="col-md-6">
                <?php
                if ($allow["admin"]==1){ ?>
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
                    <?php
                }
                ?>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
            <label for="nome">NOME</label><input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="sexo">SEXO</label>
            <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['sexo'];
                } ?>">
                    <?php
                    if ($pessoa['sexo'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['sexo'] == 1) {
                        echo "Feminino";
                    }
                    if ($pessoa['sexo'] == 2) {
                        echo "Masculino";
                    }
                    if ($pessoa['sexo'] == 3) {
                        echo "Indefinido";
                    }
                    ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Feminino</option>
                <option value="2">Masculino</option>
                <option value="3">Indefinido</option>
            </select>

        </div>
        <div class="col-md-3">
            <label for="nascimento">NASCIMENTO</label>
            <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <label for="cpf">CPF</label>
            <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#cpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
        </div>
        <div class="col-md-5">
            <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                             value="<?php echo $pessoa['rg']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#rg').mask('00.000.000.000', {reverse: true});
                });
            </script>
        </div>
        <div class="col-md-2">
            <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                   value="<?php echo $pessoa['uf_rg']; ?>"/>
        </div>

    </div>


    <hr>

    <div class="row">
        <div class="col-md-5">
            <label for="endereco">ENDEREÇO</label>
            <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="numero">NÚMERO</label>
            <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="bairro">BAIRRO</label>
            <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php
                $bairroid = $pessoa['bairro'];
                $getbairro=fncgetbairro($bairroid);
                ?>
                <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $pessoa['bairro']; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                foreach (fncbairrolist() as $item) {
                    ?>
                <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                    <?php echo $item['bairro']; ?>
                </option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <label for="referencia">REFERÊNCIA</label>
            <input autocomplete="off" id="referencia" type="text" class="form-control" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
        </div>
        <div class="col-md-5">
            <label for="telefone">TELEFONE</label>
            <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                });
            </script>
        </div>
    </div>
        <hr>
    <div class="row">
        <div class="col-md-4">
            <label for="escolaridade">ESCOLARIDADE</label>
            <select name="escolaridade" id="escolaridade" class="form-control">// vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['escolaridade'] == "") {
                                                    $z = 0;
                                                    echo $z;
                                                } else {
                                                    echo $pessoa['escolaridade'];
                                                } ?>">
                    <?php
                    switch ($pessoa['escolaridade']){
                        case 0:
                            echo "indefinido";
                            break;
                        case 1:
                            echo "Não alfabetizado";
                            break;
                        case 2:
                            echo "fundamental";
                            break;
                        case 3:
                            echo "medio";
                            break;
                    }
                    ?>
                </option>
                <option value="0">indefinido</option>
                <option value="1">Não alfabetizado</option>
                <option value="2">fundamental</option>
                <option value="3">medio</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="residencia">TIPO DE REDIDÊNCIA</label>
            <select name="residencia" id="residencia" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if (isset($pessoa['residencia']) and $pessoa['residencia']>0 and $pessoa['residencia']<3){echo $pessoa['residencia'];}else{echo 0;} ?>">
                    <?php
                    if (isset($pessoa['residencia'])){
                        if ($pessoa['residencia']==0){
                            echo "Selecione";
                        }
                        if ($pessoa['residencia']==1){
                            echo "Própria";
                        }
                        if ($pessoa['residencia']==2){
                            echo "Alugada";
                        }
                    }else{
                        echo "Selecione";
                    }
                    ?>
                </option>
                <option value="1">Própria </option>
                <option value="2">Alugada</option>
            </select>

        </div>
    </div>

    </form>
</main>


<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>