<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();

        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-8">
            <?php
            include_once("{$env->env_root}includes/caf/pessoacabecalhotop.php");
            include_once("includes/fotos.php");
            include_once("includes/arquivos.php");
            ?>

        </div>
        <div class="col-md-4">
            <?php
            include_once("includes/sectionmenulateral.php");
            include_once ("includes/arquivosform.php");
            ?>
        </div>

    </div>

    <div class="row">
    </div>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>