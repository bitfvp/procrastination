<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Crianças com cadastro ativo-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

try{
    $sql="SELECT * FROM ";
    $sql.="caf_pessoas ";
    $sql.="WHERE status=1 order by nome ";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->execute();
    $arrr = $consulta->fetchAll();
    $cont=$consulta->rowCount();
    $sql=null;
    $consulta=null;
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
?>
    <main class="container">
        <h2>Lista de crianças com cadastro ativo { <?php echo $cont;?> }</h2>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>Nome</td>
                <td>Nascimento</td>
                <td>Nome da Mae</td>
                <td>Endereço</td>
                <td>Bairro</td>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($arrr as $pessoa){
                $id=$pessoa['id'];
                $nome=$pessoa['nome'];
                if ($pessoa["nascimento"]=="1000-01-01"){
                    $nascimento = "--/--/----";
                }else{
                    $nascimento = dataBanco2data($pessoa["nascimento"]). " " .Calculo_Idade($pessoa['nascimento'])." anos" ;
                }
                $mae=$pessoa['mae'];
                $endereco=$pessoa['endereco'];
                if ($pessoa['numero']==0){$numero="S/N";}else{$numero=$pessoa['numero'];}
                $bairro=fncgetbairro($pessoa['bairro'])['bairro'];

                ?>
                <tr>
                    <td><?php echo $nome;?></td>
                    <td style= "white-space: nowrap;"><?php echo $nascimento;?></td>
                    <td><?php echo $mae;?></td>
                    <td><?php echo $endereco;?> <?php echo $numero;?></td>
                    <td><?php echo $bairro;?></td>
                </tr>
            <?php } ?>
            </tbody>
    </table>
    </main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>