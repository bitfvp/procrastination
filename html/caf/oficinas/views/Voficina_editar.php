<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Oficina editar-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="oficinasave";
    $oficina=fncgetoficina($_GET['id']);
}else{
    $a="oficinanew";
}
?>
<main class="container"><!--todo conteudo-->
    <form action="<?php echo "index.php?pg=Voficina_editar&aca={$a}"; ?>" method="post" class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" value="SALVAR"  class="btn btn-success btn-block col-md-6">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input name="id"  id="id" type="hidden" value="<?php if (isset($oficina['id'])){echo $oficina['id'];} ?>"/>
                <label for="nome">Oficina</label>
                <input name="oficina" id="oficina" type="text" class="form-control" value="<?php if (isset($oficina['oficina'])){echo $oficina['oficina'];} ?>">
            </div>
            <div class="col-md-12">
                <label for="oficineiro">Oficineiro</label>
                <input name="oficineiro" id="oficineiro" type="text" class="form-control" value="<?php if (isset($oficina['oficineiro'])){echo $oficina['oficineiro'];} ?>">
            </div>
            <div class="col-md-6">
                <label>Status</label>
                <select name="status" required="true" class="form-control">
                    <option selected="" value="<?php if (isset($oficina['status'])){echo $oficina['status'];} ?>">
                        <?php
                        if (isset($oficina['status'])){
                            if ($oficina['status']==1){
                                echo "ATIVO";
                            }else{
                                echo "DESATIVADO";
                            }
                        }
                        ?>
                    </option>
                    <option value="1">ATIVO</option>
                    <option value="0">DESATIVADO</option>
                </select>
            </div>
        </div>

    </form>

</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>
