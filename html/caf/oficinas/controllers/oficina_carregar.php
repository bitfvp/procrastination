<?php
function fncoficinalist(){
    $sql = "SELECT * FROM caf_oficina ORDER BY oficina";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $oficinalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $oficinalista;
}

function fncgetoficina($id){
    $sql = "SELECT * FROM caf_oficina WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getoficina = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getoficina;
}
?>
