<?php
class Msg{
    public function fncnewmsg($data,$msg_remetente,$msg_destinatario,$msg_conteudo){
        //inserção no banco
        try{
            $sql="INSERT INTO caf_msgs ";
            $sql.="(id, data, remetente, destinatario, conteudo)";
            $sql.=" VALUES ";
            $sql.="(NULL, :data, :remetente, :destinatario, :conteudo)";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":remetente", $msg_remetente);
            $insere->bindValue(":destinatario", $msg_destinatario);
            $insere->bindValue(":conteudo", $msg_conteudo);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

//foto
        $sql = "SELECT Max(id) FROM caf_msgs";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mxv = $consulta->fetch();
        $sql=null;
        $consulta=null;
        $maxv=$mxv[0];

        // verifica se foi enviado um arquivo
        if (isset($_FILES['arquivo']['name']) && $_FILES["arquivo"]["error"] == 0) {//if principal

            //verificar se pasta existe
            if (is_dir('../dados/caf/chat/'.$maxv.'/')) {
//pasta existe
            }else{
//cria pasta
                mkdir('../dados/caf/chat/'.$maxv.'/');
            }

            //        echo "Você enviou o arquivo: <strong>" . $_FILES['arquivo']['name'] . "</strong><br />";
            //        echo "Este arquivo é do tipo: <strong>" . $_FILES['arquivo']['type'] . "</strong><br />";
            //        echo "Temporáriamente foi salvo em: <strong>" . $_FILES['arquivo']['tmp_name'] . "</strong><br />";
            //        echo "Seu tamanho é: <strong>" . $_FILES['arquivo']['size'] . "</strong> Bytes<br /><br />";

            $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
            $nome = $_FILES['arquivo']['name'];


            // Pega a extensao
            $extensao = strrchr($nome, '.');

            // Converte a extensao para mimusculo
            $extensao = strtolower($extensao);

            // Somente imagens, .jpg;.jpeg;.gif;.png
            // Aqui eu enfilero as extesões permitidas e separo por ';'
            // Isso server apenas para eu poder pesquisar dentro desta String
            if (strstr('.jpg;.jpeg;.gif;.docx;.doc;.xls;.xlsx;.pdf;.png', $extensao)) {//if secundario
                // Cria um nome único para esta imagem
                // Evita que duplique as imagens no servidor.
                $novoNome = md5(microtime()) . $extensao;

                // Concatena a pasta com o nome
                $destino = '../dados/caf/chat/' . $maxv . '/' . $novoNome;

                // tenta mover o arquivo para o destino
                if (@move_uploaded_file($arquivo_tmp, $destino)) {//terceiro if
                    $_SESSION['fsh']=[
                        "flash"=>"Arquivo salvo com sucesso em : \" . $destino ",
                        "type"=>"success",
                    ];


                } else {//terceiro if
                    $_SESSION['fsh']=[
                        "flash"=>"Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.",
                        "type"=>"danger",
                    ];
                }//terceiro if

            } else {//segundo if
                $_SESSION['fsh']=[
                    "flash"=>"Você poderá enviar apenas arquivos *.jpg;*.jpeg;*.gif;*.docx;*.doc;*.xls;*.xlsx;*.pdf;*.png",
                    "type"=>"warning",
                ];
            }//segundo if

        } else {// if principal
//            $_SESSION['fsh']=[
//                "flash"=>"Você não enviou nenhum arquivo!",
//                "type"=>"warning",
//            ];

        }//if primario

        header("Location: ?pg=Vmsg&id={$_GET['id']}");
        exit();
    }
}
?>