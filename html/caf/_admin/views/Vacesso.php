<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}


$page="Acessos ao sistema-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <div class="col-md-9">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Prepare a select statement
            $sql = "SELECT * FROM tbl_token where user=? ORDER BY tbl_token.id DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
            $frase = $consulta->fetchall();
            $cont = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Histórico de acessos ao sistema { <?php echo $cont;?> }
                </div>
                <div class="card-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">DATA</th>
                            <th scope="col">USUÁRIO</th>
                            <th scope="col" title="TEMPO RESTANTE DE SESSÃO">T. R.</th>
                            <th scope="col">DISPOSITIVO</th>
                            <th scope="col">S.O.</th>
                            <th scope="col">NAVEGADOR</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($frase as $tx){
                            $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                            $newtm =$tx['token_time']-$tml;
                            if ($newtm<0 or $newtm>3600){$newtm=0;}
                            $newtmm=gmdate("H:i:s", $newtm);
                            $tempdata=datahoraBanco2data($tx['data']);

                            if ($newtm>0){echo "<tr class='text-success'>";}else{echo "<tr class='text-danger'>";}
                            echo "<td>{$tx['id']}</td>";
                            echo "<th>{$tempdata}</th>";
                            echo "<td>".fncgetusuario($tx['user'])['nome']."</td>";
                            echo "<td>{$newtmm}</td>";
                            echo "<td>{$tx['dispositivo']}</td>";
                            echo "<td>{$tx['sistema_operacional']}</td>";
                            echo "<td>{$tx['navegador']}</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>