<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home -".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

    if (isset($_GET['sca'])){
        $sca = $_GET['sca'];
        //consulta se ha busca
        $sql = "select * from tbl_users WHERE matriz=2 AND nome LIKE '%$sca%' ";
    }else {
//consulta se nao ha busca
        $sql = "select * from tbl_users WHERE matriz=2 AND id=0 ";
    }
    // total de registros a serem exibidos por página
    $total_reg = "30"; // número de registros por página
    //Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
    $pgn=$_GET['pgn'];
    if (!$pgn) {
        $pc = "1";
    } else {
        $pc = $pgn;
    }
    //Vamos determinar o valor inicial das buscas limitadas
    $inicio = $pc - 1;
    $inicio = $inicio * $total_reg;
    //Vamos selecionar os dados e exibir a paginação
    //limite
    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $limite=$pdo->prepare($sql.$sql2);
        $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    //todos
    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $todos=$pdo->prepare($sql);
        $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $tr=$todos->rowCount();// verifica o número total de registros
    $tp = $tr / $total_reg; // verifica o número total de páginas
    ?>
<main class="container"><!--todo conteudo-->
    <h2>Listagem de Usuários</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vhome" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por usuário..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Valloweditar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO USUÁRIO
    </a>

        <script type="text/javascript">
            function selecionaTexto()
            {
                document.getElementById("sca").select();
            }
            window.onload = selecionaTexto();
        </script>


    <table class="table table-stripe table-hover table-condensed table-striped">
            <thead>
            <tr>
                <th scope="col">NOME</th>
                <th scope="col">PROFISSÃO</th>
                <th scope="col">EMAIL</th>
                <th scope="col">STATUS</th>
                <th scope="col">EDITAR</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th scope="row">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&pgn={$anterior}&sca={$_GET['sca']}' ><span aria-hidden=\"true\">&laquo; Anterior</a></li> ";
                    }
                    echo "|";
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&pgn={$proximo}&sca={$_GET['sca']}' >Próximo &#187;</a></li>";
                    }
                    ?>
                        </ul>
                    </nav>
                </th>
                <td colspan="4" class="text-right text-info"><?php echo $tr;?> Cadastro(s) listado(s)</td>
            </tr>
            </tfoot>

            <tbody>
            <?php
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
            foreach ($limite as $dados){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $profissao = fncgetprofissao($dados["profissao"])['profissao'];
            $email = $dados["email"];
            $status = $dados["status"];
            ?>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $ccc = strtoupper($nome);
                            $cc = explode(CSA, $ccc);
                            $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                            echo $c;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>
                <td>
                    <?php
                    echo $profissao;
                    ?>
                </td>
                <td>
                    <?php
                        echo $email;
                    ?>
                </td>
                

                <td>
                    <?php
                    if ($status==1){
                        echo "<i class=\"text-success fa fa-hand-peace\">Ativo</i>";
                    }else{
                        echo "<i class=\"text-danger fa fa-thumbs-down\">Desativado</i>";
                    }
                    ?>
                </td>

                <td>
                    <?php
                    $sql = "select pessoa from caf_allow WHERE pessoa=? ";
                    global $pdo;
                    $selec=$pdo->prepare($sql);
                    $selec->bindParam(1, $id);
                    $selec->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $all_ow = $selec->fetch();
                    $sql=null;
                    $selec=null;


                    if ($all_ow){
                        echo "<a href=\"index.php?pg=Valloweditar&id={$id}\" title=\"Edite as permissões desse Cadastro\">";
                        echo "Alterar";
                        echo "</a>";
                    }else{
                        echo "<i>Não há permissoes na tabela de usuario</i>";
                    }
                    ?>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
