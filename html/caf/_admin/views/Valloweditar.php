<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Allow-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="usuariosave";
    $us_er=fncgetusuario($_GET['id']);
    $al_low=fncgetallow($_GET['id']);
}else{
    $a="usuarionew";
}
?>


<main class="container"><!--todo conteudo-->

        <form class="frmgrid" action="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>"" method="post">


        <div class="row">
            <div class="col">
                <input type="submit" value="Salvar" class="btn btn-success btn-block" />
            </div>
            <?php if (isset($_GET['id']) and is_numeric($_GET['id'])){?>
                <div class="col">
                    <a href="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=resetsenha" class="btn btn-danger btn-block">Resetar senha do usuario</a>
                </div>
            <?php } ?>
        </div>

            <hr>
            <div class="row">
                <div class="col-md-4">
                    <label  class="large" for="nome">Nome:</label>
                    <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required/>
                </div>
                <div class="col-md-4">
                    <label  class="x-small" for="status">Ativo:</label>
                    <select name="status" id="status" class="form-control" >
                        // vamos criar a visualização de rf
                        <option selected="" value="<?php if($us_er['status']==""){$z=0; echo $z;}else{ echo $us_er['status'];} ?>">
                            <?php
                            if($us_er['status']==0){echo"Não";}
                            if($us_er['status']==1){echo"Sim";} ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label  class="large" for="">E-mail<span>*</span></label>
                    <input autocomplete="off" id="email" type="email" class="form-control" name="email" value="<?php echo $us_er['email']; ?>" required/>
                </div>
                <div class="col-md-4">
                    <label>Profissão:</label>
                    <select name="profissao" id="profissao" class="form-control" required>
                        <?php
                        $getprofissao=fncgetprofissao($us_er['profissao']);
                        ?>
                        <option selected="" value="<?php echo $us_er['profissao']; ?>">
                            <?php echo $getprofissao['profissao'];?>
                        </option>
                        <?php
                        $profissaolista=fncprofissaolist();
                        foreach ($profissaolista as $item) {
                            ?>
                            <option data-tokens="<?php echo $item['profissao'];?>" value="<?php echo $item['id'];?>">
                                <?php echo $item['profissao']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label  class="large" for="">Nascimento</label>
                    <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required/>
                </div>

                <div class="col-md-4">
                    <label  class="large" for="">CPF:</label>
                    <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required/>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                </div>

            </div>

        <?php if (isset($_GET['id']) and is_numeric($_GET['id'])){?>
        <hr>
            <div class="row">
                <?php
                function Setvv($v){
                    if($v==0){echo"Não";}
                    if($v==1){echo"Sim";}
                }
                ?>
                <div class="col-md-3">
                    <label>Administrador:</label>
                    <select name="admin" id="admin" class="form-control">
                        <option selected="" value="<?php echo $al_low['admin']; ?>">
                            <?php
                            Setvv($al_low['admin']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Frota:</label>
                    <select name="allow_1" id="allow_1" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_1']; ?>">
                            <?php
                            Setvv($al_low['allow_1']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Frota admin:</label>
                    <select name="allow_2" id="allow_2" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_2']; ?>">
                            <?php
                            Setvv($al_low['allow_2']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>nova pessoa:</label>
                    <select name="allow_3" id="allow_3" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_3']; ?>">
                            <?php
                            Setvv($al_low['allow_3']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>editar pessoa:</label>
                    <select name="allow_4" id="allow_4" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_4']; ?>">
                            <?php
                            Setvv($al_low['allow_4']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>bairro:</label>
                    <select name="allow_5" id="allow_5" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_5']; ?>">
                            <?php
                            Setvv($al_low['allow_5']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_6:</label>
                    <select name="allow_6" id="allow_6" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_6']; ?>">
                            <?php
                            Setvv($al_low['allow_6']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_7:</label>
                    <select name="allow_7" id="allow_7" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_7']; ?>">
                            <?php
                            Setvv($al_low['allow_7']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>agenda:</label>
                    <select name="allow_8" id="allow_8" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_8']; ?>">
                            <?php
                            Setvv($al_low['allow_8']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>protecao basica:</label>
                    <select name="allow_9" id="allow_9" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_9']; ?>">
                            <?php
                            Setvv($al_low['allow_9']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao basica atividade:</label>
                    <select name="allow_10" id="allow_10" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_10']; ?>">
                            <?php
                            Setvv($al_low['allow_10']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao basica atividade new:</label>
                    <select name="allow_11" id="allow_11" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_11']; ?>">
                            <?php
                            Setvv($al_low['allow_11']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao basica atividade relatório:</label>
                    <select name="allow_12" id="allow_12" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_12']; ?>">
                            <?php
                            Setvv($al_low['allow_12']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>allow_13:</label>
                    <select name="allow_13" id="allow_13" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_13']; ?>">
                            <?php
                            Setvv($al_low['allow_13']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_14:</label>
                    <select name="allow_14" id="allow_14" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_14']; ?>">
                            <?php
                            Setvv($al_low['allow_14']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_15:</label>
                    <select name="allow_15" id="allow_15" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_15']; ?>">
                            <?php
                            Setvv($al_low['allow_15']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_16:</label>
                    <select name="allow_16" id="allow_16" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_16']; ?>">
                            <?php
                            Setvv($al_low['allow_16']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>protecao especializada:</label>
                    <select name="allow_17" id="allow_17" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_17']; ?>">
                            <?php
                            Setvv($al_low['allow_17']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao especializada atividade:</label>
                    <select name="allow_18" id="allow_18" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_18']; ?>">
                            <?php
                            Setvv($al_low['allow_18']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao especializada atividade new:</label>
                    <select name="allow_19" id="allow_19" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_19']; ?>">
                            <?php
                            Setvv($al_low['allow_19']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>protecao especializada atividade relatório:</label>
                    <select name="allow_20" id="allow_20" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_20']; ?>">
                            <?php
                            Setvv($al_low['allow_20']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>allow_21:</label>
                    <select name="allow_21" id="allow_21" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_21']; ?>">
                            <?php
                            Setvv($al_low['allow_21']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_22:</label>
                    <select name="allow_22" id="allow_22" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_22']; ?>">
                            <?php
                            Setvv($al_low['allow_22']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Abrigo relatorios:</label>
                    <select name="allow_23" id="allow_23" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_23']; ?>">
                            <?php
                            Setvv($al_low['allow_23']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>abrigo historico:</label>
                    <select name="allow_24" id="allow_24" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_24']; ?>">
                            <?php
                            Setvv($al_low['allow_24']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>conselho tutelar:</label>
                    <select name="allow_25" id="allow_25" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_25']; ?>">
                            <?php
                            Setvv($al_low['allow_25']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>conselho tutelar atividade:</label>
                    <select name="allow_26" id="allow_26" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_26']; ?>">
                            <?php
                            Setvv($al_low['allow_26']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_27:</label>
                    <select name="allow_27" id="allow_27" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_27']; ?>">
                            <?php
                            Setvv($al_low['allow_27']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>conselho tutelar denuncia:</label>
                    <select name="allow_28" id="allow_28" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_28']; ?>">
                            <?php
                            Setvv($al_low['allow_28']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>conselho tutelar denuncia new:</label>
                    <select name="allow_29" id="allow_29" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_29']; ?>">
                            <?php
                            Setvv($al_low['allow_29']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>conselho tutelar denuncia apurar:</label>
                    <select name="allow_30" id="allow_30" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_30']; ?>">
                            <?php
                            Setvv($al_low['allow_30']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_31:</label>
                    <select name="allow_31" id="allow_31" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_31']; ?>">
                            <?php
                            Setvv($al_low['allow_31']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_32:</label>
                    <select name="allow_32" id="allow_32" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_32']; ?>">
                            <?php
                            Setvv($al_low['allow_32']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>cesta basica:</label>
                    <select name="allow_33" id="allow_33" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_33']; ?>">
                            <?php
                            Setvv($al_low['allow_33']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>cesta basica pedir:</label>
                    <select name="allow_34" id="allow_34" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_34']; ?>">
                            <?php
                            Setvv($al_low['allow_34']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>	cesta basica entregar:</label>
                    <select name="allow_35" id="allow_35" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_35']; ?>">
                            <?php
                            Setvv($al_low['allow_35']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>cesta basica relatorio:</label>
                    <select name="allow_36" id="allow_36" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_36']; ?>">
                            <?php
                            Setvv($al_low['allow_36']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>allow_37:</label>
                    <select name="allow_37" id="allow_37" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_37']; ?>">
                            <?php
                            Setvv($al_low['allow_37']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_38:</label>
                    <select name="allow_38" id="allow_38" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_38']; ?>">
                            <?php
                            Setvv($al_low['allow_38']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_39:</label>
                    <select name="allow_39" id="allow_39" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_39']; ?>">
                            <?php
                            Setvv($al_low['allow_39']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Cesta basica quantidade:</label>
                    <input id="allow_40" type="number" class="form-control" name="allow_40"
                           value="<?php echo $al_low['allow_40']; ?>"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>bolsa familia:</label>
                    <select name="allow_41" id="allow_41" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_41']; ?>">
                            <?php
                            Setvv($al_low['allow_41']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>bolsa familia pasta:</label>
                    <select name="allow_42" id="allow_42" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_42']; ?>">
                            <?php
                            Setvv($al_low['allow_42']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_43:</label>
                    <select name="allow_43" id="allow_43" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_43']; ?>">
                            <?php
                            Setvv($al_low['allow_43']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_44:</label>
                    <select name="allow_44" id="allow_44" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_44']; ?>">
                            <?php
                            Setvv($al_low['allow_44']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>bolsa familia visita:</label>
                    <select name="allow_45" id="allow_45" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_45']; ?>">
                            <?php
                            Setvv($al_low['allow_45']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_46:</label>
                    <select name="allow_46" id="allow_46" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_46']; ?>">
                            <?php
                            Setvv($al_low['allow_46']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_47:</label>
                    <select name="allow_47" id="allow_47" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_47']; ?>">
                            <?php
                            Setvv($al_low['allow_47']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_48:</label>
                    <select name="allow_48" id="allow_48" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_48']; ?>">
                            <?php
                            Setvv($al_low['allow_48']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Passe Livre:</label>
                    <select name="allow_49" id="allow_49" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_49']; ?>">
                            <?php
                            Setvv($al_low['allow_49']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>apae:</label>
                    <select name="allow_50" id="allow_50" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_50']; ?>">
                            <?php
                            Setvv($al_low['allow_50']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>idoso:</label>
                    <select name="allow_51" id="allow_51" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_51']; ?>">
                            <?php
                            Setvv($al_low['allow_51']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>pcd:</label>
                    <select name="allow_52" id="allow_52" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_52']; ?>">
                            <?php
                            Setvv($al_low['allow_52']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>advertecia:</label>
                    <select name="allow_53" id="allow_53" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_53']; ?>">
                            <?php
                            Setvv($al_low['allow_53']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_54:</label>
                    <select name="allow_54" id="allow_54" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_54']; ?>">
                            <?php
                            Setvv($al_low['allow_54']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_55:</label>
                    <select name="allow_55" id="allow_55" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_55']; ?>">
                            <?php
                            Setvv($al_low['allow_55']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Passe livre/empresa:</label>
                    <select name="allow_56" id="allow_56" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_56']; ?>">
                            <?php
                            Setvv($al_low['allow_56']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Migrante:</label>
                    <select name="allow_57" id="allow_57" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_57']; ?>">
                            <?php
                            Setvv($al_low['allow_57']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Migrante pasta:</label>
                    <select name="allow_58" id="allow_58" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_58']; ?>">
                            <?php
                            Setvv($al_low['allow_58']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Migrante pasta relatorio:</label>
                    <select name="allow_59" id="allow_59" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_59']; ?>">
                            <?php
                            Setvv($al_low['allow_59']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Passagem:</label>
                    <select name="allow_60" id="allow_60" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_60']; ?>">
                            <?php
                            Setvv($al_low['allow_60']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Passagem lancar:</label>
                    <select name="allow_61" id="allow_61" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_61']; ?>">
                            <?php
                            Setvv($al_low['allow_61']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Passagem lancar relatorio:</label>
                    <select name="allow_62" id="allow_62" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_62']; ?>">
                            <?php
                            Setvv($al_low['allow_62']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Albergue:</label>
                    <select name="allow_63" id="allow_63" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_63']; ?>">
                            <?php
                            Setvv($al_low['allow_63']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_64:</label>
                    <select name="allow_64" id="allow_64" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_64']; ?>">
                            <?php
                            Setvv($al_low['allow_64']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Telecentro:</label>
                    <select name="allow_65" id="allow_65" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_65']; ?>">
                            <?php
                            Setvv($al_low['allow_65']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_66:</label>
                    <select name="allow_66" id="allow_66" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_66']; ?>">
                            <?php
                            Setvv($al_low['allow_66']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_67:</label>
                    <select name="allow_67" id="allow_67" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_67']; ?>">
                            <?php
                            Setvv($al_low['allow_67']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_68:</label>
                    <select name="allow_68" id="allow_68" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_68']; ?>">
                            <?php
                            Setvv($al_low['allow_68']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-3 d-none">
                    <label>allow_69:</label>
                    <select name="allow_69" id="allow_69" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_69']; ?>">
                            <?php
                            Setvv($al_low['allow_69']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_70:</label>
                    <select name="allow_70" id="allow_70" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_70']; ?>">
                            <?php
                            Setvv($al_low['allow_70']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Abrigo estoque:</label>
                    <select name="allow_71" id="allow_71" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_71']; ?>">
                            <?php
                            Setvv($al_low['allow_71']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Biblioteca:</label>
                    <select name="allow_72" id="allow_72" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_72']; ?>">
                            <?php
                            Setvv($al_low['allow_72']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-3 d-none">
                    <label>CH:</label>
                    <select name="allow_73" id="allow_73" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_73']; ?>">
                            <?php
                            Setvv($al_low['allow_73']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Oficio:</label>
                    <select name="allow_74" id="allow_74" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_74']; ?>">
                            <?php
                            Setvv($al_low['allow_74']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Fila:</label>
                    <select name="allow_75" id="allow_75" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_75']; ?>">
                            <?php
                            Setvv($al_low['allow_75']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>	Fila cadastrar:</label>
                    <select name="allow_76" id="allow_76" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_76']; ?>">
                            <?php
                            Setvv($al_low['allow_76']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-3 d-none">
                    <label>Fila chamar:</label>
                    <select name="allow_77" id="allow_77" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_77']; ?>">
                            <?php
                            Setvv($al_low['allow_77']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Fila Tela:</label>
                    <select name="allow_78" id="allow_78" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_78']; ?>">
                            <?php
                            Setvv($al_low['allow_78']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>Estoque:</label>
                    <select name="allow_79" id="allow_79" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_79']; ?>">
                            <?php
                            Setvv($al_low['allow_79']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>SysJob:</label>
                    <select name="allow_80" id="allow_80" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_80']; ?>">
                            <?php
                            Setvv($al_low['allow_80']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            </div>
        <?php } ?>

    </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>