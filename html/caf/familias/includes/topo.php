<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/syssocial2.png" alt="<?php echo $env->env__nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">

                <form id="formserchtopo" action="index.php" method="get" class="form-inline my-2 my-lg-0">
                    <button accesskey="x" id="btn-search" class="btn shadow-none border-0 bg-transparent text-light my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                    <input autocomplete="off" type="text" id="searchtopo" name="sca" placeholder="Buscar por nome..." class="form-control ml-sm-2" aria-label="Search" style="display: none;text-transform:lowercase;">
                    <input name="pg" value="Vbusca" type="hidden"/>
                </form>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navben" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        BENEFÍCIOS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navben">
                        <a class="dropdown-item" href="index.php?pg=Vb_m">Meus pedidos de benefícios</a>
                        <a class="dropdown-item" href="index.php?pg=Vb_r">Relatório de benefícios</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navati" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ATIVIDADES
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navati">
                            <a class="dropdown-item" href="index.php?pg=Vatminhas">Minhas atividades</a>
                            <a class="dropdown-item" href="index.php?pg=Vatrelatorioprof">Relatório de atividades</a>
                    </div>
                </li>

            </ul>
            <script>
                $( function() {
                    $('#btn-search').on('click', function(e) {
                        e.preventDefault();
                        $('#searchtopo').animate({width: 'toggle'}).focus();

                    });
                } () );
                //força submit on enter
                $('#searchtopo').keypress(function (e) {
                    if (e.which == 13) {
                        $('#formserchtopo').submit();
                    }
                });
            </script>
            <?php
            include_once("{$env->env_root}includes/caf/modal_msg_pontos.php");
            ?>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalPontos">
                            <i class="fa fa-star"></i>
                            <?php echo $ponto; ?>
                        </a>
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?PHP echo $env->env_url; ?>?pg=Vlogin"><i class="fa fa-undo"></i> Voltar</a>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");
