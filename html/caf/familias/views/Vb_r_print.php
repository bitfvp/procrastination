<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de Benefícios-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicio'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT "
    ."caf_beneficio.id, "
    ."tbl_users.nome AS profissional, "
    ."caf_pessoas.nome AS pessoa, "
    ."caf_beneficio_lista.beneficio, "
    ."caf_beneficio.`data`, "
    ."caf_beneficio.quantidade "
    ."FROM "
    ."caf_beneficio "
    ."INNER JOIN caf_beneficio_lista ON caf_beneficio_lista.id = caf_beneficio.beneficio "
    ."INNER JOIN tbl_users ON tbl_users.id = caf_beneficio.profissional "
    ."INNER JOIN caf_pessoas ON caf_pessoas.id = caf_beneficio.pessoa "
    ."WHERE "
    ."caf_beneficio.`data` >= :inicial AND "
    ."caf_beneficio.`data` <= :final "
    ."ORDER BY "
    ."caf_beneficio_lista.beneficio ASC, "
    ."caf_beneficio.`data` ASC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->execute();
$ati = $consulta->fetchAll();
$sql=null;
$consulta=null;

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <img src="<?php echo $env->env_estatico;?>img/caf.png" alt="">
        </div>
        <div class="col-9 pl-5 pt-2">
            <h3>CENTRO DE APOIO Á FAMILIA</h3>
            <h5>End. Rua Hervê Cordovil, s/n°, Nossa Senhora aparecida, Manhuaçu-MG <br>
                CNPJ: 10.876.048/0001-11 <br>
                Contato (33) 98443-4242
            </h5>
        </div>
    </div>
    <hr>
    <h3>Relatório de Benefícios</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicio'])." a ".dataBanco2data($_POST['data_final']);?></h5>
    <?php
    $acont=0;
    $a="a";
    echo "<table class='table table-striped table-bordered table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>Beneficiario</td>";
    echo "<td>Quantidade</td>";
    echo "<td>Data</td>";
    echo "<td>Profissional</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    foreach($ati as $at){

        if($a!=$at['beneficio'] and $acont>0){

            echo "<tr>";
            echo '<td colspan="5" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
            echo "</tr>";
            $acont=0;

        }
        if((isset($a) and ($a!=$at['beneficio']))){

            echo "<tr>";
            echo "<td colspan='5' class='text-center font-weight-bold'>";
            echo $at['beneficio'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo $at['quantidade']; ?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
            <td><?php echo $at['profissional']; ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $ppp=$at['profissional'];
        $a=$at['beneficio'];
    }
    echo "<tr>";
    echo '<td colspan="5" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
    echo "</tr>";
    $acont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
</body>
</html>