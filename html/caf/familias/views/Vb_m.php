<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="Meus benefícios-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">

    <div class="row">
        <div class="col-md-8">
        <?php
        // Recebe
            $id_user =$_SESSION['id'];
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT * \n"
                    . "FROM caf_beneficio \n"
                    . "WHERE (((caf_beneficio.profissional)=?))\n"
                    . "ORDER BY caf_beneficio.data DESC LIMIT 0,1000";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_user);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mcb = $consulta->fetchAll();
                $qcb = $consulta->rowCount();
                $sql=null;
                $consulta=null;
            }
        ?>

            <div class="card">
                <div class="card-header bg-info text-light">
                    Meus pedidos de benefícios { <?php echo $qcb;?> }
                </div>
                <div class="card-body">
                    <h6>
                    <?php
                    foreach($mcb as $cb){
                    $pessoa=fncgetpessoa($cb['pessoa']);
                    ?>
                        <blockquote class="blockquote blockquote-info">
                            Pessoa:
                            <a href="?pg=Vpessoa&id=<?php echo $cb['pessoa']; ?>">
                                <strong class="text-info">
                                    <?php echo $pessoa['nome'];?>
                                </strong>
                            </a>
                            <br>
                            Endereço:
                            <strong class="text-info">
                                <?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;
                            </strong>
                            Numero:
                            <strong class="text-info">
                                <?php echo $pessoa['numero']; ?>&nbsp;&nbsp;
                            </strong>
                            Bairro:
                            <strong class="text-info">
                                <?php echo fncgetbairro($pessoa['bairro'])['bairro']; ?>&nbsp;&nbsp;
                            </strong>
                            <br>
                            Referência:
                            <strong class="text-info">
                                <?php echo $pessoa['referencia']; ?>&nbsp;&nbsp;
                            </strong>
                            <br>
                            Benefício: <strong class="text-info"><?php echo fncgetbeneficio_lista($cb['beneficio'])['beneficio']?></strong><br>
                            Quantidade: <strong class="text-info"><?php echo $cb['quantidade'];?></strong><br>
                            Data: <strong class="text-info" title="Lançado em <?php echo datahoraBanco2data($ben['data_ts']);?>"><?php echo dataRetiraHora($cb['data']);?></strong><br>
                            Descrição: <strong class="text-info">"<?php echo $cb['descricao'];?>"</strong><br>
                            <span class="badge badge-pill badge-warning float-right"><strong><?php echo $cb['id']; ?></strong></span>
                            <br>
                        </blockquote>
                        <hr>
                    <?php } ?>
                    </h6>
                </div>
            </div>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>