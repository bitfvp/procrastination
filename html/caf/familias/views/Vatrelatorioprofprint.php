<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$profi=$_POST['profissional'];
if($profi==0){
    $sql = "SELECT caf_at.id, tbl_users.nome AS prof, caf_pessoas.nome AS pessoa, caf_atlista.atividade, caf_at.data, caf_at.descricao, caf_at.profissional\n"
        . "FROM caf_pessoas INNER JOIN (tbl_users INNER JOIN (caf_atlista INNER JOIN caf_at ON caf_atlista.id = caf_at.atividade) ON tbl_users.id = caf_at.profissional) ON caf_pessoas.id = caf_at.pessoa\n"
        . "WHERE (((caf_at.data)>=:inicial) And ((caf_at.data)<=:final) And ((caf_at.tipo)=1))\n"
        . "ORDER BY tbl_users.nome, caf_atlista.atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
//$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{
    $sql = "SELECT caf_at.id, tbl_users.nome AS prof, caf_pessoas.nome AS pessoa, caf_atlista.atividade, caf_at.data, caf_at.descricao, caf_at.profissional\n"
        . "FROM caf_pessoas INNER JOIN (tbl_users INNER JOIN (caf_atlista INNER JOIN caf_at ON caf_atlista.id = caf_at.atividade) ON tbl_users.id = caf_at.profissional) ON caf_pessoas.id = caf_at.pessoa\n"
        . "WHERE (((caf_at.data)>=:inicial) And ((caf_at.data)<=:final) AND ((caf_at.profissional)=:profi) And ((caf_at.tipo)=1))\n"
        . "ORDER BY tbl_users.nome, caf_atlista.atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <img src="<?php echo $env->env_estatico;?>img/caf.png" alt="">
        </div>
        <div class="col-9 pl-5 pt-2">
            <h3>CENTRO DE APOIO Á FAMILIA</h3>
            <h5>End. Rua Hervê Cordovil, s/n°, Nossa Senhora aparecida, Manhuaçu-MG <br>
                CNPJ: 10.876.048/0001-11 <br>
                Contato (33) 98443-4242
            </h5>
        </div>
    </div>
    <hr>
    <h3>Relatório de atividades</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
    <?php
    $acont=0;
    $a=$ppp="a";
    echo "<table class='table table-striped table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>PESSOA</td>";
    echo "<td>ATIVIDADE</td>";
    echo "<td>DATA</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach($ati as $at){

        if(((isset($ppp))and ($ppp!=$at['prof']) and ($acont>0))or((isset($a))and ($a!=$at['atividade'])) and ($acont>0)){

            echo "<tr>";
            echo "<td colspan='4' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
            echo "</tr>";
            $acont=0;
            if ((isset($ppp))and ($ppp!=$at['prof'])){
                echo "<tr>";
                echo "<td colspan='4' class='text-right font-weight-bold'>Total ".$tcont."</td>";
                echo "</tr>";
                $tcont=0;
            }

        }
        if(((isset($ppp))and ($ppp!=$at['prof']))or((isset($a))and ($a!=$at['atividade']))){

            echo "<tr>";
            echo "<td colspan='4' class='text-center font-weight-bold'>";
            echo $at['prof']." - ".$at['atividade'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo $at['atividade']; ?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $tcont++;
        $ppp=$at['prof'];
        $a=$at['atividade'];
    }
    echo "<tr>";
    echo "<td colspan='4' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
    echo "</tr>";
    $acont=0;
    echo "<tr>";
    echo "<td colspan='4' class='text-right font-weight-bold'>Total ".$tcont."</td>";
    echo "</tr>";
    $tcont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
<fieldset>
    <div id="piechart" style="width: auto; height: 900px;"></div>
</fieldset>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['Atividade', 'Quant'],
            <?php
            // Recebe
            $inicial = $_POST['data_inicial']."00:00:01";
            $final = $_POST['data_final']."23:59:59";
            $profi=$_POST['profissional'];
            if($profi==0){
                $sql = "SELECT\n"
                    . "Count(caf_at.id) AS contadora,\n"
                    . "caf_atlista.atividade AS atv\n"
                    . "FROM\n"
                    . "caf_at\n"
                    . "INNER JOIN caf_atlista ON caf_atlista.id = caf_at.atividade\n"
                    . "WHERE\n"
                    . "caf_at.`data` >= :inicial AND\n"
                    . "caf_at.`data` <= :final AND caf_at.tipo=1 \n"
                    . "GROUP BY\n"
                    . "caf_at.atividade\n"
                    . "ORDER BY\n"
                    . "contadora desc";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }else{
                $sql = "SELECT\n"
                    . "Count(caf_at.id) AS contadora,\n"
                    . "caf_atlista.atividade AS atv\n"
                    . "FROM\n"
                    . "caf_at\n"
                    . "INNER JOIN caf_atlista ON caf_atlista.id = caf_at.atividade\n"
                    . "WHERE\n"
                    . "caf_at.`data` >= :inicial AND\n"
                    . "caf_at.`data` <= :final AND\n"
                    . "caf_at.profissional = :prof AND caf_at.tipo=1 \n"
                    . "GROUP BY\n"
                    . "caf_at.atividade\n"
                    . "ORDER BY\n"
                    . "contadora desc";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->bindValue(":prof", $profi);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }

            foreach ($graf as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['atv'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);



        //opções para o gráfico pizza
        var options3 = {
            title: 'Grafico torta de atividades por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>