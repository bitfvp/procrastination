<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Benefícios-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/caf/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Pedido de benefício
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vb&aca=newbeneficio&id=<?php echo $_GET['id']; ?>" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="beneficio">Benefício:</label>
                                <select name="beneficio" id="beneficio" required  class="form-control">
                                    <option value="" selected>Selecione </option>
                                    <?php
                                    foreach (fncbeneficio_listalist() as $item){
                                        echo "<option value='{$item['id']}'>{$item['beneficio']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="quantidade">Quantidade:</label>
                                <input name="quantidade" id="quantidade" type="number" required autocomplete="off" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="data">Data:</label>
                                <input id="data" type="date" class="form-control" name="data"  value="<?php echo date("Y-m-d"); ?>"/>
                            </div>
                            <div class="col-md-12">
                                <label for="descricao">Observação:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,200,descricao,'cont')" maxlength="200" class="form-control" rows="3" name="descricao"></textarea>
                                <span id="cont">200</span>/200
                            </div>
                            <div class="col-md-12 mt-3">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer text-warning text-center">
                    Sempre verificar o endereço antes de emitir o benefício.
                </div>
            </div>
            <?php
            try {
            $sql = "SELECT * FROM caf_beneficio WHERE pessoa=? "
                . "ORDER BY data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
            $beneficio = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
            ?>
        <!--card historico-->
            <div class="card mt-3">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico de cesta básica
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vb_print&id=<?php echo $_GET['id'];?>" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão</span>
                    </a>

                    <?php
                    foreach($beneficio as $cb){
                        ?>
                        <hr>
                        <h5>
                            <blockquote class="blockquote blockquote-info">
                                Benefício: <strong class="text-info"><?php echo fncgetbeneficio_lista($cb['beneficio'])['beneficio']?></strong><br>
                                Quantidade: <strong class="text-info"><?php echo $cb['quantidade'];?></strong><br>
                                Data: <strong class="text-info" title="Lançado em <?php echo datahoraBanco2data($ben['data_ts']);?>"><?php echo dataRetiraHora($cb['data']);?></strong><br>
                                Descrição: <strong class="text-info">"<?php echo $cb['descricao'];?>"</strong><br>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $cb['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($cb['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                        </h5>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>