<?php
class Beneficio
{
    public function fncbeneficionew($b_pessoa,$b_profissional,$b_data,$b_beneficio,$b_quantidade,$b_descricao)
    {
        //inserção no banco
        try {
            $sql = "INSERT INTO caf_beneficio ";
            $sql .= "(id, pessoa, data, data_ts, beneficio, descricao, quantidade, profissional)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, :data, CURRENT_TIMESTAMP, :beneficio, :descricao, :quantidade, :profissional)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $b_pessoa);
            $insere->bindValue(":data", $b_data);
            $insere->bindValue(":beneficio", $b_beneficio);
            $insere->bindValue(":descricao", $b_descricao);
            $insere->bindValue(":quantidade", $b_quantidade);
            $insere->bindValue(":profissional", $b_profissional);
            $insere->execute();
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($insere)) {

            //reservado para log
            global $LL; $LL->fnclog($b_pessoa,$_SESSION['id'],"Novo beneficio",3,1);
            //obter codigo familiar
            try{
                $sql = "SELECT cod_familiar FROM caf_pessoas WHERE id=?";

                global $pdo;
                $queryy=$pdo->prepare($sql);
                $queryy->bindParam(1, $b_pessoa);
                $queryy->execute();
                $cod_familiar=$queryy->fetch();
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }

            //testar se já tem um codigo familiar valido
            if(isset($cod_familiar['cod_familiar']) and $cod_familiar['cod_familiar']!=null and $cod_familiar['cod_familiar']!=0) {
                $cod_f=$cod_familiar['cod_familiar'];
            }else{
                //se não é valido criar novo codigo familiar
                $token = uniqid("");
                try {
                    $sql = "UPDATE caf_pessoas SET ";
                    $sql .= "cod_familiar=:cod_familiar";
                    $sql .= " WHERE id=:id";

                    global $pdo;
                    $buali = $pdo->prepare($sql);
                    $buali->bindValue(":cod_familiar", $token);
                    $buali->bindValue(":id", $b_pessoa);
                    $buali->execute();

                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }
                $cod_f=$token;
            }

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vb&id={$b_pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new



}//fim da classe

?>