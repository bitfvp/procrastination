<?php
class Atividade
{
    public function fncatividadenew($at_pessoa,$at_profissional,$at_data,$at_restricao,$at_atividade,$at_descricao,$at_tipo,$Upin)
    {
        //inserção no banco
        try {
            $sql = "INSERT INTO caf_at ";
            $sql .= "(id, pessoa, data, profissional, restricao, atividade, descricao, tipo)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, :data, :profissional, :restricao, :atividade, :descricao, :tipo)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $at_pessoa);
            $insere->bindValue(":data", $at_data);
            $insere->bindValue(":profissional", $at_profissional);
            $insere->bindValue(":restricao", $at_restricao);
            $insere->bindValue(":atividade", $at_atividade);
            $insere->bindValue(":descricao", $at_descricao);
            $insere->bindValue(":tipo", $at_tipo);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($insere)) {
            //foto
            $sql = "SELECT Max(id) FROM caf_at";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mxv = $consulta->fetch();
            $sql = null;
            $consulta = null;
            $maxv = $mxv[0];
            // verifica se foi enviado um arquivo
            $fillle=$_FILES['arquivo']['name'];
            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
                if (is_dir("../../dados/caf/familias/atividades/" . $maxv . '/')) {} else {mkdir("../../dados/caf/familias/atividades/" . $maxv . '/');}
                // verifica se foi enviado um arquivo
                $Upin->get(
                    '../../dados/caf/familias/atividades/'.$maxv.'/', //Pasta de uploads (previamente criada)
                    $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
                    10, //Tamanho máximo
                    "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
                    "arquivo", //Atributo name do input file
                    1 //Mudar o nome? 1 = sim, 0 = não
                );
                $Upin->run();
            }
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($at_pessoa,$_SESSION['id'],"Nova atividade",2,1);
            //////////////////////////////////////////////////////////////////////////// cras 2 creas 5
            //começa alteração atendido pelo cras
            //
            //obter codigo familiar
            try{
                $sql = "SELECT cod_familiar FROM caf_pessoas WHERE id=?";

                global $pdo;
                $queryy=$pdo->prepare($sql);
                $queryy->bindParam(1, $at_pessoa);
                $queryy->execute(); global $LQ; $LQ->fnclogquery($sql);
                $cod_familiar=$queryy->fetch();
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }

            //testar se já tem um codigo familiar valido
            if(isset($cod_familiar['cod_familiar']) and $cod_familiar['cod_familiar']!=null and $cod_familiar['cod_familiar']!=0) {
                $cod_f=$cod_familiar['cod_familiar'];
            }else{
                //se não é valido criar novo codigo familiar
                $token = uniqid("");
                try {
                    $sql = "UPDATE caf_pessoas SET ";
                    $sql .= "cod_familiar=:cod_familiar";
                    $sql .= " WHERE id=:id";

                    global $pdo;
                    $atuali = $pdo->prepare($sql);
                    $atuali->bindValue(":cod_familiar", $token);
                    $atuali->bindValue(":id", $at_pessoa);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }
                $cod_f=$token;
            }

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vat&id={$at_pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new

    public function fncatividadedelete($at_id,$prof,$pessoa_id){
        try {
            $sql = "DELETE FROM `caf_at` WHERE id = :at_id and profissional = :prof_id";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":at_id", $at_id);
            $exclui->bindValue(":prof_id", $prof);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Atividade Excluida Com Sucesso",
            "type"=>"success",
        ];

        //reservado para log
        global $LL; $LL->fnclog($pessoa_id,$_SESSION['id'],"Apagar atividade",2,4);
        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg=Vat&id={$pessoa_id}");
        exit();
    }//fim da fnc delete


}//fim da classe

?>