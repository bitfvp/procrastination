<?php
function fncbeneficio_listalist(){
    $sql = "SELECT * FROM caf_beneficio_lista ORDER BY beneficio";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $beneficio_listalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $beneficio_listalista;
}

function fncgetbeneficio_lista($id){
    $sql = "SELECT * FROM caf_beneficio_lista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getbeneficio_lista = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getbeneficio_lista;
}
?>
