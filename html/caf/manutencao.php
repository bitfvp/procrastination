<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


///
try{
    $sql="select * from tbl_suport where id=2";
    global $pdo;
    $manutencao=$pdo->prepare($sql);
    $manutencao->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$manutencao=$manutencao->fetch();
if ($manutencao['apoio']!=2) {
    if ($manutencao['valor'] == 0) {
        header("Location: {$_ENV['ENV_URL']}?pg=Vlogin");
        exit();
    }else{
        if ($manutencao['apoio']!=0){
            header("Location: {$_ENV['ENV_URL']}?pg=Vlogin");
            exit();
        }
    }
}
echo"<META HTTP-EQUIV=REFRESH CONTENT = '600;URL=index.php'>";
?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet">
<style>
    body {
        margin: 0;
        padding: 0;
    }
    * {
        box-sizing: border-box;
    }
    .maintenance {
        background-image: url("../public/img/manu_fundo.jpg");
        background-repeat: no-repeat;
        background-position: center center;
        background-attachment: scroll;
        background-size: cover;
    }

    .maintenance {
        width: 100%;
        height: 100%;
        min-height: 100vh;
    }

    .maintenance {
        display: flex;
        flex-flow: column nowrap;
        justify-content: center;
        align-items: center;
    }

    .maintenance_contain {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        align-items: center;
        justify-content: center;
        width: 100%;
        padding: 15px;
    }
    .maintenance_contain img {
        width: auto;
        max-width: 100%;
    }
    .pp-infobox-title-prefix {
        font-weight: 500;
        font-size: 20px;
        color: #000000;
        margin-top: 30px;
        text-align: center;
    }

    .pp-infobox-title-prefix {
        font-family: sans-serif;
    }

    .pp-infobox-title {
        color: #000000;
        font-family: sans-serif;
        font-weight: 700;
        font-size: 40px;
        margin-top: 10px;
        margin-bottom: 10px;
        text-align: center;
        display: block;
        word-break: break-word;
    }

    .pp-infobox-description {
        color: #000000;
        font-family: "Poppins", sans-serif;
        font-weight: 400;
        font-size: 18px;
        margin-top: 0px;
        margin-bottom: 0px;
        text-align: center;
    }

    .pp-infobox-description p {
        margin: 0;
    }

    .title-text.pp-primary-title {
        color: #000000;
        padding-top: 0px;
        padding-bottom: 0px;
        padding-left: 0px;
        padding-right: 0px;
        font-family: sans-serif;
        font-weight: 500;
        font-size: 18px;
        line-height: 1.4;
        margin-top: 50px;
        margin-bottom: 0px;
    }

    .pp-social-icon {
        margin-left: 10px;
        margin-right: 10px;
        display: inline-block;
        line-height: 0;
        margin-bottom: 10px;
        margin-top: 10px;
        text-align: center;
    }

    .pp-social-icon a {
        display: inline-block;
        height: 40px;
        width: 40px;
    }

    .pp-social-icon a i {
        border-radius: 100px;
        font-size: 20px;
        height: 40px;
        width: 40px;
        line-height: 40px;
        text-align: center;
    }

    .pp-social-icon:nth-child(1) a i {
        color: #34af23;
    }
    .pp-social-icon:nth-child(1) a i {
        border: 2px solid #34af23;
    }
    .pp-social-icon:nth-child(2) a i {
        color: #0072b1;
    }
    .pp-social-icon:nth-child(2) a i {
        border: 2px solid #0072b1;
    }

    .pp-social-icons {
        display: flex;
        flex-flow: row wrap;
        align-items: center;
        justify-content: center;
    }
</style>
<div class="maintenance">
    <div class="maintenance_contain">
        <img src="../public/img/manu_principal.png" alt="maintenance">
        <span class="pp-infobox-title-prefix">VOLTAREMOS EM BREVE</span>
        <div class="pp-infobox-title-wrapper">
            <h3 class="pp-infobox-title">O sistema está em manutenção!</h3>
        </div>
        <div class="pp-infobox-description">
            <p>Estamos temporariamente indisponível devido a uma manutenção não planejada.<br>Esperamos concluir a manutenção até meia-noite.<br>Pedimos desculpas por qualquer inconveniente.</p></div>
        <span class="title-text pp-primary-title">Nos contate por aqui</span>
        <div class="pp-social-icons pp-social-icons-center pp-responsive-center">
	<span class="pp-social-icon">
		<link itemprop="url" href="#">
		<a itemprop="sameAs" href="#" target="_self" title="whats" aria-label="whats" role="button">
			<i class="fab fa-whatsapp"></i>
		</a>
        <strong>(33)98401-7757</strong>
	</span>
            <span class="pp-social-icon">
		<link itemprop="url" href="#">
		<a itemprop="sameAs" href="#" target="_self" title="email" aria-label="email" role="button">
			<i class="fas fa-envelope-open-text"></i>
		</a>
                <strong>atendimento@cpv.com.br</strong>
	</span>
        </div>
    </div>
</div>