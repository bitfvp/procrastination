<?php
$dia=date("d");
$mes=date("m");

$sql = "SELECT * FROM tbl_users WHERE status=1 and matriz=? and `nascimento` LIKE '%-{$mes}-{$dia}%' ORDER BY nome";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$_SESSION['matriz']);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$aniversarianteslista = $consulta->fetchAll();
$contaniversariantes = $consulta->rowCount();
$sql=null;
$consulta=null;

$meuaniversario=0;
foreach ($aniversarianteslista as $an){
    if ($an['id']==$_SESSION['id']){
        $meuaniversario++;
    }
}
///////////////////////////////////////////////////////
if ($contaniversariantes>0){
    if ($meuaniversario > 0){
        $aniver=fncgetusuario($_SESSION['id']);
        $primeiroNome = explode(" ", $_SESSION["nome"]);
        ?>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="aniversario" tabindex="-1" role="dialog" aria-labelledby="modalaniversarioLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div id="mmsgs" class="modal-body">
                        <h1 class="text-center text-danger">Ei você!! Pensou que não sabiamos??</h1>
                        <h1 class="text-center">A gente lembrou <i class="fa fa-smile-wink"></i></h1>
                        <h1 class="text-center text-warning"><i class="fa fa-birthday-cake fa-3x pulse"></i></h1>
                        <h1 class="text-center"><?php echo Calculo_Idade($aniver['nascimento']);?> aninhos</h1>
                        <h1 class="text-center"><strong>Feliz Aniversário <?php echo $primeiroNome[0];?></strong></h1>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#aniversario').modal('toggle');
        </script>
        <!--fim de modalmsgs-->












    <?php
    }else{
        ?>

        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="aniversario" tabindex="-1" role="dialog" aria-labelledby="modalaniversarioLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg bg-dark" role="document">
                <div class="modal-content">
                    <div id="mmsgs" class="modal-body">
                        <h1 class="text-center text-info"><i class="fa fa-surprise"></i> Você sabia??</h1>
                        <h2 class="text-center">Tem amigo fazendo aniversário hoje</h2>

                        <?php
                        foreach ($aniversarianteslista as $parabens){
                            echo "<h5 class='text-center'>".$parabens['nome']. " (".fncgetprofissao($parabens['profissao'])['profissao'].") <i class='fa fa-birthday-cake text-danger pulse-slow'></i> ".Calculo_Idade($parabens['nascimento']) ." anos</h5>";
                        }
                        ?>
                        <h1 class="text-center text-info">Aproveite para dar os parabéns <i class="fa fa-grin"></i></h1>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#aniversario').modal('toggle');
        </script>
        <!--fim de modalmsgs-->





        <?php
    }
}
?>