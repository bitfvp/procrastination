<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        if ($allow["allow_5"]!=1){
            //validação de matriz
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            //validação das permissoes
        }
    }
}

$page="Editar Bairro-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="bairrosave";
    $bairro=fncgetbairro($_GET['id']);
}else{
    $a="bairronew";
}
?>
<main class="container"><!--todo conteudo-->
<h1>Cadastro de bairro</h1>
    <hr>
    <form action="index.php?pg=Vbl&aca=<?php echo $a;?>" method="post">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="bairro">Bairro:</label>
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $_GET['id']; ?>"/>
                <input name="bairro" type="text" class="form-control" required="true" autocomplete="off" autofocus placeholder="Bairro" value="<?php echo $bairro['bairro']; ?>"/>
            </div>
            <input type="submit" name="" value="Salvar" class="btn btn-primary"/>
    </form>
    <br/>
    </div>
    </form>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>