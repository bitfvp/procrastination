<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}

$page="Chat-".$env->env_titulo;
$css="chat";
include_once("{$env->env_root}includes/head.php");

$out_id = $_GET['id'];

$sql = "SELECT nome from tbl_users where id=:destinatario";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(':destinatario', $out_id);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$chat = $consulta->fetch();
$sql = null;
$consulta = null;


?>
<!--chama msg recebida-->
<script type="text/javascript">
    $.ajaxSetup({cache: false});
    $(document).ready(function () {
        $('#messages').load('<?php echo $env->env_url;?>includes/caf/mensagens.php?out_id=<?php echo $out_id;?>');
    });
    $(document).ready(function () {
        setInterval(function () {
            $('#messages').load('<?php echo $env->env_url;?>includes/caf/mensagens.php?out_id=<?php echo $out_id;?>')
        }, 15000);
    });
</script>


<nav class="navbar navbar-expand sticky-top navbar-light bg-light">
    <nav class="container">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav mx-auto">
                <a class="nav-item nav-link text-info" href="<?php echo $env->env_url;?>caf/?pg=Vmsgsc"><i class="fa fa-chevron-left"></i></a>
                <a class="nav-item nav-link" href="#"><strong><?php echo $chat['0']; ?></strong></a>
                <a class="nav-item nav-link text-danger" href="" onclick="window.close()"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </nav>
</nav>

<main class="container">
    <ul id="messages" class="messages pb-3">
    </ul>
</main>

<nav class="navbar navbar-expand fixed-bottom navbar-light bg-light">
    <nav class="container">


                <form class="form-inline mx-auto" action="<?php echo $env->env_url;?>caf/?pg=Vmsg&aca=newmsg&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                    <script type="text/javascript">function stopEvent(event) {	if (event.preventDefault) {		event.preventDefault();		event.stopPropagation();	} else {		event.returnValue = false;		event.cancelBubble = true;	}}function areaEnvia(obj, evt) {	var e = evt || event;	var k = e.keyCode;		if(k == 13) { 		if(!e.shiftKey) {			if(obj.form)				obj.form.submit();						stopEvent(e);		}	}}</script>

                    <input name="conteudo" id="escondetexto" class="form-control mr-2 w-100" type="text" placeholder="Digite sua mensagem..." aria-label="Search" autocomplete="off" autofocus="true" type="button">
                    <div class="custom-file" id="escondefile" style="display: none;">
                        <input type="file" class="custom-file-input" name="arquivo">
                        <label class="custom-file-label" for="inputGroupFile01">Escolha o arquivo...</label>
                    </div>
                    <div class="mx-auto">
                    <button class="btn btn-outline-success my-2" id="esconder" type="button"><i class="fa fa-upload"></i>Arquivos</button>
                    <button class="btn btn-outline-success my-2" type="submit"><i class="fa fa-plus"></i>Enviar</button>
                    </div>
                </form>

    </nav>
</nav>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#esconder").click(escondefile);
        });

        function escondefile() {
            $("#escondefile").toggle();
            $("#escondetexto").toggle();
        }
    </script>
</body>
</html>

<script>
    $(function () {
        timeout = setTimeout(function () {
            window.location.href = "?pg=Vmsg&id=<?php echo $_GET['id'];?>";
        }, 120000);
    });

    $(document).on('mousemove', function () {
        if (timeout !== null) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            window.location.href = "?pg=Vmsg&id=<?php echo $_GET['id'];?>";
        }, 240000);
    });
//rola a div
    (function () {
        $('.messages').animate({scrollTop: 9999999}, 500);

    }.call(this));
</script>