<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}
$page="Chat-".$env->env_titulo;
$css="chat";
include_once("{$env->env_root}includes/head.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];

    //consulta se ha busca
    $sql = "SELECT * FROM tbl_users WHERE nome LIKE '%$sca%' AND `status`=1 AND `matriz`=2 order by nome";
}else {
//consulta se nao ha busca
    $sql = "SELECT * FROM tbl_users WHERE `status`=1 AND `matriz`=2 order by nome";
}


    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $userslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    ?>
    <nav class="navbar sticky-top navbar-light bg-light">
        <nav class='container'>
            <form class="" action="index.php" method="get">
                <input name="pg" value="Vmsgsc" hidden/>
                <div class="input-group">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" autofocus autocomplete="off" class="form-control input-sm col-lg-8" placeholder="Buscar por Nome..." name="sca" value="" />
                </div>
            </form>
        </nav>
    </nav>
<main class="container">
    <ul class="ml-3">
        <?php
        foreach($userslista as $item){
            ?>
            <li>
                <a href="<?php echo $env->env_url;?>caf/index.php?pg=Vmsg&id=<?php echo $item['id']; ?>" class='btn btn-sm btn-info mb-1' ><span class="glyphicon glyphicon-send"></span> <?php echo $item['nome']; ?></a>
            </li>
            <?php
        }
        ?>
    </ul>
</main>
</body>
</html>