<?php
class Ex
{
    public function fncexnew($id_sidim,$nome,$nome_social,$nascimento,$sexo,$cpf,$cns,$mae,$pai,$tel1,$tel2,$municipio,$bairro,$rua,$numero,$complemento)
    {
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome=ucwords(strtolower($nome));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $nome_social=ucwords(strtolower($nome_social));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));
        $mae=ucwords(strtolower($mae));
        $pai=ucwords(strtolower($pai));
        $municipio=remover_caracter(ucwords(strtolower($municipio)));
        $bairro=remover_caracter(ucwords(strtolower($bairro)));
        $rua=remover_caracter(ucwords(strtolower($rua)));
        $complemento=remover_caracter(ucwords(strtolower($complemento)));
        $rua=ucwords(strtolower($rua));
        $complemento=ucwords(strtolower($complemento));
        $nascimento=data2banco($nascimento);
        $cpf=limpadocumento($cpf);

        try{
            $sql="SELECT * FROM ";
            $sql.="mcu_sidim_pessoas";
            $sql.=" WHERE id_sidim=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id_sidim);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar==0){
            try {
                $sql = "INSERT INTO mcu_sidim_pessoas ";
                $sql .= "(id,  id_sidim, nome, nome_social, nascimento, sexo, cpf, cns, mae, pai, tel1, tel2, municipio, bairro, rua, numero, complemento)";
                $sql .= " VALUES ";
                $sql .= "(NULL, :id_sidim, :nome, :nome_social, :nascimento, :sexo, :cpf, :cns, :mae, :pai, :tel1, :tel2, :municipio, :bairro, :rua, :numero, :complemento)";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":id_sidim", $id_sidim);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":nome_social", $nome_social);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":sexo", $sexo);
                $insere->bindValue(":cpf", $cpf);
                $insere->bindValue(":cns", $cns);
                $insere->bindValue(":mae", $mae);
                $insere->bindValue(":pai", $pai);
                $insere->bindValue(":tel1", $tel1);
                $insere->bindValue(":tel2", $tel2);
                $insere->bindValue(":municipio", $municipio);
                $insere->bindValue(":bairro", $bairro);
                $insere->bindValue(":rua", $rua);
                $insere->bindValue(":numero", $numero);
                $insere->bindValue(":complemento", $complemento);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }



        if (isset($insere)) {
            $_SESSION['fsh']=[
                "flash"=>"Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vex_form");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new




}//fim da classe
