<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

///////////////////////////////////////////////////////////////////


try{
    $sql = "SELECT id, nome, guiche \n"
        . "FROM mpref_fila_pessoa\n"
        . "WHERE chamado=1 \n"
        . "ORDER BY id DESC LIMIT 0,5";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->bindParam(1, $senha);
    $consulta->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$lista=$consulta->fetchAll();


//retorna $senha, se foi chamado $chamado, $lista com os 5 ultimos nomes, é $proximo);
//retorna $senha, se foi chamado $chamado, $lista com os 5 ultimos nomes, é array $proximo

?>

<h1 class="azultemafonte display-4">ÚLTIMOS ATENDIDOS</h1>
<hr>
<?php
foreach ($lista as $lt){
    if ($lt['id']!=0){
        echo "<h1 class='display-4 azultemafonte negrito text-uppercase'>";

        echo "<span class='badge laranjatemafundo float-left mr-2'>";
        switch ($lt['guiche']){
            case 1:
                $glist = "Guichê 1";
                break;
            case 2:
                $glist = "Guichê 2";
                break;
            case 3:
                $glist = "Guichê 3";
                break;
            case 4:
                $glist = "Guichê 4";
                break;
            case 5:
                $glist = "Guichê 5";
                break;
            case 6:
                $glist = "Guichê 6";
                break;
            case 7:
                $glist = "Guichê 7";
                break;
            case 8:
                $glist = "Guichê 8";
                break;
            case 9:
                $glist = "Guichê 9";
                break;
            case 10:
                $glist = "Guichê Especial";
                break;
            case 11:
                $glist = "Sala 1";
                break;
            case 12:
                $glist = "Sala 2";
                break;
            case 13:
                $glist = "Sala 3";
                break;
            case 14:
                $glist = "Sala 4";
                break;
            case 15:
                $glist = "Sala 5";
                break;
            case 31:
                $glist = "Arrecadação";
                break;
            case 32:
                $glist = "Fazenda";
                break;
            case 50:
                $glist = "At. rápido";
                break;
            case 99:
                $glist = "Recepção";
                break;
            default:
                $glist = "VAZIO";
                break;
        }

//        echo $glist;
        echo "</span>";

        echo $lt['nome'];

        echo "</h1>";
    }
}
?>

