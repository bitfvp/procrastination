<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

    // Prepare a select statement
$sql = "SELECT mpref_log.id, mpref_log.pessoa AS codpessoa, mpref_pessoas.nome AS pessoa, tbl_users.nome AS profissional, mpref_log.data, mpref_log.descricao, mpref_loglista.atividade, mpref_log.atividade_tipo\n"
    . "FROM mpref_pessoas INNER JOIN (tbl_users INNER JOIN (mpref_loglista INNER JOIN mpref_log ON mpref_loglista.id = mpref_log.atividade) ON tbl_users.id = mpref_log.profissional) ON mpref_pessoas.id = mpref_log.pessoa\n"
    . "ORDER BY mpref_log.data DESC limit 0,25";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
$frase = $consulta->fetchall();
    $contcidade = $consulta->rowCount();
    $sql=null;
    $consulta=null;



    // Retornando frase em formato JSON
//    echo json_encode($frase);
?>
<style>
    #micro{
        font-size: 0.7em;
    }
</style>
<table id="micro" class="table table-striped table-hover table-sm">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">PESSOA</th>
        <th scope="col">PROFISSIONAL</th>
        <th scope="col">DATA</th>
        <th scope="col">DESCRIÇÃO</th>
        <th scope="col">ATIVIDADE</th>
        <th scope="col">ESTADO</th>

    </tr>
    </thead>

    <tbody>
<?php

foreach ($frase as $tx){
    $tempdata=datahoraBanco2data($tx[4]);
    if ($tx[7]==1){$temptipo="Novo";}
    if ($tx[7]==2){$temptipo="Leitura";}
    if ($tx[7]==3){$temptipo="Atualização";}
    if ($tx[7]==4){$temptipo="exclusão";}
    echo "<tr>";
    echo "<td>{$tx[0]}</td>";
    echo "<td><a href='index.php?pg=Vpessoa&id={$tx[1]}'>{$tx[2]}</a></td>";
    echo "<td>{$tx[3]}</td>";
    echo "<td>{$tempdata}</td>";
    echo "<td>{$tx[5]}</td>";
    echo "<td>{$tx[6]}</td>";
    echo "<td>{$temptipo}</td>";
    echo "</tr>";
}
?>
    </tbody>
</table>
