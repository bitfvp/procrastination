<?php
class Bairro{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarbairronovo($bairro){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_bairros ";
                $sql.="(id, bairro)";
                $sql.=" VALUES ";
                $sql.="(NULL, :bairro)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":bairro", $bairro);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vbl");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarbairroeditar($id,$bairro){
        //inserção no banco
        try{
            $sql="UPDATE mcu_bairros SET bairro=:bairro WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":bairro", $bairro);
            $up->bindValue(":id", $id);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($up)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vbl");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>