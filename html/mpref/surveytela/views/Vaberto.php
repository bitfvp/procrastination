<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_74"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="".$env->env_titulo;
$css="tela";

include_once("includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '220;URL={$env->env_url_mod}?s={$start}'>";
?>
    <div class="container-fluid" id="back">
            <div class="row m-4 ">
                <div class="col-12 text-center">
                    <img src="<?php echo $env->env_estatico; ?>img/mcu_opg.png" alt="" style="max-height: 150px;" class="mb-2">
                    <h1>Deixe sua sugestão, elogio ou crítica </h1>
                </div>
            </div>

                <div class="row m-2 mt-2">
                    <div class="col-md-8 offset-2 text-center mt-0">
                        <form action="index.php?pg=Vaberto&aca=newsugestao" method="post">
                            <div class="row">
                                <textarea id="descricao" AUTOFOCUS onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000" class="form-control" rows="5" name="descricao"></textarea>
                                <span id="cont">1000</span>/1000
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-success btn-lg mt-1">SALVAR CONTEÚDO</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="index.php?s=1"  class="btn btn-block btn-danger btn-lg mt-1">CANCELAR</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

        <div class="row">
            <div class="ocean">
                <div class="wave"></div>
                <div class="wave"></div>
            </div>
        </div>
    </div>
</body>
</html>
