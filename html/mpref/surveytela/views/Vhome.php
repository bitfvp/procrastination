<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_74"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="".$env->env_titulo;
$css="tela";

include_once("includes/head.php");
?>
<script>
    //funcao para o camarada apertar f11
    $(document).keydown(function (e) {
        if(e.which == 122)
        {
           // alert('Voce Apertou f11');
            //return false;
            document.getElementById('btntc1').style.display = 'none';
            document.getElementById('btntc2').style.display = 'block';
        }
    });
</script>
<?php
///se não tiver o get start nao inicia
$start=$_GET['s'];
if($start!=1){
    echo "<h1>";
    echo " <button type='button' id='btntc1' class='btn btn-block btn-lg btn-warning ' >";
    echo "Aperte F11 do teclado";
    echo "</button>";
    echo "<a id='btntc2' class='btn btn-block btn-lg btn-info' style='display: none' href='{$env->env_url_mod}?s=1'  >";
    echo "Clique aqui para começar";
    echo "</a>";
    echo "</h1>";
    exit;
}



echo"<META HTTP-EQUIV=REFRESH CONTENT = '30;URL={$env->env_url_mod}?s={$start}'>";


try{
    $sql = "SELECT * FROM "
        ."mpref_survey_pergunta "
        ."WHERE "
        ."status = 1 AND "
        ."estado = 0 "
        ."ORDER BY id ASC "
        ."LIMIT 0, 1";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
    $perguntaarr=$consulta->fetch();
    $quantidade=$consulta->rowCount();


    $tipo=$perguntaarr['tipo'];
    $pergunta=$perguntaarr['pergunta'];
    $id_pergunta=$perguntaarr['id'];
    ?>

    <div class="container-fluid" id="back">
        <?php
        if ($quantidade<>0) {
            ?>


            <div class="row m-4 ">
                <div class="col-12 text-center">
                    <img src="<?php echo $env->env_estatico; ?>img/mcu_opg.png" alt="" style="max-height: 150px;" class="mb-4">
                    <?php
                    $tamanho = strlen($pergunta);
                    if ($tamanho > 200) {
                        $tag = "H3";
                    }
                    if ($tamanho < 200) {
                        $tag = "H2";
                    }
                    if ($tamanho < 50) {
                        $tag = "H1";
                    }
                    echo "<" . $tag . "><strong>";
                    echo $pergunta;
                    echo "</strong></" . $tag . ">";
                    ?>
                </div>
            </div>
            <?php
            if ($tipo == 1) {
                ?>
                <div class="row m-2 mt-2">

                    <div class="col-md-2 offset-2 text-center mt-5">
                        <div class="row">
                            <div class="col-12">
                                <svg class="survey-option" viewBox="0 0 50 50" id="survey-optionruim">
                                    <circle class="fill" cx="25" cy="25" r="23.252"/>
                                    <circle class="strokeruim" cx="25" cy="25" r="23.252"/>
                                    <polyline class="checkmarkruim" points="13,24 24,36 46,-5 "/>
                                </svg>
                            </div>
                            <div class="col-12 mt-5">
                                <h3>Ruim</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 text-center mt-5">
                        <div class="row">
                            <div class="col-12">
                                <svg class="survey-option" viewBox="0 0 50 50" id="survey-optionregular">
                                    <circle class="fill" cx="25" cy="25" r="23.252"/>
                                    <circle class="strokeregular" cx="25" cy="25" r="23.252"/>
                                    <polyline class="checkmarkregular" points="13,24 24,36 46,-5 "/>
                                </svg>
                            </div>
                            <div class="col-12 mt-5">
                                <h3>Regular</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 text-center mt-5">
                        <div class="row">
                            <div class="col-12">
                                <svg class="survey-option" viewBox="0 0 50 50" id="survey-optionbom">
                                    <circle class="fill" cx="25" cy="25" r="23.252"/>
                                    <circle class="strokebom" cx="25" cy="25" r="23.252"/>
                                    <polyline class="checkmarkbom" points="13,24 24,36 46,-5 "/>
                                </svg>
                            </div>
                            <div class="col-12 mt-5">
                                <h3>Bom</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 text-center mt-5">
                        <div class="row">
                            <div class="col-12">
                                <svg class="survey-option" viewBox="0 0 50 50" id="survey-optionotimo">
                                    <circle class="fill" cx="25" cy="25" r="23.252"/>
                                    <circle class="strokeotimo" cx="25" cy="25" r="23.252"/>
                                    <polyline class="checkmarkotimo" points="13,24 24,36 46,-5 "/>
                                </svg>
                            </div>
                            <div class="col-12 mt-5">
                                <h3>Ótimo</h3>
                            </div>
                        </div>
                    </div>

                    <script>
                        (function () {
                            $("#survey-optionruim").click(function () {

                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=1&s=1";
                                    },
                                    1500);
                                return $("#survey-optionruim").toggleClass("animate select");
                            });

                        }).call(this);

                        (function () {
                            $("#survey-optionregular").click(function () {
                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=2&s=1";
                                    },
                                    1500);
                                return $("#survey-optionregular").toggleClass("animate select");
                            });

                        }).call(this);

                        (function () {
                            $("#survey-optionbom").click(function () {
                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=3&s=1";
                                    },
                                    1500);
                                return $("#survey-optionbom").toggleClass("animate select");
                            });

                        }).call(this);

                        (function () {
                            $("#survey-optionotimo").click(function () {
                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=4&s=1";
                                    },
                                    1500);
                                return $("#survey-optionotimo").toggleClass("animate select");
                            });

                        }).call(this);

                    </script>

                </div>


                <?php
            }

            if ($tipo == 2) {
                ?>
                <div class="row m-2 mt-2">
                    <link rel='stylesheet'
                          href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
                    <div class="col-md-2 offset-4 text-center mt-2">
                        <div class="middle">
                            <label>
                                <input type="radio" name="radio" id="survey-optionsim"/>
                                <div class="b-sim box">
                                    <span>Sim</span>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 text-center mt-2">
                        <div class="middle">
                            <label>
                                <input type="radio" name="radio" id="survey-optionnao"/>
                                <div class="b-nao box">
                                    <span>Não</span>
                                </div>
                            </label>
                        </div>
                    </div>

                    <script>
                        (function () {
                            $("#survey-optionsim").click(function () {

                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=1&s=1";
                                    },
                                    1000);
                            });

                        }).call(this);

                        (function () {
                            $("#survey-optionnao").click(function () {

                                setTimeout(
                                    function () {
                                        // location.reload();
                                        window.location = "?aca=srn&p=<?php echo $id_pergunta;?>&r=2&s=1";
                                    },
                                    1000);
                            });

                        }).call(this);

                    </script>

                </div>
                <?php
            }
        }else{
            if ($_SESSION["obrigado"]==1){
                ?>
                <div class="row m-4 ">
                    <div class="col-12 text-center">
                        <img src="<?php echo $env->env_estatico; ?>img/mcu_opg.png" alt="" style="max-height: 200px;" class="mb-4">
                        <h1><strong>Obrigado pela sua participação! </strong></h1>
                        <h1 class="text-info"><i class="far fa-thumbs-up fa-3x"></i></h1>
                        <a href="index.php?pg=Vaberto"  class="btn btn-outline-info btn-lg mt-1">
                            Para sugestões, elogios ou reclamaçoes
                            <BR>
                            CLICK AQUI
                        </a>
                    </div>
                </div>
                <?php
                $_SESSION['obrigado']=0;
            }else {
                ?>
                <div class="row m-4 ">
                    <div class="col-12 text-center">
                        <img src="<?php echo $env->env_estatico; ?>img/mcu_opg.png" alt="" style="max-height: 150px;" class="mb-2">
                        <h1>Queremos saber como foi sua experiência com o </h1>
                        <h1><strong>Hospital Municipal de Manhuaçu</strong></h1>
                        <h1>Sua opinião é muito importante para melhorar</h1>
                        <h1>a qualidade dos nossos serviços.</h1>
                        <a href="?aca=resetpesquisa&s=1" class="btn btn-xl btn-info mt-5">
                            <h1>Participe de nossa pesquisa</h1>
                        </a>
                    </div>
                </div>
                <?php
            }
        }

        ?>

        <div class="row">
            <div class="ocean">
                <div class="wave"></div>
                <div class="wave"></div>
            </div>
        </div>
    </div>
</body>
</html>
