<?php
class Resposta{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncrespostanew($id_prof, $pergunta, $valor){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_survey_resposta ";
                $sql.="(id, pesquisa,   pergunta,   valor,   prof)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pesquisa,      :pergunta,  :valor,  :prof)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pesquisa", $_SESSION["pesquisa"]);
                $insere->bindValue(":pergunta", $pergunta);
                $insere->bindValue(":valor", $valor);
                $insere->bindValue(":prof", $id_prof);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];



            //atualiza a tabela senha como chamada
            try{
                $sql = "UPDATE mpref_survey_pergunta SET estado = 1 WHERE id= ?";
                global $pdo;
                $altera=$pdo->prepare($sql);
                $altera->bindParam(1, $pergunta);
                $altera->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

            header("Location: index.php?s=1");
            exit();


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
}