<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-8">
            <?php include_once("includes/pessoacabecalhotop.php"); ?>

            <div class="card mt-2 mb-2">
                <div class="card-header bg-info text-light">
                    Histórico
                </div>
                <div class="card-body">
                    <?php
                    //existe um id e se ele é numérico
                    if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
                        // Captura os dados do cliente solicitado
                        $sql = "SELECT * "
                            . "FROM mpref_esporte_inscrito "
                            . "WHERE (((mpref_esporte_inscrito.cod_pessoa)=?)) order by id desc";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindParam(1, $_GET['id']);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $inscricoes = $consulta->fetchAll();
                        $sql = null;
                        $consulta = null;


                     foreach ($inscricoes as $insc){
                         echo "<blockquote class='blockquote blockquote-info'>";
                         echo "<i class='badge badge-secondary'>Equipe</i>  ";
                         echo fncgetequipe($insc['cod_equipe'])['equipe'];
                         echo "<br>";
                         echo "<i class='badge badge-primary'>Competição</i>  ";
                         echo "<a href='index.php?pg=Vcamp&id={$insc['cod_campeonato']}'>";
                             echo fncgetcampeonato($insc['cod_campeonato'])['campeonato']."<br>";
                                echo "<i class='badge badge-info'>Informação</i>  ";
                                echo "(".fncgetcampeonato($insc['cod_campeonato'])['descricao'].")";
                         echo "</a>";
                         echo "</blockquote>";
                     }

                    }
                    ?>
                </div>
            </div>

            <?php
            include_once("includes/atform.php");
            include_once("includes/at.php");
            ?>

        </div>
        <div class="col-md-4">
            <?php
            include_once ("includes/arquivosform.php");
            include_once ("includes/arquivos.php");
            ?>
        </div>

    </div>




</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>