<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Seção-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="secaosave";
    $secao=fncgetsecao($_GET['id']);
}else{
    $a="secaonew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vsecao_lista&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $secao['id']; ?>"/>
                <label for="">Seção eleitoral:</label>
                <input autocomplete="off" autofocus id="secao" type="text" class="form-control" name="secao" value="<?php echo $secao['secao']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="">Zona:</label>
                <input autocomplete="off" autofocus id="zona" type="text" class="form-control" name="zona" value="<?php echo $secao['zona']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="">Local:</label>
                <input autocomplete="off" autofocus id="local" type="text" class="form-control" name="local" value="<?php echo $secao['local']; ?>"/>
            </div>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>