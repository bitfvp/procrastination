<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="campeonato-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");


if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $competicao=fncgetcampeonato($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">

    <h3><?php echo $competicao['campeonato'];?></h3>
    Descrição: <strong><?php echo $competicao['descricao'];?></strong><br>
    Cadastrada em: <strong><?php echo dataRetiraHora($competicao['data_cadastro']);?></strong><br>

    <hr>
    <?php
    $sql = "SELECT * "
        ."FROM "
        ."mpref_esporte_equipe "
        ."WHERE campeonato=? "
        ."ORDER BY "
        ."equipe ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $competicao['id']);
    $consulta->execute();
    $equipes = $consulta->fetchall();
    $sql=null;
    $consulta=null;
    ?>

    <ul>
        <?php
        $c=1;
        foreach ($equipes as $item){
            echo "<li><strong>";
            echo $item['equipe'];
            echo "</strong></li>";
            $sql = "SELECT * "
                ."FROM "
                ."mpref_esporte_inscrito "
                ."WHERE cod_equipe=? "
                ."ORDER BY "
                ."id ASC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $item['id']);
            $consulta->execute();
            $inscritos = $consulta->fetchall();
            $sql=null;
            $consulta=null;

            echo "<table class='table table-sm'>";
            foreach ($inscritos as $insc){
                $idade = Calculo_Idade(fncgetpessoa($insc['cod_pessoa'])['nascimento']);
                echo "<tr>";
                echo "<td>";
                echo fncgetpessoa($insc['cod_pessoa'])['nome'];
                echo "</td>";

                echo "<td>";
                if ($idade>1000 or $idade<1){
                    echo "Não declarado";
                }else{
                    echo $idade." Anos";
                }
                echo "</td>";


                echo "</tr>";
            }
            echo "</table>";
            echo "<hr>";
        }
        ?>
    </ul>

</main>
</body>
</html>