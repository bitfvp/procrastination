<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Campeonato editar-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="campeonatosave";
    $campeonato=fncgetcampeonato($_GET['id']);
}else{
    $a="campeonatonew";
}
?>
<main class="container"><!--todo conteudo-->
    <form action="<?php echo "index.php?pg=Vcamp_editar&aca={$a}"; ?>" method="post" class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" value="SALVAR"  class="btn btn-success btn-block col-md-6">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input name="id"  id="id" type="hidden" value="<?php if (isset($campeonato['id'])){echo $campeonato['id'];} ?>"/>
                <label for="nome">Campeonato</label>
                <input name="campeonato" id="campeonato" autofocus type="text" class="form-control" value="<?php if (isset($campeonato['campeonato'])){echo $campeonato['campeonato'];} ?>">
            </div>
            <div class="col-md-12">
                <label for="descricao">Descrição:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,255,descricao,'cont')" maxlength="255" class="form-control" rows="2" name="descricao"><?php if (isset($campeonato['descricao'])){echo $campeonato['descricao'];} ?></textarea>
                <span id="cont">255</span>/255
            </div>
            <div class="col-md-6">
                <label>Status</label>
                <select name="status" required="true" class="form-control">
                    <option selected="" value="<?php if (isset($campeonato['status'])){echo $campeonato['status'];} ?>">
                        <?php
                        if (isset($campeonato['status'])){
                            if ($campeonato['status']==1){
                                echo "ATIVO";
                            }else{
                                echo "DESATIVADO";
                            }
                        }
                        ?>
                    </option>
                    <option value="1">ATIVO</option>
                    <option value="0">DESATIVADO</option>
                </select>
            </div>
        </div>

    </form>

</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>
