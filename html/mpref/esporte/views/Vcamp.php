<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Campeonato-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $campeonato=fncgetcampeonato($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-info text-light">
                    <?php echo $campeonato['campeonato'];?>
                </div>
                <div class="card-body">
                    Descrição: <strong><?php echo $campeonato['descricao'];?></strong><br>
                    Cadastrada em: <strong><?php echo dataRetiraHora($campeonato['data_cadastro']);?></strong><br>
                    Cadastrada por: <strong><?php echo fncgetusuario($campeonato['cod_usuario'])['nome'];?></strong><br><br>
                    Status: <strong>
                        <?php
                        if ($campeonato['status']==1){
                            echo "<strong class='text-success'>Campeonato Ativo</strong>";
                        }else{
                            echo "<strong class='text-danger'>Campeonato Desativado</strong>";
                        }
                        ?>
                    </strong>
                    <a class="btn btn-outline-primary btn-block" href="index.php?pg=Vcamp_print&id=<?php echo $_GET['id'];?>" target="_blank" title="">
                        IMPRIMIR CAMPEONATO
                    </a>
                    <a class="btn btn-success btn-block" href="index.php?pg=Vcamp_editar&id=<?php echo $campeonato['id']?>" title="Alterar dados da campeonato">
                        EDITAR CAMPEONATO
                    </a>
                </div>
            </div>
        </div>

        <?php
        $sql = "SELECT * FROM mpref_esporte_equipe WHERE campeonato=? order by equipe";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $campeonato['id']);
        $consulta->execute();
        $equipes = $consulta->fetchall();
        $equipes_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        ?>


        <div class="col-md-7">
            <table class="table table-sm table-bordered">
                <thead>
                <tr>
                    <th>Equipe</th>
                    <th>Editar</th>
                    <th>Remover</th>
                </tr>
                </thead>

                <tbody>
                <?php
                foreach ( $equipes as $equipe){
                    echo "<tr>";
                    echo "<td> <a href='index.php?pg=Vequipe&c=".$_GET['id']."&e=".$equipe['id']."'>".$equipe['equipe']."</a></td>";
                        echo "<td>";
                        echo "<a href='index.php?pg=Vequipe_editar&c=". $_GET['id']."&id=".$equipe['id']."' class='fas fa-pen'></td>";
                    echo "<td><a href=''>Desativado</a></td>";
                    echo "</tr>";

                }
                ?>
                <tr>
                    <td colspan="3"><a href="index.php?pg=Vequipe_editar&c=<?php echo $_GET['id']?>" class="btn btn-success btn-block my-2">NOVO TIME</a></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3" class="text-right"> Equipes</td>
                </tr>
                </tfoot>
            </table>

        </div>

    </div>
</main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>