<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>


<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <?php
                if ($allow["allow_65"]==1){ ?>
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
                    <?php
                }
                ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
                <label for="nome">NOME</label><input autocomplete="off" id="nome" type="text" class="form-control" name="nome" required value="<?php echo $pessoa['nome']; ?>"/>
            </div>
            <div class="col-md-3">
                <label for="nome_social">NOME SOCIAL</label>
                <input autocomplete="off" id="nome_social" type="text" class="form-control" name="nome_social" value="<?php echo $pessoa['nome_social']; ?>"/>
            </div>
            <div class="col-md-2">
                <label for="sexo">SEXO</label>
                <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                    <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['sexo'];
                    } ?>">
                        <?php
                        if ($pessoa['sexo'] == 0) {
                            echo "Selecione...";
                        }
                        if ($pessoa['sexo'] == 1) {
                            echo "Feminino";
                        }
                        if ($pessoa['sexo'] == 2) {
                            echo "Masculino";
                        }
                        if ($pessoa['sexo'] == 3) {
                            echo "Indefinido";
                        }
                        ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Feminino</option>
                    <option value="2">Masculino</option>
                    <option value="3">Indefinido</option>
                </select>

            </div>
            <div class="col-md-3">
                <label for="nascimento">NASCIMENTO</label>
                <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="cpf">CPF</label>
                <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                                 value="<?php echo $pessoa['rg']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#rg').mask('00.000.000.000', {reverse: true});
                    });
                </script>
            </div>
            <div class="col-md-2">
                <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                         value="<?php echo $pessoa['uf_rg']; ?>"/>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <label for="local_votacao">LOCAL DE VOTAÇÃO</label>
                <select name="local_votacao" id="local_votacao" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                    // vamos criar a visualização

                    <?php
                    $getlocal=fncgetsecao($pessoa['local_votacao']);
                    ?>
                    <option selected="" data-tokens="<?php echo $getlocal['secao']."-".$getlocal['local'];?>" value="<?php if ($pessoa['local_votacao'] == "") {
                                $z = 0;
                                echo $z;
                            } else {
                                echo $pessoa['local_votacao'];
                            }
                            ?>">
                        <?php echo $getlocal['secao']."-".$getlocal['local'];?>
                    </option>
                    <?php
                    foreach (fncsecaolist() as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['secao']."-".$item['local'];?>" value="<?php echo $item['id'];?>">
                            <?php echo $item['secao']."-".$item['local']; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="row">
            <div class="col-md-5">
                <label for="pai">PAI</label>
                <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $pessoa['pai']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="mae">MÃE</label>
                <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $pessoa['mae']; ?>"/>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-5">
                <label for="endereco">ENDEREÇO</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
            </div>
            <div class="col-md-2">
                <label for="numero">NÚMERO</label>
                <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="bairro">BAIRRO</label>
                <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                    // vamos criar a visualização

                    <?php
                    $bairroid = $pessoa['bairro'];
                    $getbairro=fncgetbairro($bairroid);
                    ?>
                    <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $pessoa['bairro']; ?>">
                        <?php echo $getbairro['bairro'];?>
                    </option>
                    <?php
                    foreach (fncbairrolist() as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                            <?php echo $item['bairro']; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-1">
                <?php
                if ($allow["allow_5"]==1){
                    $tempcaminho = $env->env_url;
                    switch ($_SESSION['matriz']){
                        case 1:
                            $tempcaminho.= "mcu/?pg=Vbl";
                            break;
                        case 2;
                            $tempcaminho.= "caf/?pg=Vbl";
                            break;
                    }
                    ?>
                    <label for="cad"><h6>CAD. BAIRRO</h6><a href="<?php echo $tempcaminho;?>" target="_blank"><i class="fa fa-plus"></i></a></label>
                <?php }?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label for="referencia">REFERÊNCIA</label>
                <input autocomplete="off" id="referencia" type="text" class="form-control" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
            </div>
            <div class="col-md-3">
                <label for="telefone">TELEFONE</label>
                <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>


        </div>
    </form>
</main>



<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>