<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Campeonato editar-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['c']) and is_numeric($_GET['c'])){
    if (isset($_GET['id']) and is_numeric($_GET['id'])){
        $a="equipesave";
        $equipe=fncgetequipe($_GET['id']);
    }else{
        $a="equipenew";
    }
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}

?>
<main class="container"><!--todo conteudo-->
    <form action="<?php echo "index.php?pg=Vequipe_editar&aca={$a}&c={$_GET['c']}"; ?>" method="post" class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" value="SALVAR"  class="btn btn-success btn-block col-md-6">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input name="id"  id="id" type="hidden" value="<?php if (isset($equipe['id'])){echo $equipe['id'];} ?>"/>
                <label for="nome">Equipe:</label>
                <input name="equipe" id="equipe" autofocus type="text" class="form-control" value="<?php if (isset($equipe['equipe'])){echo $equipe['equipe'];} ?>">
            </div>
        </div>

    </form>

</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>
