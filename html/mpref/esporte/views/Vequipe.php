<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Equipe-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['e']) and is_numeric($_GET['e'])){
    $equipe=fncgetequipe($_GET['e']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col-md-5">
            <div class="card">
                <div class="card-header bg-info text-light">
                    <a href="?pg=Vcamp&id=<?php echo $_GET['c'];?>" class="fa fa-backward text-danger">VOLTAR</a>
                    <?php echo $equipe['equipe'];?>
                </div>
                <div class="card-body">
                    Cadastrada em: <strong><?php echo dataRetiraHora($equipe['data_cadastro']);?></strong><br>
                    Cadastrada por: <strong><?php echo fncgetusuario($equipe['cod_usuario'])['nome'];?></strong><br><br>
                    <a class="btn btn-success btn-block" href="index.php?pg=Vequipe_editar&id=<?php echo $equipe['id']?>&c=<?php echo $_GET['c']?>" title="Alterar dados da equipe">
                        EDITAR EQUIPE
                    </a>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#serch1 input[type="text"]').on("keyup input", function () {
                    /* Get input value on change */
                    var inputVal = $(this).val();
                    var resultDropdown = $(this).siblings(".result");
                    if (inputVal.length) {
                        $.get("<?php echo $env->env_url_mod;?>includes/equipe_add_inscrito.php", {term: inputVal,cod_equipe: "<?php echo $equipe['id'];?>", pg: "<?php echo $_GET['pg'];?>",  c: "<?php echo $_GET['c'];?>"} ).done(function (data) {
                            // Display the returned data in browser
                            resultDropdown.html(data);
                        });
                    } else {
                        resultDropdown.empty();
                    }
                });

                // Set search input value on click of result item
                $(document).on("click", ".result p", function () {
                    $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
                    $(this).parent(".result").empty();
                });
            });
        </script>



        <?php
        $sql = "SELECT * FROM mpref_esporte_inscrito WHERE cod_equipe=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['e']);
        $consulta->execute();
        $inscritos = $consulta->fetchall();
        $inscritos_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        ?>


        <div class="col-md-7" id="serch1">
            <?php

            echo "<input type='text' class='form-control input-sm' autofocus id='admembro' placeholder='Adicionar atleta na equipe'>";
            ?>
            <div class="result mt-2"></div>
            <h6 class="text-info mt-2">
                <?php echo $inscritos_count." inscrito(s) nessa equipe";?>
            </h6>
            <div class="list-group">
                <?php
                foreach ($inscritos as $item){
                    //
                    echo "<div class='list-group-item'>";
                    echo "<a href='index.php?pg=Vpessoa&id=".fncgetpessoa($item['cod_pessoa'])['id']."'>".fncgetpessoa($item['cod_pessoa'])['nome']."</a>";
                    echo " <i class='badge badge-info' title='Idade'>".Calculo_Idade(fncgetpessoa($item['cod_pessoa'])['nascimento'])."</i>";

                        echo "<a class='badge badge-danger float-right' data-position='top' data-tooltip='Remover pessoa da equipe' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&e={$_GET['e']}&c={$_GET['c']}&aca=exclequipe&membro={$item['id']}'>";
                        echo "<span class='fa fa-times'>";
                        echo "</a>";

                    echo "</div>";
                }
                ?>
            </div>
        </div>

    </div>
</main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>