<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Campeonato busca-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    //consulta se ha busca
    $sql = "select * from mpref_esporte_campeonato WHERE campeonato LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mpref_esporte_campeonato ";
}
// total de registros a serem exibidos por página
$total_reg = "20"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
if (isset($_GET['pgn'])){
    $pgn=$_GET['pgn'];
}

if (!isset($pgn)) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data_cadastro desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data_cadastro desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4>Campeonatos</h4>
            <hr>
            <form action="index.php" method="get">
                <div class="input-group mb-3 col-md-6 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                    <input name="pg" value="Vcamp_lista" hidden/>
                    <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Pesquisar campeonato..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                </div>
            </form>
            <a href="index.php?pg=Vcamp_editar" class="btn btn btn-success btn-block col-md-6 float-right">
                NOVO CAMPEONATO
            </a>

            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-sm table-striped">
                    <thead>
                    <tr>
                        <th>Competição</th>
                        <th>Descrição</th>
                        <th>Status</th>
                        <th>Cadastrada em:</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if(isset($_GET['sca']) and $_GET['sca']!="") {
                        $sta = strtoupper($_GET['sca']);
                        define('CSA', $sta);//TESTE
                    }
                    // vamos criar a visualização
                    foreach ($limite as $dados){
                        $id = $dados["id"];
                        $campeonato = strtoupper($dados["campeonato"]);
                        $descricao = $dados["descricao"];
                        $data = dataRetiraHora($dados["data_cadastro"]);

                        if ($dados["status"]==1){
                            $status = "<strong class='text-success'>Campeonato Ativo</strong>";
                        }else{
                            $status = "<strong class='text-danger'>Campeonato Desativado</strong>";
                        }

                        ?>
                    <tr>
                        <td>
                            <a href="index.php?pg=Vcamp&id=<?php echo $id;?>">
                            <?php
                            if(isset($_GET['sca']) and $_GET['sca']!="") {
                                $nnn = $campeonato;
                                $nn = explode(CSA, $nnn);
                                $n = implode("<span class='text-success'>{$sta}</span>", $nn);
                                echo $n;
                            }else{
                                echo $campeonato;
                            }
                            ?>
                            </a>
                        </td>
                        <td><?php echo $descricao;?></td>
                        <td><?php echo $status;?></td>
                        <td><?php echo $data;?></td>
                        <td>
                            <a href="index.php?pg=Vcamp_editar&id=<?php echo $id;?>" title="Edite">
                                Alterar
                            </a>
                        </td>
                    </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                    <?php
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Voficina_listae&pgn={$anterior}&sca={$_GET['sca']}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                    }
                    echo "|";
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Voficina_lista&pgn={$proximo}&sca={$_GET['sca']}'>Próximo &#187;</a></li>";
                    }
                    ?>
                    </ul>
                </nav>
            </div>

            <div class="col-md-6">
                <ul class="text-info float-right">
                    <li><?php echo $tr;?> Oficina(s) listada(s)</li>
                </ul>
            </div>
        </div>
    </div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>