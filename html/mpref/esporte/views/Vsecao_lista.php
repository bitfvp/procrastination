<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de Seções Eleitorais-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    $sql = "select * from mpref_esporte_secaoeleitoral WHERE secao LIKE '%$sca%' ";
}else {

    $sql = "select * from mpref_esporte_secaoeleitoral where id=0 ";
}

$total_reg = "50"; // número de registros por página

$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}

$inicio = $pc - 1;
$inicio = $inicio * $total_reg;

try{
    $sql2= "ORDER BY secao LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY secao LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Secções eleitorais</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vsecao_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por secao..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vsecao_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVA SEÇÃO
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vsecao_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vsecao_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="3" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th>Seção</th>
            <th>Zona</th>
            <th>Local</th>
            <th>Editar</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vsecao_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vsecao_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="3" class="text-info text-right"><?php echo $tr;?> secão(s) listada(s)</th>
        </tr>
        </tfoot>

<tbody>
 <?php
 $sta = strtoupper($_GET['sca']);
 define('CSA', $sta);//TESTE
 foreach ($limite as $dados){
     $id = $dados["id"];
     $secao = strtoupper($dados["secao"]);
     $zona = strtoupper($dados["zona"]);
     $local = strtoupper($dados["local"]);
            ?>

	<tr>
        <td scope="row" id="<?php echo $id;  ?>">
            <a href="#" title="Ver secao">
                <?php
                if($_GET['sca']!="") {
                    $sta = CSA;
                    $nnn = $secao;
                    $nn = explode(CSA, $nnn);
                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                    echo $n;
                }else{
                    echo $secao;
                }
                ?>
            </a>
        </td>
        <td><?php echo $zona; ?></td>
        <td><?php echo $local; ?></td>
        <td><a href="index.php?pg=Vsecao_editar&id=<?php echo $id; ?>">Alterar</a></td>

	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
