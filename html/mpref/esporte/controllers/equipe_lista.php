<?php

function fncgetequipe($id){
    $sql = "SELECT * FROM mpref_esporte_equipe WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getequipe = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getequipe;
}
