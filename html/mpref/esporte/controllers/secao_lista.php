<?php
function fncsecaolist(){
    $sql = "SELECT * FROM mpref_esporte_secaoeleitoral ORDER BY secao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $secaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $secaolista;
}

function fncgetsecao($id){
            $sql = "SELECT * FROM mpref_esporte_secaoeleitoral WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getsecao = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getsecao;
}