<?php
function fnccampeonatolist(){
    $sql = "SELECT * FROM mpref_esporte_campeonato ORDER BY campeonato";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $campeonatolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $campeonatolista;
}

function fncgetcampeonato($id){
    $sql = "SELECT * FROM mpref_esporte_campeonato WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getcampeonato = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcampeonato;
}
