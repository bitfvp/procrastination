<?php
//inclui a classe a ser herdada um nivel abaixo da raiz que é onde estamos agora
//Env possui as configuracoes de ambientes mais abrangentes
//e tudo depende delas
include_once("../../../models/Env.class.php");

/*
 * ConfigMod será a classe contruida com base na original Env
 * e herdara todos os seus atributos
 * ela sera toda a referencia que a aplicação precisara
 */
Class ConfigMod extends Env {
    //todos as variaveis que estao sendo acrescentados nesse momento
    //vazem parte do ponto de vista do modulo em que estamos

    //sera root do ponto de vista deste modulo
    public $env_root_mod;
    //sera home doo ponto de visata deste modulo
    public $env_url_mod;
    //nome do modulo
    public $env_mod_nome;
    //será a juncao do nome da aplicação mais o nome do moodulo
    public $env_titulo;

    function __construct(){
        //parent::__construction é para que a funcão construction da classe pai tambem seja executada
        parent::__construct();
        $this->env_root_mod= $this->env_root.$this->env_htmlpasta."mpref/esporte/";
        $this->env_url_mod= $this->env_url."mpref/esporte/";
        $this->env_mod_nome=" Esporte ";
        $this->env_titulo=$this->env_mod_nome . $this->env_nome;
    }
}