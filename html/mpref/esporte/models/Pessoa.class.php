<?php
class Pessoa{
    public function fncpessoaedit(
        $id,
        $nome,
        $nome_social,
        $sexo,
        $nascimento,
        $cpf,
        $rg,
        $uf_rg,
        $local_votacao,
        $endereco,
        $numero,
        $bairro,
        $referencia,
        $telefone,
        $mae,
        $pai
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mpref_esporte_pessoas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE mpref_esporte_pessoas";
                $sql.=" SET";
                $sql .= " nome=:nome, 
                nome_social=:nome_social, 
                sexo=:sexo, 
                nascimento=:nascimento,
                cpf=:cpf, 
                rg=:rg, 
                uf_rg=:uf_rg,
                local_votacao=:local_votacao,
                endereco=:endereco, 
                numero=:numero, 
                bairro=:bairro, 
                referencia=:referencia, 
                telefone=:telefone, 
                mae=:mae, 
                pai=:pai
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":nome_social", $nome_social);
                $atualiza->bindValue(":sexo", $sexo);
                $atualiza->bindValue(":nascimento", $nascimento);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":uf_rg", $uf_rg);
                $atualiza->bindValue(":local_votacao", $local_votacao);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":referencia", $referencia);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":mae", $mae);
                $atualiza->bindValue(":pai", $pai);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew(
                                  $nome,
                                  $nome_social,
                                  $sexo,
                                  $nascimento,
                                  $cpf,
                                  $rg,
                                  $uf_rg,
                                  $local_votacao,
                                  $endereco,
                                  $numero,
                                  $bairro,
                                  $referencia,
                                  $telefone,
                                  $mae,
                                  $pai
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));

        if($sexo==""){
            $sexo="0";
        }
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        if($bairro==""){
            $bairro="0";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mpref_esporte_pessoas";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();

        try{
            $sql="SELECT * FROM ";
                $sql.="mpref_esporte_pessoas";
            $sql.=" WHERE rg=:rg";
            global $pdo;
            $consultarg=$pdo->prepare($sql);
            $consultarg->bindValue(":rg", $rg);
            $consultarg->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarrg=$consultarg->rowCount();



        if(($contarrg==0)or ($rg=="")){
        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO mpref_esporte_pessoas ";
                    $sql .= "(id,
                    data_cadastro, 
                    resp_cadastro, 
                    nome, 
                    nome_social, 
                    sexo, 
                    nascimento,
                    cpf, 
                    rg, 
                    uf_rg,
                    local_votacao,
                    endereco, 
                    numero, 
                    bairro, 
                    referencia, 
                    telefone,
                    mae, 
                    pai
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    CURRENT_TIMESTAMP, 
                    :resp_cadastro, 
                    :nome, 
                    :nome_social, 
                    :sexo, 
                    :nascimento,
                    :cpf, 
                    :rg, 
                    :uf_rg,
                    :local_votacao,
                    :endereco, 
                    :numero, 
                    :bairro, 
                    :referencia, 
                    :telefone, 
                    :mae, 
                    :pai
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":resp_cadastro", $_SESSION['id']);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":nome_social", $nome_social);
                    $insere->bindValue(":sexo", $sexo);
                    $insere->bindValue(":nascimento", $nascimento);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":uf_rg", $uf_rg);
                    $insere->bindValue(":local_votacao", $local_votacao);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":referencia", $referencia);
                    $insere->bindValue(":telefone", $telefone);
                    $insere->bindValue(":mae", $mae);
                    $insere->bindValue(":pai", $pai);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse RG!!",
                "type"=>"warning",
            ];
        }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="mpref_esporte_pessoas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vpessoa&id={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }
}
?>