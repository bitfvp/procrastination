<?php

class Equipe_inscrito
{

    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function addinscrito($cod_equipe,$cod_pessoa,$cod_campeonato,$cod_usuario)
    {
        //ver se ha ja ha a inscricao
        try{
            $sql="SELECT id FROM ";
            $sql.="mpref_esporte_inscrito";
            $sql.=" WHERE cod_equipe=:cod_equipe and cod_pessoa=:cod_pessoa";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":cod_equipe", $cod_equipe);
            $consulta->bindValue(":cod_pessoa", $cod_pessoa);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();



        if ($contar==0) {
            try {
                $sql = "INSERT INTO mpref_esporte_inscrito (`id`, `cod_equipe`, `cod_pessoa`, `cod_usuario`, `cod_campeonato` )"
                    . " VALUES(NULL, :cod_equipe, :cod_pessoa, :cod_usuario, :cod_campeonato )";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":cod_equipe", $cod_equipe);
                $insere->bindValue(":cod_pessoa", $cod_pessoa);
                $insere->bindValue(":cod_campeonato", $cod_campeonato);
                $insere->bindValue(":cod_usuario", $cod_usuario);
                $insere->execute();
            } catch (PDOException $error_msg) {
                echo 'Erroff' . $error_msg->getMessage();
            }

            if (isset($insere)) {
                //////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////////////

                $_SESSION['fsh']=[
                    "flash"=>"Adicionada a Oficina!!",
                    "type"=>"success",
                ];
                header("Location: index.php?pg=Vequipe&e={$cod_equipe}&c={$cod_campeonato}");
                exit();

            } else {
                if (empty($_SESSION['fsh'])) {
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];

                }
            }

        }else{
            $_SESSION['fsh']=[
                "flash"=>"Essa pessoa já esta inscrita na oficina!!",
                "type"=>"danger",
            ];
            header("Location: index.php?pg=Vequipe&e={$cod_equipe}&c={$cod_campeonato}");
            exit();
        }

    }



    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncexcinscrito($id_inscricao)
    {

        try {
            $sql = "DELETE FROM `mpref_esporte_inscrito` WHERE id = :id ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":id", $id_inscricao);
            $exclui->execute();
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        if (isset($exclui)) {
            //////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Membro Removido!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vequipe&e={$_GET['e']}&c={$_GET['c']}");
            exit();

        } else {
            if (empty($_SESSION['fsh'])) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}

?>