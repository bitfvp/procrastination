<?php 
class Equipe{
    public function fncequipenew($equipe, $campeonato, $cod_usuario){

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_esporte_equipe (`id`, `equipe`, `campeonato`, `cod_usuario`)"
                                ." VALUES(NULL, :equipe, :campeonato, :cod_usuario)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":equipe",$equipe);
                $insere->bindValue(":campeonato",$campeonato);
                $insere->bindValue(":cod_usuario",$cod_usuario);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vcamp&id={$campeonato}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncequipeedit($id, $equipe, $campeonato, $cod_usuario){
        try{
            $sql="SELECT id FROM ";
            $sql.="mpref_esporte_equipe";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //inserção no banco
            try{
                $sql="UPDATE mpref_esporte_equipe SET "
                    ."equipe=:equipe, campeonato=:campeonato "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":equipe",$equipe);
                $update->bindValue(":campeonato",$campeonato);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa equipe cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcamp&id={$campeonato}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao


}//fim class
