<?php
class Secao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsecaonew($secao,$zona,$local){

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_esporte_secaoeleitoral ";
                $sql.="(id, secao, zona, local)";
                $sql.=" VALUES ";
                $sql.="(NULL, :secao, :zona, :local)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":secao", $secao);
                $insere->bindValue(":zona", $zona);
                $insere->bindValue(":local", $local);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vsecao_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsecaoedit($id,$secao,$zona,$local){

        //inserção no banco
        try{
            $sql="UPDATE mpref_esporte_secaoeleitoral SET secao=:secao, zona=:zona, local =:local WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":secao", $secao);
            $insere->bindValue(":zona", $zona);
            $insere->bindValue(":local", $local);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vsecao_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>