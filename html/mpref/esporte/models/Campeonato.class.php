<?php 
class Campeonato{
    public function fnccampeonatonew($status, $campeonato, $descricao, $cod_usuario){

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_esporte_campeonato (`id`, `status`, `campeonato`, `descricao`, `cod_usuario`)"
                                ." VALUES(NULL, :status, :campeonato, :descricao, :cod_usuario)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":status",$status);
                $insere->bindValue(":campeonato",$campeonato);
                $insere->bindValue(":descricao",$descricao);
                $insere->bindValue(":cod_usuario",$cod_usuario);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="mpref_esporte_campeonato";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;
            $maid=$mid[0];
            header("Location: index.php?pg=Vcamp&id={$maid}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccampeonatoedit($id, $status, $campeonato, $descricao){
        try{
            $sql="SELECT id FROM ";
            $sql.="mpref_esporte_campeonato";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //inserção no banco
            try{
                $sql="UPDATE mpref_esporte_campeonato SET "
                    ."campeonato=:campeonato, descricao=:descricao, "
                    ."status=:status "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":campeonato",$campeonato);
                $update->bindValue(":descricao",$descricao);
                $update->bindValue(":status",$status);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa campeonato cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcamp&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao


}//fim class
