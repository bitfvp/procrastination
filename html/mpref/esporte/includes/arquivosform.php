<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Envio de arquivos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Vpessoa&aca=novoarquivo&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="arquivos" type="file" class="custom-file-input" name="arquivos[]" value="" multiple/>
                        <label class="custom-file-label" for="arquivos">Arquivos do titular...</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="ENVIAR ARQUIVOS"/>
                </div>
            </div>
        </form>
    </div>
</div>