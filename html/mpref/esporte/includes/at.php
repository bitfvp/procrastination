<?php
    //existe um id e se ele é numérico
    if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * "
            . "FROM mpref_esporte_at "
            . "WHERE (((mpref_esporte_at.pessoa)=?)) order by data desc";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $ativi = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }
?>

<div id="pointofview" class="card">
    <div class="card-header bg-info text-light">
        Acontecimentos
    </div>
    <div class="card-body">
        <h5>
            <?php
            foreach ($ativi as $at) {
                ?>
                <hr>
                <blockquote class="blockquote blockquote-info">
                    “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                    <br>
                    <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                    <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($at['profissional']);
                        echo $us['nome'];
                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                        ?>
                    </footer>

                </blockquote>
                <?php
            }
            ?>
        </h5>
    </div>
</div>