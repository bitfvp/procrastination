<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pergunta-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="surveyperguntasave";
    $pergunta=fncgetsurveypergunta($_GET['id']);
}else{
    $a="surveyperguntanew";
}
?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vhome&aca=<?php echo $a;?>" method="post">
            <div class="row">
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <input id="id" type="hidden" class="" name="id" value="<?php echo $pergunta['id']; ?>"/>
                    <label for="pergunta">Pergunta:</label>
                    <input autocomplete="off" id="pergunta" autofocus type="text" class="form-control" name="pergunta" placeholder="Digite a pergunta..." value="<?php echo $pergunta['pergunta']?>" />
                </div>

                <div class="col-md-12">
                    <label for="tipo">Tipo:</label>
                    <select name="tipo" id="tipo" class="form-control">// vamos criar a visualização
                        <option selected="" value="<?php if ($pergunta['tipo'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pergunta['tipo'];
                        } ?>">
                            <?php
                            if ($pergunta['tipo'] == 1) {
                                echo "Escala";
                            }
                            if ($pergunta['tipo'] == 2) {
                                echo "SIM/NÃO";
                            }
                            ?>
                        </option>
                        <option value="1">Escala</option>
                        <option value="2">SIM/NÃO</option>
                    </select>
                </div>

                <div class="col-md-12">
                    <label for="status">status:</label>
                    <select name="status" id="status" class="form-control">// vamos criar a visualização
                        <option selected="" value="<?php if ($pergunta['status'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pergunta['status'];
                        } ?>">
                            <?php
                            if ($pergunta['status'] == 0) {
                                echo "Desativado";
                            }
                            if ($pergunta['status'] == 1) {
                                echo "Ativo";
                            }
                            ?>
                        </option>
                        <option value="0">Desativado</option>
                        <option value="1">Ativo</option>
                    </select>
                </div>

            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>