<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}

$page="Relatório de Produção-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");


// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";


//$sql = "SELECT Count(mpref_fila_log.prof) AS total, mpref_fila_log.prof "
//."FROM mpref_fila_log "
//."WHERE (((mpref_fila_log.data)>=:inicial) And ((mpref_fila_log.data)<=:final)) "
//."GROUP BY mpref_fila_log.prof ";
//    global $pdo;
//    $consulta = $pdo->prepare($sql);
//    $consulta->bindValue(":inicial",$inicial);
//    $consulta->bindValue(":final",$final);
//    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
//    $ati = $consulta->fetchAll();
//    $sql=null;
//    $consulta=null;

$sql = "SELECT * FROM mpref_survey_pergunta ORDER BY mpref_survey_pergunta.id ASC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$perguntas = $consulta->fetchAll();
$sql=null;
$consulta=null;
?>
<div class="container">
        <h1>Relatório de perguntas fechadas </h1>
        <?php
        foreach ($perguntas as $pe){
            echo $pe['pergunta']."<br>";

//WHERE (((mpref_survey_resposta.data)>=:inicial) And ((mpref_survey_resposta.data)<=:final))


            $sql = "SELECT Count(mpref_survey_resposta.id) AS quantidade, mpref_survey_resposta.valor AS valor FROM mpref_survey_resposta "
                ."WHERE "
                ."(((mpref_survey_resposta.data)>=:inicial) And ((mpref_survey_resposta.data)<=:final) and mpref_survey_resposta.pergunta = :pergunta )"
                ."GROUP BY mpref_survey_resposta.valor";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
            $consulta->bindValue(":pergunta",$pe['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $valor = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    foreach ($valor as $val){

        if ($pe['tipo']==1){
            switch ($val['valor']){
                case 1:
                    echo "Ruim: ".$val['quantidade']."<br>";
                    break;
                case 2:
                    echo "Regular: ".$val['quantidade']."<br>";
                    break;
                case 3:
                    echo "Bom: ".$val['quantidade']."<br>";
                    break;
                case 4:
                    echo "Ótimo: ".$val['quantidade']."<br>";
                    break;
            }
        }

        if ($pe['tipo']==2){
            switch ($val['valor']){
                case 1:
                    echo "Sim: ".$val['quantidade']."<br>";
                    break;
                case 2:
                    echo "Não: ".$val['quantidade']."<br>";
                    break;
            }
        }
    }
echo "<br><br>";


        }
        ?>
</div>
</body>
</html>