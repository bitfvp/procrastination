<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}

$page="Relatório de Produção-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");


// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT * FROM mpref_survey_sugestao "
    ."WHERE "
    ."(((mpref_survey_sugestao.data)>=:inicial) And ((mpref_survey_sugestao.data)<=:final)) "
    ."ORDER BY mpref_survey_sugestao.data ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$sql=null;
$consulta=null;

?>
<div class="container">
    <h1>Relatório de sugestões  </h1>
    <table class="table table-responsive table-bordered table-sm">
        <thead>
        <tr>
            <th>Data</th>
            <th>Conteúdo</th>
        </tr>
        </thead>
        <tbody>

            <?php
            foreach ($valor as $val){
                echo "<tr>";
                echo "<td>";
                echo datahoraBanco2data($val['data']);
                echo "</td>";
                echo "<td>";
                echo $val['descricao'];
                echo "</td>";
                echo "</tr>";
            }
            ?>

        </tbody>
    </table>
</div>
</body>
</html>