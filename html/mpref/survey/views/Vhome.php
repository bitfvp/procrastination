<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}



$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '60;URL={$env->env_url_mod}'>";


?>
<main class="container"><!--todo conteudo-->
<?php


    try{
        $sql = "SELECT * from mpref_survey_pergunta ORDER BY id ";
        global $pdo;
        $people=$pdo->prepare($sql);
        $people->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $perguntas=$people->fetchAll();
    $tr=$people->rowCount();// verifica o número total de registros

?>
<div class="row">
    <div class="col-md-3">
        <a href="index.php?pg=Vperguntaeditar" class="btn btn-lg btn-success btn-block">
            NOVO
        </a>
    </div>

</div>
<!-- ====================================================== -->
    <br>
    <table class="table table-hover table-sm table-striped">
        <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">PERGUNTA</th>
                    <th scope="col">TIPO</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">PROF.</th>
                    <th>EDITAR</th>
                </tr>
                </thead>
                <tfoot class="">
                <tr>
                    <td colspan="6" class="text-right"><?php echo $tr;?> Pergunta(s) listada(s)</td>
                </tr>
                </tfoot>

        <tbody>
                <?php
                foreach ($perguntas as $pa){
                    switch ($pa['prof']){
                        case 0:
                            $fila_user ="-----";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        default:
                            $sql = "SELECT nome FROM tbl_users WHERE id = $pa[prof]";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $fila_user = $consulta->fetch();
                            $sql=null;
                            $consulta=null;
                            $p_nome = explode(" ", $fila_user['nome']);
                            break;
                    }


                    ?>

                    <tr class="<?php echo $corline;?>">
                        <th scope="row" id="<?php echo $id;  ?>" class="<?php echo $corcontraste;?>">
                            <?php echo $pa['id']; ?>
                        </th>
                        <td class="<?php echo $corcontraste;?>"><?php echo $pa['pergunta']; ?></td>
                        <td style="width: 100px;">
                            <?php
                            if ($pa['tipo']==1){
                                echo "<span class=''>"
                                    ."<i class='fas fa-star'></i>"
                                    ."<i class='fas fa-star'></i>"
                                    ."<i class='fas fa-star'></i>"
                                    ."<i class='fas fa-star'></i>"
                                    ."<i class='fas fa-star-half-alt'></i>"
                                    ."</span>";
                            }
                            if ($pa['tipo']==2){
                                echo "<span class=''>"
                                    ."<i class='badge badge-info'>SIM</i>"
                                    ."<i class='badge badge-warning'>NÃO</i>"
                                    ."</span>";
                            }
                            ?>
                        </td>

                        <td>
                            <?php
                            if($pa['status']==1){
                                echo"<i class='fas fa-hand-peace text-success'>Ativo</i>";
                            }else{
                                echo"<i class='fas fa-thumbs-down text-danger'>Desativado</i>";
                            }

                            ?>
                        </td>
                        <td class=""><?php echo $p_nome[0]; ?></td>
                        <td><a href="index.php?pg=Vperguntaeditar&id=<?php echo $pa['id'];  ?>" class="fa fa-pen"></a></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
