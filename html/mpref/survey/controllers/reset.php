<?php 
//metodo de selecionar
if($startactiona==1 && $aca=="resetresposta"){
    //limpa pessoas
    try{
        $sql="TRUNCATE TABLE `mpref_survey_resposta` ";
        global $pdo;
        $atualiza=$pdo->prepare($sql);
        $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Respostas zeradas com sucesso!!",
        "type"=>"success",
        "error"=>"",
    ];
    header("Location: {$env->env_url_mod}");
    exit();
}


if($startactiona==1 && $aca=="resettudo"){
    //limpa pessoas
    try{
        $sql="TRUNCATE TABLE `mpref_survey_resposta` ";
        global $pdo;
        $atualiza=$pdo->prepare($sql);
        $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    try{
        $sql="TRUNCATE TABLE `mpref_survey_pergunta` ";
        global $pdo;
        $atualiza=$pdo->prepare($sql);
        $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Perguntas e respostas zeradas com sucesso!!",
        "type"=>"success",
        "error"=>"",
    ];
    header("Location: {$env->env_url_mod}");
    exit();
}




if($startactiona==1 && $aca=="resetsugestao"){
    //limpa pessoas
    try{
        $sql="TRUNCATE TABLE `mpref_survey_sugestao` ";
        global $pdo;
        $atualiza=$pdo->prepare($sql);
        $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Perguntas e respostas zeradas com sucesso!!",
        "type"=>"success",
        "error"=>"",
    ];
    header("Location: {$env->env_url_mod}");
    exit();
}