<?php
class Pergunta{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncperguntanew( $id_prof, $pergunta, $tipo, $status){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_survey_pergunta ";
                $sql.="(id, pergunta, tipo, status, prof)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pergunta, :tipo, :status, :prof)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pergunta", $pergunta);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":prof", $id_prof);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

//            header("Location: index.php?pg=Vhome");
//            exit();


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncperguntaedit($id, $pergunta, $tipo, $status){
        //tratamento das variaveis

        //inserção no banco
        try{
            $sql="UPDATE mpref_survey_pergunta SET ";
            $sql.="pergunta=?, tipo=?, status=? ";
            $sql.=" WHERE id=?";
            global $pdo;
            $upd=$pdo->prepare($sql);
            $upd->bindParam(1, $pergunta);
            $upd->bindParam(2, $tipo);
            $upd->bindParam(3, $status);
            $upd->bindParam(4, $id);
            $upd->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($upd)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro Editado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
}