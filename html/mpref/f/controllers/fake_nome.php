<?php
function fncgetfakenome ($seletor){
    $fakes = [
        '0' => 'Pessoa aguardando',
        '1' => 'Raul Seixas',
        '2' => 'Getulio Vargas',
        '3' => 'Michel Jackson',
        '4' => 'Tim Maia',
        '5' => 'José Rico',
        '6' => 'Elvis',
        '7' => 'Leonel Brizola',
        '8' => 'Enéas Carneiro',
        '9' => 'Ayrton Senna',
        '10' => 'Mestre Miyage ',
        '11' => 'Mortícia Addams',
        '12' => 'Tio Phil',
        '13' => 'Roque Santeiro',
        '14' => 'Paul McCartney',
        '15' => 'Giovanni Improtta',
        '16' => 'Cazuza',
        '17' => 'Freddie Mercury',
        '18' => 'Luiz Gonzaga',
        '19' => 'Gugu Liberato',
        '20' => 'Pablo Escobar',
        '21' => 'Mussum',
        '22' => 'Zacarias',
        '23' => 'Princesa Diana',
        '24' => 'Noé',
        '25' => 'Cristovão Colombo',
        '26' => 'Roberto Bolaños',
        '27' => 'Dom Ramón Valdés',
        '28' => 'Godinez',
        '29' => 'Jaiminho',
        '30' => 'Dona Clotilde',
        '31' => 'Professor Girafales',
        '32' => 'Serafim Tibúrcio',
        '33' => 'Xica da Silva',
        '34' => 'Vito Corleone',
        '35' => 'Rei Leônidas',
        '36' => 'Nazaré Tedesco',
        '37' => 'Odete Roitman',
        '38' => 'Paola Bracho',
        '39' => 'Maria Mercedes',
        '40' => 'Marisol',
        '41' => 'Sinhozinho Malta',
        '42' => 'Odorico Paraguaçu',
        '43' => 'Viúva Porcina',
        '44' => 'Hilda Furacão',
        '45' => 'Agostinho Carrara',
        '46' => 'Beto Carrero',
        '47' => 'Chico Anysio',
        '48' => 'Ronald Golias',


    ];
    if ( ($seletor!=0 and $seletor<=48 and $seletor>=1) ){
        return $fakes[$seletor];
    }else{
        return $fakes[0];
    }

//                    return $fakes[rand(1,48)];
}