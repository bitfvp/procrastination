<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="filapessoanew"){
    $id_prof=$_SESSION["id"];
    $nome=ucwords(strtolower($_POST["nome"]));
    $obs=$_POST["obs"];
    $servico=$_POST["servico"];
    $preferencial=$_POST["preferencial"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($nome)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }else{
        //executa classe cadastro
        $salvar= new Pessoa();
        $salvar->fncpessoanew($id_prof, $nome, $obs, $servico, $preferencial);
    }
    header("Location: index.php?pg=Vhome");
    exit();
}

if($startactiona==1 && $aca=="filapessoasave"){
    $id=$_POST['id'];
    $nome=ucwords(strtolower($_POST["nome"]));
    $obs=$_POST["obs"];
    $servico=$_POST["servico"];
    $preferencial=$_POST["preferencial"];
    //começa a validação
    //se algum campo vazia retorna a msg

    if(empty($nome)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }else{
        //executa classe cadastro
        $salvar= new Pessoa();
        $salvar->fncpessoaedit($id, $nome, $obs, $servico, $preferencial);
    }
}




////////////////////////////////////
/// para testes
if($startactiona==1 && $aca=="filapessoanew_teste"){

    $id_prof=$_SESSION["id"];
    $a_nomes = array (
        "Maria Eduarda","Joao Pedro","Maria Clara","Joao Lucas","Ana Laura","Davi Lucas","Maria Fernanda","Joao Gabriel","Ana Beatriz","Vitor Gabriel","Paula Fernanda","Larissa Beatriz","Luisa Eduarda","Elis Regina","Armando Luis","Vera Lucia","Luís Antonio","Tales Dominic","Júlio Cesar"
    );
    $tests = array (
        array("flavio Vinicius","bairro tal",1,0)
    );

    foreach ($a_nomes as $a_nome){
        $n_s=rand(1,7);
        $n_p=rand(0,1);
        $b=GeraPalavra();
        $t_push = array (strtolower($a_nome),$b,$n_s ,$n_p);
        array_push($tests,$t_push);
    }


    foreach ($tests as $test){
        $nome=$test[0];
        $obs=$test[1];
        $servico=$test[2];
        $preferencial=$test[3];
        //começa a validação
        //se algum campo vazia retorna a msg
        if(empty($nome)){
            $_SESSION['fsh']=[
                "flash"=>"Preencha todos os campos!!",
                "type"=>"warning",
            ];

        }else{
            //executa classe cadastro
            $salvar= new Pessoa();
            $salvar->fncpessoanew($id_prof, $nome, $obs, $servico, $preferencial);
        }
    }
    header("Location: index.php?pg=Vhome");
    exit();

}
