<?php
//chamar proximo
if($startactiona==1 && $aca=="novasenha"){

    //busca ultima senhasenha
    try{
        $sql = "SELECT * from mpref_fila_senha WHERE id=1 ";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $sss=$consulta->fetch();
    $ultimasenha=$sss['numero'];
    $ultimotempo=$sss['tempo'];
    $ultimoservico=$sss['servico'];



    //busca os usuários da fila
    try{
        $sql = "SELECT * from mpref_fila_pessoa where chamado=0  ";
        if (isset($_GET['s']) and is_numeric($_GET['s']) and !is_null($_GET['s'])){
            $sql .= "and id=? ";
        }else{
            $sql .= "and servico=? ";
        }
        $sql .="ORDER BY id ";
        global $pdo;
        $people=$pdo->prepare($sql);

        if (isset($_GET['s']) and is_numeric($_GET['s']) and !is_null($_GET['s'])){
            $people->bindParam(1,$_GET['s']);
        }else{
            $people->bindParam(1,$_SESSION['servico']);
        }
        $people->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $pessoas=$people->fetchAll();
// verifica o número total de registros
    $total=$people->rowCount();


    //verifica se ha mais usuario ha chamar
    if($total<1) {
        $_SESSION['fsh']=[
            "flash"=>"Aguarde, Não há próximo",
            "type"=>"warning",
        ];

    }else{
        //verifica se ja passou o tempo certo
        $newtempo= mktime(date("h"), date("i"), date("s"), date("m"), date("d"), date("Y"));
        if ($newtempo>$ultimotempo){
            $difff=$newtempo-$ultimotempo;
        }else{
            $difff=$ultimotempo-$newtempo;
        }

        //
        if ($difff<30){
            $fffid=30-$difff;
            $_SESSION['fsh']=[
                "flash"=>"Aguarde 30 segundos, Uma pessoa acabou de ser chamada",
                "type"=>"warning",
                "error"=>"Faltam {$fffid} segundos",
            ];
            header("Location: {$env->env_url_mod}");
            exit();
        }

        $proximo=$pessoas[0];
        //update de senha
        try {
            $sql = "UPDATE mpref_fila_senha ";
            $sql .= "SET numero=?, chamado='0', tempo=?, guiche=?, servico=?";
            $sql .= " WHERE id=1 ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1,$proximo['id']);
            $atualiza->bindParam(2,$newtempo);
            $atualiza->bindParam(3,$_SESSION['guiche']);
            $atualiza->bindParam(4,$_SESSION['servico']);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }




        //atualiza a fila com o prof que atendeu
        $fila_prof=$_SESSION["id"];
        try {
            $sql = "UPDATE mpref_fila_pessoa ";
            $sql .= "SET prof=? , guiche=? ";
            $sql .= " WHERE id=?";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1,$fila_prof);
            $atualiza->bindParam(2,$_SESSION['guiche']);
            $atualiza->bindParam(3,$proximo['id']);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        //soma cont_ranking pro profissional
        try{
            $sql="UPDATE tbl_users SET cont_rank=cont_rank+1 WHERE id=:id";
            global $pdo;
            $atualizarankpessoa=$pdo->prepare($sql);
            $atualizarankpessoa->bindValue(":id", $fila_prof);
            $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro rank profissional '. $error_msg->getMessage();
        }

        //Log de atendimento
        try{
            $sql="INSERT INTO mpref_fila_log ";
            $sql.="(id, prof)";
            $sql.=" VALUES ";
            $sql.="(NULL, :prof)";
            global $pdo;
            $inserelog=$pdo->prepare($sql);
            $inserelog->bindValue(":prof", $fila_prof);
            $inserelog->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro rank profissional '. $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Você chamou uma nova pessoa para ser atendida",
            "type"=>"success",
//            "error"=>"Você ganhou <h2 class='blink'>0,{$sorte}</h2> pontos no ranking geral",
            "error"=>"",
        ];
        header("Location: {$env->env_url_mod}");
        exit();


    }
}












//chamar proximo
if($startactiona==1 && $aca=="novasenhaprioritario"){

    //busca ultima senhasenha
    try{
        $sql = "SELECT * from mpref_fila_senha WHERE id=1 ";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $sss=$consulta->fetch();
    $ultimasenha=$sss['numero'];
    $ultimotempo=$sss['tempo'];
    $ultimoservico=$sss['servico'];



    //busca os usuários da fila
    try{
        $sql = "SELECT * from mpref_fila_pessoa where chamado=0 and servico=? and preferencial=1 ORDER BY id ";
        global $pdo;
        $people=$pdo->prepare($sql);
        $people->bindParam(1,$_SESSION['servico']);
        $people->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $pessoas=$people->fetchAll();
// verifica o número total de registros
    $total=$people->rowCount();


    //verifica se ha mais usuario ha chamar
    if($total<1) {
        $_SESSION['fsh']=[
            "flash"=>"Aguarde, Não há próximo",
            "type"=>"warning",
        ];

    }else{
        //verifica se ja passou o tempo certo
        $newtempo= mktime(date("h"), date("i"), date("s"), date("m"), date("d"), date("Y"));
        if ($newtempo>$ultimotempo){
            $difff=$newtempo-$ultimotempo;
        }else{
            $difff=$ultimotempo-$newtempo;
        }

        //
        if ($difff<30){
            $fffid=30-$difff;
            $_SESSION['fsh']=[
                "flash"=>"Aguarde 30 segundos, Uma pessoa acabou de ser chamada",
                "type"=>"warning",
                "error"=>"Faltam {$fffid} segundos",
            ];
            header("Location: {$env->env_url_mod}");
            exit();
        }

        $proximo=$pessoas[0];
        //update de senha
        try {
            $sql = "UPDATE mpref_fila_senha ";
            $sql .= "SET numero=?, chamado='0', tempo=?, guiche=?, servico=?";
            $sql .= " WHERE id=1 ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1,$proximo['id']);
            $atualiza->bindParam(2,$newtempo);
            $atualiza->bindParam(3,$_SESSION['guiche']);
            $atualiza->bindParam(4,$_SESSION['servico']);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }




        //atualiza a fila com o prof que atendeu
        $fila_prof=$_SESSION["id"];
        try {
            $sql = "UPDATE mpref_fila_pessoa ";
            $sql .= "SET prof=? , guiche=?, servico=? ";
            $sql .= " WHERE id=?";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1,$fila_prof);
            $atualiza->bindParam(2,$_SESSION['guiche']);
            $atualiza->bindParam(3,$_SESSION['servico']);
            $atualiza->bindParam(4,$proximo['id']);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        //soma cont_ranking pro profissional
        try{
            $sql="UPDATE tbl_users SET cont_rank=cont_rank+1 WHERE id=:id";
            global $pdo;
            $atualizarankpessoa=$pdo->prepare($sql);
            $atualizarankpessoa->bindValue(":id", $fila_prof);
            $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro rank profissional '. $error_msg->getMessage();
        }

        //Log de atendimento
        try{
            $sql="INSERT INTO mpref_fila_log ";
            $sql.="(id, prof)";
            $sql.=" VALUES ";
            $sql.="(NULL, :prof)";
            global $pdo;
            $inserelog=$pdo->prepare($sql);
            $inserelog->bindValue(":prof", $fila_prof);
            $inserelog->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro rank profissional '. $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Você chamou uma nova pessoa para ser atendida",
            "type"=>"success",
//            "error"=>"Você ganhou <h2 class='blink'>0,{$sorte}</h2> pontos no ranking geral",
            "error"=>"",
        ];
        header("Location: {$env->env_url_mod}");
        exit();


    }
}
?>