<?php

function fncservicolist(){
    $sql = "SELECT * FROM mpref_fila_servico ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $servicolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $servicolista;
}

function fncgetservico($id){
    switch ($id) {
        case 1:
            $getservico = array(
                'id' => 1,
                'servico' => 'Normal'
            );
            break;
        case 2:
            $getservico = array(
                'id' => 2,
                'servico' => 'Caixa rápido'
        );
            break;
        case 3:
            $getservico = array(
                'id' => 3,
                'servico' => 'Contábil'
            );
            break;
        case 4:
            $getservico = array(
                'id' => 4,
                'servico' => 'Especial'
            );
            break;
        case 5:
            $getservico = array(
                'id' => 5,
                'servico' => 'REFIS'
            );
            break;
        case 6:
            $getservico = array(
                'id' => 6,
                'servico' => 'Fazenda'
            );
            break;
        case 7:
            $getservico = array(
                'id' => 7,
                'servico' => 'Arrecadação'
            );
            break;
        default:
            $sql = "SELECT * FROM mpref_fila_servico WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $getservico = $consulta->fetch();
            $sql=null;
            $consulta=null;
            break;
    }
    return $getservico;
}
