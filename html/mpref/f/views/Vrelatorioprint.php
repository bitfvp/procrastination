<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_77"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}

$page="Relatório de Produção-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");


// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT Count(mpref_fila_log.prof) AS total, mpref_fila_log.prof "
."FROM mpref_fila_log "
."WHERE (((mpref_fila_log.data)>=:inicial) And ((mpref_fila_log.data)<=:final)) "
."GROUP BY mpref_fila_log.prof ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container">
        <h1>Produção  </h1>
        <?php
        $x=0;
        ?>
        <table class="table table-condensed">
            <thead class="thead-default">
            <tr>
                <td>Profissional&nbsp;</td>
                <td>Total&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            include_once("{$env->env_root}controllers/usuario_lista.php");
            foreach ($ati as $at){
                ?>
                <tr>
                    <td>
                        <?php
                        $pss=fncgetusuario($at['prof']);
                        echo $pss['nome'];
                        ?>&nbsp;
                    </td>
                    <td><?php echo $at['total']; ?>&nbsp;</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
</div>
</body>
</html>