<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_75"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Msg-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vhome&aca=salvarmsgnovo" method="post">
            <input type="submit" class="btn btn-success btn-block" value="Salvar"/><br>
            <fieldset>
                <label  class="large" for="color">Color:</label>
                    <select name="color" id="color" class="form-control">
                        <option selected="" value="preto">Preto</option>
                        <option value="branco">Branco</option>
                        <option value="vermelho">vermelho</option>
                        <option value="verde">verde</option>
                        <option value="azul">azul</option>
                        <option value="amarelo">amarelo</option>
                        <option value="gold">gold</option>
                        <option value="silver">silver</option>
                        <option value="bronze">bronze</option>
                        <option value="darkgray">darkgray</option>
                    </select>
                
                <label  class="large" for="background">Background:</label>
                    <select name="background" id="background" class="form-control">
                        <option selected="" value="Btrasparente">Trasparente</option>
                        <option value="Bbranco">Branco</option>
                        <option value="Bpreto">Preto</option>
                        <option value="Bvermelho">Vermelho</option>
                        <option value="Bverde">Verde</option>
                        <option value="Bazul">Azul</option>
                        <option value="Bamarelo">Amarelo</option>
                        <option value="BRamarelovermelho">Gradiente radial amarelovermelho</option>
                        <option value="BLamarelovermelho">Gradiente linear amarelovermelho</option>
                        <option value="BRverdeamarelo">Gradiente radial verdeamarelo</option>
                        <option value="BLverdeamarelo">Gradiente linear verdeamarelo</option>
                        <option value="BRvermelhoazul">Gradiente radial vermelhoazul</option>
                        <option value="BLvermelhoazul">Gradiente linear vermelhoazul</option>
                    </select>
                
                <label  class="large" for="msg">MSG:</label>
                    <textarea autocomplete="off" id="msg" autofocus class="form-control" name="msg" placeholder="Digite o texto que você deseja..." maxlength="255"></textarea>
                

        </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>