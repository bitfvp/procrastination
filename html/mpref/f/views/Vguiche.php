<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_75"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

if (isset($_SESSION["guiche"]) and is_numeric($_SESSION["guiche"]) and $_SESSION["logado"]>0 and $_SESSION["logado"]<100 and isset($_SESSION["servico"]) and is_numeric($_SESSION["servico"])) {
    //validação se esta logado
    header("Location: index.php");
    exit();
}


$page="Editar guiche-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_SESSION["guiche"]) and is_numeric($_SESSION["guiche"]) and $_SESSION["guiche"]>0 and $_SESSION["guiche"]<100) {
    //validação se esta logado
    header("Location: index.php?pg=Vhome");
    exit();
}

?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vguiche&aca=cadguiche" method="post">
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">

                <div class="col-md-4">
                    <label for="guiche">SELECIONE O GUICHÊ DE ATENDIMENTO:</label>
                    <select name="guiche" id="guiche" class="form-control text-uppercase" required>// vamos criar a visualização de sexo
                        <option selected="" value="">...Selecione</option>
                        <option value="1">Guichê 1</option>
                        <option value="2">Guichê 2</option>
                        <option value="3">Guichê 3</option>
                        <option value="4">Guichê 4</option>
                        <option value="5">Guichê 5</option>
                        <option value="6">Guichê 6</option>
                        <option value="7">Guichê 7</option>
                        <option value="8">Guichê 8</option>
                        <option value="9">Guichê 9</option>
                        <option value="10">Guichê Especial</option>
                        <option value="11">Sala 1</option>
                        <option value="12">Sala 2</option>
                        <option value="13">Sala 3</option>
                        <option value="14">Sala 4</option>
                        <option value="15">Sala 5</option>
                        <option value="31">Setor de Arrecadação</option>
                        <option value="32">Fazenda</option>
                        <option value="50">Atendimento Rápido</option>
                        <option value="99">Recepção</option>
                    </select>
                </div>

                <div class="col-md-4">
                    <label for="servico">SELECIONE O TIPO DE SERVIÇO:</label>
                    <select name="servico" id="servico" class="form-control text-uppercase" required>// vamos criar a visualização
                        <option selected="" value="">...Selecione</option>
                        <?php
                        foreach (fncservicolist() as $item) {
                            ?>
                            <option data-tokens="<?php echo $item['servico'];?>" value="<?php echo $item['id'];?>">
                                <?php echo $item['servico']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="servico_todos">ACOMPANHAR TODOS OS SERVIÇOS:</label>
                    <select name="servico_todos" id="servico_todos" class="form-control">// vamos criar a visualização
                        <option selected="" value="0">NÃO</option>
                        <option value="1">SIM</option>
                    </select>
                </div>

            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>