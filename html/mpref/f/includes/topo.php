<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">
    <a class="navbar-brand d-none d-xl-block" href="http://www.manhuacu.mg.gov.br/diario-eletronico" target="_blank">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="<?php echo $env->env__nome; ?>">
    </a>
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/syssocial2.png" alt="<?php echo $env->env__nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <?php if($allow["allow_77"]==1) {?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navsenha" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            SENHAS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navsenha">
                            <a class="dropdown-item" href="index.php?pg=Vhome&aca=resetsenhas">RESET DAS SENHAS<i class="text-danger">*desativado</i></a>
                            <a class="dropdown-item" href="index.php?pg=Vhome&aca=filapessoanew_teste">GERA SENHAS<i class="text-danger">*desativado</i></a>
                        </div>
                    </li>

                    <?php } if($allow["allow_77"]==1) {?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navrelatorio" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        RELATÓRIO
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navrelatorio">
                        <a class="dropdown-item" href="index.php?pg=Vrelatorio">RELATÓRIO POR DATA</a>
                    </div>
                </li>
                <?php }?>
                <a class="nav-item nav-link" href="index.php?pg=Vhome&aca=resetguiche">TROCAR GUICHÊ</a>
                <a class="nav-item nav-link" href="index.php?pg=Vvideo">VIDEO</a>
            </ul>
            <?php
            include_once("{$env->env_root}includes/mpref/modal_msg_pontos.php");
            ?>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalPontos">
                            <i class="fa fa-star"></i>
                            <?php echo $ponto; ?>
                        </a>
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?PHP echo $env->env_url; ?>?pg=Vlogin"><i class="fa fa-undo"></i> Voltar</a>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
<?php if (true===false){?>
    <a class="navbar-brand d-none d-xl-block" data-toggle="tooltip" data-trigger="click" data-html="true" title="Grande dia! <i class='fas fa-thumbs-up text-warning'></i>">
        <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/patriaamada.png" alt="<?php echo $env->env__nome; ?>">
    </a>
<?php } ?>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");