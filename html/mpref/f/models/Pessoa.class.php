<?php
class Pessoa{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew( $id_prof, $nome, $obs, $servico, $preferencial){
        //tratamento das variaveis
        $nome=ucwords($nome);

            //inserção no banco
            try{
                $sql="INSERT INTO mpref_fila_pessoa ";
                $sql.="(id, nome, obs, servico, preferencial)";
                $sql.=" VALUES ";
                $sql.="(NULL, :nome, :obs, :servico, :preferencial)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":obs", $obs);
                $insere->bindValue(":servico", $servico);
                $insere->bindValue(":preferencial", $preferencial);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                 //soma cont_ranking pro profissional
            try{
                $sql="UPDATE tbl_users SET cont_rank=cont_rank+0.2 WHERE id=:id";
                global $pdo;
                $atualizarankpessoa=$pdo->prepare($sql);
                $atualizarankpessoa->bindValue(":id", $id_prof);
                $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro rank profissional '. $error_msg->getMessage();
            }


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoaedit($id, $nome, $obs, $servico, $preferencial){
        //tratamento das variaveis
        $nome=ucwords($nome);

        //inserção no banco
        try{
            $sql="UPDATE mpref_fila_pessoa SET ";
            $sql.="nome=?, obs=?, servico=?, preferencial=? ";
            $sql.=" WHERE id=?";
            global $pdo;
            $upd=$pdo->prepare($sql);
            $upd->bindParam(1, $nome);
            $upd->bindParam(2, $obs);
            $upd->bindParam(3, $servico);
            $upd->bindParam(4, $preferencial);
            $upd->bindParam(5, $id);
            $upd->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($upd)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro Editado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
}
?>