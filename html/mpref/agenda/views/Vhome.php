<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    $scb = $_GET['scb'];
    $sql = "select * from mcu_agenda WHERE nome LIKE '%$sca%' or telefone LIKE '%$sca%'";
}else {

    $sql = "select * from mcu_agenda where id=0 ";
}

$total_reg = "50"; // número de registros por página

$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}

$inicio = $pc - 1;
$inicio = $inicio * $total_reg;

try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Contatos</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vhome" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por contato..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vpessoaeditar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO CONTATO
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">TELEFONE</th>
            <th scope="col">EDITAR</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Contato(s) listado(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
        foreach ($limite as $dados){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $telefone = $dados["telefone"];
            ?>
            <tr>
                <td scope="row" id="<?php echo $id;  ?>">
                    <a href="#" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </td>
                <td>
                    <?php
                    if($_GET['sca']!="") {
                        $sta = CSA;
                        $nnn = $telefone;
                        $nn = explode(CSA, $nnn);
                        $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                        echo $n;
                    }else{
                        echo $telefone;
                    }
                    ?>
                </td>

                <td>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados desse Contato">
                        Alterar
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
