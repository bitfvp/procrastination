<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetusuario($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
  <h3>DADOS DO PROFISSIONAL</h3>
  <blockquote class="blockquote blockquote-info">
  <header>
      NOME:
      <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
  </header>
      <h6>
          E-MAIL:
          <strong class="text-info"><?php echo $pessoa['email']; ?>&nbsp;&nbsp;</strong>
          NASCIMENTO:
          <strong class="text-info"><?php
              if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                  echo "<span class='text-info'>";
                  echo dataBanco2data ($pessoa['nascimento']);
                  echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                  echo "</span>";
              }else{
                  echo "<span class='text-muted'>";
                  echo "[---]";
                  echo "</span>";
              }
              ?>
          </strong>
          <br>
          PROFISSÃO:
          <strong class="text-info"><?php
            if ($pessoa['profissao'] != "0") {
                echo fncgetprofissao($pessoa['profissao'])['profissao'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>
          </strong>&nbsp;&nbsp;
    </h6>
    <footer class="blockquote-footer">
            Mantenha atualizado</strong>&nbsp;&nbsp;
    </footer>
  </blockquote>
</div>
        <a class="btn btn-success btn-block" href="?pg=Valloweditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR USUÁRIO
        </a>
