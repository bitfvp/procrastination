<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=3){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
include_once("includes/aniversario.php");
$sorte=rand(1,10);
?>
<main class="container">
    <div class="jumbotron p-3 text-center mt-0 mb-3 jumb-<?php echo $sorte;?>">
            <h1 class="display-4">
                <img class="col-md-2" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>"/>

                <img class="col-md-4" src="<?php echo $env->env_estatico; ?>img/syssocial_alto1.png" alt="" title="<?php echo $env->env__nome; ?>"/>
                <p class="lead">
                    Prefeitura Municipal de <?php echo $env->env_mod_nome; ?>
                </p>
            </h1>
    </div>

    <div class="col-md-12 px-0">
        <?php
        function bsmenu($bs_type,$bs_btn,$bs_name,$bs_desc,$bs_url){
            echo "<div class='bs-calltoaction bs-calltoaction-{$bs_type}'>
                <div class='row'>
                    <div class='col-md-8 cta-contents mx-auto'>
                        <h1 class='cta-title'>{$bs_name}</h1>
                        <div class='cta-desc'>
                            <p>{$bs_desc}</p>
                        </div>
                    </div>
                    <div class='col-md-4 cta-button mx-auto'>
                    <a href='{$env->env_url_mod}{$bs_url}' class='btn btn-lg btn-block btn-{$bs_btn}'>Acessar <i class='fa fa-sign-in-alt'></i></a>    
                    </div>
                </div>
            </div>";
        }


        //agenda
//        if ($allow["allow_8"] == "1") {
//            bsmenu("primary","success","AGENDA TELEFÔNICA","Módulo de agenda telefônica e registro de ligações","agenda");
//        }

        //fila
        if ($allow["allow_75"] == "1") {
            bsmenu("primary","success","FILA","Módulo de controle de filas de atendimentos","f");
        }
        //fila tela
        if ($allow["allow_78"] == "1") {
            bsmenu("primary","success","FILA TELA","Módulo tela para complementar o fila de atendimento","ft");
        }

        //fila
        if ($allow["allow_73"] == "1") {
            bsmenu("primary","success","PESQUISA","","survey");
        }
        //fila tela
        if ($allow["allow_74"] == "1") {
            bsmenu("primary","success","PESQUISA TELA","","surveytela");
        }

        //fila tela
        if ($allow["allow_65"] == "1") {
            bsmenu("primary","success","ESPORTE","","esporte");
        }

        //admin
        if ($allow["admin"] == "1") {
            bsmenu("warning","success","USUÁRIOS","Apenas administradores","_admin");
        }
        ?>
    </div>
    </div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>