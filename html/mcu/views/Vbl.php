<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        if ($allow["allow_5"]!=1){
            //validação de matriz
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            //validação das permissoes
        }
    }
}

$page="Bairros-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_bairros WHERE bairro LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_bairros ";
}
// total de registros a serem exibidos por página
$total_reg = "100"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY bairro LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY bairro LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->

<h2>Listagem de Bairros</h2>
    <hr>
    <form action="index.php" method="get">
    <div class="input-group mb-3  col-md-6 float-left">
        <div class="input-group-prepend">
            <button class="btn btn-outline-success" type="submit">Ir!</button>
        </div>
        <input name="pg" value="Vbl" hidden/>
        <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por bairro..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
    </div>
    </form>
    <a href="index.php?pg=Vbe" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO BAIRRO
    </a>
    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <table id="tabela" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>BAIRRO</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $bairro = $dados["bairro"];
            ?>

            <tr data-toggle="collapse" data-target="#accordion<?php echo $id; ?>" class="clickable">
            <td><a href="index.php?pg=Vbe&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                <?php
                if($_GET['sca']!="") {
                    $sta = CSA;
                    $ccc = strtoupper($bairro);
                    $cc = explode(CSA, $ccc);
                    $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                    echo $c;
                }else{
                    echo $bairro;
                }
                ?>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>