<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
include_once("includes/aniversario.php");
$sorte=rand(1,10);
?>
<main class="container">
    <div class="jumbotron p-3 text-center mt-0 mb-3 jumb-<?php echo $sorte;?>">
            <h1 class="display-4">
                <img class="col-md-2" src="<?php echo $env->env_estatico; ?>img/mcu.png" alt="" title="<?php echo $env->env_nome; ?>"/>
                <img class="col-md-4" src="<?php echo $env->env_estatico; ?>img/syssocial_alto1.png" alt="" title="<?php echo $env->env__nome; ?>"/>
                <p class="lead">
                    Sistema de Informação Gerencial da Assistência Social de <?php echo $env->env_mod_nome; ?>
                </p>
            </h1>
    </div>

    <div class="col-md-12 px-0">
        <?php
        function bsmenu($bs_type,$bs_btn,$bs_name,$bs_desc,$bs_url){
            echo "<div class='bs-calltoaction bs-calltoaction-{$bs_type}'>
                <div class='row'>
                    <div class='col-md-8 cta-contents mx-auto'>
                        <h1 class='cta-title'>{$bs_name}</h1>
                        <div class='cta-desc'>
                            <p>{$bs_desc}</p>
                        </div>
                    </div>
                    <div class='col-md-4 cta-button mx-auto'>
                    <a href='{$env->env_url_mod}{$bs_url}' class='btn btn-lg btn-block btn-{$bs_btn}'>Acessar <i class='fa fa-sign-in-alt'></i></a>    
                    </div>
                </div>
            </div>";
        }

        //AABB comunidade
        if ($allow["allow_69"] == "1") {
            bsmenu("primary","success","AABB COMUNIDADE","","aabb");
        }
        //abrigo historico
        if ($allow["allow_24"] == "1") {
            bsmenu("primary","success","ABRIGO","Histórico","abrigo");
        }
        //Albergue
        if ($allow["allow_63"] == "1") {
            bsmenu("primary","success","ALBERGUE","Ficha e histórico do migrante no albergue ","albergue");
        }
        //agenda
//        if ($allow["allow_8"] == "1") {
//            bsmenu("primary","success","AGENDA TELEFÔNICA","Módulo de agenda telefônica e registro de ligações","agenda");
//        }
        //biblioteca
        if ($allow["allow_72"] == "1") {
            bsmenu("primary","primary","BIBLIOTECA","Controle","bb");
        }
        //bolsa familia
        if ($allow["allow_41"] == "1") {
            bsmenu("primary","success","BOLSA FAMÍLIA","Módulo de controle de histórico de usuário do bolsa família, visita averiguação","bf");
        }
        //ch
        if ($allow["allow_73"] == "1") {
            bsmenu("primary","primary","CADASTRO HABITACIONAL","","ch");
        }
        //conselho tutelar
        if ($allow["allow_25"] == "1") {
            bsmenu("primary","primary","CONSELHO TUTELAR","Módulo de controle de atividades e organização de denúncias","ct");
        }
        //novo passe livre
        if ($allow["allow_49"] == "1") {
            bsmenu("primary","success","CREDENCIAL","Idoso, Deficiente, APAE, Fibromialgia e Autista <i class='fa fa-home'></i>","cr");
        }
        //cb de entrega
        if ($allow["allow_35"] == "1") {
                bsmenu("primary","success","ENTREGA DE BENEFÍCIOS","Módulo de controle de saida de benefícios","entrega");
        }
        //estoque
        if ($allow["allow_79"] == "1") {
            bsmenu("primary","primary","ESTOQUE","Entrada, saída, controle","e");
        }
        //estoque
        if ($allow["allow_71"] == "1") {
            bsmenu("primary","success","ESTOQUE ABRIGO","Entrada, saída, controle","abrigoe");
        }
        //fila
        if ($allow["allow_75"] == "1") {
            bsmenu("primary","success","FILA","Módulo de controle de filas de atendimentos","f");
        }
        //fila tela
        if ($allow["allow_78"] == "1") {
            bsmenu("primary","success","FILA TELA","Módulo tela para complementar o fila de atendimento","ft");
        }
        //frota
        if ($allow["allow_1"] == "1") {
            bsmenu("primary","success","FROTA","Abastecimentos e frota","frota");
        }
        //migrante
        if ($allow["allow_57"] == "1") {
            bsmenu("primary","success","MIGRANTE","Controle de histórico de abordagens e controle de emissão de passagens para pessoas em situação de rua","migrante");
        }
        //oficios
        if ($allow["allow_74"] == "2") {
            bsmenu("primary","primary","Oficio","Módulo de controle de entrada e saida de Oficios","oficio");
        }
        //novo passe livre empresa
        if ($allow["allow_56"] == "1") {
            bsmenu("primary","success","PASSE LIVRE","Carteira municipal","passelivre");
        }
        //Processo seletivo
        if ($allow["allow_70"] == "1") {
            bsmenu("primary","success","PROCESSO SELETIVO","","ps");
        }
        //protecao basica
        if ($allow["allow_9"] == "1") {
            bsmenu("primary","success","PROTEÇÃO BÁSICA","Módulo de controle de histórico de usuário, produção, pedido de cesta básica.","pb");
        }
        //protecao especializada
        if ($allow["allow_17"] == "1") {
            bsmenu("primary","success","PROTEÇÃO ESPECIALIZADA","Módulo de controle de atividades e produção CREAS","pe");
        }
        //sysjob
        if ($allow["allow_80"] == "1") {
            bsmenu("primary","success","SYSJOB","Nat","sysjob");
        }
        //telecentro
        if ($allow["allow_65"] == "1") {
            bsmenu("primary","success","CURSOS","Controle de aluno","tc");
        }
        //relatorios
            bsmenu("warning","success","RELATÓRIOS","Apenas administradores","_rel");
        //admin
        if ($allow["admin"] == "1") {
            bsmenu("warning","success","USUÁRIOS","Apenas administradores","_admin");
        }
        //log
        if ($allow["admin"] == "1") {
            bsmenu("warning","success","LOG","Apenas administradores","_log");
        }
        ?>
    </div>
    </div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>