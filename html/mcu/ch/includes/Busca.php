<div  class="modal fade" id="modalserch" tabindex="-1" role="dialog" aria-labelledby="modalserchLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pesquisa</h4>
            </div>
            <div class="modal-body">

                <form class="form-signin" action="index.php" method="get">
                    <h2 class="form-signin-heading">Pesquise por...</h2>
                    <input name="pg" value="Vhome" hidden/>
                    <input type="text" autocomplete="off" class="form-control" placeholder="Buscar por Nome..." name="sca" value="<?php if(isset($_GET['sca'])){echo $_GET['sca'];}   ?>" />
                    <input type="text" autocomplete="off" class="form-control" placeholder="Buscar por CPF..." name="scb" value="<?php if(isset($_GET['scb'])){echo $_GET['scb'];}   ?>" />
                    <button class="btn btn-lg btn-success btn-block" type="submit" >Buscar</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dilog -->
</div> <!-- /.modal -->
