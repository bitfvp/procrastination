<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $ch=fncgetpessoach($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
  <h3>Dados </h3>
  <blockquote class="blockquote blockquote-info">
    <p>
    <h5>

        Nome: <strong class="text-warning"><?php echo $ch['nome']; ?>&nbsp;&nbsp;</strong><br>
        Nis: <strong class="text-warning"><?php echo $ch['nis']; ?>&nbsp;&nbsp;</strong>
        CPF: <strong class="text-warning"><?php echo $ch['cpf']; ?>&nbsp;&nbsp;</strong><br>
        Renda declarada: <strong class="text-warning">R$ <?php echo number_format($ch['renda_declarada'],2); ?>&nbsp;&nbsp;</strong><br>
        Logradouro: <strong class="text-info"><?php echo $ch['logradouro']; ?>&nbsp;&nbsp;</strong>
        Número: <strong class="text-info"><?php echo $ch['numero']; ?>&nbsp;&nbsp;</strong>

        Bairro: <strong class="text-info"><?php
              if ($ch['bairro'] != "0") {
                  $cadbairro=fncgetbairro($ch['bairro']);
                  echo $cadbairro['bairro'];
              } else {
                  echo "<span class='text-warning'>?????????</span>";
              }
              ?></strong><br>
          Idoso: <strong class="text-warning"><?php
            if($ch['c_idoso']==0){echo"Não";}
            if($ch['c_idoso']==1){echo"Sim";}
            ?>&nbsp;&nbsp;</strong>
        <hr>

          Contagem do Critério: <strong class="text-warning"><?php
              echo $ch['contcriterio'];
              ?>&nbsp;&nbsp;</strong>
          <hr>

Risco/Insalubre: <strong class="text-warning"><?php
            if($ch['risco_insalubre']==0){echo"Não";}
            if($ch['risco_insalubre']==1){echo"Sim";} 
            ?>&nbsp;&nbsp;</strong>

          Mulher Chefe de Família: <strong class="text-warning"><?php
              if($ch['c_mulher']==0){echo"Não";}
              if($ch['c_mulher']==1){echo"Sim";}
              ?>&nbsp;&nbsp;</strong>


PCD: <strong class="text-warning"><?php
            if($ch['c_pcd']==0){echo"Não";}
            if($ch['c_pcd']==1){echo"Sim";}
?>&nbsp;&nbsp;</strong>
          <hr>
          Benefício Social: <strong class="text-warning"><?php
              if($ch['c_bs']==0){echo"Não";}
              if($ch['c_bs']==1){echo"Sim";}
              ?>&nbsp;&nbsp;</strong>

Reside em MCU 5 anos ou mais: <strong class="text-warning"><?php
            if($ch['c_reside_mcu_5']==0){echo"Não";}
            if($ch['c_reside_mcu_5']==1){echo"Sim";}
?>&nbsp;&nbsp;</strong>

Família com Menores: <strong class="text-warning"><?php
            if($ch['c_familia_com_menores']==0){echo"Não";}
            if($ch['c_familia_com_menores']==1){echo"Sim";}
?>&nbsp;&nbsp;</strong>
          <hr>

          Benefício bf: <strong class="text-warning"><?php
              if($ch['beneficio_bf']==0){echo"Não";}
              if($ch['beneficio_bf']==1){echo"Sim";}
              ?>&nbsp;&nbsp;</strong>

          Benefício valor:<strong class="text-warning"> R$ <?php echo number_format($ch['beneficio_bf_valor'],2); ?>&nbsp;&nbsp;</strong>
        
    </h5>
    </p>
    <footer class="blockquote-footer">Mantenha atualizado</footer>
  </blockquote>
</div>


        <a  class="btn btn-success btn-block" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            Editar Pessoa
        </a>

<a  class="btn btn-info btn-block" href="index.php?pg=Vinsc_comprovante&id=<?php echo $_GET['id']; ?>" target="_blank" title="">
    Imprimir Comprovante
</a>

