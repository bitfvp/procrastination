<?php
class Pessoach{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncPessoachNovo($cadastro_novo,$nis,$nome,$cpf,$renda_declarada,$logradouro,$numero,$bairro,$risco_insalubre,$c_idoso,$c_pcd,$c_mulher,$c_reside_mcu_5,$c_bs,$c_familia_com_menores,$contcriterio,$beneficio_bf,$beneficio_bf_valor){
        //tratamento das variaveis
        $nome=ucwords(strtolower($nome));

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_ch ";
                $sql.="(id, cadastro_novo, nis, nome, cpf, renda_declarada, logradouro, numero, bairro, risco_insalubre, c_idoso, c_pcd, c_mulher, c_reside_mcu_5, c_bs, c_familia_com_menores, contcriterio, beneficio_bf, beneficio_bf_valor)";
                $sql.=" VALUES ";
                $sql.="(NULL, :cadastro_novo, :nis, :nome, :cpf, :renda_declarada, :logradouro, :numero, :bairro, :risco_insalubre, :c_idoso, :c_pcd, :c_mulher, :c_reside_mcu_5, :c_bs, :c_familia_com_menores, :contcriterio, :beneficio_bf, :beneficio_bf_valor)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":cadastro_novo", $cadastro_novo);
                $insere->bindValue(":nis", $nis);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":cpf", $cpf);
                $insere->bindValue(":renda_declarada", $renda_declarada);
                $insere->bindValue(":logradouro", $logradouro);
                $insere->bindValue(":numero", $numero);
                $insere->bindValue(":bairro", $bairro);
                $insere->bindValue(":risco_insalubre", $risco_insalubre);
                $insere->bindValue(":c_idoso", $c_idoso);
                $insere->bindValue(":c_pcd", $c_pcd);
                $insere->bindValue(":c_mulher", $c_mulher);
                $insere->bindValue(":c_reside_mcu_5", $c_reside_mcu_5);
                $insere->bindValue(":c_bs", $c_bs);
                $insere->bindValue(":c_familia_com_menores", $c_familia_com_menores);
                $insere->bindValue(":contcriterio", $contcriterio);
                $insere->bindValue(":beneficio_bf", $beneficio_bf);
                $insere->bindValue(":beneficio_bf_valor", $beneficio_bf_valor);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM mcu_ch ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];

            header("Location: index.php?pg=Vpessoa&id={$maid}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncPessoachedicao($id,$cadastro_novo,$nis,$nome,$cpf,$renda_declarada,$logradouro,$numero,$bairro,$risco_insalubre,$c_idoso,$c_pcd,$c_mulher,$c_reside_mcu_5,$c_bs,$c_familia_com_menores,$contcriterio,$beneficio_bf,$beneficio_bf_valor){
        //tratamento das variaveis
        $nome=ucwords(strtolower($nome));

        //inserção no banco
        try{
            $sql="UPDATE mcu_ch ";
            $sql.="SET cadastro_novo=:cadastro_novo, 
            nis=:nis, 
            nome=:nome, 
            cpf=:cpf, 
            renda_declarada=:renda_declarada, 
            logradouro=:logradouro, 
            numero=:numero, 
            bairro=:bairro, 
            risco_insalubre=:risco_insalubre, 
            c_idoso=:c_idoso, 
            c_pcd=:c_pcd, 
            c_mulher=:c_mulher, 
            c_reside_mcu_5=:c_reside_mcu_5, 
            c_bs=:c_bs, 
            c_familia_com_menores=:c_familia_com_menores, 
            contcriterio=:contcriterio, 
            beneficio_bf=:beneficio_bf, 
            beneficio_bf_valor=:beneficio_bf_valor ";
            $sql.=" WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":cadastro_novo", $cadastro_novo);
            $insere->bindValue(":nis", $nis);
            $insere->bindValue(":nome", $nome);
            $insere->bindValue(":cpf", $cpf);
            $insere->bindValue(":renda_declarada", $renda_declarada);
            $insere->bindValue(":logradouro", $logradouro);
            $insere->bindValue(":numero", $numero);
            $insere->bindValue(":bairro", $bairro);
            $insere->bindValue(":risco_insalubre", $risco_insalubre);
            $insere->bindValue(":c_idoso", $c_idoso);
            $insere->bindValue(":c_pcd", $c_pcd);
            $insere->bindValue(":c_mulher", $c_mulher);
            $insere->bindValue(":c_reside_mcu_5", $c_reside_mcu_5);
            $insere->bindValue(":c_bs", $c_bs);
            $insere->bindValue(":c_familia_com_menores", $c_familia_com_menores);
            $insere->bindValue(":contcriterio", $contcriterio);
            $insere->bindValue(":beneficio_bf", $beneficio_bf);
            $insere->bindValue(":beneficio_bf_valor", $beneficio_bf_valor);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>