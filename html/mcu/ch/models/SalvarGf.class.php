<?php
class SalvarGf{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvargfnovo($titular,$cod_atualizacao,$nis_titular,$nome,$parentesco){
        //tratamento das variaveis
        $nome=ucwords(strtolower($nome));

            //inserção no banco
            try{
                $sql="INSERT INTO ch_gf ";
                $sql.="(id, titular, cod_atualizacao, nis_titular, nome, parentesco)";
                $sql.=" VALUES ";
                $sql.="(NULL, :titular, :cod_atualizacao, :nis_titular, :nome, :parentesco)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":titular", $titular);
                $insere->bindValue(":cod_atualizacao", $cod_atualizacao);
                $insere->bindValue(":nis_titular", $nis_titular);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":parentesco", $parentesco);


                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
?>