<?php
class Fa{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncfanew($titular,$nome,$nascimento,$sexo,$deficiencia,$trabalho_formal,$trabalho_informal,$trabalho_desempregado,$renda,$beneficio_inss,$beneficio_bpc){
        //tratamento das variaveis
        $nome=ucwords(strtolower($nome));

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_ch_familia ";
                $sql.="(`id`, `titular`, `nome`, `sexo`, `nascimento`, `deficiencia`, `trabalho_formal`, `trabalho_informal`, `trabalho_desempregado`, `renda`, `beneficio_inss`, `beneficio_bpc`)";
                $sql.=" VALUES ";
                $sql.="(NULL, :titular, :nome, :sexo, :nascimento, :deficiencia, :trabalho_formal, :trabalho_informal, :trabalho_desempregado, :renda, :beneficio_inss, :beneficio_bpc)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":titular", $titular);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":sexo", $sexo);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":deficiencia", $deficiencia);
                $insere->bindValue(":trabalho_formal", $trabalho_formal);
                $insere->bindValue(":trabalho_informal", $trabalho_informal);
                $insere->bindValue(":trabalho_desempregado", $trabalho_desempregado);
                $insere->bindValue(":renda", $renda);
                $insere->bindValue(":beneficio_inss", $beneficio_inss);
                $insere->bindValue(":beneficio_bpc", $beneficio_bpc);


                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcadastro&id={$titular}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
?>