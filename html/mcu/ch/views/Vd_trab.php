<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Trabalho-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * \n"
    . "FROM mcu_ch_familia\n"
    . "where TIMESTAMPDIFF(YEAR, nascimento, CURDATE())>18";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$sql = null;
$consulta = null;
$tfcont=$ticont=$tdcont=0;
foreach ($valor as $vl){
    if ($vl['trabalho_formal']==1){
        $tfcont++;
    }
    if ($vl['trabalho_informal']==1){
        $ticont++;
    }
    if ($vl['trabalho_desempregado']==1){
        $tdcont++;
    }
}

?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var data = google.visualization.arrayToDataTable([
                ['Tipo', 'Quantidade',],
                ['Trabalho Formal', <?php echo $tfcont;?>],
                ['Trabalho Informal', <?php echo $ticont;?>],
                ['Desempregados', <?php echo $tdcont;?>]
            ]);

            var options = {
                title: '',
                chartArea: {width: '50%'},
                hAxis: {
                    title: 'Total',
                    minValue: 0
                },
                vAxis: {
                    title: ''
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Tipo&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Trabalho Formal&nbsp;</td>
            <td><?php echo $tfcont; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Trabalho Informal</td>
            <td><?php echo $ticont; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Desempregados</td>
            <td><?php echo $tdcont; ?>&nbsp;Pessoas</td>
        </tr>
        </tbody>
    </table>
    <div id="chart_div"></div>
</main>
</body>
</html>