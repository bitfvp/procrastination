<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoach($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-7">
		<?php include_once ("includes/pessoacabecalhotop.php");?>
<br>
        <?php
        // Recebe
        if(isset($_GET['id'])) {
            $idpessoa =$_GET['id'];
            //existe um id e se ele é numérico
            if (!empty($idpessoa) && is_numeric($idpessoa)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT * FROM mcu_ch_familia WHERE titular=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $idpessoa);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $at = $consulta->fetchAll();
                $sql=null;
                $consulta=null;
            }
        }
        ?>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Grupo Familiar</h3>
            </div>
            <div class="panel-body">
                <p>
                <h5 id="printable">
                    <?php
                    foreach($at as $gf){
                        ?>
                        <hr>
                        <blockquote>
                            Nome:<strong class="text-info"><?php echo $gf['nome']; ?>&nbsp;&nbsp;</strong>
                            sexo:<strong class="text-info">
                                <?php
                                if($gf['sexo']==0){echo"seleccione...";}
                                if($gf['sexo']==1){echo"Feminino";}
                                if($gf['sexo']==2){echo"Masculino";}
                                if($gf['sexo']==3){echo"Indefinido";}
                                ?>&nbsp;&nbsp;</strong>
                           Nascimento:<strong class="text-info"><?php echo dataBanco2data($gf['nascimento']); ?>&nbsp;&nbsp;</strong>
                            Deficiencia:<strong class="text-info"><?php
                                if($gf['deficiencia']==0){echo"Não";}
                                if($gf['deficiencia']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                            Trabalho formal:<strong class="text-info"><?php
                                if($gf['trabalho_formal']==0){echo"Não";}
                                if($gf['trabalho_formal']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                            Trabalho informal:<strong class="text-info"><?php
                                if($gf['trabalho_informal']==0){echo"Não";}
                                if($gf['trabalho_informal']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                            Trabalho desempregado:<strong class="text-info"><?php
                                if($gf['trabalho_desempregado']==0){echo"Não";}
                                if($gf['trabalho_desempregado']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                            Renda:<strong class="text-info">R$<?php echo $gf['renda']; ?>,00&nbsp;&nbsp;</strong>
                            Benefício inss:<strong class="text-info"><?php
                                if($gf['beneficio_inss']==0){echo"Não";}
                                if($gf['beneficio_inss']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                            Benefício bpc:<strong class="text-info"><?php
                                if($gf['beneficio_bpc']==0){echo"Não";}
                                if($gf['beneficio_bpc']==1){echo"Sim";}
                                ?>&nbsp;&nbsp;</strong>
                        </blockquote>
                        <?php
                    }
                    ?>
                </h5>
                </p>
            </div>
        </div>


<!-- /////////////////////////////////////// -->

<!-- ////////////////////////////// -->



	</div>
	<div class="col-md-5">
        <div class="card">
            <div class="card-header bg-info text-light">
                Grupo familiar
            </div>
            <div class="card-body">
                <form action="index.php?pg=Vcadastro&aca=fanew&id=<?php echo $_GET['id']; ?>" method="post">
                    <div class="col-md-12">
                        <label for="nome">Nome:</label>
                        <input required="true" autocomplete="off" id="nome" type="text" class="form-control input-sm" name="nome" value=""/>
                    </div>
                    <div class="col-md-7">
                        <label for="nascimento">Nascimento:</label>
                        <input required="true" autocomplete="off" id="nascimento" type="date" class="form-control input-sm" name="nascimento" value=""/>
                    </div>
                    <div class="col-md-7">
                        <label for="sexo">Sexo:</label>
                        <select name="sexo" id="sexo" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">seleccione...</option>
                            <option value="1">Feminino</option>
                            <option value="2">Masculino</option>
                            <option value="3">Indefinido</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="deficiencia">Deficiencia:</label>
                        <select name="deficiencia" id="deficiencia" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="">Trabalho formal:</label>
                        <select name="trabalho_formal" id="trabalho_formal" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="">Trabalho informal:</label>
                        <select name="trabalho_informal" id="trabalho_informal" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="">Desempregado:</label>
                        <select name="trabalho_desempregado" id="trabalho_desempregado" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <label for="">Renda:</label>
                        <input required="true" autocomplete="off" id="renda" type="number" class="form-control input-sm" name="renda" value=""/>
                    </div>
                    <div class="col-md-7">
                        <label for="">Beneficio inss:</label>
                        <select name="beneficio_inss" id="beneficio_inss" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="">Beneficio bpc:</label>
                        <select name="beneficio_bpc" id="beneficio_bpc" class="form-control input-sm">// vamos criar a visualização de sexo
                            <option value="0">Não</option>
                            <option value="1">Sim</option>
                        </select>
                    </div>
                    <br>
                    <div class="col-md-12">
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                    </div>
                </form>
            </div>
        </div>

	</div>
	
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>