<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoachsave";
    $ch=fncgetpessoach($_GET['id']);
}else{
    $a="pessoachnew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vhome&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
        <hr>
    <div class="row">

        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ch['id']; ?>"/>

        <div class="col-md-12">
            <label   for="cadastro_novo">Cadastro novo:</label>
            <select name="cadastro_novo" id="cadastro_novo" class="form-control">
                // vamos criar a visualizacao
                <option selected="" value="<?php if($ch['cadastro_novo']==""){$z=0; echo $z;}else{ echo $ch['cadastro_novo'];} ?>">
                    <?php
                    if($ch['cadastro_novo']==0){echo"Não";}
                    if($ch['cadastro_novo']==1){echo"Sim";}
                    ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>

        <div class="col-md-4">
        <label for="nis">NIS:</label><input  required="true" autocomplete="off" id="nis" type="text" class="form-control" name="nis" value="<?php echo $ch['nis']; ?>"/>
        </div>

        <div class="col-md-4">
        <label for="nome">Nome:</label>
            <input required="true" autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $ch['nome']; ?>"/>
        </div>

        <div class="col-md-5">
            <label for="logradouro">ENDEREÇO</label>
            <input autocomplete="off" id="logradouro" type="text" class="form-control" name="logradouro" value="<?php echo $ch['logradouro']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="numero">NÚMERO</label>
            <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $ch['numero']; ?>" required min="0"/>
        </div>

        <div class="col-md-4">
            <label for="bairro">Bairro:</label>
            <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php
                $bairroid = $ch['bairro'];
                include_once("{$env->env_root}controllers/mcu/bairrolista.php");
                $getbairro=fncgetbairro($bairroid);
                ?>
                <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $ch['bairro']; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                $bairrolista=fncbairrolist();
                foreach ($bairrolista as $item) {
                    ?>
                    <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                        <?php echo $item['bairro']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>


    <div class="row">
    <div class="col-md-4">
        <label for="cpf">CPF:</label><input  required="true" autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $ch['cpf']; ?>"/>
    </div>
    <div class="col-md-4">
     <label for="renda_declarada">Renda Declarada:</label><input required="true" autocomplete="off" id="renda_declarada" type="number" class="form-control" name="renda_declarada" value="<?php echo $ch['renda_declarada']; ?>"/>
    </div>
    </div>

<div class="row">
    <div class="col-md-4">
        <label   for="c_idoso">Idoso:</label>
        <select name="c_idoso" id="c_idoso" class="form-control">
        // vamos criar a visualizacao
        <option selected="" value="<?php if($ch['c_idoso']==""){$z=0; echo $z;}else{ echo $ch['c_idoso'];} ?>">
            <?php
            if($ch['c_idoso']==0){echo"Não";}
            if($ch['c_idoso']==1){echo"Sim";} 
            ?>
        </option>
        <option value="0">Não</option>
        <option value="1">Sim</option>
        </select>
    </div>
</div>


<div class="row">
    <div class="col-md-3">
    <label   for="risco_insalubre">Risco/Insalubre:</label>
        <select name="risco_insalubre" id="risco_insalubre" class="form-control">
        // vamos criar a visualização
        <option selected="" value="<?php if($ch['risco_insalubre']==""){$z=0; echo $z;}else{ echo $ch['risco_insalubre'];} ?>">
            <?php
            if($ch['risco_insalubre']==0){echo"Não";}
            if($ch['risco_insalubre']==1){echo"Sim";} 
            ?>
        </option>
        <option value="0">Não</option>
        <option value="1">Sim</option>
        </select>
    </div>
    <div class="col-md-3">
        <label   for="c_pcd">PCD:</label>
        <select name="c_pcd" id="c_pcd" class="form-control">
        // vamos criar a visualizacao
        <option selected="" value="<?php if($ch['c_pcd']==""){$z=0; echo $z;}else{ echo $ch['c_pcd'];} ?>">
            <?php
            if($ch['c_pcd']==0){echo"Não";}
            if($ch['c_pcd']==1){echo"Sim";} 
            ?>
        </option>
        <option value="0">Não</option>
        <option value="1">Sim</option>
        </select>
    </div>
    <div class="col-md-3">
        <label   for="c_mulher">Mulher chefe de familia:</label>
        <select name="c_mulher" id="c_mulher" class="form-control">
        // vamos criar a visualizacao
        <option selected="" value="<?php if($ch['c_mulher']==""){$z=0; echo $z;}else{ echo $ch['c_mulher'];} ?>">
            <?php
            if($ch['c_mulher']==0){echo"Não";}
            if($ch['c_mulher']==1){echo"Sim";} 
            ?>
        </option>
        <option value="0">Não</option>
        <option value="1">Sim</option>
        </select>
    </div>
    
</div>

    <div class="row">
        <div class="col-md-3">
        <label   for="c_reside_mcu_5">Reside em MCU a 5 anos ou +:</label>
            <select name="c_reside_mcu_5" id="c_reside_mcu_5" class="form-control">
            // vamos criar a visualizacao
            <option selected="" value="<?php if($ch['c_reside_mcu_5']==""){$z=0; echo $z;}else{ echo $ch['c_reside_mcu_5'];} ?>">
                <?php
                if($ch['c_reside_mcu_5']==0){echo"Não";}
                if($ch['c_reside_mcu_5']==1){echo"Sim";}
                ?>
            </option>
            <option value="0">Não</option>
            <option value="1">Sim</option>
            </select>
        </div>

        <div class="col-md-3">
        <label   for="c_bs">Benefício Social:</label>
            <select name="c_bs" id="c_bs" class="form-control">
            // vamos criar a visualizacao
            <option selected="" value="<?php if($ch['c_bs']==""){$z=0; echo $z;}else{ echo $ch['c_bs'];} ?>">
                <?php
                if($ch['c_bs']==0){echo"Não";}
                if($ch['c_bs']==1){echo"Sim";}
                ?>
            </option>
            <option value="0">Não</option>
            <option value="1">Sim</option>
            </select>
        </div>

        <div class="col-md-3">
            <label   for="c_familia_com_menores">Família com Menores:</label>
            <select name="c_familia_com_menores" id="c_familia_com_menores" class="form-control">
            // vamos criar a visualizacao
            <option selected="" value="<?php if($ch['c_familia_com_menores']==""){$z=0; echo $z;}else{ echo $ch['c_familia_com_menores'];} ?>">
                <?php
                if($ch['c_familia_com_menores']==0){echo"Não";}
                if($ch['c_familia_com_menores']==1){echo"Sim";}
                ?>
            </option>
            <option value="0">Não</option>
            <option value="1">Sim</option>
            </select>
        </div>
    </div>
    <div class="row">
    <div class="col-md-3">
        <label   for="">Benefício bf:</label>
        <select name="beneficio_bf" id="beneficio_bf" class="form-control">
            // vamos criar a visualizacao
            <option selected="" value="<?php if($ch['beneficio_bf']==""){$z=0; echo $z;}else{ echo $ch['beneficio_bf'];} ?>">
                <?php
                if($ch['beneficio_bf']==0){echo"Não";}
                if($ch['beneficio_bf']==1){echo"Sim";}
                ?>
            </option>
            <option value="0">Não</option>
            <option value="1">Sim</option>
        </select>
    </div>

    <div class="col-md-4">
        <label for="">Benefício bf valor:</label>
        <input required="true" autocomplete="off" id="beneficio_bf_valor" type="number" class="form-control" name="beneficio_bf_valor" value="<?php echo $ch['beneficio_bf_valor']; ?>"/>
    </div>
</div>

</div>

    </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>