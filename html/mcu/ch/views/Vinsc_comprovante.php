<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Impressão de Comprovante-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
// Recebe

$sql = "SELECT * \n"
    . "FROM mcu_ch \n"
    . "WHERE id=? ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ps = $consulta->fetch();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h5 class="my-0"><strong>SECRETARIA MUNICIPAL DO TRABALHO E DESENVOLVIMENTO SOCIAL</strong></h5>
            <h5 class="my-0"><strong class="text-uppercase">INSCRIÇÃO PROGRAMA CASA VERDE E AMARELA</strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo get_criptografa64($ps['id']);?></h4>
            <h3> NOME: <strong><?php echo strtoupper($ps['nome']);?></strong></h3>
            <h4> CPF: <?php echo mask($ps['cpf'],'###.###.###-##');?></h4>
            <h4 class="text-uppercase"> ENDEREÇO: <?php echo $ps['logradouro']." ".$ps['numero']." ".fncgetbairro($ps['bairro'])['bairro'];?> </h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($_SESSION['id'])['nome']);?></h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDATO:______________________________________________________</h6>
            <h6 class="float-right"> FWS</h6>
        </div>
    </div>
</main>

<br>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h5 class="my-0"><strong>SECRETARIA MUNICIPAL DO TRABALHO E DESENVOLVIMENTO SOCIAL</strong></h5>
            <h5 class="my-0"><strong class="text-uppercase">INSCRIÇÃO PROGRAMA CASA VERDE E AMARELA</strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo get_criptografa64($ps['id']);?></h4>
            <h3> NOME: <strong><?php echo strtoupper($ps['nome']);?></strong></h3>
            <h4> CPF: <?php echo mask($ps['cpf'],'###.###.###-##');?></h4>
            <h4 class="text-uppercase"> ENDEREÇO: <?php echo $ps['logradouro']." ".$ps['numero']." ".fncgetbairro($ps['bairro'])['bairro'];?> </h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($_SESSION['id'])['nome']);?></h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDATO:______________________________________________________</h6>
            <h6 class="float-right"> FWS</h6>
        </div>
    </div>
</main>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>