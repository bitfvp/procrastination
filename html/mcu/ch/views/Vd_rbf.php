<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Renda BF-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT beneficio_bf_valor \n"
    . "FROM mcu_ch\n"
    . " where beneficio_bf=1";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$cont = $consulta->rowCount();
$sql = null;
$consulta = null;
$somadora=0;
foreach ($valor as $vl){
            $somadora+=$vl[0];
}
$toral=$somadora/$cont;
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>

    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Quantidades de BF&nbsp;</th>
            <th>Media de Valores&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $cont; ?> Pessoas&nbsp;</td>
            <td><?php echo "R$".number_format($toral,"2",',','.'); ?>&nbsp;</td>
        </tr>
        </tbody>
    </table>

</main>
</body>
</html>