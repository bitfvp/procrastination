<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Relatorio-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * \n"
    . "FROM mcu_ch\n"
    . "ORDER BY nome ";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$atf = $consulta->fetchAll();
$atfcont = $consulta->rowCount();
$sql = null;
$consulta = null;
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:10px !important;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <h1>Relatorio a/z</h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Nome</th>
            <th>CPF</th>
            <th>Risco/Insalubre</th>
            <th>PCD</th>
            <th>Mulher C.F.</th>
            <th>Reside em mcu +5anos</th>
            <th>Benefício Social</th>
            <th>Familia Com Menores</th>
            <th>Contagem de Criterio</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $acont = 0;
        $a = $atf[0]['horario'];
        function restsimb ($vari) {
            if ($vari == 0) {
                echo "✖";
            }
            if ($vari == 1) {
                echo "✔";
            }
        }
        foreach ($atf as $at) {
            ?>
            <tr>
                <td><?php echo $at['nome']; ?></td>
                <td><?php echo $at['cpf']; ?></td>
                <td><?php
                    restsimb($at['risco_insalubre']);
                    ?></td>
                <td><?php
                    restsimb($at['c_pcd']);
                    ?></td>
                <td><?php
                    restsimb($at['c_mulher']);
                    ?></td>
                <td><?php
                    restsimb($at['c_reside_mcu_5']);
                    ?></td>
                <td><?php
                    restsimb($at['c_bs']);
                    ?></td>
                <td><?php
                    restsimb($at['c_familia_com_menores']);
                    ?></td>
                <td><?php echo $at['contcriterio']; ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td colspan="9">Total: <?php echo $atfcont; ?></td>
        </tr>
        </tbody>
    </table>
</main>
</body>
</html>