<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Idade-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");
//mcu_ch_familia
$sql = "SELECT TIMESTAMPDIFF(YEAR, nascimento, CURDATE()) as idade FROM mcu_ch_familia";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$cont = $consulta->rowCount();
$sql = null;
$consulta = null;
foreach ($valor as $vl){
    switch ($vl[0]){
        case ($vl[0]<=6):
            $v6++;
            break;
        case ($vl[0]>6 and $vl[0]<=14):
            $v7_14++;
            break;
        case ($vl[0]>14 and $vl[0]<=29):
            $v15_29++;
            break;
        case ($vl[0]>29 and $vl[0]<=59):
            $v30_59++;
            break;
        case ($vl[0]>59):
            $v60++;
            break;
    }
}
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Quantidade'],
                ['Ate 6 anos',     <?php echo $v6; ?>],
                ['7 a 14 anos',     <?php echo $v7_14; ?>],
                ['15 a 29 anos',     <?php echo $v15_29; ?>],
                ['30 a 59 anos',     <?php echo $v30_59; ?>],
                ['Acima de 60 anos',      <?php echo $v60; ?>]
            ]);

            var options = {
                title: '',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Faixa Etaria&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td> Ate 6 anos&nbsp;</td>
            <td><?php echo $v6; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td> 7 a 14 anos&nbsp;</td>
            <td><?php echo $v7_14; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td> 15 a 29 anos&nbsp;</td>
            <td><?php echo $v15_29; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td> 30 a 59 anos&nbsp;</td>
            <td><?php echo $v30_59; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td> Acima de 60 anos&nbsp;</td>
            <td><?php echo $v60; ?>&nbsp;Pessoas</td>
        </tr>
        </tbody>
    </table>
    <div id="donutchart" style="width: 900px; height: 500px;"></div>
</main>
</body>
</html>