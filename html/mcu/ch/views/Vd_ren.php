<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Renda-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT renda_declarada \n"
    . "FROM mcu_ch\n"
    . "";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$sql = null;
$consulta = null;

foreach ($valor as $vl){
    switch ($vl[0]){
        case ($vl[0]<469):
            $vatemeio++;
            break;
        case ($vl[0]>=469 and $vl[0]<=937):
            $vateum++;
            break;
        case ($vl[0]>937):
            $avacimaum++;
            break;
    }

}

?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Faixa', 'Quantidade'],
                ['Ate meio salario minimo', <?php echo $vatemeio;?>],
                ['Ate um salario minimo', <?php echo $vateum;?>],
                ['Acima de um salario minimo', <?php echo $avacimaum;?>],
            ]);

            var options = {
                title: '',
                slices: {  1: {offset: 0.2},
                    2: {offset: 0.2},

                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Renda&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Ate meio salario minimo&nbsp;</td>
            <td><?php echo $vatemeio; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Ate um salario minimo&nbsp;</td>
            <td><?php echo $vateum; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Acima de um salario minimo&nbsp;</td>
            <td><?php echo $avacimaum; ?>&nbsp;Pessoas</td>
        </tr>
        </tbody>
    </table>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
</main>
</body>
</html>