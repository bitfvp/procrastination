<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_73"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

    if (isset($_GET['sca'])){
        $sca = $_GET['sca'];
        $scb = $_GET['scb'];
        $sql = "select * from mcu_ch WHERE nome LIKE '%$sca%' AND cpf LIKE '%$scb%' ";
    }else {

        $sql = "select * from mcu_ch ";
    }

    $total_reg = "50"; // número de registros por página

    $pgn=$_GET['pgn'];
    if (!$pgn) {
        $pc = "1";
    } else {
        $pc = $pgn;
    }

    $inicio = $pc - 1;
    $inicio = $inicio * $total_reg;

    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $limite=$pdo->prepare($sql.$sql2);
        $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    //todos
    try{
        $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $todos=$pdo->prepare($sql);
        $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $tr=$todos->rowCount();// verifica o número total de registros
    $tp = $tr / $total_reg; // verifica o número total de páginas
    ?>

<main class="container"><!--todo conteudo-->
    <h2>Cadastro Habitacional</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vhome" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por nome..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vpessoaeditar" class="btn btn btn-success btn-block col-md-6 float-right">
        Novo Cadastro
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <br>
    <table class="table table-stripe table-hover table-sm table-striped">
        <thead>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
            <thead class="thead-dark">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
                <th scope="col">INFO</th>
                <th scope="col">EDITAR</th>
            </tr>
            </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Pessoa(s) listado(s)</th>
        </tr>
        </tfoot>

            <tbody>
            <?php
            // vamos criar a visualização
            foreach ($limite as $dados){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
                $cpf = $dados["cpf"];

                if ($dados["cadastro_novo"]==1){
                    $cadastro_novo="Cadastro novo";
                }else{
                    $cadastro_novo="";
                }
            ?>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = strtoupper($_GET['sca']);
                            define('CSA', $sta);//TESTE
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-success'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>
                <td>
                    <?php
                    if($_GET['scb']!="") {
                        $stb = strtoupper($_GET['scb']);
                        define('CSB', $stb);//TESTE
                        $stb = CSB;
                        $ttt = $cpf;
                        $tt = explode(CSB, $ttt);
                        $t = implode("<span class='text-success'>{$stb}</span>", $tt);
                        echo $t;
                    }else{
                        echo $cpf;
                    }
                    ?>
                </td>
                <td>
                    <?php
                        echo $cadastro_novo;
                    ?>
                </td>

                <td>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados desse Cadastro">
                        Alterar
                    </a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
