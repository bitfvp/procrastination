<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Genero-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * \n"
    . "FROM mcu_ch_familia\n"
    . "where sexo=1";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$mulcont = $consulta->rowCount();
$sql = null;
$consulta = null;

$sql = "SELECT * \n"
    . "FROM mcu_ch_familia\n"
    . "where sexo=2";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$homcont = $consulta->rowCount();
$sql = null;
$consulta = null;
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Quantidade'],
                ['Feminino',     <?php echo $mulcont; ?>],
                ['Masculino',      <?php echo $homcont; ?>]
            ]);

            var options = {
                title: '',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Genero&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Feminino&nbsp;</td>
            <td><?php echo $mulcont; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Masculino</td>
            <td><?php echo $homcont; ?>&nbsp;Pessoas</td>
        </tr>
        </tbody>
    </table>
    <div id="donutchart" style="width: 900px; height: 500px;"></div>
</main>
</body>
</html>