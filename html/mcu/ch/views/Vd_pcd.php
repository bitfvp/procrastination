<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Pcd-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * \n"
    . "FROM mcu_ch_familia\n"
    . "where deficiencia=1";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$pcdcont = $consulta->rowCount();
$sql = null;
$consulta = null;

$sql = "SELECT * \n"
    . "FROM mcu_ch_familia\n"
    . "where deficiencia=0";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$norcont = $consulta->rowCount();
$sql = null;
$consulta = null;
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Tipo', 'Quantidade'],
                ['Pcd', <?php echo $pcdcont;?>], ['Outros', <?php echo $norcont;?>],
            ]);

            var options = {
                title: '',
                slices: {  1: {offset: 0.2},

                },
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Tipo&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Pcd&nbsp;</td>
            <td><?php echo $pcdcont; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>Outros</td>
            <td><?php echo $norcont; ?>&nbsp;Pessoas</td>
        </tr>
        </tbody>
    </table>
    <div id="piechart" style="width: 900px; height: 500px;"></div>
</main>
</body>
</html>