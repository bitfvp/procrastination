<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_73"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Pcd-" . $env->env_titulo;
$css = "print";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT 	* \n"
    . "FROM mcu_ch_familia\n";

global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$valor = $consulta->fetchAll();
$sql = null;
$consulta = null;
$vinss=$vbpc=$vsem=0;
foreach ($valor as $vl){
    if ($vl['beneficio_inss']==0 and $vl['beneficio_bpc']==0){
        $vsem++;
    }
    if ($vl['beneficio_inss']==1){
        $vinss++;
    }
    if ($vl['beneficio_bpc']==1){
        $vbpc++;
    }
}
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:15px !important;
            widows: 2;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var data = google.visualization.arrayToDataTable([
                ['Tipo', 'Quantidade',],
//                ['Sem Beneficio', <?php //echo $vsem;?>//],
                ['BPC', <?php echo $vbpc;?>],
                ['INSS', <?php echo $vinss;?>]
            ]);

            var options = {
                title: '',
                chartArea: {width: '50%'},
                hAxis: {
                    title: 'Total',
                    minValue: 0
                },
                vAxis: {
                    title: ''
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>
    <h1>Diagnostico </h1>
    <table class="table table-striped table-bordered table-sm table-responsive">
        <thead class="thead-default">
        <tr>
            <th>Tipo&nbsp;</th>
            <th>quantidade&nbsp;</th>
        </tr>
        </thead>
        <tbody>
<!--        <tr>-->
<!--            <td>Sem Beneficio Social&nbsp;</td>-->
<!--            <td>--><?php //echo $vsem; ?><!--&nbsp;Pessoas</td>-->
<!--        </tr>-->
        <tr>
            <td>BPC&nbsp;</td>
            <td><?php echo $vbpc; ?>&nbsp;Pessoas</td>
        </tr>
        <tr>
            <td>INSS&nbsp;</td>
            <td><?php echo $vinss; ?>&nbsp;Pessoas</td>
        </tr>

        </tbody>
    </table>
    <div id="chart_div"></div>
</main>
</body>
</html>