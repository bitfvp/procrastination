<?php
function fncpessoachlist(){
    $sql = "SELECT * FROM mcu_ch ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}

function fncgetpessoach($id){
    $sql = "SELECT * FROM mcu_ch WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getpessoa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getpessoa;
}