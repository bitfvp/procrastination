<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de Categorias-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_abrigoe_categoria WHERE categoria LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_abrigoe_categoria ";
}
// total de registros a serem exibidos por página
$total_reg = "100"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY categoria LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY categoria LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->

<h2>Listagem de Categorias</h2>
<form class="" action="index.php" method="get">
                        <input name="pg" value="Vcategoria_lista" hidden/>
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            <input type="text" autofocus="true" autocomplete="off" class="form-control col-lg-8" placeholder="Buscar por Produto..." name="sca" value="" />
                            </div>
</form>

<table id="tabela" class="table table-striped table-bordered table-hover">
<a href="index.php?pg=Vcategoria_editar" class="btn btn btn-success btn-block">
    NOVA CATEGORIA
</a>
<br>
<thead>
	<tr>
		<th>CATEGORIA</th>
        <th>EDITAR</th>
	</tr>
</thead>
<tbody>
 <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $categoria = $dados["categoria"];
            ?>

	<tr>
        <td><?php
            if($_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
            $sta = CSA;
            $ccc = $categoria;
            $cc = explode(CSA, $ccc);
            $c = implode("<span class='text-success'>{$sta}</span>", $cc);
            echo strtoupper($c);
            }else{
            echo $categoria;
            }
            ?>
        </td>
        <td><a href="index.php?pg=Vcategoria_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a></td>
	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
