<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Produto-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="produtosave";
    $produto=fncgetproduto($_GET['id']);
}else{
    $a="produtonew";
}
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-6">
            <a href="index.php?pg=Vproduto_editar" class="btn btn-default btn-block">Novo</a>
        </div>

        <form class="form-signin" action="<?php echo "index.php?pg=Vproduto_lista&aca={$a}"; ?>" method="post">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $produto['id']; ?>"/>
            <label for="produto">Produto</label>
            <input autocomplete="off"  placeholder="Produto" required="true" autofocus id="produto" type="text" class="form-control" name="produto" value="<?php echo $produto['produto']; ?>"/>
        </div>

        <div class="col-md-12">
        <label   for="unidade">Unidade</label>
            <select name="unidade" required="true" id="unidade" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php echo $produto['unidade'];?>">
                    <?php
                    $unidadeid=$produto['unidade'];
                    $unidade=fncgetunidade($unidadeid);
                    echo $unidade['unidade'];
                    ?>
                </option>
                <?php
                foreach(fncunidadelist() as $item){?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['unidade']; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-12">
        <label   for="categoria">Categoria</label>
            <select name="categoria" required="true" id="categoria" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php echo $produto['categoria'];?>">
                    <?php
                    $categoriaid=$produto['categoria'];
                    $categoria=fncgetcategoria($categoriaid);
                    echo $categoria['categoria'];
                    ?>
                </option>
                <?php
                foreach(fnccategorialist() as $item){?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['categoria']; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-12">
            <label for="estoque_min">Estoque Minimo</label><input autocomplete="off"  required="true" autofocus id="estoque_min" type="text" class="form-control" name="estoque_min" value="<?php 
            if (isset($produto['estoque_min'])) {
                echo $produto['estoque_min'];
            }else{
                echo "0";
            }
            ?>"/>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>