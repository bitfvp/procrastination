<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Produto Historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $produto=fncgetproduto($_GET['id']);
}


$sql = "SELECT ";
$sql .= "Sum(mcu_abrigoe_pedido.quantidade) AS SomaDeQuantidade ";
$sql .= "FROM ";
$sql .= "mcu_abrigoe_pedido ";
$sql .= "WHERE ";
$sql .= "mcu_abrigoe_pedido.tipo = 1 AND ";
$sql .= "mcu_abrigoe_pedido.produto_id = {$_GET['id']} ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$entradasoma = $consulta->fetch();
$sql=null;
$consulta=null;

$sql = "SELECT ";
$sql .= "Sum(mcu_abrigoe_pedido.quantidade) AS SomaDeQuantidade ";
$sql .= "FROM ";
$sql .= "mcu_abrigoe_pedido ";
$sql .= "WHERE ";
$sql .= "mcu_abrigoe_pedido.tipo = 2 AND ";
$sql .= "mcu_abrigoe_pedido.produto_id = {$_GET['id']} ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$saidasoma = $consulta->fetch();
$sql=null;
$consulta=null;

$estoque = $entradasoma["SomaDeQuantidade"] - $saidasoma["SomaDeQuantidade"];
?>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Produto</h3>
                </div>
                <blockquote>
                    <header>PRODUTO:<strong class="text-info"><?php echo $produto['produto']; ?>&nbsp;&nbsp;</strong></header>
                    <p>
                    <h5>
                        CATEGORIA:<strong class="text-info">
                            <?php
                            if ($produto['categoria']!="0"){
                                $categoria=fncgetcategoria($produto['categoria']);
                                echo $categoria['categoria'];
                            }else{
                                    echo "???????";
                            }
                            ?>
                        </strong>
                    </h5>
                    <h5>
                        UNIDADE:<strong class="text-info">
                        <?php
                        if ($produto['unidade']!="0"){
                            $unidade=fncgetunidade($produto['unidade']);
                            echo $unidade['unidade'];
                        }else{
                            echo "???????";
                        }
                        ?>
                        </strong>
                    </h5>
                    <h4>
                        ESTOQUE:<strong class="text-info">
                            <?php
                            if ($estoque < $produto['estoque_min']) {
                                echo "<abbr title=\"Estoque Minimo {$produto['estoque_min']}\" class=\"initialism\"><span class=\"label label-danger\">{$estoque}</span></abbr>";
                            }else{
                                echo "<abbr title=\"Estoque Minimo {$produto['estoque_min']}\" class=\"initialism\"><span class=\"label label-success\">{$estoque}</span></abbr>";
                            }
                            ?>
                        </strong>
                    </h4>
                    </p>
                </blockquote>
                <a class="btn btn-success btn-block" href="index.php?pg=Vproduto_editar&id=<?php echo $_GET['id']; ?>" title="editar produto">
                    Editar Produto
                </a>
            </div>
            <!-- fim da col md 4 -->
        </div>

        <div class="col-md-10">
            <?php
            $sql = "SELECT mcu_abrigoe_pedido.id, mcu_abrigoe_pedido.tipo, mcu_abrigoe_pedido.produto_id, mcu_abrigoe_produto.produto, mcu_abrigoe_pedido.quantidade, mcu_abrigoe_pedido.data, tbl_users.nome AS responsavel, mcu_abrigoe_pedido.nota_id\n"
                . "FROM mcu_abrigoe_produto INNER JOIN (tbl_users INNER JOIN mcu_abrigoe_pedido ON tbl_users.id = mcu_abrigoe_pedido.responsavel) ON mcu_abrigoe_produto.id = mcu_abrigoe_pedido.produto_id\n"
                . "WHERE (((mcu_abrigoe_pedido.produto_id)=?))\n"
                . "ORDER BY mcu_abrigoe_pedido.data DESC";
            global $pdo;
            $cons = $pdo->prepare($sql);
            $cons->bindParam(1, $_GET['id']);
            $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pedlista = $cons->fetchAll();
            $sql=null;
            $consulta=null;
            ?>
            <table id="tabela" class="table table-striped table-bordered table-hover">
                <thead class="bg-primary">
                <tr>
                    <th>Tipo</th>
                    <th>Produto</th>
                    <th>Quant.</th>
                    <th>Data</th>
                </tr>
                </thead>
                <tbody>
                <?php
                // vamos criar a visualização
                foreach ($pedlista as $dados) {
                    $id = $dados["id"];
                    $produto_id = $dados["produto_id"];
                    $produto = $dados["produto"];
                    $data= $dados["data"];
                    $tipo=$dados["tipo"];
                    $nota_id=$dados["nota_id"];
                    $responsavel=$dados["responsavel"];
                    $categoria=$dados["categoria"];
                    $quant_total = $dados["quantidade"];
                    ?>

                    <tr data-toggle="collapse" data-target="#accordion<?php echo $id; ?>" class="clickable">
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                        <?php if ($tipo==1) { echo "Entrada"; } if ($tipo==2) { echo "Saida";} ?>
                        <i class="fa fa-exchange pull-right"></i>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                        <?php echo $produto; ?>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                            <?php echo $quant_total; ?>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                            <?php echo datahorabanco2data($data); ?>
                        </td>
                    </tr>

                    <tr id="accordion<?php echo $id; ?>" class="collapse">
                        <td colspan="4">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul>
                                        Responsavel:<li><?php echo $responsavel; ?></li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>
                                        Nota:<li>
                                            <?php
                                            if ($tipo==1) {
                                                $sql = "SELECT * FROM mcu_abrigoe_nota_entrada WHERE id=?";
                                                global $pdo;
                                                $consulta = $pdo->prepare($sql);
                                                $consulta->bindParam(1, $nota_id);
                                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                                $notain = $consulta->fetch();

                                                $sql=null;
                                                $consulta=null;

                                                echo "<a href=\"index.php?pg=Vnota_entrada&id={$nota_id}\">";
                                                echo $notain['identificador'];
                                                echo "</a>";
                                            }
                                            if ($tipo==2) {
                                                $sql = "SELECT * FROM mcu_abrigoe_nota_saida WHERE id=?";
                                                global $pdo;
                                                $consulta = $pdo->prepare($sql);
                                                $consulta->bindParam(1, $nota_id);
                                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                                $notaout = $consulta->fetch();

                                                $sql=null;
                                                $consulta=null;

                                                echo "<a href=\"index.php?pg=Vnota_saida&id={$nota_id}\">";
                                                echo $notaout['identificador'];
                                                echo "</a>";
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>


        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>