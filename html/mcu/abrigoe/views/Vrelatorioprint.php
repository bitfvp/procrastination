<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatorio de consumo-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
?>
<style>
    @page {
        /*margin: 0.7cm;*/
        margin-bottom: 0.5cm;
        margin-top: 0.5cm;
        margin-left: 0.5cm;
        margin-right: 0.5cm;
    }
</style>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";

    $sql ="SELECT "
        ."Sum(mcu_abrigoe_pedido.quantidade) AS quantidade, "
        ."mcu_abrigoe_produto.produto "
        ."FROM "
        ."mcu_abrigoe_pedido "
        ."INNER JOIN mcu_abrigoe_produto ON mcu_abrigoe_pedido.produto_id = mcu_abrigoe_produto.id "
        ."WHERE "
        ."mcu_abrigoe_pedido.tipo = 2 "
        ."AND mcu_abrigoe_pedido.`data` >= :inicial "
        ."AND mcu_abrigoe_pedido.`data` <= :final "
        ."GROUP BY "
        ."mcu_abrigoe_produto.produto "
        ."ORDER BY "
        ."mcu_abrigoe_produto.produto ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $consumo = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h1>RELATORIO DE CONSUMO</h1>

        <table class="table table-condensed table-striped table-sm">
            <thead>
            <tr>
                <td>PRODUTO&nbsp;</td>
                <td>QUANTIDADE&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalcb = 0;
            foreach ($consumo as $cs) {
                ?>

                <tr>
                    <td><?php echo $cs['produto']; ?>&nbsp;</td>
                    <td><?php echo $cs['quantidade']; ?>&nbsp;</td>
                </tr>


                <?php
            }
            ?>
            </tbody>
        </table>
        <h4>Periodo: <?php echo dataRetiraHora($inicial); ?> à <?php echo dataRetiraHora($final); ?></h4>
        <h5>Relatorio tirado em: <?php echo date('d'."/".'m'."/".'Y'); ?> as <?php echo date('H'.":".'i'.":".'s'); ?></h5>

    </div>


</main>
</body>
</html>