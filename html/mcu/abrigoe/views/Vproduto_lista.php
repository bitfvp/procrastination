<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_abrigoe_produto WHERE produto LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_abrigoe_produto ";
}
// total de registros a serem exibidos por página
$total_reg = "30"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->

<h2>Listagem do Estoque</h2>
<form class="" action="index.php" method="get">
                        <input name="pg" value="Vproduto_lista" hidden/>
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            <input type="text" autofocus="true" autocomplete="off" class="form-control col-lg-8" placeholder="Buscar por Produto..." name="sca" value="" />
                            </div>
</form>

<table id="tabela" class="table table-striped table-bordered table-hover">
<a href="index.php?pg=Vproduto_editar" class="btn btn btn-success btn-block">
    NOVO PRODUTO
</a>
<br>
<thead>
	<tr>
		<th>PRODUTO</th>
        <th class="bg-info">QUANTIDADE.</th>
        <th>CATEGORIA.</th>
    </tr>
</thead>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
<tbody>
 <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id_prod = $dados["id"];
            $produto = $dados["produto"];
            $unidade = $dados["unidade"];
            $categoria = $dados["categoria"];
            $estoque_min = $dados["estoque_min"];

            $sql = "SELECT ";
            $sql .= "Sum(mcu_abrigoe_pedido.quantidade) AS SomaDeQuantidade ";
            $sql .= "FROM ";
            $sql .= "mcu_abrigoe_pedido ";
            $sql .= "WHERE ";
            $sql .= "mcu_abrigoe_pedido.tipo = 1 AND ";
            $sql .= "mcu_abrigoe_pedido.produto_id = {$id_prod} ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $entradasoma = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT ";
            $sql .= "Sum(mcu_abrigoe_pedido.quantidade) AS SomaDeQuantidade ";
            $sql .= "FROM ";
            $sql .= "mcu_abrigoe_pedido ";
            $sql .= "WHERE ";
            $sql .= "mcu_abrigoe_pedido.tipo = 2 AND ";
            $sql .= "mcu_abrigoe_pedido.produto_id = {$id_prod} ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $saidasoma = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $estoque = $entradasoma["SomaDeQuantidade"] - $saidasoma["SomaDeQuantidade"];
            ?>

	<tr>
        <td><?php
            if($_GET['sca']!=null) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
            $sta = CSA;
            $ccc = $produto;
            $cc = explode(CSA, $ccc);
            $c = implode("<span class='text-success'>{$sta}</span>", $cc);
            echo strtoupper($c);
            }else{
            echo $produto;
            }
            ?>

            <a href="index.php?pg=Vproduto_historico&id=<?php echo $id_prod; ?>"><span class="fa fa-tasks pull-left"></span></a>
            <a href="index.php?pg=Vproduto_editar&id=<?php echo $id_prod; ?>"><span class="fa fa-wrench pull-right"></span></a>
        </td>
        <td class="bg-info">
            <?php
            if ($unidade!="0"){
                $unidadeid=$unidade;
                $unidade=fncgetunidade($unidadeid);
                $unn=$unidade['unidade'];
            }else{
                $unn = "???????";
            }

            $labelcor="success";
            if ($estoque < $estoque_min) {
             $labelcor="warning";
            }
            if ($estoque <= 0) {
             $labelcor="danger";
            }
            echo "<abbr title=\"Estoque Minimo {$estoque_min}\" class=\"initialism\"><span class=\"label label-{$labelcor}\">{$estoque}</span></abbr> {$unn}`s";
            ?>
        </td>
        <td>
            <?php
            if ($categoria!="0"){
                $categoriaid=$categoria;
                $categoria=fncgetcategoria($categoriaid);
                echo $categoria['categoria'];
            }else{
                echo "???????";
            }
            ?>
        </td>


	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
