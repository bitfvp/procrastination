<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Nota de Saida-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="notasaidasave";
    $nota_saida=fncgetnotasaida($_GET['id']);
}else{
    $a="notasaidanew";
}
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-6">
            <a href="index.php?pg=Vnota_saida_editar" class="btn btn-default btn-block">Novo</a>
        </div>

        <form class="form-signin" action="<?php echo "index.php?pg=Vnota_saida&aca={$a}"; ?>" method="post">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $nota_saida['id']; ?>"/>
            <label for="identificador">Identificador</label><input autocomplete="off" autofocus id="identificador" required="true" type="text" class="form-control" name="identificador" value="<?php echo $nota_saida['identificador']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="data">Data</label><input autocomplete="off" id="data" required="true" type="date" class="form-control" name="data" value="<?php echo $nota_saida['data']; ?>"/>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>