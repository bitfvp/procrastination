<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_71"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Nota de Saida-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <!-- direito -->
        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados da Nota</h3>
                </div>
                <?php
                if (isset($_GET['id']) and is_numeric($_GET['id'])){
                    $nota_saida=fncgetnotasaida($_GET['id']);
                }else{
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];
                    header("Location: index.php");
                    exit();
                }
                ?>
                <blockquote>
                    <header>
                        Identificador:<strong class="text-info"><?php echo $nota_saida['identificador']; ?>
                            &nbsp;&nbsp;</strong>
                    </header>
                    <p>
                    <h5>
                        Data:<strong class="text-info"><?php echo dataBanco2data($nota_saida['data']); ?>
                            &nbsp;&nbsp;</strong>
                    </h5>
                    </p>
                    <footer>
                        Responsavel
                        <strong class="text-info"><?php
                            if ($nota_saida['responsavel'] != "0") {
                                $profissional=fncgetprofissional($nota_entrada['responsavel']);
                                echo "<span class='text-muted'>";
                                echo $profissional['nome'];
                                echo "</span>";
                            } else {
                                echo "<span class='text-danger'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;&nbsp;
                    </footer>
                </blockquote>

                <a class="btn btn-success btn-block"
                   href="index.php?pg=Vnota_saida_editar&id=<?php echo $_GET['id']; ?>" title="Edite nota">
                    Editar Nota
                </a>
            </div>
            <!-- fim da col md 4 -->
        </div>


        <!-- esquerdo -->
        <div class="col-md-8">
            <div class="row-fluid">
                <!--  -->
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Novos Itens</h3>
                        </div>
                        <div class="panel-body">
                            <p>

                            <form class="form-signin" action="index.php?pg=Vnota_saida&aca=pedidosaidanew&id=<?php echo $_GET['id']; ?>" method="post">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-success " value="Salvar Pedido"/>
                                </div>
                                <div class="col-md-6">
                                    <label for="produto_id">Produto</label>
                                    <select name="produto_id" autofocus="yes" id="produto_id" class="form-control input-sm selectpicker" data-live-search="true">
                                        // vamos criar a visualização
                                        <?php
                                        foreach (fncprodutolist() as $item) {?>
                                            <option data-tokens="<?php echo $item['produto']; ?>" value="<?php echo $item['id']; ?>"><?php echo $item['produto']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="quantidade">Quantidade</label>
                                    <input name="quantidade" id="quantidade" type=number step="0.1" autocomplete="off" required="yes"  class="form-control input-sm" value="0"/>
                                </div>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-12">
                                    <label for="obs">Obs</label>
                                    <input autocomplete="off"  id="obs" type="text" class="form-control" name="obs" value=""/>
                                </div>
                                <hr>

                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <?php
            $sql = "SELECT mcu_abrigoe_pedido.id, mcu_abrigoe_produto.produto,  mcu_abrigoe_pedido.quantidade, mcu_abrigoe_pedido.data\n"
                . "FROM mcu_abrigoe_produto INNER JOIN mcu_abrigoe_pedido ON mcu_abrigoe_produto.id = mcu_abrigoe_pedido.produto_id\n"
                . "WHERE (((mcu_abrigoe_pedido.nota_id)=?) AND ((mcu_abrigoe_pedido.tipo)=2))\n"
                . "ORDER BY mcu_abrigoe_pedido.data DESC";
            global $pdo;
            $cons = $pdo->prepare($sql);
            $cons->bindParam(1, $_GET['id']);
            $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pedlista = $cons->fetchAll();
            $sql = null;
            $consulta = null;
            ?>
            <!-- tabela -->
            <table id="tabela" class="table table-bordered table-hover">
                <br>
                <thead class="bg-info">
                <tr>
                    <th>Produto</th>
                    <th>Quant.</th>
                    <th>ID</th>
                </tr>
                </thead>
                <script>
                    $(document).ready(function(){
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                </script>
                <tbody class="bg-danger">
                <?php
                // vamos criar a visualização
                foreach ($pedlista as $dados) {
                    $id = $dados["id"];
                    $produto = $dados["produto"];
                    $quantidade = $dados["quantidade"];
                    ?>

                    <tr>
                        <td><?php
                            echo $produto;
                            ?>
                        </td>
                        <td><?php echo $quantidade; ?></td>
                        <td>
                            <i class="badge pull-left">
                                <strong><?php echo $id; ?></strong>
                            </i>
                            <div class="dropdown pull-right">
                                <button class="btn btn-sm btn-danger" data-toggle="dropdown">
                                    Apagar<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class='bg-primary'>Não</a></li>
                                    <li><a href=<?php
                                        echo "\"?pg=Vnota_saida&id={$_GET['id']}&aca=excluirpedido&id_ped={$id}\"";
                                        ?>class='bg-danger'>
                                            Apagar
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>