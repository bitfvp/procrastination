<?php
class Pedido{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpedidonew($produto_id,$nota_id,$tipo,$responsavel,$obs,$quantidade){
        

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_abrigoe_pedido ";
                $sql.="(id, data, produto_id, nota_id, tipo, responsavel, obs, quantidade)";
                $sql.=" VALUES ";
                $sql.="(NULL, CURRENT_TIMESTAMP, :produto_id, :nota_id, :tipo, :responsavel, :obs, :quantidade)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":produto_id", $produto_id);
                $insere->bindValue(":nota_id", $nota_id);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":responsavel", $responsavel);
                $insere->bindValue(":obs", $obs);
                $insere->bindValue(":quantidade", $quantidade);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg={$_GET['pg']}&id={$nota_id}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpedidodelete($id_ped){
        try {
            $sql = "DELETE FROM `mcu_abrigoe_pedido` WHERE id = :id_ped ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":id_ped", $id_ped);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Atividade Excluida Com Sucesso",
            "type"=>"success",
        ];

        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg={$_GET['pg']}&id={$_GET['id']}");
        exit();
    }//fim da fnc delete




}
?>