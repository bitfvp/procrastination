<?php
class NotaEntrada{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncnotaentradanew($identificador,$data,$responsavel){
        

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_abrigoe_nota_entrada ";
                $sql.="(id, data, identificador, responsavel)";
                $sql.=" VALUES ";
                $sql.="(NULL, :data, :identificador, :responsavel)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":identificador", $identificador);
                $insere->bindValue(":responsavel", $responsavel);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                $sql = "SELECT Max(id) FROM mcu_abrigoe_nota_entrada";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];

                header("Location: index.php?pg=Vnota_entrada&id={$maid}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncnotaentradaedit($id,$identificador,$data,$responsavel){
        //inserção no banco
        try{
            $sql="UPDATE mcu_abrigoe_nota_entrada SET identificador=:identificador, data=:data, responsavel=:responsavel WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":identificador", $identificador);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":responsavel", $responsavel);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vnota_entrada&id={$id}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>