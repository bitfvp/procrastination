<?php
class Unidade{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncunidadenew( $unidade){
        
        //tratamento das variaveis
        $unidade=strtoupper($unidade);

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_abrigoe_unidade ";
                $sql.="(id, unidade)";
                $sql.=" VALUES ";
                $sql.="(NULL, :unidade)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":unidade", $unidade);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vunidade_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncunidadeedit( $id,$unidade){

        //inserção no banco
        try{
            $sql="UPDATE mcu_abrigoe_unidade SET unidade=:unidade WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":unidade", $unidade);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vunidade_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>