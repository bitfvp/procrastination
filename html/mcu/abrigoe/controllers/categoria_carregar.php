<?php
function fnccategorialist(){
    $sql = "SELECT * FROM mcu_abrigoe_categoria ORDER BY categoria";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $categorialista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $categorialista;
}

function fncgetcategoria($id){
    $sql = "SELECT * FROM mcu_abrigoe_categoria WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcategoria = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcategoria;
}
?>
