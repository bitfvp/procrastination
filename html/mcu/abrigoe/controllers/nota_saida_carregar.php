<?php
function fncnotasaidalist(){
    $sql = "SELECT * FROM mcu_abrigoe_nota_saida ORDER BY data desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $notasaidalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $notasaidalista;
}

function fncgetnotasaida($id){
    $sql = "SELECT * FROM mcu_abrigoe_nota_saida WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getnotasaida = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getnotasaida;
}