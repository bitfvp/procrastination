<?php
function fncnotaentradalist(){
    $sql = "SELECT * FROM mcu_abrigoe_nota_entrada ORDER BY data desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $notaentradalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $notaentradalista;
}

function fncgetnotaentrada($id){
    $sql = "SELECT * FROM mcu_abrigoe_nota_entrada WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getnotaentrada = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getnotaentrada;
}
?>
