<?php
function fncunidadelist(){
    $sql = "SELECT * FROM mcu_abrigoe_unidade ORDER BY unidade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $unidadelista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $unidadelista;
}

function fncgetunidade($id){
    $sql = "SELECT * FROM mcu_abrigoe_unidade WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getunidade = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getunidade;
}
?>