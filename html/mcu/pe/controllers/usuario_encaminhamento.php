<?php
function fncusuarioencaminhamentolist(){
    $sql = "SELECT * FROM tbl_users WHERE status=1 and matriz=? and (profissao=2 or profissao=8) ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
	$consulta->bindParam(1,$_SESSION['matriz']);
    $consulta->execute();
    $usuariolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $usuariolista;
}
