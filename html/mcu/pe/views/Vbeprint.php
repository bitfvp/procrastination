<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_33"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Benefícios-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<style media=all>
    .hr {
        border-color: #000;
    }
</style>
<main class="container">
    <hr class="hr mb-2">
    <h4 class="text-center"><strong>RELATÓRIO GERAL DE BENEFÍCIOS DO USUÁRIO</strong></h4>
    <h5 class="mt-4"><strong> I - INFORMAÇÕES RELATIVAS AO DADOS PESSOAIS DO USUÁRIO</strong></h5>
    <h6 class="float-left"><strong>Nome: </strong><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Sexo: </strong><?php echo fncgetsexo($pessoa['sexo'])['sexo']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Nascimento: </strong><?php echo Nascimentoverifica($pessoa['nascimento']);?>&nbsp;&nbsp;&nbsp;
        <?php echo Calculo_Idade($pessoa['nascimento'])." anos";?>&nbsp;&nbsp;&nbsp;
    </h6>
    <h6 class="float-left"><strong>CPF: </strong><?php echo mask($pessoa['cpf'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>RG: </strong><?php echo mask($pessoa['rg'],"###.###.###"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>UF: </strong><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>NIS: </strong><?php echo mask($pessoa['nis'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Pai: </strong><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Mãe: </strong><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;&nbsp;</h6>


    <h5 class="mt-4"><strong> II - ENDEREÇO DO USUÁRIO</strong></h5>
    <h6 class="float-left"><strong>Rua: </strong><?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Nº: </strong><?php echo $pessoa['numero']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Bairro: </strong><?php echo fncgetbairro($pessoa['bairro'])['bairro']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6 class="float-left"><strong>Referência: </strong><?php echo $pessoa['referencia']; ?>&nbsp;&nbsp;&nbsp;</h6>
    <h6><strong>Telefone: </strong><?php echo $pessoa['telefone']; ?>&nbsp;&nbsp;&nbsp;</h6>

    <h5 class="mt-4"><strong> III - COMPONENTES DA FAMÍLIA</strong></h5>
    <?php
    $pestemp=$pessoa['cod_familiar'];
    if(isset($pestemp) and $pestemp!=null and $pestemp!=0) {
        //existe um id e se ele é numérico
        $sql = "SELECT * FROM mcu_pessoas WHERE cod_familiar=? order by nome";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $pestemp);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $cfpessoa = $consulta->fetchall();
        $sql = null;
        $consulta = null;

        foreach ($cfpessoa as $item) {
            ?>
            <div class="row border border-dark mx-1 mb-1 px-2 pt-2 pb-0">
                <h6 class="float-left small"><strong>Nome : </strong><?php echo $item['nome']; ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>Sexo: </strong><?php echo fncgetsexo($item['sexo'])['sexo']; ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="small"><strong>Nascimento: </strong><?php echo Nascimentoverifica($item['nascimento']);?>&nbsp;&nbsp;&nbsp;
                    <?php echo Calculo_Idade($item['nascimento'])." anos";?>&nbsp;&nbsp;&nbsp;
                </h6>
                <h6 class="float-left small"><strong>CPF: </strong><?php echo mask($item['cpf'],"###.###.###-##"); ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>RG: </strong><?php echo mask($item['rg'],"###.###.###"); ?>&nbsp;&nbsp;&nbsp;</h6>
                <h6 class="float-left small"><strong>UF: </strong><?php echo $item['uf_rg']; ?>&nbsp;&nbsp;&nbsp;</h6>
            </div>
    <?php
        }
    }
    ?>

    <h5 class="mt-4"><strong> IV - HISTÓRICO DE BENEFÍCIOS RELACIONADAS AO USUÁRIO</strong></h5>
    <?php
    if (isset($_GET['id'])) {
        $at_idpessoa = $_GET['id'];
        if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
            $sql = "SELECT * \n"
                . "FROM mcu_beneficio \n"
                . "WHERE (pessoa=?)\n"
                . "ORDER BY mcu_beneficio.data_pedido DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $at_idpessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $cestas = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
        }
    }else{
        echo "erro, contate o admin";
        exit();
    }

    foreach ($cestas as $cb) {
        switch ($cb['entregue']){
            case 0:
                $status="Aguardando Para Ser Entregue";
                $statuss="primary";
                break;
            case 1:
                $status="Entregue";
                $statuss="success";
                break;
            case 2:
                $status="Dificuldade de Localização";
                $statuss="warning";
                break;
        }
        ?>
        <div class="row border border-dark mx-1 mb-1 px-2 pt-2 pb-0">
        <p class="small">
            Benefício:<strong><?php echo fncgetbetipo($cb['beneficio'])['tipo']; ?>&nbsp;&nbsp;</strong>
            Condição:<strong><?php echo fncgetbecondicao($cb['condicao'])['condicao']; ?>&nbsp;&nbsp;</strong>
            Quantidade:<strong ><?php echo $cb['quantidade']; ?>&nbsp;&nbsp;</strong>
            Data para entrega:<strong><?php echo dataRetiraHora($cb['data_pedido']); ?>&nbsp;&nbsp;</strong><br>
            Obs:<strong><?php echo $cb['descricao']; ?>&nbsp;&nbsp;</strong><br>
            Status:<strong><?php echo $status?></strong><br>
            Data da Entrega:<strong><?php echo dataBanco2data($cb['data_entrega']); ?>&nbsp;&nbsp;</strong><br>
            Quem Recebeu:<strong><?php echo $cb['quem_recebeu']; ?>&nbsp;&nbsp;</strong>
            Profissional:<strong>
                <?php
                $us = fncgetusuario($cb['profissional']);
                echo $us['nome'];
                echo " (" . fncgetprofissao($us['profissao'])['profissao'] . ")";
                ?>
            </strong>
        </p>
        </div><?php
    }?>

    <h5 class="text-center mt-4"><strong>INFORMAÇÕES RETIRADAS DO SYSSOCIAL</strong></h5>
    <h5 class="text-center"><strong>ÁS <?php echo date("H:i:s");?> DE <?php echo date("d/m/Y");?></strong></h5>
    <h5 class="text-center"><strong>PELO PROFISSIONAL: <?php echo $_SESSION['nome'];?></strong></h5>

</main>
</body>
</html>