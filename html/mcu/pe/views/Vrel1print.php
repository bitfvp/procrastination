<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_17"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_20"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatório-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

    $sql = "SELECT * from mcu_pessoas where id in (SELECT distinct pessoa FROM mcu_pb_at WHERE (((mcu_pb_at.data)>=:inicial) And ((mcu_pb_at.data)<=:final) And ((mcu_pb_at.tipo)=2)) ORDER BY mcu_pb_at.pessoa) ORDER BY bairro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoas = $consulta->fetchAll();
    $sql=null;
    $consulta=null;


?>
<div class="container-fluid">
    <h3>Pessoas Atendidas na Proteção Especial</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>

<table class="table table-sm">
    <thead class="">
    <tr>
        <th>Nome</th>
        <th>Nascimento</th>
        <th>Doc</th>
        <th>Endereço</th>
        <th>Bairro</th>
    </tr>
    <tr>
        <th colspan="5">
            <ul>
                <li class='ml-5'>Membros da familia</li>
            </ul>
        </th>
    </tr>
    </thead>


    <tbody>
    <?php
    foreach ($pessoas as $pe){
        $b=fncgetbairro($pe['bairro'])['bairro'];
        $n=dataBanco2data($pe['nascimento']);
        $i=Calculo_Idade($pe['nascimento']);
        if ($i<100 and $i>0){
            $i=$i."anos";
            $n=dataBanco2data($pe['nascimento']);
            $info=$n." ".$i;
        }else{
            $i="";
            $info="";
        }
        echo "<tr>
        <td>{$pe['nome']}</td>
        <td>{$info}</td>
        <td>CPF.{$pe['cpf']} RG.{$pe['rg']}</td>
        <td>{$pe['endereco']}/{$pe['numero']}</td>
        <td>{$b}</td>
    </tr>";

        $sql = "SELECT * from mcu_pessoas where cod_familiar=:cf and id<>:id and cod_familiar<>'' ORDER BY nome";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":cf",$pe['cod_familiar']);
        $consulta->bindValue(":id",$pe['id']);
        $consulta->execute();
        $familiar = $consulta->fetchAll();
        $sql=null;
        $consulta=null;
        ?>
        <tr>
        <td colspan='5'>
            <style>
                ul { list-style-type: circle; }
                ol { list-style-type: circle; }
                li { list-style-type: circle; }
            </style>
            <ul>
            <?php
            foreach ($familiar as $fam){
                $i=Calculo_Idade($fam['nascimento']);
                if ($i<100 and $i>0){
                    $i=$i." anos";
                }else{
                    $i="";
                }
                echo "<li class='ml-5'>{$fam['nome']} {$i}</li>";
            }?>
            </ul>
        </td>
        </tr>

   <?php }
    ?>

    </tbody>
</table>



</div>
</body>
</html>