<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_78"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="".$env->env_titulo;
$css="tela";

include_once("includes/head.php");
?>
<script>
    //funcao para o camarada apertar f11
    $(document).keydown(function (e) {
        if(e.which == 122)
        {
           // alert('Voce Apertou f11');
            //return false;
            document.getElementById('btntc1').style.display = 'none';
            document.getElementById('btntc2').style.display = 'block';
        }
    });
</script>
<?php
///se não tiver o get start nao inicia
$start=$_GET['start'];
if($start!=1){
    echo "<h1>";
    echo " <button type='button' id='btntc1' class='btn btn-block btn-lg btn-warning ' >";
    echo "Aperte F11 do teclado";
    echo "</button>";
    echo "<a id='btntc2' class='btn btn-block btn-lg btn-info' style='display: none' href='{$env->env_url_mod}?start=1'  >";
    echo "Clique aqui para começar";
    echo "</a>";
    echo "</h1>";
    exit;
}


echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}?msg={$msg}&start={$start}'>";
?>

    <div class="container-fluid">
<!--        bloco a-->

        <div class="row" id="bloco_a" >

        </div>
        <script type="text/javascript">
            $.ajaxSetup({cache: false});
            $(document).ready(function () {
                $('#bloco_a').load('includes/bloco_a.php');
            });
            $(document).ready(function () {
                setInterval(function () {
                    $('#bloco_a').load('includes/bloco_a.php')
                }, 20000);
            });
        </script>

        <div class="row">
            <div class="col-8 text-center">
                <?php
                //verifica video
                try{
                    $sql = "SELECT * FROM mcu_fila_video WHERE id=1";
                    global $pdo;
                    $consulta=$pdo->prepare($sql);
                    $consulta->execute();
                }catch ( PDOException $error_msg) {
                    echo 'Erroff' . $error_msg->getMessage();
                }
                $video=$consulta->fetch();

                if ($video['status']!=1){
                    echo "<img src='".$env->env_estatico."img/mcu.png' width='30%' alt='brasao' class='img-fluid'>";
                }else{?>
                    <div id="yt" style="height: 675px; width: 1200px;"></div>

                    <script>
                        // Load the IFrame Player API code asynchronously.
                        var tag = document.createElement('script');
                        tag.src = "https://www.youtube.com/player_api";
                        var firstScriptTag = document.getElementsByTagName('script')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                        // Replace the 'ytplayer' element with an <iframe> and
                        // YouTube player after the API code downloads.
                        var player;

                        function onYouTubePlayerAPIReady() {
                            player = new YT.Player('yt', {
                                height: '675',
                                width: '1200',
                                videoId: '<?php echo $video['video'];?>',
                                playerVars: {
                                    playlist: '<?php echo $video['video'];?>',
                                    loop: 1,
                                    controls: 0,
                                    autoplay: 1
                                },
                                events: {
                                    'onReady': onPlayerReady
                                }
                            });
                        }
                        function onPlayerReady(event) {
                            event.target.setVolume(<?php echo $video['volume'];?>);
                            // event.target.playVideo();
                        }
                    </script>

                <?php } ?>

            </div>


            <div class="col-4" id="bloco_b"></div>
            <script type="text/javascript">
                $.ajaxSetup({cache: false});
                $(document).ready(function () {
                    $('#bloco_b').load('includes/bloco_b.php');
                });
                $(document).ready(function () {
                    setInterval(function () {
                        $('#bloco_b').load('includes/bloco_b.php')
                    }, 20000);
                });
            </script>
        </div>
        <div class="row">
            <div class="ocean">
                <div class="wave"></div>
                <div class="wave"></div>
            </div>
        </div>
    </div>

</body>
</html>
