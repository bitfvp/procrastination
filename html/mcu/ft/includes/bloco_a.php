<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

///////////////////////////////////////////////////////////////////

try{
    $sql = "SELECT numero, guiche, chamado FROM mcu_fila_senha WHERE id=1";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$queryy=$consulta->fetch();
$senha=$queryy['numero'];
$guiche=$queryy['guiche'];
$chamado=$queryy['chamado'];


try{
    $sql = "SELECT mcu_fila_pessoa.nome FROM mcu_fila_pessoa WHERE mcu_fila_pessoa.id=?";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->bindParam(1, $senha);
    $consulta->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$proximo=$consulta->fetch();


//atualiza a tabela senha como chamada
try{
    $sql = "UPDATE mcu_fila_senha SET mcu_fila_senha.chamado = 1 WHERE mcu_fila_senha.id= 1";
    global $pdo;
    $altera=$pdo->prepare($sql);
    $altera->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}

//atualiza a tabela senha como chamada
try{
    $sql = "UPDATE mcu_fila_pessoa SET mcu_fila_pessoa.chamado = 1 WHERE mcu_fila_pessoa.id= ?";
    global $pdo;
    $altera=$pdo->prepare($sql);
    $altera->bindParam(1, $senha);
    $altera->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}

//retorna $senha, se foi chamado $chamado, $lista com os 5 ultimos nomes, é $proximo);
//retorna $senha, se foi chamado $chamado, $lista com os 5 ultimos nomes, é array $proximo

$ef=mt_rand(1,16);
//$ef=17;
switch ($ef){
    case 1:
        $animacao="dance ";
        break;
    case 2:
        $animacao="jamp";
        break;
    case 3:
        $animacao="enter-up-bounce";
        break;
    case 4:
        $animacao="enter-down-bounce";
        break;
    case 5:
        $animacao="enter-left-bounce";
        break;
    case 6:
        $animacao="enter-right-bounce";
        break;
    case 7:
        $animacao="scale-bounce";
        break;
    case 8:
        $animacao="jump-bounce";
        break;
    case 9:
        $animacao="flip-left-bounce";
        break;
    case 10:
        $animacao="flip-right-bounce";
        break;
    case 11:
        $animacao="rotate-flip";
        break;
    case 12:
        $animacao="flip-left";
        break;
    case 13:
        $animacao="flip-right";
        break;
    case 14:
        $animacao="flip-top";
        break;
    case 15:
        $animacao="flip-bottom";
        break;
    case 16:
        $animacao="rotate-flip-down";
        break;
    case 17:
        $animacao="rotate-row";
        break;
        //
    case 18:
        $animacao="journal";
        break;
    //
    case 19:
        $animacao="four-rock";
        break;
    //
    default:
        $animacao= "dance";
        break;
}
//retorna $animacao

?>

<div class="col-12 text-center mb-5">
    <div class="flip-top mt-2">
        <h2 class="display-4 text-monospace">Próximo</h2>
    </div>
    <style type="text/css">
        #enois {
            font-family: Helvetica,Helvetica Neue,Arial,sans-serif;
            font-weight: bold;
        }
    </style>
    <div class="pulse">
        <div class="<?php echo $animacao; ?>">
            <?php
            switch ($guiche){
                case 1:
                    $gguiche = "Guichê 1";
                    break;
                case 2:
                    $gguiche = "Guichê 2";
                    break;
                case 3:
                    $gguiche = "Guichê 3";
                    break;
                case 4:
                    $gguiche = "Guichê 4";
                    break;
                case 5:
                    $gguiche = "Guichê 5";
                    break;
                case 6:
                    $gguiche = "Guichê 6";
                    break;
                case 7:
                    $gguiche = "Guichê 7";
                    break;
                case 8:
                    $gguiche = "Guichê 8";
                    break;
                case 9:
                    $gguiche = "Guichê 9";
                    break;
                case 10:
                    $gguiche = "Guichê Especial";
                    break;
                case 11:
                    $gguiche = "Sala 1";
                    break;
                case 12:
                    $gguiche = "Sala 2";
                    break;
                case 13:
                    $gguiche = "Sala 3";
                    break;
                case 14:
                    $gguiche = "Sala 4";
                    break;
                case 15:
                    $gguiche = "Sala 5";
                    break;
                case 50:
                    $gguiche = "At. Expresso";
                    break;
                case 99:
                    $gguiche = "Recepção";
                    break;
                default:
                    $gguiche = "";
                    break;
            }
            ?>

            <h1 class="display-1 mt-0 text-dark text-monospace text-uppercase" id="enois"> <span class="badge badge-warning"><?php echo $gguiche;?></span> <?php echo $proximo['nome'];?></h1>

        </div>
    </div>
</div>

<?php
//if ($guiche==50){
//    $guiche="Especial";
//}else{
//    if ($guiche==50){
//        $guiche="Refis";
//    }else{
//
//    }
//
//}

if (($guiche >= 1) and ($guiche <= 9)){
    $guichee="número ".$guiche;
    $idfala=mt_rand(1,7);
    switch ($idfala){
        case 1:
            $fala="Atenção, atenção, próximo, {$proximo['nome']} no Guichê {$guichee}, Repetindo, próximo, {$proximo['nome']} no Guichê {$guichee}";
            break;
        case 2:
            $fala="Aguardando {$proximo['nome']}, Guichê {$guichee},  Repetindo, Aguardando  {$proximo['nome']},no Guichê {$guichee}";
            break;
        case 3:
            $fala="Atenção a todos, próximo, {$proximo['nome']}, no Guichê {$guichee}, próximo, {$proximo['nome']}, no Guichê {$guichee}";
            break;
        case 4:
            $fala="chamando {$proximo['nome']} , Guichê {$guichee}, Repetindo, {$proximo['nome']}";
            break;
        case 5:
            $fala="{$proximo['nome']}, dirija-se ao atendimento no Guichê {$guichee}, Repetindo, {$proximo['nome']}, dirija-se ao atendimento no Guichê {$guichee}";
            break;
        case 6:
            $fala="atenção, favor silêncio,; Próximo {$proximo['nome']}, dirija-se ao atendimento no Guichê {$guichee}, Repetindo, {$proximo['nome']}, dirija-se ao atendimento no Guichê {$guichee}";
            break;
        case 7:
            $fala="{$proximo['nome']}, Venha para o atendimento no Guichê {$guichee}, Repetindo, {$proximo['nome']}, Venha para o atendimento no Guichê {$guichee}";
            break;
        default:
            echo "dance";
            break;
    }
}
/////////////////////////////////////
if ($guiche==10){//griche especial
    $fala="Atenção a todos, próximo, {$proximo['nome']}, direto na sala de atendimento especial, ,  próximo, {$proximo['nome']}, direto na sala de atendimento especial";
}
/////////////////////////////////////
if (($guiche >= 11) and ($guiche <= 15)){
    switch ($guiche){
        case 11:
            $guichee="sala número 1";
            break;
        case 12:
            $guichee="sala número 2";
            break;
        case 13:
            $guichee="sala número 3";
            break;
        case 14:
            $guichee="sala número 4";
            break;
        case 15:
            $guichee="sala número 5";
            break;
        default:
            $guichee="sala número x";
            break;
    }
    $idfala=mt_rand(1,7);
    switch ($idfala){
        case 1:
            $fala="Atenção, atenção, próximo, {$proximo['nome']} na {$guichee}, Repetindo, próximo, {$proximo['nome']} na {$guichee}";
            break;
        case 2:
            $fala="Aguardando {$proximo['nome']}, {$guichee},  Repetindo, Aguardando  {$proximo['nome']},na {$guichee}";
            break;
        case 3:
            $fala="Atenção a todos, próximo, {$proximo['nome']}, na {$guichee}, próximo, {$proximo['nome']}, na {$guichee}";
            break;
        case 4:
            $fala="chamando {$proximo['nome']} , {$guichee}, Repetindo, {$proximo['nome']}";
            break;
        case 5:
            $fala="{$proximo['nome']}, dirija-se ao atendimento na {$guichee}, Repetindo, {$proximo['nome']}, dirija-se ao atendimento na {$guichee}";
            break;
        case 6:
            $fala="atenção, favor silêncio,; Próximo {$proximo['nome']}, dirija-se ao atendimento na {$guichee}, Repetindo, {$proximo['nome']}, dirija-se ao atendimento na {$guichee}";
            break;
        case 7:
            $fala="{$proximo['nome']}, Venha para o atendimento na {$guichee}, Repetindo, {$proximo['nome']}, Venha para o atendimento na {$guichee}";
            break;
        default:
            echo "dance";
            break;
    }
}
/////////////////////////////////////
if ($guiche==50){//Atendimento Rápido
    $fala="Atenção a todos, próximo, {$proximo['nome']}, direto no Atendimento Expresso, ,  próximo, {$proximo['nome']}, no Atendimento Expresso";
}
/////////////////////////////////////
if ($guiche==99){//recepcao
    $fala="chamando {$proximo['nome']} , direto na recepção, Repetindo, {$proximo['nome']}";
}


//retorna $fala


?>
<button class="d-none" onclick="responsiveVoice.speak('<?php echo $fala;?>', 'Brazilian Portuguese Female', {volume: 1}, {pitch: 1}, {rate: 1.5});" type="button" value="play" id="play" name="play">Play</button>

    <script type="text/javascript">
        function ClicClic(nome) {
            document.getElementById(nome).click();
        }
    </script>
<?php
//se nao chamado $chamado vai chamar
if ($chamado!=1){
    ////////sons distintos

    if ($guiche==99){
        ?>
        <script type='text/javascript'>
            new Audio('../../public/audio/atencao.wav').play();
            window.setTimeout('ClicClic("play")', 1000);
        </script>
        <?php
    }else{
        ?>
        <script type='text/javascript'>
            new Audio('../../public/audio/alerta2.wav').play();
            window.setTimeout('ClicClic("play")', 1000);
        </script>
        <?php
    }
}
?>