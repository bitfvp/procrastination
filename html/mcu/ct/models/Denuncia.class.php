<?php
class Denuncia{
    public function fncdenuncianew($pessoa,$prof,$nivel,$descricao){
        //tratamento das variaveis


            //inserção no banco
            try{
                $sql="INSERT INTO mcu_ct_denuncia ";
                $sql.="(id, pessoa, prof_cadastro, data, nivel, descricao)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :prof_cadastro, CURRENT_TIMESTAMP, :nivel, :descricao)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $pessoa);
                $insere->bindValue(":prof_cadastro", $prof);
                $insere->bindValue(":nivel", $nivel);
                $insere->bindValue(":descricao", $descricao);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($insere)){
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($pessoa,$_SESSION['id'],"Nova denuncia",8,1);
            ////////////////////////////////////////////////////////////////////////////

            //+++++++++++++++++++++++++++++++++++++++++++++++++

            ////+++++++++++++++++++++++++++++++++++++++++++++++++
            $_SESSION['fsh']=[
                "flash"=>"Denúncia Cadastrada Com Sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: index.php?pg=Vd&id={$pessoa}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }






    public function fncdenunciaedit($id,$profissional,$relatorio){
        //tratamento das variaveis


            //inserção no banco
            try{
                $sql="UPDATE mcu_ct_denuncia SET prof_acomp=:profissional, acompanhado=1, data_acomp=CURRENT_TIMESTAMP, relatorio=:relatorio WHERE mcu_ct_denuncia.id=:id";

                global $pdo;
                $atualiza=$pdo->prepare($sql);
                $atualiza->bindValue(":profissional", $profissional);
                $atualiza->bindValue(":relatorio", $relatorio);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($atualiza)){

            $_SESSION['fsh']=[
                "flash"=>"Atualizado com Sucesso!!",
                "type"=>"success",
            ];

            try{
                $sql = "SELECT `pessoa` FROM `mcu_ct_denuncia` WHERE `id`=?";
                global $pdo;
                $selec=$pdo->prepare($sql);
                $selec->bindParam(1, $id);
                $selec->execute(); global $LQ; $LQ->fnclogquery($sql);
                $id_pess = $selec->fetch();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($id_pess['0'],$_SESSION['id'],"edicao de denuncia",8,3);
            ////////////////////////////////////////////////////////////////////////////

            header("Location: index.php?pg=Vd_hist&id={$id_pess['0']}&id_den={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }




}