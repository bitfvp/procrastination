<?php
class Atividade
{
    public function fncatividadenew($at_pessoa,$at_denuncia,$at_profissional,$at_data,$at_restricao,$at_atividade,$at_descricao,$at_tipo,$Upin)
    {
        global $W_O_R_D_S;
        $W_O_R_D_S->fncwords($at_descricao);

        try {
            $sql = "INSERT INTO mcu_pb_at ";
            $sql .= "(id, pessoa, denuncia, data, profissional, restricao, atividade, descricao, tipo)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, :denuncia, :data, :profissional, :restricao, :atividade, :descricao, :tipo)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $at_pessoa);
            $insere->bindValue(":denuncia", $at_denuncia);
            $insere->bindValue(":data", $at_data);
            $insere->bindValue(":profissional", $at_profissional);
            $insere->bindValue(":restricao", $at_restricao);
            $insere->bindValue(":atividade", $at_atividade);
            $insere->bindValue(":descricao", $at_descricao);
            $insere->bindValue(":tipo", $at_tipo);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($insere)) {
            //foto
            $sql = "SELECT Max(id) FROM mcu_pb_at";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mxv = $consulta->fetch();
            $sql = null;
            $consulta = null;
            $maxv = $mxv[0];
            // verifica se foi enviado um arquivo
            $fillle=$_FILES['arquivo']['name'];
            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
                if (is_dir("../../dados/mcu/protecaobasica/atividades/" . $maxv . '/')) {} else {mkdir("../../dados/mcu/protecaobasica/atividades/" . $maxv . '/');}
                // verifica se foi enviado um arquivo
                $Upin->get(
                    '../../dados/mcu/protecaobasica/atividades/'.$maxv.'/', //Pasta de uploads (previamente criada)
                    $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
                    10, //Tamanho máximo
                    "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
                    "arquivo", //Atributo name do input file
                    1 //Mudar o nome? 1 = sim, 0 = não
                );
                $Upin->run();
            }
            //////////////////////////

            $types = array( 'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf', 'txt', 'xls', 'xlsx' );
            if(is_dir("../../dados/mcu/protecaobasica/atividades/".$maxv."/")){
//        echo "A Pasta Existe";
                if ( $handle = opendir("../../dados/mcu/protecaobasica/atividades/".$maxv."/") ) {
                    while ( $entry = readdir( $handle ) ) {
                        $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                        if( in_array( $ext, $types ) ){
                            $arquivoo = explode(".", $entry);//$entry é o arquivo
                            $extencao = end($arquivoo);

                            echo $arquivoo[0]."=====".$extencao;

                            $sql = "SELECT COUNT(`id`) FROM `mcu_pb_dados` where atividade='{$maxv}' and arquivo='{$arquivoo[0]}'";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute();
                            $total = $consulta->fetch();//$total[0]
                            $sql = null;
                            $consulta = null;
                            if($total[0]==0){
                                /////////////////////////////Fazer a inclusao
                                $sql = "INSERT INTO `mcu_pb_dados` (`id`, `data_cadastro`, `status`,  `atividade`, `arquivo`, `extensao`)"
                                    ." VALUES "
                                    ."(NULL, CURRENT_TIMESTAMP, '1',     '{$maxv}',       '{$arquivoo[0]}',        '{$extencao}');";
                                global $pdo;
                                $update = $pdo->prepare($sql);
                                $update->execute();
                                $sql = null;
                                $update = null;
                            }else {//se for diferente é por que já esta cadastrado
                                echo "Já existe na base";
                            }

                        }
                    }
                    closedir($handle);
                }
            } else {
                echo "<i class='text-muted'>A Pasta não Existe</i>";
            }

            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($at_pessoa,$_SESSION['id'],"Nova atividade",7,1);
            //////////////////////////////////////////////////////////////////////////// cras 2 creas 5
            //começa alteração atendido pelo cras
            //
            //obter codigo familiar
            try{
                $sql = "SELECT cod_familiar FROM mcu_pessoas WHERE id=?";

                global $pdo;
                $queryy=$pdo->prepare($sql);
                $queryy->bindParam(1, $at_pessoa);
                $queryy->execute(); global $LQ; $LQ->fnclogquery($sql);
                $cod_familiar=$queryy->fetch();
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }

            //testar se já tem um codigo familiar valido
            if(isset($cod_familiar['cod_familiar']) and $cod_familiar['cod_familiar']!=null and $cod_familiar['cod_familiar']!=0) {
                $cod_f=$cod_familiar['cod_familiar'];
            }else{
                //se não é valido criar novo codigo familiar
                $token = uniqid("");
                try {
                    $sql = "UPDATE mcu_pessoas SET ";
                    $sql .= "cod_familiar=:cod_familiar";
                    $sql .= " WHERE id=:id";

                    global $pdo;
                    $atuali = $pdo->prepare($sql);
                    $atuali->bindValue(":cod_familiar", $token);
                    $atuali->bindValue(":id", $at_pessoa);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }
            }

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];

            if ($at_denuncia!=0){
                header("Location: ?pg=Vd_hist&id={$at_pessoa}&id_den={$at_denuncia}");
                exit();
            }else{
                header("Location: ?pg=Vat&id={$at_pessoa}");
                exit();
            }

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new







    public function fnc_at_desativa($at_id,$prof,$pessoa_id){
        try {
            $sql = "UPDATE `mcu_pb_at` SET `status` = '0', `delete_prof` =:prof_id  WHERE id = :at_id ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof_id", $prof);
            $exclui->bindValue(":at_id", $at_id);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        try {
            $sql = "UPDATE `mcu_pb_dados` SET `status` = '0' WHERE atividade = :at_id ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":at_id", $at_id);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Atividade Desativada Com Sucesso",
            "type"=>"success",
        ];

        //reservado para log
        global $LL; $LL->fnclog($pessoa_id,$_SESSION['id'],"Desativar atividade",2,4);
        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg=Vat&id={$pessoa_id}");
        exit();
    }//fim da fnc delete


}//fim da classe
