<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_25"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_28"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Denuncia-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
?>
<main class="container">
    <style media=all>
        .table-sm {
            font-size:10px !important;
            widows: 2;
            width: 100%;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
        #stdot {
            background-color: #fff;
            border-top: 5px dashed #8c8b8b;
        }
        #stcurva1 {
            height: 30px;
            border-style: solid;
            border-color: #8c8b8b;
            border-width: 1px 0 0 0;
            border-radius: 20px;
            margin-bottom: 0;
        }
        #stcurva2 {
            display: block;
            content: "";
            height: 30px;
            margin-top: -31px;
            border-style: solid;
            border-color: #8c8b8b;
            border-width: 0 0 1px 0;
            border-radius: 20px;
            margin-top: 0;
        }
    </style>

        <header>
            <h2>
            Nome:<strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
            </h2>
        </header>
        <p>
        <h4>
            Sexo:
            <?php
            if($pessoa['sexo']!="" and $pessoa['sexo']!="0") {
                echo "<span class='azul '>";
                if($pessoa['sexo']==0){echo"Selecione...";}
                if($pessoa['sexo']==1){echo"Feminino";}
                if($pessoa['sexo']==2){echo"Masculino";}
                if($pessoa['sexo']==3){echo"Indefinido";}
                echo "</span>";
            }else{
                echo "<span class='vermelho underline'>";
                echo "???????";
                echo "</span>";
            }
            ?>

            Nis:
            <strong class="text-info"><?php
                if($pessoa['nis']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['nis'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????";
                    echo "</span>";
                }
                ?></strong>



            CPF:
            <strong class="text-info"><?php
                if($pessoa['cpf']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['cpf'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????";
                    echo "</span>";
                }
                ?></strong>


            RG:
            <strong class="text-info"><?php
                if($pessoa['rg']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['rg'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????";
                    echo "</span>";
                }
                ?></strong>


            UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong>
            Nascimento:
            <strong class="text-info"><?php
                if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="") {
                    echo "<span class='azul '>";
                    echo dataBanco2data ($pessoa['nascimento']);
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????";
                    echo "</span>";
                }
                ?></strong>


            Endereço:
            <strong class="text-info"><?php
                if($pessoa['endereco']!=""){
                    echo "<span class='azul'>";
                    echo $pessoa['endereco'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "??????????????";
                    echo "</span>";
                }
                ?></strong>&nbsp;&nbsp;


            Nº:
            <strong class="text-info"><?php
                if($pessoa['numero']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['numero'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "??";
                    echo "</span>";
                }
                ?></strong>&nbsp;&nbsp;


            Bairro:
            <strong class="text-info"><?php
                if ($pessoa['bairro'] != "0") {
                    include_once("{$env->env_root}controllers/mcu/bairrolista.php");
                    $cadbairro=fncgetbairro($pessoa['bairro']);
                    echo $cadbairro['bairro'];
                } else {
                    echo "<span class='text-warning'>?????????</span>";
                }
                ?></strong>&nbsp;&nbsp;


            Referência:
            <strong class="text-info"><?php
                if($pessoa['referencia']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['referencia'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????????";
                    echo "</span>";
                }
                ?></strong>&nbsp;


            Telefone:
            <strong class="text-info"><?php
                if($pessoa['telefone']!="") {
                    echo "<span class='azul '>";
                    echo $pessoa['telefone'];
                    echo "</span>";
                }else{
                    echo "<span class='vermelho underline'>";
                    echo "???????";
                    echo "</span>";
                }
                ?></strong>


            Mãe:
            <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;
            </strong>

            Pai:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>
            </h4>
        </p>
<?php
if (isset($_GET['id_den'])) {
    //existe um id e se ele é numérico
        // Captura os dados do cliente solicitado
        $sql = "SELECT * FROM `mcu_ct_denuncia` WHERE `id`=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id_den']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $denn = $consulta->fetch();
        $sql = null;
        $consulta = null;
}
?>
    <hr id="stcurva1">
<h4>
        <p>
            <i class="fa fa-quote-left fa-sm "></i>
            <strong class="text-success"><?php echo $denn['descricao']; ?></strong>
            <i class="fa fa-quote-right fa-sm"></i>
        </p>
        Responsavel pelo cadastro:
        <strong class="text-info">
            <?php
            if($denn['prof_cadastro']!="0"){
                include_once("{$env->env_root}controllers/profissionallista.php");
                $cadprof=fncgetprofissional($denn['prof_cadastro']);
                echo $cadprof['nome'];
            }
            ?>
        </strong>
        <br>
        Data:
        <strong class="text-info">
            <?php echo datahoraBanco2data($denn['data']); ?>&nbsp;&nbsp;
        </strong>
        <br>
        Nivel:
        <strong class="text-info"><?php if($denn['nivel']==0){echo "Normal";} if($denn['nivel']==1){echo "Urgente";} ?>
        </strong>

        Acompanhado:
        <strong class="text-info">
            <?php if($denn['acompanhado']==0){echo "Não";} if($denn['acompanhado']==1){echo "Sim";} ?>
        </strong>
        <br>
        Conselheiro:
        <strong class="text-info">
            <?php
            if($denn['profissional']!="0" and $denn['profissional']!=null){
                include_once("{$env->env_root}controllers/profissionallista.php");
                $cadprof=fncgetprofissional($denn['profissional']);
                echo $cadprof['nome'];
            }else{
                echo "Aguardando Acompanhamento";
            }
            ?>
        </strong>
        <br>
        Data:
        <strong class="text-info">
            <?php
            if($denn['data_acomp']!="0" and $denn['data_acomp']!=null){
                echo datahoraBanco2data($denn['data_acomp']);
            }else{
                echo "Aguardando Acompanhamento";
            }
            ?>
        </strong>
        <br>
        <p>
            <i class="fa fa-quote-left fa-sm "></i>
            <strong class="text-success"><?php echo $denn['relatorio']; ?></strong>
            <i class="fa fa-quote-right fa-sm"></i>
        </p>
    <br>
    <span class="badge quote-badge text-warning"><strong>Codigo:<?php echo $denn['id']; ?></strong></span>
</h4>
    <hr id="stcurva2">
<?php
// Recebe
if (isset($_GET['id'])) {
    $at_idpessoa = $_GET['id'];
    $at_den = $_GET['id_den'];
    //existe um id e se ele é numérico
        // Captura os dados do cliente solicitado
        $sql = "SELECT mcu_ct_at.id, mcu_ct_at.pessoa, mcu_pessoas.nome, mcu_ct_at.profissional AS codprof, tbl_users.nome AS profissional, mcu_ct_at.data, mcu_ct_atlista.historico, mcu_ct_at.descricao\n"
            . "FROM tbl_users INNER JOIN (mcu_ct_atlista INNER JOIN (mcu_pessoas INNER JOIN mcu_ct_at ON mcu_pessoas.id = mcu_ct_at.pessoa) ON mcu_ct_atlista.id = mcu_ct_at.historico) ON tbl_users.id = mcu_ct_at.profissional\n"
            . "WHERE ( ((mcu_ct_at.pessoa)=?) and ((mcu_ct_at.denuncia)=?))\n"
            . "ORDER BY mcu_ct_at.data DESC, mcu_ct_at.id DESC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $at_idpessoa);
    $consulta->bindParam(2, $at_den);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $ativi = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

}
//////////////////////////////////////////////////////////////////////////////

$ccc=0;
foreach ($ativi as $at) {
    if ($ccc!=0){
        echo "<hr>";
    }
        $ccc++;
    ?>
    <h4>

                                <p>
                                <i class="fa fa-quote-left fa-sm "></i> <strong class="text-success"><?php echo $at['descricao']; ?></strong>
                        <i class="fa fa-quote-right fa-sm"></i>
                    </p>
                                <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>
                                    &nbsp;&nbsp;</strong>
                                Tipo de atividade:<strong class="text-info"><?php echo $at['historico']; ?>&nbsp&nbsp</strong>
                                <br>
                                <span class="badge quote-badge pull-right text-warning"><strong><?php echo $at['id']; ?></strong></span>
                                <footer>
                                    <?php
                                    echo $at['profissional'];


                                        ?>
                                </footer>
    </h4>
                            <?php
                            if ($at['id'] != 0) {
                                $files = glob("../../dados/mcu/ct/atividades/" . $at['id'] . "/*.*");
                                for ($i = 0; $i < count($files); $i++) {
                                    $num = $files[$i];
                                    $extencao = explode(".", $num);
                                    //ultima posicao do array
                                    $ultimo = end($extencao);
                                    switch ($ultimo) {
                                        case "docx":
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $env->env_estatico . "img/docx.png alt=\"...\">";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "doc":
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $env->env_estatico . "img/doc.png alt=\"...\">";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xls":
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt=\"...\">";
                                            echo "</a>";

                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xlsx":
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt=\"...\">";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "pdf":
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $env->env_estatico . "img/pdf.png alt=\"...\">";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        default:
                                            echo "<div class=\"row\">";
                                            echo "<div class=\"col-md-10\">";
                                            echo "<a href=" . $num . " target=\"_blank\" class=\"thumbnail\">";
                                            echo "<img src=" . $num . " alt=\"...\" style=\"max-width:260px;\">" ;
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;
                                    }

                                }
                            }//fim de foto
                        }
                        ?>
        
</main>
</body>
</html>