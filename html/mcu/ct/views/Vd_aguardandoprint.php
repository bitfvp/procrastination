<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_26"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Aguardando-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");

// Captura os dados do cliente solicitado
$sql = "SELECT * FROM `mcu_ct_denuncia` WHERE `acompanhado`=0 ORDER BY mcu_ct_denuncia.data";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$hist = $consulta->fetchAll();
$sql=null;
$consulta=null;
?>
<style media=all>
    .hr {
        border-color: #000;
    }
</style>
<main class="container">
    <hr class="hr mb-2 mt-4">
    <h4 class="text-center"><strong>DENÚNCIA AGUARDANDO</strong></h4>
    <h5 class="mt-4"><strong> I - DENÚNCIAS</strong></h5>
        <?php
        foreach ($hist as $at){
            $userrr=fncgetpessoa($at['pessoa']);
            if ($userrr['numero']==0){$numero="S/N";}else{$numero=$userrr['numero'];}
            $bairro=fncgetbairro($userrr['bairro'])['bairro'];
            ?>
                <div class="row border border-dark mx-1 mb-1 px-2 pt-2 pb-0">
                    <blockquote class="border-0">
                    <strong><?php echo $userrr['nome'];?></strong><br>
                    <strong class="float-left">Rua: <?php echo $userrr['endereco']." ".$numero;?></strong>&nbsp;
                    <strong>Bairro: <?php echo $bairro;?></strong>&nbsp;
                    <strong>Ref.:<?php echo $userrr['referencia'];?></strong>

                    <p>
                        <i class="fa fa-quote-left fa-sm "></i>
                        <strong><?php echo $at['descricao']; ?></strong>
                        <i class="fa fa-quote-right fa-sm"></i>
                    </p>
                    Data:
                    <strong>
                        <?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;
                    </strong>
                    <br>
                    <footer class="blockquote-footer">
                        Responsável pelo cadastro:
                        <strong>
                            <?php
                            if($at['prof_cadastro']!="0"){
                                $us=fncgetusuario($at['prof_cadastro']);
                                echo $us['nome'];
                                echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                            }
                            ?>
                        </strong>
                    </footer>
                    </blockquote>
                </div>
        <?php
        }
        ?>

    <h5 class="text-center mt-4"><strong>INFORMAÇÕES RETIRADAS DO SYSSOCIAL</strong></h5>
    <h5 class="text-center"><strong>ÀS <?php echo date("H:i:s");?> DE <?php echo date("d/m/Y");?></strong></h5>
    <h5 class="text-center"><strong>PELO PROFISSIONAL: <?php echo $_SESSION['nome'];?></strong></h5>

</main>
</body>
</html>




