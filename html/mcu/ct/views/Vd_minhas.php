<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_25"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            //
        }
    }
}


$page="Casos-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

<div class="row">
    <div class="col-md-12">
<!-- =============================começa conteudo======================================= -->
	<?php
    // Recebe
        $id_user =$_SESSION['id'];
        //existe um id e se ele é numérico
        if (!empty($id_user) && is_numeric($id_user)) {
            // Captura os dados do cliente solicitado
            $sql = "SELECT * FROM `mcu_ct_denuncia` WHERE `prof_acomp`=? ORDER BY mcu_ct_denuncia.data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $id_user);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $hist = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
        }
    ?>
        <div class="card mt-2">
            <div id="pointofview" class="card-header bg-info text-light">
                Denúncias Aguardando Averiguação
            </div>
            <div class="card-body">
                <h6>
                    <?php
                    foreach ($hist as $at){
                        $userrr=fncgetpessoa($at['pessoa']);
                        if ($userrr['numero']==0){$numero="S/N";}else{$numero=$userrr['numero'];}
                        $bairro=fncgetbairro($userrr['bairro'])['bairro'];
                        ?>
                        <hr>
                        <blockquote class="blockquote blockquote-info">

                            <h3><?php echo $userrr['nome'];?></h3>
                            <h6><?php echo $userrr['endereco']." ".$numero;?></h6>
                            <h6><?php echo $bairro;?></h6>
                            <h6><?php echo $userrr['referencia'];?></h6>

                            <p>
                                <i class="fa fa-quote-left fa-sm "></i>
                                <strong class="text-success"><?php echo $at['descricao']; ?></strong>
                                <i class="fa fa-quote-right fa-sm"></i>
                            </p>
                            Data:
                            <strong class="text-info">
                                <?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;
                            </strong>
                            <br>
                            Nivel:
                            <strong class="text-info"><?php if($at['nivel']==0){echo "Normal";} if($at['nivel']==1){echo "Urgente";} ?></strong>
                            <footer class="blockquote-footer">
                                Responsável pelo cadastro:
                                <strong class="text-info">
                                    <?php
                                    if($at['prof_cadastro']!="0"){
                                        $us=fncgetusuario($at['prof_cadastro']);
                                        echo $us['nome'];
                                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    }
                                    ?>
                                </strong>
                            </footer>
                        </blockquote>


                        <?php if($at['acompanhado']==0){ ?>
                            <div class="dropdown show">
                                <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Assumir acompanhamento
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Não</a>
                                    <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vd_acomp&id={$at['pessoa']}&id_den={$at['id']}";?>">Acompanhar</a>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <a href="?pg=Vd_hist&id=<?php echo $at['pessoa'];?>&id_den=<?php echo $at['id']?>" class="btn btn-sm btn-success btn-block">ACESSAR</a>
                            <?php
                        } ?>

                        <br>
                        <blockquote class="blockquote blockquote-muted blockquote-inverse">
                            Acompanhado:
                            <strong class="text-info">
                                <?php if($at['acompanhado']==0){echo "Não";} if($at['acompanhado']==1){echo "Sim";} ?>
                            </strong>
                            <br>
                            Data:
                            <strong class="text-info">
                                <?php
                                if($at['data_acomp']!="0" and $at['data_acomp']!=null){
                                    echo datahoraBanco2data($at['data_acomp']);
                                }else{
                                    echo "Aguardando Acompanhamento";
                                }
                                ?>
                            </strong>
                            <br>
                            <p>
                                <i class="fa fa-quote-left fa-sm "></i>
                                <strong class="text-success"><?php echo $at['relatorio']; ?></strong>
                                <i class="fa fa-quote-right fa-sm"></i>
                            </p>

                            <footer class="blockquote-footer">
                                <strong class="text-info">
                                    <?php
                                    if($at['prof_acomp']!=""){
                                        $us=fncgetusuario($at['prof_acomp']);
                                        echo $us['nome'];
                                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    }else{
                                        echo "Aguardando Acompanhamento";
                                    }
                                    ?>
                                </strong>
                            </footer>

                        </blockquote>
                        <?php
                    }
                    ?>
                </h6>
            </div>
        </div>

<!-- =============================fim conteudo======================================= -->       
    </div>  
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>