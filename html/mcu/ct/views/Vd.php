<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_25"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_28"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Denúncia-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamento de denúncias
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vd&aca=newdenuncia&id=<?php echo $_GET['id']; ?>" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="6" name="descricao"></textarea>
                                <span id="cont">3000</span>/3000
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

<?php
// Recebe
if(isset($_GET['id'])) {
    $at_idpessoa =$_GET['id'];
    //existe um id e se ele é numérico
    if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
    // Captura os dados do cliente solicitado
        $sql = "SELECT * FROM `mcu_ct_denuncia` WHERE `pessoa`=? ORDER BY mcu_ct_denuncia.data DESC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $at_idpessoa);
    $consulta->execute();
    //global $LQ;
    //$LQ->fnclogquery($sql);
    $hist = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    }
}
?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico de denúncias
                </div>
                <div class="card-body">
                    <a href="#" target=""><span class="fa fa-print" aria-hidden="true"></ span></a>
                    <h6>
                        <?php
                        foreach ($hist as $at){?>
                            <hr>
                            <blockquote class="blockquote blockquote-info">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i>
                                        <strong class="text-success"><?php echo $at['descricao']; ?></strong>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                Data:
                                <strong class="text-info">
                                    <?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;
                                </strong>
                                <br>
                                Nivel:
                                <strong class="text-info"><?php if($at['nivel']==0){echo "Normal";} if($at['nivel']==1){echo "Urgente";} ?></strong>
                                <footer class="blockquote-footer">
                                    Responsável pelo cadastro:
                                    <strong class="text-info">
                                        <?php
                                        if($at['prof_cadastro']!="0"){
                                            $us=fncgetusuario($at['prof_cadastro']);
                                            echo $us['nome'];
                                            echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                        }
                                        ?>
                                    </strong>
                                </footer>
                            </blockquote>


                            <?php if($at['acompanhado']==0){ ?>
                                <div class="dropdown show">
                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Assumir acompanhamento
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Não</a>
                                        <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vd_acomp&id={$_GET['id']}&id_den={$at['id']}";?>">Acompanhar</a>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <a href="?pg=Vd_hist&id=<?php echo $_GET['id']?>&id_den=<?php echo $at['id']?>" class="btn btn-sm btn-success btn-block">ACESSAR</a>
                            <?php } ?>

                            <br>
                            <blockquote class="blockquote blockquote-muted blockquote-inverse">
                                Acompanhado:
                                <strong class="text-info">
                                    <?php if($at['acompanhado']==0){echo "Não";} if($at['acompanhado']==1){echo "Sim";} ?>
                                </strong>
                                <br>
                                Data:
                                <strong class="text-info">
                                    <?php
                                    if($at['data_acomp']!="0" and $at['data_acomp']!=null){
                                        echo datahoraBanco2data($at['data_acomp']);
                                    }else{
                                        echo "Aguardando Acompanhamento";
                                    }
                                    ?>
                                </strong>
                                <br>
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i>
                                    <strong class="text-success"><?php echo $at['relatorio']; ?></strong>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>

                                <footer class="blockquote-footer">
                                    <strong class="text-info">
                                        <?php
                                        if($at['prof_acomp']!=""){
                                            $us=fncgetusuario($at['prof_acomp']);
                                            echo $us['nome'];
                                            echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                        }else{
                                            echo "Aguardando Acompanhamento";
                                        }
                                        ?>
                                    </strong>
                                </footer>

                            </blockquote>
                    <?php
                        }
                        ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once ("includes/sectionmenulateral.php");?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>