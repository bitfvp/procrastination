<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_25"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_28"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Denuncia detalhes-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">

            <?php
            // Recebe
            if(isset($_GET['id_den'])) {
                //existe um id e se ele é numérico
                if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * FROM `mcu_ct_denuncia` WHERE `id`=?";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id_den']);
                    $consulta->execute();
                    //global $LQ;
                    //$LQ->fnclogquery($sql);
                    $denuncia = $consulta->fetch();
                    $sql=null;
                    $consulta=null;
                }
            }
            ?>


            <script>
                setTimeout(function(){
                    $('#collapseExample').collapse();
                }, 6000);
            </script>

            <!-- fim de denuncia-->
            <div class="card">
                <div class="card-header bg-info text-danger" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <strong>Denúncia</strong>
                </div>
                <div class="card-body collapse show" id="collapseExample">
                    <a href="index.php?pg=Vd_histprint&id=<?php echo $_GET['id'];?>&id_den=<?php echo $denuncia['id'];?>" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão da denúncia</span>
                    </a>
                    <blockquote class="blockquote blockquote-info">
                        <p>
                            <i class="fa fa-quote-left fa-sm "></i>
                            <strong class="text-success"><?php echo $denuncia['descricao']; ?></strong>
                            <i class="fa fa-quote-right fa-sm"></i>
                        </p>
                        Data:
                        <strong class="text-info">
                            <?php echo datahoraBanco2data($denuncia['data']); ?>&nbsp;&nbsp;
                        </strong>
                        <br>
                        Nivel:
                        <strong class="text-info"><?php if($denuncia['nivel']==0){echo "Normal";} if($denuncia['nivel']==1){echo "Urgente";} ?></strong>
                        <footer class="blockquote-footer">
                            Responsável pelo cadastro:
                            <strong class="text-info">
                                <?php
                                if($denuncia['prof_cadastro']!="0"){
                                    $us=fncgetusuario($denuncia['prof_cadastro']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                }
                                ?>
                            </strong>
                        </footer>
                    </blockquote>

                    <blockquote class="blockquote blockquote-muted blockquote-inverse">
                        Acompanhado:
                        <strong class="text-info">
                            <?php if($denuncia['acompanhado']==0){echo "Não";} if($denuncia['acompanhado']==1){echo "Sim";} ?>
                        </strong>
                        <br>
                        Data:
                        <strong class="text-info">
                            <?php
                            if($denuncia['data_acomp']!="0" and $denuncia['data_acomp']!=null){
                                echo datahoraBanco2data($denuncia['data_acomp']);
                            }else{
                                echo "Aguardando Acompanhamento";
                            }
                            ?>
                        </strong>
                        <br>
                        <p>
                            <i class="fa fa-quote-left fa-sm "></i>
                            <strong class="text-success"><?php echo $denuncia['relatorio']; ?></strong>
                            <i class="fa fa-quote-right fa-sm"></i>
                        </p>

                        <footer class="blockquote-footer">
                            <strong class="text-info">
                                <?php
                                if($denuncia['prof_acomp']!=""){
                                    $us=fncgetusuario($denuncia['prof_acomp']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                }else{
                                    echo "Aguardando Acompanhamento";
                                }
                                ?>
                            </strong>
                        </footer>

                    </blockquote>
                </div>
            </div>
<!-- fim de denuncia-->

<!--            inicio de form-->
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Lançamento de atividades
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vd_hist&aca=newat&id=<?php echo $_GET['id']; ?>&id_den=<?php echo $denuncia['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="data">Data:</label>
                                <input id="data" type="date" class="form-control input-sm" name="data" value="<?php echo date("Y-m-d"); ?>"/>
                                <input class="d-none" id="denuncia" type="text" name="denuncia" value="<?php echo $_GET['id_den']; ?>"/>
                            </div>
                            <div class="col-md-6">
                                <label for="restricao">Restrição:</label>
                                <select name="restricao" id="restricao" class="form-control">
                                    <option selected="" value="0">Aberto</option>
                                    <option value="1">Confidencial</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="atividade">Atividade:</label>
                                <select name="atividade" id="atividade"
                                        class="form-control input-sm">
                                    // vamos criar a visualização
                                    <option selected="" value="0">Selecione...</option>
                                    <?php
                                    foreach (fncpb_atlista() as $ativ) {
                                        ?>
                                        <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['atividade']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="7" name="descricao"></textarea>
                                <span id="cont">3000</span>/3000
                            </div>

                            <div class="col-md-12">
                                <div class="custom-file">
                                    <input id="arquivo" type="file" class="custom-file-input" name="arquivo[]" value="" multiple/>
                                    <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <?php
            // Recebe
            if (isset($_GET['id_den'])) {
                $at_iddenuncia = $_GET['id_den'];
                //existe um id e se ele é numérico
                if (!empty($at_iddenuncia) && is_numeric($at_iddenuncia)) {
                    // Captura os dados do cliente solicitado
                    if ($allow["admin"]!=1){
                        $sql = "SELECT * \n"
                            . "FROM mcu_pb_at \n"
                            . "WHERE (((mcu_pb_at.denuncia)=?) and (status=1) )\n"
                            . "ORDER BY mcu_pb_at.data DESC";
                    }else{
                        $sql = "SELECT * \n"
                            . "FROM mcu_pb_at \n"
                            . "WHERE (((mcu_pb_at.denuncia)=?))\n"
                            . "ORDER BY mcu_pb_at.data DESC";
                    }
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_iddenuncia);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $atividades = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <!--            inicio de historico-->
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico da Denúncia
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach ($atividades as $at) {
                            $autoriza_ver=0;
                            switch ($at['tipo']){
                                case 1:
                                    $cor="info";
                                    if ($allow["allow_10"]==1){$autoriza_ver=1;}
                                    break;
                                case 2:
                                    $cor="danger";
                                    if ($allow["allow_18"]==1){$autoriza_ver=1;}
                                    break;
                                case 3:
                                    $cor="warning";
                                    if ($allow["allow_26"]==1){$autoriza_ver=1;}
                                    break;
                                default:
                                    $cor="muted";
                                    $autoriza_ver=0;
                                    break;
                            }
                            $status="";
                            if ($at['status']==0){
                                $status="bg-danger-light ";
                            }
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $cor." ".$status; ?>  ">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i>
                                    <?php

                                    if($at['restricao']=="1" or $autoriza_ver!=1){
                                        if($at['profissional']==$_SESSION['id']){
                                            echo "<i class='text-danger fa fa-user-secret' title='Confidenciais e outros usuários não poderão visualizar'></i> ".$at['descricao'];
                                        }else{
                                            echo "<i class='text-danger fa fa-user-secret'> Confidencial - contate o profissional responsável por essa atividade...</i>";
                                        }
                                    }else{
                                        echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                    }
                                    ?>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                <strong class="text-info" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                Tipo de atividade:<strong class="text-info"><?php echo fncgetpb_atlista($at['atividade'])['atividade']; ?>&nbsp&nbsp</strong>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <?php
                                if ($at['denuncia']!=0){
                                    if ($env->env_mod_nome==" Conselho Tutelar ") {
                                        echo "";
                                        echo "<a href='index.php?pg=Vd_hist&id={$_GET['id']}&id_den={$at['denuncia']}'><h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6></a>";
                                    }else{
                                        echo "<h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6>";
                                    }
                                }
                                ?>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";

                                    if ((($at['profissional'] == $_SESSION['id']) and (Expiradatahora($at['data_ts'], 3)==1) and ($at['status']==1))or($allow["admin"]==1 and $at['status']==1)) {
                                        ?>
                                        <div class="dropdown show">
                                            <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Apagar
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="#">Não</a>
                                                <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vat&id={$_GET['id']}&aca=desativar_at&id_at={$at['id']}";?>">Apagar</a>
                                            </div>
                                        </div>
                                        <h6>Não é possivel apagar após 72h</h6>
                                        <?php
                                    }
                                    if ($at['status']==0){
                                        $us=fncgetusuario($at['delete_prof']);
                                        echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                                    }
                                    ?>
                                </footer>

                                <?php
                                if ($at['id'] != 0) {
                                    $sql = "SELECT * FROM `mcu_pb_dados` where atividade='{$at['id']}' ";
                                    global $pdo;
                                    $consulta = $pdo->prepare($sql);
                                    $consulta->execute();
                                    $dados = $consulta->fetchAll();//$total[0]
                                    $sql = null;
                                    $consulta = null;

                                    foreach ($dados as $dado){
                                        switch ($dado['extensao']) {
                                            case "docx":
                                                $caminho=$env->env_estatico . "img/docx.png";
                                                $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                break;
                                            case "doc":
                                                $caminho=$env->env_estatico . "img/doc.png";
                                                $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                break;
                                            case "xls":
                                                $caminho=$env->env_estatico . "img/xls.png";
                                                $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                break;
                                            case "xlsx":
                                                $caminho=$env->env_estatico . "img/xls.png";
                                                $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                break;
                                            case "pdf":
                                                $caminho=$env->env_estatico . "img/pdf.png";
                                                $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                break;
                                            default:
                                                $caminho="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                $link=$caminho;
                                                break;
                                        }
                                        echo "<div class='row'>";
                                        echo "<div class='col-md-10'>";
                                        echo "<a href='" . $link . "' target='_blank'>";
                                        echo "<img src='" . $caminho . "' alt='...' class='img-thumbnail img-fluid'>";
                                        echo "</a>";
                                        echo "</div>";
                                        echo "</div>";
                                    }

                                }//fim de foto?>
                            </blockquote>
                            <?php
                        }
                        ?>
                    </h6>
                </div>
            </div>
            <!--            inicio de historico-->
        </div>
<!--        fim da div 6-->

        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>
<!--fim da div row-->


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
<script type="text/javascript">
    function limite_textarea(valor) {
        //quant=65535;
        quant = 8000;
        total = valor.length;
        if (total <= quant) {
            resto = quant - total;
            document.getElementById('cont').innerHTML = resto;
        } else {
            document.getElementById('descricao').value = valor.substr(0, quant);
        }
    }
</script>