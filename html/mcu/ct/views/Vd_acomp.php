<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_28"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Denuncia Assumir-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

//carregar cb
if(isset($_GET['id_den'])) {
    $iddenuncia =$_GET['id_den'];
    //existe um id e se ele é numérico
    if (!empty($iddenuncia) && is_numeric($iddenuncia)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * FROM mcu_ct_denuncia WHERE id=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $iddenuncia);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $denuncia = $consulta->fetch();

        $sql=null;
        $consulta=null;
    }
}
?>
<main class="container"><!--todo conteudo-->

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">

        <div class="card">
            <div class="card-header bg-info text-light">
                Acompanhar caso
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <label for="data_pedido">Data:</label>
                    <strong class="text-info">
                        <?php echo dataRetiraHora($denuncia['data']); ?>
                    </strong>
                    <br>
                    <label for="data_pedido">Descrição:</label>
                    <strong class="text-info">"<?php echo $denuncia['descricao']; ?>"</strong>
                </div>

                <form action="index.php?pg=Vd_acomp&id=<?php echo $_GET['id'];?>&id_den=<?php echo $denuncia['id']; ?>&aca=denunciaedicao" method="post">
                    <div class="col-md-12">
                        <label for="relatorio">Breve Relatório:</label>
                        <textarea id="relatorio" onkeyup="limite_textarea(this.value,3000,relatorio,'cont')" maxlength="3000" class="form-control" rows="9" name="relatorio"></textarea>
                        <span id="cont">3000</span>/3000
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>