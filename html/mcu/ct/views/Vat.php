<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_25"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_26"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}
$page="Pasta do usuário-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamento de atividades
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vat&aca=newat&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="data">Data:</label>
                                <input id="data" type="date" class="form-control input-sm" name="data" value="<?php echo date("Y-m-d"); ?>"/>
                            </div>
                            <div class="col-md-6">
                                <label for="restricao">Restrição:</label>
                                <select name="restricao" id="restricao" class="form-control">
                                    <option selected="" value="0">Aberto</option>
                                    <option value="1">Confidencial</option>
                                </select>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="modalatividades" tabindex="-1" role="dialog" aria-labelledby="modalatividadesTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true"><i class="fas fa-times-circle"></i></span>
                                            </button>
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <?php
                                                    foreach (fncpb_atlista() as $ativ) {
                                                        ?>
                                                        <div class="col-md-6">
                                                            <input type="radio" class="form-check-input" id="<?php echo $ativ['atividade']; ?>" name="atividade" value="<?php echo $ativ['id']; ?>"  required>
                                                            <label for="<?php echo $ativ['atividade']; ?>" onclick="document.getElementById('terceiroConteudo').innerHTML = '<?php echo $ativ['atividade'];?>'">
                                                                <?php echo $ativ['atividade'];?>
                                                            </label>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Fechar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 py-2">
                                <button type="button" id="okk" class="btn btn-outline-info" data-toggle="modal" data-target="#modalatividades">
                                    Selecionar Atividade
                                </button>
                                <i id="segundoConteudo" class="text-danger">Não selecionado</i>
                                <i id="terceiroConteudo" class="text-info"></i>
                                <script type="application/javascript">
                                    var radioAtividade = document.querySelectorAll('input[name="atividade"]');
                                    var segundoConteudo = document.querySelector('#segundoConteudo');
                                    radioAtividade.forEach(function(value){
                                        value.onclick = function(event){
                                            $("#segundoConteudo").css("display", "none");
                                            // $("#terceiroConteudo").html( "Tipo de atividade selecionada" );
                                        }
                                    })
                                </script>
                            </div>
                            <div class="col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="9" name="descricao"></textarea>
                                <span id="cont">3000</span>/3000
                            </div>

                            <div class="col-md-12">
                                <div class="custom-file">
                                    <input id="arquivo" type="file" class="custom-file-input" name="arquivo[]" value="" multiple/>
                                    <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    if ($allow["admin"]!=1){
                        $sql = "SELECT * \n"
                            . "FROM mcu_pb_at \n"
                            . "WHERE (mcu_pb_at.pessoa=? and status=1 and (tipo=1 or tipo=2 or tipo=3 or tipo=4) )\n"
                            . "ORDER BY mcu_pb_at.data DESC";
                    }else{
                        $sql = "SELECT * \n"
                            . "FROM mcu_pb_at \n"
                            . "WHERE (mcu_pb_at.pessoa=? and (tipo=1 or tipo=2 or tipo=3 or tipo=4))\n"
                            . "ORDER BY mcu_pb_at.data DESC";
                    }
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $atividades = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vatprint&id=<?php echo $_GET['id'];?>" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão de histórico</span>
                    </a>
                    <script type="text/javascript">
                        function mostra_pb() {
                            var btn_pb = document.getElementById("btn_pb");
                            if (document.getElementById('bloco_pb').style.display === "none") {
                                // document.getElementById('bloco_pb').style.display = "";
                                $('h6#bloco_pb').toggle(2000);
                                btn_pb.className='btn btn-sm btn-info float-right mx-1';
                            } else {
                                // document.getElementById('bloco_pb').style.display = "none";
                                $('h6#bloco_pb').toggle(2000);
                                btn_pb.className='btn btn-sm btn-outline-info float-right mx-1';
                            }
                        }
                        function mostra_pe() {
                            var btn_pe = document.getElementById("btn_pe");
                            if (document.getElementById('bloco_pe').style.display === "none") {
                                // document.getElementById('bloco_pe').style.display = "";
                                $('h6#bloco_pe').toggle(2000);
                                btn_pe.className='btn btn-sm btn-danger float-right mx-1';
                            } else {
                                // document.getElementById('bloco_pe').style.display = "none";
                                $('h6#bloco_pe').toggle(2000);
                                btn_pe.className='btn btn-sm btn-outline-danger float-right mx-1';
                            }
                        }
                        function mostra_ct() {
                            var btn_ct = document.getElementById("btn_ct");
                            if (document.getElementById('bloco_ct').style.display === "none") {
                                // document.getElementById('bloco_ct').style.display = "";
                                $('h6#bloco_ct').toggle(2000);
                                btn_ct.className='btn btn-sm btn-warning float-right mx-1';
                            } else {
                                // document.getElementById('bloco_ct').style.display = "none";
                                $('h6#bloco_ct').toggle(2000);
                                btn_ct.className='btn btn-sm btn-outline-warning float-right mx-1';
                            }
                        }
                        function mostra_bf() {
                            var btn_bf = document.getElementById("btn_bf");
                            if (document.getElementById('bloco_bf').style.display === "none") {
                                // document.getElementById('bloco_bf').style.display = "";
                                $('h6#bloco_bf').toggle(2000);
                                btn_bf.className='btn btn-sm btn-success float-right mx-1';
                            } else {
                                // document.getElementById('bloco_bf').style.display = "none";
                                $('h6#bloco_bf').toggle(2000);
                                btn_bf.className='btn btn-sm btn-outline-success float-right mx-1';
                            }
                        }
                    </script>
                    <button id="btn_pb" onclick="mostra_pb()" class="btn btn-sm btn-info float-right mx-1">PB</button>
                    <button id="btn_pe" onclick="mostra_pe()" class="btn btn-sm btn-danger float-right mx-1">PE</button>
                    <button id="btn_ct" onclick="mostra_ct()" class="btn btn-sm btn-warning float-right mx-1">CT</button>
                    <button id="btn_bf" onclick="mostra_bf()" class="btn btn-sm btn-success float-right mx-1">BF</button>

                    <!--apenas pra funcao funcionar e o botao ficar translucido-->
                    <h6 id="bloco_pb"></h6>
                    <h6 id="bloco_pe"></h6>
                    <h6 id="bloco_ct"></h6>
                    <h6 id="bloco_bf"></h6>
                    <!--apenas pra funcao funcionar e o botao ficar translucido-->
                    <?php
                    foreach ($atividades as $at) {
                        $autoriza_ver=0;
                        switch ($at['tipo']){
                            case 1:
                                $cor="info";
                                $id_tipo="bloco_pb";
                                if ($allow["allow_10"]==1){$autoriza_ver=1;}
                                break;
                            case 2:
                                $cor="danger";
                                $id_tipo="bloco_pe";
                                if ($allow["allow_18"]==1){$autoriza_ver=1;}
                                break;
                            case 3:
                                $cor="warning";
                                $id_tipo="bloco_ct";
                                if ($allow["allow_26"]==1){$autoriza_ver=1;}
                                break;
                            case 4:
                                $cor="success";
                                $id_tipo="bloco_bf";
                                if ($allow["allow_42"]==1){$autoriza_ver=1;}
                                break;
                            default:
                                $cor="muted";
                                $autoriza_ver=0;
                                break;
                        }
                        $status="";
                        if ($at['status']==0){
                            $status="bg-danger-light ";
                        }
                        echo "<h6 id='{$id_tipo}'>";
                        ?>
                        <hr>
                        <blockquote class="blockquote blockquote-<?php echo $cor." ".$status; ?>">
                            <p>
                                <i class="fa fa-quote-left fa-sm "></i>
                                <?php
                                if($at['restricao']=="1" or $autoriza_ver!=1){
                                    if($at['profissional']==$_SESSION['id']){
                                        echo "<i class='text-danger fa fa-user-secret' title='Confidenciais e outros usuários não poderão visualizar'></i> ".$at['descricao'];
                                    }else{
                                        echo "<i class='text-danger fa fa-user-secret fa-2x'></i>Confidencial - contate o profissional responsável pela atividade...";
                                    }
                                }else{
                                    echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                }
                                ?>
                                <i class="fa fa-quote-right fa-sm"></i>
                            </p>
                            <strong class="text-info" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                            Tipo de atividade: <strong class="text-info"><?php echo fncgetpb_atlista($at['atividade'])['atividade']; ?>&nbsp&nbsp</strong>
                            <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                            <?php
                            if ($at['denuncia']!=0){
                                if ($env->env_mod_nome==" Conselho Tutelar ") {
                                    echo "";
                                    echo "<a href='index.php?pg=Vd_hist&id={$_GET['id']}&id_den={$at['denuncia']}'><h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6></a>";
                                }else{
                                    echo "<h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6>";
                                }
                            }
                            ?>
                            <footer class="blockquote-footer">
                                <?php
                                $us=fncgetusuario($at['profissional']);
                                echo $us['nome'];
                                echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                if ((($at['profissional'] == $_SESSION['id']) and (Expiradatahora($at['data_ts'], 3)==1) and ($at['status']==1))or($allow["admin"]==1 and $at['status']==1)) {
                                    ?>
                                    <div class="dropdown show">
                                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apagar
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vat&id={$_GET['id']}&aca=desativar_at&id_at={$at['id']}";?>">Apagar</a>
                                        </div>
                                    </div>
                                    <h6>Não é possivel apagar após 72h</h6>
                                    <?php
                                }
                                if ($at['status']==0){
                                    $us=fncgetusuario($at['delete_prof']);
                                    echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                                }
                                ?>
                            </footer>
                            <?php
                            if ($at['id'] != 0) {
                                $sql = "SELECT * FROM `mcu_pb_dados` where atividade='{$at['id']}' ";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->execute();
                                $dados = $consulta->fetchAll();//$total[0]
                                $sql = null;
                                $consulta = null;

                                foreach ($dados as $dado){
                                    switch ($dado['extensao']) {
                                        case "docx":
                                            $caminho=$env->env_estatico . "img/docx.png";
                                            $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                            break;
                                        case "doc":
                                            $caminho=$env->env_estatico . "img/doc.png";
                                            $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                            break;
                                        case "xls":
                                            $caminho=$env->env_estatico . "img/xls.png";
                                            $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'];
                                            break;
                                        case "xlsx":
                                            $caminho=$env->env_estatico . "img/xls.png"."";
                                            $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'];
                                            break;
                                        case "pdf":
                                            $caminho=$env->env_estatico . "img/pdf.png";
                                            $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                            break;
                                        default:
                                            $caminho="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                            $link=$caminho;
                                            break;
                                    }
                                    echo "<div class='row'>";
                                    echo "<div class='col-md-10'>";
                                    echo "<a href='" . $link . "' target='_blank'>";
                                    echo "<img src='" . $caminho . "' alt='...' class='img-thumbnail img-fluid'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                }
                            }//fim de foto?>
                        </blockquote>
                        <?php
                        echo "</h6>";
                    }
                    ?>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>