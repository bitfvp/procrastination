<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();

//classe para debugar e salvar sqls ao banco de dados
include_once("{$env->env_root}models/LogQuery.class.php");//classe de log de query
$LQ = new LogQuery();

//classe para debugar e salvar sqls ao banco de dados
include_once("{$env->env_root}models/Words.class.php");//classe de log de query
$W_O_R_D_S = new Words();

//classe com funcao para salvar os logs
include_once("{$env->env_root}models/mcu/Log.class.php");//inclui classe de log
$LL = new Log();

//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

////troca tema
include_once("{$env->env_root}controllers/trocatema.php");
////logout
include_once("{$env->env_root}controllers/logout.php");

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

//valida se há manutenção
include_once("{$env->env_root}controllers/validaManutencao.php");

//chat msg
include_once("{$env->env_root}models/mcu/Chat_Msg.class.php");
include_once("{$env->env_root}controllers/mcu/chat_msg.php");

/* inicio do Bloco dedidado*/
if (isset($_SESSION['logado']) and $_SESSION['logado']=="1"){

    include_once("{$env->env_root}controllers/usuario_lista.php");

    include_once("{$env->env_root}controllers/profissao_lista.php");
    include_once("{$env->env_root}controllers/mcu/bairro_lista.php");
    include_once("{$env->env_root}controllers/sexo_lista.php");

    //pessoa manipulacao
    include_once("{$env->env_root}models/mcu/Pessoa.class.php");
    include_once("{$env->env_root}controllers/mcu/pessoa_new.php");
    include_once("{$env->env_root}controllers/mcu/pessoa_edit.php");
    include_once("{$env->env_root}controllers/mcu/pessoa_lista.php");

    include_once("models/Atividade.class.php");
    include_once("controllers/at_new.php");
    include_once("controllers/at_excluir.php");
    include_once("controllers/at_lista.php");

    //denuncia
    include_once("models/Denuncia.class.php");
    include_once("controllers/d_new.php");
//    include_once("controllers/denunciaedit.php");

    include_once("{$env->env_root}models/mcu/Cf.class.php");
    include_once("{$env->env_root}controllers/mcu/cf.php");

    include_once("{$env->env_root}includes/mcu/rank_top_pessoa.php");

}
/* fim do bloco dedicado*/


//metodo de checar usuario
include_once("{$env->env_root}controllers/confirmUser.php");