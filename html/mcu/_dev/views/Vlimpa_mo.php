<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="limpa-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');



?>

<main class="container">
    <div class="row">
        <div class="col-6">
        </div>
        <div class="col-6">

            <?php
            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=0";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $penaoverificado = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $peverificado = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=9";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $peapagar = $consulta->fetch();
            $sql=null;
            $consulta=null;


            echo "<br><i class='text-dark'>Aguardando:".$penaoverificado[0]."</i><br>";
            echo "<i class='text-info'>Possui registro:".$peverificado[0]."</i><br>";
            echo "<i class='text-danger'>Apagar:".$peapagar[0]."</i><br>";

            ?>

        </div>
    </div>











    <script>
        setTimeout(function() {
            window.location.reload(1);
        }, 10000); // segundos
    </script>



</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>