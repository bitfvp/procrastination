<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="limpa-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');


//parada de emergencia
//header("Location: index.php?pg=Vhome");
//exit();

if (!isset($_GET['quant']) or !is_numeric($_GET['quant'])){
    header("Location: index.php?pg=Vhome");
    exit();
}else{
    $quant=$_GET['quant'];
}
$quant=10;

if (isset($_GET['orderby']) and ($_GET['orderby']=="nome" or $_GET['orderby']=="nascimento" or $_GET['orderby']=="endereco")){
    $orderby=$_GET['orderby'];
}else{
    $orderby="nome";
}

if (isset($_GET['ordem']) and ($_GET['ordem']=="ASC" or $_GET['ordem']=="DESC")){
    $ordem=$_GET['ordem'];
}else{
    $ordem="ASC";
}



?>

<main class="container">
    <div class="row">

        <div class="col-12">
            <h1 class="display-1"><span id="minuts">00</span>:<span id="segons">00</span>:<span id="ml_segons">00</span></h1>
            <?php
            echo "<i>{$orderby} {$ordem}</i>";
            ?>
        </div>

        <div class="col-6">

            <?php
            $sql = "SELECT id,nome FROM mcu_pessoas WHERE verifica=0 order by `{$orderby}` {$ordem} limit 0,{$quant}";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $lipessoa = $consulta->fetchall();
            $sql=null;
            $consulta=null;


            foreach ($lipessoa as $pessoa){
                echo $pessoa['nome'];
                echo " // ";
                $tempp = verifica_registro($pessoa['id']);
                echo $tempp;
                echo " // ";

                if ($tempp===0){
                    //vai apagar
                    try{
                        $sql="UPDATE mcu_pessoas SET verifica=9 WHERE id=:id";
                        global $pdo;
                        $atualizar=$pdo->prepare($sql);
                        $atualizar->bindValue(":id", $pessoa['id']);
                        $atualizar->execute();
                    }catch ( PDOException $error_msg){
                        echo 'Erro '. $error_msg->getMessage();
                    }
                    $atualizar=null;
                    $sql=null;

                    echo "<i class='text-danger'>Marcado</i>";
                }else{
                    //não vai apagar
                    try{
                        $sql="UPDATE mcu_pessoas SET verifica=1 WHERE id=:id";
                        global $pdo;
                        $atualizarankpessoa=$pdo->prepare($sql);
                        $atualizarankpessoa->bindValue(":id", $pessoa['id']);
                        $atualizarankpessoa->execute();
                    }catch ( PDOException $error_msg){
                        echo 'Erro  '. $error_msg->getMessage();
                    }
                    $atualizar=null;
                    $sql=null;

                    echo "<i class='text-info'>Possui registro</i>";

                }
                echo "<hr class='my-0 py-0'>";


            }
            ?>

        </div>
        <div class="col-6">

            <?php
            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=0";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $penaoverificado = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $peverificado = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT COUNT(id) FROM mcu_pessoas WHERE verifica=9";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $peapagar = $consulta->fetch();
            $sql=null;
            $consulta=null;


            echo "<br><h1 class='text-dark'>Aguardando:<br>".$penaoverificado[0]."</h1><br>";
            echo "<i class='text-info'>Possui registro:".$peverificado[0]."</i><br>";
            echo "<i class='text-danger'>Apagar:".$peapagar[0]."</i><br>";

            ?>

        </div>

    </div>











    <script>
        setTimeout(function() {
            window.location.reload(1);
        }, 1000); // segundos
    </script>

</main>

<script>
    var cronometre;



    function detenerse()
    {
        clearInterval(cronometre);
    }
    function cargar()
    {
        contador_mls = 0;
        contador_s = 0;
        contador_m = 0;
        mls = document.getElementById("ml_segons");
        s = document.getElementById("segons");
        m = document.getElementById("minuts");

        cronometre = setInterval(
            function()
            {
                if (contador_mls ==60)
                {
                    contador_mls = 0;
                    contador_s++;
                    s.innerHTML = contador_s;

                    if (contador_s==60)
                    {
                        contador_s = 0;
                        contador_m++;
                        m.innerHTML = contador_m;
                    }
                }
                mls.innerHTML = contador_mls;
                contador_mls++;

            }
            ,16.666666);
    }


    $(window).on("load", function(){
        cargar();
    });
</script>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>