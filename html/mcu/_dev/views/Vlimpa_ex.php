<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="limpa-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');
?>

<main class="container">
    <?php
    $sql = "DELETE FROM mcu_pessoas WHERE verifica=9 ";
    global $pdo;
    $deleta = $pdo->prepare($sql);
    $deleta->execute();
    $sql=null;
    $deleta=null;
    ?>

    <i class="text-info">Todos os registros marcados como 9, deletados</i>


</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>