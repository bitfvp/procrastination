<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar atividade-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="atividadelistasave";
    $atividade=fncgetatividade($_GET['id']);
}else{
    $a="atividadelistanew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vat_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de atividade</h3>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $atividade['id']; ?>"/>
                <label for="atividade">ATIVIDADE:</label>
                <input autocomplete="off" id="atividade" placeholder="atividade" type="text" class="form-control" name="atividade" value="<?php echo $atividade['atividade']; ?>"/>
            </div>

            <div class="col-md-5">
                <label for="pb">PB</label>
                <select name="pb" id="pb" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['pb'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['pb'];
                    } ?>">
                        <?php
                        if ($atividade['pb'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['pb'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="pe">PE</label>
                <select name="pe" id="pe" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['pe'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['pe'];
                    } ?>">
                        <?php
                        if ($atividade['pe'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['pe'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="ct">CT</label>
                <select name="ct" id="ct" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['ct'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['ct'];
                    } ?>">
                        <?php
                        if ($atividade['ct'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['ct'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="bf">BF</label>
                <select name="bf" id="bf" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['bf'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['bf'];
                    } ?>">
                        <?php
                        if ($atividade['bf'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['bf'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="aabb">AABB</label>
                <select name="aabb" id="aabb" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($atividade['aabb'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $atividade['aabb'];
                    } ?>">
                        <?php
                        if ($atividade['aabb'] == 0) {
                            echo "Não";
                        }
                        if ($atividade['aabb'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>