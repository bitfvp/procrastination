<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();

$apagacidadesextra=1;
if ($apagacidadesextra==1) {
    $sql = "DELETE FROM mcu_sidim_pessoas_mcu "
        . "WHERE "
        . "municipio <>'Manhuacu' ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "<br>APAGADO<br>";
}
?>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>