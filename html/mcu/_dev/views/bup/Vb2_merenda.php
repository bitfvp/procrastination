<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');
?>

<main class="container"><!--todo conteudo-->
        <?php


        ///////////////////////
        if (isset($_GET['cont']) and is_numeric($_GET['cont'])){

            $temp_var1=0;
            $temp_var2=$_GET['cont'];
        }else{
            $temp_var1=0;
            $temp_var2=100;
        }

        //pessoas de sidim com cpf
        $sql = "SELECT * FROM `merenda_crianca` limit {$temp_var1},{$temp_var2}";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoasmerenda = $consulta->fetchAll();
        $contmerenda = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>
    <h1>Merenda</h1>
    <h6>pessoas da base merenda:<?php echo $contmerenda;?>(limitado)</h6>
    <div id="general">
        <h1><span id="minuts">00</span>:<span id="segons">00</span>:<span id="ml_segons">00</span></h1>
        </p>
        <div id="tecles">
            <a href='index.php?pg=Vb2_merenda&cont=100' class='btn btn-success m-1' onclick='cargar()' >100 (8s)</a>
            <a href='index.php?pg=Vb2_merenda&cont=1000' class='btn btn-success m-1' onclick='cargar()' >1000 (1,20m)</a>
            <a href='index.php?pg=Vb2_merenda&cont=10000' class='btn btn-success m-1' onclick='cargar()' >10000 (13,30m)</a>
            <a href='index.php?pg=Vb2_merenda&cont=20000' class='btn btn-success m-1' onclick='cargar()' >20000 (20m)</a>
            <a href='index.php?pg=Vb2_merenda&cont=50000' class='btn btn-success m-1' onclick='cargar()' >50000 (50m)</a>
        </div>
    </div>
    <hr>
    <table class="table table-sm table-bordered">
        <thead>
        <tr>
            <td>id</td>
            <td>crianca</td>
            <td>crianca</td>
            <td>rf</td>
            <td>bairro</td>
            <td>escola</td>
        </tr>
        </thead>

        <tbody>
    <?php
    $cont=0;
        foreach ($pessoasmerenda as $pessoamerenda){


            $sql = "SELECT * FROM merenda_todos  where nome=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $pessoamerenda['nome']);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $conttodos = $consulta->rowCount();
            $ptodos = $consulta->fetch();
            $sql = null;
            $update = null;

            if ($conttodos>1){
                $hamaisdeum=1;
            }else{
                $hamaisdeum=0;
            }


            $cont++;
            echo "<tr>";
            echo "<td>";
            echo $cont;
            echo "</td>";
                echo "<td>";
                echo $pessoamerenda['nome'];
                echo "</td>";
            echo "<td>";
            echo $ptodos['nome'];
            echo "</td>";

            echo "<td>";
            echo $ptodos['rf'];
            echo "</td>";
            echo "<td>";
            echo $ptodos['bairro'];
            echo "</td>";
            echo "<td>";
            echo $ptodos['escola'];
            echo "</td>";

            echo "</tr>";
        }
        ?>
        </tbody>

    </table>

    <script>
        var cronometre;



        function detenerse()
        {
            clearInterval(cronometre);
        }
        function cargar()
        {
            contador_mls = 0;
            contador_s = 0;
            contador_m = 0;
            mls = document.getElementById("ml_segons");
            s = document.getElementById("segons");
            m = document.getElementById("minuts");

            cronometre = setInterval(
                function()
                {
                    if (contador_mls ==60)
                    {
                        contador_mls = 0;
                        contador_s++;
                        s.innerHTML = contador_s;

                        if (contador_s==60)
                        {
                            contador_s = 0;
                            contador_m++;
                            m.innerHTML = contador_m;
                        }
                    }
                    mls.innerHTML = contador_mls;
                    contador_mls++;

                }
                ,16.666666);
        }
    </script>







</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>