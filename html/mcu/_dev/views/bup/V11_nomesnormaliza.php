<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');


//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();
?>

<main class="container">
        <?php


        ///////////////////////
        if (isset($_GET['cont']) and is_numeric($_GET['cont'])){

            $temp_var1=0;
            $temp_var2=$_GET['cont'];
        }else{
            $temp_var1=0;
            $temp_var2=100;
        }

        //pessoas de sidim com cpf
        $sql = "SELECT * FROM `mcu_pessoas` ORDER BY nome limit {$temp_var1},{$temp_var2}";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoasmcu = $consulta->fetchAll();
        $contmcu = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>
    <h1>corrige nomes mcu</h1>
    <h6>pessoas :<?php echo $contmcu;?>(limitado)</h6>
    <div id="general">
        <h1><span id="minuts">00</span>:<span id="segons">00</span>:<span id="ml_segons">00</span></h1>
        <div id="tecles">
            <a href='index.php?pg=V11_nomesnormaliza&cont=100' class='btn btn-success m-1' onclick='cargar()' >100</a>
            <a href='index.php?pg=V11_nomesnormaliza&cont=20000' class='btn btn-success m-1' onclick='cargar()' >20000</a>
            <a href='index.php?pg=V11_nomesnormaliza&cont=200000' class='btn btn-success m-1' onclick='cargar()' >200000</a>
        </div>
    </div>
    <hr>
    <?php
        foreach ($pessoasmcu as $pessoamcu){
//
            $nome=remover_caracter(ucwords(strtolower($pessoamcu['nome'])));
            $mae=remover_caracter(ucwords(strtolower($pessoamcu['mae'])));
            $pai=remover_caracter(ucwords(strtolower($pessoamcu['pai'])));

                //    fazer update no mcu
                $sql = "UPDATE mcu_pessoas SET ";
                $sql.="nome=:nome, mae=:mae, pai=:pai where id=:id";
                global $pdo;
                $update = $pdo->prepare($sql);
                $update->bindValue('nome', $nome);
                $update->bindValue('mae', $mae);
                $update->bindValue('pai', $pai);
                $update->bindValue('id', $pessoamcu['id']);
                $update->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $sql = null;
                $update = null;


        }
        ?>

    <script>
        var cronometre;



        function detenerse()
        {
            clearInterval(cronometre);
        }
        function cargar()
        {
            contador_mls = 0;
            contador_s = 0;
            contador_m = 0;
            mls = document.getElementById("ml_segons");
            s = document.getElementById("segons");
            m = document.getElementById("minuts");

            cronometre = setInterval(
                function()
                {
                    if (contador_mls ==60)
                    {
                        contador_mls = 0;
                        contador_s++;
                        s.innerHTML = contador_s;

                        if (contador_s==60)
                        {
                            contador_s = 0;
                            contador_m++;
                            m.innerHTML = contador_m;
                        }
                    }
                    mls.innerHTML = contador_mls;
                    contador_mls++;

                }
                ,16.666666);
        }
    </script>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>