<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();


$corigebairro1=1;

if ($corigebairro1==1){

    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='1' "
//        ."SET bairro='*ALFA SUL' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Alfa Sul' "
        ."or bairro = 'Alfasul' "
        ."or bairro = 'Bairo Alfa Sul'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com ALFA SUL<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='2' "
//        ."SET bairro='*BAIXADA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Baixada' "
        ."or bairro = 'Baixada' "
        ."or bairro = 'Delegacia' "
        ."or bairro = 'Felipe Nacif' "
        ."or bairro = 'Melo Viana' "
        ."or bairro = 'Pedregal' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BAIXADA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='4' "
//        ."SET bairro='*BARREIRO DE CIMA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Barreiro' "
        ."or bairro = 'Barreiros' "
        ."or bairro = 'Cgo Barreiro'"
        ."or bairro = 'Corrego Barreiro'"
        ."or bairro = 'Corrego Do Barreiro'"
        ."or bairro = 'Pedra Furada'"
        ."or bairro = 'Cgo Sinceridade'"
        ."or bairro = 'Corrego Do Balsamo'"
        ."or bairro = 'Corrego Sinceridade'"
        ."or bairro = 'Corrego Tres Barras'"
        ."or bairro = 'Corrego0 Do Balssamo'"
        ."or bairro = 'Fazenda Roca Grande'"
        ."or bairro = 'Usina Da Sinceridade'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BARREIRO DE CIMA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='5' "
//        ."SET bairro='*BELA VISTA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Bela Vista' "
        ."or bairro = 'Bela Vista' "
        ."or bairro = 'Loteamento Bela Vista'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BELA VISTA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='6' "
//        ."SET bairro='*BOM JARDIM' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bom Jardim' "
        ."or bairro = 'Bom Jardima' "
        ."or bairro = 'Corrego Bom Jardim'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BOM JARDIM<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='7' "
//        ."SET bairro='*BOM JESUS DE REALEZA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bom Jesus' "
        ."or bairro = 'Bom Jesus' "
        ."or bairro = 'Bom Jesus De Realeza'"
        ."or bairro = 'Bom Jesus Do Realeza'"
        ."or bairro = 'Bom  Jesus'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BOM JESUS DE REALEZA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='8' "
//        ."SET bairro='*BOM PASTOR' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bom Pastor' "
        ."or bairro = 'Bom Pastor Ap 402' "
        ."or bairro = 'Bompastor'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com BOM PASTOR<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='9' "
//        ."SET bairro='*CATUAI' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Catuai' "
        ."or bairro = 'Caatuai' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com CATUAI<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='10' "
//        ."SET bairro='*CENTRO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Centro' "
        ."or bairro = 'Centro' "
        ."or bairro = 'Centro.'"
        ."or bairro = 'Cemtro'"
        ."or bairro = 'Cenro'"
        ."or bairro = 'PraCa Cesar Leite'"
        ."or bairro = 'Proximo Ao B. Brasil'"
        ."or bairro = '15 De Novembro'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com CENTRO<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='11' "
//        ."SET bairro='*COLINA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'B Colina' "
        ."or bairro = 'Bairro Colina' "
        ."or bairro = 'Colina'"
        ."or bairro = 'Colinas'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com COLINA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='12' "
//        ."SET bairro='*COQUEIRO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Coqueiro' "
        ."or bairro = 'Coqueiro' "
        ."or bairro = 'Peixoto' "
        ."or bairro = 'Antonio Miranda Sete' "
        ."or bairro = 'Av. Getulio Varfas N.565' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com COQUEIRO<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='13' "
//        ."SET bairro='*COQUEIRO RURAL' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Coqueiro Rural' "
        ."or bairro = 'Corrego Coqueiro Rural' "
        ."or bairro = 'Comunidade Terapeutica' "
        ."or bairro = 'Fazenda Heringer' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com COQUEIRO RURAL<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='14' "
//        ."SET bairro='*DOM CORREA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Dom Correa' "
        ."or bairro = 'Dom Correa Zona Rural' "
        ."or bairro = 'Dom Correia'"
        ."or bairro = 'Zona Rural-dom Correa'"
        ."or bairro = 'Zona Rural-domcorrea'"
        ."or bairro = 'Bom Correia'"
        ."or bairro = 'D.correa'"
        ."or bairro = 'Do Correa'"
        ."or bairro = 'Don Correia'"
        ."or bairro = 'Com Correa'"
        ."or bairro = 'Corrego Gaviao'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com DOM CORREA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='15' "
//        ."SET bairro='*ENGENHO DA SERRA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Eng Da Serra' "
        ."or bairro = 'Engenho Da Sarra' "
        ."or bairro = 'Engenho Da Serr'"
        ."or bairro = 'Engenho Da Serra'"
        ."or bairro = 'Engenho Da Serrra'"
        ."or bairro = 'Engenho De Serra'"
        ."or bairro = 'Engenho Serra'"
        ."or bairro = 'Engerro Da Serra'"
        ."or bairro = 'Perto Do Matadouro'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com ENGENHO DA SERRA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='16' "
//        ."SET bairro='*LAJINHA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Lajinha' "
        ."or bairro = 'Lajinda' "
        ."or bairro = 'Lajinha'"
        ."or bairro = 'Lajunha'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com LAJINHA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='17' "
//        ."SET bairro='*LUCIANO HERINGER' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Luciano Heringer' "
        ."or bairro = 'Luciano Heriger' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com LUCIANO HERINGER<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='18' "
//        ."SET bairro='*MANHUACUZINHO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'MahuaCuzinho' "
        ."or bairro = 'ManhaCuzinho' "
        ."or bairro = 'ManhuaCizinho'"
        ."or bairro = 'ManhuaCuziinho'"
        ."or bairro = 'ManhuaCuzinho'"
        ."or bairro = 'ManhuaCuzinho Zona Rural'"
        ."or bairro = 'ManuaCuzinho'"
        ."or bairro = 'Corrego Santa Clara'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com MANHUACUZINHO<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='19' "
//        ."SET bairro='*MATINHA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Da Matinha' "
        ."or bairro = 'Bairro Matinha' "
        ."or bairro = 'Matina'"
        ."or bairro = 'Matinha'"
        ."or bairro = 'Matinha Av Palmeiras'"
        ."or bairro = 'Maatinha'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com MATINHA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='20' "
//        ."SET bairro='*MONTE ALVERNE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Monte Alverne' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com MONTE ALVERNE<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='21' "
//        ."SET bairro='*NOSSA SENHORA APARECIDA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Nossa Senhora Aparecida' "
        ."or bairro = 'Nossa Aparecida' "
        ."or bairro = 'Nossa Semhora Aparecida'"
        ."or bairro = 'Nossa Semhora Da Aparecida'"
        ."or bairro = 'Nossa Senh0ra Aparecida'"
        ."or bairro = 'Nossa Senhoara Aparecida'"
        ."or bairro = 'Nossa Senhora'"
        ."or bairro = 'Nossa Senhora Aaprecida'"
        ."or bairro = 'Nossa Senhora Aoarecida'"
        ."or bairro = 'Nossa Senhora Aparecida'"
        ."or bairro = 'Nossa Senhora Aprecida'"
        ."or bairro = 'Nossa Senhora Da Aparecida'"
        ."or bairro = 'Nossa Senhora Parecida'"
        ."or bairro = 'Nossa Senhra Aparecida'"
        ."or bairro = 'Nossa Sra Aparecida'"
        ."or bairro = 'Nosssa Senhora Aparecia'"
        ."or bairro = 'Nosssa Senhora Aparecida'"
        ."or bairro = 'Ns Aparecida'"
        ."or bairro = 'B. Nsa. Senhora Aparecida'"
        ."or bairro = 'Mossa Senhora Aparecida'"
        ."or bairro = 'N S Aparecida '"
        ."or bairro = 'N Senhora Aparecida'"
        ."or bairro = 'N. S. Aparecida'"
        ."or bairro = 'N.s.aparecida'"
        ."or bairro = 'Noissa Senhora Aparecida'"
        ."or bairro = 'Nosa Senhora Aparecida'"
        ."or bairro = 'Nosas Senhoara Aparecida'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com NOSSA SENHORA APARECIDA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='22' "
//        ."SET bairro='*OPERARIOS' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Dos Operarios' "
        ."or bairro = 'Operario' "
        ."or bairro = 'Operarios'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com OPERARIOS<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='23' "
//        ."SET bairro='*PALMEIRAS' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Cgo Das Lajes -palmeiras' "
        ."or bairro = 'Cgo Palmeiras' "
        ."or bairro = 'Palmeira'"
        ."or bairro = 'Palmeiras'"
        ."or bairro = 'Palmeiras Do ManhuaCu'"
        ."or bairro = 'Palmeiras Zona Rural'"
        ."or bairro = 'Zona Rural Palmeiras'"
        ."or bairro = 'Barra Taquaral'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PALMEIRAS<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='52' "
//        ."SET bairro='*PALMEIRINHA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Palmeirinha' "
        ."or bairro = 'Palmeirinhas' "
        ."or bairro = 'Palmerinha'"
        ."or bairro = 'Palmirinha'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PALMEIRINHA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='24' "
//        ."SET bairro='*PETRINA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Petrina' "
        ."or bairro = 'Petrina' "
        ."or bairro = 'Rua Rodriues Sette '"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PETRINA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='25' "
//        ."SET bairro='*PINHEIRO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Pinheiro' "
        ."or bairro = 'Pinheiro 1' "
        ."or bairro = 'Pimheiro' "
        ."or bairro = 'Pinheiros' "
        ."or bairro = 'Pnheiro' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PINHEIRO<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='26' "
//        ."SET bairro='*PINHEIRO II' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Pinheiro 2' "
        ."or bairro = 'Pinheiro Does' "
        ."or bairro = 'Pinheiro Dois'"
        ."or bairro = 'Pinheiro Ii'"
        ."or bairro = 'Pinheiro Ll'"
        ."or bairro = 'Pinheiro2'"
        ."or bairro = 'P. Dois'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PINHEIRO II<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='53' "
//        ."SET bairro='*PINHEIRO III' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Pinheiro Iii' "
        ."or bairro = 'Pinheiro3' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PINHEIRO III<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='27' "
//        ."SET bairro='*PONTE DA ALDEIA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Ponte Aldeia' "
        ."or bairro = 'Ponte Da' "
        ."or bairro = 'Ponte Da Adeia'"
        ."or bairro = 'Ponte Da Aladeia'"
        ."or bairro = 'Ponte Da Aldeia'"
        ."or bairro = 'Ponte Da Aldiea'"
        ."or bairro = 'Ponte De Aldeia'"
        ."or bairro = 'Ponteda Aldeia'"
        ."or bairro = 'Zona Rural /ponte Da Aldeia'"
        ."or bairro = 'Boa Vista'"
        ."or bairro = 'Corr Boa Vista'"
        ."or bairro = 'Corrego Boa Vista'"
        ."or bairro = 'Corrego Carapina'"
        ."or bairro = 'Ponte Da Da Aldeia'"
        ."or bairro = 'Recanto Da Aldeia'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PONTE DA ALDEIA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='49' "
//        ."SET bairro='*PONTE DO EVARISTO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Ponte Do Evaristo' "
        ."or bairro = 'Ponte Evaristo' "
        ."or bairro = 'Chacara Dos Colibris' "
        ."or bairro = 'Palmital' "
        ."or bairro = 'Zona Rural Corrego Dos Coelhos' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PONTE DO EVARISTO<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='28' "
//        ."SET bairro='*PONTE DO SILVA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Corrego Ponte Do Silva' "
        ."or bairro = 'Ponte Da Silva' "
        ."or bairro = 'Ponte De Silva'"
        ."or bairro = 'Ponte Do Silva'"
        ."or bairro = 'Ponte Do Silva Zona Rural'"
        ."or bairro = 'Ponte Do Silvaa'"
        ."or bairro = 'Ponte Silva'"
        ."or bairro = 'Cgo Dos Hotts'"
        ."or bairro = 'Corrego Dos Hott'"
        ."or bairro = 'Gamelera'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com PONTE DO SILVA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='29' "
//        ."SET bairro='*POUSO ALEGRE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Pouso Alegre' "
        ."or bairro = 'Pouso Alegre' "
        ."or bairro = 'Pouso  Alegre'"
        ."or bairro = 'Darei'"
        ."or bairro = 'Fazenda Pouso Alegre'"
        ."or bairro = 'Orto'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com POUSO ALEGRE<br>";
    ?>




    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='30' "
//        ."SET bairro='*REALEZA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Realeza' "
        ."or bairro = 'Realeza Zona Rural' "
        ."or bairro = 'Rua Turmalina -realeza'"
        ."or bairro = 'Perto Do Barrigao'"
        ."or bairro = 'Turmalina'"
        ."or bairro = 'Vale Verde'"
        ."or bairro = 'Av. Jk'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com REALEZA<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='51' "
//        ."SET bairro='*ROCA GRANDE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Corrego RoCa Grande' "
        ."or bairro = 'Roca Grande' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com ROCA GRANDE<br>";
    ?>


    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='31' "
//        ."SET bairro='*SACRAMENTO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Centro Sacramento' "
        ."or bairro = 'Sacramento' "
        ."or bairro = 'Sacramento Zona Rural'"
        ."or bairro = 'Sao Sebastiao Do Sacramento'"
        ."or bairro = 'Sao Sebastiao'"
        ."or bairro = 'Sao Sebastioa'"
        ."or bairro = 'Sagramento'"
        ."or bairro = 'Corrego Sao Sebastiao '"
        ."or bairro = 'Sacremento'"
        ."or bairro = 'Saramento'"
        ."or bairro = 'Zona Rural Sacramento'"
        ."or bairro = 'Corrego Da Cachoeira'"
        ."or bairro = 'Corrego Marico'"
        ."or bairro = 'Santa Cruz'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SACRAMENTO<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='32' "
//        ."SET bairro='*SAGRADA FAMILIA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Sagrada Familia' "
        ."or bairro = 'Sacrada Familia' "
        ."or bairro = 'Sag Familia'"
        ."or bairro = 'Sagada Familia'"
        ."or bairro = 'Sagrada Famamilia'"
        ."or bairro = 'Sagrada Famila'"
        ."or bairro = 'Sagrada Familia'"
        ."or bairro = 'Sagrada Fmilia'"
        ."or bairro = 'Sagrafa Familia'"
        ."or bairro = 'Praca Antonio Brum'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SAGRADA FAMILIA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='33' "
//        ."SET bairro='*SANTA LUZIA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Santa Luzia' "
        ."or bairro = 'Sana Luzia' "
        ."or bairro = 'Sanata Luzia'"
        ."or bairro = 'Santa Luzia'"
        ."or bairro = 'Santaluzia'"
        ."or bairro = 'Santya Luzia'"
        ."or bairro = 'Sta Luzia'"
        ."or bairro = 'Santa Luiza'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SANTA LUZIA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='34' "
//        ."SET bairro='*SANTA TEREZINHA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Santa Terezinha' "
        ."or bairro = 'Bairro Santaterezinha' "
        ."or bairro = 'S Terezinha'"
        ."or bairro = 'Santa Teresinha'"
        ."or bairro = 'Santa Terezinha'"
        ."or bairro = 'Sante Terezinha'"
        ."or bairro = 'Sasnta Terezinha'"
        ."or bairro = 'Sata Terezinha'"
        ."or bairro = 'Sta Terezinha'"
        ."or bairro = 'Santa Terzinha'"
        ."or bairro = 'Santa Tetrezinha'"
        ."or bairro = 'Sta Rerezinha'"
        ."or bairro = 'Santa Ines'"
        ."or bairro = 'Santa Nterezinha'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SANTA TEREZINHA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='35' "
//        ."SET bairro='*SANTANA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'B Santana' "
        ."or bairro = 'Bairro Santana' "
        ."or bairro = 'Santana'"
        ."or bairro = 'Ssantana'"
        ."or bairro = 'Padre Fialho'"
        ."or bairro = 'Sanatana'"
        ."or bairro = 'Trevo Da Zebu'"
        ."or bairro = 'Triangulo'"
        ."or bairro = 'Rua Coronel Afonso'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SANTANA<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='36' "
//        ."SET bairro='*SANTO AMARO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Santo Amaro' "
        ."or bairro = 'Santo Amaro De Minas' "
        ."or bairro = 'Ceasa' "
        ."or bairro = 'Corrego Sao Roque' "
        ."or bairro = 'Rua Da Matriz' "
        ."or bairro = 'Santo Amarao' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SANTO AMARO<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='37' "
//        ."SET bairro='*SANTO ANTONIO' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Santo Antonio' "
        ."or bairro = 'Santo Antonio' "
        ."or bairro = 'Santoantonio'"
        ."or bairro = 'Sto Antonio'"
        ."or bairro = 'Santo Antnio'"
        ."or bairro = 'Julio Bueno'"
        ."or bairro = 'Parquinho'"
        ."or bairro = 'San To Antonio'"
        ."or bairro = 'Antonio Welerson'"
        ."or bairro = 'Antonio Wlerson'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SANTO ANTONIO<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='38' "
//        ."SET bairro='*SAO FRANCISCO DE ASSIS' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Campo Aviao' "
        ."or bairro = 'Campo De AviaCAo' "
        ."or bairro = 'Campo De Aviao'"
        ."or bairro = 'Campo Do Aviao'"
        ."or bairro = 'Bairro Francisco De Assis'"
        ."or bairro = 'Bairro SAo Francisco De Assis'"
        ."or bairro = 'Comunidade SAo Francisco De Assis'"
        ."or bairro = 'Francisco De Assis'"
        ."or bairro = 'S.francisco De Assis'"
        ."or bairro = 'Sao F. De Assis'"
        ."or bairro = 'Sao Fracisco De Assis'"
        ."or bairro = 'SAo Francisaco'"
        ."or bairro = 'Sao Francisco'"
        ."or bairro = 'Sao Francisco Assis'"
        ."or bairro = 'Sao Francisco De Asis'"
        ."or bairro = 'Sao Francisco De Asisis'"
        ."or bairro = 'Sao Francisco De Ass'"
        ."or bairro = 'Sao Francisco De Assis'"
        ."or bairro = 'Sao Franciso De Assis'"
        ."or bairro = 'SAo Frascisco Do Assis'"
        ."or bairro = 'Soa Francisco De Assis'"
        ."or bairro = 'S F Assis'"
        ."or bairro = 'S F De Assis'"
        ."or bairro = 'Sampo De Aviao'"
        ."or bairro = 'Ao Fransisco De Assis'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SAO FRANCISCO DE ASSIS<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='39' "
//        ."SET bairro='*SAO JORGE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro SAo Jorge' "
        ."or bairro = 'Sao Jorge' "
        ."or bairro = 'Sap Jorge'"
        ."or bairro = 'Pedro Faria'"
        ."or bairro = 'Sao Gorge'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SAO JORGE<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='40' "
//        ."SET bairro='*SAO PEDRO DO AVAI' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Sao Pedor Do Avai' "
        ."or bairro = 'Sao Pedro' "
        ."or bairro = 'Sao Pedro Avai'"
        ."or bairro = 'SAo Pedro De Avai'"
        ."or bairro = 'Sao Pedro Do Avai'"
        ."or bairro = 'Sao Pedro Do Avai /santa Cruz'"
        ."or bairro = 'Sao Pedro Do Avai´'"
        ."or bairro = 'Sao Pedro Do Havai'"
        ."or bairro = 'Sao Pedro Havai'"
        ."or bairro = 'Sao Pedrodo Avai'"
        ."or bairro = 'Soa Pedro Do Avai'"
        ."or bairro = 'Zona Rural Sao Pedro Do Avai'"
        ."or bairro = 'S P Do Havai'"
        ."or bairro = 'Corrego Da Tenda'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SAO PEDRO DO AVAI<br>";
    ?>



    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='41' "
//        ."SET bairro='*SAO VICENTE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro S?o Vicente' "
        ."or bairro = 'Bairro SAo Vicente' "
        ."or bairro = 'S Vicente'"
        ."or bairro = 'Sao  Vicente'"
        ."or bairro = 'Sao Vicente'"
        ."or bairro = 'Soa Vicente'"
        ."or bairro = 'Sao Vicete'"
        ."or bairro = 'Sao Voicente'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SAO VICENTE<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='42' "
//        ."SET bairro='*SOLEDADE' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Corrego Da Soledade ' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com SOLEDADE<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='43' "
//        ."SET bairro='*TODOS OS SANTOS' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Todos Os Santos' "
        ."or bairro = 'Todos Santos' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com TODOS OS SANTOS<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='50' "
//        ."SET bairro='*VILA BOA ESPERANCA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Boa Esperanca' "
        ."or bairro = 'Vila Boa EsperanCa' "
        ."or bairro = 'Vila EsperanCa'"
        ."or bairro = 'Cachoeira Sete'"
        ."or bairro = 'Corrego Dos Laias'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA BOA ESPERANCA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='44' "
//        ."SET bairro='*VILA CACHOEIRINHA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Vila Cachoeirinha' "
        ."or bairro = 'Vila Cachoerinha' "
        ."or bairro = 'Vila Cahoeirinha'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA CACHOEIRINHA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='45' "
//        ."SET bairro='*VILA DE FATIMA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Fatima' "
        ."or bairro = 'Vila De Fatima' "
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA DE FATIMA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='46' "
//        ."SET bairro='*VILA DEOLINDA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Bairro Vila Deolinda' "
        ."or bairro = 'Vila Deolinda' "
        ."or bairro = 'Vila De Olinda'"
        ."or bairro = 'Vila Delinda'"
        ."or bairro = 'Vila Diolinda'"
        ."or bairro = 'Vial Diolinda'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA DEOLINDA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='47' "
//        ."SET bairro='*VILA FORMOSA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'Fila Formosa' "
        ."or bairro = 'Via Formosa' "
        ."or bairro = 'Via Formoza'"
        ."or bairro = 'Vila Famosa'"
        ."or bairro = 'Vila Farmosa'"
        ."or bairro = 'Vila Formosa'"
        ."or bairro = 'Vila Formoza'"
        ."or bairro = 'Agrovila'"
        ."or bairro = 'Corrego Taquara Preta'"
        ."or bairro = 'Taguara Preta'"
        ."or bairro = 'Taquara Preta'"
        ."or bairro = 'Vila SAo Jose'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA FORMOSA<br>";
    ?>

    <?php
    $sql = "UPDATE mcu_sidim_pessoas_mcu "
        ."SET bairro='48' "
//        ."SET bairro='*VILA NOVA' "
        ."WHERE "
        ."municipio ='Manhuacu' and "
        ."("
        ."bairro = 'V Ila Nova' "
        ."or bairro = 'Vila Nova' "
        ."or bairro = 'Vila Nova  Centro'"
        ."or bairro = 'Vila Nova Ii'"
        ."or bairro = 'Vilanova'"
        ."or bairro = 'Cerntro'"
        ."or bairro = 'Corgo Sao Bento'"
        .") ";
    global $pdo;
    $update = $pdo->prepare($sql);
    $update->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $update = null;
    echo "feito com VILA NOVA<br>";
    ?>


    <?php
}
?>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>