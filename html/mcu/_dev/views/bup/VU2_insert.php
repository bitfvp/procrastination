<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');

//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();
?>

<main class="container"><!--todo conteudo-->
    <?php




    ///////////////////////
    if (isset($_GET['cont']) and is_numeric($_GET['cont'])){

        $temp_var1=0;
        $temp_var2=$_GET['cont'];
    }else{
        $temp_var1=0;
        $temp_var2=100;
    }

    //pessoas de sidim com cpf
    $sql = "SELECT * FROM `mcu_sidim_pessoas_mcu` where CHARACTER_LENGTH(nome) > 10 and status=0 ORDER BY nome limit {$temp_var1},{$temp_var2}";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $pessoassidim = $consulta->fetchAll();
    $contsidim = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>
    <h1>insere em mcu todos que os sidim que ainda nao estao em mcu</h1>
    <div id="general">
        <h1><span id="minuts">00</span>:<span id="segons">00</span>:<span id="ml_segons">00</span></h1>
        </p>
        <div id="tecles">
            <a href='index.php?pg=VU4_tranfere_com_nome_diferente&cont=100' class='btn btn-success m-1' onclick='cargar()' >100</a>
            <a href='index.php?pg=VU4_tranfere_com_nome_diferente&cont=1000' class='btn btn-success m-1' onclick='cargar()' >1000</a>
            <a href='index.php?pg=VU4_tranfere_com_nome_diferente&cont=10000' class='btn btn-success m-1' onclick='cargar()' >10000</a>
            <a href='index.php?pg=VU4_tranfere_com_nome_diferente&cont=20000' class='btn btn-success m-1' onclick='cargar()' >20000</a>
        </div>
    </div>
    <hr>
    <?php
    foreach ($pessoassidim as $pessoasidim){
//
        $nnnn=$pessoasidim['nome'];
        $sql = "SELECT * FROM mcu_pessoas WHERE nome LIKE '%$nnnn%'";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $contmcu = $consulta->rowCount();
        $pmcu = $consulta->fetch();
        $sql = null;
        $update = null;


        if ($contmcu==0) {
            //    fazer update no mcu
            //atualiza em mcu o que tem em sidim
            $temp_tel=" ".$pessoasidim['tel1']." ".$pessoasidim['tel2'];
            $sql = "INSERT INTO `mcu_pessoas` (`id`, `status`, `data_cadastro`, `resp_cadastro`, `cont_rank`, `nome`, `nome_social`, `sexo`, `nascimento`, `cpf`, `rg`, `uf_rg`, `nis`, `ctps`, `cod_familiar`, `responsavel_familiar`, `parentesco`, `endereco`, `numero`, `bairro`, `referencia`, `telefone`, `raca_cor`, `agente_saude`, `mae`, `pai`, `possui_veiculo`, `paga_aluguel`, `possui_propriedade`, `alfabetizado`, `renda`, `telecentro`, `curso`, `horario`, `bpc`, `pbf`, `deficiencia`, `deficiencia_desc`, `passe_livre`)"
                ." VALUES (NULL, '1', CURRENT_TIMESTAMP, '1', '0', '{$pessoasidim['nome']}', '{$pessoasidim['nome_social']}', '{$pessoasidim['sexo']}', '{$pessoasidim['nascimento']}', '{$pessoasidim['cpf']}', '', '', '', '0', '', '0', '0', '{$pessoasidim['rua']}', '{$pessoasidim['numero']}', '{$pessoasidim['bairro']}', '{$pessoasidim['complemento']}', '{$temp_tel}', '0', '', '{$pessoasidim['mae']}', '{$pessoasidim['pai']}', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '', '0');";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;


            //  apagar sidim_mcu
            $sql = "DELETE FROM mcu_sidim_pessoas_mcu "
                . "WHERE "
                . "id=:id ";
            global $pdo;
            $DELETE = $pdo->prepare($sql);
            $DELETE->bindValue('id', $pessoasidim['id']);
            $DELETE->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $DELETE = null;
            echo "APAGADO<br>";

            echo "<div class='float-left d-block border border-primary'>{";
            echo $pessoasidim['nome'] ;
            echo "<br>";
            echo " não existe em mcu ";

            echo "}</div><br><br>";


        }else{


            //
            $sql = "UPDATE mcu_sidim_pessoas_mcu SET status=1  where id=?";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $pessoasidim['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;


            echo "<div class='float-right d-block border border-danger'>{";
            echo "movido ";
            echo $pessoasidim['nome'] ;
            echo " existe em mcu ";

            echo "}</div><br><br>";
        }

    }
    ?>

    <script>
        var cronometre;



        function detenerse()
        {
            clearInterval(cronometre);
        }
        function cargar()
        {
            contador_mls = 0;
            contador_s = 0;
            contador_m = 0;
            mls = document.getElementById("ml_segons");
            s = document.getElementById("segons");
            m = document.getElementById("minuts");

            cronometre = setInterval(
                function()
                {
                    if (contador_mls ==60)
                    {
                        contador_mls = 0;
                        contador_s++;
                        s.innerHTML = contador_s;

                        if (contador_s==60)
                        {
                            contador_s = 0;
                            contador_m++;
                            m.innerHTML = contador_m;
                        }
                    }
                    mls.innerHTML = contador_mls;
                    contador_mls++;

                }
                ,16.666666);
        }
    </script>







</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>