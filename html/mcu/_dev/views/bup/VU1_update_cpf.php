<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');


//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();
?>

<main class="container"><!--todo conteudo-->
        <?php

        //quem ta em mcu que ta no sidim com cpf
        $sql = "SELECT Count(mcu_pessoas.id) FROM `mcu_pessoas` where cpf IN (SELECT cpf FROM `mcu_sidim_pessoas_mcu` where CHARACTER_LENGTH(cpf)=11)";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $contmcu_cpf_in = $consulta->fetch();
        $sql = null;
        $consulta = null;


        //pessoas de mcu com cpf
        $sql = "SELECT Count(mcu_pessoas.id) FROM `mcu_pessoas` where CHARACTER_LENGTH(cpf)=11";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $contmcu_cpf = $consulta->fetch();
        $sql = null;
        $consulta = null;


        ///////////////////////
        if (isset($_GET['cont']) and is_numeric($_GET['cont'])){

            $temp_var1=0;
            $temp_var2=$_GET['cont'];
        }else{
            $temp_var1=0;
            $temp_var2=100;
        }

        //pessoas de sidim com cpf
        $sql = "SELECT * FROM `mcu_sidim_pessoas_mcu` where CHARACTER_LENGTH(cpf)=11 and status=0 limit {$temp_var1},{$temp_var2}";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoassidim = $consulta->fetchAll();
        $contsidim = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>
    <h1>update com cpf</h1>
    <h6>pessoas na base do sidim que tem cpf:<?php echo $contsidim;?>(limitado)</h6>
    <h6>pessoas na base do mcu que tem cpf que consta em sidim:<?php echo $contmcu_cpf_in[0];?></h6>
    <h6>pessoas na base do mcu que tem cpf:<?php echo $contmcu_cpf[0];?></h6>
    <div id="general">
        <h1><span id="minuts">00</span>:<span id="segons">00</span>:<span id="ml_segons">00</span></h1>
        </p>
        <div id="tecles">
            <a href='index.php?pg=VU1_update_cpf&cont=100' class='btn btn-success m-1' onclick='cargar()' >100 (8s)</a>
            <a href='index.php?pg=VU1_update_cpf&cont=1000' class='btn btn-success m-1' onclick='cargar()' >1000 (1,20m)</a>
            <a href='index.php?pg=VU1_update_cpf&cont=10000' class='btn btn-success m-1' onclick='cargar()' >10000 (13,30m)</a>
            <a href='index.php?pg=VU1_update_cpf&cont=20000' class='btn btn-success m-1' onclick='cargar()' >20000 (20m)</a>
            <a href='index.php?pg=VU1_update_cpf&cont=50000' class='btn btn-success m-1' onclick='cargar()' >50000 (50m)</a>
        </div>
    </div>
    <hr>
    <?php
        foreach ($pessoassidim as $pessoasidim){

            $sql = "SELECT * FROM mcu_pessoas  where cpf=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $pessoasidim['cpf']);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $contmcu = $consulta->rowCount();
            $pmcu = $consulta->fetch();
            $sql = null;
            $update = null;

            if ($contmcu!=0) {
            //    fazer update no mcu
                //atualiza em mcu o que tem em sidim
                $temp_tel=" ".$pessoasidim['tel1']." ".$pessoasidim['tel2'];
                $sql = "UPDATE mcu_pessoas SET ";
                if ($pmcu['sexo']=="0"){
                    $sql.="sexo='{$pessoasidim['sexo']}', ";
                }
                if ($pmcu['nascimento']=="1900-01-01"){
                    $sql.="nascimento='{$pessoasidim['nascimento']}', ";
                }
                if ($pmcu['endereco']==""){
                    $sql.="endereco='{$pessoasidim['rua']}', numero='{$pessoasidim['numero']}', bairro='{$pessoasidim['bairro']}', referencia='{$pessoasidim['complemento']}', ";
                }
                if ($pmcu['telefone']==""){
                    $sql.="telefone = CONCAT(telefone,'{$temp_tel}'), ";
                }
                if ($pmcu['mae']==""){
                    $sql.="mae='{$pessoasidim['mae']}', ";////////////
                }
                if ($pmcu['pai']==""){
                    $sql.="pai='{$pessoasidim['pai']}', ";////////////
                }
                $sql.="cpf=:cpf1 where cpf=:cpf2";
                global $pdo;
                $update = $pdo->prepare($sql);
                $update->bindValue('cpf1', $pessoasidim['cpf']);
                $update->bindValue('cpf2', $pessoasidim['cpf']);
                $update->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $sql = null;
                $update = null;


              //  apagar sidim_mcu
                $sql = "DELETE FROM mcu_sidim_pessoas_mcu "
                    . "WHERE "
                    . "cpf =? ";
                global $pdo;
                $DELETE = $pdo->prepare($sql);
                $DELETE->bindParam(1, $pessoasidim['cpf']);
                $DELETE->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $sql = null;
                $DELETE = null;
                echo "APAGADO<br>";

                echo "<div class='float-left d-block border border-primary'>{";
                echo $pessoasidim['nome'] ;
                echo $pessoasidim['cpf'] ;
                echo "<br>";
                echo " contem ";

                echo "}</div><br><br>";


            }else{


                //
                $sql = "UPDATE mcu_sidim_pessoas_mcu SET status=1  where id=?";
                global $pdo;
                $update = $pdo->prepare($sql);
                $update->bindParam(1, $pessoasidim['id']);
                $update->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $sql = null;
                $update = null;


                echo "<div class='float-right d-block border border-danger'>{";
                echo "movido ";
                echo $pessoasidim['nome'] ;
                echo $pessoasidim['cpf'] ;
                echo " não contem ";

                echo "}</div><br><br>";
            }

        }
        ?>

    <script>
        var cronometre;



        function detenerse()
        {
            clearInterval(cronometre);
        }
        function cargar()
        {
            contador_mls = 0;
            contador_s = 0;
            contador_m = 0;
            mls = document.getElementById("ml_segons");
            s = document.getElementById("segons");
            m = document.getElementById("minuts");

            cronometre = setInterval(
                function()
                {
                    if (contador_mls ==60)
                    {
                        contador_mls = 0;
                        contador_s++;
                        s.innerHTML = contador_s;

                        if (contador_s==60)
                        {
                            contador_s = 0;
                            contador_m++;
                            m.innerHTML = contador_m;
                        }
                    }
                    mls.innerHTML = contador_mls;
                    contador_mls++;

                }
                ,16.666666);
        }
    </script>







</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>