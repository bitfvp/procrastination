<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


//bloqueado para execução
header("Location: {$env->env_url}?pg=Vlogin");
exit();
?>

<main class="container"><!--todo conteudo-->


        <?php
        $sql = "SELECT id,nome,cpf FROM `mcu_pessoas` where CHARACTER_LENGTH(cpf)=0 or CHARACTER_LENGTH(cpf)=1 or CHARACTER_LENGTH(cpf)=2";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        foreach ($pessoas as $pessoa){

            $sql = "UPDATE mcu_pessoas "
                ."SET cpf='0' "
                ."WHERE "
                ."id=? ";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $pessoa['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;
        }



        $sql = "SELECT id,nome,cpf FROM `mcu_pessoas` where CHARACTER_LENGTH(cpf)=8";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        foreach ($pessoas as $pessoa){
//            echo $pessoa['id']."   ";
//            echo $pessoa['nome']."   ";
//            echo $pessoa['cpf']."   <br>";

            $cpf="000".$pessoa['cpf'];

            $sql = "UPDATE mcu_pessoas "
                ."SET cpf=? "
                ."WHERE "
                ."id=? ";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $cpf);
            $update->bindParam(2, $pessoa['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;
        }


        $sql = "SELECT id,nome,cpf FROM `mcu_pessoas` where CHARACTER_LENGTH(cpf)=9";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        foreach ($pessoas as $pessoa){
//            echo $pessoa['id']."   ";
//            echo $pessoa['nome']."   ";
//            echo $pessoa['cpf']."   <br>";

            $cpf="00".$pessoa['cpf'];

            $sql = "UPDATE mcu_pessoas "
                ."SET cpf=? "
                ."WHERE "
                ."id=? ";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $cpf);
            $update->bindParam(2, $pessoa['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;
        }


        $sql = "SELECT id,nome,cpf FROM `mcu_pessoas` where CHARACTER_LENGTH(cpf)=10";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        foreach ($pessoas as $pessoa){
//            echo $pessoa['id']."   ";
//            echo $pessoa['nome']."   ";
//            echo $pessoa['cpf']."   <br>";

            $cpf="0".$pessoa['cpf'];

            $sql = "UPDATE mcu_pessoas "
                ."SET cpf=? "
                ."WHERE "
                ."id=? ";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $cpf);
            $update->bindParam(2, $pessoa['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;
        }

        ///////////////////////////////////////comeca sidim
        $sql = "SELECT id,nome,cpf FROM `mcu_sidim_pessoas_mcu` where CHARACTER_LENGTH(cpf)<11";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $pessoas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        foreach ($pessoas as $pessoa){


            $sql = "UPDATE mcu_sidim_pessoas_mcu "
                ."SET cpf='0' "
                ."WHERE "
                ."id=? ";
            global $pdo;
            $update = $pdo->prepare($sql);
            $update->bindParam(1, $pessoa['id']);
            $update->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $update = null;
        }
        ?>





</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>