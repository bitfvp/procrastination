<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="limpa-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
ini_set('memory_limit', '6024M');
?>

<main class="container">
    <?php
    try{
        $sql="UPDATE mcu_pessoas SET verifica=0 WHERE id > 0";
        global $pdo;
        $atualizar=$pdo->prepare($sql);
        $atualizar->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro rank pessoa '. $error_msg->getMessage();
    }
    $sql=null;
    ?>

    <i class="text-info">Todos os registros desmarcados</i>


</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>