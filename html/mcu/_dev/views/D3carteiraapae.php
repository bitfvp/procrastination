<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

//bloqueado para execução
//header("Location: {$env->env_url}?pg=Vlogin");
//exit();



for ($i = 1; $i <= 85000; $i++){
    echo $i."  ";
    $types = array( 'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf', 'txt' );
    if(is_dir("../../dados/mcu/p_apae/fotos/".$i."/")){
//        echo "A Pasta Existe";

        //titular
        if ( $handle = opendir("../../dados/mcu/p_apae/fotos/".$i."/titular/") ) {
            while ( $entry = readdir( $handle ) ) {
                $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                if( in_array( $ext, $types ) ){
                    $arquivoo = explode(".", $entry);//$entry é o arquivo
                    $extencao = end($arquivoo);
                    echo $arquivoo[0]."=====".$extencao;
                    $sql = "SELECT COUNT(`id`) FROM `mcu_p_apae_dados` where carteira='{$i}' and arquivo='{$arquivoo[0]}'";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $total = $consulta->fetch();//$total[0]
                    $sql = null;
                    $consulta = null;
                    if($total[0]==0){
                        /////////////////////////////Fazer a inclusao
                        $sql = "INSERT INTO `mcu_p_apae_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                            ." VALUES "
                            ."(NULL, CURRENT_TIMESTAMP, '1', '{$i}', '{$arquivoo[0]}', '{$extencao}', 'titular');";
                        global $pdo;
                        $update = $pdo->prepare($sql);
                        $update->execute();
                        $sql = null;
                        $update = null;
                    }else {//se for diferente é por que já esta cadastrado
                        echo "Já existe na base";
                    }
                }
            }
            closedir($handle);
        }
        //titular

        //acompanhante_1
        if ( $handle = opendir("../../dados/mcu/p_apae/fotos/".$i."/acompanhante_1/") ) {
            while ( $entry = readdir( $handle ) ) {
                $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                if( in_array( $ext, $types ) ){
                    $arquivoo = explode(".", $entry);//$entry é o arquivo
                    $extencao = end($arquivoo);
                    echo $arquivoo[0]."=====".$extencao;
                    $sql = "SELECT COUNT(`id`) FROM `mcu_p_apae_dados` where carteira='{$i}' and arquivo='{$arquivoo[0]}'";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $total = $consulta->fetch();//$total[0]
                    $sql = null;
                    $consulta = null;
                    if($total[0]==0){
                        /////////////////////////////Fazer a inclusao
                        $sql = "INSERT INTO `mcu_p_apae_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                            ." VALUES "
                            ."(NULL, CURRENT_TIMESTAMP, '1', '{$i}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_1');";
                        global $pdo;
                        $update = $pdo->prepare($sql);
                        $update->execute();
                        $sql = null;
                        $update = null;
                    }else {//se for diferente é por que já esta cadastrado
                        echo "Já existe na base";
                    }
                }
            }
            closedir($handle);
        }
        //acompanhante_1

        //acompanhante_2
        if ( $handle = opendir("../../dados/mcu/p_apae/fotos/".$i."/acompanhante_2/") ) {
            while ( $entry = readdir( $handle ) ) {
                $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                if( in_array( $ext, $types ) ){
                    $arquivoo = explode(".", $entry);//$entry é o arquivo
                    $extencao = end($arquivoo);
                    echo $arquivoo[0]."=====".$extencao;
                    $sql = "SELECT COUNT(`id`) FROM `mcu_p_apae_dados` where carteira='{$i}' and arquivo='{$arquivoo[0]}'";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $total = $consulta->fetch();//$total[0]
                    $sql = null;
                    $consulta = null;
                    if($total[0]==0){
                        /////////////////////////////Fazer a inclusao
                        $sql = "INSERT INTO `mcu_p_apae_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                            ." VALUES "
                            ."(NULL, CURRENT_TIMESTAMP, '1', '{$i}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_2');";
                        global $pdo;
                        $update = $pdo->prepare($sql);
                        $update->execute();
                        $sql = null;
                        $update = null;
                    }else {//se for diferente é por que já esta cadastrado
                        echo "Já existe na base";
                    }
                }
            }
            closedir($handle);
        }
        //acompanhante_2

        //arquivos
        if ( $handle = opendir("../../dados/mcu/p_apae/fotos/".$i."/arquivos/") ) {
            while ( $entry = readdir( $handle ) ) {
                $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                if( in_array( $ext, $types ) ){
                    $arquivoo = explode(".", $entry);//$entry é o arquivo
                    $extencao = end($arquivoo);
                    echo $arquivoo[0]."=====".$extencao;
                    $sql = "SELECT COUNT(`id`) FROM `mcu_p_apae_dados` where carteira='{$i}' and arquivo='{$arquivoo[0]}'";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $total = $consulta->fetch();//$total[0]
                    $sql = null;
                    $consulta = null;
                    if($total[0]==0){
                        /////////////////////////////Fazer a inclusao
                        $sql = "INSERT INTO `mcu_p_apae_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                            ." VALUES "
                            ."(NULL, CURRENT_TIMESTAMP, '1', '{$i}', '{$arquivoo[0]}', '{$extencao}', 'arquivos');";
                        global $pdo;
                        $update = $pdo->prepare($sql);
                        $update->execute();
                        $sql = null;
                        $update = null;
                    }else {//se for diferente é por que já esta cadastrado
                        echo "Já existe na base";
                    }
                }
            }
            closedir($handle);
        }
        //arquivos


    } else {
        echo "<i class='text-muted'>A Pasta não Existe</i>";
    }

    echo "<br>";
}
?>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>