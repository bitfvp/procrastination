<?php
function fncatividadelist(){
    $sql = "SELECT * FROM mcu_pb_atlista ORDER BY atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $atividadelista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $atividadelista;
}

function fncgetatividade($id){
    $sql = "SELECT * FROM mcu_pb_atlista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getatividade = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getatividade;
}
