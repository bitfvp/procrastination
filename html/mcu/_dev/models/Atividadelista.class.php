<?php
class Atividadelista{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncatividadelistanew($atividade,$pb,$pe,$ct,$bf,$aabb){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_pb_atlista ";
                $sql.="(id, atividade, pb, pe, ct, bf, aabb)";
                $sql.=" VALUES ";
                $sql.="(NULL, :atividade, :pb, :pe, :ct, :bf, :aabb)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":atividade", $atividade);
                $insere->bindValue(":pb", $pb);
                $insere->bindValue(":pe", $pe);
                $insere->bindValue(":ct", $ct);
                $insere->bindValue(":bf", $bf);
                $insere->bindValue(":aabb", $aabb);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vat_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncatividadelistaedit($id,$atividade,$pb,$pe,$ct,$bf,$aabb){

        //inserção no banco
        try{
            $sql="UPDATE mcu_pb_atlista SET atividade=:atividade, pb=:pb, pe=:pe, ct=:ct, bf=:bf, aabb=:aabb WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":atividade", $atividade);
            $insere->bindValue(":pb", $pb);
            $insere->bindValue(":pe", $pe);
            $insere->bindValue(":ct", $ct);
            $insere->bindValue(":bf", $bf);
            $insere->bindValue(":aabb", $aabb);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vat_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>