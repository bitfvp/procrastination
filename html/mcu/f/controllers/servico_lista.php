<?php

function fncservicolist(){
    $sql = "SELECT * FROM mcu_fila_servico ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $servicolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $servicolista;
}

function fncgetservico($id){
    $sql = "SELECT * FROM mcu_fila_servico WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getservico = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getservico;
}
