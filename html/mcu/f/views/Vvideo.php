<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_76"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

$sql = "SELECT * FROM mcu_fila_video WHERE id=1";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$video = $consulta->fetch();
$sql=null;
$consulta=null;

?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vvideo&aca=savevideo" method="post">
            <div class="row">
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <label for="video">VIDEO:</label>
                    <input autocomplete="off" id="video" autofocus type="text" class="form-control" name="video" placeholder="url do video..." value="<?php echo $video['video']?>" required />
                </div>

                <div class="col-md-6">
                    <label for="volume">VOLUME:</label>
                    <input id="volume" type="number" class="form-control" name="volume" min="0" max="100" value="<?php echo $video['volume']?>" />
                </div>

                <div class="col-md-12">
                    <label for="status">STATUS:</label>
                    <select name="status" id="status" class="form-control" required>// vamos criar a visualização

                        <option selected=""
                                value="<?php if ($video['status'] == "" or $video['status'] == 0) {
                            echo 0;
                        } else {
                                    echo 1;
                        } ?>">

                            <?php if ($video['status'] == "" or $video['status'] == 0) {
                                echo "DESATIVADO";
                            } else {
                                echo "ATIVO";
                            } ?>
                        </option>
                            <option value="0">
                                DESATIVADO
                            </option>
                        <option value="1">
                            ATIVO
                        </option>
                    </select>
                </div>

            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>