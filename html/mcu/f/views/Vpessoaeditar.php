<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_76"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="filapessoasave";
    $pessoa=fncgetfilapessoa($_GET['id']);
}else{
    $a="filapessoanew";
}
?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vhome&aca=<?php echo $a;?>" method="post">
            <div class="row">
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <input id="id" type="hidden" class="" name="id" value="<?php echo $pessoa['id']; ?>"/>
                    <label for="nome">NOME:</label>
                    <input autocomplete="off" id="nome" autofocus type="text" class="form-control" name="nome" placeholder="Digite o nome da pessoa..." value="<?php echo $pessoa['nome']?>" required />
                </div>

                <div class="col-md-6">
                    <label for="obs">OBS:</label>
                    <input autocomplete="off" id="obs" type="text" class="form-control" name="obs" placeholder="Relevante ao atendimento" value="<?php echo $pessoa['obs']?>" />
                </div>

                <div class="col-md-12">
                    <label for="servico">SERVIÇO:</label>
                    <select name="servico" id="servico" class="form-control" required>// vamos criar a visualização

                        <option selected="" value="<?php if ($pessoa['servico'] == "") {
                            $z = "";
                            echo $z;
                        } else {
                            echo $pessoa['servico'];
                        } ?>">
                            <?php
                            echo fncgetservico($pessoa['servico'])['servico'];
                            ?>
                        </option>

                        <?php
                        foreach (fncservicolist() as $item) {
                            ?>
                            <option data-tokens="<?php echo $item['servico'];?>" value="<?php echo $item['id'];?>">
                                <?php echo $item['servico']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="col-md-12">
                    <label for="preferencial">PREFERENCIAL:</label>
                    <select name="preferencial" id="preferencial" class="form-control">// vamos criar a visualização de sexo
                        <option selected="" value="<?php if ($pessoa['preferencial'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pessoa['preferencial'];
                        } ?>">
                            <?php
                            if ($pessoa['preferencial'] == 0) {
                                echo "Normal";
                            }
                            if ($pessoa['preferencial'] == 1) {
                                echo "Prioritário";
                            }
                            ?>
                        </option>
                        <option value="0">Normal</option>
                        <option value="1">Prioritário</option>

                    </select>
                </div>

            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>