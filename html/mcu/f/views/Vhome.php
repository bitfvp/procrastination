<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_75"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}



$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '100;URL={$env->env_url_mod}'>";


if (isset($_SESSION["guiche"]) and is_numeric($_SESSION["guiche"]) and $_SESSION["logado"]>0 and $_SESSION["logado"]<100 and isset($_SESSION["servico"]) and is_numeric($_SESSION["servico"]) and isset($_SESSION["servico_todos"]) and is_numeric($_SESSION["servico_todos"])) {
//TUDO OK
}else{
    //validação se esta logado
    header("Location: index.php?pg=Vguiche");
    exit();
}

try{
    $sql = "SELECT * from mcu_fila_senha WHERE id=1 ";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$sss=$consulta->fetch();
$senha=$sss['numero'];
$chamado=$sss['chamado'];


if ($_SESSION["guiche"]==99 or $_SESSION['servico_todos']==1){
    try{
        $sql = "SELECT * from mcu_fila_pessoa ORDER BY id DESC ";
        global $pdo;
        $people=$pdo->prepare($sql);
        $people->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $pessoas=$people->fetchAll();
    $tr=$people->rowCount();// verifica o número total de registros
    $tr=$tr-1;
}else{
    try{
        $sql = "SELECT * from mcu_fila_pessoa where servico=? ORDER BY id DESC ";
        global $pdo;
        $people=$pdo->prepare($sql);
        $people->bindParam(1,$_SESSION['servico']);
        $people->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $pessoas=$people->fetchAll();
    $tr=$people->rowCount();// verifica o número total de registros
}

?>

<main class="container">
<div class="row">
    <div class="col-md-2">
        <?php
    if($allow["allow_76"]==1) {
        ?>
        <a href="index.php?pg=Vpessoaeditar" class="btn btn-lg btn-success btn-block">
            NOVO
        </a>
        <?php
    }
    ?>
    </div>
    <div class="col-md-2">
        <a href="index.php?pg=Vhome&aca=novasenha" class="btn btn-lg btn-warning btn-block">
            CHAMAR
        </a>
    </div>
    <div class="col-md-2">
        <a href="index.php?pg=Vhome&aca=novasenhaprioritario" class="btn btn-lg btn-danger btn-block">
            PRIORITÁRIO
        </a>
    </div>
    <div class="col-md-2">
        <a href="index.php?pg=Vhome&aca=repetir" class="btn btn-lg btn-warning btn-block">
            REPETIR
        </a>
    </div>

    <div class="col-md-4 text-right">
        <label class="badge badge-dark">
            <?php
            switch ($_SESSION['guiche']){
                case 1:
                    echo "Guichê 1";
                    break;
                case 2:
                    echo "Guichê 2";
                    break;
                case 3:
                    echo "Guichê 3";
                    break;
                case 4:
                    echo "Guichê 4";
                    break;
                case 5:
                    echo "Guichê 5";
                    break;
                case 6:
                    echo "Guichê 6";
                    break;
                case 7:
                    echo "Guichê 7";
                    break;
                case 8:
                    echo "Guichê 8";
                    break;
                case 9:
                    echo "Guichê 9";
                    break;
                case 10:
                    echo "Guichê Especial";
                    break;
                case 11:
                    echo "Sala 1";
                    break;
                case 12:
                    echo "Sala 2";
                    break;
                case 13:
                    echo "Sala 3";
                    break;
                case 14:
                    echo "Sala 4";
                    break;
                case 15:
                    echo "Sala 5";
                    break;
                case 50:
                    echo "Atendimento Expresso";
                    break;
                case 99:
                    echo "Recepção";
                    break;
            }
            ?>
        </label><br>
        <label class="badge badge-dark">
            <?php
            echo fncgetservico($_SESSION['servico'])['servico'];
            ?>
        </label>
    </div>

</div>
<!-- ====================================================== -->
    <br>
    <table class="table table-hover table-sm ">
        <thead class="thead-light">
                <tr>
                    <th scope="col">NOME</th>
                    <th scope="col">PRIORIDADE</th>
                    <th scope="col">SERVIÇO</th>
                    <th scope="col">STATUS</th>
                    <th>PROFISSIONAL</th>
                    <th scope="col"><strong class="fa fa-edit"></strong></th>
                </tr>
                </thead>
                <tfoot class="">
                <tr>
                    <td colspan="7" class="text-right"><?php echo $tr;?> Pessoa(s) listada(s)</td>
                </tr>
                </tfoot>

        <tbody>
                <?php
                function fncgetfakenome (){
                    $fakes = [
                        '0' => 'Pessoa aguardando',
                        '1' => 'Raul Seixas',
                        '2' => 'Getulio Vargas',
                        '3' => 'Michel Jackson',
                        '4' => 'Tim Maia',
                        '5' => 'José Rico',
                        '6' => 'Elvis',
                        '7' => 'Leonel Brizola',
                        '8' => 'Enéas Carneiro',
                        '9' => 'Ayrton Senna',
                        '10' => 'Mestre Miyage ',
                        '11' => 'Mortícia Addams',
                        '12' => 'Tio Phil',
                        '13' => 'Roque Santeiro',
                        '14' => 'Paul McCartney',
                        '15' => 'Giovanni Improtta',
                        '16' => 'Cazuza',
                        '17' => 'Freddie Mercury',
                        '18' => 'Luiz Gonzaga',
                        '19' => 'Gugu Liberato',
                        '20' => 'Pablo Escobar',
                        '21' => 'Mussum',
                        '22' => 'Zacarias',
                        '23' => 'Princesa Diana',
                        '24' => 'Noé',
                        '25' => 'Cristovão Colombo',
                        '26' => 'Roberto Bolaños',
                        '27' => 'Dom Ramón Valdés',
                        '28' => 'Godinez',
                        '29' => 'Jaiminho',
                        '30' => 'Dona Clotilde',
                        '31' => 'Professor Girafales',
                        '32' => 'Serafim Tibúrcio',
                        '33' => 'Xica da Silva',
                        '34' => 'Vito Corleone',
                        '35' => 'Rei Leônidas',
                        '36' => 'Nazaré Tedesco',
                        '37' => 'Odete Roitman',
                        '38' => 'Paola Bracho',
                        '39' => 'Maria Mercedes',
                        '40' => 'Marisol',
                        '41' => 'Sinhozinho Malta',
                        '42' => 'Odorico Paraguaçu',
                        '43' => 'Viúva Porcina',
                        '44' => 'Hilda Furacão',
                        '45' => 'Agostinho Carrara',
                        '46' => 'Beto Carrero',
                        '47' => 'Chico Anysio',
                        '48' => 'Ronald Golias',


                    ];
                    return $fakes[rand(0,0)];
//                    return $fakes[rand(1,48)];
                }

                $tm1 = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));

                foreach ($pessoas as $pa){
                    $comp_data=explode(" ",$pa['data_ts']);
                    $comp_data=$comp_data[0];
                    $comp_data=explode("-",$comp_data);//[0],[1],[2] ano mes e dia

                    $comp_hora=explode(" ",$pa['data_ts']);
                    $comp_hora=$comp_hora[1];
                    $comp_hora=explode(":",$comp_hora);//[0],[1],[2] hora minuto e segundo

                    $tm2 = mktime($comp_hora[0], $comp_hora[1], $comp_hora[2], $comp_data[1], $comp_data[2], $comp_data[0]);

                    $tm3 = $tm1-$tm2;

                    if ($tm3<=59){
                        $tm3=number_format($tm3,0);
                        $tm3.="s ";
                    }

                    if ($tm3>59 and $tm3< 3600){
                        $tm3=$tm3/60;
                        $tm3=number_format($tm3,0);
                        $tm3.="m ";
                    }

                    if ($tm3>3599){
                        $tm3=(($tm3/60)/60);
                        $tm3=number_format($tm3,0);
                        $tm3.="H ";
                    }

                    switch ($pa['prof']){
                        case 0:
                            $fila_user ="-----";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 17:
                            $fila_user ="Geisla De Souza Pereira";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 35:
                            $fila_user ="Anderson Jose Knupp";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 44:
                            $fila_user ="Cibele Cristina Ciconha Da Silva";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 98:
                            $fila_user ="Alessandro Fernandes De Abreu";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 99:
                            $fila_user ="Debora Barbosa Da Silva Almeida";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 100:
                            $fila_user ="Ana Carolina De Oliveira Moreira";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 101:
                            $fila_user ="Simone Pereira De Souza Lacerda";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 102:
                            $fila_user ="Maria De Fatima Da Mercere Pereira";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 103:
                            $fila_user ="Jamila De Oliveira";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 104:
                            $fila_user ="Maria Aparecida Gomes";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 105:
                            $fila_user ="Nayara Carvalho Silva Breder";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 106:
                            $fila_user ="Katia De Oliveira";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        case 107:
                            $fila_user ="Lara Cardeiro Hott";
                            $p_nome = explode(" ", $fila_user);
                            break;
                        default:
                            $sql = "SELECT nome FROM tbl_users WHERE id = $pa[prof]";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $fila_user = $consulta->fetch();
                            $sql=null;
                            $consulta=null;
                            $p_nome = explode(" ", $fila_user['nome']);
                            break;
                    }

                    //cor da linha se for seu atendimento
                    if ($pa['servico']==$_SESSION['servico']){
                        $corline = " font-weight-bold mark ";
                    }else{
                        $corline = " ";
                    }


                    $espera="<i class='small text-info float-right mr-1' title='Tempo de espera'> <i class='fas fa-clock'></i> {$tm3}</i>";
                    $corcontraste = "";
                    //troca as cores se for chamado
                    if ($pa['chamado']==1){
                        $corline = "bg-dark";
                        $corcontraste = "text-warning";
                        $espera='';
                    }

                    if($pa['chamado']==1){
                        $status = "<span class='text-success'>Atendido</span>";
                    }else{
                        if ($senha==$pa['id'] and  $pa['chamado']!=1){
                            $status = "<span class='text-info'>Aguardando tela...</span>";
                        }else{
                            $status = "<span class='text-danger'>Não chamado</span>";
                        }
                    }

                    //verifica se é 99 recepcao mostra os nomes proximos, se não é pucha tabela fake
                    $mostrarpessoa=0;
                    if ($pa['chamado']==1 or $_SESSION["guiche"]==99){
                        $mostrarpessoa=1;
                    }
                    $mostrarpessoa=1;//força mostrar
                    ?>

                    <tr class="<?php echo $corline;?>">
                        <td class="<?php echo $corcontraste;?>">
                            <?php if ($mostrarpessoa==1){
                                echo $pa['nome']." <i><strong>".$pa['obs']."</strong></i>";
                            }else{
                                echo fncgetfakenome()."<i class='float-right text-danger' title='Nome real ocultado!'>(?)</i>";
                            }  ?>

                            <?php
                                echo $espera;
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($pa['preferencial']==1){
                                echo "<span class='badge badge-warning'><i class='fa fa-universal-access fa-2x'>Preferencial</i></span>";
                            }else{
                                echo "<span class='badge badge-info'>Normal</span>";
                            }
                            ?>
                        </td>
                        <td class="<?php echo $corcontraste;?>">
                            <?php
                            if ($pa['servico'] != 0 and $pa['servico'] != 99) {
                                switch ($pa['servico']) {
//                                    case 1:
//                                        echo "Bolsa Família";
//                                        break;
//                                    case 2:
//                                        echo "CADUNICO";
//                                        break;
                                    default:
                                        echo fncgetservico($pa['servico'])['servico'];
                                        break;
                                }
                            } else {
                                echo "-----";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $status;
                            ?>
                        </td>
                        <td class="<?php echo $corcontraste;?>"><?php echo $p_nome[0]; ?></td>

                        <th scope="row" id="<?php echo $id;  ?>" class="<?php echo $corcontraste;?>">
                            <i class="badge badge-info">
                            <?php
                            if ($mostrarpessoa==1){
                                echo "<a href='index.php?pg=Vpessoaeditar&id=".$pa['id']."' class='fa fa-pen text-dark'></a>";
                            }else{
                                echo "<i class='fas fa-ban'></i>";
                            }
                            echo $pa['id'];

                            if (isset($_SESSION["servico"]) and is_numeric($_SESSION["servico"]) and $pa['chamado']!=1 and ($_SESSION["servico"]!=$pa['servico'])) {
                                ?>
                                <a href="index.php?pg=Vhome&aca=novasenha&s=<?php echo $pa['id']; ?>" class="fas fa-bullhorn text-dark"></a>
                                <?php
                            }else{

                                //alterar no futuro
                            ?>
                                <a href="index.php?pg=Vhome&aca=novasenha&s=<?php echo $pa['id']; ?>" class="fas fa-bullhorn text-dark"></a>
                                <?php
                            }
                            ?>
                            </i>
                        </th>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
