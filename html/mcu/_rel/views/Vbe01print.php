<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Beneficio relatório-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";
    $beneficio = $_POST['beneficio'];
    $condicao = $_POST['condicao'];
    $ordem = $_POST['ordem'];
    $quem_recebeu = $_POST['quem_recebeu'];


    $sql_base= "SELECT\n"
        . "mcu_beneficio.id,\n"
        . "mcu_beneficio.pessoa,\n"
        . "mcu_beneficio.beneficio,\n"
        . "mcu_beneficio.quantidade,\n"
        . "mcu_beneficio.data_pedido,\n"
        . "mcu_beneficio.data_ts,\n"
        . "mcu_beneficio.condicao,\n"
        . "mcu_beneficio.descricao,\n"
        . "mcu_beneficio.profissional,\n"
        . "mcu_beneficio.entregue,\n"
        . "mcu_beneficio.data_entrega,\n"
        . "mcu_beneficio.quem_recebeu,\n"
        . "mcu_beneficio.imprimir, \n"
        . "mcu_pessoas.nome, \n"
        . "mcu_pessoas.cpf, \n"
        . "mcu_bairros.bairro \n"
        . "FROM \n"
        . "mcu_beneficio \n"
        . "INNER JOIN mcu_pessoas ON mcu_beneficio.pessoa = mcu_pessoas.id\n"
        . "INNER JOIN tbl_users ON mcu_beneficio.profissional = tbl_users.id\n"
        . "LEFT JOIN mcu_bairros ON mcu_pessoas.bairro = mcu_bairros.id \n";

    $sql_where="where \n"
        . "mcu_beneficio.entregue=1 and \n"
        . "mcu_beneficio.data_entrega >= '".$inicial."' and \n"
        . "mcu_beneficio.data_entrega < '".$final."' and \n"
        . "mcu_beneficio.quem_recebeu LIKE '%$quem_recebeu%' and \n"
        . "mcu_beneficio.beneficio=".$beneficio." ";
    if (isset($_POST['condicao']) and is_numeric($_POST['condicao']) and $_POST['condicao']>0){
        $sql_where.= 'and mcu_beneficio.condicao='.$_POST['condicao'].' ';
    }

    $temp_ordem="ORDER BY ";
    if ($_POST['ordem']=='1'){//Data da entrega
        $temp_ordem.='mcu_beneficio.data_entrega ASC';
        $iifo='Data da entrega';
    }
    if ($_POST['ordem']=='2'){//Bairro
        $temp_ordem.='mcu_bairros.bairro ASC';
        $iifo='Bairro';
    }
    if ($_POST['ordem']=='3'){//Beneficiário
        $temp_ordem.='mcu_pessoas.nome ASC';
        $iifo='Beneficiário';
    }
    if ($_POST['ordem']=='4'){//Profissional
        $temp_ordem.='tbl_users.nome ASC';
        $iifo='Profissional';
    }
    $sql = $sql_base.$sql_where.$temp_ordem;
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $bnfcs1 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;


            $sql_base= "SELECT \n"
            . "Sum(mcu_beneficio.quantidade) AS quant, \n"
            . "mcu_bairros.bairro \n"
            . "FROM \n"
            . "mcu_pessoas \n"
            . "INNER JOIN mcu_beneficio ON mcu_pessoas.id = mcu_beneficio.pessoa\n"
            . "INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro\n";
            $sql_where="where \n"
            . "mcu_beneficio.entregue=1 and \n"
            . "mcu_beneficio.data_entrega >= '".$inicial."' and \n"
            . "mcu_beneficio.data_entrega < '".$final."' and \n"
            . "mcu_beneficio.quem_recebeu LIKE '%$quem_recebeu%' and \n"
            . "mcu_beneficio.beneficio=".$beneficio." ";
            if (isset($_POST['condicao']) and is_numeric($_POST['condicao']) and $_POST['condicao']>0){
                $sql_where.= 'and mcu_beneficio.condicao='.$_POST['condicao'].' ';
            }
            $temp_group="GROUP BY mcu_bairros.bairro ";
            $temp_ordem="ORDER BY quant DESC";
            $sql = $sql_base.$sql_where.$temp_group.$temp_ordem;
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $bnfcs2 = $consulta->fetchAll();
            $sql = null;
            $consulta = null;


    $sql_base= "SELECT\n"
    . "SUM(quantidade) AS contadora,\n"
    . "tbl_users.nome AS proff\n"
    . "FROM\n"
    . "mcu_beneficio\n"
    . "INNER JOIN tbl_users ON tbl_users.id = mcu_beneficio.profissional\n";
    $sql_where="where \n"
    . "mcu_beneficio.entregue=1 and \n"
    . "mcu_beneficio.data_entrega >= '".$inicial."' and \n"
    . "mcu_beneficio.data_entrega < '".$final."' and \n"
    . "mcu_beneficio.quem_recebeu LIKE '%$quem_recebeu%' and \n"
    . "mcu_beneficio.beneficio=".$beneficio." ";
    if (isset($_POST['condicao']) and is_numeric($_POST['condicao']) and $_POST['condicao']>0){
        $sql_where.= 'and mcu_beneficio.condicao='.$_POST['condicao'].' ';
    }
    $temp_group="GROUP BY tbl_users.nome ";
    $temp_ordem="ORDER BY contadora desc";
    $sql = $sql_base.$sql_where.$temp_group.$temp_ordem;
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $bnfcs3 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h3>RELATÓRIO DE <?php echo fncgetbetipo($beneficio)['tipo'];?></h3>
        <h6>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h6>
        <h6>Gerado em: <?php echo date('d/m/Y');?></h6>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>ENTREGA&nbsp;</td>
                <td>Q.&nbsp;</td>
                <td>BENEFICIÁRIO&nbsp;</td>
                <td>CPF&nbsp;</td>
                <td>PROFISSIONAL </td>
                <td>BAIRRO&nbsp;</td>
                <td>CONDIÇÃO&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalcb = 0;
            foreach ($bnfcs1 as $be) {
                $proff=fncgetusuario($be['profissional'])['nome'];
                $proff = explode(" ", $proff);
                ?>
            <tr>
                <td><?php echo dataBanco2data($be['data_entrega']); ?>&nbsp;</td>
                <td><?php echo $be['quantidade'];
                    $totalcb = $totalcb + $be['quantidade']; ?>&nbsp;
                </td>
                <td><?php echo $be['nome']; ?>&nbsp;</td>
                <td><?php echo $be['cpf']; ?>&nbsp;</td>
                <td><?php echo $proff[0]." ".end($proff); ?>&nbsp;</td>
                <td><?php echo $be['bairro']; ?>&nbsp;</td>
                <td><?php echo fncgetbecondicao($be['condicao'])['condicao']; ?>&nbsp;</td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <h4>TOTAL DE <?php echo fncgetbetipo($beneficio)['tipo'].": ".$totalcb; ?></h4>

        <div id="piechart" style="width: auto; height: 900px;"></div>
        <div id="chart_div" style="height: 1500px;"></div>
    </div>
</main>
</body>
</html>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Bairro', 'Quantidade',],
                <?php
                foreach ($bnfcs2 as $gf) {
                    echo "['{$gf['quant']} {$gf['bairro']}', {$gf['quant']}],\n";
                }
            ?>
        ]);

        var options = {
            title: 'VOLUME DE <?php echo fncgetbetipo($beneficio)['tipo'];?> POR BAIRRO',
            chartArea: {width: '50%'},
            hAxis: {
                title: '<?php echo $totalcb;?>',
                minValue: 0,
            },
            vAxis: {
                title: 'Bairros',
                textStyle : {
                    fontSize: 11 // or the number you want
                },
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['pessoa', 'Quant'],
            <?php

            foreach ($bnfcs3 as $gf) {
                $proff = explode(" ", $gf['proff']);
                $proff = $proff[0]." ".end($proff);
                echo '[\''.$gf['contadora'].' '.$proff.'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);
        //opções para o gráfico pizza
        var options3 = {
            title: 'Grafico por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>