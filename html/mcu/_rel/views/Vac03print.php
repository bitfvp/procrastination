<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_12"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatório de atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

    $sql = "SELECT DISTINCT mcu_pessoas.cod_familiar, mcu_pessoas.bairro \n"
        . "FROM mcu_pessoas \n"
        . "WHERE \n"
        . "(\n"
            . "mcu_pessoas.id IN (SELECT pessoa from mcu_pb_at where mcu_pb_at.tipo = 1 AND mcu_pb_at.`status` = 1 and mcu_pb_at.data>=:inicial and mcu_pb_at.data<=:final) \n"
            . "or \n"
            . "mcu_pessoas.id IN (SELECT pessoa from mcu_beneficio where mcu_beneficio.beneficio=1 and mcu_beneficio.data_pedido>=:inicial and mcu_beneficio.data_pedido<=:final) \n"
        . ") and mcu_pessoas.cod_familiar<>0 and mcu_pessoas.cod_familiar<>'' \n"
        . "ORDER BY mcu_pessoas.bairro ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
//$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoas = $consulta->fetchAll();
    $pessoascont = $consulta->rowCount()    ;
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <h3>Relatório de famílias acompanhadas</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>

        <?php

        foreach ($pessoas as $pessoa){
            if ($pessoa['cod_familiar']!=0 and $pessoa['cod_familiar']!="" and $pessoa['cod_familiar']!=null) {
                $bairro = fncgetbairro($pessoa['bairro'])['bairro'];
                echo "<table class='table table-sm'>";
                echo "<thead class='thead-dark'>";
                echo "<tr>";
                echo "<th>NOME</th>";
                echo "<th>NASCIMENTO</th>";
                echo "<th>CPF</th>";
                echo "<th>CF</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";

                $sql = "SELECT mcu_pessoas.nome, mcu_pessoas.nascimento, mcu_pessoas.cpf, mcu_pessoas.cod_familiar \n"
                    . "FROM mcu_pessoas \n"
                    . "WHERE mcu_pessoas.cod_familiar = :cod_familiar ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":cod_familiar", $pessoa['cod_familiar']);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $familia = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
                foreach ($familia as $fam) {
                    echo "<tr>";
                    echo "<td>";
                    echo $fam['nome'];
                    echo "</td>";

                    echo "<td>";
                    echo dataBanco2data($fam['nascimento']);
                    echo "</td>";

                    echo "<td>";
                    echo $fam['cpf'];
                    echo "</td>";

                    echo "<td>";
                    echo $fam['cod_familiar'];
                    echo "</td>";

                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
        }

        echo "<i>{$pessoascont} Famílias acompanhadas</i>";
        ?>



</div>
<fieldset>
<!--    <div style="page-break-after: always;"></div>-->
    <div id="chart_div" style="height: 1500px;"></div>
</fieldset>
</body>
</html>
<?php
$inicial = $_POST['data_inicial']." 00:00:01";
$final = $_POST['data_final']." 23:59:59";

$sql = "SELECT Count(DISTINCT mcu_pessoas.cod_familiar) AS contadora, \n"
    . "mcu_pessoas.bairro\n"
    . "FROM mcu_pessoas \n"
    . "WHERE \n"
    . "(\n"
    . "mcu_pessoas.id IN (SELECT pessoa from mcu_pb_at where mcu_pb_at.tipo = 1 AND mcu_pb_at.`status` = 1 and mcu_pb_at.data>=:inicial and mcu_pb_at.data<=:final) \n"
    . " or \n"
    . "mcu_pessoas.id IN (SELECT pessoa from mcu_beneficio where mcu_beneficio.beneficio=1 and mcu_beneficio.data_pedido>=:inicial and mcu_beneficio.data_pedido<=:final) \n"
    . ") and mcu_pessoas.cod_familiar<>0 and mcu_pessoas.cod_familiar<>'' \n"
    . "GROUP BY\n"
    . "mcu_pessoas.bairro\n"
    . "ORDER BY\n"
    . "contadora desc";

    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $graf1 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);
    function drawBasic() {
        var data = google.visualization.arrayToDataTable([
            ['Bairro', 'Quantidade',],
            <?php
            $cont=0;
            foreach ($graf1 as $gf) {
                $bairro=fncgetbairro($gf['bairro'])['bairro'];
                $cont=$cont+$gf['contadora'];
                echo "['{$gf['contadora']} {$bairro}', {$gf['contadora']}],\n";
            }
            ?>
        ]);
        var options = {
            title: 'Gráfico de famílias acompanhadas por bairro',
            chartArea: {width: '50%'},
            hAxis: {
                title: '<?php echo $cont;?> Familias acompanhadas',
                minValue: 0,
            },
            vAxis: {
                title: 'Bairros',
                textStyle : {
                    fontSize: 11 // or the number you want
                },
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
