<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_12"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Registro mensal de atendimentos do CRAS-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial_a=$_POST['data_inicial'];
$inicial_b=$inicial_a." 00:00:00";
$final_a=$_POST['data_final'];
$final_b=$final_a." 23:59:59";

//a2
/////////////////////////////////////////////////
$sql = "SELECT Count(mcu_pessoas.cod_familiar) AS quantidade,mcu_pessoas.cod_familiar \n"
    . "FROM mcu_pessoas INNER JOIN mcu_pb_at ON mcu_pessoas.id = mcu_pb_at.pessoa \n"
    . "WHERE mcu_pb_at.data>=:inicial and mcu_pb_at.data<=:final AND \n"
    . "mcu_pb_at.atividade = 18 AND \n"
    . "mcu_pb_at.`status` = 1 AND \n"
    . "mcu_pb_at.tipo = 1 GROUP BY mcu_pessoas.cod_familiar";
global $pdo;
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$a2list = $consulta->fetchAll();
$a2 = $consulta->rowCount();
$sql=null;
$consulta=null;

//$cc=0;
//foreach ($a2list as $aaa){
//    $cc++;
//    echo $cc."-".$aaa[0]."///".$aaa[1]."///".$aaa[2]."<br>";
//}
//echo $a2;

/////////////////////////////////////////////////
//a2



//c1
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=2) and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c1 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c1

//c2
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=4) and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c2 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c2

//c3
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=34) and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c3 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c3

//c4
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=3) and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c4 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c4

//c5
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=22)  and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c5 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c5

//c6
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=12)  and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_b);
$consulta->bindValue(":final",$final_b);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c6 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c6

//c8
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Count(mcu_pb_at.id) AS quantidade \n"
    . "FROM \n"
    . "mcu_pb_at \n"
    . "WHERE ((mcu_pb_at.data>=:inicial) and (mcu_pb_at.data<:final) and (mcu_pb_at.atividade=33) and (mcu_pb_at.tipo=1) and (mcu_pb_at.status=1) )";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial_b);
    $consulta->bindValue(":final",$final_b);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $c8 = $consulta->fetch();
    $sql=null;
    $consulta=null;
/////////////////////////////////////////////////
//c8

//c9
/////////////////////////////////////////////////
$sql = "SELECT  \n"
    . "Sum(mcu_beneficio.quantidade) AS quantidade \n"
    . "FROM \n"
    . "mcu_beneficio \n"
    . "WHERE ((mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final) and (mcu_beneficio.beneficio=1) and (mcu_beneficio.entregue=1) )";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial_a);
$consulta->bindValue(":final",$final_a);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$c9 = $consulta->fetch();
$sql=null;
$consulta=null;
/////////////////////////////////////////////////
//c9
?>
<div class="container-fluid">
    <h3>Registro mensal de atendimentos do CRAS</h3>
    <h5>Período escolhido: <?php echo dataBanco2data($_POST['data_inicial'])." a ".dataBanco2data($_POST['data_final']);?></h5>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                Bloco 1 - Famílias em acompanhamento Pelo PAIF
            </th>
        </tr>
        </thead>
    </table>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                A. Volume de famílias em acompanhamento pelo PAIF
            </th>
            <th>
                Quantidade
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>A.1 Total de famílias em acompanhamento pelo PAIF</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>A.2 Novas famílias inseridas no acompanhamento do PAIF durante o mês de referência</td>
            <td class="text-center"><?php echo $a2;?></td>
        </tr>
        </tbody>
    </table>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                B. Perfil das novas familias inseridas em acompanhamento no PAIF, no mês de referência
            </th>
            <th>
                Total
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>B.1. Famílias em situação de extrema pobreza</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>B.2. Famílias Beneficiárias do programa Bolsa familia</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>B.3. Famílias Beneficiárias do programa bolsa familia, em descumprimento de condicionalidades</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>B.4. Famílias com membros beneficiarios do BPC</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>B.5. Famílias com crianças ou adolescentes em situação de trabalho infantil</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>B.6. Famílias com crianças ou adolescentes em serviço de acolhimento</td>
            <td class="text-center"></td>
        </tr>
        </tbody>
    </table>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                Bloco 2 - Atendimentos particularizados realizados no CRAS
            </th>
        </tr>
        </thead>
    </table>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                C. Volume de atendimentos particularizados realizados no CRAS no mês de referência
            </th>
            <th>
                Quantidade
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>C.1 Total de atendimentos particularizados realizados no mẽs de referência</td>
            <td class="text-center"><?php echo $c1['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.2 Famílias encaminhadas para inclusão no cadastro único</td>
            <td class="text-center"><?php echo $c2['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.3 Famílias encaminhadas para atualização cadastral no cadastro único</td>
            <td class="text-center"><?php echo $c3['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.4 Indivíduos encaminhados para o acesso ao BPC</td>
            <td class="text-center"><?php echo $c4['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.5 Famílias encaminhadas para o CREAS</td>
            <td class="text-center"><?php echo $c5['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.6 Visitas domiciliares realizadas</td>
            <td class="text-center"><?php echo $c6['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.7 Total de auxílios-natalidade concedidos/entregues durante o mês de referência</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>C.8 Total de auxílios-funeral concedidos/entregues durante o mês de referência</td>
            <td class="text-center"><?php echo $c8['quantidade'];?></td>
        </tr>
        <tr>
            <td>C.9 Outros benefícios eventuais concedidos/entregues durante o mês de referência</td>
            <td class="text-center"><?php echo $c9['quantidade'];?></td>
        </tr>
        </tbody>
    </table>



    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                Bloco 3 - Atendimentos coletivos realizados no CRAS
            </th>
        </tr>
        </thead>
    </table>

    <table class="table table-sm table-responsive table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>
                D. Volume dos serviços de convivencia e foratalecimento de vinculo no mês de referência
            </th>
            <th>
                Quantidade
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>D.1 Famílias participando regularmente de grupos no âmbito do PAIF</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.2 Crianças de 0 a 6 anos em Serviços de Convivência e Fortalecimento de Vínculos</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.3 Crianças/adolescentes de 7 a 14 anos em Serviços de Convivência e Fortalecimento de Vínculos</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.4 Adolescentes de 15 a 17 anos em Serviços de Convivência e Fortalecimento de Vínculos</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.8 Adultos entre 18 e 59 anos em Serviços de Convivência e Fortalecimento de Vínculos</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.5 Idosos em Serviços de Convivência e Fortalecimento de Vínculos para idosos</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.6 Pessoas que participaram de palestras, oficinas e outras atividades coletivas de caráter não continuado</td>
            <td class="text-center"></td>
        </tr>
        <tr>
            <td>D.7 Pessoas com deficiência, participando dos Serviços de Convivência ou dos grupos do PAIF</td>
            <td class="text-center"></td>
        </tr>
        </tbody>
    </table>

</div>
</body>
</html>