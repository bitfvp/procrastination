<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_12"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatório de novas familias acompanhadas com base em avaliações-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT * \n"
    . "FROM mcu_pb_avaliacao \n"
    . "WHERE (mcu_pb_avaliacao.data_ts>=:inicial and mcu_pb_avaliacao.data_ts<=:final and a2=1)\n"
    . "ORDER BY cf ASC, data_ts DESC LIMIT 0,1000";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$familias = $consulta->fetchAll();
$contar = $consulta->rowCount();
$sql = null;
$consulta = null;

?>
<div class="container-fluid">
    <h3>Relatório de novas famílias acompanhadas com base em avaliações</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>



    <table class="table table-sm">
        <thead>
        <tr>
            <th>Avaliações</th>
            <th>Familia</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $aa=0;
        $a1=0;
        $a2=0;
        $b1=0;
        $b2=0;
        $b3=0;
        $b4=0;
        $b5=0;
        $b6=0;

        $cf_temp=0;
        $aa_temp=0;
        $a1_temp=0;
        $a2_temp=0;
        $b1_temp=0;
        $b2_temp=0;
        $b3_temp=0;
        $b4_temp=0;
        $b5_temp=0;
        $b6_temp=0;

        foreach ($familias as $familia){
            if ($familia['cf']!=0 and $familia['cf']!="" and $familia['cf']!=null) {

                if ($familia['cf']!=$cf_temp){
                    $aa_temp=0;
                    $a1_temp=0;
                    $a2_temp=0;
                    $b1_temp=0;
                    $b2_temp=0;
                    $b3_temp=0;
                    $b4_temp=0;
                    $b5_temp=0;
                    $b6_temp=0;
                    $cf_temp=$familia['cf'];
                }




                echo "<tr>";
                    echo "<td>";
                        if ($familia['aa']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PROFISSIONAL QUE FEZ O REGISTRO</strong><br>";
                            if ($aa_temp==0){
                                $aa++;
                                $aa_temp=1;
                            }
                        }
                        if ($familia['a1']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMILIA É ACOMPANHADA PELO PAIF</strong><br>";
                            if ($a1_temp==0){
                                $a1++;
                                $a1_temp=1;
                            }
                        }
                        if ($familia['a2']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>É UMA NOVA FAMÍLIA ACOMPANHADA</strong><br>";
                            if ($a2_temp==0){
                                $a2++;
                                $a2_temp=1;
                            }
                        }
                        if ($familia['b1']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA ESTÁ EM SITUAÇÃO DE EXTREMA POBREZA</strong><br>";
                            if ($b1_temp==0){
                                $b1++;
                                $b1_temp=1;
                            }
                        }
                        if ($familia['b2']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA É BENEFICIÁRIA NO PROGRAMA BOLSA FAMÍLIA</strong><br>";
                            if ($b2_temp==0){
                                $b2++;
                                $b2_temp=1;
                            }
                        }
                        if ($familia['b3']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>POSSUI BOLSA FAMÍLIA MAS ESTÁ EM DESCUMPRIMENTO DE CONDICIONALIDADES</strong><br>";
                            if ($b3_temp==0){
                                $b3++;
                                $b3_temp=1;
                            }
                        }
                        if ($familia['b4']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI MEMBROS BENEFICIÁRIOS DO BPC</strong><br>";
                            if ($b4_temp==0){
                                $b4++;
                                $b4_temp=1;
                            }
                        }
                        if ($familia['b5']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SITUAÇÃO DE TRABALHO INFANTIL</strong><br>";
                            if ($b5_temp==0){
                                $b5++;
                                $b5_temp=1;
                            }
                        }
                        if ($familia['b6']==1){
                            echo "<i class='fas fa-arrow-circle-right text-dark'></i><strong class='text-success'>FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SERVIÇO DE ACOLHIMENTO</strong><br>";
                            if ($b6_temp==0){
                                $b6++;
                                $b6_temp=1;
                            }
                        }
                echo "<strong class='text-info'>lançada em: ".dataBanco2data($familia['data_inicial'])."</strong><br>";
                $us=fncgetusuario($familia['profissional']);


                        echo "<strong class='text-info'>";
                        echo $us['nome'];
                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                        echo "</strong><br>";

                    echo "</td>";

                        echo "<td>";

                $sql = "SELECT * FROM mcu_pessoas WHERE cod_familiar=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $familia['cf']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $cfpessoa = $consulta->fetchall();
                $sql=null;
                $consulta=null;

                foreach ($cfpessoa as $cfp){

                    echo "<div class='list-group-item'>\n";
                    $nome = explode(" ", $cfp['nome']);

                    echo "<div class='dropdown show float-right dropleft'>\n"
                        ."<a class='badge badge-danger btn-sm' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>\n"
                        ."<span class='fa fa-times'></span>\n"
                        ."</a>\n"
                        ."<div class='dropdown-menu left' aria-labelledby='dropdownMenuLink'>\n"
                        ."<a class='dropdown-item' href='#'>Manter como está</a>\n"
                        ."<a class='dropdown-item bg-danger' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}'>Remover da família</a>\n"
                        ."</div>\n"
                        ."</div>\n";

                    echo "<a href='#' "
                        ."class='' "
                        ."data-toggle='popover' "
                        ."data-placement='top' "
                        ."title='{$cfp['nome']}' "
                        ."data-html='true' "
                        ."data-trigger='hover' "
                        ."data-content='"
                        .dataBanco2data ($cfp['nascimento']). " idade:" .Calculo_Idade($cfp['nascimento'])." ano(s)<br>"
                        ."CPF:".$cfp['cpf']."<br>"
                        ."RG:".$cfp['rg']."<br>"
                        ."' >\n";

                    echo "    {$nome[0]} </a>\n";
//                                        echo "    {$cfp['nome']} </a>";

                    echo "</div>\n";
                }

                        echo "</td>";

                echo "</tr>";
            }
        }
        ?>
        </tbody>
    </table>
    <hr>
    <table class="table table-sm">
        <thead>
        <tr>
            <th>Titulo</th>
            <th>Quantidade</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>FAMILIA É ACOMPANHADA PELO PROFISSIONAL QUE FEZ O REGISTRO</td>
            <td><?php echo $aa;?></td>
        </tr>
        <tr>
            <td>A1-FAMILIA É ACOMPANHADA PELO PAIF</td>
            <td><?php echo $a1;?></td>
        </tr>
        <tr>
            <td>A2-É UMA NOVA FAMÍLIA ACOMPANHADA</td>
            <td><?php echo $a2;?></td>
        </tr>
        <tr>
            <td>B1-FAMÍLIA ESTÁ EM SITUAÇÃO DE EXTREMA POBREZA</td>
            <td><?php echo $b1;?></td>
        </tr>
        <tr>
            <td>B2-FAMÍLIA É BENEFICIÁRIA NO PROGRAMA BOLSA FAMÍLIA</td>
            <td><?php echo $b2;?></td>
        </tr>
        <tr>
            <td>B3-POSSUI BOLSA FAMÍLIA MAS ESTÁ EM DESCUMPRIMENTO DE CONDICIONALIDADES</td>
            <td><?php echo $b3;?></td>
        </tr>
        <tr>
            <td>B4-FAMÍLIA POSSUI MEMBROS BENEFICIÁRIOS DO BPC</td>
            <td><?php echo $b4;?></td>
        </tr>
        <tr>
            <td>B5-FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SITUAÇÃO DE TRABALHO INFANTIL</td>
            <td><?php echo $b5;?></td>
        </tr>
        <tr>
            <td>B6-FAMÍLIA POSSUI CRIANÇAS OU ADOLESCENTES EM SERVIÇO DE ACOLHIMENTO</td>
            <td><?php echo $b6;?></td>
        </tr>
        </tbody>
    </table>

</div>

</body>
</html>
