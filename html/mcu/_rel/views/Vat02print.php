<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_12"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatório de atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$profi=$_POST['profissional'];
if($profi==0){

    $sql = "SELECT mcu_pessoas.nome, mcu_pessoas.cod_familiar, mcu_pb_at.`data`, mcu_pb_at.profissional \n"
        . "FROM mcu_pessoas INNER JOIN mcu_pb_at ON mcu_pessoas.id = mcu_pb_at.pessoa \n"
        . "WHERE mcu_pb_at.atividade = 18 AND mcu_pb_at.tipo = 1 AND mcu_pb_at.`status` = 1 and mcu_pb_at.data>=:inicial and mcu_pb_at.data<=:final \n"
        . "ORDER BY mcu_pb_at.`data` ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
//$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoas = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{

    $sql = "SELECT mcu_pessoas.nome, mcu_pessoas.cod_familiar, mcu_pb_at.`data`, mcu_pb_at.profissional \n"
        . "FROM mcu_pessoas INNER JOIN mcu_pb_at ON mcu_pessoas.id = mcu_pb_at.pessoa \n"
        . "WHERE mcu_pb_at.atividade = 18 AND mcu_pb_at.tipo = 1 AND mcu_pb_at.`status` = 1 and mcu_pb_at.data>=:inicial and mcu_pb_at.data<=:final and mcu_pb_at.profissional=:profi \n"
        . "ORDER BY mcu_pb_at.`data` ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoas = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}
?>
<div class="container-fluid">
    <h3>Relatório de novas familias atendidas</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>

        <?php
        foreach ($pessoas as $pessoa){
            echo "<table class='table table-sm'>";
                echo "<thead class='thead-dark'>";
                    echo "<tr>";
                        echo "<th>NOME</th>";
                        echo "<th>DATA</th>";
                        echo "<th>PROFISSIONAL</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                    echo "<tr>";
                        echo "<td>";
                            echo $pessoa['nome'];
                        echo "</td>";

                        echo "<td>";
                            echo dataRetiraHora($pessoa['data']);
                        echo "</td>";

                        echo "<td>";
                            echo fncgetusuario($pessoa['profissional'])['nome'];
                        echo "</td>";
                    echo "</tr>";

            $sql = "SELECT mcu_pessoas.nome, mcu_pessoas.nascimento \n"
                . "FROM mcu_pessoas \n"
                . "WHERE mcu_pessoas.cod_familiar = :cod_familiar ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":cod_familiar",$pessoa['cod_familiar']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $familia = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
                foreach ( $familia as $fam){
                    echo "<tr>";
                    echo "<td>";

                    echo "</td>";

                    echo "<td>->";
                    echo $fam['nome'];
                    echo "</td>";

                    echo "<td>";
                    echo dataBanco2data($fam['nascimento']);
                    echo "</td>";
                    echo "</tr>";
                }

                echo "</tbody>";
            echo "</table>";
        }
        ?>



</div>
<fieldset>
    <div id="piechart" style="width: auto; height: 900px;"></div>
    <div style="page-break-after: always;"></div>
    <div id="chart_div" style="height: 1500px;"></div>
</fieldset>
</body>
</html>
<?php
$inicial = $_POST['data_inicial']." 00:00:01";
$final = $_POST['data_final']." 23:59:59";
$profi=$_POST['profissional'];
if($profi==null or $profi==0){
    $sql = "SELECT\n"
        . "Count(mcu_pb_at.id) AS contadora,\n"
        . "tbl_users.nome as prof\n"
        . "FROM\n"
        . "tbl_users\n"
        . "INNER JOIN mcu_pb_at ON tbl_users.id = mcu_pb_at.profissional\n"
        . "WHERE\n"
        . "mcu_pb_at.`data` >= :inicial AND\n"
        . "mcu_pb_at.`data` <= :final AND mcu_pb_at.tipo=1 and mcu_pb_at.status=1 and mcu_pb_at.atividade = 18 \n"
        . "GROUP BY\n"
        . "mcu_pb_at.profissional\n"
        . "ORDER BY\n"
        . "contadora desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $graf1 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

    $sql = "SELECT\n"
        . "Count(mcu_pb_at.id) AS contadora,\n"
        . "mcu_pessoas.bairro\n"
        . "FROM\n"
        . "mcu_pessoas\n"
        . "INNER JOIN mcu_pb_at ON mcu_pessoas.id = mcu_pb_at.pessoa\n"
        . "WHERE\n"
        . "mcu_pb_at.`data` >= :inicial AND\n"
        . "mcu_pb_at.`data` <= :final AND mcu_pb_at.tipo=1 and mcu_pb_at.status=1 and mcu_pb_at.atividade = 18 \n"
        . "GROUP BY\n"
        . "mcu_pessoas.bairro\n"
        . "ORDER BY\n"
        . "contadora desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $graf2 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

}else{
    $sql = "SELECT\n"
        . "Count(mcu_pb_at.id) AS contadora,\n"
        . "tbl_users.nome as prof\n"
        . "FROM\n"
        . "tbl_users\n"
        . "INNER JOIN mcu_pb_at ON tbl_users.id = mcu_pb_at.profissional\n"
        . "WHERE\n"
        . "mcu_pb_at.`data` >= :inicial AND\n"
        . "mcu_pb_at.`data` <= :final AND mcu_pb_at.tipo=1 and mcu_pb_at.status=1 and mcu_pb_at.atividade = 18 and mcu_pb_at.profissional = :prof \n"
        . "GROUP BY\n"
        . "mcu_pb_at.profissional\n"
        . "ORDER BY\n"
        . "contadora desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->bindValue(":prof", $profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $graf1 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

    $sql = "SELECT\n"
        . "Count(mcu_pb_at.id) AS contadora,\n"
        . "mcu_pessoas.bairro\n"
        . "FROM\n"
        . "mcu_pessoas\n"
        . "INNER JOIN mcu_pb_at ON mcu_pessoas.id = mcu_pb_at.pessoa\n"
        . "WHERE\n"
        . "mcu_pb_at.`data` >= :inicial AND \n"
        . "mcu_pb_at.`data` <= :final AND mcu_pb_at.tipo=1 and mcu_pb_at.status=1 and mcu_pb_at.atividade = 18 and mcu_pb_at.profissional=:prof \n"
        . "GROUP BY\n"
        . "mcu_pessoas.bairro\n"
        . "ORDER BY\n"
        . "contadora desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->bindValue(":prof", $profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $graf2 = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
}
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['Atividade', 'Quant'],
            <?php
            foreach ($graf1 as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['prof'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);
        //opções para o gráfico pizza
        var options3 = {
            title: 'Gráfico torta de novas familias acompanhadas',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }



    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);
    function drawBasic() {
        var data = google.visualization.arrayToDataTable([
            ['Bairro', 'Quantidade',],
            <?php
            $cont=0;
            foreach ($graf2 as $gf) {
                $bairro=fncgetbairro($gf['bairro'])['bairro'];
                $cont=$cont+$gf['contadora'];
                echo "['{$gf['contadora']} {$bairro}', {$gf['contadora']}],\n";
            }
            ?>
        ]);
        var options = {
            title: 'Gráfico de novas familias acompanhadas por bairro',
            chartArea: {width: '50%'},
            hAxis: {
                title: '<?php echo $cont;?>',
                minValue: 0,
            },
            vAxis: {
                title: 'Bairros',
                textStyle : {
                    fontSize: 11 // or the number you want
                },
            }
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>