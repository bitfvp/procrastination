<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Cesta basica relatório-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info text-light">
                Relatório de benefícios
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vbe01print" method="post" target="_blank">

                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO"/>
                            </div>
                            <div class="col-md-6">
                                <label for="data_inicial">Data Inicial:</label>
                                <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="<?php echo date("Y-m-01");?>" required/>
                            </div>
                            <div class="col-md-6">
                                <label for="data_final">Data Final:</label>
                                <input id="data_final" type="date" class="form-control" name="data_final" value="<?php echo date("Y-m-t");?>" required/>
                            </div>
                            <div class="col-md-6">
                                <label for="beneficio">Benefício:</label>
                                <select name="beneficio" id="beneficio" class="form-control" required>
                                    // vamos criar a visualização
                                    <option selected="" value="">
                                        Selecione...
                                    </option>
                                    <?php
                                    foreach(fncbetipolist() as $item){
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['tipo']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="condicao">Condição:</label>
                                <select name="condicao" id="condicao" class="form-control">
                                    // vamos criar a visualização
                                    <option selected="" value="0">
                                        Selecione...
                                    </option>
                                    <?php
                                    foreach(fncbecondicaolist_todos() as $item){
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['condicao']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="ordem">Ordenar por:</label>
                                <select name="ordem" id="ordem" class="form-control" required>
                                    <option selected="" value="">
                                        Selecione...
                                    </option>
                                    <option value="1">
                                        Data
                                    </option>
                                    <option value="2">
                                        Bairro
                                    </option>
                                    <option value="3">
                                        Beneficiário
                                    </option>
                                    <option value="4">
                                        Profissional
                                    </option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="quem_recebeu">Quem recebeu:</label>
                                <input id="quem_recebeu" type="text" class="form-control" name="quem_recebeu" value="" />
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>