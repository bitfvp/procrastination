<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>

<main class="container"><!--todo conteudo-->

    <?php if($allow["allow_36"]==1) {?>
    <h5>Benefícios</h5>
    <table class="table table-sm ">
        <thead class="thead-dark">
            <tr>
                <th>código</th>
                <th>descrição</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>be01</td>
                <td><a href="index.php?pg=Vbe01"><strong>Benefícios geral <i class="text-info">Há repetição de pessoas</i> </strong></a></td>
            </tr>

            <tr>
                <td>be04</td>
                <td><strong>Famílias que receberam o benefício por bairro <i class="text-info">Não há repetição de familias</i> <i class="text-danger"> aguardando implementação</i></strong></a></td>
            </tr>

            <tr>
                <td>be05</td>
                <td><a href="index.php?pg=Vbe05"><strong>Pessoas que receberam benefícios por bairro <i class="text-info">Não há repetição de pessoas</i> </strong></a></td>
            </tr>

            <tr>
                <td>beinf01</td>
                <td><a href="index.php?pg=Vbeinf01"><strong>Beneficios em gráficos </strong></a></td>
            </tr>
        </tbody>
    </table>
    <?php }?>


    <?php if($allow["allow_12"]==1) {?>
    <h5>Atividades proteção basica</h5>
    <table class="table table-sm ">
        <thead class="thead-dark">
        <tr>
            <th>código</th>
            <th>descrição</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>at01</td>
            <td><a href="index.php?pg=Vat01"><strong>contagem de tividade por profissional ou geral </strong></a></td>
        </tr>

        <tr>
            <td>at02</td>
            <td><a href="index.php?pg=Vat02"><strong>Novas famílias acompanhadas </strong></a></td>
        </tr>
        <tr>
            <td>atinf01</td>
            <td><a href="index.php?pg=Vatinf01"><strong>Atividades em gráficos </strong></a></td>
        </tr>
        </tbody>
    </table>
    <?php }?>

    <?php if($allow["allow_12"]==1) {?>
    <h5>Demanda em acompanhamento pela proteção basica</h5>
    <table class="table table-sm ">
        <thead class="thead-dark">
        <tr>
            <th>código</th>
            <th>descrição</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>rma_cras</td>
            <td><a href="index.php?pg=Vrma_cras"><strong>RMA CRAS <i class="text-info">Critérios definidos pelo setor.</i></strong></a></td>
        </tr>

        <tr>
            <td>ac01</td>
            <td><a href="index.php?pg=Vac01"><strong>Pessoas em acompanhamento <i class="text-info">Possui Beneficio ou (atendimento + visita) ou Nova Familia Acompanhada ou Agendamento BPC</i></strong></a></td>
        </tr>
        <tr>
            <td>ac02</td>
            <td><a href="index.php?pg=Vac02"><strong>Famílias em acompanhamento <i class="text-info">Possui Beneficio ou (atendimento + visita) ou Nova Familia Acompanhada ou Agendamento BPC</i></strong></a></td>
        </tr>
        <tr>
            <td>ac03</td>
            <td><a href="index.php?pg=Vac03"><strong>Famílias em acompanhamento <i class="text-info">possuir qualquer tipo de atendimento na pb ou beneficio</i></strong></a></td>
        </tr>

        <tr>
            <td>ac04</td>
            <td><a href="index.php?pg=Vac04"><strong>Famílias em acompanhamento por bairro <i class="text-info">possuir qualquer tipo de atendimento na pb ou beneficio</i></strong></a></td>
        </tr>

        <tr>
            <td>ac05</td>
            <td><a href="index.php?pg=Vac05"><strong>Novas famílias em acompanhamento PB com base em avaliações</strong></a></td>
        </tr>

        <tr>
            <td>ac06</td>
            <td><a href="index.php?pg=Vac06"><strong>Famílias em acompanhamento PAIF com base em avaliações</strong></a></td>
        </tr>
        <tr>
            <td>ac07</td>
            <td><a href="index.php?pg=Vac07"><strong>Novas famílias em acompanhamento PB com base em avaliações</strong></a></td>
        </tr>

        </tbody>
    </table>
    <?php }?>

    <?php if($allow["allow_20"]==1) {?>
    <h5>Atividades proteção especializada</h5>
    <table class="table table-sm ">
        <thead class="thead-dark">
        <tr>
            <th>código</th>
            <th>descrição</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>pe01</td>
            <td><a href="index.php?pg=Vpe01"><strong>contagem de tividade por profissional ou geral </strong></a></td>
        </tr>
        <tr>
            <td>pe02</td>
            <td><a href="index.php?pg=Vpe02"><strong>Familias acompanhadas por profissional </strong></a></td>
        </tr>
        </tbody>
    </table>
    <?php }?>


    <?php if($allow["allow_25"]==1) {?>
        <h5>Atividades Conselho Tutelar</h5>
        <table class="table table-sm ">
            <thead class="thead-dark">
            <tr>
                <th>código</th>
                <th>descrição</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>ct01</td>
                <td><a href="index.php?pg=Vct01"><strong>contagem de tividade por profissional ou geral </strong></a></td>
            </tr>
            </tbody>
        </table>
    <?php }?>




</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>