<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Cesta básica relatório-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">

<div class="row">
<div class="col-md-4"></div>
    <div class="col-md-4">
<!-- =============================começa conteudo======================================= -->
        <div class="card">
            <div class="card-header bg-info text-light">
            Relatório de pessoas que receberam benefícios por bairro
            </div>
            <div class="card-body">
                <form action="index.php?pg=Vbe05print" method="post" target="_blank">
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO"/>

                    <div class="form-group">
                        <label for="data_inicial">Data Inicial:</label>
                        <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="" required/>
                    </div>

                    <div class="form-group">
                        <label for="data_final">Data Final:</label>
                        <input id="data_final" type="date" class="form-control" name="data_final" value="" required/>
                    </div>

                    <div class="form-group">
                        <label for="beneficio">Benefício:</label>
                        <select name="beneficio" id="beneficio" class="form-control" required>
                            <option selected="" value="">
                                Selecione...
                            </option>
                            <?php
                            foreach(fncbetipolist() as $item){
                                ?>
                                <option value="<?php echo $item['id']; ?>"><?php echo $item['tipo']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="condicao">Condição:</label>
                        <select name="condicao" id="condicao" class="form-control">
                            <option selected="" value="0">
                                Selecione...
                            </option>
                            <?php
                            foreach(fncbecondicaolist_todos() as $item){
                                ?>
                                <option value="<?php echo $item['id']; ?>"><?php echo $item['condicao']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="bairro">Bairro:</label>
                        <select name="bairro" id="bairro" class="form-control" required>
                            <option selected="" value="0">
                                Selecione...
                            </option>
                            <?php
                            foreach(fncbairrolist() as $item){
                                ?>
                                <option value="<?php echo $item['id']; ?>"><?php echo $item['bairro']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                </form>
            </div>
        </div>

<!-- =============================fim conteudo======================================= -->       
    </div>
    <div class="col-md-4"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>