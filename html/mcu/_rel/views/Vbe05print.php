<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Beneficio relatório-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";
    $beneficio = $_POST['beneficio'];
    $condicao = $_POST['condicao'];
    $bairro = $_POST['bairro'];

        $sql = "SELECT DISTINCT \n"
            . "mcu_beneficio.pessoa, \n"
            . "mcu_pessoas.nome, \n"
            . "mcu_pessoas.cpf, \n"
            . "mcu_pessoas.numero,\n"
            . "mcu_pessoas.endereco,\n"
            . "mcu_pessoas.referencia,\n"
            . "mcu_pessoas.bairro, \n"
            . "mcu_pessoas.telefone \n"
            . "FROM \n"
            . "mcu_beneficio \n"
            . "INNER JOIN mcu_pessoas ON mcu_beneficio.pessoa = mcu_pessoas.id\n";

        $sql .= "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  \n";

                if ($beneficio!="" and $beneficio!=0){
                    $sql .= "and (mcu_beneficio.beneficio=:beneficio) ";
                }

        if ($condicao!="" and $condicao!=0){
            $sql .= "and (mcu_beneficio.condicao=:condicao) ";
        }

        if ($bairro!="" and $bairro!=0){
            $sql .= "and (mcu_pessoas.bairro=:bairro) ";
        }


        $sql .=  " )\n"
            . "ORDER BY\n"
            . "mcu_pessoas.bairro ASC, mcu_pessoas.nome ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
    if ($condicao!="" and $condicao!=0){
        $consulta->bindValue(":condicao", $condicao);
    }
    if ($bairro!="" and $bairro!=0){
        $consulta->bindValue(":bairro", $bairro);
    }
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $bnf = $consulta->fetchAll();
        $bnfcount = $consulta->rowCount();
        $sql = null;
        $consulta = null;

    ?>
    <div class="row-fluid text-uppercase">

        <!-- ////////////////////////////////////////// -->
        <h3>RELATÓRIO DE PESSOAS COM <?php echo fncgetbetipo($beneficio)['tipo'];?> POR BAIRRO</h3>

        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>BENEFICIÁRIO&nbsp;</td>
                <td>CPF&nbsp;</td>
                <td>ENDEREÇO&nbsp;</td>
                <td>BAIRRO&nbsp;</td>
                <td>TELEFONE&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($bnf as $be) {
                if($be['cpf']!="" and $be['cpf']!=0 ) {
                    $tempcpf = mask($be['cpf'],'###.###.###-##');
                }else{
                    $tempcpf = "[---]";
                }
                ?>
            <tr>
                <td><?php echo $be['nome']; ?>&nbsp;</td>
                <td style= "white-space: nowrap;"><?php echo $tempcpf; ?>&nbsp;</td>
                <td><?php echo $be['endereco']." / ".$be['numero']." / ".$be['referencia']; ?>&nbsp;</td>
                <td><?php echo fncgetbairro($be['bairro'])['bairro']; ?>&nbsp;</td>
                <td><?php echo $be['telefone']; ?>&nbsp;</td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <h4>TOTAL DE <?php echo fncgetbetipo($beneficio)['tipo'].": ".$bnfcount; ?></h4>

    </div>
</main>
</body>
</html>