<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Beneficio relatório-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");

    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";
    $beneficio = $_POST['beneficio'];
    $condicao = $_POST['condicao'];

    if ($condicao=="" or $condicao==0){
        $sql = "SELECT "
            ."YEAR(mcu_beneficio.data_entrega ) AS ano, "
            ."Sum(mcu_beneficio.quantidade) AS quantidade "
            ."FROM "
            ."mcu_beneficio "
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) ) \n"
            ."GROUP BY "
            ."YEAR(mcu_beneficio.data_entrega) ";

        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $graf1 = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        $sql = "SELECT "
            ."YEAR(mcu_beneficio.data_entrega ) AS ano, "
            ."MONTH(mcu_beneficio.data_entrega ) AS mes, "
            ."Sum(mcu_beneficio.quantidade) AS quantidade "
            ."FROM "
            ."mcu_beneficio "
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) ) \n"
            ."GROUP BY "
            ."YEAR(mcu_beneficio.data_entrega), MONTH(mcu_beneficio.data_entrega) ";

        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $graf2 = $consulta->fetchAll();
        $sql = null;
        $consulta = null;


    }else{

//        $sql = "SELECT "
//            ."YEAR(mcu_beneficio.data_entrega ) AS ano, "
//            ."Sum(mcu_beneficio.quantidade) AS quantidade "
//            ."FROM "
//            ."mcu_beneficio "
//            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) and (mcu_beneficio.condicao=:condicao) ) \n"
//            ."GROUP BY "
//            ."YEAR(mcu_beneficio.data_entrega) ";
//        global $pdo;
//        $consulta = $pdo->prepare($sql);
//        $consulta->bindValue(":inicial", $inicial);
//        $consulta->bindValue(":final", $final);
//        $consulta->bindValue(":beneficio", $beneficio);
//        $consulta->bindValue(":condicao", $condicao);
//        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
//        $graf1 = $consulta->fetchAll();
//        $sql = null;
//        $consulta = null;


    }
    $bairros=fncbairrolist();
    ?>
<main class="container">


        <h3>RELATÓRIO GRÁFICO DE <?php echo fncgetbetipo($beneficio)['tipo'];?> </h3>
        <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
            <div id="chart_div1" class="ml-0 my-2" ></div>
    <br>
            <div id="chart_div2" class="ml-0 my-2" ></div>

        <?php
        foreach ($bairros as $bairro){
            echo "<br><div id='chart_divB{$bairro['id']}' class='ml-0 my-2' title='{$bairro['bairro']}' ></div> \n";
        }
        ?>


</main>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawLineColors1);
    google.charts.setOnLoadCallback(drawLineColors2);
    <?php
    foreach ($bairros as $bairro){
        echo "google.charts.setOnLoadCallback(drawLineColorsB{$bairro['id']});\n";
    }
    ?>

    function drawLineColors1() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'ANO');
        data.addColumn('number', 'Quantidade');

        data.addRows([
            <?php
            foreach ($graf1 as $gf) {
                if (!is_null($gf[0])){
                    echo "['" . $gf['ano'] . "', " . $gf['quantidade'] . "]," . "\n";
                }
            }
            ?>
        ]);
        var options = {
            title: '<?php echo fncgetbetipo($beneficio)['tipo'];?> POR ANO',
            hAxis: {
                title: 'Anos'
            },
            // curveType: 'function',
            height: 600,
            width: 1300,
            vAxis: {
                title: 'Quantidade'
            },
            legend: { position: 'bottom' },
            colors: ['#a52714', '#097138'],
            pointSize: 10,
            series: {
                0: { pointShape: 'diamond' },
                1: { pointShape: 'triangle' },
                2: { pointShape: 'square' },
                3: { pointShape: 'star' },
                4: { pointShape: 'circle' },
                5: { pointShape: 'polygon' }
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));
        chart.draw(data, options);
    }




    function drawLineColors2() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'mês-ano');
        data.addColumn('number', 'Quantidade');

        data.addRows([
            <?php
            foreach ($graf2 as $gf) {
                if (!is_null($gf[0])){
                    echo "['" . fncgetmes($gf['mes']) . "-" . $gf['ano'] . "', " . $gf['quantidade'] . "]," . "\n";
                }
            }
            ?>
        ]);
        var options = {
            title: '<?php echo fncgetbetipo($beneficio)['tipo'];?> POR MẼS',
            hAxis: {
                title: 'mês-ano'
            },
            height: 600,
            width: 1300,
            vAxis: {
                title: 'Quantidade'
            },
            legend: { position: 'bottom' },
            colors: ['#a52714', '#097138'],
            pointSize: 10,
            series: {
                0: { pointShape: 'diamond' },
                1: { pointShape: 'triangle' },
                2: { pointShape: 'square' },
                3: { pointShape: 'star' },
                4: { pointShape: 'circle' },
                5: { pointShape: 'polygon' }
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
        chart.draw(data, options);
    }


    <?php
    if ($condicao=="" or $condicao==0){
        foreach ($bairros as $bairro){
            $sql = "SELECT "
                ."YEAR(mcu_beneficio.data_entrega ) AS ano, "
                ."MONTH(mcu_beneficio.data_entrega ) AS mes, "
                ."Sum(mcu_beneficio.quantidade) AS quantidade "
                ."FROM "
                ."mcu_beneficio INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_beneficio.pessoa "
                . "WHERE ( ((mcu_pessoas.bairro)=:bairro) and ((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) ) \n"
                ."GROUP BY "
                ."YEAR(mcu_beneficio.data_entrega), MONTH(mcu_beneficio.data_entrega) ";

            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":bairro", $bairro['id']);
            $consulta->bindValue(":inicial", $inicial);
            $consulta->bindValue(":final", $final);
            $consulta->bindValue(":beneficio", $beneficio);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $grafb = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            ?>
    function drawLineColorsB<?php echo $bairro['id'];?>() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'mês-ano');
        data.addColumn('number', 'Quantidade');

        data.addRows([
            <?php
            foreach ($grafb as $gf) {
                if (!is_null($gf[0])){
                    echo "['" . fncgetmes($gf['mes']) . "-" . $gf['ano'] . "', " . $gf['quantidade'] . "]," . "\n";
                }
            }
            ?>
        ]);
        var options = {
            title: '<?php echo fncgetbetipo($beneficio)['tipo'];?> ENTREGUES NA LOCALIDADE <?php echo $bairro['bairro'];?>',
            hAxis: {
                title: 'mês-ano'
            },
            curveType: 'function',
            height: 600,
            width: 1300,
            vAxis: {
                title: 'Quantidade'
            },
            legend: { position: 'bottom' },
            colors: ['#097138', '#a52714'],
            pointSize: 15,
            series: {
                0: { pointShape: 'star' },
                1: { pointShape: 'triangle' },
                2: { pointShape: 'square' },
                3: { pointShape: 'diamond' },
                4: { pointShape: 'circle' },
                5: { pointShape: 'polygon' }
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_divB<?php echo $bairro['id'];?>'));
        chart.draw(data, options);
    }

<?php
        }

    }else{


    }
    ?>
</script>