<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_17"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_20"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatório de atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$profi=$_POST['profissional'];
if($profi==0){
    $sql = "SELECT mcu_pb_at.id, tbl_users.nome AS prof, mcu_pessoas.nome AS pessoa, mcu_pb_atlista.atividade, mcu_pb_at.data, mcu_pb_at.descricao, mcu_pb_at.profissional\n"
        . "FROM mcu_pessoas INNER JOIN (tbl_users INNER JOIN (mcu_pb_atlista INNER JOIN mcu_pb_at ON mcu_pb_atlista.id = mcu_pb_at.atividade) ON tbl_users.id = mcu_pb_at.profissional) ON mcu_pessoas.id = mcu_pb_at.pessoa\n"
        . "WHERE (((mcu_pb_at.data)>=:inicial) And ((mcu_pb_at.data)<=:final) And ((mcu_pb_at.tipo)=2) and mcu_pb_at.status=1 )\n"
        . "ORDER BY tbl_users.nome, mcu_pb_atlista.atividade, mcu_pessoas.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
//$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{
    $sql = "SELECT mcu_pb_at.id, tbl_users.nome AS prof, mcu_pessoas.nome AS pessoa, mcu_pb_atlista.atividade, mcu_pb_at.data, mcu_pb_at.descricao, mcu_pb_at.profissional\n"
        . "FROM mcu_pessoas INNER JOIN (tbl_users INNER JOIN (mcu_pb_atlista INNER JOIN mcu_pb_at ON mcu_pb_atlista.id = mcu_pb_at.atividade) ON tbl_users.id = mcu_pb_at.profissional) ON mcu_pessoas.id = mcu_pb_at.pessoa\n"
        . "WHERE (((mcu_pb_at.data)>=:inicial) And ((mcu_pb_at.data)<=:final) AND ((mcu_pb_at.profissional)=:profi) And ((mcu_pb_at.tipo)=2) and mcu_pb_at.status=1 )\n"
        . "ORDER BY tbl_users.nome, mcu_pb_atlista.atividade, mcu_pessoas.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
$consulta->bindValue(":profi",$profi);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}
?>
<div class="container-fluid">
    <h3>Relatório de atividades</h3>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
    <?php
    $acont=0;
    $a=$ppp="a";
    echo "<table class='table table-striped table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>PESSOA</td>";
    echo "<td>ATIVIDADE</td>";
    echo "<td>DATA</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach($ati as $at){

        if(((isset($ppp))and ($ppp!=$at['prof']) and ($acont>0))or((isset($a))and ($a!=$at['atividade'])) and ($acont>0)){

            echo "<tr>";
            echo "<td colspan='4' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
            echo "</tr>";
            $acont=0;
            if ((isset($ppp))and ($ppp!=$at['prof'])){
                echo "<tr>";
                echo "<td colspan='4' class='text-right font-weight-bold'>Total ".$tcont."</td>";
                echo "</tr>";
                $tcont=0;
            }

        }
        if(((isset($ppp))and ($ppp!=$at['prof']))or((isset($a))and ($a!=$at['atividade']))){

            echo "<tr>";
            echo "<td colspan='4' class='text-center font-weight-bold'>";
            echo $at['prof']." - ".$at['atividade'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo $at['atividade']; ?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $tcont++;
        $ppp=$at['prof'];
        $a=$at['atividade'];
    }
    echo "<tr>";
    echo "<td colspan='4' class='text-right font-weight-bold'>subtotal ".$acont."</td>";
    echo "</tr>";
    $acont=0;
    echo "<tr>";
    echo "<td colspan='4' class='text-right font-weight-bold'>Total ".$tcont."</td>";
    echo "</tr>";
    $tcont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
<fieldset>
    <div id="piechart" style="width: auto; height: 900px;"></div>
</fieldset>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['Atividade', 'Quant'],
            <?php
            // Recebe
            $inicial = $_POST['data_inicial']." 00:00:01";
            $final = $_POST['data_final']." 23:59:59";
            $profi=$_POST['profissional'];
            if($profi==0){
                $sql = "SELECT\n"
                    . "Count(mcu_pb_at.id) AS contadora,\n"
                    . "mcu_pb_atlista.atividade AS atv\n"
                    . "FROM\n"
                    . "mcu_pb_at\n"
                    . "INNER JOIN mcu_pb_atlista ON mcu_pb_atlista.id = mcu_pb_at.atividade\n"
                    . "WHERE\n"
                    . "mcu_pb_at.`data` >= :inicial AND\n"
                    . "mcu_pb_at.`data` <= :final AND mcu_pb_at.tipo=2 and mcu_pb_at.status=1 \n"
                    . "GROUP BY\n"
                    . "mcu_pb_at.atividade\n"
                    . "ORDER BY\n"
                    . "contadora desc";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }else{
                $sql = "SELECT\n"
                    . "Count(mcu_pb_at.id) AS contadora,\n"
                    . "mcu_pb_atlista.atividade AS atv\n"
                    . "FROM\n"
                    . "mcu_pb_at\n"
                    . "INNER JOIN mcu_pb_atlista ON mcu_pb_atlista.id = mcu_pb_at.atividade\n"
                    . "WHERE\n"
                    . "mcu_pb_at.`data` >= :inicial AND\n"
                    . "mcu_pb_at.`data` <= :final AND\n"
                    . "mcu_pb_at.profissional = :prof AND mcu_pb_at.tipo=2 and mcu_pb_at.status=1 \n"
                    . "GROUP BY\n"
                    . "mcu_pb_at.atividade\n"
                    . "ORDER BY\n"
                    . "contadora desc";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->bindValue(":prof", $profi);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }

            foreach ($graf as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['atv'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);



        //opções para o gráfico pizza
        var options3 = {
            title: 'Grafico torta de atividades por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>