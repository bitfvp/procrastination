<link href="../public/css/bs4/bootstrap.min.css" rel="stylesheet" >
<script src="../public/js/bootstrap.min.js"></script>
<table class="table table-sm table-responsive">
    <thead class="thead-inverse">
    <tr>
        <th>Nome allow</th>
        <th>Vinculação</th>
    </tr>
    </thead>

    <tbody>
    <tr class="bg-info">
        <td>allow_1</td>
        <td>Frota</td>
    </tr>
    <tr class="bg-info">
        <td>allow_2</td>
        <td>Frota admin</td>
    </tr>
    <tr class="bg-info">
        <td>allow_3</td>
        <td>nova pessoa</td>
    </tr>
    <tr class="bg-info">
        <td>allow_4</td>
        <td>editar pessoa</td>
    </tr>
    <tr class="bg-info">
        <td>allow_5</td>
        <td>bairro</td>
    </tr>
    <tr class="bg-info">
        <td>allow_6</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_7</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_8</td>
        <td>agenda</td>
    </tr>
    <tr class="bg-success">
        <td>allow_9</td>
        <td>protecao basica</td>
    </tr>
    <tr class="bg-success">
        <td>allow_10</td>
        <td>protecao basica atividade</td>
    </tr>
    <tr class="bg-success">
        <td>allow_11</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_12</td>
        <td>protecao basica atividade relatorio</td>
    </tr>
    <tr class="bg-success">
        <td>allow_13</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_14</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_15</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_16</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_17</td>
        <td>protecao especializada</td>
    </tr>
    <tr class="bg-info">
        <td>allow_18</td>
        <td>protecao especializada atividade</td>
    </tr>
    <tr class="bg-info">
        <td>allow_19</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_20</td>
        <td>protecao especializada atividade relatorio</td>
    </tr>
    <tr class="bg-info">
        <td>allow_21</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_22</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_23</td>
        <td>Abrigo relatorio</td>
    </tr>
    <tr class="bg-info">
        <td>allow_24</td>
        <td>Abrigo historico</td>
    </tr>
    <tr class="bg-success">
        <td>allow_25</td>
        <td>conselho tutelar </td>
    </tr>
    <tr class="bg-success">
        <td>allow_26</td>
        <td>conselho tutelar atividade</td>
    </tr>
    <tr class="bg-success">
        <td>allow_27</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_28</td>
        <td>conselho tutelar denuncia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_29</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_30</td>
        <td>conselho tutelar denuncia apurar</td>
    </tr>
    <tr class="bg-success">
        <td>allow_31</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_32</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_33</td>
        <td>cesta basica</td>
    </tr>
    <tr class="bg-info">
        <td>allow_34</td>
        <td>cesta basica pedir</td>
    </tr>
    <tr class="bg-info">
        <td>allow_35</td>
        <td>cesta basica entregar</td>
    </tr>
    <tr class="bg-info">
        <td>allow_36</td>
        <td>cesta basica relatorio</td>
    </tr>
    <tr class="bg-info">
        <td>allow_37</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_38</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_39</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_40</td>
        <td>Quantidade de cestas basicas mes</td>
    </tr>
    <tr class="bg-success">
        <td>allow_41</td>
        <td>bolsa família</td>
    </tr>
    <tr class="bg-success">
        <td>allow_42</td>
        <td>bolsa família pasta</td>
    </tr>
    <tr class="bg-success">
        <td>allow_43</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_44</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_45</td>
        <td>bolsa família visita</td>
    </tr>
    <tr class="bg-success">
        <td>allow_46</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_47</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-success">
        <td>allow_48</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_49</td>
        <td>Passe Livre</td>
    </tr>
    <tr class="bg-info">
        <td>allow_50</td>
        <td>apae</td>
    </tr>
    <tr class="bg-info">
        <td>allow_51</td>
        <td>idoso</td>
    </tr>
    <tr class="bg-info">
        <td>allow_52</td>
        <td>pcd</td>
    </tr>
    <tr class="bg-info">
        <td>allow_53</td>
        <td>advertecia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_54</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_55</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_56</td>
        <td>Passe livre/empresa</td>
    </tr>
    <tr class="bg-success">
        <td>allow_57</td>
        <td>Migrante</td>
    </tr>
    <tr class="bg-success">
        <td>allow_58</td>
        <td>Migrante pasta</td>
    </tr>
    <tr class="bg-success">
        <td>allow_59</td>
        <td>Migrante pasta relatorio</td>
    </tr>
    <tr class="bg-success">
        <td>allow_60</td>
        <td>Passagem</td>
    </tr>
    <tr class="bg-success">
        <td>allow_61</td>
        <td>Passagem lancar</td>
    </tr>
    <tr class="bg-success">
        <td>allow_62</td>
        <td>Passagem lancar relatorio</td>
    </tr>
    <tr class="bg-success">
        <td>allow_63</td>
        <td>Albergue</td>
    </tr>
    <tr class="bg-success">
        <td>allow_64</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_65</td>
        <td>Telecentro</td>
    </tr>
    <tr class="bg-info">
        <td>allow_66</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_67</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_68</td>
        <td class="bg-danger">vazia</td>
    </tr>
    <tr class="bg-info">
        <td>allow_69</td>
        <td>AABB</td>
    </tr>
    <tr class="bg-info">
        <td>allow_70</td>
        <td>processos seletivos</td>
    </tr>
    <tr class="bg-info">
        <td>allow_71</td>
        <td>abrigo estoque</td>
    </tr>
    <tr class="bg-info">
        <td>allow_72</td>
        <td>Biblioteca</td>
    </tr>
    <tr class="bg-success">
        <td>allow_73</td>
        <td>CH</td>
    </tr>
    <tr class="bg-success">
        <td>allow_74</td>
        <td>Oficio</td>
    </tr>
    <tr class="bg-success">
        <td>allow_75</td>
        <td>Fila</td>
    </tr>
    <tr class="bg-success">
        <td>allow_76</td>
        <td>Fila cadastrar</td>
    </tr>
    <tr class="bg-success">
        <td>allow_77</td>
        <td>Fila relatório</td>
    </tr>
    <tr class="bg-success">
        <td>allow_78</td>
        <td>Fila Tela</td>
    </tr>
    <tr class="bg-success">
        <td>allow_79</td>
        <td>Estoque</td>
    </tr>
    <tr class="bg-success">
        <td>allow_80</td>
        <td>SysJob</td>
    </tr>
    </tbody>
    <table>
