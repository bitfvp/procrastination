<?php
class Visita{
    public function fncvisitanew($pessoa,$data_pedido,$prof_cad,$cad_novo,$cad_transferencia,$cad_atualizado,$bolsa_familia,$descricao,$prof_resp){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_visitaaveriguacao ";
                $sql.="(id, pessoa, data_pedido, prof_cad, cad_novo, cad_transferencia, cad_atualizado, bolsa_familia, descricao, prof_resp)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :data_pedido, :prof_cad, :cad_novo, :cad_transferencia, :cad_atualizado, :bolsa_familia, :descricao, :prof_resp)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $pessoa);
                $insere->bindValue(":data_pedido", $data_pedido);
                $insere->bindValue(":prof_cad", $prof_cad);
                $insere->bindValue(":cad_novo", $cad_novo);
                $insere->bindValue(":cad_transferencia", $cad_transferencia);
                $insere->bindValue(":cad_atualizado", $cad_atualizado);
                $insere->bindValue(":bolsa_familia", $bolsa_familia);
                $insere->bindValue(":descricao", $descricao);
                $insere->bindValue(":prof_resp", $prof_resp);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($insere)){
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($pessoa,$_SESSION['id'],"Novo pedido de visita",9,1);
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: index.php?pg=Vvisita&id={$pessoa}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////




    public function fncvisitadelete($vi_id,$prof,$pessoa_id){
        try {
            $sql = "DELETE FROM `mcu_visitaaveriguacao` WHERE id = :vi_id and prof_cad = :prof_id";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":vi_id", $vi_id);
            $exclui->bindValue(":prof_id", $prof);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Visita excluida com sucesso",
            "type"=>"success",
        ];

        //reservado para log
        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg=Vvisita&id={$pessoa_id}");
        exit();
    }//fim da fnc delete

}
?>