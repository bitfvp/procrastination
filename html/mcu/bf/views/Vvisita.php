<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_42"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page = "Pessoa Atividades" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Formulário de pedidos de averiguação
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vvisita&aca=newvisita&id=<?php echo $_GET['id']; ?>" method="post">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Cad. novo:</label>
                                <select name="cad_novo" id="cad_novo" class="form-control">
                                    <option selected="" value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Transf. cidade:</label>
                                <select name="cad_transferencia" id="cad_transferencia" class="form-control">
                                    <option selected="" value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Cad. atualizado:</label>
                                <select name="cad_atualizado" id="cad_atualizado" class="form-control">
                                    <option selected="" value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Esta recebendo bolsa família:</label>
                                <select name="bolsa_familia" id="bolsa_familia" class="form-control">
                                    <option selected="" value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>

                            <div class="form-group col-md-5">
                                <label for="">Data:</label>
                                <input id="data_pedido" type="date" class="form-control" name="data_pedido" value="<?php echo date("Y-m-d"); ?>"/>
                            </div>

                            <div class="form-group col-md-7">
                                <label for="">Encaminha visita para:</label>
                                <select name="prof_resp" id="prof_resp" class="form-control">
                                    <option selected="" value="0">
                                        Selecione...
                                    </option>
                                    <?php
                                    foreach (fncusuarioencaminhamentolist2() as $item) {
                                        ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="7" name="descricao"></textarea>
                                <span id="cont">3000</span>/3000
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * "
                        ."FROM mcu_visitaaveriguacao "
                        ."WHERE (((mcu_visitaaveriguacao.pessoa)=?)) ORDER BY mcu_visitaaveriguacao.data_pedido DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $hist = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <a href="" target="" onclick="alert('entre en contato com o admin')">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão do prontuário</span>
                    </a>
                    <h5>
                        <?php
                        foreach ($hist as $at){?>
                            <hr>
                        <blockquote class="blockquote blockquote-inverse blockquote-primary ml-0 pl-0">
                            <blockquote class="blockquote blockquote-success">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i> <strong
                                            class="text-success"><?php echo $at['descricao']; ?></strong>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                Data do pedido:<strong class="text-info"><?php echo dataBanco2data($at['data_pedido']); ?>&nbsp;&nbsp;</strong>
                                <br>
                                Cadastro novo:<strong class="text-info"><?php if ($at['cad_novo']==0){echo "Não";}if ($at['cad_novo']==1){echo "Sim";}?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                <br>
                                Transferência de cidade:<strong class="text-info"><?php if ($at['cad_transferencia']==0){echo "Não";}if ($at['cad_transferencia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                <br>
                                Cadastro esta atualizado:<strong class="text-info"><?php if ($at['cad_atualizado']==0){echo "Não";}if ($at['cad_atualizado']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                <br>
                                Está recebendo bolsa família:<strong class="text-info"><?php if ($at['bolsa_familia']==0){echo "Não";}if ($at['bolsa_familia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                <br>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    Pedido feito por
                                    <?php
                                    $us=fncgetusuario($at['prof_cad']);
                                    echo $us['nome'];
                                    echo " <br>(".fncgetprofissao($us['profissao'])['profissao'].")";

                                    ///
                                    if ($at['prof_cad'] == $_SESSION['id'] and $at['status'] <> 1 ) {
                                    ?>
                                    <div class="dropdown show">
                                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apagar
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vvisita&id={$_GET['id']}&aca=excluirvisita&vi_id={$at['id']}";?>">Apagar</a>
                                        </div>
                                    </div>
                                    <h6>Não é possivel apagar após a averiguação feita</h6>
                                    <?php
                                    }
                                    ?>
                                </footer>
                            </blockquote>

                            <blockquote class="blockquote blockquote-<?php echo ($at['status_visita']==1) ? "success" : "info"; ?>">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i> <strong
                                            class="text-success"><?php echo $at['relatorio']; ?></strong>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                Status da visita:<strong class="text-info"><?php if ($at['status_visita']==0){echo "Aguardando visita";}if ($at['status_visita']==1){echo "Apurado";}if ($at['status_visita']==2){echo "Não encontrado";} ?>&nbsp&nbsp</strong>
                                <br>
                                Perfil cadunico:<strong class="text-info"><?php if ($at['perfil_cadunico']==0){echo "Não";}if ($at['perfil_cadunico']==1){echo "Sim";} ?>&nbsp&nbsp</strong>
                                <br>
                                Perfil bolsa família:<strong class="text-info"><?php if ($at['perfil_bolsafamilia']==0){echo "Não";}if ($at['perfil_bolsafamilia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>
                                <br>
                                Perfil benefício:<strong class="text-info"><?php if ($at['tipo_beneficio']==0){echo "Nenhum";}if ($at['tipo_beneficio']==1){echo "Variavel";}if ($at['tipo_beneficio']==2){echo "Basico/Variavel";} ?>&nbsp&nbsp</strong>
                                <br>
                                Data da visita:
                                <strong class="text-info">
                                    <?php
                                    if($at['data_visita']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01"){
                                        echo dataBanco2data($at['data_visita']);
                                    }
                                    ?>&nbsp;&nbsp;
                                </strong>
                                <footer class="blockquote-footer">
                                    Responsável pela visita e relatório: <br>
                                    <?php
                                    $us=fncgetusuario($at['prof_resp']);
                                    echo $us['nome'];
                                    echo " <br>(".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                        </blockquote>
                    <?php
                    }
                    ?>
                    </h5>
                    </p>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>