<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_41"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
            if ($allow["allow_42"] != 1) {
                header("Location: {$env->env_url_mod}");
                exit();
            } else {
                //ira abrir
            }
        }
    }
}

$page = "Minhas atividades-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Recebe
            $id_user = $_SESSION['id'];
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
            if ($allow["admin"]==1){
                $sql = "SELECT * \n"
                    . "FROM mcu_pb_at \n"
                    . "WHERE (profissional=?)\n"
                    . "ORDER BY data DESC LIMIT 0,1000";
            }else{
                $sql = "SELECT * \n"
                    . "FROM mcu_pb_at \n"
                    . "WHERE (profissional=? and status=1)\n"
                    . "ORDER BY data DESC LIMIT 0,1000";
            }
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_user);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mat = $consulta->fetchAll();
                $contar = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Minhas atividades - { <?php echo $contar ?> }
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach ($mat as $at) {
                            switch ($at['tipo']){
                                case 1:
                                    $cor="info";
                                    break;
                                case 2:
                                    $cor="danger";
                                    break;
                                case 3:
                                    $cor="warning";
                                    break;
                                case 4:
                                    $cor="success";
                                    break;
                                default:
                                    $cor="muted";
                                    break;
                            }
                            $status="";
                            if ($at['status']==0){
                                $status="bg-danger-light ";
                            }
                            ?>
                            <hr>
                            <div class="row <?php echo $status; ?>">
                                <div class="col-md-8">
                                    <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                        Pessoa:<strong class="text-info">
                                            <a href="?pg=Vat&id=<?php echo $at['pessoa']; ?>"><?php echo fncgetpessoa($at['pessoa'])['nome']; ?></a>
                                        </strong><br>
                                        Data:<strong class="text-info" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                        Atividade:<strong class="text-info"><?php echo fncgetpb_atlista($at['atividade'])['atividade'];?>&nbsp;&nbsp;</strong><br>
                                        Descrição:
                                        <i class="fa fa-quote-left fa-sm "></i>
                                        <strong class="text-success"><?php echo $at['descricao']; ?></strong>
                                        <i class="fa fa-quote-right fa-sm"></i>
                                        <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                        <?php
                                        if ($at['denuncia']!=0){
                                            if ($env->env_mod_nome==" Conselho Tutelar ") {
                                                echo "";
                                                echo "<a href='index.php?pg=Vd_hist&id={$at['pessoa']}&id_den={$at['denuncia']}'><h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6></a>";
                                            }else{
                                                echo "<h6 class='animated pulse infinite'><ins class='text-info'>(Essa atividade faz parte de uma pasta especial em Denúncias do Conselho Tutelar)</ins></h6>";
                                            }
                                        }
                                        ?>

                                        <?php
                                        if ($at['status']==0){
                                        $us=fncgetusuario($at['delete_prof']);
                                            echo "<footer class='blockquote-footer'>";
                                        echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                                            echo "</footer>";
                                        }
                                        ?>
                                    </blockquote>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    if ($at['id'] != 0) {
                                        $sql = "SELECT * FROM `mcu_pb_dados` where atividade='{$at['id']}' ";
                                        global $pdo;
                                        $consulta = $pdo->prepare($sql);
                                        $consulta->execute();
                                        $dados = $consulta->fetchAll();//$total[0]
                                        $sql = null;
                                        $consulta = null;

                                        foreach ($dados as $dado){
                                            switch ($dado['extensao']) {
                                                case "docx":
                                                    $caminho=$env->env_estatico . "img/docx.png";
                                                    $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    break;
                                                case "doc":
                                                    $caminho=$env->env_estatico . "img/doc.png";
                                                    $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    break;
                                                case "xls":
                                                    $caminho=$env->env_estatico . "img/xls.png";
                                                    $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    break;
                                                case "xlsx":
                                                    $caminho=$env->env_estatico . "img/xls.png";
                                                    $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    break;
                                                case "pdf":
                                                    $caminho=$env->env_estatico . "img/pdf.png";
                                                    $link="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    break;
                                                default:
                                                    $caminho="../../dados/mcu/protecaobasica/atividades/" . $dado['atividade'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                                    $link=$caminho;
                                                    break;
                                            }
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href='" . $link . "' target='_blank'>";
                                            echo "<img src='" . $caminho . "' alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                        }

                                    }//fim de foto?>
                                </div>
                            </div>
                            <br>
                        <?php } ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>