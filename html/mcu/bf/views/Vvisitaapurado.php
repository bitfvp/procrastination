<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
        //validação das permissoes
        if ($allow["allow_42"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}
$page = "Lista aguardando" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <?php
    $sql = "SELECT * \n"
        . "FROM mcu_visitaaveriguacao\n"
        . "WHERE (((mcu_visitaaveriguacao.status_visita)=1))";
    // total de registros a serem exibidos por página
    $total_reg = "30"; // número de registros por página
    //Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
    $pgn=$_GET['pgn'];
    if (!$pgn) {
        $pc = "1";
    } else {
        $pc = $pgn;
    }
    //Vamos determinar o valor inicial das buscas limitadas
    $inicio = $pc - 1;
    $inicio = $inicio * $total_reg;
    //Vamos selecionar os dados e exibir a paginação
    //limite
    try{
        $sql2= "ORDER BY mcu_visitaaveriguacao.data_pedido DESC LIMIT $inicio,$total_reg";
        global $pdo;
        $limite=$pdo->prepare($sql.$sql2);
        $limite->execute(); global $LQ; $LQ->fnclogquery($sql);

    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    //todos
    try{
        $sql2= "ORDER BY mcu_visitaaveriguacao.data_pedido DESC LIMIT $inicio,$total_reg";
        global $pdo;
        $todos=$pdo->prepare($sql);
        $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $tr=$todos->rowCount();// verifica o número total de registros
    $tp = $tr / $total_reg; // verifica o número total de páginas
    ?>

    <h1>Averiguações já apurados</h1>
    <!--        //começalista-->
    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">PESSOA</th>
            <th scope="col">DATA DO PEDIDO</th>
            <th scope="col">BAIRRO</th>
            <th scope="col">STATUS</th>
            <th scope="col">PROFISSIONAL</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vvisitaapurado&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vvisitaapurado&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th class="text-info float-right"><?php echo $todos->rowCount();?> Visita(s) listadas(s)</th>
        </tr>
        </tfoot>
        <?php
        // vamos criar a visualização de cestas
        while ($dados =$limite->fetch()){
        $vi_id = $dados["id"];
        $us = fncgetpessoa($dados["pessoa"]);
        $nome = $us['nome'];
        $data_pedido = dataBanco2data ($dados["data_pedido"]);
        $bairro = fncgetbairro($us['bairro'])["bairro"];
        $profissional= fncgetusuario($dados['prof_resp'])['nome'];
        $status_visita = $dados["status_visita"];
        $pe_id = $dados["pessoa"]; ?>
        <tbody>
        <tr>
            <th scope="row" id="<?php echo $id;  ?>">
                <a href="index.php?pg=Vvisita&id=<?php echo $pe_id; ?>" title="Para visualizar click e va a pasta da visita">
                    <?php echo $nome; ?>
                </a>
            </th>
            <td><?php echo $data_pedido; ?></td>
            <td><?php echo $bairro; ?></td>
            <td>
                <?php
                if($status_visita==0){echo"Aguardando visita";}
                if($status_visita==1){echo"Apurado";}
                if($status_visita==2){echo"Não encontrado";}
                ?>
            </td>
            <td><?php echo $profissional; ?></td>
        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>