<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();

//classe para debugar e salvar sqls ao banco de dados
include_once("{$env->env_root}models/LogQuery.class.php");//classe de log de query
$LQ = new LogQuery();

//classe com funcao para salvar os logs
include_once("{$env->env_root}models/mcu/Log.class.php");//inclui classe de log
$LL = new Log();

//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

////troca tema
include_once("{$env->env_root}controllers/trocatema.php");
////logout
include_once("{$env->env_root}controllers/logout.php");

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

//valida se há manutenção
include_once("{$env->env_root}controllers/validaManutencao.php");

//chat msg
include_once("{$env->env_root}models/mcu/Chat_Msg.class.php");
include_once("{$env->env_root}controllers/mcu/chat_msg.php");

/* inicio do Bloco dedidado*/
if (isset($_SESSION['logado']) and $_SESSION['logado']=="1"){

    include_once("{$env->env_root}controllers/usuario_lista.php");
    include_once("{$env->env_root}controllers/profissao_lista.php");

    include_once("{$env->env_root}controllers/mcu/bairro_lista.php");
    include_once("{$env->env_root}controllers/sexo_lista.php");

//include das class
    include_once("models/SalvarCategoria.class.php");
    include_once("models/SalvarUnidade.class.php");
    include_once("models/SalvarProduto.class.php");
    include_once("models/SalvarFornecedor.class.php");
    include_once("models/SalvarNotaEntrada.class.php");
    include_once("models/SalvarPedidoEntrada.class.php");
    include_once("models/SalvarNotaSaida.class.php");
    include_once("models/SalvarPedidoSaida.class.php");

//controllers
    include_once("controllers/categoriacarregareditar.php");
    include_once("controllers/categoriaedicao.php");
    include_once("controllers/categorianova.php");
    include_once("controllers/categorialista.php");

    include_once("controllers/fornecedoredicao.php");
    include_once("controllers/fornecedornovo.php");
//include_once("controllers/fornecedorlista.php");
    include_once("controllers/fornecedorcarregareditar.php");

    include_once("controllers/produtoedicao.php");
    include_once("controllers/produtonovo.php");
//include_once("controllers/produtolista.php");
    include_once("controllers/produtocarregareditar.php");

    include_once("controllers/unidadecarregareditar.php");
    include_once("controllers/unidadeedicao.php");
    include_once("controllers/unidadenovo.php");
    include_once("controllers/unidadelista.php");

    include_once("controllers/nota_entrada_edicao.php");
    include_once("controllers/nota_entrada_nova.php");
//include_once("controllers/nota_entrada_lista.php");
    include_once("controllers/nota_entrada_carregareditar.php");

    include_once("controllers/nota_saida_edicao.php");
    include_once("controllers/nota_saida_nova.php");
//include_once("controllers/nota_entrada_lista.php");
    include_once("controllers/nota_saida_carregareditar.php");

    include_once("controllers/pedidonovo.php");

}
/* fim do bloco dedicado*/


//metodo de checar usuario
include_once("{$env->env_root}controllers/confirmUser.php");