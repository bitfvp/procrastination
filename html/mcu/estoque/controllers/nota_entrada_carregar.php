<?php
    if(isset($_GET['id'])) {
        $idpessoa =$_GET['id'];
        //existe um id e se ele é numérico
        if (!empty($idpessoa) && is_numeric($idpessoa)) {
            // Captura os dados do cliente solicitado
            $sql = "SELECT * FROM mcu_estoque_notas_entrada WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $idpessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $notas_entrada = $consulta->fetch();

            $sql=null;
            $consulta=null;
        }
    }
?>