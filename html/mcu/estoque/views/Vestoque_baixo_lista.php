<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Estoque Baixo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");



if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_estoque WHERE produto LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_estoque ";
}
// total de registros a serem exibidos por página
$total_reg = "100"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->

<h2>Estoque Baixo</h2>
<form class="" action="index.php" method="get">
                        <input name="pg" value="Vestoque_baixo_lista" hidden/>
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            <input type="text" autofocus="true" autocomplete="off" class="form-control col-lg-8" placeholder="Buscar por Produto..." name="sca" value="" />
                            </div>
</form>

<table id="tabela" class="table table-striped table-bordered table-hover">

<thead>
	<tr>
		<th>Produto</th>
		<th>Uni.</th>
        <th class="bg-info">Estoque</th>
		<th><abbr title="Estoque do Cras" class="initialism">A</abbr></th>
		<th><abbr title="Estoque do Creas" class="initialism">B</abbr></th>
        <th><abbr title="Estoque do Casi" class="initialism">C</abbr></th>
        <th><abbr title="Estoque do Casa Lar" class="initialism">D</abbr></th>
        <th><abbr title="Estoque do Peti" class="initialism">E</abbr></th>
        <th><abbr title="Estoque do Albergue" class="initialism">F</abbr></th>
        <th><abbr title="Estoque do Bolsa Familia" class="initialism">G</abbr></th>
        <th><abbr title="Estoque da SMTDS" class="initialism">H</abbr></th>
        <th><abbr title="Estoque do Conselho Tutelar" class="initialism">I</abbr></th>
        <th><abbr title="Estoque dos Conselhos" class="initialism">J</abbr></th>
        <th><abbr title="Estoque da AABB" class="initialism">K</abbr></th>
        <th><abbr title="Estoque da SCFV" class="initialism">L</abbr></th>
        <th>Editar</th>
	</tr>
</thead>
<tbody>
 <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $produto = $dados["produto"];
            $unidade = $dados["unidade"];
            $categoria = $dados["categoria"];
            $estoque_min = $dados["estoque_min"];

            $sql = "SELECT mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo, Sum(mcu_estoque_pedidos.estoque_total) AS SomaDeestoque_total, Sum(mcu_estoque_pedidos.estoque_a) AS SomaDeestoque_a, Sum(mcu_estoque_pedidos.estoque_b) AS SomaDeestoque_b, Sum(mcu_estoque_pedidos.estoque_c) AS SomaDeestoque_c, Sum(mcu_estoque_pedidos.estoque_d) AS SomaDeestoque_d, Sum(mcu_estoque_pedidos.estoque_e) AS SomaDeestoque_e, Sum(mcu_estoque_pedidos.estoque_f) AS SomaDeestoque_f, Sum(mcu_estoque_pedidos.estoque_g) AS SomaDeestoque_g, Sum(mcu_estoque_pedidos.estoque_h) AS SomaDeestoque_h, Sum(mcu_estoque_pedidos.estoque_i) AS SomaDeestoque_i, Sum(mcu_estoque_pedidos.estoque_j) AS SomaDeestoque_j, Sum(mcu_estoque_pedidos.estoque_k) AS SomaDeestoque_k, Sum(mcu_estoque_pedidos.estoque_l) AS SomaDeestoque_l
FROM mcu_estoque INNER JOIN mcu_estoque_pedidos ON mcu_estoque.id = mcu_estoque_pedidos.produto_id
GROUP BY mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo
HAVING (((mcu_estoque_pedidos.produto_id)={$id}) AND ((mcu_estoque_pedidos.tipo)=1))
ORDER BY mcu_estoque.produto";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $entradasoma = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $sql = "SELECT mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo, Sum(mcu_estoque_pedidos.estoque_total) AS SomaDeestoque_total, Sum(mcu_estoque_pedidos.estoque_a) AS SomaDeestoque_a, Sum(mcu_estoque_pedidos.estoque_b) AS SomaDeestoque_b, Sum(mcu_estoque_pedidos.estoque_c) AS SomaDeestoque_c, Sum(mcu_estoque_pedidos.estoque_d) AS SomaDeestoque_d, Sum(mcu_estoque_pedidos.estoque_e) AS SomaDeestoque_e, Sum(mcu_estoque_pedidos.estoque_f) AS SomaDeestoque_f, Sum(mcu_estoque_pedidos.estoque_g) AS SomaDeestoque_g, Sum(mcu_estoque_pedidos.estoque_h) AS SomaDeestoque_h, Sum(mcu_estoque_pedidos.estoque_i) AS SomaDeestoque_i, Sum(mcu_estoque_pedidos.estoque_j) AS SomaDeestoque_j, Sum(mcu_estoque_pedidos.estoque_k) AS SomaDeestoque_k, Sum(mcu_estoque_pedidos.estoque_l) AS SomaDeestoque_l
FROM mcu_estoque INNER JOIN mcu_estoque_pedidos ON mcu_estoque.id = mcu_estoque_pedidos.produto_id
GROUP BY mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo
HAVING (((mcu_estoque_pedidos.produto_id)={$id}) AND ((mcu_estoque_pedidos.tipo)=2))
ORDER BY mcu_estoque.produto";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $saidasoma = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $estoque = $entradasoma["SomaDeestoque_total"] - $saidasoma["SomaDeestoque_total"];
            $estoque_1 = $entradasoma["SomaDeestoque_a"] - $saidasoma["SomaDeestoque_a"];
            $estoque_2 = $entradasoma["SomaDeestoque_b"] - $saidasoma["SomaDeestoque_b"];
            $estoque_3 = $entradasoma["SomaDeestoque_c"] - $saidasoma["SomaDeestoque_c"];
            $estoque_4 = $entradasoma["SomaDeestoque_d"] - $saidasoma["SomaDeestoque_d"];
            $estoque_5 = $entradasoma["SomaDeestoque_e"] - $saidasoma["SomaDeestoque_e"];
            $estoque_6 = $entradasoma["SomaDeestoque_f"] - $saidasoma["SomaDeestoque_f"];
            $estoque_7 = $entradasoma["SomaDeestoque_g"] - $saidasoma["SomaDeestoque_g"];
            $estoque_8 = $entradasoma["SomaDeestoque_h"] - $saidasoma["SomaDeestoque_h"];
            $estoque_9 = $entradasoma["SomaDeestoque_i"] - $saidasoma["SomaDeestoque_i"];
            $estoque_10 = $entradasoma["SomaDeestoque_j"] - $saidasoma["SomaDeestoque_j"];
            $estoque_11 = $entradasoma["SomaDeestoque_k"] - $saidasoma["SomaDeestoque_k"];
            $estoque_12 = $entradasoma["SomaDeestoque_l"] - $saidasoma["SomaDeestoque_l"];
 if ($estoque < $estoque_min) {
     ?>

     <tr>
         <td><?php
             if ($_GET['sca'] != "") {
                 $sta = strtoupper($_GET['sca']);
                 define('CSA', $sta);//TESTE
                 $sta = CSA;
                 $ccc = $produto;
                 $cc = explode(CSA, $ccc);
                 $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                 echo strtoupper($c);
             } else {
                 echo $produto;
             }
             ?>
             <a href="index.php?pg=Vproduto_historico&id=<?php echo $id; ?>"><span
                         class="glyphicon glyphicon-tasks pull-right"></span></a>
         </td>
         <td>
             <?php
             if ($unidade != "0") {
                 include_once("controllers/unidadelista.php");
                 $unidadeid = $unidade;
                 $unidadeid -= 1;
                 echo $unidadelista[$unidadeid]['unidade'];
             } else {
                 echo "???????";
             }
             ?>
         </td>
         <td class="bg-info">
             <?php
             if ($estoque < $estoque_min) {
                 echo "<abbr title=\"Estoque Minimo {$estoque_min}\" class=\"initialism\"><span class=\"label label-danger\">{$estoque}</span></abbr>";
             } else {
                 echo "<abbr title=\"Estoque Minimo {$estoque_min}\" class=\"initialism\"><span class=\"label label-success\">{$estoque}</span></abbr>";
             }

             ?>
         </td>
         <td><?php echo $estoque_1; ?></td>
         <td><?php echo $estoque_2; ?></td>
         <td><?php echo $estoque_3; ?></td>
         <td><?php echo $estoque_4; ?></td>
         <td><?php echo $estoque_5; ?></td>
         <td><?php echo $estoque_6; ?></td>
         <td><?php echo $estoque_7; ?></td>
         <td><?php echo $estoque_8; ?></td>
         <td><?php echo $estoque_9; ?></td>
         <td><?php echo $estoque_10; ?></td>
         <td><?php echo $estoque_11; ?></td>
         <td><?php echo $estoque_12; ?></td>
         <td><a href="index.php?pg=Vproduto_editar&acb=carregarproduto&id=<?php echo $id; ?>"><span
                         class="fa fa-pen"></span></a></td>
     </tr>

     <?php
 }
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
