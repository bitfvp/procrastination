<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de Fornecedores-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");



if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_estoque_fornecedor WHERE razao_social LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_estoque_fornecedor ";
}
// total de registros a serem exibidos por página
$total_reg = "100"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY razao_social LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY razao_social LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->

<h2>Listagem de Fornecedores</h2>
<form class="" action="index.php" method="get">
                        <input name="pg" value="Vfornecedor_lista" hidden/>
                            <div class="input-group">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                            </span>
                            <input type="text" autofocus="true" autocomplete="off" class="form-control col-lg-8" placeholder="Buscar por Produto..." name="sca" value="" />
                            </div>
</form>
    <table id="tabela" class="table table-striped table-bordered table-hover">
        <a href="index.php?pg=Vfornecedor_editar&acb=novofornecedor" class="btn btn btn-success btn-block">
            Novo Fornecedor
        </a>
        <br>
        <thead>
        <tr>
            <th>RAZÃO SOCIAL</th>
            <th>CNPJ</th>
            <th>TELEFONE</th>
            <th>EDITAR</th>
        </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $razao_social = $dados["razao_social"];
            $cnpj = $dados["cnpj"];
            $telefone = $dados["telefone"];
            ?>

            <tr data-toggle="collapse" data-target="#accordion<?php echo $id; ?>" class="clickable">
            <td>
                <?php
                if($_GET['sca']!="") {
                    $sta = strtoupper($_GET['sca']);
                    define('CSA', $sta);
                    $sta = CSA;
                    $ccc = $razao_social;
                    $cc = explode(CSA, $ccc);
                    $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                    echo strtoupper($c);
                }else{
                    echo $razao_social;
                }
                ?>
            </td>
            <td><?php echo $cnpj; ?></td>
            <td><?php echo $telefone; ?></td>
            <td><a href="index.php?pg=Vfornecedor_editar&acb=carregarfornecedor&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a></td>
            </tr>

            <?php
            $sql = "SELECT mcu_estoque_notas_entrada.id, mcu_estoque_notas_entrada.data, mcu_estoque_notas_entrada.identificador, mcu_estoque_notas_entrada.fornecedor, mcu_estoque_notas_entrada.responsavel, mcu_estoque_notas_entrada.valor FROM mcu_estoque_notas_entrada WHERE (((mcu_estoque_notas_entrada.fornecedor)=:id)) ORDER BY mcu_estoque_notas_entrada.data DESC;";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(id , $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $lnot = $consulta->fetchAll();

            $sql=null;
            $consulta=null;
            ?>
            <tr id="accordion<?php echo $id; ?>" class="collapse bg-info">
                <td colspan="4">
            <?php
                foreach ($lnot as $not_in) { ?>


                        <div class="row">
                            <div class="col-md-4">
                                <ul>
                                    Data:
                                    <li><?php echo databanco2data($not_in['data']); ?></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    Nota:
                                    <li>
                                        <?php
                                        echo "<a href=\"index.php?pg=Vnota_entrada&id={$not_in['id']}\">";
                                        echo $not_in['identificador'];
                                        echo "</a>";
                                        ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <hr>



                <?php
            }
            ?>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>