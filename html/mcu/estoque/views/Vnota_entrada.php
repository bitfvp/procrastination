<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Nota de Entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <!-- direito -->
        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados da Nota</h3>
                </div>
                <?php include_once("controllers/nota_entrada_carregar.php"); ?>
                <?php
                if (isset($_GET['sucesso']) and $_GET['sucesso'] == "s") {
                    $_SESSION['fsh']=[
                        "flash"=>"Ação feita com SUCESSO!!",
                        "type"=>"success",
                    ];
                }
                ?>
                <blockquote>
                    <header>
                        Identificador:<strong class="text-info"><?php echo $notas_entrada['identificador']; ?>&nbsp;&nbsp;</strong>
                    </header>
                    <p>
                    <h5>
                        Data:<strong class="text-info"><?php echo dataBanco2data($notas_entrada['data']); ?>
                            &nbsp;&nbsp;</strong>
                        Valor da Nota:<strong
                                class="text-info"><?php echo "R$ " . number_format($notas_entrada['valor'], 2); ?>&nbsp;&nbsp;</strong>
                        Fornecedor:
                        <strong class="text-info">
                            <?php
                            if ($notas_entrada['fornecedor'] != "0") {
                                $sql = "SELECT * FROM mcu_estoque_fornecedor";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                $flista = $consulta->fetchAll();
                                $sql = null;
                                $consulta = null;
                                $fid = $notas_entrada['fornecedor'];
                                $fid -= 1;
                                echo $flista[$fid]['razao_social'];
                            } else {
                                echo "<span class='vermelho underline'>";
                                echo "???????";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;&nbsp;
                    </h5>
                    </p>
                    <footer>
                        Responsavel
                        <strong class="text-info"><?php
                            if ($notas_entrada['responsavel'] != "0") {
                                $sql = "SELECT * FROM tbl_users";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                $plista = $consulta->fetchAll();
                                $sql = null;
                                $consulta = null;
                                $pid = $notas_entrada['responsavel'];
                                $pid -= 1;
                                echo "<span class='text-muted'>";
                                echo $plista[$pid]['nome'];
                                echo "</span>";
                            } else {
                                echo "<span class='vermelho underline'>";
                                echo "???????";
                                echo "</span>";
                            }
                            ?></strong>&nbsp;&nbsp;
                    </footer>
                </blockquote>

                <a class="btn btn-success btn-block"
                   href="?pg=Vnota_entrada_editar&acb=carregarnotaentrada&id=<?php echo $_GET['id']; ?>"
                   title="Edite nota">
                    Editar Nota
                </a>
            </div>
            <!-- fim da col md 4 -->
        </div>


        <!-- esquerdo -->
        <div class="col-md-8">
            <div class="row-fluid">
                <!--  -->
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Formúlario de Novos Pedidos</h3>
                        </div>
                        <div class="panel-body">
                            <p>

                            <form class="form-signin"
                                  action="index.php?pg=Vnota_entrada&aca=salvarpedidoentradanovo&id=<?php echo $_GET['id']; ?>"
                                  method="post">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-sm btn-success " value="Salvar Pedido"/>
                                </div>
                                <div class="col-md-6">
                                    <label for="produto_id">Produto</label>
        <select name="produto_id" autofocus="yes" id="produto_id" class="form-control input-sm selectpicker" data-live-search="true">
            // vamos criar a visualização
            <?php
            include_once("controllers/produtolista.php");

             foreach ($produtolista as $item) {
                ?>
                <option data-tokens="<?php echo $item['produto']; ?>" value="<?php echo $item['id']; ?>"><?php echo $item['produto']; ?></option>
                <?php
            }
            ?>
        </select>
<!--                                    <script>-->
<!--                                        $(function() {-->
<!---->
<!--                                            var focus = setTimeout(function() {-->
<!--                                                $("#produto_id").focus();-->
<!--                                            }, 5000); // 5000 = 5 segundos-->
<!---->
<!--                                        });-->
<!--                                    </script>-->
                                </div>
                                <div class="col-md-6">
                                    <input autocomplete="off" id="nota_id" required="yes" type="hidden"
                                           class="form-control input-sm" name="nota_id"
                                           value="<?php echo $_GET['id']; ?>"/>
                                    <input autocomplete="off"  id="tipo" required="yes" type="hidden"
                                           class="form-control input-sm" name="tipo" value="1"/>
                                    <label for="obs">Obs</label><input autocomplete="off"  id="obs" type="text"
                                                                       class="form-control" name="obs"
                                                                       value=""/>
                                </div>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-4">
                                    <label for="estoque_a">CRAS</label><input autocomplete="off"
                                                                                      id="estoque_a" required="yes"
                                                                                      type="text"
                                                                                      class="form-control input-sm"
                                                                                      name="estoque_a" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_b">CREAS</label><input autocomplete="off"
                                                                                       id="estoque_b" required="yes"
                                                                                       type="text"
                                                                                       class="form-control input-sm"
                                                                                       name="estoque_b" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_c">CASA</label><input autocomplete="off"
                                                                                      id="estoque_c" required="yes"
                                                                                      type="text"
                                                                                      class="form-control input-sm"
                                                                                      name="estoque_c" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_d">CASA LAR</label><input autocomplete="off"
                                                                                          id="estoque_d" required="yes"
                                                                                          type="text"
                                                                                          class="form-control input-sm"
                                                                                          name="estoque_d" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_e">PETI</label><input autocomplete="off"
                                                                                      id="estoque_e" required="yes"
                                                                                      type="text"
                                                                                      class="form-control input-sm"
                                                                                      name="estoque_e" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_f">ALBERGUE</label><input autocomplete="off"
                                                                                          id="estoque_f" required="yes"
                                                                                          type="text"
                                                                                          class="form-control input-sm"
                                                                                          name="estoque_f" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_g">BOLSA FAMILIA</label><input autocomplete="off"
                                                                                                id="estoque_g"
                                                                                               required="yes"
                                                                                               type="text"
                                                                                               class="form-control input-sm"
                                                                                               name="estoque_g"
                                                                                               value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_h">SMTDS</label><input autocomplete="off"
                                                                                       id="estoque_h" required="yes"
                                                                                       type="text"
                                                                                       class="form-control input-sm"
                                                                                       name="estoque_h" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_i">CONSELHO TUTELAR</label><input autocomplete="off"

                                                                                                   id="estoque_i"
                                                                                                   required="yes"
                                                                                                   type="text"
                                                                                                   class="form-control input-sm"
                                                                                                   name="estoque_i"
                                                                                                   value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_j">CONSELHOS</label><input autocomplete="off"
                                                                                           id="estoque_j" required="yes"
                                                                                           type="text"
                                                                                           class="form-control input-sm"
                                                                                           name="estoque_j" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_k">AABB</label><input autocomplete="off"
                                                                                      id="estoque_k" required="yes"
                                                                                      type="text"
                                                                                      class="form-control input-sm"
                                                                                      name="estoque_k" value="0"/>
                                </div>
                                <div class="col-md-4">
                                    <label for="estoque_l">SCFV</label><input autocomplete="off"
                                                                                      id="estoque_l" required="yes"
                                                                                      type="text"
                                                                                      class="form-control input-sm"
                                                                                      name="estoque_l" value="0"/>
                                </div>
                                <hr>

                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>


                <div class="col-md-12">
                    <?php
                    $sql = "SELECT mcu_estoque_pedidos.id, mcu_estoque.produto, mcu_estoque.unidade, mcu_estoque.categoria, mcu_estoque_pedidos.nota_id, mcu_estoque_pedidos.tipo, mcu_estoque_pedidos.estoque_total, mcu_estoque_pedidos.estoque_a, mcu_estoque_pedidos.estoque_b, mcu_estoque_pedidos.estoque_c, mcu_estoque_pedidos.estoque_d, mcu_estoque_pedidos.estoque_e, mcu_estoque_pedidos.estoque_f, mcu_estoque_pedidos.estoque_g, mcu_estoque_pedidos.estoque_h, mcu_estoque_pedidos.estoque_i, mcu_estoque_pedidos.estoque_j, mcu_estoque_pedidos.estoque_k, mcu_estoque_pedidos.estoque_l, mcu_estoque_pedidos.data\n"
                        . "FROM mcu_estoque INNER JOIN mcu_estoque_pedidos ON mcu_estoque.id = mcu_estoque_pedidos.produto_id\n"
                        . "WHERE (((mcu_estoque_pedidos.nota_id)=?) AND ((mcu_estoque_pedidos.tipo)=1))\n"
                        . "ORDER BY mcu_estoque_pedidos.data DESC";
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $pedlista = $cons->fetchAll();
                    $sql = null;
                    $consulta = null;
                    ?>
                    <!-- tabela -->
                    <table id="tabela" class="table table-bordered table-hover">
                        <br>
                        <thead class="bg-info">
                        <tr>
                            <th>Produto</th>
                            <th>Quant.</th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Cras!"><img src="<?php echo $env->env_estatico;?>img/crasmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Creas!"><img src="<?php echo $env->env_estatico;?>img/creasmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casi!"><img src="<?php echo $env->env_estatico;?>img/casimini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casa Lar!"><img src="<?php echo $env->env_estatico;?>img/casalarmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Peti!"><img src="<?php echo $env->env_estatico;?>img/petimini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Albergue!"><img src="<?php echo $env->env_estatico;?>img/alberguemini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Bolsa Familia!"><img src="<?php echo $env->env_estatico;?>img/bfmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SMTDS!"><img src="<?php echo $env->env_estatico;?>img/smtds.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Conselho Tutelar!"><img src="<?php echo $env->env_estatico;?>img/ctmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque dos Conselhos!"><img src="<?php echo $env->env_estatico;?>img/concmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da AABB!"><img src="<?php echo $env->env_estatico;?>img/aabbmini.png" alt="" style="height: 30px;"></a></th>
                            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SCFV!"><img src="<?php echo $env->env_estatico;?>img/scfvmini.png" alt="" style="height: 30px;"></a></th>
                            <th>ID</th>
                        </tr>
                        </thead>
                        <script>
                            $(document).ready(function(){
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                        </script>
                        <tbody class="bg-success">
                        <?php
                        // vamos criar a visualização
                        foreach ($pedlista as $dados) {
                            $id = $dados["id"];
                            $produto = $dados["produto"];
                            $quant_total = $dados["estoque_total"];
                            $estoque_1 = $dados["estoque_a"];
                            $estoque_2 = $dados["estoque_b"];
                            $estoque_3 = $dados["estoque_c"];
                            $estoque_4 = $dados["estoque_d"];
                            $estoque_5 = $dados["estoque_e"];
                            $estoque_6 = $dados["estoque_f"];
                            $estoque_7 = $dados["estoque_g"];
                            $estoque_8 = $dados["estoque_h"];
                            $estoque_9 = $dados["estoque_i"];
                            $estoque_10 = $dados["estoque_j"];
                            $estoque_11 = $dados["estoque_k"];
                            $estoque_12 = $dados["estoque_l"];
                            ?>

                            <tr>
                                <td><?php
                                    echo $produto;
                                    ?>
                                </td>
                                <td><?php echo $quant_total; ?></td>
                                <td><?php echo $estoque_1; ?></td>
                                <td><?php echo $estoque_2; ?></td>
                                <td><?php echo $estoque_3; ?></td>
                                <td><?php echo $estoque_4; ?></td>
                                <td><?php echo $estoque_5; ?></td>
                                <td><?php echo $estoque_6; ?></td>
                                <td><?php echo $estoque_7; ?></td>
                                <td><?php echo $estoque_8; ?></td>
                                <td><?php echo $estoque_9; ?></td>
                                <td><?php echo $estoque_10; ?></td>
                                <td><?php echo $estoque_11; ?></td>
                                <td><?php echo $estoque_12; ?></td>
                                <td class="info"><i class="fa fa-star"> </i><?php echo $id; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>