<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Produto Historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php

    if(isset($_GET['id'])) {
        $idproduto =$_GET['id'];
        //existe um id e se ele é numérico
        if (!empty($idproduto) && is_numeric($idproduto)) {
            // Captura os dados do cliente solicitado
            $sql = "SELECT * FROM mcu_estoque WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $idproduto);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $produto = $consulta->fetch();

            $sql=null;
            $consulta=null;
        }
    }

    ///começa a verificação de estoque
$sql = "SELECT mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo, Sum(mcu_estoque_pedidos.estoque_total) AS SomaDeestoque_total, Sum(mcu_estoque_pedidos.estoque_a) AS SomaDeestoque_a, Sum(mcu_estoque_pedidos.estoque_b) AS SomaDeestoque_b, Sum(mcu_estoque_pedidos.estoque_c) AS SomaDeestoque_c, Sum(mcu_estoque_pedidos.estoque_d) AS SomaDeestoque_d, Sum(mcu_estoque_pedidos.estoque_e) AS SomaDeestoque_e, Sum(mcu_estoque_pedidos.estoque_f) AS SomaDeestoque_f, Sum(mcu_estoque_pedidos.estoque_g) AS SomaDeestoque_g, Sum(mcu_estoque_pedidos.estoque_h) AS SomaDeestoque_h, Sum(mcu_estoque_pedidos.estoque_i) AS SomaDeestoque_i, Sum(mcu_estoque_pedidos.estoque_j) AS SomaDeestoque_j, Sum(mcu_estoque_pedidos.estoque_k) AS SomaDeestoque_k, Sum(mcu_estoque_pedidos.estoque_l) AS SomaDeestoque_l
FROM mcu_estoque INNER JOIN mcu_estoque_pedidos ON mcu_estoque.id = mcu_estoque_pedidos.produto_id
GROUP BY mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo
HAVING (((mcu_estoque_pedidos.produto_id)={$_GET['id']}) AND ((mcu_estoque_pedidos.tipo)=1))
ORDER BY mcu_estoque.produto";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$entradasoma = $consulta->fetch();
$sql=null;
$consulta=null;

$sql = "SELECT mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo, Sum(mcu_estoque_pedidos.estoque_total) AS SomaDeestoque_total, Sum(mcu_estoque_pedidos.estoque_a) AS SomaDeestoque_a, Sum(mcu_estoque_pedidos.estoque_b) AS SomaDeestoque_b, Sum(mcu_estoque_pedidos.estoque_c) AS SomaDeestoque_c, Sum(mcu_estoque_pedidos.estoque_d) AS SomaDeestoque_d, Sum(mcu_estoque_pedidos.estoque_e) AS SomaDeestoque_e, Sum(mcu_estoque_pedidos.estoque_f) AS SomaDeestoque_f, Sum(mcu_estoque_pedidos.estoque_g) AS SomaDeestoque_g, Sum(mcu_estoque_pedidos.estoque_h) AS SomaDeestoque_h, Sum(mcu_estoque_pedidos.estoque_i) AS SomaDeestoque_i, Sum(mcu_estoque_pedidos.estoque_j) AS SomaDeestoque_j, Sum(mcu_estoque_pedidos.estoque_k) AS SomaDeestoque_k, Sum(mcu_estoque_pedidos.estoque_l) AS SomaDeestoque_l
FROM mcu_estoque INNER JOIN mcu_estoque_pedidos ON mcu_estoque.id = mcu_estoque_pedidos.produto_id
GROUP BY mcu_estoque.produto, mcu_estoque_pedidos.produto_id, mcu_estoque_pedidos.tipo
HAVING (((mcu_estoque_pedidos.produto_id)={$_GET['id']}) AND ((mcu_estoque_pedidos.tipo)=2))
ORDER BY mcu_estoque.produto";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$saidasoma = $consulta->fetch();
$sql=null;
$consulta=null;

$estoque = $entradasoma["SomaDeestoque_total"] - $saidasoma["SomaDeestoque_total"];
$estoque_1 = $entradasoma["SomaDeestoque_a"] - $saidasoma["SomaDeestoque_a"];
$estoque_2 = $entradasoma["SomaDeestoque_b"] - $saidasoma["SomaDeestoque_b"];
$estoque_3 = $entradasoma["SomaDeestoque_c"] - $saidasoma["SomaDeestoque_c"];
$estoque_4 = $entradasoma["SomaDeestoque_d"] - $saidasoma["SomaDeestoque_d"];
$estoque_5 = $entradasoma["SomaDeestoque_e"] - $saidasoma["SomaDeestoque_e"];
$estoque_6 = $entradasoma["SomaDeestoque_f"] - $saidasoma["SomaDeestoque_f"];
$estoque_7 = $entradasoma["SomaDeestoque_g"] - $saidasoma["SomaDeestoque_g"];
$estoque_8 = $entradasoma["SomaDeestoque_h"] - $saidasoma["SomaDeestoque_h"];
$estoque_9 = $entradasoma["SomaDeestoque_i"] - $saidasoma["SomaDeestoque_i"];
$estoque_10 = $entradasoma["SomaDeestoque_j"] - $saidasoma["SomaDeestoque_j"];
$estoque_11 = $entradasoma["SomaDeestoque_k"] - $saidasoma["SomaDeestoque_k"];
$estoque_12 = $entradasoma["SomaDeestoque_l"] - $saidasoma["SomaDeestoque_l"];
?>

<div class="row">
<!-- direito -->
<div class="col-md-8">
<div class="panel panel-info">
<div class="panel-heading">
<h3 class="panel-title">Produto</h3>
</div>

<blockquote>
<header>
PRODUTO:<strong class="text-info"><?php echo $produto['produto']; ?>&nbsp;&nbsp;</strong>
</header>
<p>
<h5>
CATEGORIA:<strong class="text-info">
        <?php
        if ($produto['categoria']!="0"){
            include_once("controllers/categorialista.php");
            $categoriaid=$produto['categoria'];
            $categoriaid-=1;
            echo $categorialista[$categoriaid]['categoria'];
            }else{
                echo "???????";
            }
         ?>
&nbsp;&nbsp;</strong>
</h5>
<h5>
UNIDADE:<strong class="text-info">
        <?php
        if ($produto['unidade']!="0"){
            include_once("controllers/unidadelista.php");
            $unidadeid=$produto['unidade'];
            $unidadeid-=1;
            echo $unidadelista[$unidadeid]['unidade'];
            }else{
                echo "???????";
            }
         ?>
&nbsp;&nbsp;</strong>
</h5>
<h4>

ESTOQUE:<strong class="text-info">
        <?php
        if ($estoque < $produto['estoque_min']) {
            echo "<abbr title=\"Estoque Minimo {$produto['estoque_min']}\" class=\"initialism\"><span class=\"label label-danger\">{$estoque}</span></abbr>";
        }else{
            echo "<abbr title=\"Estoque Minimo {$produto['estoque_min']}\" class=\"initialism\"><span class=\"label label-success\">{$estoque}</span></abbr>";
        }

         ?>
&nbsp;&nbsp;</strong>
</h4>

<table class="table table-striped table-bordered table-hover text-center">
    <thead>
        <tr>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Cras!"><img src="<?php echo $env->env_estatico;?>img/crasmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Creas!"><img src="<?php echo $env->env_estatico;?>img/creasmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casi!"><img src="<?php echo $env->env_estatico;?>img/casimini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casa Lar!"><img src="<?php echo $env->env_estatico;?>img/casalarmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Peti!"><img src="<?php echo $env->env_estatico;?>img/petimini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Albergue!"><img src="<?php echo $env->env_estatico;?>img/alberguemini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Bolsa Familia!"><img src="<?php echo $env->env_estatico;?>img/bfmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SMTDS!"><img src="<?php echo $env->env_estatico;?>img/smtds.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Conselho Tutelar!"><img src="<?php echo $env->env_estatico;?>img/ctmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque dos Conselhos!"><img src="<?php echo $env->env_estatico;?>img/concmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da AABB!"><img src="<?php echo $env->env_estatico;?>img/aabbmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SCFV!"><img src="<?php echo $env->env_estatico;?>img/scfvmini.png" alt="" style="height: 30px;"></a></th>
        </tr>
    </thead>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <tbody>
        <tr>
            <td><?php echo $estoque_1; ?></td>
            <td><?php echo $estoque_2; ?></td>
            <td><?php echo $estoque_3; ?></td>
            <td><?php echo $estoque_4; ?></td>
            <td><?php echo $estoque_5; ?></td>
            <td><?php echo $estoque_6; ?></td>
            <td><?php echo $estoque_7; ?></td>
            <td><?php echo $estoque_8; ?></td>
            <td><?php echo $estoque_9; ?></td>
            <td><?php echo $estoque_10; ?></td>
            <td><?php echo $estoque_11; ?></td>
            <td><?php echo $estoque_12; ?></td>
        </tr>
    </tbody>
</table>



</p>
</blockquote>
<a class="btn btn-success btn-block" href="index.php?pg=Vproduto_editar&acb=carregarproduto&id=<?php echo $_GET['id']; ?>" title="Edite nota">
Editar Produto
</a>
</div>
<!-- fim da col md 4 -->
</div>


<!-- esquerdo -->
<div class="col-md-10">
<?php
$sql = "SELECT mcu_estoque_pedidos.id, mcu_estoque_pedidos.tipo, mcu_estoque_pedidos.produto_id, mcu_estoque.produto, mcu_estoque_pedidos.estoque_total, mcu_estoque_pedidos.data, tbl_users.nome AS responsavel, mcu_estoque_pedidos.nota_id, mcu_estoque_pedidos.estoque_a, mcu_estoque_pedidos.estoque_b, mcu_estoque_pedidos.estoque_c, mcu_estoque_pedidos.estoque_d, mcu_estoque_pedidos.estoque_e, mcu_estoque_pedidos.estoque_f, mcu_estoque_pedidos.estoque_g, mcu_estoque_pedidos.estoque_h, mcu_estoque_pedidos.estoque_i, mcu_estoque_pedidos.estoque_j, mcu_estoque_pedidos.estoque_k, mcu_estoque_pedidos.estoque_l\n"
    . "FROM mcu_estoque INNER JOIN (tbl_users INNER JOIN mcu_estoque_pedidos ON tbl_users.id = mcu_estoque_pedidos.responsavel) ON mcu_estoque.id = mcu_estoque_pedidos.produto_id\n"
    . "WHERE (((mcu_estoque_pedidos.produto_id)=?))\n"
    . "ORDER BY mcu_estoque_pedidos.data DESC";
global $pdo;
$cons = $pdo->prepare($sql);
$cons->bindParam(1, $_GET['id']);
$cons->execute(); global $LQ; $LQ->fnclogquery($sql);
$pedlista = $cons->fetchAll();
$sql=null;
$consulta=null;
?>
<table id="tabela" class="table table-striped table-bordered table-hover">
<thead class="bg-primary">
<tr>
<th>Tipo</th>
<th>Produto</th>
<th>Quant.</th>
<th>Data</th>
</tr>
</thead>
<tbody>

<?php
// vamos criar a visualização
foreach ($pedlista as $dados) {
$id = $dados["id"];
$produto_id = $dados["produto_id"];
$produto = $dados["produto"];
$data= $dados["data"];
$tipo=$dados["tipo"];
$nota_id=$dados["nota_id"];
$responsavel=$dados["responsavel"];
$categoria=$dados["categoria"];
$quant_total = $dados["estoque_total"];
$estoque_1 = $dados["estoque_a"];
$estoque_2 = $dados["estoque_b"];
$estoque_3 = $dados["estoque_c"];
$estoque_4 = $dados["estoque_d"];
$estoque_5 = $dados["estoque_e"];
$estoque_6 = $dados["estoque_f"];
$estoque_7 = $dados["estoque_g"];
$estoque_8 = $dados["estoque_h"];
$estoque_9 = $dados["estoque_i"];
$estoque_10 = $dados["estoque_j"];
$estoque_11 = $dados["estoque_k"];
    $estoque_12 = $dados["estoque_l"];
?>

<tr data-toggle="collapse" data-target="#accordion<?php echo $id; ?>" class="clickable">
    <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
    <?php if ($tipo==1) { echo "Entrada"; } if ($tipo==2) { echo "Saida";} ?>
    <i class="fa fa-exchange pull-right"></i>
    </td>
    <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
    <?php echo $produto; ?>
    </td>
    <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
        <?php echo $quant_total; ?>
    </td>
    <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
        <?php echo datahorabanco2data($data); ?>
    </td>
</tr>

<tr id="accordion<?php echo $id; ?>" class="collapse">
    <td colspan="4">

<div class="row">
    <div class="col-md-4">
    <ul>
        Responsavel:
        <li><?php echo $responsavel; ?></li>
    </ul>
    </div>
    <div class="col-md-4">
    <ul>
        Nota:
        <li>
        <?php
    if ($tipo==1) {
        $sql = "SELECT * FROM mcu_estoque_notas_entrada WHERE id=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $nota_id);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $notain = $consulta->fetch();

        $sql=null;
        $consulta=null;

        echo "<a href=\"index.php?pg=Vnota_entrada&id={$nota_id}\">";
        echo $notain['identificador'];
        echo "</a>";
    }
    if ($tipo==2) {
        $sql = "SELECT * FROM mcu_estoque_notas_saida WHERE id=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $nota_id);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $notaout = $consulta->fetch();

        $sql=null;
        $consulta=null;

        echo "<a href=\"index.php?pg=Vnota_saida&id={$nota_id}\">";
        echo $notaout['identificador'];
        echo "</a>";
    }
    ?>
    </li>
    </ul>
    </div>
</div>





    <table class="table table-striped  table-hover text-center">
    <thead>
        <tr class="bg-warning">
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Cras!"><img src="<?php echo $env->env_estatico;?>img/crasmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Creas!"><img src="<?php echo $env->env_estatico;?>img/creasmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casi!"><img src="<?php echo $env->env_estatico;?>img/casimini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Casa Lar!"><img src="<?php echo $env->env_estatico;?>img/casalarmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Peti!"><img src="<?php echo $env->env_estatico;?>img/petimini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Albergue!"><img src="<?php echo $env->env_estatico;?>img/alberguemini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Bolsa Familia!"><img src="<?php echo $env->env_estatico;?>img/bfmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SMTDS!"><img src="<?php echo $env->env_estatico;?>img/smtds.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque do Conselho Tutelar!"><img src="<?php echo $env->env_estatico;?>img/ctmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque dos Conselhos!"><img src="<?php echo $env->env_estatico;?>img/concmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da AABB!"><img src="<?php echo $env->env_estatico;?>img/aabbmini.png" alt="" style="height: 30px;"></a></th>
            <th><a href="#" data-toggle="tooltip" data-placement="bottom" title="Estoque da SCFV!"><img src="<?php echo $env->env_estatico;?>img/scfvmini.png" alt="" style="height: 30px;"></a></th>
        </tr>
    </thead>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    <tbody>
        <tr>
            <td><?php echo $estoque_1; ?></td>
            <td><?php echo $estoque_2; ?></td>
            <td><?php echo $estoque_3; ?></td>
            <td><?php echo $estoque_4; ?></td>
            <td><?php echo $estoque_5; ?></td>
            <td><?php echo $estoque_6; ?></td>
            <td><?php echo $estoque_7; ?></td>
            <td><?php echo $estoque_8; ?></td>
            <td><?php echo $estoque_9; ?></td>
            <td><?php echo $estoque_10; ?></td>
            <td><?php echo $estoque_11; ?></td>
            <td><?php echo $estoque_12; ?></td>
        </tr>
    </tbody>
</table>
<hr>

    </td>
</tr>
<?php
}
?>
</tbody>
</table>


</div>
</div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>