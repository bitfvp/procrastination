<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Nota de Entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-6">
            <a href="index.php?pg=Vnota_entrada_editar&acb=novanotaentrada" class="btn btn-default btn-block">Novo</a>
        </div>

        <form class="form-signin" action="
<?php
        $acbget=$_GET['acb'];

        if($acbget=="carregarnotaentrada"){$a="salvarnotaentradaedicao";}
        
        if($acbget=="novanotaentrada"){$a="salvarnotaentradanova";}
        
        echo "index.php?pg=Vnota_entrada&sc=&aca={$a}";
        ?>" method="post">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $nota_entrada['id']; ?>"/>
            <label for="identificador">Identificador</label><input autocomplete="off" required="true" autofocus id="identificador"  required="yes" type="text" class="form-control" name="identificador" value="<?php echo $nota_entrada['identificador']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="data">Data</label><input autocomplete="off" id="data"  required="true" type="date" class="form-control" name="data" value="<?php echo $nota_entrada['data']; ?>"/>
        </div>

        <div class="col-md-12">
        <label   for="fornecedor">Fornecedor</label><select name="fornecedor" id="fornecedor"   required="true" class="form-control">
        // vamos criar a visualização
        <option selected="" value="<?php echo $nota_entrada['fornecedor'];
        $fornecedorid=$nota_entrada['fornecedor']; 
        $fornecedorid-=1;
        ?>">
        <?php
        include_once("controllers/fornecedorlista.php");
        echo $fornecedorlista[$fornecedorid]['razao_social'];
        ?>
        </option>
        <?php
        foreach($fornecedorlista as $item){
        ?>
        <option value="<?php echo $item['id']; ?>"><?php echo $item['razao_social']; ?></option>
        <?php
        }
        ?>
        </select>
        </div>

        <div class="col-md-12">
            <label for="valor">Valor</label><input autocomplete="off" id="valor" type="text" class="form-control" required="true" name="valor" value="<?php echo $nota_entrada['valor']; ?>"/>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>