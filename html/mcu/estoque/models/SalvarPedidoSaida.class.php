<?php
class SalvarPedidoSaida{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarpedidosaidanovo($data,$produto_id,$nota_id,$tipo,$responsavel,$obs,$estoque_a,$estoque_b,$estoque_c,$estoque_d,$estoque_e,$estoque_f,$estoque_g,$estoque_h,$estoque_i,$estoque_j,$estoque_k,$estoque_l,$estoque_total){
        

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque_pedidos ";
                $sql.="(id, data, produto_id, nota_id, tipo, responsavel, obs, estoque_a, estoque_b, estoque_c, estoque_d, estoque_e, estoque_f, estoque_g, estoque_h, estoque_i, estoque_j, estoque_k, estoque_l, estoque_total)";
                $sql.=" VALUES ";
                $sql.="(NULL, :data, :produto_id, :nota_id, :tipo, :responsavel, :obs, :estoque_a, :estoque_b, :estoque_c, :estoque_d, :estoque_e, :estoque_f, :estoque_g, :estoque_h, :estoque_i, :estoque_j, :estoque_k, :estoque_l, :estoque_total)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":produto_id", $produto_id);
                $insere->bindValue(":nota_id", $nota_id);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":responsavel", $responsavel);
                $insere->bindValue(":obs", $obs);
                $insere->bindValue(":estoque_a", $estoque_a);
                $insere->bindValue(":estoque_b", $estoque_b);
                $insere->bindValue(":estoque_c", $estoque_c);
                $insere->bindValue(":estoque_d", $estoque_d);
                $insere->bindValue(":estoque_e", $estoque_e);
                $insere->bindValue(":estoque_f", $estoque_f);
                $insere->bindValue(":estoque_g", $estoque_g);
                $insere->bindValue(":estoque_h", $estoque_h);
                $insere->bindValue(":estoque_i", $estoque_i);
                $insere->bindValue(":estoque_j", $estoque_j);
                $insere->bindValue(":estoque_k", $estoque_k);
                $insere->bindValue(":estoque_l", $estoque_l);
                $insere->bindValue(":estoque_total", $estoque_total);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

//            try{
//            $sql="//UPDATE estoque SET estoque=estoque - :estoque_total, estoque_1=estoque_1 - :estoque_a, estoque_2=estoque_2 - :estoque_b, estoque_3=estoque_3 - :estoque_c, estoque_4=estoque_4 - :estoque_d, estoque_5=estoque_5 - :estoque_e, estoque_6=estoque_6 - :estoque_f, estoque_7=estoque_7 - :estoque_g, estoque_8=estoque_8 - :estoque_h, estoque_9=estoque_9 - :estoque_i, estoque_10=estoque_10 - :estoque_j, estoque_11=estoque_11 - :estoque_k WHERE estoque.id = :id";
//
//            global $pdo;
//            $atualiza=$pdo->prepare($sql);
//            $atualiza->bindValue(":estoque_total", $estoque_total);
//            $atualiza->bindValue(":estoque_a", $estoque_a);
//            $atualiza->bindValue(":estoque_b", $estoque_b);
//            $atualiza->bindValue(":estoque_c", $estoque_c);
//            $atualiza->bindValue(":estoque_d", $estoque_d);
//            $atualiza->bindValue(":estoque_e", $estoque_e);
//            $atualiza->bindValue(":estoque_f", $estoque_f);
//            $atualiza->bindValue(":estoque_g", $estoque_g);
//            $atualiza->bindValue(":estoque_h", $estoque_h);
//            $atualiza->bindValue(":estoque_i", $estoque_i);
//            $atualiza->bindValue(":estoque_j", $estoque_j);
//            $atualiza->bindValue(":estoque_k", $estoque_k);
//
//            $atualiza->bindValue(":id", $produto_id);
//            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
//        }catch ( PDOException $error_msg){
//            echo 'Erro'. $error_msg->getMessage();
//        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
?>