<?php
class SalvarCategoria{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarcategorianova( $categoria){
        $nome=strtoupper($categoria);

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque_categoria ";
                $sql.="(id, categoria)";
                $sql.=" VALUES ";
                $sql.="(NULL, :categoria)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":categoria", $categoria);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }











    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarcategoriaeditar($id,$categoria){

        //inserção no banco
        try{
            $sql="UPDATE mcu_estoque_categoria SET categoria=:categoria WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":categoria", $categoria);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>