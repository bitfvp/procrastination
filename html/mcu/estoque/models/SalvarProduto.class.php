<?php
class SalvarProduto{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarprodutonovo($produto,$unidade,$categoria,$estoque_min){

        $produto=strtoupper($produto);

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque ";
                $sql.="(id, produto, unidade, categoria, estoque_min)";
                $sql.=" VALUES ";
                $sql.="(NULL, :produto, :unidade, :categoria, :estoque_min)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":produto", $produto);
                $insere->bindValue(":unidade", $unidade);
                $insere->bindValue(":categoria", $categoria);
                $insere->bindValue(":estoque_min", $estoque_min);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
        
    }

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarprodutoedicao($id,$produto,$unidade,$categoria,$estoque_min){
        $produto=strtoupper($produto);

        //inserção no banco
        try{
            $sql="UPDATE mcu_estoque SET produto=:produto, unidade=:unidade, categoria=:categoria, estoque_min=:estoque_min WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":produto", $produto);
            $up->bindValue(":unidade", $unidade);
            $up->bindValue(":categoria", $categoria);
            $up->bindValue(":estoque_min", $estoque_min);
            $up->bindValue(":id", $id);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro errado '. $error_msg->getMessage();
        }

        if(isset($up)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>