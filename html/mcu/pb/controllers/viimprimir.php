<?php
//metodo de selecionar cbs pra imprimir
if ($startactiona == 1 && $aca == "imprimirvisim") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }

        try {
            $sql = "UPDATE mcu_visitaaveriguacao ";
            $sql .= "SET imprimir='1'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        if (isset($_GET["pg"])) {
            $_SESSION['fsh']=[
                "flash"=>"Alterado!!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg={$pg}&b={$_GET['b']}");
            exit();


        } else {
            $_SESSION['fsh']=[
                "flash"=>"Alterado!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();
        }
    }
}
//metodo de selecionar cbs pra nao imprimir
if ($startactiona == 1 && $aca == "imprimirvinao") {
    //verificar se as super variaveis post estao setadas
    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }
        try {
            $sql = "UPDATE mcu_visitaaveriguacao ";
            $sql .= "SET imprimir='0'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($_GET["pg"])) {
            $_SESSION['fsh']=[
                "flash"=>"Alterado!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg={$pg}&b={$_GET['b']}");
            exit();
        } else {
            $_SESSION['fsh']=[
                "flash"=>"Alterado!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();
        }
    }
}
?>