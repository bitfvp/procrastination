<?php
class Visita{
    public function fncvisitaedit($vi,$vi_pessoa,$data_visita,$status_visita,$perfil_cadunico,$perfil_bolsafamilia,$tipo_beneficio,$prof_resp,$relatorio,$upin){

        //inserção no banco
        try{
            $sql="UPDATE mcu_visitaaveriguacao SET ";
            $sql.="status_visita=:status_visita, perfil_cadunico=:perfil_cadunico, "
            ."perfil_bolsafamilia=:perfil_bolsafamilia, tipo_beneficio=:tipo_beneficio, "
            ."data_visita=:data_visita, prof_resp=:prof_resp, relatorio=:relatorio ";
            $sql.=" WHERE id=:id ";

            global $pdo;
            $atuali=$pdo->prepare($sql);
            $atuali->bindValue(":status_visita", $status_visita);
            $atuali->bindValue(":perfil_cadunico", $perfil_cadunico);
            $atuali->bindValue(":perfil_bolsafamilia", $perfil_bolsafamilia);
            $atuali->bindValue(":tipo_beneficio", $tipo_beneficio);
            $atuali->bindValue(":tipo_beneficio", $tipo_beneficio);
            $atuali->bindValue(":data_visita", $data_visita);
            $atuali->bindValue(":prof_resp", $prof_resp);
            $atuali->bindValue(":relatorio", $relatorio);
            $atuali->bindValue(":id",$vi);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        if(isset($atuali)){

            $at_profissional=$_SESSION['id'];
            $at_atividade=12;
            $data_visita.=" ".date("H").":".date("i").":".date("s");
            if(empty($relatorio)){
                //não exec
            }else{
                $at_denuncia=0;
                $salvar= new Atividade();
                $salvar->fncatividadenew($vi_pessoa,$at_denuncia,$at_profissional,$data_visita,0,$at_atividade,$relatorio,1,$upin);
            }
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($vi_pessoa,$_SESSION['id'],"Editar visita",9,3);
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
//            header("Location: ?pg=Vviaguardando");
//            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da function

}//fim da classe