<?php
class Avaliacao
{
    public function fncavaliacaonew($pessoa,$profissional,$cf,$data_inicial,$data_alerta,$data_final,$aa,$a1,$a2,$b1,$b2,$b3,$b4,$b5,$b6)
    {

        //desativar os anteriores do mesmo profissional
        try{
            $sql="UPDATE mcu_pb_avaliacao SET ";
            $sql.="status=0 ";
            $sql.=" WHERE profissional=:profissional and cf=:cf ";

            global $pdo;
            $atuali=$pdo->prepare($sql);
            $atuali->bindValue(":profissional", $profissional);
            $atuali->bindValue(":cf", $cf);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        try {
            $sql = "INSERT INTO mcu_pb_avaliacao ";
            $sql .= "(id, profissional, cf, data_inicial, data_alerta, data_final, aa, a1, a2, b1, b2,b3, b4, b5, b6)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :profissional, :cf, :data_inicial, :data_alerta, :data_final, :aa, :a1, :a2, :b1, :b2, :b3, :b4, :b5, :b6)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":profissional", $profissional);
            $insere->bindValue(":cf", $cf);
            $insere->bindValue(":data_inicial", $data_inicial);
            $insere->bindValue(":data_alerta", $data_alerta);
            $insere->bindValue(":data_final", $data_final);
            $insere->bindValue(":aa", $aa);
            $insere->bindValue(":a1", $a1);
            $insere->bindValue(":a2", $a2);
            $insere->bindValue(":b1", $b1);
            $insere->bindValue(":b2", $b2);
            $insere->bindValue(":b3", $b3);
            $insere->bindValue(":b4", $b4);
            $insere->bindValue(":b5", $b5);
            $insere->bindValue(":b6", $b6);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere)) {

            /////////////////////////////////////////////////////
            //reservado para log

            //////////////////////////////////////////////////////////////////////////// cras 2 creas 5


            $_SESSION['fsh']=[
                "flash"=>"Avaliação Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new
    


}//fim da classe
