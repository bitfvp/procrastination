<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_33"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Benefícios-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <?php
            if ($allow["allow_34"] == 1) {
                ?>
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Pedido de benefício
                    </div>
                    <div class="card-body">
                        <form action="index.php?pg=Vbe&aca=newbe&id=<?php echo $_GET['id']; ?>" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="beneficio">Benefício:</label>
                                    <select name="beneficio" id="beneficio" class="form-control">
                                        // vamos criar a visualização
                                        <?php
                                        foreach (fncbetipolist() as $ativ) {
                                            ?>
                                            <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['tipo']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label for="condicao">Condição:</label>
                                    <select name="condicao" id="condicao" class="form-control">
                                        // vamos criar a visualização
                                        <?php
                                        foreach (fncbecondicaolist() as $ativ) {
                                            ?>
                                            <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['condicao']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for="quantidade">Quantidade:</label>
                                    <select name="quantidade" id="quantidade" class="form-control">
                                        // vamos criar a visualização
                                        <option selected="" value="1"  class="text-info">1</option>
                                        <option value="2" class="text-danger">2</option>
                                        <option value="3" class="text-danger">3</option>
                                        <option value="4" class="text-danger">4</option>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <label for="data_pedido">Data para entrega:</label>
                                    <input id="data_pedido" type="date" class="form-control" name="data_pedido" value=""/>
                                </div>
                                <div class="col-md-4">
                                    <label for="entrega">Entrega:</label>
                                    <input name="entrega" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-exclamation-circle'></i>ENTREGUE" data-off="HABITUAL" data-onstyle="danger" data-offstyle="info">
                                </div>

                                <div class="col-md-12">
                                    <label for="descricao">Observação:</label>
                                    <textarea id="descricao" onkeyup="limite_textarea(this.value,200,descricao,'cont')" maxlength="200" class="form-control" rows="3" name="descricao"></textarea>
                                    <span id="cont">200</span>/200
                                </div>
                                <div class="col-md-12 mt-3">
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer text-warning text-center">
                        Sempre verificar o endereço antes de emitir o benefício.
                    </div>
                </div>

                <?php
                $cont_exibe_balao=$_GET['id'];
                $_SESSION["be".$cont_exibe_balao]=$_SESSION["be".$cont_exibe_balao]+1;
                if ($_SESSION["be".$cont_exibe_balao]==1){

                    $sql = "SELECT * FROM mcu_beneficio "
                        ."WHERE ((pessoa=?) and beneficio='1') "
                        ."ORDER BY data_pedido DESC LIMIT 0,1";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ultima = $consulta->fetch();
                    $ultima_veri = $consulta->rowCount();
                    $sql = null;
                    $consulta = null;
                    if ($ultima_veri!=0){
                        $datinha=data2banco(dataRetiraHora($ultima['data_pedido']));
                        $data=date("Y-m-d");
                        $diferenca = abs(diasDatas($datinha,$data));
                        if ($diferenca<30){
                        ?>
                        <div class="modal fade bd-example-modal-lg" id="alerta" tabindex="-1" role="dialog" aria-labelledby="modalalertaLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg bg-dark" role="document">
                                <div class="modal-content">
                                    <div id="mmsgs" class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h1 class="text-center text-warning"><i class="fa fa-exclamation-triangle"></i> Atenção</h1>
                                        <h4 class="text-center">Este beneficiário recebeu um agendamento </h4>
                                        <h4 class="text-center">em um período de <i class="text-danger"><?php echo $diferenca;?> dia(s)</i></h4>
                                        <h4 class="text-center">Antes de dar um novo benefício revise a pasta do USUÁRIO e BENEFÍCIOS.</h4>
                                        <h6 class="text-center">Aviso, você foi avisado.</h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script>
                            $('#alerta').modal('toggle');
                        </script>
                    <?php
                        }
                    }

                }


            }

            // Recebe
            if (isset($_GET['id'])) {
                //existe um id e se ele é numérico
                if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
                    // Captura os dados do cliente solicitado
                    //   $sql = "SELECT * FROM cestabasica WHERE pessoa=? ORDER BY data_pedido DESC";
                    $sql = "SELECT * \n"
                        . "FROM mcu_beneficio \n"
                        . "WHERE (pessoa=?)\n"
                        . "ORDER BY data_pedido DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $cestas = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>

            <div class="card mt-3">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico de benefícios
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vbeprint&id=<?php echo $_GET['id'];?>" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão</span>
                    </a>

                        <?php
                        foreach ($cestas as $cb){
                        switch ($cb['entregue']){
                            case 0:
                                $status="Aguardando Para Ser Entregue";
                                $statuss="primary";
                                break;
                            case 1:
                                $status="Entregue";
                                $statuss="success";
                                break;
                            case 2:
                                $status="Dificuldade de Localização";
                                $statuss="warning";
                                break;
                        }
                        switch ($cb['beneficio']){
                            case 1:
                                $icon="<i class='fas fa-shopping-basket'></i>";
                                break;
                            case 2:
                                $icon="<i class='fas fa-bed'></i>";
                                break;
                            case 3:
                                $icon="<i class='fas fa-tshirt'></i>";
                                break;
                            case 4:
                                $icon="<i class='fas fa-question'></i>";
                                break;
                            case 5:
                                $icon="<i class='fas fa-fire'></i>";
                                break;
                        }
                        ?>
                    <hr>
                    <h5>
                        <blockquote class="blockquote blockquote-<?php echo $statuss?>">
                            Tipo:<strong class="bg-<?php echo $statuss?>"><?php echo fncgetbetipo($cb['beneficio'])['tipo']. "  ". $icon; ?>&nbsp;&nbsp;</strong><br>
                            Quantidade:<strong class="text-<?php echo $statuss?>"><?php echo $cb['quantidade']; ?>&nbsp;&nbsp;</strong>
                            Data para entrega:<strong title="Lançada em: <?php echo datahoraBanco2data($cb['data_ts']); ?>" class="text-<?php echo $statuss?>"><?php echo dataRetiraHora($cb['data_pedido']); ?>&nbsp;&nbsp;</strong><br>
                            Obs:<strong class="text-<?php echo $statuss?>"><?php echo $cb['descricao']; ?>&nbsp;&nbsp;</strong><br>
                            Condição:<strong class="bg-<?php echo $statuss?>"><?php echo fncgetbecondicao($cb['condicao'])['condicao'];?></strong><br>
                            Status:<strong class="text-<?php echo $statuss?>"><?php echo $status?></strong><br>
                            Data da Entrega:<strong class="text-<?php echo $statuss?>"><?php echo dataBanco2data($cb['data_entrega']); ?>&nbsp;&nbsp;</strong><br>
                            Quem Recebeu:<strong class="text-<?php echo $statuss?>"><?php echo $cb['quem_recebeu']; ?>&nbsp;&nbsp;</strong>
                            <span class="badge badge-pill badge-warning float-right"><strong><?php echo $cb['id']; ?></strong></span>
                            <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($cb['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";

                                if ($cb['profissional'] == $_SESSION['id'] and $cb['entregue'] <> 1 ) {
                                    ?>
                                    <div class="dropdown show">
                                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apagar
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vcb&id={$_GET['id']}&aca=excluirbe&cb_id={$cb['id']}";?>">Apagar</a>
                                        </div>
                                    </div>
                                    <h6>Não é possivel apagar após a entrega</h6>
                                <?php
                                }
                                ?>
                            </footer>
                        </blockquote>
                    </h5>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>