<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Guia de visita-".$env->env_titulo;
$css="guiavisita";

include_once("{$env->env_root}includes/head.php");
?>
<body>
<main class="container">
    <?php
    $id_user = $_SESSION['id'];
    if (!empty($id_user) && is_numeric($id_user)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT "
            ."mcu_visitaaveriguacao.id, "
            ."mcu_visitaaveriguacao.pessoa, "
            ."mcu_visitaaveriguacao.data_pedido, "
            ."mcu_visitaaveriguacao.prof_cad, "
            ."mcu_visitaaveriguacao.cad_novo, "
            ."mcu_visitaaveriguacao.cad_transferencia, "
            ."mcu_visitaaveriguacao.cad_atualizado, "
            ."mcu_visitaaveriguacao.bolsa_familia, "
            ."mcu_visitaaveriguacao.descricao, "
            ."mcu_visitaaveriguacao.prof_resp, "
            ."mcu_visitaaveriguacao.status_visita, "
            ."mcu_visitaaveriguacao.perfil_cadunico, "
            ."mcu_visitaaveriguacao.perfil_bolsafamilia, "
            ."mcu_visitaaveriguacao.tipo_beneficio, "
            ."mcu_visitaaveriguacao.data_visita, "
            ."mcu_visitaaveriguacao.relatorio, "
            ."mcu_visitaaveriguacao.imprimir, mcu_pessoas.bairro "
            ."FROM "
            ."mcu_pessoas "
            ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
            ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
            ."WHERE "
            ."mcu_visitaaveriguacao.imprimir = 1 AND "
            ."mcu_visitaaveriguacao.prof_resp = ? order by bairro ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $id_user);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mat = $consulta->fetchAll();
        $quantrows = $consulta->rowCount();
        $sql = null;
        $consulta = null;
    }
    ?>
    <h4>GUIA DE VISITAS</h4>
    <?php
    $y=0;$x=0;$fol=1;
    echo date("d/m/Y");
    $roteiro = array();
    foreach($mat as $at){
        $pe=fncgetpessoa($at['pessoa']);
        ?>
        <hr class="hrgrosso">
        <div class="row">
            <div class="col-12">
                <label for="pessoa" class="border-left border-bottom pl-2">Beneficiário:<h6><?php echo $pe['nome']; ?></h6></label>
                <label for="profissional" class="border-left border-bottom pl-2">Pedido feito por:<h6>
                        <?php
                        $us=fncgetusuario($at['prof_cad']);
                        echo $us['nome'];
                        ?>
                    </h6>
                </label>
                <label for="data_pedido" class="border-left border-bottom pl-2">Data do pedido:<h6><?php echo dataRetiraHora($at['data_pedido']); ?></h6></label>
                <label for="endereco" class="border-left border-bottom pl-2">Endereco:<h6><?php echo $pe['endereco']; ?></h6></label>
                <label for="numero" class="border-left border-bottom pl-2">Número:<h6><?php echo $pe['numero']; ?></h6></label>
                <label for="referencia" class="border-left border-bottom pl-2">Referência:<h6><?php echo $pe['referencia']; ?></h6></label>
                <label for="bairro" class="border-left border-bottom pl-2">Bairro:<h6><i class="fa"></i><?php echo fncgetbairro($pe['bairro'])['bairro']; ?></h6></label>
                <label for="informação" class="border-left border-bottom pl-2">Inf.:<h6><?php echo $at['descricao']; ?></h6></label>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <?php
                $y++;$x++;
                if ($x==5 or $quantrows==$y){
                    $x=0;

                    echo "<h6 class='text-center'>Folha: {$fol}</h6>";
                    $fol++;
                    echo "<h6 class='text-center'> FlavioW<i class='fa fa-cogs'></i>rks&nbsp;</h6>";
                    if ($quantrows>$y){
                        echo "<div style='page-break-before:always;'>&nbsp</div>";
                        echo "<h4>GUIA DE VISITAS</h4>";
                        echo date("d/m/Y");
                    }else{

                        //rota
                        echo "<h6 class='text-left border p-2'>";
//                        foreach ($roteiro as $rota){
//                            echo "<i class='far fa-square fa-2x'></i> ".$rota."<br>";
//                        }

                        $sql = "SELECT DISTINCT "
                            ."Count(mcu_visitaaveriguacao.id) as quantidade, "
                            ."mcu_bairros.bairro, mcu_bairros.id AS id_bairro "
                            ."FROM "
                            ."mcu_pessoas "
                            ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
                            ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
                            ."WHERE "
                            ."mcu_visitaaveriguacao.imprimir = 1 AND "
                            ."mcu_visitaaveriguacao.prof_resp = ? "
                            ."GROUP BY "
                            ."mcu_bairros.id";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindParam(1, $id_user);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lugares = $consulta->fetchAll();
                        $sql = null;
                        $consulta = null;
                        foreach ($lugares as $rota){
                            echo "<i class='far fa-square fa-2x'></i> ".$rota[0]."-".$rota[1]."<br>";
                        }
                        echo "</h6>";

                    }
                }?>
            </div>
        </div>
        <?php
    }
    ?>
</main>
</body>
</html>