<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_34"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Beneficio relatório-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']."00:00:01";
    $final = $_POST['data_final']."23:59:59";
    $beneficio = $_POST['beneficio'];
    $condicao = $_POST['condicao'];
    if ($condicao=="" or $condicao==0){

        $sql = "SELECT mcu_beneficio.id, mcu_beneficio.data_entrega, mcu_beneficio.condicao, mcu_beneficio.quantidade, tbl_users.nome AS profissional, mcu_pessoas.nome, mcu_pessoas.endereco, mcu_bairros.bairro, mcu_beneficio.entregue\n"
            . "FROM (tbl_users INNER JOIN mcu_beneficio ON tbl_users.id = mcu_beneficio.profissional) INNER JOIN (mcu_bairros INNER JOIN mcu_pessoas ON mcu_bairros.id = mcu_pessoas.bairro) ON mcu_beneficio.pessoa = mcu_pessoas.id\n"
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) and (mcu_beneficio.profissional=:profissional) ) ORDER BY mcu_beneficio.data_entrega";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->bindValue(":profissional", $_SESSION['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $cestas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        $sql = "SELECT\n"
            . "Sum(mcu_beneficio.quantidade) AS quant,\n"
            . "mcu_bairros.bairro\n"
            . "FROM\n"
            . "mcu_pessoas\n"
            . "INNER JOIN mcu_beneficio ON mcu_pessoas.id = mcu_beneficio.pessoa\n"
            . "INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro\n"
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) and (mcu_beneficio.profissional=:profissional) )\n"
            . "GROUP BY\n"
            . "mcu_bairros.bairro\n"
            . "ORDER BY\n"
            . "quant DESC";

        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->bindValue(":profissional", $_SESSION['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $bnfcs2 = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

    }else{

        $sql = "SELECT mcu_beneficio.id, mcu_beneficio.data_entrega, mcu_beneficio.condicao, mcu_beneficio.quantidade, tbl_users.nome AS profissional, mcu_pessoas.nome, mcu_pessoas.endereco, mcu_bairros.bairro, mcu_beneficio.entregue\n"
            . "FROM (tbl_users INNER JOIN mcu_beneficio ON tbl_users.id = mcu_beneficio.profissional) INNER JOIN (mcu_bairros INNER JOIN mcu_pessoas ON mcu_bairros.id = mcu_pessoas.bairro) ON mcu_beneficio.pessoa = mcu_pessoas.id\n"
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) and (mcu_beneficio.condicao=:condicao) and (mcu_beneficio.profissional=:profissional) ) ORDER BY mcu_beneficio.data_entrega";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->bindValue(":condicao", $condicao);
        $consulta->bindValue(":profissional", $_SESSION['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $cestas = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

        $sql = "SELECT\n"
            . "Sum(mcu_beneficio.quantidade) AS quant,\n"
            . "mcu_bairros.bairro\n"
            . "FROM\n"
            . "mcu_pessoas\n"
            . "INNER JOIN mcu_beneficio ON mcu_pessoas.id = mcu_beneficio.pessoa\n"
            . "INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro\n"
            . "WHERE (((mcu_beneficio.entregue)=1) and (mcu_beneficio.data_entrega>=:inicial) and (mcu_beneficio.data_entrega<:final)  and (mcu_beneficio.beneficio=:beneficio) and (mcu_beneficio.condicao=:condicao) and (mcu_beneficio.profissional=:profissional) )\n"
            . "GROUP BY\n"
            . "mcu_bairros.bairro\n"
            . "ORDER BY\n"
            . "quant DESC";

        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":beneficio", $beneficio);
        $consulta->bindValue(":condicao", $condicao);
        $consulta->bindValue(":profissional", $_SESSION['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $bnfcs2 = $consulta->fetchAll();
        $sql = null;
        $consulta = null;

    }

    $usuario=fncgetusuario($_SESSION['id']);
    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h3>MEU RELATÓRIO DE ENTREGA DE <?php echo fncgetbetipo($beneficio)['tipo'];?></h3>
        <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        <h6>Profissional:<?php echo $usuario['nome']." ( ".fncgetprofissao($usuario['profissao'])['profissao'];?> )</h6>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>ENTREGA&nbsp;</td>
                <td>QUANT.&nbsp;</td>
                <td>BENEFICIÁRIO&nbsp;</td>
                <td>CONDIÇÃO&nbsp;</td>
                <td>BAIRRO&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalcb = 0;
            foreach ($cestas as $cb) {
                ?>
            <tr>
                <td><?php echo dataBanco2data($cb['data_entrega']); ?>&nbsp;</td>
                <td><?php echo $cb['quantidade'];
                    $totalcb = $totalcb + $cb['quantidade']; ?>&nbsp;
                </td>
                <td><?php echo $cb['nome']; ?>&nbsp;</td>
                <td><?php echo fncgetbecondicao($cb['condicao'])['condicao']; ?>&nbsp;</td>
                <td><?php echo $cb['bairro']; ?>&nbsp;</td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <h4>TOTAL DE <?php echo fncgetbetipo($beneficio)['tipo'].": ".$totalcb; ?></h4>

        <div id="chart_div" style="height: 1500px;"></div>
    </div>
</main>
</body>
</html>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Bairro', 'Quantidade',],
            <?php
            foreach ($bnfcs2 as $gf) {
                echo "['{$gf['quant']} {$gf['bairro']}', {$gf['quant']}],\n";
            }
            ?>
        ]);

        var options = {
            title: 'VOLUME DE <?php echo fncgetbetipo($beneficio)['tipo'];?> POR BAIRRO',
            chartArea: {width: '50%'},
            hAxis: {
                title: '<?php echo $totalcb;?>',
                minValue: 0,
            },
            vAxis: {
                title: 'Bairros',
                textStyle : {
                    fontSize: 11 // or the number you want
                },
            }

        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
