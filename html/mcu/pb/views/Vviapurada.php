<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_9"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
            if ($allow["allow_10"] != 1) {
                header("Location: {$env->env_url_mod}");
                exit();
            } else {
                //ira abrir
            }
        }
    }
}

$page = "Visitas apurada-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Recebe
            $id_user = $_SESSION['id'];
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT * "
                ."FROM mcu_visitaaveriguacao "
                ."WHERE (((mcu_visitaaveriguacao.prof_resp)=?) AND ((mcu_visitaaveriguacao.status_visita)<>0)) "
                ."ORDER BY mcu_visitaaveriguacao.data_pedido DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_user);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mat = $consulta->fetchAll();
                $contar = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Visitas apuradas - { <?php echo $contar ?> }
                </div>
                <div class="card-body">
                    <table class="table table-sm table-striped table-hover">
                        <thead>
                        <tr>
                            <th>NOME</th>
                            <th>DATA DO PEDIDO</th>
                            <th>BAIRRO</th>
                            <th>STATUS</th>
                            <th>EDITAR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($mat as $at) {
                            $pe = fncgetpessoa($at['pessoa']);
                            ?>
                            <tr>
                                <td><a href="index.php?pg=Vpessoa&id=<?php echo $at['pessoa'];?>"><?php echo $pe['nome'];?></a></td>
                                <td><?php echo dataBanco2data($at['data_pedido']); ?></td>
                                <td><?php echo fncgetbairro($pe['bairro'])['bairro'];?></td>
                                <td><?php if ($at['status_visita']==0){echo "Aguardando visita";}if ($at['status_visita']==1){echo "Apurado";}if ($at['status_visita']==2){echo "Não encontrado";} ?></td>
                                <td><a href="?pg=Vvieditar&id=<?php echo $at['id']; ?>" class="fa fa-pen"></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>