<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_9"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
            if ($allow["allow_10"] != 1) {
                header("Location: {$env->env_url_mod}");
                exit();
            } else {
                //ira abrir
            }
        }
    }
}

$page = "Visitas aguardando-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-8">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Recebe
            $id_user = $_SESSION['id'];

            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT DISTINCT "
                    ."Count(mcu_visitaaveriguacao.id) as quantidade, "
                    ."mcu_bairros.bairro, mcu_bairros.id AS id_bairro "
                    ."FROM "
                    ."mcu_pessoas "
                    ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
                    ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
                    ."WHERE "
                    ."mcu_visitaaveriguacao.status_visita = 0 AND "
                    ."mcu_visitaaveriguacao.prof_resp = ? "
                    ."GROUP BY "
                    ."mcu_bairros.id";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_user);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lugares = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }
            ?>
            <div class="card mb-1">
                <div class="card-header bg-info text-light">
                    Bairros
                </div>
                <div class="card-body">
                    <h5>
                        <?php
                        echo "<a href='index.php?pg=Vviaguardando&b=0' class='btn btn-sm btn-warning m-1'>TODOS</a>";
                        foreach ($lugares as $at){
                            echo "<a href='index.php?pg=Vviaguardando&b=".$at['id_bairro']."' class='btn btn-sm btn-warning m-1'>".$at['bairro']."-".$at['quantidade']."</a>";
                        }
                        ?>
                    </h5>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-1">
                <div class="card-header bg-dark text-light">
                    Comandos
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vviaguardando&aca=resetviimprimir" class="btn btn-lg btn-warning mt-2 btn-block">Resetar selecionados</a>
                    <a href="index.php?pg=Vviguia" class="btn btn-lg btn-success btn-block">Imprimir</a>
                    <?php
                    $sql = "SELECT DISTINCT "
                        ."Count(mcu_visitaaveriguacao.id) as quantidade, "
                        ."mcu_bairros.bairro, mcu_bairros.id AS id_bairro "
                        ."FROM "
                        ."mcu_pessoas "
                        ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
                        ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
                        ."WHERE "
                        ."mcu_visitaaveriguacao.imprimir = 1 AND "
                        ."mcu_visitaaveriguacao.prof_resp = ? "
                        ."GROUP BY "
                        ."mcu_bairros.id";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $id_user);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $lugares = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;

                    foreach ($lugares as $at){
                        echo "<i class='mt-3'>*".$at['bairro']."-".$at['quantidade']."</i><br>";
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">

            <?php
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                if ($_GET['b']==0 and isset($_GET['b'])){
                    $sql = "SELECT "
                        ."mcu_visitaaveriguacao.id, "
                        ."mcu_visitaaveriguacao.pessoa, "
                        ."mcu_visitaaveriguacao.data_pedido, "
                        ."mcu_visitaaveriguacao.prof_cad, "
                        ."mcu_visitaaveriguacao.cad_novo, "
                        ."mcu_visitaaveriguacao.cad_transferencia, "
                        ."mcu_visitaaveriguacao.cad_atualizado, "
                        ."mcu_visitaaveriguacao.bolsa_familia, "
                        ."mcu_visitaaveriguacao.descricao, "
                        ."mcu_visitaaveriguacao.prof_resp, "
                        ."mcu_visitaaveriguacao.status_visita, "
                        ."mcu_visitaaveriguacao.perfil_cadunico, "
                        ."mcu_visitaaveriguacao.perfil_bolsafamilia, "
                        ."mcu_visitaaveriguacao.tipo_beneficio, "
                        ."mcu_visitaaveriguacao.data_visita, "
                        ."mcu_visitaaveriguacao.relatorio, "
                        ."mcu_visitaaveriguacao.imprimir "
                        ."FROM "
                        ."mcu_pessoas "
                        ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
                        ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
                        ."WHERE "
                        ."mcu_visitaaveriguacao.status_visita = 0 AND "
                        ."mcu_visitaaveriguacao.prof_resp = ? ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $id_user);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $mat = $consulta->fetchAll();
                    $contar = $consulta->rowCount();
                    $sql = null;
                    $consulta = null;
                }else{
                    $sql = "SELECT "
                        ."mcu_visitaaveriguacao.id, "
                        ."mcu_visitaaveriguacao.pessoa, "
                        ."mcu_visitaaveriguacao.data_pedido, "
                        ."mcu_visitaaveriguacao.prof_cad, "
                        ."mcu_visitaaveriguacao.cad_novo, "
                        ."mcu_visitaaveriguacao.cad_transferencia, "
                        ."mcu_visitaaveriguacao.cad_atualizado, "
                        ."mcu_visitaaveriguacao.bolsa_familia, "
                        ."mcu_visitaaveriguacao.descricao, "
                        ."mcu_visitaaveriguacao.prof_resp, "
                        ."mcu_visitaaveriguacao.status_visita, "
                        ."mcu_visitaaveriguacao.perfil_cadunico, "
                        ."mcu_visitaaveriguacao.perfil_bolsafamilia, "
                        ."mcu_visitaaveriguacao.tipo_beneficio, "
                        ."mcu_visitaaveriguacao.data_visita, "
                        ."mcu_visitaaveriguacao.relatorio, "
                        ."mcu_visitaaveriguacao.imprimir "
                        ."FROM "
                        ."mcu_pessoas "
                        ."INNER JOIN mcu_visitaaveriguacao ON mcu_pessoas.id = mcu_visitaaveriguacao.pessoa "
                        ."INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro "
                        ."WHERE "
                        ."mcu_visitaaveriguacao.status_visita = 0 AND "
                        ."mcu_visitaaveriguacao.prof_resp = ? AND "
                        ."mcu_bairros.id = ?";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $id_user);
                    $consulta->bindParam(2, $_GET['b']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $mat = $consulta->fetchAll();
                    $contar = $consulta->rowCount();
                    $sql = null;
                    $consulta = null;
                }

            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Visitas aguardando -
                    <?php
                    echo fncgetbairro($_GET['b'])['bairro'];
                    echo "  {".$contar."}";
                    ?>
                </div>
                <div class="card-body">
                    <h5>
                        <?php
                        if ($contar==0){
                            echo "<h1>Não há visitas pendentes</h1>";
                        }
                        foreach ($mat as $at){
                            $pe=fncgetpessoa($at['pessoa']);
                            ?>
                            <hr>

                            <?php

                            if($at['imprimir']==0){
                                echo "<a href='index.php?pg=Vviaguardando&id={$at['id']}&b={$_GET['b']}&aca=imprimirvisim' class='btn' title='Click para alterar para sim'><span class='fa fa-thumbs-down text-warning' aria-hidden='true'></span>";
                                echo"Não está marcado para impressão";
                                echo "</a>";
                            }
                            if($at['imprimir']==1){
                                echo "<a href='index.php?pg=Vviaguardando&id={$at['id']}&b={$_GET['b']}&aca=imprimirvinao' class='btn' title='Click para alterar para não'><span class='fa fa-thumbs-up text-success' aria-hidden='true'></span>";
                                echo"Sim, está marcado para impressão";
                                echo "</a>";
                            }
                            ?>

                                <blockquote class="blockquote blockquote-inverse blockquote-primary ml-0 pl-0">
                                    <h2><a href="index.php?pg=Vpessoa&id=<?php echo $at['pessoa'];?>"><?php echo $pe['nome'];?></a></h2>
                                    <h4>ENDEREÇO: <?php echo $pe['endereco'];?>, <?php echo $pe['numero'];?>, <?php echo fncgetbairro($pe['bairro'])['bairro'];?></h4>
                                    <h4>REFERÊNCIA: <?php echo $pe['referencia'];?></h4>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <blockquote class="blockquote blockquote-success">
                                                <p>
                                                    <i class="fa fa-quote-left fa-sm "></i> <strong
                                                            class="text-success"><?php echo $at['descricao']; ?></strong>
                                                    <i class="fa fa-quote-right fa-sm"></i>
                                                </p>
                                                Data do pedido:<strong class="text-info"><?php echo dataBanco2data($at['data_pedido']); ?>&nbsp;&nbsp;</strong>
                                                <br>
                                                Cadastro novo:<strong class="text-info"><?php if ($at['cad_novo']==0){echo "Não";}if ($at['cad_novo']==1){echo "Sim";}?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                                <br>
                                                Transferência de cidade:<strong class="text-info"><?php if ($at['cad_transferencia']==0){echo "Não";}if ($at['cad_transferencia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                                <br>
                                                Cadastro esta atualizado:<strong class="text-info"><?php if ($at['cad_atualizado']==0){echo "Não";}if ($at['cad_atualizado']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                                <br>
                                                Está recebendo bolsa família:<strong class="text-info"><?php if ($at['bolsa_familia']==0){echo "Não";}if ($at['bolsa_familia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>&nbsp;&nbsp;
                                                <br>
                                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                                <footer class="blockquote-footer">
                                                    Pedido feito por
                                                    <?php
                                                    $us=fncgetusuario($at['prof_cad']);
                                                    echo $us['nome'];
                                                    echo " <br>(".fncgetprofissao($us['profissao'])['profissao'].")";
                                                    ?>
                                                </footer>
                                            </blockquote>
                                        </div>
                                        <div class="col-md-6">
                                            <blockquote class="blockquote blockquote-<?php echo ($at['status_visita']==1) ? "success" : "info"; ?>">
                                                <p>
                                                    <i class="fa fa-quote-left fa-sm "></i> <strong
                                                            class="text-success"><?php echo $at['relatorio']; ?></strong>
                                                    <i class="fa fa-quote-right fa-sm"></i>
                                                </p>
                                                Status da visita:<strong class="text-info"><?php if ($at['status_visita']==0){echo "Aguardando visita";}if ($at['status_visita']==1){echo "Apurado";}if ($at['status_visita']==2){echo "Não encontrado";} ?>&nbsp&nbsp</strong>
                                                <br>
                                                Perfil cadunico:<strong class="text-info"><?php if ($at['perfil_cadunico']==0){echo "Não";}if ($at['perfil_cadunico']==1){echo "Sim";} ?>&nbsp&nbsp</strong>
                                                <br>
                                                Perfil bolsa família:<strong class="text-info"><?php if ($at['perfil_bolsafamilia']==0){echo "Não";}if ($at['perfil_bolsafamilia']==1){echo "Sim";} ?>&nbsp&nbsp</strong>
                                                <br>
                                                Perfil benefício:<strong class="text-info"><?php if ($at['tipo_beneficio']==0){echo "Nenhum";}if ($at['tipo_beneficio']==1){echo "Variavel";}if ($at['tipo_beneficio']==2){echo "Basico/Variavel";} ?>&nbsp&nbsp</strong>
                                                <br>
                                                Data da visita:
                                                <strong class="text-info">
                                                    <?php
                                                    if($at['data_visita']!="1900-01-01" and $at['data_visita']!="" and $at['data_visita']!="1000-01-01"){
                                                        echo dataBanco2data($at['data_visita']);
                                                    }
                                                    ?>&nbsp;&nbsp;
                                                </strong>
                                                <footer class="blockquote-footer">
                                                    Responsável pela visita e relatório: <br>
                                                    <?php
                                                    $us=fncgetusuario($at['prof_resp']);
                                                    echo $us['nome'];
                                                    echo " <br>(".fncgetprofissao($us['profissao'])['profissao'].")";

                                                    if ($at['prof_resp'] == $_SESSION['id'] and $at['status'] <> 1 ) {
                                                        ?>
                                                        <br>
                                                        <a href="?pg=Vvieditar&id=<?php echo $at['id']; ?>" class="btn btn-info btn-sm btn-block mt-2">Preencher relatório</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </blockquote>
                            <?php
                        } ?>
                    </h5>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>