<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_9"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
            if ($allow["allow_10"] != 1) {
                header("Location: {$env->env_url_mod}");
                exit();
            } else {
                //ira abrir
            }
        }
    }
}

$page = "Visitas-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if(isset($_GET['id'])) {
    $idvi =$_GET['id'];
    //existe um id e se ele é numérico
    if (!empty($idvi) && is_numeric($idvi)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * FROM mcu_visitaaveriguacao WHERE id=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $idvi);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $vi = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }
}
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Relatório de visita
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vviaguardando&aca=savevi&id=<?php echo $_GET['id'];?>" method="post">
                        <input type="hidden" name="pessoa" value="<?php echo $vi['pessoa']?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="descricao">Descrição da solicitação: </label>
                                    <textarea id="descricao"  rows="8" class="form-control" name="descricao" readonly="true" ><?php echo $vi['descricao']; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-6 float-left">
                                    <label for="status_visita">Status da visita:</label>
                                    <select name="status_visita" id="status_visita" class="form-control">
                                        // vamos criar a visualização
                                        <option selected="" value="<?php if($vi['status_visita']==""){$z=0; echo $z;}else{ echo $vi['status_visita'];} ?>">
                                            <?php
                                            if($vi['status_visita']==0){echo"Aguardando visita";}
                                            if($vi['status_visita']==1){echo"Apurado";}
                                            if($vi['status_visita']==2){echo"Não encontrado";}
                                            ?>
                                        </option>
                                        <option value="0">Aguardando visita</option>
                                        <option value="1">Apurado</option>
                                        <option value="2">Não encontrado</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 float-left">
                                    <label for="perfil_cadunico">Perfil cadunico:</label>
                                    <select name="perfil_cadunico" id="perfil_cadunico" class="form-control">
                                        // vamos criar a visualização
                                        <option selected="" value="<?php if($vi['perfil_cadunico']==""){$z=0; echo $z;}else{ echo $vi['perfil_cadunico'];} ?>">
                                            <?php
                                            if($vi['perfil_cadunico']==0){echo"Não";}
                                            if($vi['perfil_cadunico']==1){echo"Sim";}
                                            ?>
                                        </option>
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6 float-left">
                                    <label for="perfil_bolsafamilia">Perfil bolsa família:</label>
                                    <select name="perfil_bolsafamilia" id="perfil_bolsafamilia" class="form-control">
                                        // vamos criar a visualização
                                        <option selected="" value="<?php if($vi['perfil_bolsafamilia']==""){$z=0; echo $z;}else{ echo $vi['perfil_bolsafamilia'];} ?>">
                                            <?php
                                            if($vi['perfil_bolsafamilia']==0){echo"Não";}
                                            if($vi['perfil_bolsafamilia']==1){echo"Sim";}
                                            ?>
                                        </option>
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6 float-left">
                                    <label for="tipo_beneficio">Tipo de benefício:</label>
                                    <select name="tipo_beneficio" id="tipo_beneficio" class="form-control">
                                        // vamos criar a visualização
                                        <option selected="" value="<?php if($vi['tipo_beneficio']==""){$z=0; echo $z;}else{ echo $vi['tipo_beneficio'];} ?>">
                                            <?php
                                            if($vi['tipo_beneficio']==0){echo"Nenhum";}
                                            if($vi['tipo_beneficio']==1){echo"Variavel";}
                                            if($vi['tipo_beneficio']==2){echo"Básico_Variavel";}
                                            ?>
                                        </option>
                                        <option value="0">Nenhum</option>
                                        <option value="1">Variavel</option>
                                        <option value="2">Básico/Variavel</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-6 float-left">
                                    <label for="Data da Visita">Data da visita: </label>
                                    <input type="date" name="data_visita" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                </div>

                                <div class="form-group col-md-6 float-left">
                                    <label for="prof_resp">Responsável pela visita:</label>
                                    <select name="prof_resp" id="prof_resp" class="form-control" <?php echo ($vi['status_visita']!=0) ? "readonly" : "" ;?>>
                                        <?php
                                        $usuarioid = $vi['prof_resp'];
                                        $getusuario=fncgetusuario($usuarioid);
                                        ?>
                                        <option selected="" value="<?php echo $vi['prof_resp']; ?>">
                                            <?php echo $getusuario['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncusuarioencaminhamentolist() as $item) {
                                            ?>
                                            <option value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                            </div><!-- fim da segunda coluna-->
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="relatorio">Relatório da visita:</label>
                                <textarea id="relatorio" name="relatorio" onkeyup="limite_textarea(this.value,3000,relatorio,'cont')" maxlength="3000" class="form-control" rows="6"><?php echo $vi['relatorio']; ?></textarea>
                                <span id="cont">3000</span>/3000
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>