<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_10"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Pessoas por bairro-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$bairro=$_POST['bairro'];
                    include_once("{$env->env_root}controllers/mcu/bairrolista.php");
                    $getbairro=fncgetbairro($bairro);
$sql = "SELECT * FROM mcu_pessoas WHERE mcu_pessoas.bairro = :bairro ORDER by mcu_pessoas.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":bairro",$bairro);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container">
    <h1>Pessoas por bairro: <?php echo $getbairro['bairro']; ?></h1>
    <style media=all>
        .table-sm {
            font-size:10px !important;
            widows: 2;
            width: 100%;
        }
        @media print {
            @page {
                margin: 0.79cm auto;
            }
        }
    </style>
    <?php
    $acont=0;
    $a=$ppp="a";
    echo "<table class=\"table table-striped table-bordered table-sm table-responsive\">";
    echo "<thead class=\"thead-default\">";
    echo "<tr>";
    echo "<td>Nome</td>";
    echo "<td>CPF</td>";
    echo "<td>RG</td>";
    echo "<td>NASCIMENTO</td>";
    echo "<td>RUA</td>";
    echo "<td>NUMERO</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach($ati as $at){
?>
        <tr>
            <td><?php echo $at['nome']; ?>&nbsp;</td>
            <td><?php echo $at['cpf'];?>&nbsp;</td>
            <td><?php echo $at['rg'];?>&nbsp;</td>
            <td><?php echo dataBanco2data($at['nascimento']); ?>&nbsp;</td>
            <td><?php echo $at['endereco'];?>&nbsp;</td>
            <td><?php echo $at['numero'];?>&nbsp;</td>
        </tr>

        <?php
        $tcont++;
    }

    echo "<tr>";
    echo '<td colspan="6" class=\'text-right font-weight-bold\'>Total '.$tcont.'</td>';
    echo "</tr>";
    $tcont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>

</div>
</body>
</html>