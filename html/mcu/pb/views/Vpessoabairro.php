<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_10"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Lista pessoa por bairro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

<div class="row">
<div class="col-md-3"></div>
    <div class="col-md-6">
<!-- =============================começa conteudo======================================= -->
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Pessoas por bairro</h3>
    </div>
    <div class="panel-body">
        <p>
        <form action="index.php?pg=Vpessoabairroprint" method="post" target="_blank">
            <input type="submit" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO"/>

            <div class="form-group">
                <label for="bairro">Bairro:</label>
                <select name="bairro" id="bairro" class="form-control" required>
                    // vamos criar a visualização
                    <?php
                    include_once("{$env->env_root}controllers/mcu/bairrolista.php");
                    $bairrolista=fncbairrolist();
                    foreach ($bairrolista as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                            <?php echo $item['bairro']; ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </form>

        </p>
    </div>

    <div class="panel-footer text-warning">Selecione um bairro pra listar.</div>
</div>

<!-- =============================fim conteudo======================================= -->       
        </div>
    <div class="col-md-4"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>