<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_56"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-8">
        <?php
        include_once ("includes/pessoa_cabecalho.php");
        include_once ("includes/pcd/pcd_cabecalho.php");

        if (!empty($processo)) {
            include_once("includes/pcd/pcd_fotos.php");
            include_once("includes/pcd/pcd_atform.php");
            include_once("includes/pcd/pcd_at.php");
        }
        ?>

	</div>
	<div class="col-md-4">

        <?php
        if (!empty($processo)){
            include_once ("includes/pcd/pcd_feedback.php");
            include_once ("includes/pcd/pcd_arquivos.php");
        }
        ?>
    </div>
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>