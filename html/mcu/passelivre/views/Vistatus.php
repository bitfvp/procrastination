<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_56"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Status Idoso-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm">
                <thead class="thead-dark">
                <tr>
                    <th>Pedido</th>
                    <th>Nome</th>
                    <th>Foto tirada</th>
                    <th>Enviado</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sql = "SELECT\n"
                    . "mcu_p_idoso.id, mcu_p_idoso.pessoa, mcu_pessoas.nome, mcu_p_idoso.carteira, mcu_p_idoso.foto, mcu_p_idoso.envio, mcu_p_idoso.cartao_feito\n"
                    . "FROM\n"
                    . "mcu_pessoas\n"
                    . "INNER JOIN mcu_p_idoso ON mcu_pessoas.id = mcu_p_idoso.pessoa\n"
                    . "ORDER BY\n"
                    . "mcu_p_idoso.carteira DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $sql=null;
                $consulta=null;

                foreach ($lista as $lt) {
                    echo "<tr>";
                    echo "<td>{$lt['carteira']}</td>";
                    echo "<td><a href='index.php?pg=Vi&id={$lt['pessoa']}'>{$lt['nome']}</a></td>";
                    echo ($lt['foto']==1) ? "<td class='text-muted bg-success'>Sim</td>": "<td class='text-muted bg-info'>Não</td>";
                    echo ($lt['envio']==1) ? "<td class='text-muted bg-success'>Sim</td>": "<td class='text-muted bg-info'>Não</td>";

                    if ($lt['cartao_feito']==0){echo "<td class='text-muted bg-info'>Aguardando...</td>";}
                    if ($lt['cartao_feito']==1){echo "<td class='text-muted bg-success'>Confeccionado</td>";}
                    if ($lt['cartao_feito']==2){echo "<td class='text-muted bg-danger'>Ocorreu um problema!!</td>";}

                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>



</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>