<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Confirmação de entrega
    </div>
    <div class="card-body">
        <form action="index.php?pg=Va&aca=apaefeedback&id=<?php echo $_GET['id']; ?>" method="post">
            <div class="form-group">
                <label for="">Status do cartão:</label>
                <select name="cartao_feito" id="cartao_feito" class="form-control">
                    // vamos criar a visualização de entregue
                    <option value="0">Aguardando...</option>
                    <option value="1">Confeccionado</option>
                    <option value="2">Ocorreu um problema!!</option>
                </select>
            </div>
            <div class="form-group">
                <label for="obs_uniao">Descrição:</label>
                <textarea id="obs_uniao" onkeyup="limite_textarea(this.value,1000,obs_uniao,'cont2')" maxlength="1000" class="form-control" rows="4" name="obs_uniao"></textarea>
                <span id="cont2">1000</span>/1000
            </div>
            <input type="submit" class="btn btn-lg btn-success btn-block" value="Enviar"/>
        </form>
    </div>
</div>