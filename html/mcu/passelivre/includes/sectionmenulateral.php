
<section class="sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <?php
        $sql = "SELECT mcu_p_apae.id \n"
            . "FROM mcu_p_apae \n"
            . "WHERE mcu_p_apae.pessoa = ?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $apaeCont = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        //
        $sql = "SELECT mcu_p_idoso.id \n"
            . "FROM mcu_p_idoso \n"
            . "WHERE mcu_p_idoso.pessoa = ?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $idosoCont = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        //
        $sql = "SELECT mcu_p_pcd.id \n"
            . "FROM mcu_p_pcd \n"
            . "WHERE mcu_p_pcd.pessoa = ?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $pcdCont = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        //////////////////
        if ($apaeCont!=0){
            echo "<a href='?pg=Va&id={$_GET['id']}' class='list-group-item'><i class='fab fa-empire'></i>    PASSE APAE</a>";
        }
        if ($idosoCont!=0){
            echo "<a href='?pg=Vi&id={$_GET['id']}' class='list-group-item'><i class='fab fa-empire'></i>    PASSE IDOSO</a>";
        }
        if ($pcdCont!=0){
            echo "<a href='?pg=Vp&id={$_GET['id']}' class='list-group-item'><i class='fab fa-empire'></i>    PASSE PCD</a>";
        }
        ?>
        <a href="?pg=Vadv&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fab fa-empire"></i>    ADVERTÊNCIAS</a>
    </div>
</section>
<script type="application/javascript">
    var offset = $('#sidebar').offset().top;
    var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
    $(document).on('scroll', function () {
        if (offset <= $(window).scrollTop()) {
            $meuMenu.addClass('fixarmenu');
        } else {
            $meuMenu.removeClass('fixarmenu');
        }
    });
</script>
