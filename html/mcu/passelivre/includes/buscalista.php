<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input style="text-transform:lowercase;" type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control " placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <div class="dropdown dropdown-lg">

                <button class="btn btn-outline-info btn-lg" type="button" data-toggle="modal" data-target="#modalserchadvanced" aria-expanded="false">
                    <i class="fa fa-plus"></i>
                </button>

                <!-- Modal modalserchadvanced -->
                <div class="modal fade" id="modalserchadvanced" tabindex="-1" role="dialog" aria-labelledby="modalPontosLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body mb-0">
                                <form action="#">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-9">
                                                <div class="row">
                                                    <div class="col-6 mb-3">
                                                        <label for="nascimento">Buscar por data de nascimento</label>
                                                        <input type="date" autocomplete="off" class="form-control" id="nascimento" name="nascimento" placeholder="" value="<?php if (isset($_GET['nascimento'])) {echo $_GET['nascimento'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 mb-3">
                                                        <label for="cpf">Buscar por CPF</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="cpf" name="cpf" placeholder="" value="<?php if (isset($_GET['cpf'])) {echo $_GET['cpf'];} ?>">
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <label for="rg">Buscar por número de identidade</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="rg" name="rg" placeholder="" value="<?php if (isset($_GET['rg'])) {echo $_GET['rg'];} ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-9 mb-3">
                                                        <label for="endereco">Buscar por rua</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="endereco" name="endereco" placeholder="" value="<?php if (isset($_GET['endereco'])) {echo $_GET['endereco'];} ?>">
                                                    </div>
                                                    <div class="col-3 mb-3">
                                                        <label for="numero">Número</label>
                                                        <input type="search" autocomplete="off" class="form-control" id="numero" name="numero" placeholder="" value="<?php if (isset($_GET['numero'])) {echo $_GET['numero'];} ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12 mb-3">
                                                        <label for="bairro">Buscar por bairro</label>
                                                        <select name="bairro" id="bairro" class="form-control">
                                                            <?php
                                                            if (isset($_GET['bairro']) and is_numeric($_GET['bairro'])){
                                                                $getbairro=fncgetbairro($_GET['bairro']);
                                                                echo "<option value='{$getbairro['id']}'>{$getbairro['bairro']}</option>";
                                                            }else{
                                                                echo "<option value='0'></option>\n";
                                                            }

                                                            foreach (fncbairrolist() as $item) {
                                                                echo "<option value='{$item['id']}'>{$item['bairro']}</option>\n";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="col-3">
                                                <h4 class="mb-3">Gênero</h4>
                                                <div class="d-block my-3">
                                                    <div class="custom-control custom-radio">
                                                        <input id="todos" name="genero" type="radio" class="custom-control-input" value="0" <?php echo $_GET['genero']==0 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="todos"><i class="fas fa-transgender fa-2x"></i>Todos</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input id="feminino" name="genero" type="radio" class="custom-control-input" value="1" <?php echo $_GET['genero']==1 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="feminino"><i class="fas fa-venus fa-2x"></i>Feminino</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input id="masculino" name="genero" type="radio" class="custom-control-input" value="2" <?php echo $_GET['genero']==2 ? "checked" : "";?> required>
                                                        <label class="custom-control-label" for="masculino"><i class="fas fa-mars fa-2x"></i>Masculino</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div><!--row-->
                                        <div class="row">
                                            <div class="col-8">
                                                <button type="submit" class="btn btn-lg btn-outline-success btn-block float-left fa fa-search"> BUSCAR</button>
                                            </div>
                                            <div class="col-4">
                                                <a href="index.php?pg=Vbusca" class="btn btn-outline-dark btn-block float-right">LIMPAR BUSCA</a>
                                            </div>
                                        </div>
                                    </div><!--conteiner fluid-->
                                </form>
                            </div><!--body-->
                        </div>
                    </div>
                </div>
                <!--fim de modal modalserchadvanced-->

            </div>


        </div>
    </form>


    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
</div>

<br>
<div class="container">
<table class="table table-striped table-hover table-sm">
    <thead>
    <tr>
        <th scope="row" colspan="3">
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm mb-0">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                    }
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$proximo}'>Próximo →</a></li>";
                    }
                    ?>
                </ul>
            </nav>
        </th>
        <th colspan="2" class="text-info text-right"><?php echo $tr;?> pessoa(s) listada(s)</th>
    </tr>
    </thead>
    <thead class="thead-dark">
    <tr>
        <th scope="col">NOME</th>
        <th scope="col">NASCIMENTO</th>
        <th scope="col">CPF</th>
        <th scope="col"><?php if (isset($_GET['endereco']) and $_GET['endereco']!=""){echo "ENDEREÇO";}else{echo "RG";}?></th>
        <th scope="col">BAIRRO</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th scope="row" colspan="3">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php
                    // agora vamos criar os botões "Anterior e próximo"
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                    }
                    if ($pc<$tp) {
                        echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&nascimento={$_GET['nascimento']}&cpf={$_GET['cpf']}&rg={$_GET['rg']}&endereco={$_GET['endereco']}&numero={$_GET['numero']}&bairro={$_GET['bairro']}&genero={$_GET['genero']}&pgn={$proximo}'>Próximo →</a></li>";
                    }
                    ?>
                </ul>
            </nav>
        </th>
        <th colspan="2" class="text-info text-right"><?php echo $tr;?> pessoa(s) listada(s)</th>
    </tr>
    </tfoot>

    <?php
    if(isset($_GET['sca']) and $_GET['sca']!="") {
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
    }
    if (isset($_GET['nascimento']) and $_GET['nascimento'] != "") {
        $stnascimento = strtoupper($_GET['nascimento']);
        define('CSNASCIMENTO', $stnascimento);//TESTE
    }
    if (isset($_GET['cpf']) and $_GET['cpf'] != "") {
        $stcpf = strtoupper($_GET['cpf']);
        define('CSCPF', $stcpf);//TESTE
    }
    if (isset($_GET['rg']) and $_GET['rg'] != "") {
        $strg = strtoupper($_GET['rg']);
        define('CSRG', $strg);//TESTE
    }
    if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
        $stendereco = strtoupper($_GET['endereco']);
        define('CSENDERECO', $stendereco);//TESTE
    }
    if (isset($_GET['numero']) and $_GET['numero'] != "") {
        $stnumero = strtoupper($_GET['numero']);
        define('CSNUMERO', $stnumero);//TESTE
    }
    if (isset($_GET['bairro']) and $_GET['bairro'] != "") {
        $stbairro = strtoupper($_GET['bairro']);
        define('CSBAIRRO', $stbairro);//TESTE
    }
    if (isset($_GET['genero']) and $_GET['genero'] != "") {
        $stgenero = strtoupper($_GET['genero']);
        define('CSGENERO', $stgenero);//TESTE
    }

    // vamos criar a visualização
    while ($dados =$limite->fetch()){
    $id = $dados["id"];
    $nome = strtoupper($dados["nome"]);
    $nascimento = dataBanco2data ($dados["nascimento"]);
    $idade = Calculo_Idade($dados["nascimento"]);
    $cpf = $dados["cpf"];
    $rg = $dados["rg"];
    $endereco = $dados["endereco"];
    $numero = $dados["numero"];
    $cod_familiar=$dados["cod_familiar"];
    $bairro = $dados["bairro"];

    ?>
    <tbody>
    <tr>
        <th scope="row" id="<?php echo $id;  ?>">
            <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                <?php

                if(isset($_GET['sca']) and $_GET['sca']!="") {
                    $sta = CSA;
                    $nnn = $nome;
                    $nn = explode(CSA, $nnn);
                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                    echo $n;
                }else{
                    echo $nome;
                }
                ?>
            </a>
            <?php
            if ($allow["admin"]==1 and $env->env_mod_nome==" Proteção Basica "){
                echo "<a href='index.php?pg=Vbe&id={$id}' class='fas fa-hands-helping float-right text-success'></a> ";
                echo "<a href='index.php?pg=Vat&id={$id}' class='fas fa-quote-left float-right text-success'></a>";
            }
            ?>
        </th>
        <td style="white-space: nowrap;">
            <?php
            if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                echo $nascimento. "<span class='badge badge-info ml-1 text-center' title='idade'>" . $idade. "</span>";
            }else{
                echo "<span class='text-danger'>--/--/----</span>";
            }
            ?>
        </td>
        <td  style="white-space: nowrap;">
            <?php
            if($cpf!="0" and $cpf!="") {
                if (isset($_GET['cpf']) and $_GET['cpf'] != "") {
                    $stcpf = CSCPF;
                    $ccc = $cpf;
                    $cc = explode(CSCPF, $ccc);
                    $c = implode("<span class='text-danger'>{$stcpf}</span>", $cc);
                    echo $c;
                } else {
                    echo mask($cpf,'###.###.###-##');
                }
            }else{
                echo "----";
            }
            ?></td>
        <td>
            <?php if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
                if($endereco!="0" and $endereco!="") {
                    if (isset($_GET['endereco']) and $_GET['endereco'] != "") {
                        $stc = CSENDERECO;
                        $rrr = $endereco;
                        $rr = explode(CSENDERECO, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $endereco;
                    }
                }else{
                    echo "<i>-- </i>";
                }
                echo "/";
                if($numero!="0" and $numero!="") {
                    if (isset($_GET['numero']) and $_GET['numero'] != "") {
                        $stc = CSNUMERO;
                        $rrr = $numero;
                        $rr = explode(CSNUMERO, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $numero;
                    }
                }else{
                    echo "<i>s/n </i>";
                }
            } else {
                if($rg!="0" and $rg!="") {
                    if (isset($_GET['rg']) and $_GET['rg'] != "") {
                        $stc = CSRG;
                        $rrr = $rg;
                        $rr = explode(CSRG, $rrr);
                        $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                        echo $r;
                    } else {
                        echo $rg;
                    }
                }else{
                    echo "---";
                }
            }

            ?></td>
        <td class="small">
            <?php
            if ($bairro != "0") {
                $cadbairro=fncgetbairro($bairro);
                echo $cadbairro['bairro'];
            } else {
                echo "<span class='text-warning'>-----</span>";
            }
            ?>
        </td>

    </tr>
    <?php
    }
    ?>
    </tbody>
</table>
</div>
