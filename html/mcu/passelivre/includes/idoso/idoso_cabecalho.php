<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_p_idoso WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $processo = $consulta->fetch();
    $sql=null;
    $consulta=null;

}else{
    $_SESSION['fsh']=[
        "flash"=>"Ação abortada devido a falta de identificação do cadastro!!",
        "type"=>"danger",
    ];
    header("Location: ?pg=Vhome");
    exit();
}
?>
<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Ações
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <h5>
                <?php
                if (empty($processo)){
                    echo "<a class='btn btn-primary' href='?pg=Vi&aca=novaidoso&id=".$_GET['id']."' title=''>";
                    echo "Pedido de Carteira";
                    echo "</a>";
                }else{
                    ?>
                    Número do pedido:<strong class="text-info"><?php echo $processo['carteira']; ?>&nbsp;&nbsp;</strong>
                    <br>

                    Foto tirada:
                    <strong class="text-info"><?php
                        if($processo['foto']==0){echo"Não";}
                        if($processo['foto']==1){echo"Sim";} ?>&nbsp;&nbsp;
                    </strong>

                    Segunda via:
                    <strong class="text-info"><?php if($processo['segunda_via']==1){echo"Sim";}else{echo "Não";} ?>&nbsp;&nbsp;</strong>

                    Enviado:
                    <strong class="text-info"><?php
                        if($processo['envio']==0){echo"Não";}
                        if($processo['envio']==1){echo"Sim";} ?>&nbsp;&nbsp;
                    </strong>
                    <br>

                    Cartão magnético confeccionado:
                    <strong class="text-info"><?php
                        if($processo['cartao_feito']==0){echo"Aguardando...";}
                        if($processo['cartao_feito']==1){echo"Confeccionado";}
                        if($processo['cartao_feito']==2){echo"Ocorreu um problema!!";}
                        ?>&nbsp;&nbsp;
                    </strong>

                    Observação da Empresa:<strong class="text-info"><?php echo $processo['obs_uniao']; ?>&nbsp;&nbsp;</strong>
                    <br>
                    <?php
                }//fim de if de existe pedido
                ?>
            </h5>
        </blockquote>
    </div>
</div>