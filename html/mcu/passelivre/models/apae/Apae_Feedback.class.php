<?php
class Apae_Feedback{
    public function fncapaeFeedback($id,$cartao_feito,$obs_uniao,$descricao){

        try {
            $sql="UPDATE mcu_p_apae SET cartao_feito=:cartao_feito, obs_uniao=:obs_uniao WHERE pessoa=:pessoa";
            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindValue(":cartao_feito", $cartao_feito);
            $atualiza->bindValue(":obs_uniao", $obs_uniao);
            $atualiza->bindValue(":pessoa", $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        try {
            $sql = "INSERT INTO mcu_p_apae_at ";
            $sql .= "(id, pessoa, data, profissional, descricao)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, CURRENT_TIMESTAMP, :profissional, :descricao)";
            $id_prof=$_SESSION['id'];
            /////////////////

            ///////////////////
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $id);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere) and isset($atualiza)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: index.php?pg=Va&id={$id}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim de function
}//fim de class
?>