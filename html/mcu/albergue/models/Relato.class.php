<?php
class Relato
{
    public function fncrelatonew($profissional_id,$descricao,$matriz)
    {

        //inserção no banco
        try {
            $sql = "INSERT INTO migrante_alb_relato ";
            $sql .= "(id, data, profissional, descricao, matriz)";
            $sql .= " VALUES ";
            $sql .= "(NULL, CURRENT_TIMESTAMP, :profissional, :descricao, :matriz)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":profissional", $profissional_id);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":matriz", $matriz);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Relato cadastrado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: index.php");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//function

}//class