<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetmigrante($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
    <h5 class="ml-2">DADOS DO USUÁRIO</h5>
    <blockquote class="blockquote blockquote-info">
        <?php if ($allow["allow_4"]==1){ ?>
            <a class="btn btn-success btn-block mb-2" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
                EDITAR PESSOA
            </a>
        <?php }?>
        <h5>NOME:
            <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
        </h5>
        <hr>
        <h5>NOME SOCIAL:<strong class="text-info"><?php echo $pessoa['nome_social']; ?>&nbsp;&nbsp;</strong></h5>
        <h5>SEXO:
        <strong class="text-info">
        <?php
        if($pessoa['sexo']!="" and $pessoa['sexo']!="0") {
        if($pessoa['sexo']==0){echo"Selecione...";}
        if($pessoa['sexo']==1){echo"Feminino";}
        if($pessoa['sexo']==2){echo"Masculino";}
        if($pessoa['sexo']==3){echo"Indefinido";}
        }else{
        echo "???????";
        }
        ?>
        </strong>
        </h5>

        <h5>Nascimento:
                <strong class="text-info"><?php
                    if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                        echo "<span class='text-info'>";
                        echo dataBanco2data ($pessoa['nascimento']);
                        echo "<br><i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
        </h5>
        

        <h5>CPF:
            <strong class="text-info"><?php
                if($pessoa['cpf']!="") {
                    echo "<span class='text-info'>";
                    echo mask($pessoa['cpf'],'###.###.###-##');
                    echo "</span>";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong></h5>
        
       <h5> RG:
           <strong class="text-info"><?php
               if($pessoa['rg']!="") {
                   echo "<span class='text-info'>";
                   echo mask($pessoa['rg'],'###.###.###');
                   echo "</span>";
               }else{
                   echo "<span class='text-muted'>";
                   echo "[---]";
                   echo "</span>";
               }
               ?>
           </strong></h5>

               <h5>UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong></h5>
        
       <h5> BO:
           <strong class="text-info"><?php
               if($pessoa['bo']!="") {
                   echo "<span class='text-info'>";
                   echo $pessoa['bo'];
                   echo "</span>";
               }else{
                   echo "<span class='text-muted'>";
                   echo "[---]";
                   echo "</span>";
               }
               ?>
           </strong></h5>
        
       <h5 title="CARTEIRA DE TRABALHO E PREVIDENCIA SOCIAL">CTPS:
        <strong class="text-info">
        <?php
        if($pessoa['cart_trabalho']!="") {
            echo $pessoa['cart_trabalho'];
        }else{
            echo "[---]";
        }
        ?>
        </strong></h5>
        
        <h5>CN:
        <strong class="text-info">
        <?php
        if($pessoa['cn']!="") {
            echo $pessoa['cn'];
        }else{
            echo "[---]";
        }
        ?>
        </strong></h5>
        
        <h5 TITLE="CERTIDÃO DE CASAMENTO">CC:
        <strong class="text-info">
        <?php
        if($pessoa['c_casamento']!="") {
            echo $pessoa['c_casamento'];
        }else{
            echo "[---]";
        }
        ?>
        </strong></h5>

        <h5>TITULO ELEITOR:
        <strong class="text-info">
        <?php
        if($pessoa['titulo_eleitor']!="") {
            echo $pessoa['titulo_eleitor'];
        }else{
            echo "[---]";
        }
        ?>
        </strong></h5>
        
       <h5> RAÇA/COR:
        <strong class="text-info"><?php
        if(($pessoa['raca_cor']==0)or($pessoa['raca_cor']=="")){echo"Selecione...";}
        if($pessoa['raca_cor']==1){echo"Branca";}
        if($pessoa['raca_cor']==2){echo"Negra";}
        if($pessoa['raca_cor']==3){echo"Amarela";}
        if($pessoa['raca_cor']==4){echo"Parda";}
        if($pessoa['raca_cor']==5){echo"Indigena";} ?>&nbsp;&nbsp;
        </strong></h5>

       <h5> MÃE:<strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;</strong></h5>
       <h5> PAI:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong></h5>
        <hr>
       <h5> ALFABETIZADO:
        <strong class="text-info"><?php
        if($pessoa['alfabetizado']==0){echo"Não";}
        if($pessoa['alfabetizado']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong></h5>
       <h5> ESTADO CIVIL:
        <strong class="text-info"><?php
        if($pessoa['estado_civil']==0){echo"Solteiro(a)";}
        if($pessoa['estado_civil']==1){echo"Casado(a)";}
        if($pessoa['estado_civil']==2){echo"Divorciado(a)";}
        if($pessoa['estado_civil']==3){echo"Uniao Estavel";}
        if($pessoa['estado_civil']==4){echo"Viuvo(a)";}
        ?>&nbsp;&nbsp;
        </strong></h5>

       <h5> ORIGEM:<strong class="text-info"><?php echo $pessoa['origem']; ?>&nbsp;&nbsp;</strong></h5>
       <h5> CONTATO FAMILIAR:<strong class="text-info"><?php echo $pessoa['contato_familiar']; ?>&nbsp;&nbsp;</strong></h5>
      <h5>  PROFISSÃO:<strong class="text-info"><?php echo $pessoa['profissao']; ?>&nbsp;&nbsp;</strong></h5>
       <h5> OBS:<strong class="text-info"><?php echo $pessoa['obs']; ?>&nbsp;&nbsp;</strong></h5>
       <h5> ALCOOL:
        <strong class="text-info"><?php
        if($pessoa['alcool']==0){echo"Não";}
        if($pessoa['alcool']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong></h5>

       <h5> CIGARRO:
        <strong class="text-info"><?php
        if($pessoa['cigarro']==0){echo"Não";}
        if($pessoa['cigarro']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong></h5>

       <h5> MACONHA:
        <strong class="text-info"><?php
        if($pessoa['maconha']==0){echo"Não";}
        if($pessoa['maconha']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong></h5>

       <h5> CRACK:
        <strong class="text-info"><?php
        if($pessoa['crack']==0){echo"Não";}
        if($pessoa['crack']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong></h5>

        <hr>

               <h5>POSSUI DEFICIÊNCIA:

                   <strong class="text-info"><?php
                       if($pessoa['deficiencia']==0){echo"Não";}
                       if($pessoa['deficiencia']==1){echo"Sim";} ?>&nbsp;&nbsp;
                   </strong>
               </h5>
               <h5>DESCRIÇÃO DA DEFICIÊNCIA:
                   <strong class="text-info"><?php
                       if($pessoa['deficiencia_desc']!="") {
                           echo "<span class='text-info'>";
                           echo $pessoa['deficiencia_desc'];
                           echo "</span>";
                       } ?>
                   </strong>
               </h5>
    
    </h5>
    <footer class="blockquote-footer">Mantenha atualizado</footer>
  </blockquote>
</div>