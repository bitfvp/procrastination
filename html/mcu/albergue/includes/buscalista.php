<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />

            <div class="dropdown dropdown-lg">
                <button class="btn btn-outline-info btn-lg" type="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-plus"></i></button>

                <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <div class="form-group">
                        <label for="contain">CPF</label>
                        <input type="search" autocomplete="off" class="form-control" placeholder="Buscar por CPF..." name="scb" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="contain">RG</label>
                        <input type="search" autocomplete="off" class="form-control" placeholder="Buscar por RG..." name="scc" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="contain">MÃE</label>
                        <input type="search" autocomplete="off" class="form-control" placeholder="Buscar por nome da Mãe..." name="scd" value="<?php if (isset($_GET['scd'])) {echo $_GET['scd'];} ?>"/>
                    </div>
                </div>
            </div>

        </div>
    </form>

        <a href="index.php?pg=Vpessoaeditar" class="btn btn-info btn-block col-md-6 float-right btn-lg">
            NOVO CADASTRO
        </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
</div>


    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">NASCIMENTO</th>
            <th scope="col">CPF</th>
            <th scope="col">RG</th>
            <th scope="col">MÃE</th>
            <th scope="col">PERNOITE</th>
            <th scope="col">JANTAR</th>
            <th scope="col">EDITAR</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}&scd={$_GET['scd']}&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li>";
                        }

                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}&scd={$_GET['scd']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>

            <th colspan="3" class="text-info text-right"><?php echo $todos->rowCount();?> Pessoa(s) listada(s)</th>
        </tr>
        </tfoot>

        <?php
        if(isset($_GET['sca']) and $_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        if (isset($_GET['scb']) and $_GET['scb'] != "") {
            $stb = strtoupper($_GET['scb']);
            define('CSB', $stb);//TESTE
        }
        if (isset($_GET['scc']) and $_GET['scc'] != "") {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);//TESTE
        }
        if (isset($_GET['scd']) and $_GET['scd'] != "") {
            $std = strtoupper($_GET['scd']);
            define('CSD', $std);//TESTE
        }
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $nascimento = dataBanco2data ($dados["nascimento"]);
            $cpf = $dados["cpf"];
            $mae=strtoupper($dados["mae"]);
            $rg = $dados["rg"];
            if (isset($_GET['sca'])){
                $sca=$_GET['sca'];
            }else{
                $sca="";
            }
            ?>
        <tbody>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if(isset($_GET['sca']) and $_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>
                <td>
                    <?php
                    if($nascimento!="01/01/1000" and $nascimento!="01/01/1900" and $nascimento!="0"){
                        echo $nascimento;
                    }else{
                        echo "<span class='text-danger'>--/--/----</span>";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($cpf!="0" and $cpf!="") {
                        if(isset($_GET['scb']) and $_GET['scb']!="" and $_GET['scb']!="0") {
                            $stb = CSB;
                            $ccc = $cpf;
                            $cc = explode(CSB, $ccc);
                            $c = implode("<span class='text-danger'>{$stb}</span>", $cc);
                            echo $c;
                        }else{
                            echo mask($cpf,'###.###.###_##');
                        }
                    }else{
                        echo "--- --- --- --";
                    }
                    ?></td>
                <td>
                    <?php
                    if($rg!="0" and $rg!="") {
                        if(isset($_GET['scc']) and $_GET['scc']!="") {
                            $stc = CSC;
                            $rrr = $rg;
                            $rr = explode(CSC, $rrr);
                            $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                            echo $r;
                        }else{
                            echo $rg;
                        }
                    }else{
                        echo "--- --- ---";
                    }
                    ?></td>
                <td>
                    <?php
                    if($mae!="0" and $mae!=""){
                        if(isset($_GET['scd']) and $_GET['scd']!="") {
                            $std = CSD;
                            $mmm = $mae;
                            $mm = explode(CSD, $mmm);
                            $m = implode("<span class='text-danger'>{$std}</span>", $mm);
                            echo $m;
                        }else{
                            echo $mae;
                        }
                    }else{
                        echo "[---]";
                    }
                    ?>
                </td>
                <td class="text-center"><a href="index.php?pg=Vbusca&aca=pernoitenew&id=<?php echo $id; ?>&sca=<?php echo $sca; ?>" class="fa fa-bed fa-2x"></a></td>
                <td class="text-center"><a href="index.php?pg=Vbusca&aca=jantarnew&id=<?php echo $id; ?>&sca=<?php echo $sca; ?>" class="fa fa-utensils fa-2x"></a></td>

                <td>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa">
                        Alterar
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>