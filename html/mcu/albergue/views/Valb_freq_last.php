<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_63"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="últimas frequências-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->

            <?php
            // Recebe
            $sql = "SELECT * \n"
                . "FROM migrante_alb_freq \n"
                . "WHERE (matriz=1) \n"
                . "ORDER BY data DESC limit 0,20";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $hist = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Histórico De frequência no albergue
                </div>
                <div class="card-body">
                    <a href="#" onclick="alert('opcao ainda nao disponivel')" ><span class="fa fa-print" aria-hidden="true"></ span></a>
                    <table class="table table-striped table-sm ">
                        <thead>
                        <tr>
                            <th>BENEFICIÁRIO</th>
                            <th>PROFISSIONAL</th>
                            <th>DATA</th>
                            <th>TIPO</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($hist as $at){
                            $proff= explode(" ",fncgetusuario($at['profissional'])['nome']);
                            ?>
                            <tr>
                                <td><?php echo fncgetmigrante($at['pessoa'])['nome']; ?>&nbsp&nbsp</td>
                                <td><?php echo $proff['0']; ?>&nbsp&nbsp</td>
                                <td><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</td>
                                <td><?php if($at['tipo']==1){echo "Pernoite <i class='fa fa-bed float-right'></i>";} if($at['tipo']==2){echo "Jantar <i class='fa fa-utensils float-right'></i>";} ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>