<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_63"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}

$page="Minhas atividades-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
	<?php
    // Recebe
        $id_user =$_SESSION['id'];
        //existe um id e se ele é numérico
        if (!empty($id_user) && is_numeric($id_user)) {
            // Captura os dados do cliente solicitado
            $sql = "SELECT * \n"
                . "FROM migrante_at \n"
                . "WHERE (((migrante_at.profissional)=?))\n"
                . "ORDER BY migrante_at.data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $id_user);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mat = $consulta->fetchAll();
            $contar = $consulta->rowCount();
            $sql=null;
            $consulta=null;
        }
    ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Minhas atividades - { <?php echo $contar ?> }
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach($mat as $at){?>
                            <hr>
                            <div class="row">
                                <div class="col-md-8">
                                    <blockquote class="blockquote blockquote-info">
                                        Pessoa:<strong class="text-info">
                                            <a href="?pg=Vat&id=<?php echo $at['pessoa']; ?>"><?php echo fncgetmigrante($at['pessoa'])['nome']; ?></a>
                                        </strong><br>
                                        Data:<strong class="text-info"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                        Atividade:<strong class="text-info"><?php echo fncgetmi_at($at['atividade'])['atividade'];?>&nbsp;&nbsp;</strong><br>
                                        Descricao:
                                        <p>
                                            <i class="fa fa-quote-left fa-sm "></i><?php
                                            if($at['restricao']=="1"){
                                                if($at['profissional']==$_SESSION['id']){
                                                    echo "<i class='text-danger fa fa-user-secret' title='Confidenciais e outros usuários não poderão visualizar'></i> ".$at['descricao'];
                                                }else{
                                                    echo "<i class='text-danger fa fa-user-secret'></i> Confidencial - contate o profissional responsável por essa atividade...";
                                                }
                                            }else{
                                                echo "<strong class='text-success'> {$at['descricao']} </strong>";
                                            }
                                            ?><i class="fa fa-quote-right fa-sm"></i>
                                        </p>
                                        <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                    </blockquote>
                                </div>
                                <div class="col-md-4">
                                    <?php
                                    if ($at['id'] != 0) {
                                        $files = glob("../../dados/mcu/migrante/atividades/" . $at['id'] . "/*.*");
                                        for ($i = 0; $i < count($files); $i++) {
                                            $num = $files[$i];
                                            $extencao = explode(".", $num);
                                            //ultima posicao do array
                                            $ultimo = end($extencao);
                                            switch ($ultimo) {
                                                case "docx":
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;

                                                case "doc":
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;

                                                case "xls":
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;

                                                case "xlsx":
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;

                                                case "pdf":
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;

                                                default:
                                                    echo "<div class='col-md-10'>";
                                                    echo "<a href=" . $num . " target='_blank'>";
                                                    echo "<img src=" . $num . " alt='...' class='img-fluid img-thumbnail'>";
                                                    echo "</a>";
                                                    echo "</div>";
                                                    break;
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <br>
                        <?php } ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>