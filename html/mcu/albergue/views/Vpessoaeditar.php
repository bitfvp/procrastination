<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="migrantesave";
    $pessoa=fncgetmigrante($_GET['id']);
}else{
    $a="migrantenew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
                <label   for="nome">NOME<span>*</span></label>
                <input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
            </div>
            <div class="col-md-4">
                <label   for="nome_social">NOME SOCIAL</label>
                <input autocomplete="off" id="nome_social" type="text" class="form-control" name="nome_social" value="<?php echo $pessoa['nome_social']; ?>"/>
            </div>
            <div class="col-md-3">
                <label   for="sexo">SEXO</label>
                <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                    <option selected="" value="<?php if($pessoa['sexo']==""){$z=0; echo $z;}else{ echo $pessoa['sexo'];} ?>">
                        <?php
                        if($pessoa['sexo']==0){echo"Selecione...";}
                        if($pessoa['sexo']==1){echo"Feminino";}
                        if($pessoa['sexo']==2){echo"Masculino";}
                        if($pessoa['sexo']==3){echo"Indefinido";}
                        ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Feminino</option>
                    <option value="2">Masculino</option>
                    <option value="3">Indefinido</option>
                </select>
            </div>
        </div>

    <div class="row">
        <div class="col-md-3">
            <label   for="nascimento">NASCIMENTO</label>
            <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="cpf">CPF</label><input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf"
                                               value="<?php echo $pessoa['cpf']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#cpf').mask('000.000.000-00', {reverse: false});
                });
            </script>
        </div>
        <div class="col-md-3">
            <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                             value="<?php echo $pessoa['rg']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#rg').mask('00.000.000.000', {reverse: true});
                });
            </script>
        </div>
        <div class="col-md-2">
            <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                   value="<?php echo $pessoa['uf_rg']; ?>"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label for="pai">PAI</label>
            <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $pessoa['pai']; ?>"/>

        </div>
        <div class="col-md-6">
            <label for="mae">MÃE</label>
            <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $pessoa['mae']; ?>"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label for="bo">BOLETIM DE OCORRENCIA</label>
            <input autocomplete="off" id="bo" type="text" class="form-control" name="bo" value="<?php echo $pessoa['bo']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="cart_trabalho"><i title=" CARTEIRA DE TRABALHO E PREVIDENCIA SOCIAL">CTPS</i></label>
            <input autocomplete="off" id="cart_trabalho" type="text" class="form-control" name="cart_trabalho" value="<?php echo $pessoa['cart_trabalho']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="cn">CERTIDÃO DE NASCIMENTO</label>
            <input autocomplete="off" id="cn" type="text" class="form-control" name="cn" value="<?php echo $pessoa['cn']; ?>"/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label   for="c_casamento">CERTIDAO DE CASAMENTO</label>
            <input autocomplete="off" id="c_casamento" type="text" class="form-control" name="c_casamento" value="<?php echo $pessoa['c_casamento']; ?>"/>
        </div>
        <div class="col-md-4">
            <label   for="titulo_eleitor">TITULO DE ELEITOR</label>
            <input autocomplete="off" id="titulo_eleitor" type="text" class="form-control" name="titulo_eleitor" value="<?php echo $pessoa['titulo_eleitor']; ?>"/>
        </div>
        <div class="col-md-4">
            <label   for="raca_cor">RAÇA/COR</label>
            <select name="raca_cor" id="raca_cor" class="form-control">
                // vamos criar a visualização de cor-raça
                <option selected="" value="<?php if($pessoa['raca_cor']==""){$z=0; echo $z;}else{ echo $pessoa['raca_cor'];} ?>">
                    <?php
                    if(($pessoa['raca_cor']==0)or($pessoa['raca_cor']=="")){echo"Selecione...";}
                    if($pessoa['raca_cor']==1){echo"Branca";}
                    if($pessoa['raca_cor']==2){echo"Negra";}
                    if($pessoa['raca_cor']==3){echo"Amarela";}
                    if($pessoa['raca_cor']==4){echo"Parda";}
                    if($pessoa['raca_cor']==5){echo"Indigena";} ?>
                </option>
                <option value="0">Selecione...</option>
                <option value="1">Branca</option>
                <option value="2">Negra</option>
                <option value="3">Amarela</option>
                <option value="4">Parda(a)</option>
                <option value="5">Indigena</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label   for="estado_civil">ESTADO CIVIL</label>
            <select name="estado_civil" id="estado_civil" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['estado_civil']==""){$z=0; echo $z;}else{ echo $pessoa['estado_civil'];} ?>">
                    <?php
                    if($pessoa['estado_civil']==0){echo"Solteiro(a)";}
                    if($pessoa['estado_civil']==1){echo"Casado(a)";}
                    if($pessoa['estado_civil']==2){echo"Divorciado(a)";}
                    if($pessoa['estado_civil']==3){echo"Uniao Estavel";}
                    if($pessoa['estado_civil']==4){echo"Viuvo(a)";}
                    ?>
                </option>
                <option value="0">Solteiro(a)</option>
                <option value="1">Casado(a)</option>
                <option value="2">Divorciado(a)</option>
                <option value="3">Uniao Estavel</option>
                <option value="4">Viuvo(a)</option>
            </select>
            </div>
        <div class="col-md-4">
            <label   for="alfabetizado">ALFABETIZADO</label>
            <select name="alfabetizado" id="alfabetizado" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['alfabetizado']==""){$z=0; echo $z;}else{ echo $pessoa['alfabetizado'];} ?>">
                    <?php
                    if($pessoa['alfabetizado']==0){echo"não";}
                    if($pessoa['alfabetizado']==1){echo"Sim";}
                    ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
        <div class="col-md-4">
            <label   for="profissao">PROFISÃO</label>
            <input autocomplete="off" id="profissao" type="text" class="form-control" name="profissao" value="<?php echo $pessoa['profissao']; ?>"/>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md-6">
            <label   for="origem">ORIGEM</label>
            <input autocomplete="off" id="origem" type="text" class="form-control" name="origem" value="<?php echo $pessoa['origem']; ?>"/>
        </div>
        <div class="col-md-6">
            <label   for="contato_familiar">CONTATO FAMILIAR</label><textarea autocomplete="off" id="contato_familiar" type="text" class="form-control" name="contato_familiar"><?php echo $pessoa['contato_familiar']; ?></textarea>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label  class="full" for="obs">OBSERVAÇÃO</label><textarea autocomplete="off" id="obs" type="text" class="form-control" name="obs"><?php echo $pessoa['obs']; ?></textarea>

        </div>
    </div>
<hr>
    <div class="row">
        <div class="col-md-3">
            <label   for="alcool">USO DE ALCOOL</label>
            <select name="alcool" id="alcool" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['alcool']==""){$z=0; echo $z;}else{ echo $pessoa['alcool'];} ?>">
                    <?php
                    if($pessoa['alcool']==0){echo"Não";}
                    if($pessoa['alcool']==1){echo"Sim";} ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
        <div class="col-md-3">
            <label   for="cigarro">USO DE CIGARRO</label>
            <select name="cigarro" id="cigarro" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['cigarro']==""){$z=0; echo $z;}else{ echo $pessoa['cigarro'];} ?>">
                    <?php
                    if($pessoa['cigarro']==0){echo"Não";}
                    if($pessoa['cigarro']==1){echo"Sim";} ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
        <div class="col-md-3">
            <label   for="maconha">USO DE MACONHA</label>
            <select name="maconha" id="maconha" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['maconha']==""){$z=0; echo $z;}else{ echo $pessoa['maconha'];} ?>">
                    <?php
                    if($pessoa['maconha']==0){echo"Não";}
                    if($pessoa['maconha']==1){echo"Sim";} ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
        <div class="col-md-3">
            <label   for="crack">USO DE CRACK</label>
            <select name="crack" id="crack" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if($pessoa['crack']==""){$z=0; echo $z;}else{ echo $pessoa['crack'];} ?>">
                    <?php
                    if($pessoa['crack']==0){echo"Não";}
                    if($pessoa['crack']==1){echo"Sim";} ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-4">
            <label for="deficiencia">POSSUI DEFICIÊNCIA</label>
            <select name="deficiencia" id="deficiencia" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php if ($pessoa['deficiencia'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $pessoa['deficiencia'];
                } ?>">
                    <?php
                    if ($pessoa['deficiencia'] == 0) {
                        echo "Não";
                    }
                    if ($pessoa['deficiencia'] == 1) {
                        echo "Sim";
                    } ?>
                </option>
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>

        </div>

        <div class="col-md-8">
            <label for="deficiencia_desc">DESCRIÇÃO DA DEFICIÊNCIA</label>
            <input autocomplete="off" id="deficiencia_desc" type="text" class="form-control" name="deficiencia_desc" value="<?php echo $pessoa['deficiencia_desc']; ?>"/>
        </div>
    </div>
    </form>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>