<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_63"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
//echo"<META HTTP-EQUIV=REFRESH CONTENT = '1200;URL={$env->env_url_mod}'>";
include_once("includes/topo.php");
?>
<main class="container">
    <div class="card">
        <div class="card-header bg-info text-light">
            Lançamento
        </div>
        <div class="card-body">
            <form action="?aca=newrelato" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="descricao">Descrição:</label>
                        <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000"
                                  class="form-control" rows="4" name="descricao"></textarea>
                        <span id="cont">1000</span>/1000
                    </div>
                </div>
                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
            </form>
        </div>
    </div>



    <?php
    $maxreg=20;
        // Captura os dados do cliente solicitado
        $sql = "SELECT * "
            . "FROM "
            . "migrante_alb_relato where matriz=?"
            . "ORDER BY "
            . "`data` DESC "
            . "LIMIT 0, {$maxreg}";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$_SESSION['matriz']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $ativi = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    ?>

    <div class="card mt-3">
        <div id="pointofview" class="card-header bg-info text-light">
            Histórico
        </div>
        <div class="card-body">
                <?php
                foreach ($ativi as $at) {
                    ?>
                    <hr>

                    <blockquote class="blockquote blockquote-info">
                        <h5>
                        “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                        <br>
                        <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                        <span class="badge badge-pill badge-info float-right"><strong><?php echo $at['id']; ?></strong></span>
                        <footer class="blockquote-footer">
                            <?php
                            $us=fncgetusuario($at['profissional']);
                            echo $us['nome'];
                            echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                            ?>
                        </footer>
                        </h5>
                    </blockquote>

                    <?php
                }
                ?>
        </div>
    </div>



</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>