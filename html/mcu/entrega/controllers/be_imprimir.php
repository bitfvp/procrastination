<?php
//metodo de selecionar cbs pra imprimir
if ($startactiona == 1 && $aca == "imprimirbesim") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }

        try {
            $sql = "UPDATE mcu_beneficio ";
            $sql .= "SET imprimir='1'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Alterado!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vhome&sca={$_GET['sca']}&b={$_GET['b']}&pv={$id}");
        exit();

    }
}
//metodo de selecionar cbs pra nao imprimir
if ($startactiona == 1 && $aca == "imprimirbenao") {
    //verificar se as super variaveis post estao setadas
    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }
        try {
            $sql = "UPDATE mcu_beneficio ";
            $sql .= "SET imprimir='0'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Alterado!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vhome&sca={$_GET['sca']}&b={$_GET['b']}&pv={$id}");
        exit();

    }
}