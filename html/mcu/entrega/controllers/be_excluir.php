<?php
//metodo de acao cadastro de usuario
if ($startactiona == 1 && $aca == "excluirbe") {
    $prof_id = $_SESSION["id"];
    $cb_id = $_GET["cb_id"];
    $pessoa_id = $_GET["pessoa"];

    if ((isset($prof_id) and is_numeric($prof_id)) and (isset($cb_id) and is_numeric($cb_id))) {

            try {
                $sql = "DELETE FROM `mcu_beneficio` WHERE id = :cb_id and pessoa = :pessoa_id and entregue <> 1";
                global $pdo;
                $exclui = $pdo->prepare($sql);
                $exclui->bindValue(":cb_id", $cb_id);
                $exclui->bindValue(":pessoa_id", $pessoa_id);
                $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro:' . $error_msg->getMessage();
            }

            $_SESSION['fsh']=[
                "flash"=>"Benefício excluido com sucesso",
                "type"=>"success",
            ];
            //reservado para log
            global $LL; $LL->fnclog($pessoa_id,$_SESSION['id'],"Apagar CB",3,4);
            ////////////////////////////////////////////////////////////////////////////
            header("Location: index.php?pg=Vhome");
            exit();


    }
}
?>