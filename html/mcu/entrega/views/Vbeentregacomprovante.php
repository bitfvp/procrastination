<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_35"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Benefício comprovante-".$env->env_titulo;
$css="cbcomprovante";

include_once("{$env->env_root}includes/head.php");
?>
<body>
<main class="container">
    <?php
    $sql = "SELECT mcu_beneficio.id, mcu_beneficio.descricao, mcu_beneficio.beneficio, mcu_beneficio.condicao, mcu_pessoas.nome, mcu_pessoas.cpf, mcu_pessoas.endereco, mcu_pessoas.numero, mcu_bairros.bairro, tbl_users.nome AS profissional, mcu_pessoas.referencia, mcu_pessoas.telefone, mcu_beneficio.data_pedido, mcu_beneficio.quantidade, mcu_beneficio.entregue, mcu_beneficio.imprimir\n"
        . "FROM (tbl_users INNER JOIN mcu_beneficio ON tbl_users.id = mcu_beneficio.profissional) INNER JOIN (mcu_bairros INNER JOIN mcu_pessoas ON mcu_bairros.id = mcu_pessoas.bairro) ON mcu_beneficio.pessoa = mcu_pessoas.id\n"
        . "WHERE ((mcu_beneficio.imprimir)=1) order by bairro, endereco";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cestas = $consulta->fetchAll();
    $quantrows = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    ?>
    <h4>COMPROVANTE DE ENTREGA DE BENEFÍCIOS</h4>
    <?php
    $y=0;$x=0;$fol=1;
    echo date("d/m/Y");
    $roteiro = array();
    foreach($cestas as $cb){
        $proff = explode(" ", $cb['profissional']);
        ?>
        <hr>
        <div class="row">
            <div class="col-2">
                <label for="id"><h3>Código:</h3><h1>
                        <?php
                        echo $cb['id'];
                        switch ($cb['beneficio']){
                            case 1:
                                echo "<i class='fas fa-shopping-basket'></i>";
                                break;
                            case 2:
                                echo "<i class='fas fa-bed'></i>";
                                break;
                            case 3:
                                echo "<i class='fas fa-tshirt'></i>";
                                break;
                            case 4:
                                echo "<i class='fas fa-question'></i>";
                                break;
                                case 5:
                                echo "<i class='fas fa-fire'></i>";
                                break;
                        }
                        ?>
                    </h1></label>
            </div>
            <div class="col-8">
                <label for="pessoa" class="border-left border-bottom pl-2">Beneficiário:<h6><?php echo $cb['nome']; ?></h6></label>
                <label for="cpf" class="border-left border-bottom pl-2">CPF:<h6><?php echo mask($cb['cpf'], "###.###.###/##"); ?></h6></label>
                <label for="endereco" class="border-left border-bottom pl-2">Endereco:<h6><?php echo ucwords(strtolower($cb['endereco']))."     // ".$cb['numero']; ?></h6></label>
                <label for="referencia" class="border-left border-bottom pl-2">Referência:<h6><?php echo $cb['referencia']; ?></h6></label>
                <label for="bairro" class="border-left border-bottom pl-2">Bairro:<h6><i class="fa"></i><?php echo $cb['bairro']; ?></h6></label>
                <label for="bairro" class="border-left border-bottom pl-2">Telefone:<h6><i class="fa"></i><?php echo $cb['telefone']; ?></h6></label>
                <label for="obs" class="border-left border-bottom pl-2">Obs. de entrega:<h6><?php echo $cb['descricao']; ?></h6></label>
                <label for="data_entrega" class="border-left border-bottom pl-2"><h4>Entrega:___/___/_____ Assinatura:__________________</h4></label>
            </div>
            <div class="col-2">
                <label for="pessoa" class="border-left border-bottom pl-2"><h6><?php echo fncgetbecondicao($cb['condicao'])['condicao']; ?></h6></label>
                <label for="profissional" class="border-left border-bottom pl-2"><small><?php echo $proff[0]." ".end($proff); ?></small></label>
                <label for="quantidade" class="border-left border-bottom pl-2">Quantidade: <?php echo $cb['quantidade']; ?></label>
                <label for="data_pedido" class="border-left border-bottom pl-2">Agendado para:<small><?php echo dataRetiraHora($cb['data_pedido']); ?></small></label>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php
                $y++;$x++;
                if ($x==5 or $quantrows==$y){
                    $x=0;

                    echo "<h6 class='text-center'>Assinar e colocar a data no local determinado, Se outra pessoa receber anotar no local especificado*</h6>";
                    echo "<h6 class='text-center'>Folha: {$fol}</h6>";
                    $fol++;
                    echo "<h6 class='text-center'> FlavioW<i class='fa fa-cogs'></i>rks&nbsp;</h6>";
                    if ($quantrows>$y){
                        echo "<div style='page-break-before:always;'>&nbsp</div>";
                        echo "<h4>COMPROVANTE DE ENTREGA DE BENEFÍCIOS</h4>";
                        echo date("d/m/Y");
                    }else{

                        //rota
                        echo "<h6 class='text-left border p-2'>";
//                        foreach ($roteiro as $rota){
//                            echo "<i class='far fa-square fa-2x'></i> ".$rota."<br>";
//                        }

                        $sql = "SELECT DISTINCT "
                            ."Sum(mcu_beneficio.quantidade), "
                            ."mcu_bairros.bairro, mcu_bairros.id "
                            ."FROM "
                            ."mcu_beneficio "
                            ."INNER JOIN mcu_pessoas ON mcu_beneficio.pessoa = mcu_pessoas.id "
                            ."INNER JOIN mcu_bairros ON mcu_pessoas.bairro = mcu_bairros.id "
                            ."WHERE "
                            ."mcu_beneficio.imprimir = 1 and mcu_beneficio.beneficio = 1 "
                            ."GROUP BY "
                            ."mcu_bairros.bairro "
                            ."ORDER BY "
                            ."mcu_bairros.bairro ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $roteiro = $consulta->fetchAll();
                        $sql=null;
                        $consulta=null;
                        foreach ($roteiro as $rota){
                            echo "<i class='far fa-square fa-2x'></i> ".$rota[0]."-".$rota[1]."<br>";

                            //    seleciona ruas do bairro
                            $sql = "SELECT "
                                ."mcu_beneficio.id, "
                                ."mcu_pessoas.endereco, mcu_pessoas.numero "
                                ."FROM "
                                ."mcu_pessoas "
                                ."INNER JOIN mcu_beneficio ON mcu_beneficio.pessoa = mcu_pessoas.id "
                                ."WHERE "
                                ."mcu_beneficio.imprimir = 1 AND "
                                ."mcu_beneficio.beneficio = 1 AND "
                                ."mcu_pessoas.bairro = ".$rota[2]
                                ." ORDER BY mcu_pessoas.endereco ASC";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $ruas = $consulta->fetchAll();
                            $sql=null;
                            $consulta=null;
                            foreach ($ruas as $rua){
                                echo "<i class='ml-5 fas fa-map-marker-alt'></i> ".ucwords(strtolower($rua[1]))."<br>";
                            }
                            //    seleciona ruas do bairro FIM

                        }
                        echo "</h6>";

                    }
                }?>
            </div>
        </div>
        <?php
    }
    ?>
</main>
</body>
</html>