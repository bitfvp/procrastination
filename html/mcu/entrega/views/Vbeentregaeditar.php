<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_35"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Benefício Editar-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $cb=fncgetbe($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!-- =============================começa conteudo======================================= -->
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Finalizar entrega
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vhome&aca=salvarbeedicao" method="post">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Código do benefício:</label>
                                <input id="id" type="text" readonly="true" class="form-control disabled" name="id" value="<?php echo $cb['id']; ?>"/>
                                <input id="pessoa" type="hidden" readonly="true" class="d-none" name="pessoa" value="<?php echo $cb['pessoa']; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Benefíciario:</label>
                                <input id="beneficiario" type="text" readonly="true" class="form-control disabled" name="beneficiario" value="<?php echo fncgetpessoa($cb['pessoa'])['nome']; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Benefício:</label>
                                <input id="beneficio" type="text" readonly="true" class="form-control disabled" name="beneficio" value="<?php echo fncgetbetipo($cb['beneficio'])['tipo']; ?>"/>
                            </div>
                            <div class="form-group col-md-3">
                                    <label for="quantidade">Quantidade:</label>
                                    <input readonly="true" id="quantidade" type="text" class="form-control disabled" name="quantidade" value="<?php echo $cb['quantidade']; ?>"/>
                            </div>
                            <div class="form-group col-md-3">
                                    <label for="data_pedido">Data do pedido:</label><input readonly="true" id="data_pedido" type="text" class="form-control" name="data_pedido" value="<?php echo dataRetiraHora($cb['data_pedido']); ?>"/>
                            </div>

                            <div class="form-group col-md-3">
                                    <label for="entregue">Entregue:</label>
                                    <select name="entregue" id="entregue" class="form-control">
                                        // vamos criar a visualização de entregue
                                        <option selected="" value="<?php if($cb['entregue']==""){$z=0; echo $z;}else{ echo $cb['entregue'];} ?>">
                                            <?php
                                            if($cb['entregue']==0){echo"Aguardando";}
                                            if($cb['entregue']==1){echo"Sim";}
                                            if($cb['entregue']==2){echo"Dificuldade de localizar";}
                                            ?>
                                        </option>
                                        <option value="0">Aguardando</option>
                                        <option value="1">Sim</option>
                                        <option value="2">Dificuldade de localizar</option>
                                    </select>
                            </div>
                            <div class="form-group col-md-3">
                                    <label for="data_entrega">Data da entrega:</label>
                                    <input id="data_entrega" type="date" class="form-control" name="data_entrega" value="<?php echo $cb['data_entrega']; ?>"/>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="quem_recebeu">Quem recebeu:</label>
                                <textarea id="quem_recebeu" onkeyup="limite_textarea(this.value,255,quem_recebeu,'cont')" maxlength="3000" class="form-control" rows="3" name="quem_recebeu"><?php echo $cb['quem_recebeu']; ?></textarea>
                                <span id="cont">255</span>/255
                            </div>

                            <script>
                                function isererecebeu(valor) {
                                    var str = $("#quem_recebeu").val();
                                    $('#quem_recebeu').val(str+" "+valor);
                                    // $('#quem_recebeu').focus();
                                }
                            </script>

                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR REGISTRO"/>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <button id="emoji" class="btn btn-sm m-1 border" onclick="isererecebeu('Ela mesma')">Ela mesma</button>
                            <button id="emoji" class="btn btn-sm m-1 border" onclick="isererecebeu('Ele mesmo')">Ele mesmo</button>
                            <button id="emoji" class="btn btn-sm m-1 border" onclick="isererecebeu('Indecifrável')">Indecifrável</button>
                        </div>
                    </div>

                    </p>
                </div>
            </div>


<!-- =============================fim conteudo======================================= -->       
    </div>
    <div class="col-md-3"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>