<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_35"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Entrega-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

try{
    $sql = "SELECT "
    ."mcu_beneficio.id, "
    ."mcu_beneficio.pessoa, "
    ."mcu_beneficio.beneficio, "
    ."mcu_beneficio.quantidade, "
    ."mcu_beneficio.data_pedido, "
    ."mcu_beneficio.data_ts, "
    ."mcu_beneficio.condicao, "
    ."mcu_beneficio.descricao, "
    ."mcu_beneficio.profissional, "
    ."mcu_beneficio.entregue, "
    ."mcu_beneficio.data_entrega, "
    ."mcu_beneficio.quem_recebeu, "
    ."mcu_beneficio.imprimir "
    ."FROM "
    ."mcu_pessoas "
    ."INNER JOIN mcu_beneficio ON mcu_beneficio.pessoa = mcu_pessoas.id "
    ."WHERE "
    ."mcu_beneficio.entregue <> 1 ";
     if (isset($_GET['b']) and  is_numeric($_GET['b']) and $_GET['b']!=0){
         $sql .="AND mcu_pessoas.bairro = :bairrim ";
     }
     if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
         $sql .="AND mcu_beneficio.id LIKE :beneficio_id ";
    }
    $sql .="order by mcu_beneficio.data_pedido asc ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    if (isset($_GET['b']) and  is_numeric($_GET['b']) and $_GET['b']!=0) {
        $consulta->bindValue("bairrim", $_GET['b']);
    }
    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
        $consulta->bindValue("beneficio_id", "%".$_GET['sca']."%");
    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$beneficios = $consulta->fetchAll();
$quantidade = $consulta->rowCount();
$sql = null;
$consulta = null;

?>
<main class="container"><!--todo conteudo-->

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vhome" hidden/>
            <input type="number" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Código..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <select name="b" id="b" class="form-control">
                <?php
            if (isset($_GET['b']) and  is_numeric($_GET['b']) and $_GET['b']!=0){
                $bairroid = $_GET['b'];
                $getbairro=fncgetbairro($bairroid);
            }else{
                $bairroid = 0;
                $getbairro = [
                    "id" => "0",
                    "bairro" => "TODOS OS BAIRROS",
                ];
            }

                ?>
                <option selected="" value="<?php echo $bairroid; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                foreach (fncbairrolist() as $item) {
                    ?>
                    <option value="<?php echo $item['id'];?>">
                        <?php echo $item['bairro']; ?>
                    </option>
                    <?php
                }
                ?>
                <option value="0">
                    TODOS OS BAIRROS
                </option>
            </select>
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

<div class="row">
    <div class="col-md-3">
        <a href="index.php?pg=Vbeentregacomprovante" class="btn btn-success mb-2" target="_blank"><i class="far fa-list-alt"></i> Romaneio</a>
    </div>
    <div class="col-md-5">
        <a href='index.php?pg=Vhome&aca=resetbeimprimir' class="btn btn-warning btn-block mb-2"><i class="fas fa-sync-alt"></i> Desmarcar Todos</a>
    </div>
    <div class="col-md-4">
        <a href='index.php?pg=Vhome&sca=<?php echo $_GET['sca'];?>&b=<?php echo $_GET['b'];?>&aca=imprimirtodos&s=<?php
        foreach ($beneficios as $be){
            if ($be["entregue"]==0) {
                echo $be["id"] . "[*]";
            }
        }
        ?>' class="btn btn-warning btn-block mb-2"><i class="fas fa-arrow-alt-circle-down"></i>Marcar Lista</a>
    </div>
</div>

            <table class="table table-stripe table-hover table-condensed table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col" class="text-center">#</th>
                    <th scope="col"><small>BENEFICIÁRIO</small></th>
                    <th scope="col"><small>TIPO</small></th>
                    <th scope="col"><small>CONDIÇÃO</small></th>
                    <th scope="col"><small>QUANT</small></th>
                    <th scope="col"><small>PEDIDO</small></th>
                    <th scope="col"><small>PROFISSIONAL</small></th>
                    <th scope="col"><small>BAIRRO</small></th>
                    <th scope="col" class="text-center"><small>AÇÕES</small></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th scope="row" colspan="6" >
                    </th>
                    <td colspan="4"><?php echo $quantidade;?> Benefício(s) encontrado(s)</td>
                </tr>
                </tfoot>

                <tbody>
                <script>
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                    })
                </script>
                <?php
                foreach ($beneficios as $dados){
                    $cb_id = $dados["id"];
                    $codnome = $dados["pessoa"];
                    $entregue = $dados["entregue"];//2
                    $beneficio = $dados["beneficio"];
                    $condicao = $dados["condicao"];
                    $quantidade = $dados["quantidade"];
                    $data_pedido = $dados["data_pedido"];
                    $descricao = $dados["descricao"];
                    $profissional = $dados["profissional"];
                    $imprimir=$dados["imprimir"];

                    if ($entregue==2){
                        $cor="bg-warning";
                    }else{
                        $cor="";
                    }

                    //BUSCA NOME DA PESSOA
                    $gente = fncgetpessoa($codnome);

                    if (isset($_GET['pv']) and is_numeric($_GET['pv'])){?>
                    <script>
                        $(document).ready(function() {
                            $('html, body').animate({
                                scrollTop: $("#<?php echo $_GET['pv'];?>").offset().top
                            }, 2000); // Tempo em ms que a animação irá durar
                            $("#<?php echo $_GET['pv'];?>").removeAttr('id');
                        });
                    </script>
                <?php } ?>

                    <tr id="<?php echo $cb_id;?>" class="<?php echo $cor;?>">
                        <th scope="row" id="<?php echo $id;  ?>">
                            <h3><i class="badge badge-success"><?php echo $cb_id; ?></i></h3>
                        </th>
                        <th scope="row" id="<?php echo $id;  ?>">
                            <small>
                                <?php echo $gente['nome']; ?>
                            </small>
                        </th>
                        <td data-toggle="tooltip" data-placement="left" data-html="true" title="<em class='text-warning'><?php echo $descricao;?></em>">
                            <?php
                            switch ($beneficio){
                                case 1:
                                    echo "<i class='fas fa-shopping-basket fa-2x'></i>";
                                    break;
                                case 2:
                                    echo "<i class='fas fa-bed fa-2x'></i>";
                                    break;
                                case 3:
                                    echo "<i class='fas fa-tshirt fa-2x'></i>";
                                    break;
                                case 4:
                                    echo "<i class='fas fa-question fa-2x'></i>";
                                    break;
                                    case 5:
                                    echo "<i class='fas fa-fire fa-2x'></i>";
                                    break;
                            }
                            ?>
                        </td>
                        <td><small>
                            <?php
                            echo fncgetbecondicao($condicao)['condicao'];
                            ?>
                            </small>
                        </td>
                        <td><?php echo $quantidade; ?></td>
                        <td>
                            <small>
                            <?php
                            if ($data_pedido < date("Y-m-d H:i:s")){
                                echo "<span class='text-danger'>";
                                echo dataRetiraHora($data_pedido);
                                echo "</span>";
                            }else{
                                echo "<span class='text-success'>";
                                echo dataRetiraHora($data_pedido);
                                echo "</span>";
                            }
                            ?>
                            </small>
                        </td>
                        <td>
                            <small>
                            <?php
                            $proff = explode(" ", fncgetusuario($profissional)['nome']);
                            echo $proff[0]." ".end($proff);
                            ?>
                            </small>
                        </td>
                        <td>
                            <small>
                            <?php
                            //buscar nome da pessoa conforme o codigo
                            echo fncgetbairro($gente['bairro'])['bairro'];
                            ?>
                            </small>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-6 p-1">
                                    <a href="index.php?pg=Vbeentregaeditar&id=<?php echo $cb_id; ?>" title="Editar entrega" class="btn btn-sm btn-outline-primary fa fa-pen float-right"></a>
                                </div>
                                <div class="col-6 p-1">
                                    <div class="dropdown show float-left" title="apagar">
                                        <a class="btn btn-sm btn-danger fa fa-trash dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "index.php?pg=Vhome&pessoa={$codnome}&aca=excluirbe&cb_id={$cb_id}";?>">Apagar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 p-1">
                                    <a href="../pb/index.php?pg=Vat&id=<?php echo $codnome; ?>" target="_blank" title="ir para pasta do usuario" class="btn btn-sm btn-outline-dark fa fa-folder float-right "></a>
                                </div>
                                <div class="col-6 p-1">
                                    <?php
                                    if($imprimir==0){
                                        echo "<a href='index.php?pg=Vhome&id={$cb_id}&aca=imprimirbesim&sca={$_GET['sca']}&b={$_GET['b']}' title='Click para alterar para sim' class='btn btn-sm btn-outline-danger fas fa-print text-danger float-left'></a>";
                                    }
                                    if($imprimir==1){
                                        echo "<a href='index.php?pg=Vhome&id={$cb_id}&aca=imprimirbenao&sca={$_GET['sca']}&b={$_GET['b']}' title='Click para alterar para não' class='btn btn-sm btn-outline-success fas fa-print text-success float-left'></a>";
                                    }
                                    ?>
                                </div>
                            </div>






                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>