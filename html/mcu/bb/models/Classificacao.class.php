<?php
class Classificacao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncclassificacaonew( $classificacao){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_biblioteca_classificacao ";
                $sql.="(id, classificacao)";
                $sql.=" VALUES ";
                $sql.="(NULL, :classificacao)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":classificacao", $classificacao);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vclassificacao_lista");
            exit();


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }











    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncclassificacaoedit($id,$classificacao){

        //inserção no banco
        try{
            $sql="UPDATE mcu_biblioteca_classificacao SET classificacao=:classificacao WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":classificacao", $classificacao);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vclassificacao_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>