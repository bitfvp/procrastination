<?php
class B_Pessoa{
    public function fncpessoaedit(
        $id,
        $nome,
        $nascimento,
        $cpf,
        $rg,
        $endereco,
        $numero,
        $bairro,
        $telefone
    ){
        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_biblioteca_pessoa";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                    $sql="UPDATE mcu_biblioteca_pessoa";
                $sql.=" SET";
                $sql .= " ultima_atualizacao=CURRENT_TIMESTAMP,
                nome=:nome, 
                nascimento=:nascimento,
                cpf=:cpf, 
                rg=:rg,  
                endereco=:endereco, 
                numero=:numero, 
                bairro=:bairro, 
                telefone=:telefone
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":nascimento", $nascimento);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log

            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew(
                                  $nome,
                                  $nascimento,
                                  $cpf,
                                  $rg,
                                  $endereco,
                                  $numero,
                                  $bairro,
                                  $telefone
    ){

        if($bairro==""){
            $bairro="0";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_biblioteca_pessoa";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();

        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_biblioteca_pessoa";
            $sql.=" WHERE rg=:rg";
            global $pdo;
            $consultarg=$pdo->prepare($sql);
            $consultarg->bindValue(":rg", $rg);
            $consultarg->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarrg=$consultarg->rowCount();


        if(($contarrg==0)or ($rg=="")){
        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                        $sql="INSERT INTO mcu_biblioteca_pessoa ";
                    $sql .= "(id,
                    ultima_atualizacao, 
                    nome,
                    nascimento,
                    cpf, 
                    rg,
                    endereco, 
                    numero, 
                    bairro, 
                    telefone
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    CURRENT_TIMESTAMP, 
                    :nome, 
                    :nascimento,
                    :cpf, 
                    :rg, 
                    :endereco, 
                    :numero, 
                    :bairro, 
                    :telefone
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":nascimento", $nascimento);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":telefone", $telefone);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse RG!!",
                "type"=>"warning",
            ];
        }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="mcu_biblioteca_pessoa";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vpessoa&id={$maid}");
                    exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }
}
?>