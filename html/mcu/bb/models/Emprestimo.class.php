<?php
class Emprestimo{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncemprestimonew($pessoa,$livro,$data,$profissional,$data_devolucao){
            //inserção no banco
            try{
                $sql="INSERT INTO mcu_biblioteca_emprestimo ";
                $sql.="(id, status, pessoa, livro, data, profissional, data_devolucao)";
                $sql.=" VALUES ";
                $sql.="(NULL, 2, :pessoa, :livro, :data, :profissional, :data_devolucao)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $pessoa);
                $insere->bindValue(":livro", $livro);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":profissional", $profissional);
                $insere->bindValue(":data_devolucao", $data_devolucao);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        try{
            $sql="UPDATE mcu_biblioteca_livro SET status=2 WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":id", $livro);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro errado '. $error_msg->getMessage();
        }

//        try {
//                $sql="UPDATE mcu_biblioteca_pessoa";
//            $sql.=" SET ";
//            $sql .= "status=2
//                WHERE id=:id";
//            global $pdo;
//            $atualiza = $pdo->prepare($sql);
//            $atualiza->bindValue(":id", $pessoa);
//            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
//
//        } catch (PDOException $error_msg) {
//            echo 'Erro' . $error_msg->getMessage();
//        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vpessoa&id={$pessoa}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
        
    }

}
?>