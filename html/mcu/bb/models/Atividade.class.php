<?php
class Atividade
{
    public function fncatividadenew($emprestimo,$profissional,$atividade,$descricao)
    {
        //inserção no banco
        try {
            $sql = "INSERT INTO mcu_biblioteca_at ";
            $sql .= "(id, emprestimo, data, profissional, atividade, descricao)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :emprestimo, CURRENT_TIMESTAMP, :profissional, :atividade, :descricao)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":emprestimo", $emprestimo);
            $insere->bindValue(":profissional", $profissional);
            $insere->bindValue(":atividade", $atividade);
            $insere->bindValue(":descricao", $descricao);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        if (isset($insere)) {
            //renovacao
            if ($atividade==1){
                $data = date('Y-m-d');
                $dat = date('Y-m-d', strtotime("+10 days",strtotime($data)));
                /// atualiza emprestimo
                try{
                    $sql="UPDATE mcu_biblioteca_emprestimo SET ";
                    $sql.="data_devolucao=:renov ";
                    $sql.=" WHERE id=:id";
                    global $pdo;
                    $atuali=$pdo->prepare($sql);
                    $atuali->bindValue(":renov", $dat);
                    $atuali->bindValue(":id", $emprestimo);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro '. $error_msg->getMessage();
                }
            }

            //devolucao
            if ($atividade==2){
                $sql = "SELECT mcu_biblioteca_emprestimo.pessoa, mcu_biblioteca_emprestimo.livro "
                    . "FROM mcu_biblioteca_emprestimo "
                    . "WHERE (((mcu_biblioteca_emprestimo.id)=?))";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $emprestimo);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $val = $consulta->fetch();
                $sql = null;
                $consulta = null;
                $valpessoa = $val[0];
                $vallivro = $val[1];

                /// atualiza emprestimo
            try{
                $sql="UPDATE mcu_biblioteca_emprestimo SET ";
                $sql.="status='1' ";
                $sql.=" WHERE id=:id";
                global $pdo;
                $atuali=$pdo->prepare($sql);
                $atuali->bindValue(":id", $emprestimo);
                $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }

                /// atualiza livro
                try{
                    $sql="UPDATE mcu_biblioteca_livro SET ";
                    $sql.="status='1' ";
                    $sql.=" WHERE id=:id";
                    global $pdo;
                    $atuali=$pdo->prepare($sql);
                    $atuali->bindValue(":id", $vallivro);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro '. $error_msg->getMessage();
                }

                /// atualiza pessoa
//                try{
//                    $sql="UPDATE mcu_biblioteca_pessoa SET ";
//                    $sql.="status='1' ";
//                    $sql.=" WHERE id=:id";
//                    global $pdo;
//                    $atuali=$pdo->prepare($sql);
//                    $atuali->bindValue(":id", $valpessoa);
//                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
//                }catch ( PDOException $error_msg){
//                    echo 'Erro '. $error_msg->getMessage();
//                }
            }


            //remove do acervo
            if ($atividade==3){
                $sql = "SELECT mcu_biblioteca_emprestimo.pessoa, mcu_biblioteca_emprestimo.livro "
                    . "FROM mcu_biblioteca_emprestimo "
                    . "WHERE (((mcu_biblioteca_emprestimo.id)=?))";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $emprestimo);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $val = $consulta->fetch();
                $sql = null;
                $consulta = null;
                $valpessoa = $val[0];
                $vallivro = $val[1];

                /// atualiza emprestimo
                try{
                    $sql="UPDATE mcu_biblioteca_emprestimo SET ";
                    $sql.="status='3' ";
                    $sql.=" WHERE id=:id";
                    global $pdo;
                    $atuali=$pdo->prepare($sql);
                    $atuali->bindValue(":id", $emprestimo);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro '. $error_msg->getMessage();
                }

                /// atualiza livro
                try{
                    $sql="UPDATE mcu_biblioteca_livro SET ";
                    $sql.="status='3' ";
                    $sql.=" WHERE id=:id";
                    global $pdo;
                    $atuali=$pdo->prepare($sql);
                    $atuali->bindValue(":id", $vallivro);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro '. $error_msg->getMessage();
                }

                /// atualiza pessoa
//                try{
//                    $sql="UPDATE mcu_biblioteca_pessoa SET ";
//                    $sql.="status='1' ";
//                    $sql.=" WHERE id=:id";
//                    global $pdo;
//                    $atuali=$pdo->prepare($sql);
//                    $atuali->bindValue(":id", $valpessoa);
//                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
//                }catch ( PDOException $error_msg){
//                    echo 'Erro '. $error_msg->getMessage();
//                }
            }




            /////////////////////////////////////////////////////
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////


            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vemprestimo&id={$emprestimo}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new

}//fim da classe

?>