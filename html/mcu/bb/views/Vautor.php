<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Autor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
    <div class="row">
        <div class="col-md-4">
            <?php include_once("includes/autorcabecalhoside.php"); ?>
        </div>

        <div class="col-md-8">

            <?php
            // Recebe
            if (isset($_GET['id'])) {
                //existe um id e se ele é numérico
                if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * ";
                    $sql .= "FROM mcu_biblioteca_livro ";
                    $sql .= "WHERE (mcu_biblioteca_livro.autor=:pessoa) ";
                    $sql .= "ORDER BY mcu_biblioteca_livro.livro ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindValue(":pessoa", $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $liv = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Livros desse autor
                </div>
                <div class="card-body">

                    <table class="table table-striped table-hover table-sm">
                        <thead class="thead-dark">
                        <tr>
                            <th>LIVRO</th>
                            <th>REGISTRO</th>
                            <th>ACERVO</th>
                            <th>STATUS</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($liv as $dados){
                            $id = $dados["id"];
                            $livro = strtoupper($dados["livro"]);
                            $autor = fncgetautor($dados["autor"])['autor'];
                            $registro = $dados["registro"];
                            $acervo = dataBanco2data($dados["acervo"]);
                            $status = $dados["status"];
                            switch ($status){
                                case 1:
                                    $statuss="<i class='text-success  fas fa-thumbs-up'>DISPONIVEL</i>";
                                    break;
                                case 2:
                                    $statuss="<i class='text-warning fas fa-book'>EMPRESTADO</i>";
                                    break;
                                case 3:
                                    $statuss="<i class='text-danger'>REMOVIDO DO ACERVO</i>";
                                    break;
                            }
                            ?>

                            <tr>
                                <td scope="row" id="<?php echo $id;  ?>">
                                    <a href="index.php?pg=Vlivro&id=<?php echo $id;?>" title="Ver livro">
                                        <?php
                                            echo $livro;
                                        ?>
                                    </a>
                                </td>
                                <td class="text-uppercase"><?php echo $registro; ?></td>
                                <td><?php echo $acervo; ?></td>
                                <td><?php echo $statuss; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>


                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>