<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Livro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="livrosave";
    $livro=fncgetlivro($_GET['id']);
}else{
    $a="livronew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vlivro_lista&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post" id="form1">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="gogogo" class="btn btn-success btn-block" value="SALVAR"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $livro['id']; ?>"/>
            <label for="">LIVRO:</label>
            <input autocomplete="off"  placeholder="livro" required="true" autofocus id="livro" type="text" class="form-control" name="livro" value="<?php echo $livro['livro']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="">REGISTRO DO LIVRO:</label>
            <input autocomplete="off"  placeholder="codigo do livro" required="true" autofocus id="registro" type="text" class="form-control" name="registro" value="<?php echo $livro['registro']; ?>"/>
        </div>

            <div class="col-md-12">
                <label for="">ENTRADA NO ACERVO:</label>
                <input autocomplete="off"  required="true" autofocus id="acervo" type="date" class="form-control" name="acervo" value="<?php echo $livro['acervo']; ?>"/>
            </div>

        <div class="col-md-12">
            <label for="">AUTOR:</label>
            <select name="autor" id="autor" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php $getautor=fncgetautor($livro['autor']); ?>
                <option selected="" data-tokens="<?php echo $getautor['autor'];?>" value="<?php echo $livro['autor']; ?>">
                    <?php echo $getautor['autor'];?>
                </option>
                <?php
                foreach (fncautorlist() as $item) {
                    ?>
                    <option data-tokens="<?php echo $item['autor'];?>" value="<?php echo $item['id'];?>">
                        <?php echo strtoupper($item['autor']); ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="col-md-12">
            <label for="">CLASSIFICAÇÃO:</label>
            <select name="classificacao" id="classificacao" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização

                <?php
                $getclassificacao=fncgetclassificacao($livro['classificacao']);
                ?>
                <option selected="" data-tokens="<?php echo $getclassificacao['classificacao'];?>" value="<?php echo $livro['classificacao']; ?>">
                    <?php echo $getclassificacao['classificacao'];?>
                </option>
                <?php
                foreach (fncclassificacaolist() as $item) {
                    ?>
                    <option data-tokens="<?php echo $item['classificacao'];?>" value="<?php echo $item['id'];?>">
                        <?php echo strtoupper($item['classificacao']); ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>


    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>