<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de livros-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = remover_caracter($_GET['sca']);
    $sql = "select * from mcu_biblioteca_livro WHERE livro LIKE '%$sca%' ";
}else {

    $sql = "select * from mcu_biblioteca_livro where id=0 ";
}

$total_reg = "50"; // número de registros por página

$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}

$inicio = $pc - 1;
$inicio = $inicio * $total_reg;

try{
    $sql2= "ORDER BY livro LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY livro LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Livros</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vlivro_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por livro..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo remover_caracter($_GET['sca']);} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vlivro_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO LIVRO
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="3">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlivro_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlivro_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th>LIVRO</th>
            <th>AUTOR</th>
            <th>REGISTRO</th>
            <th>ACERVO</th>
            <th>STATUS</th>
            <th>EDITAR</th>
        </tr>
        </thead>


        <tfoot>
        <tr>
            <th scope="row" colspan="3">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlivro_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlivro_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Livro(s) listado(s)</th>
        </tr>
        </tfoot>

<tbody>
 <?php
 if($_GET['sca']!="" and isset($_GET['sca'])) {
     $sta = strtoupper(remover_caracter($_GET['sca']));
     define('CSA', $sta);//TESTE
 }
 foreach ($limite as $dados){
            $id = $dados["id"];
            $livro = strtoupper($dados["livro"]);
            $autor = fncgetautor($dados["autor"])['autor'];
     $registro = $dados["registro"];
     $acervo = dataBanco2data($dados["acervo"]);
            $status = $dados["status"];
            switch ($status){
                case 1:
                    $statuss="<i class='text-success  fas fa-thumbs-up'>DISPONIVEL</i>";
                    break;
                case 2:
                    $statuss="<i class='text-warning fas fa-book'>EMPRESTADO</i>";
                    break;
                case 3:
                    $statuss="<i class='text-danger'>REMOVIDO DO ACERVO</i>";
                    break;
            }
            ?>

	<tr>
        <td scope="row" id="<?php echo $id;  ?>">
            <a href="index.php?pg=Vlivro&id=<?php echo $id;?>" title="Ver livro">
                <?php
                if($_GET['sca']!="") {
                    $sta = CSA;
                    $nnn = $livro;
                    $nn = explode(CSA, $nnn);
                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                    echo $n;
                }else{
                    echo $livro;
                }
                ?>
            </a>
        </td>
        <td class="text-uppercase">
            <a href="index.php?pg=Vautor&id=<?php echo $dados["autor"];?>" title="Ver livro">
            <?php echo $autor; ?>
            </a>
        </td>
        <td class="text-uppercase"><?php echo $registro; ?></td>
        <td><?php echo $acervo; ?></td>
        <td><?php echo $statuss; ?></td>
        <td><a href="index.php?pg=Vlivro_editar&id=<?php echo $id; ?>">Alterar</a></td>
	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
