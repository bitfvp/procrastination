<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Autor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="autorsave";
    $autor=fncgetautor($_GET['id']);
}else{
    $a="autornew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vautor_lista&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post" id="form1">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="gogogo" class="btn btn-success btn-block" value="SALVAR"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $autor['id']; ?>"/>
            <label for="">AUTOR:</label>
            <input autocomplete="off" autofocus id="autor" type="text" class="form-control" name="autor" value="<?php echo $autor['autor']; ?>"/>
        </div>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>