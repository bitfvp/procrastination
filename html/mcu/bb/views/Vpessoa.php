<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Pasta do Usuario-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
    <div class="row">
        <div class="col-md-4">
            <?php include_once("includes/pessoacabecalhoside.php"); ?>


            <?php
            ($pessoa['impressao']==0)? $xx="btn-outline-info text-danger": $xx="btn-info";
            ($pessoa['impressao']==0)? $xy="Impressão da carteira desativada": $xy="Impressão da carteira ativada";
            ?>
            <a class="btn <?php echo $xx;?>" href="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>&aca=bot_imprime" title="">
                <?php echo $xy;?>
            </a>

<!--                --><?php //include_once ("includes/arquivosform.php");?>
<!--                --><?php //include_once ("includes/arquivos.php");?>

        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-light">
                    FORMULÁRIO DE EMPRÉSTIMO
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vpessoa&aca=emprestimonew&id=<?php echo $_GET['id']; ?>" method="post">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">DATA: </label>
                                <input id="data" type="date" class="form-control input-sm"
                                       name="data"
                                       value="<?php echo date("Y-m-d"); ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">DEVOLUÇÃO: </label>
                                <input id="data_devolucao" type="date" class="form-control input-sm"
                                       name="data_devolucao" required
                                       value=""/>
                            </div>
<!--/////////////////////////////////////////////////////-->
                            <script type="text/javascript">

                                $(document).ready(function () {
                                    $('#serchlivro input[type="text"]').on("keyup input", function () {
                                        /* Get input value on change */
                                        var inputValf = $(this).val();
                                        var resultDropdownf = $(this).siblings(".resultlivro");
                                        if (inputValf.length) {
                                            $.get("includes/livros.php", {term: inputValf}).done(function (data) {
                                                // Display the returned data in browser
                                                resultDropdownf.html(data);
                                            });
                                        } else {
                                            resultDropdownf.empty();
                                        }
                                    });

                                    // Set search input value on click of result item
                                    $(document).on("click", ".resultlivro p", function () {
                                        var livro = this.id;
                                        $.when(
                                            // $(this).parents("#serchlivro").find('input[type="text"]').val($(this).text()),
                                            // $(this).parents("#serchlivro").find('input[type="hidden"]').val($(this).text()),
                                            // $(this).parent(".resultlivro").empty()

                                            $(this).parents("#serchlivro").find('input[type="text"]').val($(this).text()),
                                            $('#livro').val(livro),
                                            $(this).parent(".resultlivro").empty()

                                        ).then(
                                            function() {
                                                // roda depois de acao1 e acao2 terminarem
                                                // setTimeout(function() {
                                                //     //do something special
                                                //     $( "#gogo" ).click();
                                                // }, 500);
                                            });
                                    });
                                });

                            </script>
<!--/////////////////////////////////////////////////////-->

                            <div class="col-md-12" id="serchlivro">
                                <label for="livro">LIVRO: </label>
                                <div class="input-group">
                                    <?php
                                        $v_livro_id="";
                                        $v_livro="";
                                    $c_livro = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_livro;?>" id="c<?php echo $c_livro;?>" value="<?php echo $v_livro; ?>" placeholder="Click no resultado ao aparecer" required/>
                                    <input id="livro" autocomplete="false" type="hidden" class="form-control" name="livro" value="<?php echo $v_livro_id; ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="r_livro">
                                            <i class="fa fas fa-backspace "></i>
                                        </span>
                                    </div>
                                    <div class="resultlivro"></div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $("#r_fornecedor").click(function(ev){
                                            document.getElementById('livro').value='';
                                            document.getElementById('c<?php echo $c_livro;?>').value='';
                                        });
                                    });
                                </script>
                            </div>

                        </div>
                        <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                    </form>
                </div>
            </div>


            <?php
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT mcu_biblioteca_emprestimo.id, mcu_biblioteca_emprestimo.pessoa, mcu_biblioteca_emprestimo.livro as codlivro, mcu_biblioteca_emprestimo.data, mcu_biblioteca_emprestimo.data_devolucao, mcu_biblioteca_livro.livro, mcu_biblioteca_autor.autor, mcu_biblioteca_classificacao.classificacao, tbl_users.nome AS profissional, mcu_biblioteca_emprestimo.status ";
                    $sql .= "FROM tbl_users INNER JOIN ((mcu_biblioteca_autor INNER JOIN (mcu_biblioteca_classificacao INNER JOIN mcu_biblioteca_livro ON mcu_biblioteca_classificacao.id = mcu_biblioteca_livro.classificacao) ON mcu_biblioteca_autor.id = mcu_biblioteca_livro.autor) INNER JOIN mcu_biblioteca_emprestimo ON mcu_biblioteca_livro.id = mcu_biblioteca_emprestimo.livro) ON tbl_users.id = mcu_biblioteca_emprestimo.profissional ";
                    $sql .= "WHERE (((mcu_biblioteca_emprestimo.pessoa)=:pessoa)) ";
                    $sql .= "ORDER BY mcu_biblioteca_emprestimo.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindValue(":pessoa", $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $emp = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico de empréstimos
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach ($emp as $at) {
                            switch ($at['status']){
                                case 0:

                                    break;
                                case 1:
                                    $status="DEVOLVIDO";
                                    $statuss="success";
                                    break;
                                case 2:
                                    $status="AGUARDANDO ENTREGA";
                                    $statuss="warning";
                                    break;
                                case 3:
                                    $status="REMOVIDO DO ACERVO";
                                    $statuss="danger";
                                    break;
                            }
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $statuss; ?> text-uppercase">
                                <p>

                                    <strong class="text-<?php echo $statuss;?>">
                                        <a href="index.php?pg=Vlivro&id=<?php echo $at['codlivro'];?>">
                                        <?php echo $at['livro']; ?>&nbsp;&nbsp;
                                        </a>
                                    </strong>

                                </p>
                                Autor: <strong class="text-<?php echo $statuss;?>"><?php echo $at['autor']; ?>&nbsp;&nbsp;</strong><br>
                                CLASSIFICAÇÃO: <strong class="text-<?php echo $statuss;?>"><?php echo $at['classificacao']; ?>&nbsp;&nbsp;</strong><br>
                                Data: <strong class="text-<?php echo $statuss;?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                Data: <strong class="text-<?php echo $statuss;?>"><?php echo dataRetiraHora($at['data_devolucao']); ?>&nbsp;&nbsp;</strong>
                                Status: <strong class="text-<?php echo $statuss;?>"><?php echo $status; ?>&nbsp;&nbsp;</strong>

                                <a href="index.php?pg=Vemprestimo&id=<?php echo $at['id'];?>" class="btn btn-info btn-block my-2">Acessar</a>

                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    echo $at['profissional'];
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>

    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>