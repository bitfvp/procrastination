<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Histórico do livro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
    <div class="row">
        <div class="col-md-5">
            <?php include_once("includes/livrocabecalhoside.php"); ?>
        </div>

        <div class="col-md-7">

            <?php
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT mcu_biblioteca_emprestimo.id, mcu_biblioteca_emprestimo.pessoa, mcu_biblioteca_emprestimo.data, mcu_biblioteca_emprestimo.data_devolucao, mcu_biblioteca_livro.livro, mcu_biblioteca_autor.autor, mcu_biblioteca_classificacao.classificacao, tbl_users.nome AS profissional, mcu_biblioteca_emprestimo.status ";
                    $sql .= "FROM tbl_users INNER JOIN ((mcu_biblioteca_autor INNER JOIN (mcu_biblioteca_classificacao INNER JOIN mcu_biblioteca_livro ON mcu_biblioteca_classificacao.id = mcu_biblioteca_livro.classificacao) ON mcu_biblioteca_autor.id = mcu_biblioteca_livro.autor) INNER JOIN mcu_biblioteca_emprestimo ON mcu_biblioteca_livro.id = mcu_biblioteca_emprestimo.livro) ON tbl_users.id = mcu_biblioteca_emprestimo.profissional ";
                    $sql .= "WHERE (((mcu_biblioteca_emprestimo.livro)=:pessoa)) ";
                    $sql .= "ORDER BY mcu_biblioteca_emprestimo.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindValue(":pessoa", $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $emp = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Histórico de emprestimos
                </div>
                <div class="card-body">
                        <?php
                        foreach ($emp as $at) {
                        switch ($at['status']){
                            case 1:
                                $status="DEVOLVIDO";
                                $statuss="success";
                                break;
                            case 2:
                                $status="AGUARDANDO ENTREGA";
                                $statuss="warning";
                                break;
                            case 3:
                                $status="REMOVIDO DO ACERVO";
                                $statuss="danger";
                                break;
                        }
                            ?>
                        <hr>
                        <blockquote class="blockquote blockquote-<?php echo $statuss; ?>">
                        <strong class="text-<?php echo $statuss;?>">
                            <a href="index.php?pg=Vpessoa&id=<?php echo $at['pessoa'];?>">
                            <?php echo fncgetpessoa($at['pessoa'])['nome']; ?>
                            </a>
                        </strong><br>
                            Data:<strong class="text-<?php echo $statuss;?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong><br>
                            Data para entrega:<strong class="text-<?php echo $statuss;?>"><?php echo dataRetiraHora($at['data_devolucao']); ?>&nbsp;&nbsp;</strong><br>
                            Status:<strong class="text-<?php echo $statuss;?>"><?php echo $status; ?>&nbsp;&nbsp;</strong><br>

                            <a href="index.php?pg=Vemprestimo&id=<?php echo $at['id'];?>" class="btn btn-info btn-block my-2">Acessar</a>

                            <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                            <footer class="blockquote-footer">
                                <?php
                                echo $at['profissional'];
                                ?>
                            </footer>
                    </blockquote>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>