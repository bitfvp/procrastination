<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Carteira-".$env->env_titulo;
$css="carteira";

include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * "
    . "FROM mcu_biblioteca_pessoa "
    . "WHERE (((mcu_biblioteca_pessoa.impressao)=1)) order by mcu_biblioteca_pessoa.nome limit 0,4";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$carteiras = $consulta->fetchAll();
$sql=null;
$consulta=null;

?>

<div class="container">

    <?php
    foreach ($carteiras as $carteira){
    ?>
    <!-- //////////////////////////////// -->
    <div class="ladoa">
        <img src="<?php echo $env   ->env_estatico; ?>img/mcu.jpg" style="width: 56px; height: 56px; float: left;" alt="">
        <h4 style="float: left; text-align: center; "> Prefeitura Municipal de Manhuaçu</h4>
        <h6 style="float: left; text-align: center; ">Secretaria Municipal de Cultura e Turismo</h6>
        <br><br>
        <h6 style="float: left; text-align: center; "><strong>CREDENCIAL DE UTILIZAÇÃO <BR>DA BIBLIOTECA MUNICIPAL</strong></h6>
        <div class="c_numero"># B<?php echo $carteira['id']; ?></div>
        <div class="foto"></div>
        <div class="principal">
<!--            <img src="--><?php //echo $env->env_estatico; ?><!--img/mcu.jpg" alt="">-->
            <h4>Nome: <strong><?php echo strtoupper($carteira['nome']); ?></strong></h4>
            <h4>Doc. RG: <strong><?php echo $carteira['rg']; ?></strong></h4>
            <h4>Doc .: <strong><?php echo $carteira['cpf']; ?></strong></h4>
            <h4>Nasc.: <strong><?php echo dataBanco2data($carteira['nascimento']); ?></strong></h4>
            <h5>End.: <strong><?php echo strtoupper($carteira['endereco']); ?> <?php echo $carteira['numero']; ?></strong></h5>
            <h5>Bairro:
                <strong>
                    <?php
                    echo fncgetbairro($carteira['bairro'])['bairro'];
                    ?>
                </strong>
            </h5>

        </div>
        <div class="fot">
        <div class="emissao">Emissão: <?php echo date('d/m/Y'); ?>
            <br>
            Validade: <?php echo date('d/m/');
            $zzz=date('Y')+2;
            echo $zzz;
             ?>
        </div>
        <div class="assisA">
        <div class="assis">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Responsável
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        </div>


        </div>
    </div>
    <!-- //////////////////////////////////// -->
    <div class="ladob">
        <br>
        <h4>AO PORTADOR É DADO O DIREITO DE</h4>
        <h3> UTILIZAÇÃO DA BIBLIOTECA MUNICIPAL PROFª CUSTÓDIA FÉRES ABI-SÁBER</h3>
        <br><br>
        <h6>LEI MUNICIPAL N 1495 DE 7 DE MARÇO DE 1986</h6>
        <BR>
        <h6>A BIBLIOTECÁRIA PODERÁ SOLICITAR PARA COMPROVAÇÃO UM DOCUMENTO DO USUÁRIO</h6>
        <BR><BR>
        <h3><strong>USO PESSOAL E INTRANSFERÍVEL</strong></h3>
<!--        <h6 class="pull-right"> FlavioW<i class="fa fa-cogs"></i>rks&nbsp;</h6>-->
    </div>
        <hr style="width: 100%;">
    <?php } ?>


</div>

</body>
</html>