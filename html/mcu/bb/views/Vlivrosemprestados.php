<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Livros emprestados-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
<div class="row">
	<div class="col-md-12">
        <?php
        $sql = "SELECT * ";
        $sql .= "FROM mcu_biblioteca_emprestimo ";
        $sql .= "WHERE ((mcu_biblioteca_emprestimo.status=2)) order by mcu_biblioteca_emprestimo.data ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":emprestimo", $id_emprestimo);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $att = $consulta->fetchall();
        $sql = null;
        //$consulta = null;
        ?>
        <div class="card">
            <div class="card-header bg-info text-light">
                Livros que estão emprestados
            </div>
            <div class="card-body">

                <table class="table table-striped table-hover table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <td>Livro</td>
                        <td>Pessoa</td>
                        <td>Emprestimo</td>
                        <td>Devolução</td>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th colspan="4" class="text-info text-right"><?php echo $consulta->rowCount();?> Encontrado(s)</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    foreach ($att as $at){
                        echo "<tr>";
                            echo "<td>";
                            echo fncgetlivro($at['livro'])['livro']." #R ".fncgetlivro($at['livro'])['registro']." #A ".dataBanco2data(fncgetlivro($at['livro'])['acervo']);
                            echo "</td>";

                            echo "<td>";
                        echo "<a href='index.php?pg=Vpessoa&id=".$at['pessoa']."'>";
                            echo fncgetpessoa($at['pessoa'])['nome'];
                            echo "</a>";
                        echo "</td>";

                            echo "<td>";
                        echo dataRetiraHora($at['data']);
                            echo "</td>";

                            echo "<td>";
                        echo dataRetiraHora($at['data_devolucao']);
                            echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>

            </div>
        </div>


	</div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>