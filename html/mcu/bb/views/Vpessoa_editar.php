<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vpessoa_lista&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post" id="form1">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="gogogo" class="btn btn-success btn-block" value="SALVAR"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $pessoa['id']; ?>"/>
            <label for="nome">NOME:</label>
            <input  id="nome" name="nome" autocomplete="off" placeholder="Nome completo" required="true" autofocus type="text" class="form-control" value="<?php echo $pessoa['nome']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="">NASCIMENTO:</label>
            <input  id="nascimento" name="nascimento" autocomplete="off" required="true" autofocus type="date" class="form-control" value="<?php echo $pessoa['nascimento']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="">CPF:</label>
            <input  id="cpf" name="cpf" autocomplete="off" required="true" autofocus type="text" class="form-control" value="<?php echo $pessoa['cpf']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="">RG:</label>
            <input  id="rg" name="rg" autocomplete="off" required="true" autofocus type="text" class="form-control" value="<?php echo $pessoa['rg']; ?>"/>
        </div>
        <script>
            $(document).ready(function(){
                $('#cpf').mask('000.000.000-00', {reverse: false});
                $('#rg').mask('00.000.000.000', {reverse: true});
                $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
            });
        </script>
        <div class="col-md-6">
            <label for="">ENDEREÇO:</label>
            <input  id="endereco" name="endereco" autocomplete="off" required="true" autofocus type="text" class="form-control" value="<?php echo $pessoa['endereco']; ?>"/>
        </div>
        <div class="col-md-2">
            <label for="">NÚMERO:</label>
            <input  id="numero" name="numero" autocomplete="off" required="true" autofocus type="number" class="form-control" value="<?php echo $pessoa['numero']; ?>"/>
        </div>
        <div class="col-md-4">
            <label for="">BAIRRO:</label>
            <select name="bairro" id="bairro" class="form-control input-sm <?php //echo "selectpicker";?>" data-live-search="true">
                // vamos criar a visualização
                <?php
                $getbairro=fncgetbairro($pessoa['bairro']);
                ?>
                <option selected="" data-tokens="<?php echo $getbairro['bairro'];?>" value="<?php echo $pessoa['bairro']; ?>">
                    <?php echo $getbairro['bairro'];?>
                </option>
                <?php
                foreach (fncbairrolist() as $item) {
                    ?>
                    <option data-tokens="<?php echo $item['bairro'];?>" value="<?php echo $item['id'];?>">
                        <?php echo $item['bairro']; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-md-6">
            <label for="">TELEFONE:</label>
            <input  id="telefone" name="telefone" autocomplete="off" required="true" autofocus type="text" class="form-control" value="<?php echo $pessoa['telefone']; ?>"/>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>