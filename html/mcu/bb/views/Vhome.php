<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>

<main class="container">
    <div class="row">


        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Carteiras aguardando impressão
                </div>
                <div class="card-body">
                    <?php
                    $sql = "SELECT * \n"
                        . "FROM mcu_biblioteca_pessoa \n"
                        . "WHERE (mcu_biblioteca_pessoa.impressao)=1 order by mcu_biblioteca_pessoa.nome";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $lista = $consulta->fetchAll();
                    $listaCont = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;
                    ?>
                    <a href="index.php?pg=Vcarteirinhas" target="_blank" class="btn btn-block btn-success mb-2"> IMPRIMIR CARTEIRAS</a>
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>
                                NOME
                            </th>
                            <th>
                                AÇÃO
                            </th>
                        </tr>
                        </thead>
                        <tbody class="text-uppercase">
                        <?php
                        if ($listaCont!=0){
                            $cont=0;
                            foreach ($lista as $lt) {
                                $cont++;
                                ?>
                                <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                    <td><a href="index.php?pg=Va&id=<?php echo $lt['id'];?>"><?php echo $lt['nome'];?></a></td>
                                    <td><a href='?pg=Vhome&aca=bot_imprime2&id=<?php echo $lt['id'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo "<tr><td colspan='2' class='text-success'>Não há passes aguardando para imprimir!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-danger text-center">
                    São impressas apenas 4 por vez.
                </div>
            </div>
        </div>
    </div>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
