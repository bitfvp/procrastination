<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de pessoas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = remover_caracter($_GET['sca']);
    $sql = "select * from mcu_biblioteca_pessoa WHERE nome LIKE '%$sca%' ";
}else {

    $sql = "select * from mcu_biblioteca_pessoa where id=0 ";
}

$total_reg = "50"; // número de registros por página

$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}

$inicio = $pc - 1;
$inicio = $inicio * $total_reg;

try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Pessoas</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vpessoa_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo remover_caracter($_GET['sca']);} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vpessoa_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVA PESSOA
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="3">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vpessoa_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vpessoa_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th>NOME</th>
            <th>NASCIMENTO</th>
            <th>CPF</th>
            <th>RG</th>
            <th>EDITAR</th>
        </tr>
        </thead>

        <tfoot>
        <tr>
            <th scope="row" colspan="3">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vpessoa_lista&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vpessoa_lista&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Pessoa(s) listada(s)</th>
        </tr>
        </tfoot>


        <tbody>
 <?php
 $sta = strtoupper(remover_caracter($_GET['sca']));
 define('CSA', $sta);//TESTE
 foreach ($limite as $dados){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $nascimento = dataBanco2data($dados["nascimento"]);
            $cpf = $dados["cpf"];
     $rg = $dados["rg"];
            $status = $dados["status"];
//            switch ($status){
//                case 0:
//                    $statuss="<i class='text-danger'>IRREGULAR</i>";
//                    break;
//                case 1:
//                    $statuss="<i class='text-success fas fa-thumbs-up' title='tudo correto'></i>";
//                    break;
//                case 2:
//                    $statuss="<i class='text-warning fas fa-book'  title='está com um livro emprestado'></i>";
//                    break;
//            }
            ?>

	<tr>
        <td scope="row" id="<?php echo $id;  ?>">
            <a href="index.php?pg=Vpessoa&id=<?php echo $id;?>" title="Ver pessoa">
                <?php
                if($_GET['sca']!="") {
                    $sta = CSA;
                    $nnn = $nome;
                    $nn = explode(CSA, $nnn);
                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                    echo $n;
                }else{
                    echo $nome;
                }
                ?>
            </a>
        </td>

		<td><?php echo $nascimento; ?></td>
		<td><?php echo $cpf; ?></td>
        <td><?php echo $rg; ?></td>
		<td>
            <a href="index.php?pg=Vpessoa_editar&id=<?php echo $id; ?>">Alterar</a>
        </td>

	</tr>

	<?php
 }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
