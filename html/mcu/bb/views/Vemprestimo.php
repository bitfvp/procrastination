<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_72"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Emprestimo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-7">
        <div class="card">
            <div class="card-header bg-info text-light">
                Lançamento de atividades
            </div>
            <div class="card-body">
                <form action="?pg=Vemprestimo&id=<?php echo $_GET['id']; ?>&aca=newat" method="post">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Atividade:</label>
                            <select name="atividade" id="atividade" class="form-control input-sm" required>
                                // vamos criar a visualização
                                <option selected="" value="0">Selecione...</option>
                                <option value="1">Renovacao</option>
                                <option value="2">Devolucao</option>
                                <option value="3">Removido do acervo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="descricao">Descrição:</label>
                            <textarea id="descricao" onkeyup="limite_textarea(this.value,200,descricao,'cont')" maxlength="200"
                                      class="form-control" rows="3" name="descricao"></textarea>
                            <span id="cont">200</span>/200
                        </div>
                    </div>
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                </form>
            </div>
        </div>

        <?php
        // Recebe
        if (isset($_GET['id'])) {
            $id_den = $_GET['id'];
            //existe um id e se ele é numérico
            if (!empty($id_den) && is_numeric($id_den)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT mcu_biblioteca_at.id, mcu_biblioteca_at.emprestimo, mcu_biblioteca_at.data, tbl_users.nome AS profissional, mcu_biblioteca_atlista.atividade, mcu_biblioteca_at.descricao "
                    ."FROM tbl_users INNER JOIN (mcu_biblioteca_atlista INNER JOIN mcu_biblioteca_at ON mcu_biblioteca_atlista.id = mcu_biblioteca_at.atividade) ON tbl_users.id = mcu_biblioteca_at.profissional "
                    ."WHERE (((mcu_biblioteca_at.emprestimo)=?)) "
                    ."ORDER BY mcu_biblioteca_at.data DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_den);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $ativi = $consulta->fetchAll();
                $sql = null;
                $consulta = null;
            }
        }
        ?>

        <div class="card mt-2">
            <div class="card-header bg-info text-light">
                Histórico do Empréstimo
            </div>
            <div class="card-body">
                <h5 id="">
                    <?php
                    foreach ($ativi as $at) {
                        ?>
                        <hr>
                        <blockquote class="blockquote blockquote-info">
                            “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                            <br>
                            <strong class="text-info"><?php echo $at['atividade']; ?>&nbsp;&nbsp;</strong><br>
                            <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                            <span class="badge badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                            <footer class="blockquote-footer">
                                <?php
                                echo $at['profissional'];
                                ?>
                            </footer>

                        </blockquote>
                        <?php
                    }
                    ?>
                </h5>

            </div>
        </div>
	</div>
<!--    /////////////////////////////////////////////////////////////////////////////////////////////-->
	<div class="col-md-5">
<?php
if (isset($_GET['id'])) {
    $id_emprestimo = $_GET['id'];
    //existe um id e se ele é numérico
    if (!empty($id_emprestimo) && is_numeric($id_emprestimo)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT mcu_biblioteca_emprestimo.id, mcu_biblioteca_pessoa.nome, mcu_biblioteca_emprestimo.status, mcu_biblioteca_emprestimo.data, mcu_biblioteca_emprestimo.data_devolucao, mcu_biblioteca_livro.livro, mcu_biblioteca_autor.autor, mcu_biblioteca_classificacao.classificacao ";
        $sql .= "FROM (mcu_biblioteca_autor INNER JOIN (mcu_biblioteca_classificacao INNER JOIN mcu_biblioteca_livro ON mcu_biblioteca_classificacao.id = mcu_biblioteca_livro.classificacao) ON mcu_biblioteca_autor.id = mcu_biblioteca_livro.autor) INNER JOIN (mcu_biblioteca_pessoa INNER JOIN mcu_biblioteca_emprestimo ON mcu_biblioteca_pessoa.id = mcu_biblioteca_emprestimo.pessoa) ON mcu_biblioteca_livro.id = mcu_biblioteca_emprestimo.livro ";
        $sql .= "WHERE (((mcu_biblioteca_emprestimo.id)=:emprestimo)) ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":emprestimo", $id_emprestimo);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $at = $consulta->fetch();
        $sql = null;
        $consulta = null;
        switch ($at['status']){

            case 1:
                $status="DEVOLVIDO";
                $statuss="success";
                break;
            case 2:
                $status="AGUARDANDO ENTREGA";
                $statuss="warning";
                break;
            case 3:
                $status="REMOVIDO DO ACERVO";
                $statuss="danger";
                break;
        }
    }
}
        ?>
        <div class="card mt-2">
            <div class="card-header bg-info text-light">
                Empréstimo
            </div>
            <div class="card-body">
                <blockquote class="blockquote blockquote-<?php echo $statuss; ?> text-uppercase">
                    <h5>Usuário: <strong class="text-<?php echo $statuss;?>"><?php echo $at['nome']; ?>&nbsp;&nbsp;</strong></h5>
                    <h5>Livro: <strong class="text-<?php echo $statuss;?>"><?php echo $at['livro']; ?>&nbsp;&nbsp;</strong></h5>
                    <hr>
                    <h5>Autor: <strong class="text-info"><?php echo $at['autor']; ?></strong></h5>
                    <h5>Classificação: <strong class="text-info"><?php echo $at['classificacao']; ?></strong></h5>
                    <h5>Data: <strong class="text-info"><?php echo dataRetiraHora($at['data']); ?></strong></h5>
                    <h5>Data de devolução: <strong class="text-info"><?php echo dataRetiraHora($at['data_devolucao']); ?></strong></h5>
                    <footer class="blockquote-footer">
                        Status: <strong class="text-<?php echo $statuss;?>"><?php echo $status; ?></strong>
                    </footer>
                </blockquote>
            </div>
        </div>
    </div>

</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>