<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Arquivos
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
                <div class="row">
                    <?php
                    $files = glob("arquivos/" . $_GET['id'] . "/*.*");
                    for ($i = 0; $i < count($files); $i++) {
                        $num = $files[$i];
                        echo "<div class='col-md-6'>
                               <a href=" . $num . " target='_blank'>
                                   <img src=" . $num . "  alt='...' class='img-fluid img-thumbnail'>
                               </a>
                           </div>";
                    }
                    ?>
                </div>
            <footer class="blockquote-footer text-danger">
                Click no arquivo pra acessar
            </footer>
        </blockquote>
    </div>
</div>