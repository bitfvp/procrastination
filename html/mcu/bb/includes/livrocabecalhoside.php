<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $livro=fncgetlivro($_GET['id']);
}else{
    echo "erro";
}

switch ($livro['status']){
    case 3:
        $status="Retirado do acervo";
        $statuss="danger";
        break;
    case 1:
        $status="Disponível";
        $statuss="Success";
        break;
    case 2:
        $status="Aguardando entrega";
        $statuss="warning";
        break;
}
?>

<div class="container-fluid">
    <h5 class="ml-3">DADOS DO LIVRO</h5>
    <blockquote class="blockquote blockquote-info text-uppercase">
        <a class="btn btn-success btn-block mb-2" href="?pg=Vlivro_editar&id=<?php echo $_GET['id']; ?>" title="Edite os dados desse livro">
            EDITAR LIVRO
        </a>

        <h5>
          Título: <strong class="text-info"><?php echo $livro['livro']; ?>&nbsp;&nbsp;</strong>
      </h5>
      <h5>Status: <strong class="text-info"><?php echo $status; ?></strong>
      </h5>
      <hr>
      <h5>Número de registro: <strong class="text-info"><?php echo $livro['registro']; ?></strong>
      </h5>
        <h5>Data de entrada no acervo:
            <strong class="text-info"><?php echo dataBanco2data($livro['acervo']); ?></strong>
        </h5>

      <h5>Autor: <strong class="text-info"><?php
                echo fncgetautor($livro['autor'])['autor'];
            ?>
            </strong>
        </h5>

      <h5>Classificação: <strong class="text-info"><?php
              echo fncgetclassificacao($livro['classificacao'])['classificacao'];
              ?>
          </strong>
      </h5>

        <footer class="blockquote-footer">Favor, mantenha atualizado</footer>
    </blockquote>
</div>
