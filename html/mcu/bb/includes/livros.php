<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES latin1');
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


if(isset($_REQUEST['term'])){
    // Prepare a select statement
    $sql = "SELECT * FROM mcu_biblioteca_livro WHERE livro LIKE '%$_REQUEST[term]%' and status=1 limit 0,15";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $listlivro = $consulta->fetchall();
    $contlivro = $consulta->rowCount();
    $sql=null;
    $consulta=null;

            if($contlivro > 0){
                // Fetch result rows as an associative array
                foreach ($listlivro as $lc){
                    //echo utf8_encode("<p>#" . $lc["id"] . "#  <i class='text-danger'>". $lc["livro"]."</i>  #" . $lc["id"] ."# ".dataBanco2data($lc["acervo"])."</p>");
//                    echo "<p>#" . $lc["id"] . "#  <i class='text-danger'>". $lc["livro"]."</i>  #" . $lc["id"] ."# ".dataBanco2data($lc["acervo"])."</p>";
                    echo "<p id='{$lc["id"]}' class='btn btn-block my-0'>". strtoupper($lc["livro"])." - ".dataBanco2data($lc["acervo"])."</p>";
                }
            } else{
                echo "<p>Sem combinações encontradas</p>";
            }
    }
?>