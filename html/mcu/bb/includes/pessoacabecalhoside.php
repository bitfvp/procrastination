<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "erro";
}
?>

<div class="container-fluid">
    <h5 class="ml-3">DADOS DO USUÁRIO</h5>
    <blockquote class="blockquote blockquote-info text-uppercase">
        <a class="btn btn-success btn-block mb-2" href="?pg=Vpessoa_editar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
            EDITAR PESSOA
        </a>

        <h5>Nome: <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong></h5>
        <hr>
    <h5>CPF: <strong class="text-info"><?php
            if($pessoa['cpf']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['cpf'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
            </h5>
    
    <h5>RG: <strong class="text-info"><?php
            if($pessoa['rg']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['rg'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
            </h5>
    
    <h5>Nascimento: <strong class="text-info"><?php
            if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="") {
                echo "<span class='text-info'>";
                echo dataBanco2data ($pessoa['nascimento']);
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
            </h5>
    
    <h5>Endereço: <strong class="text-info"><?php
            if($pessoa['endereco']!=""){
                echo "<span class='azul'>";
                echo $pessoa['endereco'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
        ?>&nbsp;&nbsp;</strong>
            </h5>
    
    <h5>Número: <strong class="text-info"><?php
            if($pessoa['numero']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['numero'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>&nbsp;&nbsp;</strong>
        </h5>
    
    <h5>Bairro: <strong class="text-info"><?php
            if ($pessoa['bairro'] != "0") {
                $cadbairro=fncgetbairro($pessoa['bairro']);
                echo $cadbairro['bairro'];
            } else {
                echo "<span class='text-warning'>[---]</span>";
            }
            ?>&nbsp;&nbsp;</strong>
        </h5>
    
    <h5>Telefone: <strong class="text-info"><?php
            if($pessoa['telefone']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['telefone'];
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
            </strong>
        </h5>

        <footer class="blockquote-footer">Favor, mantenha atualizado</footer>
    </blockquote>
</div>
