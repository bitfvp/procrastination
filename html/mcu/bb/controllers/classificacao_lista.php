<?php
function fncclassificacaolist(){
    $sql = "SELECT * FROM mcu_biblioteca_classificacao ORDER BY classificacao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $classificacaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $classificacaolista;
}

function fncgetclassificacao($id){
    $sql = "SELECT * FROM mcu_biblioteca_classificacao WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getclassificacao = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getclassificacao;
}