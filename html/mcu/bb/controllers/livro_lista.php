<?php
function fnclivrolist(){
    $sql = "SELECT * FROM mcu_biblioteca_livro ORDER BY livro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $livrolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $livrolista;
}

function fncgetlivro($id){
    $sql = "SELECT * FROM mcu_biblioteca_livro WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getlivro = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getlivro;
}
?>
