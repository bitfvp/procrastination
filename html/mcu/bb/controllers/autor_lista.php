<?php
function fncautorlist(){
    $sql = "SELECT * FROM mcu_biblioteca_autor ORDER BY autor";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $autorlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $autorlista;
}

function fncgetautor($id){
            $sql = "SELECT * FROM mcu_biblioteca_autor WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getautor = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getautor;
}
?>