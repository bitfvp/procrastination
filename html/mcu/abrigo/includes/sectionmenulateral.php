<section class="sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <a href="?pg=Vreg&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-folder"></i>   REGISTROS</a>
        <a href="?pg=Vage&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-clock"></i>   AGENDAMENTOS</a>
        <a href="?pg=Vbe&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fas fa-paperclip"></i>    ANEXOS</a>
    </div>
</section>
<script type="application/javascript">
    var offset = $('#sidebar').offset().top;
    var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
    $(document).on('scroll', function () {
        if (offset <= $(window).scrollTop()) {
            $meuMenu.addClass('fixarmenu');
        } else {
            $meuMenu.removeClass('fixarmenu');
        }
    });
</script>