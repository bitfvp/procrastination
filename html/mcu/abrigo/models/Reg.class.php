<?php
class Reg
{
    public function fncregnew($at_pessoa,$at_profissional,$at_data,$at_restricao,$at_titulo,$at_descricao,$at_tipo,$Upin)
    {
        global $W_O_R_D_S;
        $W_O_R_D_S->fncwords($at_descricao);

        try {
            $sql = "INSERT INTO mcu_abrigo_reg ";
            $sql .= "(id, pessoa, data, profissional, restricao, titulo, descricao, tipo)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, :data, :profissional, :restricao, :titulo, :descricao, :tipo)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $at_pessoa);
            $insere->bindValue(":data", $at_data);
            $insere->bindValue(":profissional", $at_profissional);
            $insere->bindValue(":restricao", $at_restricao);
            $insere->bindValue(":titulo", $at_titulo);
            $insere->bindValue(":descricao", $at_descricao);
            $insere->bindValue(":tipo", $at_tipo);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }
        if (isset($insere)) {
            //foto
            $sql = "SELECT Max(id) FROM mcu_abrigo_reg";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mxv = $consulta->fetch();
            $sql = null;
            $consulta = null;
            $maxv = $mxv[0];
            // verifica se foi enviado um arquivo
            $fillle=$_FILES['arquivo']['name'];
            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
                if (is_dir("../../dados/mcu/abrigo/reg/" . $maxv . '/')) {} else {mkdir("../../dados/mcu/abrigo/reg/" . $maxv . '/');}
                // verifica se foi enviado um arquivo
                $Upin->get(
                    '../../dados/mcu/abrigo/reg/'.$maxv.'/', //Pasta de uploads (previamente criada)
                    $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
                    10, //Tamanho máximo
                    "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
                    "arquivo", //Atributo name do input file
                    1 //Mudar o nome? 1 = sim, 0 = não
                );
                $Upin->run();
            }
            /////////////////////////////////////////////////////
            //reservado para log

            //////////////////////////////////////////////////////////////////////////// cras 2 creas 5


            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vreg&id={$at_pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new

    public function fncatividadedelete($at_id,$prof,$pessoa_id){
        try {
            $sql = "DELETE FROM `mcu_pb_at` WHERE id = :at_id and profissional = :prof_id";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":at_id", $at_id);
            $exclui->bindValue(":prof_id", $prof);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Atividade Excluida Com Sucesso",
            "type"=>"success",
        ];

        //reservado para log
        global $LL; $LL->fnclog($pessoa_id,$_SESSION['id'],"Apagar atividade",2,4);
        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg=Vreg&id={$pessoa_id}");
        exit();
    }//fim da fnc delete


}//fim da classe

?>