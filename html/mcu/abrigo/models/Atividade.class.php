<?php
class Atividade
{
    public function fncatnew($id_prof,$descricao,$tipo)
    {

        global $W_O_R_D_S;
        $W_O_R_D_S->fncwords($descricao);
        //inserção no banco
        try {
            $sql = "INSERT INTO mcu_abrigo_at ";
            $sql .= "(id, data, profissional, descricao, tipo)";
            $sql .= " VALUES ";
            $sql .= "(NULL, CURRENT_TIMESTAMP, :profissional, :descricao, :tipo)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":tipo", $tipo);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: index.php");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }

}

?>