<?php
class Pessoa{
    public function fncpessoaedit(
        $id,
        $nome,
        $nome_social,
        $sexo,
        $nascimento,
        $naturalidade,
        $cpf,
        $rg,
        $uf_rg,
        $endereco,
        $numero,
        $bairro,
        $municipio,
        $cep,
        $referencia,
        $raca_cor,
        $mae,
        $pai,
        $deficiencia,
        $deficiencia_desc
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_abrigo_pessoas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE mcu_abrigo_pessoas";
                $sql.=" SET";
                $sql .= " nome=:nome, 
                nome_social=:nome_social, 
                sexo=:sexo, 
                nascimento=:nascimento,
                naturalidade=:naturalidade,
                cpf=:cpf, 
                rg=:rg, 
                uf_rg=:uf_rg,
                endereco=:endereco, 
                numero=:numero, 
                bairro=:bairro, 
                municipio=:municipio, 
                cep=:cep, 
                referencia=:referencia, 
                raca_cor=:raca_cor, 
                mae=:mae, 
                pai=:pai, 
                deficiencia=:deficiencia,
                deficiencia_desc=:deficiencia_desc
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":nome_social", $nome_social);
                $atualiza->bindValue(":sexo", $sexo);
                $atualiza->bindValue(":nascimento", $nascimento);
                $atualiza->bindValue(":naturalidade", $naturalidade);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":uf_rg", $uf_rg);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":referencia", $referencia);
                $atualiza->bindValue(":municipio", $municipio);
                $atualiza->bindValue(":cep", $cep);
                $atualiza->bindValue(":raca_cor", $raca_cor);
                $atualiza->bindValue(":mae", $mae);
                $atualiza->bindValue(":pai", $pai);
                $atualiza->bindValue(":deficiencia", $deficiencia);
                $atualiza->bindValue(":deficiencia_desc", $deficiencia_desc);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew(
        $nome,
        $nome_social,
        $sexo,
        $nascimento,
        $naturalidade,
        $cpf,
        $rg,
        $uf_rg,
        $endereco,
        $numero,
        $bairro,
        $municipio,
        $cep,
        $referencia,
        $raca_cor,
        $mae,
        $pai,
        $deficiencia,
        $deficiencia_desc
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));

        if($sexo==""){
            $sexo="0";
        }
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        if($bairro==""){
            $bairro="0";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_abrigo_pessoas";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();

        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_abrigo_pessoas";
            $sql.=" WHERE rg=:rg";
            global $pdo;
            $consultarg=$pdo->prepare($sql);
            $consultarg->bindValue(":rg", $rg);
            $consultarg->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarrg=$consultarg->rowCount();



        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO mcu_abrigo_pessoas ";
                    $sql .= "(id,
                    data_cadastro, 
                    resp_cadastro, 
                    nome, 
                    nome_social, 
                    sexo, 
                    nascimento,
                    naturalidade,
                    cpf, 
                    rg, 
                    uf_rg, 
                    endereco, 
                    numero, 
                    bairro,
                    municipio,
                    cep,
                    referencia, 
                    raca_cor,
                    mae, 
                    pai, 
                    deficiencia, 
                    deficiencia_desc
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    CURRENT_TIMESTAMP, 
                    :resp_cadastro, 
                    :nome, 
                    :nome_social, 
                    :sexo, 
                    :nascimento,
                    :naturalidade,
                    :cpf, 
                    :rg, 
                    :uf_rg, 
                    :endereco, 
                    :numero, 
                    :bairro,
                    :municipio,
                    :cep,
                    :referencia, 
                    :raca_cor,
                    :mae, 
                    :pai, 
                    :deficiencia, 
                    :deficiencia_desc
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":resp_cadastro", $_SESSION['id']);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":nome_social", $nome_social);
                    $insere->bindValue(":sexo", $sexo);
                    $insere->bindValue(":nascimento", $nascimento);
                    $insere->bindValue(":naturalidade", $naturalidade);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":uf_rg", $uf_rg);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":municipio", $municipio);
                    $insere->bindValue(":cep", $cep);
                    $insere->bindValue(":referencia", $referencia);
                    $insere->bindValue(":raca_cor", $raca_cor);
                    $insere->bindValue(":mae", $mae);
                    $insere->bindValue(":pai", $pai);
                    $insere->bindValue(":deficiencia", $deficiencia);
                    $insere->bindValue(":deficiencia_desc", $deficiencia_desc);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!!",
                "type"=>"warning",
            ];
        }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="mcu_abrigo_pessoas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log

            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vpessoa&id={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }




}
