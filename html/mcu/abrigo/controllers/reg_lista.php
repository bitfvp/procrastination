<?php
function fnc_reglist(){
    $sql = "SELECT * FROM mcu_abrigo_reglist ORDER BY titulo";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $reglista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $reglista;
}

function fncget_reglist($id){
    $sql = "SELECT * FROM mcu_abrigo_reglist WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getreg = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getreg;
}
?>
