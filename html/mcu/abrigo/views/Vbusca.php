<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_24"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Busca-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    //consulta se ha busca
    $sql = "select * from mcu_abrigo_pessoas WHERE nome LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_abrigo_pessoas WHERE id=0 ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <div class="container">
        <h2>Cadastros de acolhimentos</h2>
        <hr>
        <form action="index.php" method="get">
            <div class="input-group mb-3 col-md-6 float-left">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                </div>
                <input name="pg" value="Vbusca" hidden/>
                <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            </div>
        </form>
        <a href="index.php?pg=Vpessoaeditar" class="btn btn btn-success btn-block col-md-6 float-right">
            NOVO CADASTRO
        </a>

        <script type="text/javascript">
            function selecionaTexto()
            {
                document.getElementById("sca").select();
            }
            window.onload = selecionaTexto();
        </script>
    </div>

    <div class="container">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th scope="row" colspan="3">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-sm mb-0">
                            <?php
                            // agora vamos criar os botões "Anterior e próximo"
                            $anterior = $pc -1;
                            $proximo = $pc +1;
                            if ($pc>1) {
                                echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                            }
                            echo "|";
                            if ($pc<$tp) {
                                echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                            }
                            ?>
                        </ul>
                    </nav>
                </th>
                <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
            </tr>
            </thead>

            <thead class="thead-dark">
            <tr>
                <th scope="col">NOME</th>
                <th scope="col">NASCIMENTO</th>
                <th scope="col">CPF</th>
                <th scope="col">RG</th>
                <th scope="col">EDITAR</th>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <th scope="row" colspan="3">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <?php
                            // agora vamos criar os botões "Anterior e próximo"
                            $anterior = $pc -1;
                            $proximo = $pc +1;
                            if ($pc>1) {
                                echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                            }
                            if ($pc<$tp) {
                                echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                            }
                            ?>
                        </ul>
                    </nav>
                </th>
                <th colspan="2" class="text-info text-right"><?php echo $todos->rowCount();?> pessoa(s) listada(s)</th>
            </tr>
            </tfoot>

            <?php
            if(isset($_GET['sca']) and $_GET['sca']!="") {
                $sta = strtoupper($_GET['sca']);
                define('CSA', $sta);//TESTE
            }
            // vamos criar a visualização
            while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $nascimento = dataBanco2data ($dados["nascimento"]);
            $cpf = $dados["cpf"];
            $rg = $dados["rg"];
            $cod_familiar=$dados["cod_familiar"];
            ?>
            <tbody>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if(isset($_GET['sca']) and $_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>
                <td>
                    <?php
                    if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                        echo $nascimento;
                    }else{
                        echo "<span class='text-danger'>--/--/----</span>";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($cpf!="0" and $cpf!="") {
                        echo mask($cpf,'###.###.###_##');
                    }else{
                        echo "--- --- --- --";
                    }
                    ?></td>
                <td>
                    <?php
                    if($rg!="0" and $rg!="") {
                        echo $rg;
                    }else{
                        echo "--- --- ---";
                    }
                    ?></td>
                <td>
                        <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa">
                            Alterar
                        </a>
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>



</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>