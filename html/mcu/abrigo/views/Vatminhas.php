<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_24"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
        }
    }
}

$page = "Minhas atividades-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Recebe
            $id_user = $_SESSION['id'];
            //existe um id e se ele é numérico
            if (!empty($id_user) && is_numeric($id_user)) {
                // Captura os dados do cliente solicitado
                $sql = "SELECT * \n"
                    . "FROM mcu_abrigo_at \n"
                    . "WHERE (profissional=?) \n"
                    . "ORDER BY data DESC LIMIT 0,1000";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$id_user);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mat = $consulta->fetchAll();
                $contar = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Minhas atividades - { <?php echo $contar ?> }
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach ($mat as $at) {?>
                            <hr>

                            <blockquote class="blockquote blockquote-info">
                                <h5>
                                    “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                                    <br>
                                    <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                                    <span class="badge badge-pill badge-info float-right"><strong><?php echo $at['id']; ?></strong></span>
                                    <footer class="blockquote-footer">
                                        <?php
                                        $us=fncgetusuario($at['profissional']);
                                        echo $us['nome'];
                                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                        ?>
                                    </footer>
                                </h5>
                            </blockquote>
                        <?php } ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>