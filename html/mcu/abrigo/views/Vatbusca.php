<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_24"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Busca-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    //consulta se ha busca
    $sql = "select * from mcu_abrigo_at WHERE descricao LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_abrigo_at WHERE id=0 ";
}
// total de registros a serem exibidos por página
$total_reg = "30"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container">
    <h2>Busca por relato</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-12 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vatbusca" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por relato..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <br>
    <table class="table table-stripe table-hover table-condensed table-striped">
        <tfoot>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc - 1;
                        $proximo = $pc + 1;
                        if ($pc > 1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$anterior}'><span aria-hidden=\"true\">&laquo; Anterior</a></li> ";
                        }
                        if ($pc < $tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="1" class="text-right text-info"><?php echo $todos->rowCount(); ?> Registro(s) listado(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
        while ($dados = $limite->fetch()){
            $id = $dados["id"];
            $prof = $dados["profissional"];
            $data = datahoraBanco2data($dados["data"]);
            $descricao = strtoupper($dados["descricao"]);
            ?>
            <tr>
                <td scope="row" id="<?php echo $id; ?>" colspan="2">
                    <blockquote class="blockquote blockquote-info">
                        <h5>
                            <i class="fa fa-quote-left fa-sm "></i>
                            <strong class="text-success">
                                <?php
                                if ($_GET['sca'] != "") {
                                    $sta = CSA;
                                    $nnn = $descricao;
                                    $nn = explode(CSA, $nnn);
                                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                                    echo $n;
                                } else {
                                    echo $descricao;
                                }
                                ?>
                            </strong>
                            <i class="fa fa-quote-right fa-sm"></i>
                            <br>
                            <strong class="text-info" ><?php echo $data; ?>&nbsp;&nbsp;</strong>

                            <span class="badge badge-pill badge-info float-right"><strong><?php echo $id; ?></strong></span>
                            <footer class="blockquote-footer">
                                <?php
                                $us=fncgetusuario($prof);
                                echo $us['nome'];
                                echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                ?>
                            </footer>
                        </h5>
                    </blockquote>

                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>