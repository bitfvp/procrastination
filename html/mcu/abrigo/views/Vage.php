<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_24"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Agendamentos-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("includes/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->

            <?php

            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-1),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-1),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativiontem = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-0),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")-0),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativihoje = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+1),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+1),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativiamanha = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;

                    //////////////////////
                    $diasemana = array('Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-feira', 'Sexta-Feira', 'Sabado');

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+2),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+2),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $atividois = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                    //
                    $dt=explode(" ", $dataone);
                    $diasemana_numero = date('w', strtotime($dt[0]));
                    $diadois = $diasemana[$diasemana_numero];

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+3),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+3),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativitres = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                    //
                    $dt=explode(" ", $dataone);
                    $diasemana_numero = date('w', strtotime($dt[0]));
                    $diatres = $diasemana[$diasemana_numero];

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+4),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+4),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativiquatro = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                    //
                    $dt=explode(" ", $dataone);
                    $diasemana_numero = date('w', strtotime($dt[0]));
                    $diaquatro = $diasemana[$diasemana_numero];

                    // Captura os dados do cliente solicitado
                    $dataone=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+5),date("Y")))." 00:00:01";
                    $datatwo=date("Y-m-d", mktime(0,0,0,date("m"),(date("d")+5),date("Y")))." 23:59:59";
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (mcu_abrigo_reg.pessoa=? and mcu_abrigo_reg.data>=? and mcu_abrigo_reg.data<=? and tipo=1 ) \n"
                        . "ORDER BY mcu_abrigo_reg.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->bindParam(2, $dataone);
                    $consulta->bindParam(3, $datatwo);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativicinco = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                    //
                    $dt=explode(" ", $dataone);
                    $diasemana_numero = date('w', strtotime($dt[0]));
                    $diacinco = $diasemana[$diasemana_numero];


                }
            }
            ?>

            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Agendamentos
                </div>
                <div class="card-body">
                    <h6>
                        <h5>Ontem</h5>
                        <hr>
                        <?php
                        foreach ($ativiontem as $at) {
                            $cor="danger";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                    <?php
                                        echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                    ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5>Hoje</h5>
                        <hr>
                        <?php
                        foreach ($ativihoje as $at) {
                            $cor="success";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5>Amanhã</h5>
                        <hr>
                        <?php
                        foreach ($ativihoje as $at) {
                            $cor="info";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5><?php echo $diadois;?></h5>
                        <hr>
                        <?php
                        foreach ($atividois as $at) {
                            $cor="info";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5><?php echo $diatres;?></h5>
                        <hr>
                        <?php
                        foreach ($ativitres as $at) {
                            $cor="info";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5><?php echo $diaquatro;?></h5>
                        <hr>
                        <?php
                        foreach ($ativiquatro as $at) {
                            $cor="info";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                        <h5><?php echo $diacinco;?></h5>
                        <hr>
                        <?php
                        foreach ($ativicinco as $at) {
                            $cor="info";
                            $horinha=datahoraBanco2data($at['data']);
                            ?>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <?php
                                echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                ?>
                                <br>
                                <strong class="text-<?php echo $cor; ?>" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-<?php echo $cor; ?>"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                        }
                        ?>

                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>