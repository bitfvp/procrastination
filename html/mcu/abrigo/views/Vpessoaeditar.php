<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_24"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
                <label for="nome">NOME</label><input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
            </div>
            <script type="text/javascript">
                $(function(){
                    var campo = $("#nome");
                    campo.keyup(function(e){
                        e.preventDefault();
                        campo.val($(this).val().toLowerCase());
                    });
                });

            </script>
            <div class="col-md-3">
                <label for="nome_social">NOME SOCIAL</label>
                <input autocomplete="off" id="nome_social" type="text" class="form-control" name="nome_social" value="<?php echo $pessoa['nome_social']; ?>"/>
            </div>
            <div class="col-md-2">
                <label for="sexo">SEXO</label>
                <select name="sexo" id="sexo" class="form-control">// vamos criar a visualização de sexo
                    <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['sexo'];
                    } ?>">
                        <?php
                        if ($pessoa['sexo'] == 0) {
                            echo "Selecione...";
                        }
                        if ($pessoa['sexo'] == 1) {
                            echo "Feminino";
                        }
                        if ($pessoa['sexo'] == 2) {
                            echo "Masculino";
                        }
                        if ($pessoa['sexo'] == 3) {
                            echo "Indefinido";
                        }
                        ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Feminino</option>
                    <option value="2">Masculino</option>
                    <option value="3">Indefinido</option>
                </select>

            </div>
            <div class="col-md-3">
                <label for="nascimento">NASCIMENTO</label>
                <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="cpf">CPF</label>
                <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="rg">RG</label><input autocomplete="off" id="rg" type="text" class="form-control" name="rg"
                                                 value="<?php echo $pessoa['rg']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#rg').mask('00.000.000.000', {reverse: true});
                    });
                </script>
            </div>
            <div class="col-md-2">
                <label for="uf_rg">UF (RG)</label><input id="uf_rg" type="text" class="form-control" name="uf_rg" maxlength="2"
                                                         value="<?php echo $pessoa['uf_rg']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="uf_rg">NATURALIDADE</label><input id="naturalidade" type="text" class="form-control" name="naturalidade" maxlength="100"
                                                         value="<?php echo $pessoa['naturalidade']; ?>"/>
            </div>
        </div>


        <div class="row">
            <div class="col-md-5">
                <label for="pai">PAI</label>
                <input autocomplete="off" id="pai" type="text" class="form-control" name="pai" value="<?php echo $pessoa['pai']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="mae">MÃE</label>
                <input autocomplete="off" id="mae" type="text" class="form-control" name="mae" value="<?php echo $pessoa['mae']; ?>"/>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-5">
                <label for="endereco">ENDEREÇO</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
            </div>
            <div class="col-md-2">
                <label for="numero">NÚMERO</label>
                <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
            </div>

            <div class="col-md-4">
                <label for="bairro">Bairro</label>
                <input id="bairro" type="text" autocomplete="off" class="form-control" name="bairro" value="<?php echo $pessoa['bairro']; ?>"/>
            </div>

        </div>

        <div class="row">
            <div class="col-md-5">
                <label for="municipio">MUNICIPIO</label>
                <input autocomplete="off" id="municipio" type="text" class="form-control" name="municipio" value="<?php echo $pessoa['municipio']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="cep">CEP</label>
                <input autocomplete="off" id="cep" type="text" class="form-control" name="cep" value="<?php echo $pessoa['cep']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#cep').mask('00000-000', {reverse: false});
                });
            </script>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="referencia">REFERÊNCIA</label>
                <input autocomplete="off" id="referencia" type="text" class="form-control" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <label for="raca_cor">RAÇA/COR</label>
                <select name="raca_cor" id="raca_cor" class="form-control">
                    // vamos criar a visualização de cor-raça
                    <option selected="" value="<?php if ($pessoa['raca_cor'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['raca_cor'];
                    } ?>">
                        <?php
                        if (($pessoa['raca_cor'] == 0) or ($pessoa['raca_cor'] == "")) {
                            echo "Selecione...";
                        }
                        if ($pessoa['raca_cor'] == 1) {
                            echo "Branca";
                        }
                        if ($pessoa['raca_cor'] == 2) {
                            echo "Negra";
                        }
                        if ($pessoa['raca_cor'] == 3) {
                            echo "Amarela";
                        }
                        if ($pessoa['raca_cor'] == 4) {
                            echo "Parda";
                        }
                        if ($pessoa['raca_cor'] == 5) {
                            echo "Indigena";
                        } ?>
                    </option>
                    <option value="0">Selecione...</option>
                    <option value="1">Branca</option>
                    <option value="2">Negra</option>
                    <option value="3">Amarela</option>
                    <option value="4">Parda(a)</option>
                    <option value="5">Indigena</option>
                </select>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <label for="deficiencia">POSSUI DEFICIÊNCIA</label>
                <select name="deficiencia" id="deficiencia" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php if ($pessoa['deficiencia'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $pessoa['deficiencia'];
                    } ?>">
                        <?php
                        if ($pessoa['deficiencia'] == 0) {
                            echo "Não";
                        }
                        if ($pessoa['deficiencia'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>

            </div>

            <div class="col-md-6">
                <label for="deficiencia_desc">DESCRIÇÃO DA DEFICIÊNCIA</label>
                <input autocomplete="off" id="deficiencia_desc" type="text" class="form-control" name="deficiencia_desc" value="<?php echo $pessoa['deficiencia_desc']; ?>"/>
            </div>
        </div>
    </form>
</main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>