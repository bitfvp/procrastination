<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_24"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}


$page="Registros-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("includes/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Registro
                    </div>
                    <div class="card-body">
                        <form action="index.php?pg=Vreg&aca=newreg&id=<?php echo $_GET['id']; ?>" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="titulo">Titulo:</label>
                                    <select name="titulo" id="titulo" class="form-control" required="true">
                                        <option selected="" value="">Selecione...</option>
                                        <?php
                                        foreach (fnc_reglist() as $ativ) {
                                            ?>
                                            <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['titulo']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="restricao">Restrição:</label>
                                    <select name="restricao" id="restricao" class="form-control">
                                        <option selected="" value="0">Aberto</option>
                                        <option value="1">Confidencial</option>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="tipo">Tipo:</label>
                                    <input name="tipo" type="checkbox" data-toggle="toggle" data-on="<i class='fa fa-clock'></i>AGENDADA" data-off="HABITUAL" data-onstyle="danger" data-offstyle="info">
                                </div>

                                <div class="col-md-6">
                                    <label for="data">Data:</label>
                                    <input id="data" type="date" class="form-control input-sm" name="data" required value="<?php echo date("Y-m-d"); ?>"/>
                                </div>

                                <div class="col-md-6">
                                    <label for="hora">Hora:</label>
                                    <input id="hora" type="time" class="form-control input-sm" name="hora" value=""/>
                                </div>

                                <div class="col-md-12">
                                    <label for="descricao">Descrição:</label>
                                    <textarea id="descricao" onkeyup="limite_textarea(this.value,3000,descricao,'cont')" maxlength="3000" class="form-control" rows="9" name="descricao"></textarea>
                                    <span id="cont">3000</span>/3000
                                </div>

                                <div class="col-md-12">
                                    <div class="custom-file">
                                        <input id="arquivo" type="file" class="custom-file-input" name="arquivo[]" value="" multiple/>
                                        <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                                    </div>
                                </div>

                                <div class="col-md-12 mt-3">
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                                </div>
                            </div>
                        </form>
                    </div>
<!--                    <div class="card-footer text-warning text-center">-->
<!--                        Sempre verificar o endereço antes de emitir o benefício.-->
<!--                    </div>-->
                </div>

                <?php

            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * \n"
                        . "FROM mcu_abrigo_reg \n"
                        . "WHERE (((mcu_abrigo_reg.pessoa)=?))\n"
                        . "ORDER BY mcu_abrigo_reg.data_ts DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativi = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>

            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vatprint&id=<?php echo $_GET['id'];?>" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão do prontuário</span>
                    </a>
                    <h6>
                        <?php
                        foreach ($ativi as $at) {

                            if ($at['tipo']==0){
                                $cor="info";
                                $horinha=dataRetiraHora($at['data']);
                            }
                            if ($at['tipo']==1){
                                $cor="warning";
                                $horinha=datahoraBanco2data($at['data']);
                            }
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i>
                                    <?php
                                        echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                    ?>
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                <strong class="text-info" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo $horinha; ?>&nbsp;&nbsp;</strong><br>
                                Tipo de atividade:<strong class="text-info"><?php echo fncget_reglist($at['titulo'])['titulo']; ?>&nbsp&nbsp</strong>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";

                                    if (($at['profissional'] == $_SESSION['id']) and (Expiradatahora($at['data_ts'], 1)==1)) {
                                        ?>
                                        <div class="dropdown show">
                                            <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Apagar
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="#">Não</a>
                                                <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vat&id={$_GET['id']}&aca=excluiratividade&id_at={$at['id']}";?>">Apagar</a>
                                            </div>
                                        </div>
                                        <h6>Não é possivel apagar após 24h</h6>
                                        <?php

                                    }
                                    ?>
                                </footer>
                            </blockquote>
                            <?php
                            if ($at['id'] != 0) {
                                $files = glob("../../dados/mcu/abrigo/reg/" . $at['id'] . "/*.*");
                                for ($i = 0; $i < count($files); $i++) {
                                    $num = $files[$i];
                                    $extencao = explode(".", $num);
                                    //ultima posicao do array
                                    $ultimo = end($extencao);
                                    switch ($ultimo) {
                                        case "docx":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "doc":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xls":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";

                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xlsx":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "pdf":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        default:
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank' >";
                                            echo "<img src=" . $num . " alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;
                                    }
                                }
                            }//fim de foto
                        }
                        ?>
                    </h6>
                </div>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>