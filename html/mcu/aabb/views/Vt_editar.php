<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_69"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
//        }else{
//            if ($allow["expecifica"]!=1){
//                header("Location: {$env->env_url_mod}");
//                exit();
//            }else{
//                //ira abrir
//            }
        }
    }
}

$page="Turma Editar-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="turmaupdate";
    $turma=fncgetturma($_GET['id']);
}else{
    $a="turmainsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form action="<?php echo "index.php?pg=Vt_editar&aca={$a}"; ?>" method="post" class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" value="SALVAR"  class="btn btn-success btn-block col-md-6">
                <hr>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <label for="ano">Ano</label>
                <input name="ano" id="ano" type="number" min="2022" max="2030" class="form-control" value="<?php if (isset($turma['ano'])){echo $turma['ano'];} ?>">
            </div>
            <div class="col-md-6">
                <label>Status</label>
                <select name="status" required="true" class="form-control">
                    <option selected="" value="<?php if (isset($turma['status'])){echo $turma['status'];} ?>">
                        <?php
                        if (isset($turma['status'])){
                            if ($turma['status']==1){
                                echo "ATIVO";
                            }else{
                                echo "DESATIVADO";
                            }
                        }
                        ?>
                    </option>
                    <option value="1">ATIVO</option>
                    <option value="0">DESATIVADO</option>
                </select>
            </div>
            <div class="col-md-12">
                <input name="id"  id="id" type="hidden" value="<?php if (isset($turma['id'])){echo $turma['id'];} ?>"/>
                <label for="descricao">Descrição da turma</label>
                <label for="descricao">Descrição:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,100,descricao,'cont')" maxlength="100" class="form-control" rows="2" name="descricao"><?php if (isset($turma['descricao'])){echo $turma['descricao'];} ?></textarea>
                <span id="cont">100</span>/100
            </div>
        </div>

    </form>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>