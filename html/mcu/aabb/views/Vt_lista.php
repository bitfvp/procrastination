<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_69"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
//        }else{
//            if ($allow["expecifica"]!=1){
//                header("Location: {$env->env_url_mod}");
//                exit();
//            }else{
//                //ira abrir
//            }
        }
    }
}

$page="Turma Lista-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    $scb = $_GET['scb'];
    $sql = "select * from mcu_aabb_turmas WHERE descricao LIKE '%$sca%' or ano LIKE '%$sca%'";
}else {

    $sql = "select * from mcu_aabb_turmas where id>0 ";
}

$total_reg = "50"; // número de registros por página

$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}

$inicio = $pc - 1;
$inicio = $inicio * $total_reg;

try{
    $sql2= "ORDER BY ano LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY ano LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Turmas</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vt_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por turma ou ano..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vt_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVA TURMA
    </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-striped table-hover table-sm text-uppercase">
        <thead>
        <tr>
            <th scope="row" colspan="2">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vt_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vt_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th>Descrição</th>
            <th>Ano</th>
            <th>Status</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="2">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vt_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vt_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Turma(s) listada(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
        foreach ($limite as $dados){
            $id = $dados["id"];
            $descricao = strtoupper($dados["descricao"]);
            $ano = $dados["ano"];
            if ($dados["status"]==1){
                $status = "<strong class='text-success'>Turma Ativa</strong>";
            }else{
                $status = "<strong class='text-danger'>Turma Desativada</strong>";
            }
            ?>
            <tr>
                <td scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vt&id=<?php echo $id;?>" title="Ver turma">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $descricao;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $descricao;
                        }
                        ?>
                    </a>
                </td>
                <td>
                    <?php
                    if($_GET['sca']!="") {
                        $sta = CSA;
                        $nnn = $ano;
                        $nn = explode(CSA, $nnn);
                        $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                        echo $n;
                    }else{
                        echo $ano;
                    }
                    ?>
                </td>

                <td><?php echo $status;?></td>

                <td>
                    <a href="index.php?pg=Vt_editar&id=<?php echo $id; ?>" title="Edite os dados desse Contato">
                        Alterar
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>