<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_69"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}
$page="Matrículas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>
        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamento de matrículas
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vm&aca=matriculainsert&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="row text-uppercase">

                            <div class="form-group col-md-12">
                                <label for="turma">TURMA:</label>
                                <select name="turma" id="turma" class="form-control text-uppercase" required>
                                    <option selected="" value="">Selecione...</option>
                                    <?php
                                    foreach(fncturmalist2() as $ps){
                                        ?>
                                        <option value="<?php echo $ps['id']; ?>"><?php echo $ps['descricao']; ?>--<?php echo $ps['ano']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label for="escola">ESCOLA:</label>
                                <input autocomplete="off" id="escola" type="text" class="form-control" name="escola" value="" required/>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="serie">SÉRIE:</label>
                                <select name="serie" id="serie" class="form-control text-uppercase" required>
                                    <option selected="" value="">Selecione...</option>
                                    <option value="1">1° Serie</option>
                                    <option value="2">2° Serie</option>
                                    <option value="3">3° Serie</option>
                                    <option value="4">4° Serie</option>
                                    <option value="5">5° Serie</option>
                                    <option value="6">6° Serie</option>
                                    <option value="7">7° Serie</option>
                                    <option value="8">8° Serie</option>
                                    <option value="9">9° Serie</option>
                                    <option value="10">1° Ano</option>
                                    <option value="11">2° Ano</option>
                                    <option value="12">3° Ano</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="turno">TURNO:</label>
                                <select name="turno" id="turno" class="form-control text-uppercase" required>
                                    <option selected="" value="">Selecione...</option>
                                    <option value="1">Matutino</option>
                                    <option value="2">Vespertino</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="camisa">CAMISA/BLUSA:</label>
                                <input autocomplete="off" id="camisa" type="number" class="form-control" name="camisa" value="" min="6" max="42" required/>
                            </div>

                            <div class="col-md-4">
                                <label for="calca">CALÇA:</label>
                                <input autocomplete="off" id="calca" type="number" class="form-control" name="calca" value="" min="28" max="42" required/>
                            </div>

                            <div class="col-md-4">
                                <label for="calcado">CALÇADO:</label>
                                <input autocomplete="off" id="calcado" type="number" class="form-control" name="calcado" value="" min="28" max="42" required/>
                            </div>

                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            // Recebe
            if (isset($_GET['id'])) {
                //existe um id e se ele é numérico
                if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
                    // Captura os dados do cliente solicitado
                    if ($allow["admin"]!=1){
                        $sql = "SELECT * \n"
                        . "FROM mcu_aabb_matriculas \n"
                        . "WHERE (mcu_aabb_matriculas.pessoa=? ) \n"
                        . "ORDER BY mcu_aabb_matriculas.data_ts DESC";
                    }else{
                        $sql = "SELECT * \n"
                            . "FROM mcu_aabb_matriculas \n"
                            . "WHERE (mcu_aabb_matriculas.pessoa=? ) \n"
                            . "ORDER BY mcu_aabb_matriculas.data_ts DESC";
                    }
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $atividades = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico de matrículas
                </div>
                <div class="card-body">
                    <a href="#" target="">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão de histórico</span>
                    </a>
                        <?php
                        foreach ($atividades as $at) {
                            $status="";
                            if ($at['status']==0){
                                $status="bg-danger-light ";
                            }
                            $turma=fncgetturma($at['turma']);
                            $tempdata_ts=dataRetiraHora($at['data_ts']);
                            $tempdata_ts=data2banco($tempdata_ts);
                            $temp_dias=diasDatas($pessoa['nascimento'],$tempdata_ts);
                            $temp_anos = $temp_dias / 365;

                            ?>
                            <h6>
                                <hr>
                                <blockquote class="blockquote blockquote-success <?php echo $status; ?> text-uppercase">
                                    Turma: <strong class="text-info"><?php echo $turma['descricao']." - ".$turma['ano']; ?></strong><br>
                                    <strong class="text-info" title=""><?php echo floor($temp_anos)." anos em: ". dataRetiraHora($at['data_ts']); ?></strong><br>
                                    Escola: <strong class="text-info"><?php echo $at['escola']; ?>&nbsp&nbsp</strong><br>
                                    Serie: <strong class="text-info">
                                        <?php
                                        switch ($at['serie']){
                                            case 1:
                                                $serie="1° Serie";
                                                break;
                                            case 2:
                                                $serie="2° Serie";
                                                break;
                                            case 3:
                                                $serie="3° Serie";
                                                break;
                                            case 4:
                                                $serie="4° Serie";
                                                break;
                                            case 5:
                                                $serie="5° Serie";
                                                break;
                                            case 6:
                                                $serie="6° Serie";
                                                break;
                                            case 7:
                                                $serie="7° Serie";
                                                break;
                                            case 8:
                                                $serie="8° Serie";
                                                break;
                                            case 9:
                                                $serie="9° Serie";
                                                break;
                                            case 10:
                                                $serie="1° Ano";
                                                break;
                                            case 11:
                                                $serie="2° Ano";
                                                break;
                                            case 12:
                                                $serie="3° Ano";
                                                break;
                                            default:
                                                $serie="xxx";
                                                break;
                                        }
                                        echo $serie;
                                        ?>
                                    </strong>
                                    Turno: <strong class="text-info">
                                        <?php
                                        switch ($at['turno']){
                                            case 1:
                                                $turno="Manhã";
                                                break;
                                            case 2:
                                                $turno="Tarde";
                                                break;
                                            default:
                                                $turno="xxx";
                                                break;
                                        }
                                        echo $turno;
                                        ?>
                                    </strong><br>
                                    Camisa/Blusa: <strong class="text-info"><?php echo $at['camisa']; ?>&nbsp&nbsp</strong>
                                    Calça: <strong class="text-info"><?php echo $at['calca']; ?>&nbsp&nbsp</strong>
                                    Calçado: <strong class="text-info"><?php echo $at['calcado']; ?>&nbsp&nbsp</strong><br>
                                    <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                    <footer class="blockquote-footer">
                                        <?php
                                        $us=fncgetusuario($at['usuario']);
                                        echo $us['nome'];
//                                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                        if ($at['status']==1 and $turma['status']==1) {
                                            ?>
                                            <div class="dropdown show">
                                                <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Apagar
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a class="dropdown-item" href="#">Não</a>
                                                    <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vm&id={$_GET['id']}&aca=matriculadelete&tabela_id={$at['id']}";?>">Apagar</a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        if ($at['status']==0){
                                            $us=fncgetusuario($at['delete_prof']);
                                            echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                                        }
                                        ?>
                                    </footer>
                                </blockquote>
                            </h6>
                        <?php
                        }
                        ?>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>