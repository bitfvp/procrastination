<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_69"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
//        }else{
//            if ($allow["expecifica"]!=1){
//                header("Location: {$env->env_url_mod}");
//                exit();
//            }else{
//                //ira abrir
//            }
        }
    }
}

$page="Turma-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");


if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $turma=fncgetturma($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container text-uppercase">

    <h3><?php echo $turma['descricao'];?></h3>
    Ano: <strong><?php echo $turma['ano'];?></strong><br>
    Cadastrada em: <strong><?php echo dataRetiraHora($turma['data_ts']);?></strong><br>
    Cadastrada por: <strong><?php echo fncgetusuario($turma['usuario'])['nome'];?></strong><br><br>
    Status:<strong>
        <?php
        if ($turma['status']==1){
            echo "<strong class='green-text'>Turma Ativa</strong>";
        }else{
            echo "<strong class='red-text'>Turma Desativada</strong>";
        }
        ?>
    </strong>

    <hr>
    <?php
    $sql = "SELECT * FROM mcu_aabb_matriculas "
        ."INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_aabb_matriculas.pessoa "
        ."WHERE	mcu_aabb_matriculas.turma = ? and mcu_aabb_matriculas.status=1 "
        ."ORDER BY "
        ."YEAR(mcu_pessoas.nascimento) DESC, "
        ."mcu_pessoas.nome ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $turma['id']);
    $consulta->execute();
    $inscritos = $consulta->fetchall();
    $inscritos_count = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    ?>

    <h6><?php echo $inscritos_count." inscrito(s) nessa turma";?></h6>
    <div class="col-md-12">
        <table class="table table-sm text-uppercase">
            <thead>
            <tr>
                <th>NOME</th>
                <th>CPF</th>
                <th>NASCIMENTO</th>
                <th>INSCRIÇÃO</th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach ($inscritos as $item){
                $pessoa=fncgetpessoa($item['pessoa']);
                $idade = Calculo_Idade($pessoa["nascimento"]);

                $tempdata_ts=dataRetiraHora($item['data_ts']);
                $tempdata_ts=data2banco($tempdata_ts);
                $temp_dias=diasDatas($pessoa['nascimento'],$tempdata_ts);
                $temp_anos = $temp_dias / 365;

                echo "<tr>";

                echo "<td>";
                echo $pessoa['nome'];
                echo "</td>";

                echo "<td>";
                echo mask($pessoa['cpf'],'###.###.###-##');
                echo "</td>";

                echo "<td>";
                echo dataBanco2data($pessoa['nascimento']);
                echo "</td>";

                echo "<td>";
                echo floor($temp_anos)." anos em: ". dataRetiraHora($item['data_ts']);
                echo "</td>";

                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
    </div>

</main>
</body>
</html>