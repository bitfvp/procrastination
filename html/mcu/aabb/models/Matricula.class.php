<?php
class Matricula{
    public function fncmatriculainsert($pessoa, $turma, $escola, $serie, $turno, $camisa, $calca, $calcado, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO mcu_aabb_matriculas ";
            $sql.="(id, pessoa, turma, escola, serie, turno, camisa, calca, calcado, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :pessoa, :turma, :escola, :serie, :turno, :camisa, :calca, :calcado, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":turma", $turma);
            $insere->bindValue(":escola", $escola);
            $insere->bindValue(":serie", $serie);
            $insere->bindValue(":turno", $turno);
            $insere->bindValue(":camisa", $camisa);
            $insere->bindValue(":calca", $calca);
            $insere->bindValue(":calcado", $calcado);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vm&id={$pessoa}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncmatriculaupdate($id, $pessoa, $turma, $escola, $serie, $turno, $camisa, $calca, $calcado ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="mcu_aabb_matriculas ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE mcu_aabb_matriculas ";
                $sql.="SET ";
                $sql .= "pessoa=:pessoa,
turma=:turma,
escola=:escola,
serie=:serie,
turno=:turno,
camisa=:camisa,
calca=:calca,
calcado=:calcado
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":pessoa", $pessoa);
                $atualiza->bindValue(":turma", $turma);
                $atualiza->bindValue(":escola", $escola);
                $atualiza->bindValue(":serie", $serie);
                $atualiza->bindValue(":turno", $turno);
                $atualiza->bindValue(":camisa", $camisa);
                $atualiza->bindValue(":calca", $calca);
                $atualiza->bindValue(":calcado", $calcado);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncmatriculadelete($tabela_id,$usuario_off,$pessoa_id ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="mcu_aabb_matriculas ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE mcu_aabb_matriculas ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vm&id={$pessoa_id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe