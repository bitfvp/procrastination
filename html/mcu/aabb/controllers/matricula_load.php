<?php
//gerado pelo geracode
function fncmatriculalist(){
    $sql = "SELECT * FROM mcu_aabb_matriculas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $matriculalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $matriculalista;
}

function fncgetmatricula($id){
    $sql = "SELECT * FROM mcu_aabb_matriculas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getmcu_aabb_matriculas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getmcu_aabb_matriculas;
}
?>