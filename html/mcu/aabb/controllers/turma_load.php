<?php
//gerado pelo geracode
function fncturmalist(){
    $sql = "SELECT * FROM mcu_aabb_turmas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $turmalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $turmalista;
}

function fncturmalist2(){
    $sql = "SELECT * FROM mcu_aabb_turmas where status=1 ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $turmalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $turmalista;
}

function fncgetturma($id){
    $sql = "SELECT * FROM mcu_aabb_turmas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getmcu_aabb_turmas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getmcu_aabb_turmas;
}
?>