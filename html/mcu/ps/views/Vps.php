<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Processo seletivo-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $ps=fncgetps($_GET['id']);
}else{
    echo "HOUVE ALGUM ERRO";
}
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do processo seletivo
                </div>
                <div class="card-body">
                <blockquote class="blockquote blockquote-info">
                    <header>CÓDIGO: <strong class="text-info"><?php echo $ps['cod_ps']; ?>&nbsp;&nbsp;</strong></header>
                    <h5>
                        PROCESSO SELETIVO: <strong class="text-info"><?php echo $ps['nome_ps']; ?></strong>
                    </h5>
                    <h5>
                        SECRETARIA: <strong class="text-info"><?php echo $ps['secretaria']; ?></strong>
                    </h5>
                    <h5>
                        AÇÃO: <strong class="text-info"><?php echo $ps['acao']; ?></strong>
                    </h5>
                    <h5>
                        DATA: <strong class="text-info"><?php echo dataRetiraHora($ps['data_ts']); ?></strong>
                    </h5>
                    <h5>
                        STATUS: <strong class="text-info">
                            <?php
                            if ($ps['ativo']==1){
                                echo "<i class='text-success'>ATIVO</i>";
                            }else{
                                echo "<i class='text-danger'>INATIVO</i>";
                            }
                            ?>
                        </strong>
                    </h5>
                </blockquote>
                    <a class="btn btn-success btn-block" href="index.php?pg=Vps_editar&id=<?php echo $ps['id']; ?>" title="Edite os dados dessa pessoa">
                        EDITAR
                    </a>
                </div>
            <!-- fim da col md 4 -->
            </div>
        </div>

        <?php
        if(isset($_GET['id'])) {
            $id_ps =$_GET['id'];
            $sql = "SELECT * \n"
                . "FROM mcu_ps_inscricao \n"
                . "WHERE ((id_ps=?))\n"
                . "ORDER BY cod_cadastro DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $id_ps);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $hist = $consulta->fetchAll();
                $sql=null;
                $consulta=null;

        }
        ?>
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Inscrições prontas
                </div>
                <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>DATA</th>
                            <th>NÚMERO DE CADASTRO</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($hist as $insc){
                            echo "<tr>";
                            echo "<td>";
                            echo "<a href='index.php?pg=Vinsc&id={$insc['id_pessoa']}'>";
                            echo fncgetpessoa($insc['id_pessoa'])['nome'];
                            echo "</a>";
                            echo "</td>";
                            echo "<td>". mask(fncgetpessoa($insc['id_pessoa'])['cpf'],'###.###.###-##')."</td>";
                            echo "<td>".dataRetiraHora($insc['data_ts'])."</td>";
                            echo "<td>".$insc['cod_cadastro']."</td>";
                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>