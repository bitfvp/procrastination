<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Busca-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <?php
    if (isset($_GET['sca'])){
        $sca = $_GET['sca'];
        $scnascimento = $_GET['nascimento'];
        $sccpf = $_GET['cpf'];
        $scrg = $_GET['rg'];
        $scendereco = $_GET['endereco'];
        $scnumero = $_GET['numero'];
        $scbairro = $_GET['bairro'];
        if ($scbairro==0){
            $scbairro="";
        }
        $scgenero = $_GET['genero'];
        if ($scgenero==0){
            $scgenero="";
        }
        //consulta se ha busca
        $sql_where = "WHERE nome LIKE '%$sca%' AND nascimento LIKE '%$scnascimento%' AND cpf LIKE '%$sccpf%' AND rg LIKE '%$scrg%' AND endereco LIKE '%$scendereco%' AND numero LIKE '%$scnumero%' AND bairro LIKE '%$scbairro%' AND sexo LIKE '%$scgenero%' ";
        $sql = "select * from mcu_pessoas ";
    }else {
//consulta se nao ha busca
        $sql_where = "WHERE id=0 ";
        $sql = "select * from mcu_pessoas ";
    }
    // total de registros a serem exibidos por página
    $total_reg = "50"; // número de registros por página
    //Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
    $pgn=$_GET['pgn'];
    if (!$pgn) {
        $pc = "1";
    } else {
        $pc = $pgn;
    }
    //Vamos determinar o valor inicial das buscas limitadas
    $inicio = $pc - 1;
    $inicio = $inicio * $total_reg;
    //Vamos selecionar os dados e exibir a paginação
    //limite
    try{
        $sql_orderby= "ORDER BY nome LIMIT $inicio,$total_reg";
        global $pdo;
        $limite=$pdo->prepare($sql.$sql_where.$sql_orderby);
        $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    //todos
    try{
        $sql = "select COUNT('id') from mcu_pessoas ".$sql_where;
        global $pdo;
        $todos=$pdo->prepare($sql);
        $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $tr=$todos->fetch();// verifica o número total de registros
    $tr=$tr[0];
    $tp = $tr / $total_reg; // verifica o número total de páginas
include_once ("includes/buscalista.php");
?>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>