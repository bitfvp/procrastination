<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Inscrição".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamento de inscrições
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vinsc&aca=newinsc&id=<?php echo $_GET['id']; ?>" method="post"  enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="id_ps">Processo seletivo:</label>
                                <select name="id_ps" id="id_ps" class="form-control" required>
                                    <option selected="" value="0">Selecione...</option>
                                    <?php
                                    foreach(fncpslist() as $ps){
                                        ?>
                                        <option value="<?php echo $ps['id']; ?>"><?php echo $ps['cod_ps']; ?>--<?php echo $ps['acao']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="referencia">Referência:</label>
                                <textarea id="referencia" onkeyup="limite_textarea(this.value,250,referencia,'cont')" maxlength="250" class="form-control" rows="4" name="referencia"></textarea>
                                <span id="cont">250</span>/250
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php
            if(isset($_GET['id'])) {
                $at_idpessoa =$_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    $sql = "SELECT * \n"
                        . "FROM mcu_ps_inscricao \n"
                        . "WHERE ((id_pessoa=?))\n"
                        . "ORDER BY data_ts DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $hist = $consulta->fetchAll();
                    $sql=null;
                    $consulta=null;
                }
            }
            ?>
            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <h6>
                        <?php
                        foreach($hist as $at){
                            switch (fncgetps($at['id_ps'])['cabecalho']){
                                case 1:
                                    $cabe="";
                                    break;
                                case 2:
                                    $cabe="cmddca";
                                    break;
                                default:
                                    break;
                            }
                            ?>
                            <hr>
                            <a href="index.php?pg=Vinsc_comprovante<?php echo $cabe; ?>&id=<?php echo $at['id']; ?>" target="_blank" onclick="">
                                <span class="fa fa-print text-warning mb-2" aria-hidden="true"> Impressão do comprovante</span>
                            </a>
                            <blockquote class="blockquote blockquote-info">
                                <strong class="text-success"><?php echo fncgetps($at['id_ps'])['nome_ps']; ?></strong><br>
                                <strong class="text-success"><?php echo fncgetps($at['id_ps'])['cod_ps']; ?></strong><br>

                                Inscrição feita em:<strong class="text-info"><?php echo dataRetiraHora($at['data_ts']); ?>&nbsp;&nbsp;</strong><br>

                                Secretaria:<strong class="text-info"><?php echo fncgetps($at['id_ps'])['secretaria']; ?></strong>
                                <br>
                                Ação:<strong class="text-info"><?php echo fncgetps($at['id_ps'])['acao']; ?></strong>
                                <br>

                                Referência:<strong class="text-info"><?php echo $at['referencia']; ?></strong>
                                <br>


                                <span class="badge badge-pill badge-warning float-right">
                                    <strong title="<?php echo $at['id']; ?>"><?php echo $at['cod_cadastro']; ?></strong>
                                </span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['responsavel']);
                                    echo $us['nome'];
                                    ?>
                                </footer>
                            </blockquote>
                        <?php } ?>
                    </h6>
                    </p>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once ("includes/sectionmenulateral.php");?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>