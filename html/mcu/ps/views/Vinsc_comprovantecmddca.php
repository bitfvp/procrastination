<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Impressão de Comprovante-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
// Recebe

$sql = "SELECT * \n"
    . "FROM mcu_ps_inscricao \n"
    . "WHERE id=? ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ps = $consulta->fetch();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/CMDDCA.png" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h4 class="text-left ml-4 my-0"><strong>CONSELHO MUNICIPAL DE DEFESA DOS DIREITOS DA CRIANÇA E DO ADOLESCENTE - CMDDCA</strong></h4>
            <h6 class="text-left ml-4 my-0">Lei 3.466/2015 Alterada pelas Leis 3.924/2019 e 3.88/2018</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Rua Monsenhor Gonzalez, 498 - Manhuaçu - Minas Gerais CEP 36900-000 (33)3332-3800</h6>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['nome_ps'];?> <?php echo fncgetps($ps['id_ps'])['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['acao'];?></strong></h4>
            <h5 class="my-0"><strong><?php echo $ps['referencia'];?></strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo $ps['cod_cadastro'];?></h4>
            <h3> NOME COMPLETO: <strong><?php echo strtoupper(fncgetpessoa($ps['id_pessoa'])['nome']);?></strong></h3>
            <h4 class="float-right"> TELEFONE: <?php echo fncgetpessoa($ps['id_pessoa'])['telefone'];?></h4>
            <h4> CPF: <?php echo mask(fncgetpessoa($ps['id_pessoa'])['cpf'],'###.###.###-##');?></h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($ps['responsavel'])['nome']);?></h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDADO:______________________________________________________</h6>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>

<br>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/CMDDCA.png" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h4 class="text-left ml-4 my-0"><strong>CONSELHO MUNICIPAL DE DEFESA DOS DIREITOS DA CRIANÇA E DO ADOLESCENTE - CMDDCA</strong></h4>
            <h6 class="text-left ml-4 my-0">Lei 3.466/2015 Alterada pelas Leis 3.924/2019 e 3.88/2018</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Rua Monsenhor Gonzalez, 498 - Manhuaçu - Minas Gerais CEP 36900-000 (33)3332-3800</h6>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['nome_ps'];?> <?php echo fncgetps($ps['id_ps'])['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['acao'];?></strong></h4>
            <h5 class="my-0"><strong><?php echo $ps['referencia'];?></strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo $ps['cod_cadastro'];?></h4>
            <h3> NOME COMPLETO: <strong><?php echo strtoupper(fncgetpessoa($ps['id_pessoa'])['nome']);?></strong></h3>
            <h4 class="float-right"> TELEFONE: <?php echo fncgetpessoa($ps['id_pessoa'])['telefone'];?></h4>
            <h4> CPF: <?php echo mask(fncgetpessoa($ps['id_pessoa'])['cpf'],'###.###.###-##');?></h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($ps['responsavel'])['nome']);?></h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDADO:______________________________________________________</h6>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>



<main class="container border">
    <div class="row">
        <div class="col-12 text-center mt-3 ">
            <h4 class="text-left mx-3"><strong>DECISÃO DA COMISSÃO ORGANIZADORA</strong></h4>
            <h6 class="text-left mx-3"><strong>(&nbsp;&nbsp;&nbsp;)</strong>DEFERIDA &nbsp;&nbsp;<strong>(&nbsp;&nbsp;&nbsp;)</strong>INDEFERIDA</h6>
            <h6 class="text-left mx-3">MOTIVOS DO INDEFERIMENTO:
                <style>.hrespacado{ margin-top: 35px; border-color: #0a0b0d; width: auto;</style>
                <hr class="hrespacado">
                <hr class="hrespacado">
                <hr class="hrespacado">
            </h6>
        </div>
    </div>


    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-left ml-3">Presidente da Comissão Organizadora:______________________________________________________</h6>
            <h6 class="text-left ml-3">Manhuaçu, ____/____/______</h6>
        </div>
    </div>
</main>



</body>
</html>