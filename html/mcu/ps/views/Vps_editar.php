<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar PS-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pssave";
    $ps=fncgetps($_GET['id']);
}else{
    $a="psnew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vps_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de Processo Seletivo</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ps['id']; ?>"/>
                <label for="cod_ps">Código do processo seletivo:</label>
                <input autocomplete="off" id="cod_ps" placeholder="codigo" type="text" class="form-control" name="cod_ps" value="<?php echo $ps['cod_ps']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cod_ps').mask('000/0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-12">
                <label for="nome_ps">Denominação:</label>
                <textarea id="nome_ps" onkeyup="limite_textarea(this.value,1000,nome_ps,'cont1')" maxlength="1000" class="form-control" rows="4" name="nome_ps"><?php echo $ps['nome_ps']; ?></textarea>
                <span id="cont1">1000</span>/1000
            </div>
            <div class="col-md-12">
                <label for="secretaria">Secretaria:</label>
                <textarea id="secretaria" onkeyup="limite_textarea(this.value,1000,secretaria,'cont2')" maxlength="1000" class="form-control" rows="4" name="secretaria"><?php echo $ps['secretaria']; ?></textarea>
                <span id="cont2">1000</span>/1000
            </div>

            <div class="col-md-12">
                <label for="acao">Ação:</label>
                <textarea id="acao" onkeyup="limite_textarea(this.value,1000,acao,'cont3')" maxlength="1000" class="form-control" rows="4" name="acao"><?php echo $ps['acao']; ?></textarea>
                <span id="cont3">1000</span>/1000
            </div>




            <div class="col-md-6">
                <label for="ativo">Ativo:</label>
                <select name="ativo" id="ativo" class="form-control">
                    <option selected="" value="<?php if ($ps['ativo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $ps['ativo'];
                    } ?>">
                        <?php
                        if ($ps['ativo'] == 0) {
                            echo "Não";
                        }
                        if ($ps['ativo'] ==1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-6">
                <label for="cabecalho">Cabeçalho:</label>
                <select name="cabecalho" id="cabecalho" class="form-control">
                    <option selected="" value="<?php if ($ps['cabecalho'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $ps['cabecalho'];
                    } ?>">
                        <?php
                        if ($ps['cabecalho'] == 1) {
                            echo "Prefeitura";
                        }
                        if ($ps['cabecalho'] ==2) {
                            echo "CMDDCA";
                        } ?>
                    </option>
                    <option value="1">Prefeitura</option>
                    <option value="2">CMDDCA</option>
                </select>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success btn-lg btn-block my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>