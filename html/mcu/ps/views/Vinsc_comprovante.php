<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_70"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}


$page="Impressão de Comprovante-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
// Recebe

$sql = "SELECT * \n"
    . "FROM mcu_ps_inscricao \n"
    . "WHERE id=? ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ps = $consulta->fetch();
    $sql=null;
    $consulta=null;
?>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['nome_ps'];?> <?php echo fncgetps($ps['id_ps'])['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['acao'];?></strong></h4>
            <h5 class="my-0"><strong><?php echo $ps['referencia'];?></strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo $ps['cod_cadastro'];?></h4>
            <h3> NOME COMPLETO: <strong><?php echo strtoupper(fncgetpessoa($ps['id_pessoa'])['nome']);?></strong></h3>
            <h4 class="float-right"> TELEFONE: <?php echo fncgetpessoa($ps['id_pessoa'])['telefone'];?></h4>
            <h4> CPF: <?php echo mask(fncgetpessoa($ps['id_pessoa'])['cpf'],'###.###.###-##');?></h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($ps['responsavel'])['nome']);?></h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDATO:______________________________________________________</h6>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>

<br>

<main class="container border">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h2 class="text-left ml-4 my-0"><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h2>
            <h6 class="text-left ml-4 my-0">Lei Provincial n°. 2407 de 5/XI/1877 - Área: 628.43 km² - Altitude: 612 metros</h6>
            <h6 class="mb-4 text-left ml-4 my-0">Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</h6>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['secretaria'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['nome_ps'];?> <?php echo fncgetps($ps['id_ps'])['cod_ps'];?></strong></h4>
            <h4 class="my-0"><strong><?php echo fncgetps($ps['id_ps'])['acao'];?></strong></h4>
            <h5 class="my-0"><strong><?php echo $ps['referencia'];?></strong></h5>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-8 offset-2">
            <h4 class="float-right"> DATA DO PROTOCOLO: <?php echo dataRetiraHora($ps['data_ts']);?></h4>
            <h4> CÓDIGO: <?php echo $ps['cod_cadastro'];?></h4>
            <h3> NOME COMPLETO: <strong><?php echo strtoupper(fncgetpessoa($ps['id_pessoa'])['nome']);?></strong></h3>
            <h4 class="float-right"> TELEFONE: <?php echo fncgetpessoa($ps['id_pessoa'])['telefone'];?></h4>
            <h4> CPF: <?php echo mask(fncgetpessoa($ps['id_pessoa'])['cpf'],'###.###.###-##');?></h4>
            <h4> RESPONSÁVEL: <?php echo strtoupper(fncgetusuario($ps['responsavel'])['nome']);?></h4>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-12">
            <h6 class="text-center">ASSINATURA DO CANDIDATO:______________________________________________________</h6>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>
</body>
</html>