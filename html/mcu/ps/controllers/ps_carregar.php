<?php
function fncpslist(){
    $sql = "SELECT * FROM mcu_ps where ativo=1 ORDER BY data_ts desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pslista;
}

function fncgetps($id){
    $sql = "SELECT * FROM mcu_ps WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getps = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getps;
}
?>