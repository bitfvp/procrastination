<?php
class Ps{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpsnew($cod_ps,$nome_ps,$secretaria,$acao,$ativo,$cabecalho){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_ps ";
                $sql.="(id, cod_ps, nome_ps, secretaria, acao, ativo, cabecalho)";
                $sql.=" VALUES ";
                $sql.="(NULL, :cod_ps, :nome_ps, :secretaria, :acao, :ativo, :cabecalho)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":cod_ps", $cod_ps);
                $insere->bindValue(":nome_ps", $nome_ps);
                $insere->bindValue(":secretaria", $secretaria);
                $insere->bindValue(":acao", $acao);
                $insere->bindValue(":ativo", $ativo);
                $insere->bindValue(":cabecalho", $cabecalho);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vps_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpsedit($id,$cod_ps,$nome_ps,$secretaria,$acao,$ativo,$cabecalho){

        //inserção no banco
        try{
            $sql="UPDATE mcu_ps SET cod_ps=:cod_ps, nome_ps=:nome_ps, secretaria=:secretaria, acao=:acao, ativo=:ativo, cabecalho=:cabecalho WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":cod_ps", $cod_ps);
            $insere->bindValue(":nome_ps", $nome_ps);
            $insere->bindValue(":secretaria", $secretaria);
            $insere->bindValue(":acao", $acao);
            $insere->bindValue(":ativo", $ativo);
            $insere->bindValue(":cabecalho", $cabecalho);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vps_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}