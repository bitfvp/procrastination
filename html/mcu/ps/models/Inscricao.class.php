<?php
class Inscricao
{
    public function fncinscricaonew($id_pessoa,$id_ps,$referencia,$responsavel,$cod_cadastro)
    {

        //inserção no banco
        try {
            $sql = "INSERT INTO mcu_ps_inscricao ";
            $sql .= "(id, id_ps, id_pessoa, cod_cadastro, data_ts, referencia, responsavel)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :id_ps, :id_pessoa, :cod_cadastro, CURRENT_TIMESTAMP, :referencia, :responsavel)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":id_ps", $id_ps);
            $insere->bindValue(":id_pessoa", $id_pessoa);
            $insere->bindValue(":cod_cadastro", $cod_cadastro);
            $insere->bindValue(":referencia", $referencia);
            $insere->bindValue(":responsavel", $responsavel);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Inscrição cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vinsc&id={$id_pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }

}

?>