<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}


$page="Atividades".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
               <div class="col-md-9">
                   <?php
                   if (!empty($_SESSION['id']) && is_numeric($_SESSION['id'])) {
                       $sql = "SELECT "
                           . "COUNT(*) AS total,YEAR(mcu_pb_at.data), MONTH(mcu_pb_at.data)"
                           . "FROM "
                           . "mcu_pb_at "
                           . "WHERE (((mcu_pb_at.profissional)=?)) "
                           . "GROUP BY "
                           . "YEAR(mcu_pb_at.data), MONTH(mcu_pb_at.data)"
                           . " ORDER BY mcu_pb_at.data desc";
                       global $pdo;
                       $consulta = $pdo->prepare($sql);
                       $consulta->bindParam(1, $_GET['id']);
                       $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                       $qat = $consulta->fetchAll();
                       $sql=null;
                       $consulta=null;
                   }
                   ?>

                   <div class="card">
                       <div class="card-header bg-info text-light">
                           Histórico
                       </div>
                       <div class="card-body">
                           <table class="table table-sm">
                               <thead>
                               <tr>
                                   <th>PERÍODO</th>
                                   <th>QUANTIDADE</th>
                               </tr>
                               </thead>
                               <tbody>
                               <?php
                               $contar=0;
                               setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                               date_default_timezone_set('America/Sao_Paulo');
                               foreach($qat as $at){
                                   echo "<tr>";
                                   echo "<td>".utf8_encode(strftime('%B de %Y', strtotime("{$at[1]}-{$at[2]}-01")))."</td>";
                                   echo "<td>{$at[0]} atividades</td>";
                                   echo "</tr>";
                                   $contar+=$at[0];
                               }
                               ?>
                               </tbody>
                           </table>
                           <div class="card-footer text-info text-right">
                               Total:<?php echo $contar;?>
                           </div>

                       </div>
                   </div>


                <?php
                // Recebe
                $id_user =$_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($id_user) && is_numeric($id_user)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * \n"
                        . "FROM mcu_pb_at \n"
                        . "WHERE (profissional=?)\n"
                        . "ORDER BY data DESC LIMIT 0,1000";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $id_user);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $mat = $consulta->fetchAll();
                    $qat = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;
                }
                $proff=explode(" ",fncgetusuario($_GET['id'])['nome']);
                ?>

                <div class="card">
                    <div class="card-header bg-info text-light">
                        Atividade de <?php echo $proff[0];?> { <?php echo $qat;?> }
                    </div>
                    <div class="card-body">
                        <h6>
                            <?php
                            foreach ($mat as $at) {
                                switch ($at['tipo']){
                                    case 1:
                                        $cor="info";
                                        break;
                                    case 2:
                                        $cor="danger";
                                        break;
                                    default:
                                        $cor="muted";
                                        break;
                                }
                                ?>
                                <hr>
                                <div class="row">
                                    <div class="col-md-8">
                                        <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                            Pessoa:<strong class="text-info">
                                                <a href="?pg=Vat&id=<?php echo $at['pessoa']; ?>"><?php echo fncgetpessoa($at['pessoa'])['nome']; ?></a>
                                            </strong><br>
                                            Data:<strong class="text-info" title="Lançada em: <?php echo datahoraBanco2data($at['data_ts']); ?>"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                            Atividade:<strong class="text-info"><?php echo fncgetpb_atlista($at['atividade'])['atividade'];?>&nbsp;&nbsp;</strong><br>
                                            Descrição:
                                            <i class="fa fa-quote-left fa-sm "></i>
                                            <strong class="text-success"><?php echo $at['descricao']; ?></strong>
                                            <i class="fa fa-quote-right fa-sm"></i>
                                            <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                        </blockquote>
                                    </div>
                                    <div class="col-md-4">
                                        <?php
                                        if ($at['id'] != 0) {
                                            $files = glob("../../dados/mcu/protecaobasica/atividades/" . $at['id'] . "/*.*");
                                            for ($i = 0; $i < count($files); $i++) {
                                                $num = $files[$i];
                                                $extencao = explode(".", $num);
                                                //ultima posicao do array
                                                $ultimo = end($extencao);
                                                switch ($ultimo) {
                                                    case "docx":
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;

                                                    case "doc":
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;

                                                    case "xls":
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;

                                                    case "xlsx":
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;

                                                    case "pdf":
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;

                                                    default:
                                                        echo "<div class='col-md-10'>";
                                                        echo "<a href=" . $num . " target='_blank'>";
                                                        echo "<img src=" . $num . " alt='...' class='img-fluid img-thumbnail'>";
                                                        echo "</a>";
                                                        echo "</div>";
                                                        break;
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <br>
                            <?php } ?>
                        </h6>
                    </div>
                </div>
               </div><!--     fim de coluna 9   -->

            <!-- =============================fim conteudo======================================= -->
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>