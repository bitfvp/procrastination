<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}


$page="Cestas básicas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
               <div class="col-md-5">
                <?php
                // Recebe
                $id_user =$_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($id_user) && is_numeric($id_user)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * \n"
                        . "FROM mcu_beneficio \n"
                        . "WHERE (((mcu_beneficio.profissional)=?))\n"
                        . "ORDER BY mcu_beneficio.data_pedido DESC LIMIT 0,1000";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $id_user);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $mcb = $consulta->fetchAll();
                    $qcb = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;
                }
                $proff=explode(" ",fncgetusuario($_GET['id'])['nome']);
                ?>

                <div class="card">
                    <div class="card-header bg-info text-light">
                        Pedidos de cestas básicas de <?php echo $proff[0];?> { <?php echo $qcb;?> }
                    </div>
                    <div class="card-body">
                        <h6>
                            <?php
                            foreach($mcb as $cb){
                                switch ($cb['entregue']){
                                    case 0:
                                        $status="Aguardando Para Ser Entregue";
                                        $statuss="primary";
                                        break;
                                    case 1:
                                        $status="Entregue";
                                        $statuss="success";
                                        break;
                                    case 2:
                                        $status="Dificuldade de Localização";
                                        $statuss="warning";
                                        break;
                                }
                                $pessoa=fncgetpessoa($cb['pessoa']);
                                ?>
                                <blockquote class="blockquote blockquote-<?php echo $statuss?>">
                                    Pessoa:
                                    <a href="?pg=Vpessoa&id=<?php echo $cb['pessoa']; ?>">
                                        <strong class="text-<?php echo $statuss?>">
                                            <?php echo $pessoa['nome'];?>
                                        </strong>
                                    </a>
                                    <br>
                                    Endereço:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $pessoa['endereco']; ?>&nbsp;&nbsp;
                                    </strong>
                                    Numero:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $pessoa['numero']; ?>&nbsp;&nbsp;
                                    </strong>
                                    Bairro:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo fncgetbairro($pessoa['bairro'])['bairro']; ?>&nbsp;&nbsp;
                                    </strong>
                                    <br>
                                    Referência:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $pessoa['referencia']; ?>&nbsp;&nbsp;
                                    </strong>
                                    <br>
                                    Quantidade:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $cb['quantidade']; ?>&nbsp;&nbsp;
                                    </strong>
                                    Data para entrega:
                                    <strong class="text-<?php echo $statuss?>" title="Lançada em: <?php echo datahoraBanco2data($cb['data_ts']); ?>">
                                        <?php echo dataRetiraHora($cb['data_pedido']); ?>&nbsp;&nbsp;
                                    </strong>
                                    <br>
                                    obs:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $cb['descricao']; ?>&nbsp;&nbsp;
                                    </strong>
                                    <br>
                                    Status:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $status;?>&nbsp;&nbsp;
                                    </strong>
                                    Data da entrega:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo dataBanco2data($cb['data_entrega']); ?>&nbsp;&nbsp;
                                    </strong>
                                    Quem recebeu:
                                    <strong class="text-<?php echo $statuss?>">
                                        <?php echo $cb['quem_recebeu']; ?>&nbsp;&nbsp;
                                    </strong>

                                    <strong class="text-<?php echo $statuss?>">
                                        <?php
                                        if($cb['encontrado']==0){echo"<span class='text-danger'>Houve um problema com essa cesta basica, favor contate o adiministrador o mais rapido possivel.</span>";}
                                        if($cb['encontrado']==1){echo"";}
                                        ?>&nbsp;&nbsp;
                                    </strong>
                                    <span class="badge badge-pill badge-warning float-right"><strong><?php echo $cb['id']; ?></strong></span>
                                </blockquote>
                                <hr>
                            <?php } ?>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <?php
                if (!empty($_SESSION['id']) && is_numeric($_SESSION['id'])) {
                    $sql = "SELECT "
                        . "SUM(quantidade) AS total,YEAR(mcu_beneficio.data_pedido), MONTH(mcu_beneficio.data_pedido)"
                        . "FROM "
                        . "mcu_beneficio "
                        . "WHERE (((mcu_beneficio.profissional)=?)) "
                        . "GROUP BY "
                        . "YEAR(mcu_beneficio.data_pedido), MONTH(mcu_beneficio.data_pedido)"
                        . " ORDER BY mcu_beneficio.data_pedido desc";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $qcb = $consulta->fetchAll();
                    $sql=null;
                    $consulta=null;
                }
                ?>

                <div class="card">
                    <div class="card-header bg-info text-light">
                        Histórico
                    </div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th>PERÍODO</th>
                                <th>QUANTIDADE</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $contar=0;
                            setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                            date_default_timezone_set('America/Sao_Paulo');
                            foreach($qcb as $cb){
                                echo "<tr>";
                                echo "<td>".utf8_encode(strftime('%B de %Y', strtotime("{$cb[1]}-{$cb[2]}-01")))."</td>";
                                echo "<td>{$cb[0]} cestas básicas</td>";
                                echo "</tr>";
                                $contar+=$cb[0];
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="card-footer text-info text-right">
                            Total:<?php echo $contar;?>
                        </div>

                    </div>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>