<section class="sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <a href="index.php?pg=Vpessoa&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-user"></i> PROFFISIONAL</a>
        <a href="index.php?pg=Vacesso&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-sign-in-alt"></i> ACESSOS AO SISTEMA</a>
        <a href="index.php?pg=Vcb&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-shopping-basket"></i> CESTAS BÁSICAS</a>
        <a href="index.php?pg=Vat&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-folder"></i> ATIVIDADES</a>
        <a href="index.php?pg=Vlogs&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-terminal"></i> Logs</a>
    </div>
</section>

<script type="application/javascript">
    var offset = $('#sidebar').offset().top;
    var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
    $(document).on('scroll', function () {
        if (offset <= $(window).scrollTop()) {
            $meuMenu.addClass('fixarmenu');
        } else {
            $meuMenu.removeClass('fixarmenu');
        }
    });
</script>