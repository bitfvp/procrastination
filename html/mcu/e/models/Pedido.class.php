<?php
class Pe{
    public function fncpenew($data,$produto_id,$nota_id,$tipo,$responsavel,$obs,$quantidade){

        //inserção no banco
        try{
            $sql="INSERT INTO mcu_estoque_pedido ";
            $sql.="(id, data, produto_id, nota_id, tipo, responsavel, obs, quantidade)";
            $sql.=" VALUES ";
            $sql.="(NULL, :data, :produto_id, :nota_id, :tipo, :responsavel, :obs, :quantidade)";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":produto_id", $produto_id);
            $insere->bindValue(":nota_id", $nota_id);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":responsavel", $responsavel);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":quantidade", $quantidade);

            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        try{
            $sql="UPDATE mcu_estoque_atualiza SET status=1 WHERE id=1";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vne&id={$nota_id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}



class Ps{
    public function fncpsnew($data,$produto_id,$nota_id,$tipo,$responsavel,$obs,$quantidade){
        //inserção no banco
        try{
            $sql="INSERT INTO mcu_estoque_pedido ";
            $sql.="(id, data, produto_id, nota_id, tipo, responsavel, obs, quantidade)";
            $sql.=" VALUES ";
            $sql.="(NULL, :data, :produto_id, :nota_id, :tipo, :responsavel, :obs, :quantidade)";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":produto_id", $produto_id);
            $insere->bindValue(":nota_id", $nota_id);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":responsavel", $responsavel);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":quantidade", $quantidade);

            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        try{
            $sql="UPDATE mcu_estoque_atualiza SET status=1 WHERE id=1";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }

}