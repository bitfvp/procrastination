<?php
class Ne{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncnenew($identificador,$data,$fornecedor,$valor,$responsavel){
        

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque_nota_entrada ";
                $sql.="(id, data, identificador, fornecedor, responsavel, valor)";
                $sql.=" VALUES ";
                $sql.="(NULL, :data, :identificador, :fornecedor, :responsavel, :valor)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":identificador", $identificador);
                $insere->bindValue(":fornecedor", $fornecedor);
                $insere->bindValue(":responsavel", $responsavel);
                $insere->bindValue(":valor", $valor);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                $sql = "SELECT Max(id) FROM mcu_estoque_nota_entrada";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];

                header("Location: index.php?pg=Vne&id={$maid}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncneedit($id,$identificador,$data,$fornecedor,$valor,$responsavel){
 

        //inserção no banco
        try{
            $sql="UPDATE mcu_estoque_nota_entrada SET identificador=:identificador, data=:data, fornecedor=:fornecedor, valor=:valor, responsavel=:responsavel WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":identificador", $identificador);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":fornecedor", $fornecedor);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":responsavel", $responsavel);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vne&id={$id}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>