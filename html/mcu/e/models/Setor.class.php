<?php
class Setor{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsetornew( $setor){
        $nome=strtoupper($setor);

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque_setor ";
                $sql.="(id, setor)";
                $sql.=" VALUES ";
                $sql.="(NULL, :setor)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":setor", $setor);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vsetor_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }











    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsetoredit($id,$setor){

        //inserção no banco
        try{
            $sql="UPDATE mcu_estoque_setor SET setor=:setor WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":setor", $setor);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vsetor_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}