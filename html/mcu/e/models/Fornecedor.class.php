<?php
class Fornecedor{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncfornecedornew($razao_social,$cnpj,$telefone){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_estoque_fornecedor ";
                $sql.="(id, razao_social, cnpj, telefone)";
                $sql.=" VALUES ";
                $sql.="(NULL, :razao_social, :cnpj, :telefone)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":razao_social", $razao_social);
                $insere->bindValue(":cnpj", $cnpj);
                $insere->bindValue(":telefone", $telefone);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vfornecedor_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncfornecedoredit($id,$razao_social,$cnpj,$telefone){

        //inserção no banco
        try{
            $sql="UPDATE mcu_estoque_fornecedor SET razao_social=:razao_social, cnpj=:cnpj, telefone=:telefone WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":razao_social", $razao_social);
            $up->bindValue(":cnpj", $cnpj);
            $up->bindValue(":telefone", $telefone);
            $up->bindValue(":id", $id);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($up)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vfornecedor_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>