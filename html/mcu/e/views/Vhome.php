<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_estoque_produto WHERE produto LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_estoque_produto ";
}
// total de registros a serem exibidos por página
$total_reg = "30"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY produto LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas


$sql = "SELECT status FROM mcu_estoque_atualiza where id=1 ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$atualiza = $consulta->fetch();
$sql=null;
$consulta=null;

if ($atualiza[0]==1){
    header("Location: index.php?pg=Vhome&sca={$_GET['sca']}&aca=updateestoque");
    exit();
}
?>

<main class="container">
    <h2>Listagem do Estoque</h2>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <form action="index.php" method="get">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                    <input name="pg" value="Vhome" hidden/>
                    <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por produto..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vproduto_editar" class="btn btn btn-success btn-block">
                Novo produto
            </a>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vhome&aca=updateestoque&sca=<?php echo $_GET['sca']; ?>" class="btn btn btn-info btn-block">
                Atualiza estoque
            </a>
        </div>
    </div>




    <table id="tabela" class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
<!--                        --><?php
//                        // agora vamos criar os botões "Anterior e próximo"
//                        $anterior = $pc -1;
//                        $proximo = $pc +1;
//                        if ($pc>1) {
//                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
//                        }
//                        echo "|";
//                        if ($pc<$tp) {
//                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vindex&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
//                        }
//                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
            <tr>
                <th>PRODUTO</th>
                <th>UNIDADE</th>
                <th>QUANTIDADE</th>
            </tr>
        </thead>
            <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>

<tbody>
 <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id_prod = $dados["id"];
            $produto = $dados["produto"];
            $unidade = $dados["unidade"];
            $categoria = $dados["categoria"];
            $estoque = $dados["estoque"];
            $estoque_min = $dados["estoque_min"];
            ?>

	<tr>
        <td><?php
            if($_GET['sca']!=null) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
            $sta = CSA;
            $ccc = $produto;
            $cc = explode(CSA, $ccc);

            $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
            echo $c;
            }else{
            echo $produto;
            }
            ?>

            <a href="index.php?pg=Vproduto_historico&id=<?php echo $id_prod; ?>"><span class="fa fa-list float-left mr-2"></span></a>
            <a href="index.php?pg=Vproduto_editar&id=<?php echo $id_prod; ?>"><span class="fa fa-wrench float-right"></span></a>
        </td>
        <td>
        <?php
        if ($unidade!="0"){
            $unidade=fncgetunidade($unidade);
            echo $unidade['unidade'];
        }else{
            echo "???????";
        }
         ?>
         </td>
		<td>
        <?php
        if ($estoque < $estoque_min) {
            echo "<abbr title='Estoque Minimo {$estoque_min}' class='initialism'><span class='badge badge-danger'>{$estoque}</span></abbr>";
        }else{
            echo "<abbr title='Estoque Minimo {$estoque_min}' class='initialism'><span class='badge badge-success'>{$estoque}</span></abbr>";
        }
         ?>
        </td>

	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
