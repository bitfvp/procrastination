<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Unidade-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="unidadesave";
    $unidade=fncgetunidade($_GET['id']);
}else{
    $a="unidadenew";
}
?>
<main class="container">
    <h3 class="form-cadastro-heading">Cadastro de Apresentação</h3>
    <hr>
        <form class="form-signin" action="<?php echo "index.php?pg=Vunidade_lista&aca={$a}"; ?>" method="post">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $unidade['id']; ?>"/>
            <label for="unidade">Descrição</label>
            <input autocomplete="off" autofocus id="unidade" placeholder="" type="text" class="form-control" name="unidade" value="<?php echo $unidade['unidade']; ?>"/>
            <input type="submit" name="" value="Salvar" class="btn btn-primary mt-3" />
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>