<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Categoria-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="categoriasave";
    $categoria=fncgetcategoria($_GET['id']);
}else{
    $a="categorianew";
}
?>
<main class="container"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Cadastro de categoria</h3>
    <hr>
    <form class="form-signin" action="<?php echo "index.php?pg=Vcategoria_lista&aca={$a}"; ?>" method="post">
        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $categoria['id']; ?>"/>
        <label for="categoria">Categoria:</label>
        <input autocomplete="off" autofocus id="categoria" type="text" class="form-control" name="categoria" value="<?php echo $categoria['categoria']; ?>"/>
        <input type="submit" class="btn btn-info mt-3" value="Salvar"/>
    </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>