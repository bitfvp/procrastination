<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Produto Historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


    if(isset($_GET['id'])) {
        $produto=fncgetproduto($_GET['id']);
    }
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <!-- direito -->
        <div class="col-md-4">

            <div class="card">
                <div class="card-header bg-info text-light">
                    Produto
                </div>
                <div class="card-body">
                    Produto:<strong class="text-info"><?php echo $produto['produto']; ?>&nbsp;&nbsp;</strong><br>
                    Categoria:
                    <strong class="text-info">
                        <?php
                        if ($produto['categoria'] != "0") {
                            echo fncgetcategoria($produto['categoria'])['categoria'];
                        } else {
                            echo "???????";
                        }
                        ?>
                    </strong>&nbsp;&nbsp;<br>
                    Apresentação:
                    <strong class="text-info">
                        <?php
                        if ($produto['unidade'] != "0") {
                            echo fncgetunidade($produto['unidade'])['unidade'];
                        } else {
                            echo "???????";
                        }
                        ?>
                    </strong>&nbsp;&nbsp;<br>

                    Estoque:
                    <strong class="text-info">
                        <?php
                        if ($produto['estoque'] < $produto['estoque_min']) {
                            echo "<abbr title='Estoque Minimo {$produto['estoque_min']}' class='initialism'><span class='badge badge-danger'>{$produto['estoque']}</span></abbr>";
                        }else{
                            echo "<abbr title='Estoque Minimo {$produto['estoque_min']}' class='initialism'><span class='badge badge-success'>{$produto['estoque']}</span></abbr>";
                        }
                        ?>
                    </strong>
                    <a href="index.php?pg=Vproduto_editar&id=<?php echo $_GET['id']; ?>" title="Edite produto" class="btn btn-success btn-block mt-4">
                        Editar produto
                    </a>
                </div>
            </div>
            <!-- fim da col md 4 -->
        </div>


        <!-- esquerdo -->
        <div class="col-md-10 mt-3">
            <?php
            $sql = "SELECT * FROM mcu_estoque_pedido \n"
                . "WHERE produto_id=? \n"
                . "ORDER BY mcu_estoque_pedido.data DESC";
            global $pdo;
            $cons = $pdo->prepare($sql);
            $cons->bindParam(1, $_GET['id']);
            $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pedidos = $cons->fetchAll();
            $sql=null;
            $consulta=null;
            ?>
            <table id="tabela" class="table table-striped table-sm table-hover">
                <thead class="bg-primary">
                <tr>
                    <th>Tipo</th>
                    <th>Produto</th>
                    <th>Quant.</th>
                    <th>Data</th>
                </tr>
                </thead>

                <?php
                // vamos criar a visualização
                foreach ($pedidos as $dados) {
                $id = $dados["id"];
                $produto_id = $dados["produto_id"];
                $produto = fncgetproduto($produto_id)['produto'];
                $data= $dados["data"];
                $tipo=$dados["tipo"];
                $nota_id=$dados["nota_id"];
                $responsavel=fncgetusuario($dados["responsavel"])['nome'];

                $quantidade = $dados["quantidade"];
                ?>
                <tbody>
                    <tr data-toggle="collapse" data-target="#accordion<?php echo $id; ?>" class="clickable">
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                        <?php if ($tipo==1) { echo "Entrada"; } if ($tipo==2) { echo "Saida";} ?>
                        <i class="fa fa-exchange pull-right"></i>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                        <?php echo $produto; ?>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                            <?php echo $quantidade; ?>
                        </td>
                        <td class="<?php if ($tipo==1) { echo "bg-success"; }else{ echo "bg-danger";} ?>">
                            <?php echo datahorabanco2data($data); ?>
                        </td>
                    </tr>

                    <tr id="accordion<?php echo $id; ?>" class="collapse">
                        <td colspan="4">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul>
                                        Responsável pelo lançamento:
                                        <li><?php echo $responsavel; ?></li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <ul>Nota:
                                        <li>
                                            <?php
                                        if ($tipo==1) {
                                            $sql = "SELECT * FROM mcu_estoque_nota_entrada WHERE id=?";
                                            global $pdo;
                                            $consulta = $pdo->prepare($sql);
                                            $consulta->bindParam(1, $nota_id);
                                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                            $notain = $consulta->fetch();

                                            $sql=null;
                                            $consulta=null;

                                            echo "<a href='index.php?pg=Vne&id={$nota_id}'>";
                                            echo $notain['identificador'];
                                            echo "</a>";
                                        }
                                        if ($tipo==2) {
                                            $sql = "SELECT * FROM mcu_estoque_nota_saida WHERE id=?";
                                            global $pdo;
                                            $consulta = $pdo->prepare($sql);
                                            $consulta->bindParam(1, $nota_id);
                                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                            $notaout = $consulta->fetch();

                                            $sql=null;
                                            $consulta=null;

                                            echo "<a href='index.php?pg=Vns&id={$nota_id}'>";
                                            echo $notaout['identificador'];
                                            echo "</a>";
                                        }
                                        ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr><?php
                } ?>
                </tbody>
            </table>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>