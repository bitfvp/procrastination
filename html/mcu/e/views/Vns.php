<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Nota de saida-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $ns=fncgetns($_GET['id']);
}else{
    header("Location: index.php");
    exit();
}
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <!-- direito -->
        <div class="col-md-4">

            <div class="card">
                <div class="card-header bg-info text-light">
                    Dados da nota
                </div>
                <div class="card-body">
                    Identificador:<strong class="text-info"><?php echo $ns['identificador']; ?>&nbsp;&nbsp;</strong><br>
                    Data:<strong class="text-info"><?php echo dataBanco2data($ns['data']); ?>&nbsp;&nbsp;</strong><br>
                    Setor:
                    <strong class="text-info">
                        <?php
                        if ($ns['setor'] != "0") {
                            echo fncgetsetor($ns['setor'])['setor'];
                        } else {
                                echo "???????";
                        }
                        ?>
                    </strong>&nbsp;&nbsp;<br>
                    <hr>
                    Responsável pelo cadastro:<br>
                    <strong class="text-info"><?php
                        if ($ns['responsavel'] != "0") {
                            echo fncgetusuario($ns['responsavel'])['nome'];

                        } else {
                                echo "???????";
                        }
                            ?>
                    </strong>&nbsp;&nbsp;
                    <a href="index.php?pg=Vns_editar&id=<?php echo $_GET['id']; ?>" title="Edite nota" class="btn btn-success btn-block mt-4">
                        Editar nota
                    </a>
                </div>
            </div>
            <!-- fim da col md 4 -->
        </div>

        <!-- esquerdo -->
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Formúlario de Novos Itens
                </div>
                <div class="card-body">
                    <form class="form-signin" action="index.php?pg=Vns&id=<?php echo $_GET['id']; ?>&aca=psnew" method="post">
                        <input type="submit" class="btn btn-success btn-block" value="Salvar pedido"/>
                        <hr>
                        <label for="produto_id">Produto</label>
                        <select name="produto_id" autofocus="yes" id="produto_id" class="form-control input-sm selectpicker" data-live-search="true"  data-style="btn-warning">
                            // vamos criar a visualização
                            <?php
                            foreach (fncprodutolist() as $item) {
                                ?>
                                <option data-tokens="<?php echo $item['produto']; ?>" value="<?php echo $item['id']; ?>"><?php echo $item['produto']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input autocomplete="off" id="nota_id" required="yes" type="hidden" class="form-control input-sm" name="nota_id" value="<?php echo $_GET['id']; ?>"/>
                        <input autocomplete="off"  id="tipo" required="yes" type="hidden" class="form-control input-sm" name="tipo" value="2"/>
                        <label for="quantidade">Quantidade</label>
                        <input id="quantidade" required="yes" type="number" class="form-control input-sm"  step="0.1" name="quantidade" value="0"/>
                        <label for="obs">Obs</label>
                        <input autocomplete="off"  id="obs" type="text" class="form-control" name="obs" value=""/>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
                    <?php
                    $sql = "SELECT mcu_estoque_pedido.id, mcu_estoque_pedido.responsavel, mcu_estoque_produto.produto, mcu_estoque_produto.unidade, mcu_estoque_produto.categoria, mcu_estoque_pedido.nota_id, mcu_estoque_pedido.tipo, mcu_estoque_pedido.quantidade, mcu_estoque_pedido.data\n"
                        . "FROM mcu_estoque_produto INNER JOIN mcu_estoque_pedido ON mcu_estoque_produto.id = mcu_estoque_pedido.produto_id\n"
                        . "WHERE (((mcu_estoque_pedido.nota_id)=?) AND ((mcu_estoque_pedido.tipo)=2))\n"
                        . "ORDER BY mcu_estoque_pedido.data DESC";
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $pedlista = $cons->fetchAll();
                    $sql = null;
                    $consulta = null;
                    ?>
                    <!-- tabela -->
                    <table id="tabela" class="table table-hover table-sm">
                        <thead class="bg-info">
                        <tr>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Responsável</th>
                            <th>ID</th>
                        </tr>
                        </thead>
                        <script>
                            $(document).ready(function(){
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                        </script>
                        <tbody class="bg-danger">
                        <?php
                        // vamos criar a visualização
                        foreach ($pedlista as $dados) {
                            $id = $dados["id"];
                            $produto = $dados["produto"];
                            $quantidade = $dados["quantidade"];
                            $responsavel = fncgetusuario($dados['responsavel'])['nome'];
                            ?>

                            <tr>
                                <td><?php
                                    echo $produto;
                                    ?>
                                </td>
                                <td><?php echo $quantidade; ?></td>
                                <td><?php echo $responsavel; ?></td>
                                <td class="info"><i class="fa fa-star"> </i><?php echo $id; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>