<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Fornecedor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="fornecedorsave";
    $fornecedor=fncgetfornecedor($_GET['id']);
}else{
    $a="fornecedornew";
}
?>
<main class="container"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Cadastro de fornecedor</h3>
    <hr>
    <form class="form-signin" action="<?php echo "index.php?pg=Vfornecedor_lista&aca={$a}"; ?>" method="post">
        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $fornecedor['id']; ?>"/>
        <label for="razao_social">Razão Social</label>
        <input autocomplete="off" autofocus id="razao_social" placeholder="Razão Social" type="text" class="form-control" required="true" name="razao_social" value="<?php echo $fornecedor['razao_social']; ?>"/>
        <label for="">CNPJ</label>
        <input type="text" name="cnpj" id="cnpj" value="<?php echo $fornecedor['cnpj']; ?>" title="CNPJ da empresa" class="form-control" placeholder="CNPJ" />
        <script>
            $(document).ready(function(){
                $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
            });
        </script>
        <label for="">Telefone</label>
        <input type="text" name="telefone" value="<?php echo $fornecedor['telefone']; ?>" title="Telefone de Contato" class="form-control" placeholder="Telefone" />
        <input type="submit" name="" value="Salvar" class="btn btn-info mt-3" />
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>