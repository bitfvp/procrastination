<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{

        }
    }
}

$page="Relatório de consumo-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$setor=$_POST['setor'];

    $sql = "SELECT	mcu_estoque_pedido.produto_id, Sum(	mcu_estoque_pedido.quantidade) AS quantidade "
        ."FROM "
        ."mcu_estoque_pedido INNER JOIN mcu_estoque_nota_saida ON mcu_estoque_nota_saida.id = mcu_estoque_pedido.nota_id "
        ."WHERE "
        ."mcu_estoque_nota_saida.setor = :setor "
        ."AND mcu_estoque_pedido.`data` >= :inicial "
        ."AND mcu_estoque_pedido.`data` <= :final "
        ."AND mcu_estoque_pedido.`tipo` = 2 "
        ."GROUP BY mcu_estoque_pedido.produto_id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":setor",$setor);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $consumo = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <h3>Relatório de consumo</h3>
    <h5><?php echo fncgetsetor($setor)['setor']?></h5>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>

    <table class="table table-sm">
        <thead>
        <th>Produto</th>
        <th>Quantidade</th>
        </thead>
        <tbody>
        <?php
        foreach ($consumo as $con){
            echo "<tr>";
            echo "<td>".fncgetproduto($con['produto_id'])['produto']."</td>";
            echo "<td>".$con['quantidade']."</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>


</div>
</html>