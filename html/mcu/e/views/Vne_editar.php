<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Nota de Entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="nesave";
    $ne=fncgetne($_GET['id']);
}else{
    $a="nenew";
}
?>
<main class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Vne_lista&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ne['id']; ?>"/>
            <label for="identificador">Identificador</label><input autocomplete="off" required="true" autofocus id="identificador"  required="yes" type="text" class="form-control" name="identificador" value="<?php echo $ne['identificador']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="data">Data</label><input autocomplete="off" id="data"  required="true" type="date" class="form-control" name="data" value="<?php echo $ne['data']; ?>"/>
        </div>

        <div class="col-md-12">
        <label   for="fornecedor">Fornecedor</label>
            <select name="fornecedor" required="true" id="fornecedor" class="form-control"  required="true">
                // vamos criar a visualização
                <option selected="" value="<?php echo $ne['fornecedor'];?>">
                    <?php
                    $fornecedor=fncgetfornecedor($ne['fornecedor']);
                    echo $fornecedor['razao_social'];
                    ?>
                </option>
                <?php
                foreach(fncfornecedorlist() as $item){?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['razao_social']; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-12">
            <label for="valor">Valor</label><input id="valor" type="number" class="form-control" required="true" name="valor" step="0.01" value="<?php echo $ne['valor']; ?>"/>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>