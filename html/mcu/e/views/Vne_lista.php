<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista Notas de Entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_estoque_nota_entrada WHERE identificador LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_estoque_nota_entrada ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>
<main class="container">
    <h2>Listagem de notas de entrada</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vne_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vne_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        Nova nota de entrada
    </a>

    <table id="tabela" class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <!--                        --><?php
                        //                        // agora vamos criar os botões "Anterior e próximo"
                        //                        $anterior = $pc -1;
                        //                        $proximo = $pc +1;
                        //                        if ($pc>1) {
                        //                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vhome&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        //                        }
                        //                        echo "|";
                        //                        if ($pc<$tp) {
                        //                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vindex&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        //                        }
                        //                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $tr;?> Resultado(s)</th>
        </tr>
        </thead>

        <thead class="thead-dark">
	<tr>
		<th>IDENTIFICADOR</th>
		<th>DATA</th>
		<th>FORNECEDOR</th>
		<th>VALOR</th>
        <th>EDITAR</th>
	</tr>
</thead>
<tbody>
 <?php
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $identificador = $dados["identificador"];
            $data = dataBanco2data($dados["data"]);
            $fornecedor = $dados["fornecedor"];
            $valor = $dados["valor"];
            ?>

	<tr>
        <td>
        <a href="index.php?pg=Vne&id=<?php echo $id; ?>">
        <?php
            if($_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
            $sta = CSA;
            $ccc = $identificador;
            $cc = explode(CSA, $ccc);
            $c = implode("<span class='text-success'>{$sta}</span>", $cc);
            echo $c;
            }else{
            echo $identificador;
            }
            ?>
        </a>
        </td>

        <td>
            <?php echo $data; ?>
        </td>

		<td>
        <?php
        if ($fornecedor!="0"){
            $fornecedor=fncgetfornecedor($fornecedor);
            echo $fornecedor['fornecedor'];
        }else{
            echo "???????";
        }
         ?>
         </td>

        <td><?php echo "R$ ".number_format($valor,2); ?></td>
        <td><a href="index.php?pg=Vne_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a></td>
	</tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
