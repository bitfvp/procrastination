<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Setor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="setorsave";
    $setor=fncgetsetor($_GET['id']);
}else{
    $a="setornew";
}
?>
<main class="container"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Cadastro de setor</h3>
    <hr>
    <form class="form-signin" action="<?php echo "index.php?pg=Vsetor_lista&aca={$a}"; ?>" method="post">
        <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $setor['id']; ?>"/>
        <label for="setor">Setor:</label>
        <input autocomplete="off" autofocus id="setor" type="text" class="form-control" name="setor" value="<?php echo $setor['setor']; ?>"/>
        <input type="submit" class="btn btn-info mt-3" value="Salvar"/>
    </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>