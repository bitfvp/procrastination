<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_79"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Nota de Entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="nssave";
    $ns=fncgetns($_GET['id']);
}else{
    $a="nsnew";
}
?>
<main class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Vns_lista&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $ns['id']; ?>"/>
            <label for="identificador">Identificador</label><input autocomplete="off" required="true" autofocus id="identificador"  required="yes" type="text" class="form-control" name="identificador" value="<?php echo $ns['identificador']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="data">Data</label><input autocomplete="off" id="data"  required="true" type="date" class="form-control" name="data" value="<?php echo $ns['data']; ?>"/>
        </div>

        <div class="col-md-12">
        <label   for="setor">Setor</label>
            <select name="setor" required="true" id="setor" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php echo $ns['setor'];?>">
                    <?php
                    $setor=fncgetsetor($ns['setor']);
                    echo $setor['setor'];
                    ?>
                </option>
                <?php
                foreach(fncsetorlist() as $item){?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['setor']; ?></option>
                <?php } ?>
            </select>
        </div>

    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>