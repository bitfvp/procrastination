<?php
function fncfornecedorlist(){
    $sql = "SELECT * FROM mcu_estoque_fornecedor ORDER BY razao_social";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $fornecedorlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $fornecedorlista;
}

function fncgetfornecedor($id){
    $sql = "SELECT * FROM mcu_estoque_fornecedor WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getfornecedor = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getfornecedor;
}
?>
