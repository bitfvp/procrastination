<?php
function fncprodutolist(){
    $sql = "SELECT * FROM mcu_estoque_produto ORDER BY produto";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $produtolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $produtolista;
}

function fncgetproduto($id){
    $sql = "SELECT * FROM mcu_estoque_produto WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getproduto = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getproduto;
}
?>
