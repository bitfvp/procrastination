<?php
function fncsetorlist(){
    $sql = "SELECT * FROM mcu_estoque_setor ORDER BY setor";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $setorlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $setorlista;
}

function fncgetsetor($id){
    $sql = "SELECT * FROM mcu_estoque_setor WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getsetor = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getsetor;
}