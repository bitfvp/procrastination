<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="updateestoque"){

    $sql = "SELECT * FROM mcu_estoque_produto ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $produtos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;


    foreach ($produtos as $pro){
        $sql = "SELECT * FROM mcu_estoque_pedido where produto_id=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$pro['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $pedidos = $consulta->fetchAll();
        $sql=null;
        $consulta=null;

        $variavel=0;

        foreach ($pedidos as $ped){
            if ($ped['tipo']==1){
                $variavel=$variavel+$ped['quantidade'];
            }
            if ($ped['tipo']==2){
                $variavel=$variavel-$ped['quantidade'];
            }

        }

        try{
            $sql="UPDATE mcu_estoque_produto SET estoque=:estoque WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":estoque", $variavel);
            $insere->bindValue(":id", $pro['id']);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


    }

    try{
        $sql="UPDATE mcu_estoque_atualiza SET status=0 WHERE id=1";

        global $pdo;
        $insere=$pdo->prepare($sql);
        $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Atualização de estoque realizado com sucesso!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vhome&sca={$_GET['sca']}");
    exit();

}