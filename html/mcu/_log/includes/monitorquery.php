<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");
include_once("{$_ENV['ENV_ROOT']}controllers/usuario_lista.php");
$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


// Prepare a select statement
    $sql = "SELECT * FROM tbl_logquery WHERE id>=1 ORDER by id DESC limit 0,50";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
$frase = $consulta->fetchall();
    $contcidade = $consulta->rowCount();
    $sql=null;
    $consulta=null;


$dia=date("d");
$mes=date("m");
$ano=date("Y");
$hora=date("H");
$minuto=date("i");

// Prepare a select statement
$sql = "SELECT COUNT(*) FROM tbl_logquery WHERE `data` LIKE '%{$ano}-{$mes}-{$dia} {$hora}:{$minuto}%'";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
$quantidadeporminuto = $consulta->fetch();
$sql=null;
$consulta=null;


    // Retornando frase em formato JSON
//    echo json_encode($frase);

echo "<hr> Quantidade nesse minuto:";
echo $quantidadeporminuto[0];

?>


<table class="table table-striped table-hover table-sm">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">DATA</th>
        <th scope="col">USUÁRIO</th>
        <th scope="col">FONTE</th>
        <th scope="col">QUERY</th>

    </tr>
    </thead>

    <tbody>
<?php
foreach ($frase as $tx){
    $tempdata=datahoraBanco2data($tx[1]);
    echo "<tr>";
    echo "<td>{$tx[0]}</td>";
    echo "<th>{$tempdata}</th>";
    echo "<td>".fncgetusuario($tx[3])['nome']."</td>";
    echo "<td>{$tx[4]}</td>";
    echo "<td>{$tx[2]}</td>";
    echo "</tr>";
}
?>
    </tbody>
</table>
