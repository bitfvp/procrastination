<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
    $realurl=$_ENV['ENV_URL_IP'];
}else{
    $realurl=$_ENV['ENV_URL'];
}

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");
include_once("{$_ENV['ENV_ROOT']}controllers/usuario_lista.php");
$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}

    // Prepare a select statement
$sql = "SELECT * FROM tbl_token ORDER BY tbl_token.id DESC LIMIT 0, 50";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
$frase = $consulta->fetchall();
    $contcidade = $consulta->rowCount();
    $sql=null;
    $consulta=null;



    // Retornando frase em formato JSON
//    echo json_encode($frase);
?>
<div class="col-md-8" >
    <table class="table table-hover table-sm table-responsive">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">DATA</th>
            <th scope="col">USUÁRIO</th>
            <th scope="col">TEMPO RESTANTE</th>
            <th scope="col">DISPOSITIVO</th>
            <th scope="col">S.O.</th>
            <th scope="col">NAVEGADOR</th>
        </tr>
        </thead>

        <tbody>
    <?php
    foreach ($frase as $tx){
        $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
        $newtm =$tx['token_time']-$tml;
        if ($newtm<0 or $newtm>3600){$newtm=0;}
        $newtmm=gmdate("H:i:s", $newtm);
        $tempdata=datahoraBanco2data($tx['data']);

        if ($newtm>0){echo "<tr class='text-success'>";}else{echo "<tr class='text-danger'>";}
    //    echo "<tr>";
        echo "<td>{$tx['id']}</td>";
        echo "<th>{$tempdata}</th>";
        echo "<td>".fncgetusuario($tx['user'])['nome']."</td>";
        echo "<td>{$newtmm}</td>";
        echo "<td>{$tx['dispositivo']}</td>";
        echo "<td>{$tx['sistema_operacional']}</td>";
        echo "<td>{$tx['navegador']}</td>";
        echo "</tr>";
    }
    ?>
        </tbody>
    </table>
</div>

<div class="col-md-4">
    <a href="index.php?pg=Vmonitorlogin&intervalo=60" class="btn btn-sm btn-outline-danger"><i class="fa fa-clock"></i> 1 minuto</a>
    <a href="index.php?pg=Vmonitorlogin&intervalo=300" class="btn btn-sm btn-outline-danger"><i class="fa fa-clock"></i> 5 minutos</a>
    <a href="index.php?pg=Vmonitorlogin&intervalo=600" class="btn btn-sm btn-outline-danger"><i class="fa fa-clock"></i> 10 minutos</a>
    <hr class="my-2">
    <?php
    if (is_numeric($_GET['intervalo']) and isset($_GET['intervalo'])){
        $intervalo=$_GET['intervalo'];
    }else{
        $intervalo=600;
    }
    // Prepare a select statement
    $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    $tm2 = ($tml+3600) + $intervalo;
    $tml+=(3600 - $intervalo);
    try {
        $sql = "SELECT "
            ."tbl_token.id, tbl_token.token_time, tbl_users.nome, tbl_token.user AS id_prof "
            ."FROM tbl_users INNER JOIN tbl_token ON tbl_users.id = tbl_token.user WHERE (((tbl_token.token_time)>?) and ((tbl_token.token_time)<?) and ((tbl_users.matriz)>0)) ORDER BY tbl_users.nome";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1,$tml);
        $consulta->bindParam(2,$tm2);
        $consulta->execute();
    } catch (PDOException $error) {
        echo 'erro ao executar:' . $error->getMessage();
    }
    $frase = $consulta->fetchall();
    $contcidade = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    $verif=0;
    ?>

    <section class="sidebar-offcanvas" id="sidebar">
        <div class="list-group text-left">
            <?php
            $contausuariosonline=0;
            foreach ($frase as $tx){
                if ($verif!=$tx[3]){
                    $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                    $newtm =$tx[1]-$tml;
                    if ($newtm<0){$newtm=0;}
                    $newtm=gmdate("H:i:s", $newtm);

                    //link pra msg que abre em novo painel

                    if ($tx[3]!=$_SESSION['id']){
                        echo "<a href='{$realurl}mcu/?pg=Vmsg&id={$tx[3]}' target=\"_blank\" onclick=\"window.open(this.href, this.target, 'width=500,height=650'); return false;\" ";
                        echo " target='_blank' onclick='window.open(this.href, this.target, 'width=500,height=650'); return false;'";
                    }else{
                        echo "<a href='#' ";
                    }
                    echo " class='list-group-item text-info'  id='listonline' ><i class='fa fa-user-circle text-success'></i> ";
                    echo $tx[2];
                    echo "</a>";

                    $verif=$tx[3];
                    $contausuariosonline++;

                }
            }
            echo "<strong class='list-group-item text-info'  id='listonline' ><i class='fa fa-calculator text-info'></i> ";
            echo $contausuariosonline;
            echo "</strong>";
            ?>
        </div>
    </section>

</div>
