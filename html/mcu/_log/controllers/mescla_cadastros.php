<?php
//controller
if($startactiona==1 && $aca=="mesclar_cadastros"){

    $id=$_GET['id'];
    $id2=$_GET['id2'];
    if ((isset($id) and is_numeric($id)) and (isset($id2) and is_numeric($id2)) and $id!=$id2){

        $tabelas = array(
            'mcu_beneficio',
            'mcu_bf_at',
            'mcu_cr_idoso',
            'mcu_cr_idoso_at',
            'mcu_cr_pcd',
            'mcu_cr_pcd_at',
            'mcu_ct_denuncia',
            'mcu_curso_inscrito',
            'mcu_log',
            'mcu_p_adv',
            'mcu_p_apae',
            'mcu_p_apae_at',
            'mcu_p_fibro',
            'mcu_p_fibro_at',
            'mcu_p_idoso',
            'mcu_p_idoso_at',
            'mcu_p_pcd',
            'mcu_p_pcd_at',
            'mcu_pb_at',
            'mcu_ps_inscricao',
            'mcu_tel_at',
            'mcu_tel_frequencia',
            'mcu_visitaaveriguacao',
        );

        $campos = array(
            'pessoa',       //'mcu_beneficio',
            'pessoa',       //'mcu_bf_at',
            'pessoa',       //'mcu_cr_idoso',
            'pessoa',       //'mcu_cr_idoso_at',
            'pessoa',       //'mcu_cr_pcd',
            'pessoa',       //'mcu_cr_pcd_at',
            'pessoa',       //'mcu_ct_denuncia',
            'pessoa',       //'mcu_curso_inscrito',
            'pessoa',       //'mcu_log',
            'pessoa',       //'mcu_p_adv',
            'pessoa',       //'mcu_p_apae',
            'pessoa',       //'mcu_p_apae_at',
            'pessoa',       //'mcu_p_fibro',
            'pessoa',       //'mcu_p_fibro_at',
            'pessoa',       //'mcu_p_idoso',
            'pessoa',       //'mcu_p_idoso_at',
            'pessoa',       //'mcu_p_pcd',
            'pessoa',       //'mcu_p_pcd_at',
            'pessoa',       //'mcu_p_tea',
            'pessoa',       //'mcu_p_tea_at',
            'pessoa',       //'mcu_pb_at',
            'id_pessoa',       //'mcu_ps_inscricao',
            'pessoa',       //'mcu_tel_at',
            'pessoa',       //'mcu_tel_frequencia',
            'pessoa',       //'mcu_visitaaveriguacao',
        );

        $textos = array(
            ' registros de cestas básicas',       //'mcu_beneficio',
            ' registros no bolsa familia',       //'mcu_bf_at',
            ' registros em cr_idoso',       //'cr_idoso',
            ' registros em cr_idoso_at',       //'cr_idoso_at',
            ' registros em cr_pcd',       //'cr_pcd',
            ' registros em cr_pcd_at',       //'cr_pcd_at',
            ' registros de denuncias',       //'mcu_ct_denuncia',
            ' registros em curso',       //'mcu_curso_inscrito',
            ' registros em log',       //'mcu_log',
            ' registros em advertencias em carteiras',       //'mcu_p_adv',
            ' registros carteira apae',       //'mcu_p_apae',
            ' registros em atividades carteira apae',       //'mcu_p_apae_at',
            ' registros carteira fibro',       //'mcu_p_fibro',
            ' registros em atividades carteira fibro',       //'mcu_p_fibro_at',
            ' registros carteira idoso',       //'mcu_p_idoso',
            ' registros em atividades carteira idoso',       //'mcu_p_idoso_at',
            ' registros carteira pcd',       //'mcu_p_pcd',
            ' registros em atividades carteira pcd',       //'mcu_p_pcd_at',
            ' registros carteira tea',       //'mcu_p_tea',
            ' registros em atividades carteira tea',       //'mcu_p_tea_at',
            ' registros atividades prot basica',       //'mcu_pb_at',
            ' registros processo seletivo',       //'mcu_ps_inscricao',
            ' registros telecentro at',       //'mcu_tel_at',
            ' registros tel frequencia',       //'mcu_tel_frequencia',
            ' registros visita averiguação',       //'mcu_visitaaveriguacao',
        );


        $contadora=0;
        foreach ($tabelas as $tab) {

            try{
                $sql="UPDATE " . $tab . " SET " . $campos[$contadora] . "= ? where " . $campos[$contadora] . "=?";
                global $pdo;
                $consulta=$pdo->prepare($sql);
                $consulta->bindParam(1, $id);
                $consulta->bindParam(2, $id2);
                $consulta->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }
            $contadora++;
        }

        //altera o dado em tabela duplicada
        try{
            $sql="UPDATE mcu_pessoas_duplicadas SET verificada = 1 where pessoa=? or pessoa=?";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $id);
            $consulta->bindParam(2, $id2);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        //apaga duplicada
        try{
            $sql="DELETE FROM mcu_pessoas where id=?";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $id2);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        //msg e retorno
        $_SESSION['fsh']=[
            "flash"=>"Registros mesclados e cadastro duplicado apagado",
            "type"=>"success",
        ];
        header("Location: index.php");
        exit();

    }else{

        //se os ids estiverem errado
        $_SESSION['fsh']=[
            "flash"=>"Houve um problema",
            "type"=>"danger",
            ];
        header("Location: index.php");
        exit();
    }
}


if($startactiona==1 && $aca=="falsa_duplicata"){

    $id=$_GET['id'];

    if ((isset($id) and is_numeric($id))){

        //altera o dado em tabela duplicada
        try{
            $sql="UPDATE mcu_pessoas_duplicadas SET verificada = 1 where pessoa=? ";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }


        //msg e retorno
        $_SESSION['fsh']=[
            "flash"=>"falsa duplicata desativada",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vpess_duplicadas_d");
        exit();

    }else{

        //se os ids estiverem errado
        $_SESSION['fsh']=[
            "flash"=>"Houve um problema",
            "type"=>"danger",
        ];
        header("Location: index.php");
        exit();
    }
}
