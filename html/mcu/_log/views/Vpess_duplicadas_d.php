<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
    }
}


$page = "Pessoas duplicadas-denúncias-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

// Captura os dados do cliente solicitado
$sql = "SELECT pessoa, profissional FROM `mcu_pessoas_duplicadas` where `verificada`<>1";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$pessoas_duplicadas = $consulta->fetchAll();
$sql=null;
$consulta=null;
?>

<main class="container"><!--todo conteudo-->

<div class="row">
    <div class="col-md-12">
        <div class="container">
            <table class="table table-striped table-hover table-sm">
                <thead>
                <tr>
                    <th scope="col">NOME</th>
                    <th scope="col">NASCIMENTO</th>
                    <th scope="col">CPF</th>
                    <th scope="col">RG</th>
                    <th scope="col">PROF.</th>
                    <th scope="col">BAIRRO</th>
                    <th scope="col">FALSA DUPLICATA</th>
                    <th scope="col">MESCLAR</th>
                </tr>
                </thead>
                <tbody>


                <?php
                foreach ($pessoas_duplicadas as $duplicatas){
                    $pessoa=fncgetpessoa($duplicatas['pessoa']);
                    $profissional=fncgetusuario($duplicatas['profissional']);
                    $id = $pessoa["id"];
                    $nome = strtoupper($pessoa["nome"]);
                    $nascimento = dataBanco2data ($pessoa["nascimento"]);
                    $cpf = $pessoa["cpf"];
                    $rg = $pessoa["rg"];
                    $prof=$profissional['nome'];
                    $cod_familiar=$pessoa["cod_familiar"];
                    $bairro = $pessoa["bairro"];
                    ?>
                    <tr>
                        <th scope="row" id="<?php echo $id;  ?>">
                            <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                                <?php
                                    echo $nome;
                                ?>
                            </a>
                        </th>
                        <td>
                            <?php
                            if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                                echo $nascimento;
                            }else{
                                echo "<span class='text-danger'>--/--/----</span>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($cpf!="0" and $cpf!="") {
                                    echo mask($cpf,'###.###.###_##');
                            }else{
                                echo "--- --- --- --";
                            }
                            ?></td>
                        <td>
                            <?php
                            if($rg!="0" and $rg!="") {
                                    echo $rg;
                            }else{
                                echo "--- --- ---";
                            }
                            ?></td>
                        <td>
                            <?php
                            echo $prof;
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($bairro != "0") {
                                $cadbairro=fncgetbairro($bairro);
                                echo $cadbairro['bairro'];
                            } else {
                                echo "<span class='text-warning'>-----</span>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($allow["allow_4"]==1){ ?>
                                <a href="index.php?pg=Vpess_duplicadas_d&aca=falsa_duplicata&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa">
                                    Alterar
                                </a>
                                <?php
                            }else{
                                echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($allow["allow_4"]==1){ ?>
                                <a href="index.php?pg=Vp_mesclar_et1&id=<?php echo $id; ?>" title="Mesclar pessoas" class="fas fa-magnet"></a>
                                <?php
                            }else{
                                echo "<i class='fa fa-ban' title='você não tem permissão pra mesclar'></i>";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>