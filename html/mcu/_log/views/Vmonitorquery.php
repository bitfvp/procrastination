<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
    }
}


$page = "Log de consultas-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<script type="text/javascript">
   $.ajaxSetup({ cache: false });
   $(document).ready(function() {
       $('#frase').load('includes/monitorquery.php');
   });
   $(document).ready(function() {
           setInterval(function () {
               $('#frase').load('includes/monitorquery.php')
           }, 2000);
       });
</script>

<main class="container"><!--todo conteudo-->

<div class="row">
    <div class="col-md-12">
        <a href="?pg=Vmonitorquery&aca=resetmonitor" class="btn btn-danger">RESET</a>
        <a href="?pg=Vmonitorquery&aca=ativamonitor" class="btn btn-info">Ativa/Desativa</a>
    </div>
    <div class="col-md-12">
        <div id="frase"></div>
    </div>
</div>


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>