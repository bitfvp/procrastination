<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Mesclar Pessoa et1-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
    <main class="container">
        <div class="row">
            <div class="col-md-4">
                <?php include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php");
                echo verifica_registro($_GET['id']);
                ?>
            </div>
            <div class="col-md-4">
                <h3 class="text-danger">Mesclar cadastros iguais</h3>
                <h5>Se você está nessa tela significa que encontrou um registro duplicado desta pessoa e deseja combina-los.  </h5>
                <h6 class="text-info">Preencha o código do cadastro repetido</h6>
                <form action="index.php?" method="get">

                    <div class="form-group">
                        <input id="pg" type="hidden" class="form-control" name="pg" value="Vp_mesclar_et2"/>
                        <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $_GET['id']; ?>"/>
                        <input id="id2" type="number" class="form-control" name="id2" value="" placeholder="Código duplicado" required autofocus/>
                    </div>

                    <input type="submit" class="btn btn-lg btn-success btn-block mb-2 fa-forward" value="CONTINUAR >>"/>
                </form>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>