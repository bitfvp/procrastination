<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Mesclar Pessoa et2-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (!isset($_GET['id']) or !is_numeric($_GET['id']) or !isset($_GET['id2']) or !is_numeric($_GET['id2'])){
    $_SESSION['fsh']=[
        "flash"=>"Houve um erro por falta de ids",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
    <main class="container">
        <div class="row">
            <div class="col-md-4">
                <?php
                include_once("{$env->env_root}includes/mcu/pessoacabecalhoside.php");
                echo verifica_registro($_GET['id']);
                ?>
            </div>
            <div class="col-md-4">
                <h3 class="text-danger">Atenção</h3>
                <h5>Verifique os registros atrelados aos cadastros e prossiga se estiver de acordo.  </h5>
                <h6 class="text-danger animated pulse infinite">Todos os registros da direita será atrelados a esquerda e o cadastro duplicado será apagado</h6>

                <a href="index.php?pg=Vp_mesclar_et2&id=<?php echo $_GET['id']; ?>&id2=<?php echo $_GET['id2']; ?>&aca=mesclar_cadastros" class="btn btn-lg btn-block btn-outline-danger fas fa-forward"> Continuar</a>
                <a href="index.php?pg=Vp_mesclar_et1&id=<?php echo $_GET['id'];?>" class="btn btn-lg btn-success btn-block mb-2">Cancelar</a>

            </div>
            <div class="col-md-4">
                <?php
                $_GET['id']=$_GET['id2'];
                include("{$env->env_root}includes/mcu/pessoacabecalhoside.php");
                echo verifica_registro($_GET['id2']);
                ?>
            </div>
        </div>
    </main>


<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>