<?php
function fnccursolist(){
    $sql = "SELECT * FROM mcu_curso ORDER BY curso";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cursolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cursolista;
}

function fncgetcurso($id){
    $sql = "SELECT * FROM mcu_curso WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcurso = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcurso;
}
?>