<?php

    if(isset($_SESSION['curso_id'])) {
        $idpessoa =$_SESSION['curso_id'];
        //existe um id e se ele é numérico
        if (!empty($idpessoa) && is_numeric($idpessoa)) {
            // Captura os dados do cliente solicitado
            $sql = "SELECT * FROM mcu_curso_inscrito WHERE pessoa=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $idpessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $array_incricao = $consulta->fetchAll();

            $sql=null;
            $consulta=null;
        }
    }
?>