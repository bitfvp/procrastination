<?php
$page="Login-".$env->env_titulo;
$css="style2";

if(isset($_SESSION['curso_logado'])){
    header("Location: {$env->env_url_mod}");
    exit();
}
include_once("{$env->env_root}includes/head.php");
?>

<div class="container">
    <div class="panel-title text-center">
        <a href="<?php echo $env->env_url_mod;?>">
            <img src="<?php echo $env->env_estatico; ?>img/syssocial1.png" alt="" title="<?php echo $env->env_nome; ?>"/>
        </a>
        <h2>Cursos</h2>
    </div>
    <br>
    <div class="login-wrap">
        <h2>Login</h2>
        <div class="form">
            <form action="index.php?pg=VloginCurso&aca=logar_curso" method="post" autocomplete="off">
                <input autofocus type="text" name="cpf" id="cpf"  placeholder="CPF"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
                <button type="submit" > ENTRAR </button>
            </form>
        </div>
    </div>
</div> <!-- /container -->

<div id='stars'></div>
<div id='stars2'></div>
<!--<div id='stars3'></div>-->


<?php
$oculta_online=1;//variavel setada para ocultar contatos online se tiver logado
include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>