<?php
if (!isset($_SESSION["curso_logado"]) and $_SESSION["curso_logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url_mod}?pg=VloginCurso");
    exit();
}

$page="Home-".$env->env_titulo;
$css="stylecurso";

include_once("{$env->env_root}includes/head.php");

if(isset($_SESSION['curso_id'])) {
    $idpessoa = $_SESSION['curso_id'];
    $sql = "SELECT * FROM mcu_curso_inscrito WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $idpessoa);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $array_inscricoes = $consulta->fetchAll();

    $sql = null;
    $consulta = null;
}else{
    header("Location: {$env->env_url_mod}?pg=VloginCurso?aca=logout");
    exit();
}
?>
<main class="container">
    <div class="row mt-4 mb-5">
        <div class="col-md-9">
            <a href="#">
                <img src="conteudo/telecentro.png" alt="..." class="img-fluid">
                <!--850x1300-->
            </a>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Cursos disponíveis
                </div>
                <div class="card-body">
                    <section class="sidebar-offcanvas" id="sidebar">
                        <div class="list-group">
                            <?php
                            $arr=$array_inscricoes;
                            if ($arr==null){
                                echo "<h5 class='text-danger'>Não há cursos habilitados para você</h5>";
                            }else{
                                foreach ($array_inscricoes as $item){
                                    echo "<a href='?pg=Vcurso&c={$item['curso']}' class='list-group-item'>".fncgetcurso($item['curso'])['curso']."</a>";
                                }
                            }
                            ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("includes/footer.php"); ?>
</body>
</html>