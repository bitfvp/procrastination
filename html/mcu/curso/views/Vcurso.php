<?php
if (!isset($_SESSION["curso_logado"]) and $_SESSION["curso_logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url_mod}?pg=VloginCurso");
    exit();
}

if(!isset($_SESSION['curso_id'])) {//se nao tiver o id do camarada da ruim
    header("Location: {$env->env_url_mod}?pg=VloginCurso?aca=logout");
    exit();
}

$page="Curso-".$env->env_titulo;
$css="stylecurso";

include_once("{$env->env_root}includes/head.php");

if (!isset($_GET['c']) or !is_numeric($_GET['c'])){
    header("Location: {$env->env_url_mod}");
    exit();
}else{
    $curso=$_GET['c'];
}

$idpessoa = $_SESSION['curso_id'];
$sql = "SELECT * FROM mcu_curso_inscrito WHERE pessoa=? and curso=?";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1, $idpessoa);
$consulta->bindParam(2, $curso);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$inscricao = $consulta->fetch();
$sql = null;
$consulta = null;
?>
<main class="container"><!--todo conteudo-->

<div class="row mt-4 mb-5">
	<div class="col-md-9">
        <?php

        if (!isset($_GET['st']) or !is_numeric($_GET['st'])){
            $cursostep=$etapa=$inscricao['etapa'];
        }else{
            $cursostep=$_GET['st'];
            $etapa=$inscricao['etapa'];
        }

        if ($_GET['st']>$etapa){
            header("Location: index.php?pg=Vcurso&c={$curso}&st={$etapa}");
            exit();
        }

        $sql = "SELECT * FROM mcu_curso_etapa WHERE curso=? and ordem=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $curso);
        $consulta->bindParam(2, $cursostep);
        $consulta->execute();
        global $LQ;$LQ->fnclogquery($sql);
        $etapa = $consulta->fetch();
        $sql = null;
        $consulta = null;

        echo "<a href='#'>";
        echo "<img src='".$etapa['imagem']."' alt='...' class='img-fluid'>";
        echo "</a>";
        ?>
	</div>

	<div class="col-md-3">
        <?php include_once("includes/sectionmenulateral.php");?>
	</div>
	
</div>
</main><!--fim de conteiner-->
<?php include_once("includes/footer_curso.php"); ?>
</body>
</html>