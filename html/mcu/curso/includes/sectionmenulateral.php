<div class="card">
    <div class="card-header bg-info text-light text-center">
            <?php
            echo "Curso de ".fncgetcurso($curso)['curso'];
            ?>
    </div>
    <div class="card-body">
        <style>
            #list-group-sm {
                padding: 5px 15px;
            }
        </style>
        <div class="list-group">
            <?php
            $sql="select id,ordem,titulo from mcu_curso_etapa where curso=? order by ordem";
            $step=$inscricao['etapa'];
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $curso);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $array_curso = $consulta->fetchall();
            $array_cont=$consulta->rowCount();

            if ($cursostep>$array_cont){
                header("Location: {$env->env_url_mod}");
                exit();
            }

            $sql=null;
            $consulta=null;
            foreach ($array_curso as $arr){
                if ($arr['id']<=$step){
                echo "<a href='index.php?pg=Vcurso&c={$curso}&st={$arr['ordem']}' id='list-group-sm' class='list-group-item'>".$arr['titulo']."</a>";
                }
            }
            ?>
        </div>
    </div>
</div>