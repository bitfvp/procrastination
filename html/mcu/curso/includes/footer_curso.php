<nav id="navbar" class="navbar fixed-bottom navbar-expand-lg navbar-dark bg-dark">
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" src="<?php echo $env->env_estatico; ?>img/syssocial2.png" alt="<?php echo $env->env__nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <form class="navbar-form navbar-left" action="index.php?pg=Vcurso&c=<?php echo $_GET['c'];?>&st=<?php echo $cursostep;?>&aca=next_step" method="post" autocomplete="off">
                    <div class="input-group">
                        <?php

                        $sql="select tipo from mcu_curso_etapa where curso=? and ordem=?";
                        $sql.="";
                        $step=$incricao['etapa'];
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindParam(1,$curso);
                        $consulta->bindParam(2,$cursostep);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $var_tipo = $consulta->fetch();

                        ?>

                        <input type="<?php if ($var_tipo[0]==1){echo "hidden";}else{echo "password";} ?>" autocomplete="off" class="form-control" placeholder="Código do monitor..." name="codigo" value=""/>
                        <span class="input-group-btn">
                            <button class="btn btn-success form-control" type="submit"><i class="fa fa-forward"></i></button>
                            </span>
                    </div>
                </form>
            </ul>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        Olá
                        <?php
                        $primeiroNome = explode(" ", $_SESSION["curso_nome"]);
                        echo $primeiroNome[0]; // Fulano
                        echo", ".Comprimentar();
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown dropup">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</nav>
<style>
    .navbar-brand>img {
        height: 40px;
    }
</style>