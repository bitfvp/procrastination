<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Filtro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-12">
            <script type="text/javascript">
                function ch_c() {
                    var butcu = document.getElementById("btncu");
                    var butli = document.getElementById("btnli");
                    //butcu.hidden= true;
                    butcu.className='btn hidden';
                    //butli.hidden= false;
                    butli.className='btn';
//                var butchange = document.getElementById("change");
//                butchange.value = "Vcurriculos";
                    document.form1.action = "?pg=Vcurriculos";
                    //document.form1.submit();
                }

                function ch_l() {
                    var butcu = document.getElementById("btncu");
                    var butli = document.getElementById("btnli");
                    //butcu.hidden= false;
                    butcu.className='btn';
                    //butli.hidden= true;
                    butli.className='btn hidden';
//                var butchange = document.getElementById("change");
//                butchange.value = "Vbusca";
                    document.form1.action = "?pg=Vbusca&avancado=1";
                    //document.form1.submit();
                }
            </script>
            <?php
            echo "<button class='btn hidden' id=\"btncu\" onclick=\"ch_c()\" >Click para Mudar Para Curriculo</button>\n";
            echo "<button class='btn ml-1' id=\"btnli\" onclick=\"ch_l()\" >Click para Mudar Para Lista</button>\n";
            ?>
        </div>
    </div>

    <form name="form1" class="form-horizontal" method="post" action="?pg=Vcurriculos&avancado=1"  target="_blank">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class=" control-label" for="sexo">Sexo</label>
                <div class="">
                    <div class="radio">
                        <label for="sexo-0">
                            <input type="radio" name="sexo" id="sexo" value="" checked="checked">
                            Todos
                        </label>
                    </div>
                    <div class="radio">
                        <label for="sexo-1">
                            <input type="radio" name="sexo" id="sexo" value="1">
                            Feminino
                        </label>
                    </div>
                    <div class="radio">
                        <label for="sexo-2">
                            <input type="radio" name="sexo" id="sexo" value="2">
                            Masculino
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="form-group">
                <label class=" control-label" for="cnh">CNH</label>
                <div class="">
                    <div class="radio">
                        <label for="cnh-0">
                            <input type="radio" name="cnh" id="cnh" value="" checked="checked">
                            Todos
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-1">
                            <input type="radio" name="cnh" id="cnh" value="A">
                            A
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-2">
                            <input type="radio" name="cnh" id="cnh" value="B">
                            B
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-3">
                            <input type="radio" name="cnh" id="cnh" value="C">
                            C
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-4">
                            <input type="radio" name="cnh" id="cnh" value="D">
                            D
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-5">
                            <input type="radio" name="cnh" id="cnh" value="E">
                            E
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-6">
                            <input type="radio" name="cnh" id="cnh" value="AB">
                            AB
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-7">
                            <input type="radio" name="cnh" id="cnh" value="AC">
                            AC
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-8">
                            <input type="radio" name="cnh" id="cnh" value="AD">
                            AD
                        </label>
                    </div>
                    <div class="radio">
                        <label for="cnh-9">
                            <input type="radio" name="cnh" id="cnh" value="AE">
                            AE
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <h5><strong>Formação Específica:</strong></h5>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pcd" value="1" > PCD
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="alfabetizado" value="1" > Alfabetizado
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="a1a4" value="1" > 1ª a 4º
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="a5a8" value="1" > 5ª a 8ª
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ensino_medio" value="1" > Ensino Médio
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="superior" value="1" > Ensino Superior
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="administracao" value="1" > Administração
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="contabilidade" value="1" > Contabilidade
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="psicologia" value="1" > Psicologia
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servico_social" value="1" > Serviço Social
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="sistema_informacao" value="1" > Informática
                </label>
            </div>
        </div>

        <div class="col-md-3">
            <h5><strong>Experiencia Especifica:</strong></h5>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="primeiro_emprego" value="1" > Primeiro Emprego
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="assistente_administrativo" value="1" > Assistente Administrativo
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="assistente_financeiro" value="1" > Assistente Financeiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="atendimento_balconista" value="1" > Atendimento Balconista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="auxiliar_cozinha" value="1" > Auxiliar Cozinha
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="auxiliar_producao" value="1" > Auxiliar Produção
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="baba" value="1" > Babá
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="bombeiro" value="1" > Bombeiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cabelereira" value="1" > Cabelereira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="camareira" value="1" > Camareira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="changueiro" value="1" > Changueiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="copeira" value="1" > Copeira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cozinheiro" value="1" > Cozinheiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cuidadora" value="1" > Cuidadora
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="decorador" value="1" > Decorador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="depiladora" value="1" > Depiladora
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="detetizacao" value="1" > Dedetização
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="domestica" value="1" > Doméstica
                </label>
            </div>
        </div>

        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="eletricista" value="1" > Eletricista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="embalador" value="1" > Embalador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="encanador" value="1" > Encanador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="estagiario" value="1" > Estagiário
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="esteticista" value="1" > Esteticista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="faxineira_diarista" value="1" > Faxineira Diarista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="fiscal_loja" value="1" > Fiscal Loja
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="frentista" value="1" > Frentista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="garcon" value="1" > Garçom
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="gerente_loja" value="1" > Gerente Loja
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="jardinagem" value="1" > Jardinagem
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="lavadeira" value="1" > Lavadeira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="limpador_de_piscina" value="1" > Limpador de Piscina
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="marcineiro" value="1" > Marcineiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="manicure" value="1" > Manicure
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="massagista" value="1" > Massagista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="mecanico" value="1" > Mecanico
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="monitor" value="1" > Monitor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="montador_moveis" value="1" > Montador de Moveis
                </label>
            </div>
        </div>

        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="motoboy" value="1" > Motoboy
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="motorista" value="1" > Motorista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="operador_caixa" value="1" > Operador Caixa
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="passadeira" value="1" > Passadeira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pedreiro" value="1" > Pedreiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pintor" value="1" > Pintor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="porteiro" value="1" > Porteiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="recepcionista" value="1" > Recepcionista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="safrista" value="1" > Safrista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="secretaria" value="1" > Secretaria
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="seguranca" value="1" > Segurança
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servente_pedreiro" value="1" > servente de Pedreiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servico_gerais" value="1" > Serviços Gerais
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="supervisor" value="1" > Supervisor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="telefonista" value="1" > Telefonista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vendedor" value="1" > Vendedor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vigia" value="1" > Vigia
                </label>
            </div>
        </div>

        <div class="col-md-12">
            <button type="submit" class="btn btn-info btn-block">Gerar</button>
        </div>

    </div>
    </form>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>