<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Empresa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="empresasave";
    $empresa=fncgetempresa($_GET['id']);
}else{
    $a="empresanew";
}
?>
<main class="container"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Cadastro de Empresas</h3>
    <form class="form-signin" action="<?php echo "index.php?pg=Vempresa_lista&sc=&aca={$a}";?>" method="post">
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $empresa['id']; ?>"/>
            <label for="razao_social">Razão Social</label>
            <input autocomplete="off" autofocus id="empresa" placeholder="Razão Social" type="text"
                   class="form-control" required="true" name="empresa"
                   value="<?php echo $empresa['empresa']; ?>"/>

            <label for="">Endereço</label>
            <input type="text" name="endereco" value="<?php echo $empresa['endereco']; ?>" title="endereco da empresa"
                   class="form-control" placeholder="endereco"/>
            <label for="">Contato</label>
            <input type="text" name="contato" value="<?php echo $empresa['contato']; ?>"
                   title="Contato" class="form-control" placeholder="contato"/>
            <hr>
            <input type="submit" name="" value="Salvar" class="btn btn-primary"/>
    </form>
    <br/>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>