<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Vaga -".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
     $vaga=fncgetvaga($_GET['id']);
}else{
    echo "houve um erro";
}
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <!-- direito -->
        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados da vaga</h3>
                </div>
                <blockquote>
                    <header>
                        Vaga:<strong class="text-success"><?php echo $vaga['vaga']; ?>&nbsp;&nbsp;</strong>
                    </header>
                    <p>
                    <h5>
                        Data:
                        <strong class="text-info"><?php echo datahoraBanco2data($vaga['data']); ?>&nbsp;&nbsp;</strong>
                        Descrição:
                        <strong class="text-info"><?php echo $vaga['descricao']; ?>&nbsp;&nbsp;</strong>
                        Empresa:
                        <strong class="text-info">
                            <?php
                            if ($vaga['empresa'] != "0") {
                                $sql = "SELECT empresa FROM mcu_sysjob_empresas WHERE id={$vaga['empresa']}";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                $elista = $consulta->fetch();
                                $sql = null;
                                $consulta = null;
                                echo $elista[0];
                            } else {
                                echo "<span class='vermelho underline'>";
                                echo "???????";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;&nbsp;
                        <br>
                        Filtros:<br>
                        <strong class="text-warning">
                            <?php
                            $cc=0;
                            foreach ($vaga as $vg  => $value){
                                $cc++;
                                if (($cc%2!=0) and ($value>0 or $value>"A") and ($cc>10)){
                                    echo "{$vg}<br> ";

                                }
                            }
                            ?>&nbsp;&nbsp;
                        </strong>
                    </h5>
                    </p>
                </blockquote>

                <a class="btn btn-success btn-block"
                   href="?pg=Vvaga_editar&id=<?php echo $_GET['id']; ?>"
                   title="Edite vaga">
                    EDITAR VAGA
                </a>
            </div>
            <!-- fim da col md 4 -->
        </div>


        <!-- esquerdo -->
        <div class="col-md-8">
            <div class="row-fluid">
                <!--  -->
                <div class="col-md-12 bg-muted">
                    <h3 class="text-center">Lista decruzamento de vaga</h3>

                    <?php

                    $sql = "SELECT * FROM `mcu_sysjob_pessoas` WHERE ";
                    //
                    $cc=0;
                    foreach ($vaga as $gt  => $value){
                        $cc++;
                        if ($cc%2!=0 and $cc>10){
                            if ($cc>11){
                                $sql .= "AND";
                            }
                            if ($value==0 and is_numeric($value)){$value="";}
                            $sql .= " `{$gt}` LIKE '%{$value}%' ";
                        }
                    }
//echo $sql;
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $peslista = $cons->fetchAll();
                    $sql = null;
                    $consulta = null;
                    ?>
                    <!-- tabela -->
                    <table id="tabela" class="table table-bordered table-hover">
                        <br>
                        <thead class="bg-primary">
                        <tr>
                            <th>Nome</th>
                            <th>Nascimento</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // vamos criar a visualização
                        foreach ($peslista as $dados) {
                            $id = $dados["id"];
                            $nome = $dados["nome"];
                            $sexo = $dados["sexo"];
                            $nascimento = dataBanco2data($dados["nascimento"]);
                            ?>

                            <tr class="<?php
                            if ($sexo == 1) {
                                echo "bg-warning";
                            }
                            if ($sexo == 2) {
                                echo "";
                            }
                            ?>">
                                <td>
                                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                                        <?php
                                        echo $nome;
                                        if ($sexo == 1) {
                                            echo "    <i class=\"fa fa-venus\"></i>";
                                        }
                                        if ($sexo == 2) {
                                            echo "    <i class=\"fa fa-mars\"></i>";
                                        }
                                        ?>
                                    </a>
                                </td>
                                <td><?php echo $nascimento; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>