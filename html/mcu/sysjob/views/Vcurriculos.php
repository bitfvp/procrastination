<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Curriculos-".$env->env_titulo;
$css="curriculo";

include_once("{$env->env_root}includes/head.php");

// Captura os dados do cliente solicitado
$sql = "SELECT * FROM `mcu_sysjob_pessoas` WHERE ";
//
$cc=0;
foreach ($_POST as $gt  => $value){
    $cc++;
    if ($cc>1){
        $sql .= "AND";
    }
    $sql .= " `{$gt}` LIKE '%{$value}%' ";
}
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$pss = $consulta->fetchall();

$sql = null;
$consulta = null;

?>
<div class="container">
    <?php
    foreach ($pss as $pessoa) {
        ?>
        <h1 class="text-dark"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</h1>

        <h4>
            <strong class="text-dark">
                <?php
                if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                    echo Calculo_Idade($pessoa['nascimento'])." anos";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>,
            </strong>
            <?php
            echo "<strong class='text-dark'>";
            if ($pessoa['estado_civil'] == 0) {
                echo "Solteiro(a)";
            }
            if ($pessoa['estado_civil'] == 1) {
                echo "Casado(a)";
            }
            if ($pessoa['estado_civil'] == 2) {
                echo "Divorciado(a)";
            }
            if ($pessoa['estado_civil'] == 3) {
                echo "Uniao Estavel";
            }
            if ($pessoa['estado_civil'] == 4) {
                echo "Viuvo(a)";
            }
            echo "</strong>, ";

            if ($pessoa['sexo'] != "" and $pessoa['sexo'] != "0") {
                echo "<strong class='text-dark'>";
                if ($pessoa['sexo'] == 0) {
                    echo "Selecione...";
                }
                if ($pessoa['sexo'] == 1) {
                    echo '<i class="fa fa-venus">Feminino</i>';
                }
                if ($pessoa['sexo'] == 2) {
                    echo '<i class="fa fa-mars"> Masculino</i>';
                }
                if ($pessoa['sexo'] == 3) {
                    echo '<i class="fa fa-venus-mars"> </i>';
                }
                echo "</strong>";
            } else {
                echo "<span class='text-danger'>";
                echo "???????";
                echo "</span>";
            }
            echo "<br>";
            if ($pessoa['cpf'] != "0") {
                echo "CPF:<strong class='text-dark'>{$pessoa['cpf']}</strong>&nbsp;&nbsp;";
            }
            if ($pessoa['rg'] != "0") {
                echo "RG:<strong class='text-dark'>{$pessoa['rg']}</strong>&nbsp;&nbsp;";
            }
            if ($pessoa['cnh'] != "0") {
                echo "CNH:<strong class='text-dark'>{$pessoa['cnh']}</strong>&nbsp;&nbsp;";
            }
            if ($pessoa['ctps'] != "0") {
                echo "CTPS:<strong class='text-dark'>{$pessoa['ctps']}</strong>&nbsp;&nbsp;";
            }
            ?>

        </h4>
        <hr>
        <h3><i class="fa fa-map-marker"></i> Endereço</h3>
        <h4>
            <strong class="text-dark"><?php echo $pessoa['endereco']; ?></strong>&nbsp;&nbsp;
            N°:
            <strong class="text-dark"><?php echo $pessoa['numero']; ?></strong>&nbsp;&nbsp;
            Bairro:
            <strong class="text-dark">
                <?php echo $pessoa['bairro']; ?>
            </strong>&nbsp;&nbsp;
            Cidade:
            <strong class="text-dark">
                <?php echo $pessoa['cidade']; ?>
            </strong>&nbsp;
        </h4>
    <?php
    if ($pessoa['referencia'] != "0") {
        echo "<h4>
                Referência:
                <strong class='text-dark'>
                    {$pessoa['referencia']}
                </strong>&nbsp;&nbsp;
            </h4>";
    }
    echo "<hr>";
?>
        <h3><i class="fa fa-phone "></i> Telefone</h3>
        <h4>

            <strong class="text-dark"><?php echo $pessoa['telefone1']; ?>&nbsp;&nbsp;
            </strong>
            <?php
            if ($pessoa['telefone2'] != "0") {
                echo "<br>
                <strong class='text-dark'>{$pessoa['telefone2']}&nbsp;&nbsp;
                </strong>";
            }
            ?>
        </h4>
        <hr>
<?php
    if ($pessoa['email'] != "") {
        echo "<h3><i class='fa fa-at'></i> E-Mail</h3>
            <h4>
                <strong class='text-dark'>{$pessoa['email']}&nbsp;&nbsp;
                </strong>
            </h4>

            <hr>";
    }
    ?>




        <h3><i class="fa fa-check-circle"></i> Objetivo</h3>
        <h4>
            <strong class="text-dark"><?php echo $pessoa['objetivo']; ?>&nbsp;&nbsp;
            </strong>
        </h4>

        <hr>


        <h3><i class="fa fa-user-graduate"></i> Formação</h3>
    <?php
    $sql = "SELECT * FROM mcu_sysjob_formacao WHERE pessoa_id=? ORDER by periodo desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $pessoa['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $formacaolista = $consulta->fetchall();
    $sql = null;
    $consulta = null;


    foreach ($formacaolista as $flist){
    echo "<div>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$flist['titulo']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$flist['descricao']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$flist['escola']}&nbsp;&nbsp;</strong>";
    echo "<strong class='text-dark'>{$flist['periodo']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "<h4>";
    echo "<strong class='text-dark'>";
    if ($flist['concluido'] == 1) {
        echo "Concluído";
    } else {
        echo "Incompleto";
    }
    echo "&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "</div>";
    echo "<hr class='hrfino'>\n";
    ?>
        <script type="text/javascript">
            function bqf<?php echo $flist['id']?>() {
                var div = document.getElementById("qbf<?php echo $flist['id']?>");
                div.innerHTML = "<div class='quebra'></div>";
            }
        </script>
    <?php
    echo "<button id='btnEnviar' onclick='bqf{$flist['id']}()' >Inserir Quebra</button>\n";
    echo "<div id='qbf{$flist['id']}'></div>\n";
    }
    ?>

        <hr>


    <?php
    if ($pessoa['primeiro_emprego'] != "1"){
    echo "<h3><i class='fa fa-star'></i> Histórico profissional</h3>";
    $sql = "SELECT * FROM mcu_sysjob_experiencia WHERE pessoa_id=? ORDER by periodo desc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $pessoa['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $experiencialista = $consulta->fetchall();
    $sql = null;
    $consulta = null;

    foreach ($experiencialista as $elist){
    echo "<div>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$elist['empresa']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$elist['cargo']}&nbsp;&nbsp;</strong>";
    echo "<strong class='text-dark'>{$elist['periodo']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "<h4>";
    echo "<strong class='text-dark'>{$elist['descricao']}&nbsp;&nbsp;</strong>";
    echo "</h4>";
    echo "</div>";
    echo "<hr class='hrfino'>";
    ?>
        <script type="text/javascript">
            function bqe<?php echo $elist['id']?>() {
                var div = document.getElementById("qbe<?php echo $elist['id']?>");
                div.innerHTML = "<div class='quebra'></div>";
            }
        </script>
    <?php
    echo "<button id='btnEnviar' onclick='bqe{$elist['id']}()' >Inserir Quebra</button>\n";
    echo "<div id='qbe{$elist['id']}'></div>\n";
    }
    echo "<hr>";
    }//fim do if $pessoa['primeiro_emprego']!="1"
    ?>


        <h3><i class="fa fa-asterisk"></i> Outros pontos</h3>
        <h4>
            Último salário:
            <strong class="text-dark"><?php echo $pessoa['ultimo_salario']; ?>&nbsp;&nbsp;
            </strong>

            Pretensão salarial:
            <strong class="text-dark"><?php echo $pessoa['pretensao_salarial']; ?>&nbsp;&nbsp;
            </strong>
        </h4>
    <?php
    if ($pessoa['informacoes_complementares'] != "0") {
        echo "<hr>
            <h3><i class='fa fa-info-circle'></i> Informações complementares</h3>
            <h4>
                <strong class='text-dark'>
                    {$pessoa['informacoes_complementares']}&nbsp;&nbsp;
                </strong>
            </h4>";
    }
    ?>
        <br>
        <div class="float-right"><h6>FlavioW<i class="fa fa-cogs"></i>rks</h6></div>
        <script type="text/javascript">
            function bq<?php echo $pessoa['id']?>() {
                var div = document.getElementById("qb<?php echo $pessoa['id']?>");
                div.innerHTML = "<div class='quebra'></div>";
            }
        </script>
        <?php
        echo "<button id='btnEnviar' onclick='bq{$pessoa['id']}()' >Inserir Quebra</button>\n";
        echo "<div id='qb{$pessoa['id']}'><div class='quebra'></div></div>\n";


    }
    ?>
</div>

</body>
</html>