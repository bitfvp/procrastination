<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>

<main class="container"><!--todo conteudo-->
        <form class="form-signin" action="<?php echo "index.php?pg=Vbusca&aca={$a}";?>" method="post">
            <div class="row">
                <div class="col-md-6">
                    <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $pessoa['id']; ?>"/>
                    <label for="nome">NOME:</label>
                    <input autocomplete="off" id="nome" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>"/>
                </div>
                <div class="col-md-3">
                    <label for="nascimento">NASCIMENTO:</label>
                    <input id="nascimento" type="date" class="form-control input-sm" name="nascimento" value="<?php echo $pessoa['nascimento'];?>"/>
                </div>

                <div class="col-md-2">
                    <label for="sexo">SEXO:</label>
                    <select name="sexo" id="sexo" class="form-control input-sm">// vamos criar a visualização de sexo
                        <option selected="" value="<?php if ($pessoa['sexo'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pessoa['sexo'];
                        } ?>">
                            <?php
                            if ($pessoa['sexo'] == 0) {
                                echo "Selecione...";
                            }
                            if ($pessoa['sexo'] == 1) {
                                echo "Feminino";
                            }
                            if ($pessoa['sexo'] == 2) {
                                echo "Masculino";
                            }
                            if ($pessoa['sexo'] == 3) {
                                echo "Indefinido";
                            }
                            ?>
                        </option>
                        <option value="0">Selecione...</option>
                        <option value="1">Feminino</option>
                        <option value="2">Masculino</option>
                        <option value="3">Indefinido</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <label for="estado_civil">ESTADO CIVIL:</label>
                    <select name="estado_civil" id="estado_civil" class="form-control input-sm">
                        // vamos criar a visualização
                        <option selected="" value="<?php if ($pessoa['estado_civil'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pessoa['estado_civil'];
                        } ?>">
                            <?php
                            if ($pessoa['estado_civil'] == 0) {
                                echo "Solteiro(a)";
                            }
                            if ($pessoa['estado_civil'] == 1) {
                                echo "Casado(a)";
                            }
                            if ($pessoa['estado_civil'] == 2) {
                                echo "Divorciado(a)";
                            }
                            if ($pessoa['estado_civil'] == 3) {
                                echo "Uniao Estavel";
                            }
                            if ($pessoa['estado_civil'] == 4) {
                                echo "Viuvo(a)";
                            }
                            ?>
                        </option>
                        <option value="0">Solteiro(a)</option>
                        <option value="1">Casado(a)</option>
                        <option value="2">Divorciado(a)</option>
                        <option value="3">Uniao Estavel</option>
                        <option value="4">Viuvo(a)</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label for="cpf">CPF:</label>
                    <input autocomplete="off" id="cpf" type="text" class="form-control input-sm" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                </div>

                <div class="col-md-3">
                    <label for="rg">RG:</label>
                    <input autocomplete="off" id="rg" type="text" class="form-control input-sm" name="rg" value="<?php echo $pessoa['rg']; ?>"/>
                    <script>
                        $(document).ready(function(){
                            $('#rg').mask('00.000.000.000', {reverse: true});
                        });
                    </script>
                </div>

                <div class="col-md-3">
                    <label for="cnh">CNH:</label>
                    <select name="cnh" id="cnh" class="form-control input-sm">

                        <option selected="" value="<?php if ($pessoa['cnh'] == "") {
                            $z = 0;
                            echo $z;
                        } else {
                            echo $pessoa['cnh'];
                        }
                        echo "\">";
                        echo $pessoa['cnh'];
                        ?>
                        </option>
                        <option value="0">Nenhum</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="AB">AB</option>
                        <option value="AC">AC</option>
                        <option value="AD">AD</option>
                        <option value="AE">AE</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <label for="cpts">CTPS:</label>
                    <input autocomplete="off" id="cpts" type="text" class="form-control input-sm" name="ctps" value="<?php echo $pessoa['ctps']; ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="endereco">ENDEREÇO:</label>
                    <input autocomplete="off" id="endereco" type="text" class="form-control input-sm" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
                </div>
                <div class="col-md-2">
                    <label for="numero">NÚMERO:</label>
                    <input id="numero" type="number" autocomplete="off" class="form-control input-sm" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
                </div>
                <div class="col-md-3">
                    <label for="bairro">BAIRRO:</label>
                    <input id="bairro" type="text" autocomplete="off" class="form-control input-sm" name="bairro" value="<?php echo $pessoa['bairro']; ?>" />
                </div>

                <div class="col-md-3">
                    <label for="cidade">CIDADE:</label>
                    <input id="cidade" type="text" autocomplete="off" class="form-control input-sm" name="cidade" value="<?php echo $pessoa['cidade']; ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-11">
                    <label for="referencia">REFERÊNCIA:</label>
                    <input autocomplete="off" id="referencia" type="text" class="form-control input-sm" name="referencia" value="<?php echo $pessoa['referencia']; ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="email">E-MAIL:</label>
                    <input autocomplete="off" id="email" type="email" class="form-control input-sm" name="email" value="<?php echo $pessoa['email']; ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="telefone1">CELULARES:</label>
                    <input autocomplete="off" id="telefone1" type="text" class="form-control input-sm" name="telefone1" value="<?php echo $pessoa['telefone1']; ?>"/>
                </div>

                <div class="col-md-6">
                    <label for="telefone2">TELEFONE FIXO:</label>
                    <input autocomplete="off" id="telefone2" type="text" class="form-control input-sm" name="telefone2" value="<?php echo $pessoa['telefone2']; ?>"/>
                    <script>
                        $(document).ready(function(){
                            $('#telefone1').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                            $('#telefone2').mask('(00)0000-0000', {reverse: false});
                        });
                    </script>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="objetivo">OBJETIVO:</label>
                    <textarea id="objetivo" onkeyup="limite_textarea(this.value,1000,objetivo,'contZ')" maxlength="1000" class="form-control" rows="4" name="objetivo"><?php echo $pessoa['objetivo']; ?></textarea>
                    <span id="contZ">1000</span>/1000
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="ultimo_salario">ÚLTIMO SALÁRIO:</label>
                    <input autocomplete="off" id="ultimo_salario" type="text" class="form-control input-sm" name="ultimo_salario" value="<?php echo $pessoa['ultimo_salario']; ?>" placeholder="R$ XXX.xx ou texto"/>
                </div>

                <div class="col-md-6">
                    <label for="pretensao_salarial">PRETENSÃO SALARIAL:</label>
                    <input autocomplete="off" id="pretensao_salarial" type="text" class="form-control input-sm" name="pretensao_salarial" value="<?php echo $pessoa['pretensao_salarial']; ?>" placeholder="R$ XXX.xx ou texto"/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="informacoes_complementares">OBJETIVO:</label>
                    <textarea id="informacoes_complementares" onkeyup="limite_textarea(this.value,1000,informacoes_complementares,'contY')" maxlength="1000" class="form-control" rows="4" name="informacoes_complementares"><?php echo $pessoa['informacoes_complementares']; ?></textarea>
                    <span id="contY">1000</span>/1000
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <h5><strong>Formação Especifica:</strong></h5>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pcd" value="1" <?php if ($pessoa['pcd'] == 1) {echo "checked";}?>> PCD
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="alfabetizado" value="1" <?php if ($pessoa['alfabetizado'] == 1) {echo "checked";}?>> Alfabetizado
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="a1a4" value="1" <?php if ($pessoa['a1a4'] == 1) {echo "checked";}?>> 1° a 4° Serie
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="a5a8" value="1" <?php if ($pessoa['a5a8'] == 1) {echo "checked";}?>> 5° a 8° Serie
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="ensino_medio" value="1" <?php if ($pessoa['ensino_medio'] == 1) {echo "checked";}?>> Ensino Medio
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="superior" value="1" <?php if ($pessoa['superior'] == 1) {echo "checked";}?>> Ensino Superior
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="administracao" value="1" <?php if ($pessoa['administracao'] == 1) {echo "checked";}?>> Administração
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="contabilidade" value="1" <?php if ($pessoa['contabilidade'] == 1) {echo "checked";}?>> Contabilidade
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="psicologia" value="1" <?php if ($pessoa['psicologia'] == 1) {echo "checked";}?>> Psicologia
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="servico_social" value="1" <?php if ($pessoa['servico_social'] == 1) {echo "checked";}?>> Servico Social
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="sistema_informacao" value="1" <?php if ($pessoa['sistema_informacao'] == 1) {echo "checked";}?>> Informática
                        </label>
                    </div>
                </div>



                <div class="col-md-3">
                    <h5><strong>Experiencia Especifica:</strong></h5>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="primeiro_emprego" value="1" <?php if ($pessoa['primeiro_emprego'] == 1) {echo "checked";}?>> Primeiro Emprego
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="assistente_administrativo" value="1" <?php if ($pessoa['assistente_administrativo'] == 1) {echo "checked";}?>> Assistente administrativo
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="assistente_financeiro" value="1" <?php if ($pessoa['assistente_financeiro'] == 1) {echo "checked";}?>> Assistente Financeiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="atendimento_balconista" value="1" <?php if ($pessoa['atendimento_balconista'] == 1) {echo "checked";}?>> Atendimento Balconista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="auxiliar_cozinha" value="1" <?php if ($pessoa['auxiliar_cozinha'] == 1) {echo "checked";}?>> Auxiliar Cozinha
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="auxiliar_producao" value="1" <?php if ($pessoa['auxiliar_producao'] == 1) {echo "checked";}?>> Auxiliar Produção
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="baba" value="1" <?php if ($pessoa['baba'] == 1) {echo "checked";}?>> Babá
                        </label>
                    </div>


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="bombeiro" value="1" <?php if ($pessoa['bombeiro'] == 1) {echo "checked";}?>> Bombeiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="cabelereira" value="1" <?php if ($pessoa['cabelereira'] == 1) {echo "checked";}?>> Cabelereira
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="camareira" value="1" <?php if ($pessoa['camareira'] == 1) {echo "checked";}?>> Camareira
                        </label>
                    </div>


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="changueiro" value="1" <?php if ($pessoa['changueiro'] == 1) {echo "checked";}?>> Changueiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="copeira" value="1" <?php if ($pessoa['copeira'] == 1) {echo "checked";}?>> Copeira
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="cozinheiro" value="1" <?php if ($pessoa['cozinheiro'] == 1) {echo "checked";}?>> Cozinheiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="cuidadora" value="1" <?php if ($pessoa['cuidadora'] == 1) {echo "checked";}?>> Cuidadora
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="decorador" value="1" <?php if ($pessoa['decorador'] == 1) {echo "checked";}?>> Decorador
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="depiladora" value="1" <?php if ($pessoa['depiladora'] == 1) {echo "checked";}?>> Depiladora
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="detetizacao" value="1" <?php if ($pessoa['detetizacao'] == 1) {echo "checked";}?>> Detetizador
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="domestica" value="1" <?php if ($pessoa['domestica'] == 1) {echo "checked";}?>> Domestica
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="eletricista" value="1" <?php if ($pessoa['eletricista'] == 1) {echo "checked";}?>> Eletricista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="embalador" value="1" <?php if ($pessoa['embalador'] == 1) {echo "checked";}?>> Embalador
                        </label>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="encanador" value="1" <?php if ($pessoa['encanador'] == 1) {echo "checked";}?>> Encanador
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="estagiario" value="1" <?php if ($pessoa['estagiario'] == 1) {echo "checked";}?>> Estagiario
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="esteticista" value="1" <?php if ($pessoa['esteticista'] == 1) {echo "checked";}?>> Esteticista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="faxineira_diarista" value="1" <?php if ($pessoa['faxineira_diarista'] == 1) {echo "checked";}?>> Faxineira - Diarista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="fiscal_loja" value="1" <?php if ($pessoa['fiscal_loja'] == 1) {echo "checked";}?>> Fiscal em Loja
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="frentista" value="1" <?php if ($pessoa['frentista'] == 1) {echo "checked";}?>> Frentista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="garcon" value="1" <?php if ($pessoa['garcon'] == 1) {echo "checked";}?>> Garçon
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="gerente_loja" value="1" <?php if ($pessoa['gerente_loja'] == 1) {echo "checked";}?>> Gerente de Loja
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="jardinagem" value="1" <?php if ($pessoa['jardinagem'] == 1) {echo "checked";}?>> Jardinagem
                        </label>
                    </div>


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="lavadeira" value="1" <?php if ($pessoa['lavadeira'] == 1) {echo "checked";}?>> Lavadeira
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="limpador_de_piscina" value="1" <?php if ($pessoa['limpador_de_piscina'] == 1) {echo "checked";}?>> Limpador de Piscina
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="marcineiro" value="1" <?php if ($pessoa['marcineiro'] == 1) {echo "checked";}?>> Marcineiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="manicure" value="1" <?php if ($pessoa['manicure'] == 1) {echo "checked";}?>> Manicure
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="massagista value="1" <?php if ($pessoa['massagista'] == 1) {echo "checked";}?>> Massagista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="mecanico" value="1" <?php if ($pessoa['mecanico'] == 1) {echo "checked";}?>> Mecânico
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="monitor" value="1" <?php if ($pessoa['monitor'] == 1) {echo "checked";}?>> monitor
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="montador_moveis" value="1" <?php if ($pessoa['montador_moveis'] == 1) {echo "checked";}?>> Montador de Moveis
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="motoboy" value="1" <?php if ($pessoa['motoboy'] == 1) {echo "checked";}?>> Motoboy
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="motorista" value="1" <?php if ($pessoa['motorista'] == 1) {echo "checked";}?>> Motorista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="operador_caixa" value="1" <?php if ($pessoa['operador_caixa'] == 1) {echo "checked";}?>> Operador de Caixa
                        </label>
                    </div>
                </div>

                <div class="col-md-3">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="passadeira" value="1" <?php if ($pessoa['passadeira'] == 1) {echo "checked";}?>> Passadeira
                        </label>
                    </div>


                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pedreiro" value="1" <?php if ($pessoa['pedreiro'] == 1) {echo "checked";}?>> Pedreiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="pintor" value="1" <?php if ($pessoa['pintor'] == 1) {echo "checked";}?>> Pintor
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="porteiro" value="1" <?php if ($pessoa['porteiro'] == 1) {echo "checked";}?>> Porteiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="recepcionista" value="1" <?php if ($pessoa['recepcionista'] == 1) {echo "checked";}?>> Recepcionista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="safrista" value="1" <?php if ($pessoa['safrista'] == 1) {echo "checked";}?>> Safrista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="secretaria" value="1" <?php if ($pessoa['secretaria'] == 1) {echo "checked";}?>> Secretaria
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="seguranca" value="1" <?php if ($pessoa['seguranca'] == 1) {echo "checked";}?>> Seguranca
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="servente_pedreiro" value="1" <?php if ($pessoa['servente_pedreiro'] == 1) {echo "checked";}?>> Servente de Pedreiro
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="servico_gerais" value="1" <?php if ($pessoa['servico_gerais'] == 1) {echo "checked";}?>> Servico Gerais
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="supervisor" value="1" <?php if ($pessoa['supervisor'] == 1) {echo "checked";}?>> Supervisor
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="telefonista" value="1" <?php if ($pessoa['telefonista'] == 1) {echo "checked";}?>> Telefonista
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="vendedor" value="1" <?php if ($pessoa['vendedor'] == 1) {echo "checked";}?>> Vendedor
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="vigia" value="1" <?php if ($pessoa['vigia'] == 1) {echo "checked";}?>> Vigia
                        </label>
                    </div>

                </div>
            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>