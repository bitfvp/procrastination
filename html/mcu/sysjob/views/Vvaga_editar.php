<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_80"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Vaga-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="vagasave";
    $vaga=fncgetvaga($_GET['id']);
}else{
    $a="vaganew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vvaga&aca={$a}&id={$_GET['id']}"; ?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-block" value="Salvar"/>
            </div>
        </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <label for="vaga">vaga</label>
            <input autocomplete="off" required="true" autofocus id="vaga" required="yes" type="text"
                   class="form-control" name="vaga" value="<?php echo $vaga['vaga']; ?>"/>
        </div>

        <div class="col-md-12">
            <label for="descricao">descricao</label>
            <input autocomplete="off" required="true" autofocus id="descricao" required="yes" type="text"
                   class="form-control" name="descricao" value="<?php echo $vaga['descricao']; ?>"/>
        </div>


        <div class="col-md-12">
            <label for="empresa">empresa</label>
            <select name="empresa" id="empresa" required="true" class="form-control">
                // vamos criar a visualização
                <option selected="" value="<?php echo $vaga['empresa'];?>">
                    <?php
                    echo fncgetempresa($vaga['empresa'])['empresa'];
                    ?>
                </option>
                <?php
                foreach (fncempresalista() as $item) {
                    ?>
                    <option value="<?php echo $item['id']; ?>"><?php echo $item['empresa']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>

        <div class="col-md-2">
            <label for="sexo">Sexo:</label>
            <select name="sexo" id="sexo" class="form-control input-sm">// vamos criar a visualização de sexo
                <option selected="" value="<?php if ($vaga['sexo'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $vaga['sexo'];
                } ?>">
                    <?php
                    if ($vaga['sexo'] == 0) {
                        echo "Todos";
                    }
                    if ($vaga['sexo'] == 1) {
                        echo "Feminino";
                    }
                    if ($vaga['sexo'] == 2) {
                        echo "Masculino";
                    }
                    if ($vaga['sexo'] == 3) {
                        echo "Indefinido";
                    }
                    ?>
                </option>
                <option value="0">Todos</option>
                <option value="1">Feminino</option>
                <option value="2">Masculino</option>
                <option value="3">Indefinido</option>
            </select>
        </div>

        <div class="col-md-10">
            <label for="cnh">CNH:</label>
            <select name="cnh" id="cnh" class="form-control input-sm">

                <option selected="" value="<?php if ($vaga['cnh'] == "") {
                    $z = 0;
                    echo $z;
                } else {
                    echo $vaga['cnh'];
                }
                echo "\">";
                echo $vaga['cnh'];
                ?>
                </option>
                <option value="0">Qualquer</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
                <option value="AB">AB</option>
                <option value="AC">AC</option>
                <option value="AD">AD</option>
                <option value="AE">AE</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <h5><strong>Formação Especifica:</strong></h5>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pcd" value="1" <?php if ($vaga['pcd'] == 1) {echo "checked";}?>> PCD
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="alfabetizado" value="1" <?php if ($vaga['alfabetizado'] == 1) {echo "checked";}?>> Alfabetizado
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="a1a4" value="1" <?php if ($vaga['a1a4'] == 1) {echo "checked";}?>> 1° a 4° Serie
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="a5a8" value="1" <?php if ($vaga['a5a8'] == 1) {echo "checked";}?>> 5° a 8° Serie
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="ensino_medio" value="1" <?php if ($vaga['ensino_medio'] == 1) {echo "checked";}?>> Ensino Medio
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="superior" value="1" <?php if ($vaga['superior'] == 1) {echo "checked";}?>> Ensino Superior
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="administracao" value="1" <?php if ($vaga['administracao'] == 1) {echo "checked";}?>> Administracao
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="contabilidade" value="1" <?php if ($vaga['contabilidade'] == 1) {echo "checked";}?>> Contabilidade
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="psicologia" value="1" <?php if ($vaga['psicologia'] == 1) {echo "checked";}?>> Psicologia
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servico_social" value="1" <?php if ($vaga['servico_social'] == 1) {echo "checked";}?>> Servico Social
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="sistema_informacao" value="1" <?php if ($vaga['sistema_informacao'] == 1) {echo "checked";}?>> Informatica
                </label>
            </div>
        </div>



        <div class="col-md-3">
            <h5><strong>Experiencia Especifica:</strong></h5>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="primeiro_emprego" value="1" <?php if ($vaga['primeiro_emprego'] == 1) {echo "checked";}?>> Primeiro Emprego
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="assistente_administrativo" value="1" <?php if ($vaga['assistente_administrativo'] == 1) {echo "checked";}?>> Assistente administrativo
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="assistente_financeiro" value="1" <?php if ($vaga['assistente_financeiro'] == 1) {echo "checked";}?>> assistente_financeiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="atendimento_balconista" value="1" <?php if ($vaga['atendimento_balconista'] == 1) {echo "checked";}?>> atendimento_balconista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="auxiliar_cozinha" value="1" <?php if ($vaga['auxiliar_cozinha'] == 1) {echo "checked";}?>> auxiliar_cozinha
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="auxiliar_producao" value="1" <?php if ($vaga['auxiliar_producao'] == 1) {echo "checked";}?>> auxiliar_producao
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="baba" value="1" <?php if ($vaga['baba'] == 1) {echo "checked";}?>> Babá
                </label>
            </div>


            <div class="checkbox">
                <label>
                    <input type="checkbox" name="bombeiro" value="1" <?php if ($vaga['bombeiro'] == 1) {echo "checked";}?>> Bombeiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cabelereira" value="1" <?php if ($vaga['cabelereira'] == 1) {echo "checked";}?>> cabelereira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="camareira" value="1" <?php if ($vaga['camareira'] == 1) {echo "checked";}?>> Camareira
                </label>
            </div>


            <div class="checkbox">
                <label>
                    <input type="checkbox" name="changueiro" value="1" <?php if ($vaga['changueiro'] == 1) {echo "checked";}?>> Changueiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="copeira" value="1" <?php if ($vaga['copeira'] == 1) {echo "checked";}?>> Copeira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cozinheiro" value="1" <?php if ($vaga['cozinheiro'] == 1) {echo "checked";}?>> cozinheiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="cuidadora" value="1" <?php if ($vaga['cuidadora'] == 1) {echo "checked";}?>> Cuidadora
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="decorador" value="1" <?php if ($vaga['decorador'] == 1) {echo "checked";}?>> Decorador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="depiladora" value="1" <?php if ($vaga['depiladora'] == 1) {echo "checked";}?>> depiladora
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="detetizacao" value="1" <?php if ($vaga['detetizacao'] == 1) {echo "checked";}?>> Detetizador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="domestica" value="1" <?php if ($vaga['domestica'] == 1) {echo "checked";}?>> Domestica
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="eletricista" value="1" <?php if ($vaga['eletricista'] == 1) {echo "checked";}?>> eletricista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="embalador" value="1" <?php if ($vaga['embalador'] == 1) {echo "checked";}?>> embalador
                </label>
            </div>
        </div>

        <div class="col-md-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="encanador" value="1" <?php if ($vaga['encanador'] == 1) {echo "checked";}?>> Encanador
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="estagiario" value="1" <?php if ($vaga['estagiario'] == 1) {echo "checked";}?>> estagiario
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="esteticista" value="1" <?php if ($vaga['esteticista'] == 1) {echo "checked";}?>> esteticista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="faxineira_diarista" value="1" <?php if ($vaga['faxineira_diarista'] == 1) {echo "checked";}?>> Faxineira - Diarista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="fiscal_loja" value="1" <?php if ($vaga['fiscal_loja'] == 1) {echo "checked";}?>> fiscal_loja
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="frentista" value="1" <?php if ($vaga['frentista'] == 1) {echo "checked";}?>> frentista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="garcon" value="1" <?php if ($vaga['garcon'] == 1) {echo "checked";}?>> Garçon
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="gerente_loja" value="1" <?php if ($vaga['gerente_loja'] == 1) {echo "checked";}?>> gerente_loja
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="jardinagem" value="1" <?php if ($vaga['jardinagem'] == 1) {echo "checked";}?>> Jardinagem
                </label>
            </div>


            <div class="checkbox">
                <label>
                    <input type="checkbox" name="lavadeira" value="1" <?php if ($vaga['lavadeira'] == 1) {echo "checked";}?>> Lavadeira
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="limpador_de_piscina" value="1" <?php if ($vaga['limpador_de_piscina'] == 1) {echo "checked";}?>> Limpador de Piscina
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="marcineiro" value="1" <?php if ($vaga['marcineiro'] == 1) {echo "checked";}?>> Marcineiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="manicure" value="1" <?php if ($vaga['manicure'] == 1) {echo "checked";}?>> manicure
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="massagista value="1" <?php if ($vaga['massagista'] == 1) {echo "checked";}?>> massagista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="mecanico" value="1" <?php if ($vaga['mecanico'] == 1) {echo "checked";}?>> Mecanico
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="monitor" value="1" <?php if ($vaga['monitor'] == 1) {echo "checked";}?>> monitor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="montador_moveis" value="1" <?php if ($vaga['montador_moveis'] == 1) {echo "checked";}?>> montador_moveis
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="motoboy" value="1" <?php if ($vaga['motoboy'] == 1) {echo "checked";}?>> motoboy
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="motorista" value="1" <?php if ($vaga['motorista'] == 1) {echo "checked";}?>> motorista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="operador_caixa" value="1" <?php if ($vaga['operador_caixa'] == 1) {echo "checked";}?>> Operador de Caixa
                </label>
            </div>
        </div>

        <div class="col-md-3">

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="passadeira" value="1" <?php if ($vaga['passadeira'] == 1) {echo "checked";}?>> Passadeira
                </label>
            </div>


            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pedreiro" value="1" <?php if ($vaga['pedreiro'] == 1) {echo "checked";}?>> Pedreiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="pintor" value="1" <?php if ($vaga['pintor'] == 1) {echo "checked";}?>> pintor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="porteiro" value="1" <?php if ($vaga['porteiro'] == 1) {echo "checked";}?>> porteiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="recepcionista" value="1" <?php if ($vaga['recepcionista'] == 1) {echo "checked";}?>> Recepcionista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="safrista" value="1" <?php if ($vaga['safrista'] == 1) {echo "checked";}?>> safrista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="secretaria" value="1" <?php if ($vaga['secretaria'] == 1) {echo "checked";}?>> Secretaria
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="seguranca" value="1" <?php if ($vaga['seguranca'] == 1) {echo "checked";}?>> seguranca
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servente_pedreiro" value="1" <?php if ($vaga['servente_pedreiro'] == 1) {echo "checked";}?>> servente_pedreiro
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="servico_gerais" value="1" <?php if ($vaga['servico_gerais'] == 1) {echo "checked";}?>> Servico Gerais
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="supervisor" value="1" <?php if ($vaga['supervisor'] == 1) {echo "checked";}?>> supervisor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="telefonista" value="1" <?php if ($vaga['telefonista'] == 1) {echo "checked";}?>> Telefonista
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vendedor" value="1" <?php if ($vaga['vendedor'] == 1) {echo "checked";}?>> vendedor
                </label>
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="vigia" value="1" <?php if ($vaga['vigia'] == 1) {echo "checked";}?>> vigia
                </label>
            </div>

        </div>
    </div>

    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>