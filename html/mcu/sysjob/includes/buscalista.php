<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <a href="?pg=Vfiltro" class="btn btn-outline-info btn-lg" ><i class="fa fa-plus"></i></a>
        </div>
    </form>
        <a href="index.php?pg=Vpessoaeditar" class="btn btn-info btn-lg btn-block col-md-6 float-right">
            NOVO CADASTRO
        </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
</div>

    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">NASCIMENTO</th>
            <th scope="col">CIDADE</th>
            <th scope="col">EDITAR</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="2">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&scb={$_GET['scb']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="2" class="text-info text-right"><?php echo $todos->rowCount();?> pessoa(s) listada(s)</th>
        </tr>
        </tfoot>

        <?php
        // vamos criar a visualização
        if(isset($_GET['sca']) and $_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $nascimento = dataBanco2data ($dados["nascimento"]);
            $cidade = $dados["cidade"];
            $idade = Calculo_Idade($dados["nascimento"]);
            ?>
        <tbody>
            <tr>
                <td scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </td>

                <th>
                    <?php
                    if($nascimento!="01/01/1000" and $nascimento!="01/01/1900"){
                        echo $nascimento. " <span class='badge badge-info' title='idade'>" . $idade. "</span>";
                    }else{
                        echo "<span class='text-warning'>??/??/????</span>";
                    }
                    ?>
                </th>

                <th>
                    <?php
                        echo $cidade;
                    ?>
                </th>
                
                <td>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa">
                        Alterar
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
