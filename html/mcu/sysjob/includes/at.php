<?php
// Recebe
if (isset($_GET['id'])) {
    $at_idpessoa = $_GET['id'];
    //existe um id e se ele é numérico
    if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * "
            ."FROM mcu_sysjob_atividades "
            ."WHERE mcu_sysjob_atividades.pessoa = ? "
            ."ORDER BY "
            ."mcu_sysjob_atividades.`data` DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $at_idpessoa);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $ativi = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }
}
?>
<div id="pointofview" class="card">
    <div class="card-header bg-info text-light">
        Histórico de movimentações
    </div>
    <div class="card-body">
        <h5>
            <?php
            foreach ($ativi as $at) {
                ?>
                <hr>
                <blockquote class="blockquote blockquote-info">
                    “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                    <br>
                    <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                    <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($at['profissional']);
                        echo $us['nome'];
                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                        ?>
                    </footer>

                </blockquote>
                <?php
            }
            ?>
        </h5>
    </div>
</div>