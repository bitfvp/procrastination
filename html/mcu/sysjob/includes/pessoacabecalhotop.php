<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
    <div class="card mb-2">
        <div class="card-header bg-info text-light">
            Dados do usuário
        </div>
        <div class="card-body">

            <img src="<?php echo $env->env_estatico; ?>img/semfoto.png" alt="">

            <h1 class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</h1>
            <h5>
                Nascimento:
                <strong class="text-info">
                    <?php
                    if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                        echo "<span class='text-info'>";
                        echo dataBanco2data ($pessoa['nascimento']);
                        echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>

                Estado civil:
                <?php
                echo "<strong class='text-info'>";
                if ($pessoa['estado_civil'] == 0) {
                    echo "Solteiro(a)";
                }
                if ($pessoa['estado_civil'] == 1) {
                    echo "Casado(a)";
                }
                if ($pessoa['estado_civil'] == 2) {
                    echo "Divorciado(a)";
                }
                if ($pessoa['estado_civil'] == 3) {
                    echo "Uniao Estavel";
                }
                if ($pessoa['estado_civil'] == 4) {
                    echo "Viuvo(a)";
                }
                echo "</strong>";
                ?>

                Sexo:
                <?php
                if ($pessoa['sexo'] != "" and $pessoa['sexo'] != "0") {
                    echo "<strong class='text-info'>";
                    if ($pessoa['sexo'] == 0) {
                        echo "Selecione...";
                    }
                    if ($pessoa['sexo'] == 1) {
                        echo "Feminino";
                    }
                    if ($pessoa['sexo'] == 2) {
                        echo "Masculino";
                    }
                    if ($pessoa['sexo'] == 3) {
                        echo "Indefinido";
                    }
                    echo "</strong>";
                } else {
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
                <br>
                CPF:
                <strong class="text-info"><?php echo $pessoa['cpf']; ?></strong>
                RG:
                <strong class="text-info"><?php echo $pessoa['rg']; ?></strong>
                CNH:
                <strong class="text-info"><?php echo $pessoa['cnh']; ?></strong>
                CTPS:
                <strong class="text-info"><?php echo $pessoa['ctps']; ?></strong>
            </h5>
            <hr>
            <label for=""><i class="fa fa-map-marker"></i> Endereço</label>
            <h5>
                <strong class="text-info"><?php
                    if ($pessoa['endereco'] != "") {
                        echo "<span class='text-info'>";
                        echo $pessoa['endereco'];
                        echo "</span>";
                    } else {
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?></strong>&nbsp;&nbsp;
                Número:
                <strong class="text-info"><?php
                    if ($pessoa['numero'] != "") {
                        echo "<span class='text-info'>";
                        echo $pessoa['numero'];
                        echo "</span>";
                    } else {
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?></strong>&nbsp;&nbsp;
                Bairro
                <strong class="text-info">
                    <?php
                    echo $pessoa['bairro'];
                    ?>
                </strong>&nbsp;&nbsp;
                Cidade:
                <strong class="text-info">
                    <?php
                    echo $pessoa['cidade'];
                    ?>
                </strong>&nbsp;&nbsp;
            </h5>
            <h5>
                Referência:
                <strong class="text-info">
                    <?php
                    echo $pessoa['referencia'];
                    ?>
                </strong>&nbsp;&nbsp;
            </h5>

            <hr>

            <label for=""><i class="fa fa-at"></i> E-mail</label>
            <h5>
                <strong class="text-info"><?php echo $pessoa['email']; ?>&nbsp;&nbsp;
                </strong>
            </h5>

            <hr>

            <label for=""><i class="fa fa-phone "></i> Telefone</label>
            <h5>

                <strong class="text-info"><?php echo $pessoa['telefone1']; ?>&nbsp;&nbsp;
                </strong>
                <br>
                <strong class="text-info"><?php echo $pessoa['telefone2']; ?>&nbsp;&nbsp;
                </strong>
            </h5>
            <hr>

            <label for=""><i class="fa fa-check-circle"></i> Objetivo:</label>
            <h5>
                <strong class="text-info"><?php echo $pessoa['objetivo']; ?>&nbsp;&nbsp;
                </strong>
            </h5>

            <hr>

            <label for="" id="pointofview2"><i class="fa fa-user-graduate"></i> Formação:</label>
                <blockquote class="blockquote blockquote-info my-2">
                    <?php
                    $sql = "SELECT * FROM mcu_sysjob_formacao WHERE pessoa_id=? ORDER by periodo desc";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $formacaolista = $consulta->fetchall();
                    $sql=null;
                    $consulta=null;

                    echo "<ul class='list-group'>";
                    foreach ($formacaolista as $flist){
                        echo "<li class='list-group-item list-group-item-default'>";

                        echo "<h5>";
                        echo "<a href='index.php?pg=Vpessoa&id={$_GET['id']}&aca=excluirformacao&id_f={$flist['id']}'' class='float-right'><i class='fa fa-times'></i></a>";
                        echo "<strong class='text-info'>{$flist['titulo']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$flist['escola']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$flist['periodo']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$flist['descricao']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";

                        echo "<h5>";
                        echo "<strong class='text-info'>";
                        if ($flist['concluido']==1){echo "Concluido";}else{echo "Incompleto";}
                        echo "&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "</li>";
                    }
                    echo "</ul>";
                    ?>
                    <form action="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>&aca=novaformacao" method="post" class="mt-3">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Título:</label>
                                <input type="text" class="form-control input-sm" name="titulo" placeholder="Digite o Titulo">
                            </div>
                            <div class="col-md-4">
                                <label for="">Escola:</label>
                                <input type="text" class="form-control input-sm" name="escola" placeholder="Digite a escola/Faculdade">
                            </div>
                            <div class="col-md-4">
                                <label for="">Período final:</label>
                                <input type="text" class="form-control input-sm" name="periodo" placeholder="Digite o período">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Descrição:</label>
                                <input type="text" class="form-control input-sm" name="descricao" placeholder="Digite a descrição">
                            </div>

                            <div class="col-md-4 align-self-end">
                                    <label>
                                        <input type="checkbox" name="concluido" value="1" checked> Concluido
                                    </label>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <input type="submit" class="btn btn-sm btn-primary btn-block" value="+">
                            </div>
                        </div>
                    </form>
                </blockquote>

            <hr>
            <label for="" id="pointofview3"><i class="fa fa-star"></i> Histórico profissional:</label>
                <blockquote class="blockquote blockquote-info my-2">
                    <?php
                    $sql = "SELECT * FROM mcu_sysjob_experiencia WHERE pessoa_id=? ORDER by periodo desc";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $experiencialista = $consulta->fetchall();
                    $sql=null;
                    $consulta=null;

                    echo "<ul class='list-group'>";
                    foreach ($experiencialista as $elist){
                        echo "<li class='list-group-item list-group-item-default'>";
                        echo "<h5>";
                        echo "<a href='index.php?pg=Vpessoa&id={$_GET['id']}&aca=excluirexperiencia&id_e={$elist['id']}'' class='float-right'><i class='fa fa-times'></i></a>";
                        echo "<strong class='text-info'>{$elist['empresa']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$elist['cargo']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$elist['periodo']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "<h5>";
                        echo "<strong class='text-info'>{$elist['descricao']}&nbsp;&nbsp;</strong>";
                        echo "</h5>";
                        echo "</li>";
                    }
                    echo "</ul>";
                    ?>
                    <form action="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>&aca=novaexperiencia" method="post" class="mt-2">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Empresa:</label>
                                <input type="text" class="form-control input-sm" name="empresa">
                            </div>
                            <div class="col-md-6">
                                <label for="">Cargo:</label>
                                <input type="text" class="form-control input-sm" name="cargo">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <label for="">Periodo final:</label>
                                <input type="text" class="form-control input-sm" name="periodo">
                            </div>
                            <div class="col-md-7">
                                <label for="">Descrição:</label>
                                <input type="text" class="form-control input-sm" name="descricao">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <input type="submit" class="btn btn-sm btn-primary btn-block" value="+">
                            </div>
                        </div>

                    </form>
                </blockquote>

            <hr>
            <label for=""><i class="fa fa-asterisk"></i> Outros objetivos</label>
            <h5>
                Último salário:
                <strong class="text-info"><?php echo $pessoa['ultimo_salario']; ?>&nbsp;&nbsp;
                </strong>

                Pretensão salarial:
                <strong class="text-info"><?php echo $pessoa['pretensao_salarial']; ?>&nbsp;&nbsp;
                </strong>
            </h5>
            <hr>

            <label for=""><i class="fa fa-info-circle"></i> Informações complementares</label>
            <h5>
                <strong class="text-info">
                    <?php echo $pessoa['informacoes_complementares']; ?>&nbsp;&nbsp;
                </strong>
            </h5>

        </div>
        <a class="btn btn-info btn-block" href="?pg=Vcurriculo&id=<?php echo $_GET['id']; ?>"
           title="Gerar Curriculo" target="_blank">
            GERAR CURRICULO
        </a>

        <a class="btn btn-success btn-block" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>"
           title="Edite os dados dessa pessoa">
            EDITAR PESSOA
        </a>

    </div>
    <!--fim de div dados-->
</div>