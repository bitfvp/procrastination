<?php
function fncvagalista(){
    $sql = "SELECT * FROM mcu_sysjob_vagas ORDER BY vaga";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $vagalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $vagalista;
}

function fncgetvaga($id){
    $sql = "SELECT * FROM mcu_sysjob_vagas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getvaga = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getvaga;
}
