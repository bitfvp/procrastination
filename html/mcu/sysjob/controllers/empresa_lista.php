<?php
function fncempresalista(){
    $sql = "SELECT * FROM mcu_sysjob_empresas ORDER BY empresa";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $empresalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $empresalista;
}

function fncgetempresa($id){
    $sql = "SELECT * FROM mcu_sysjob_empresas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getempresa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getempresa;
}