<?php
class SalvarExperiencia{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarexperiencianovo(
        $pessoa_id,
        $empresa,
        $cargo,
        $periodo,
        $descricao
    ){

        
                try {
                    $sql = "INSERT INTO mcu_sysjob_experiencia ";
                    $sql .= "(id, pessoa_id, empresa, cargo, periodo, descricao)";
                    $sql .= " VALUES ";
                    $sql .= "(NULL, :pessoa_id, :empresa, :cargo, :periodo, :descricao)";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":pessoa_id", $pessoa_id);
                    $insere->bindValue(":empresa", $empresa);
                    $insere->bindValue(":cargo", $cargo);
                    $insere->bindValue(":periodo", $periodo);
                    $insere->bindValue(":descricao", $descricao);

                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro:' . $error_msg->getMessage();
                }



        if(isset($insere)){
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"3",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa_id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }//da function
}//d class
?>