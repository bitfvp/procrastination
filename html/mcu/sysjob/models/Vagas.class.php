<?php

class Vaga
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarvaganova(
        $vaga,
        $descricao,
        $empresa,
        $sexo,
        $cnh,
        $pcd,
        $alfabetizado,
        $a1a4,
        $a5a8,
        $ensino_medio,
        $superior,
        $administracao,
        $contabilidade,
        $psicologia,
        $servico_social,
        $sistema_informacao,
        $primeiro_emprego,
        $assistente_administrativo,
        $assistente_financeiro,
        $atendimento_balconista,
        $auxiliar_cozinha,
        $auxiliar_producao,
        $baba,
        $bombeiro,
        $cabelereira,
        $camareira,
        $changueiro,
        $copeira,
        $cozinheiro,
        $cuidadora,
        $decorador,
        $depiladora,
        $detetizacao,
        $domestica,
        $eletricista,
        $embalador,
        $encanador,
        $estagiario,
        $esteticista,
        $faxineira_diarista,
        $fiscal_loja,
        $frentista,
        $garcon,
        $gerente_loja,
        $jardinagem,
        $lavadeira,
        $limpador_de_piscina,
        $marcineiro,
        $manicure,
        $massagista,
        $mecanico,
        $monitor,
        $montador_moveis,
        $motoboy,
        $motorista,
        $operador_caixa,
        $passadeira,
        $pedreiro,
        $pintor,
        $porteiro,
        $recepcionista,
        $safrista,
        $secretaria,
        $seguranca,
        $servente_pedreiro,
        $servico_gerais,
        $supervisor,
        $telefonista,
        $vendedor,
        $vigia
    )
    {


        //inserção no banco
        try {
            $sql = "INSERT INTO mcu_sysjob_vagas ";
            $sql .= "(id, data, vaga, descricao, empresa, sexo, cnh, pcd, alfabetizado, a1a4, a5a8, ensino_medio, superior, administracao, contabilidade, psicologia, servico_social, sistema_informacao, primeiro_emprego, assistente_administrativo, assistente_financeiro, atendimento_balconista, auxiliar_cozinha, auxiliar_producao, baba, bombeiro, cabelereira, camareira, changueiro, copeira, cozinheiro, cuidadora, decorador, depiladora, detetizacao, domestica, eletricista, embalador, encanador, estagiario, esteticista, faxineira_diarista, fiscal_loja, frentista, garcon, gerente_loja, jardinagem, lavadeira, limpador_de_piscina, marcineiro, manicure, massagista, mecanico, monitor, montador_moveis, motoboy, motorista, operador_caixa, passadeira, pedreiro, pintor, porteiro, recepcionista, safrista, secretaria, seguranca, servente_pedreiro, servico_gerais, supervisor, telefonista, vendedor, vigia)";
            $sql .= " VALUES ";
            $sql .= "(NULL, CURRENT_TIMESTAMP, :vaga, :descricao, :empresa, :sexo, :cnh, :pcd, :alfabetizado, :a1a4, :a5a8, :ensino_medio, :superior, :administracao, :contabilidade, :psicologia, :servico_social, :sistema_informacao, :primeiro_emprego, :assistente_administrativo, :assistente_financeiro, :atendimento_balconista, :auxiliar_cozinha, :auxiliar_producao, :baba, :bombeiro, :cabelereira, :camareira, :changueiro, :copeira, :cozinheiro, :cuidadora, :decorador, :depiladora, :detetizacao, :domestica, :eletricista, :embalador, :encanador, :estagiario, :esteticista, :faxineira_diarista, :fiscal_loja, :frentista, :garcon, :gerente_loja, :jardinagem, :lavadeira, :limpador_de_piscina, :marcineiro, :manicure, :massagista, :mecanico, :monitor, :montador_moveis, :motoboy, :motorista, :operador_caixa, :passadeira, :pedreiro, :pintor, :porteiro, :recepcionista, :safrista, :secretaria, :seguranca, :servente_pedreiro, :servico_gerais, :supervisor, :telefonista, :vendedor, :vigia )";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":vaga", $vaga);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":empresa", $empresa);
            $insere->bindValue(":sexo", $sexo);
            $insere->bindValue(":cnh", $cnh);
            $insere->bindValue(":pcd", $pcd);
            $insere->bindValue(":alfabetizado", $alfabetizado);
            $insere->bindValue(":a1a4", $a1a4);
            $insere->bindValue(":a5a8", $a5a8);
            $insere->bindValue(":ensino_medio", $ensino_medio);
            $insere->bindValue(":superior", $superior);
            $insere->bindValue(":administracao", $administracao);
            $insere->bindValue(":contabilidade", $contabilidade);
            $insere->bindValue(":psicologia", $psicologia);
            $insere->bindValue(":servico_social", $servico_social);
            $insere->bindValue(":sistema_informacao", $sistema_informacao);
            $insere->bindValue(":primeiro_emprego", $primeiro_emprego);
            $insere->bindValue(":assistente_administrativo", $assistente_administrativo);
            $insere->bindValue(":assistente_financeiro", $assistente_financeiro);
            $insere->bindValue(":atendimento_balconista", $atendimento_balconista);
            $insere->bindValue(":auxiliar_cozinha", $auxiliar_cozinha);
            $insere->bindValue(":auxiliar_producao", $auxiliar_producao);
            $insere->bindValue(":baba", $baba);
            $insere->bindValue(":bombeiro", $bombeiro);
            $insere->bindValue(":cabelereira", $cabelereira);
            $insere->bindValue(":camareira", $camareira);
            $insere->bindValue(":changueiro", $changueiro);
            $insere->bindValue(":copeira", $copeira);
            $insere->bindValue(":cozinheiro", $cozinheiro);
            $insere->bindValue(":cuidadora", $cuidadora);
            $insere->bindValue(":decorador", $decorador);
            $insere->bindValue(":depiladora", $depiladora);
            $insere->bindValue(":detetizacao", $detetizacao);
            $insere->bindValue(":domestica", $domestica);
            $insere->bindValue(":eletricista", $eletricista);
            $insere->bindValue(":embalador", $embalador);
            $insere->bindValue(":encanador", $encanador);
            $insere->bindValue(":estagiario", $estagiario);
            $insere->bindValue(":esteticista", $esteticista);
            $insere->bindValue(":faxineira_diarista", $faxineira_diarista);
            $insere->bindValue(":fiscal_loja", $fiscal_loja);
            $insere->bindValue(":frentista", $frentista);
            $insere->bindValue(":garcon", $garcon);
            $insere->bindValue(":gerente_loja", $gerente_loja);
            $insere->bindValue(":jardinagem", $jardinagem);
            $insere->bindValue(":lavadeira", $lavadeira);
            $insere->bindValue(":limpador_de_piscina", $limpador_de_piscina);
            $insere->bindValue(":marcineiro", $marcineiro);
            $insere->bindValue(":manicure", $manicure);
            $insere->bindValue(":massagista", $massagista);
            $insere->bindValue(":mecanico", $mecanico);
            $insere->bindValue(":monitor", $monitor);
            $insere->bindValue(":montador_moveis", $montador_moveis);
            $insere->bindValue(":motoboy", $motoboy);
            $insere->bindValue(":motorista", $motorista);
            $insere->bindValue(":operador_caixa", $operador_caixa);
            $insere->bindValue(":passadeira", $passadeira);
            $insere->bindValue(":pedreiro", $pedreiro);
            $insere->bindValue(":pintor", $pintor);
            $insere->bindValue(":porteiro", $porteiro);
            $insere->bindValue(":recepcionista", $recepcionista);
            $insere->bindValue(":safrista", $safrista);
            $insere->bindValue(":secretaria", $secretaria);
            $insere->bindValue(":seguranca", $seguranca);
            $insere->bindValue(":servente_pedreiro", $servente_pedreiro);
            $insere->bindValue(":servico_gerais", $servico_gerais);
            $insere->bindValue(":supervisor", $supervisor);
            $insere->bindValue(":telefonista", $telefonista);
            $insere->bindValue(":vendedor", $vendedor);
            $insere->bindValue(":vigia", $vigia);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        if (isset($insere)) {
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            $sql = "SELECT Max(id) FROM mcu_sysjob_vagas";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql = null;
            $consulta = null;

            $maid = $mid[0];

            header("Location: index.php?pg=Vvaga&id={$maid}");
            exit();

        } else {
            if (empty($flash)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarvagaeditar(
        $id,
        $vaga,
        $descricao,
        $empresa,
        $sexo,
        $cnh,
        $pcd,
        $alfabetizado,
        $a1a4,
        $a5a8,
        $ensino_medio,
        $superior,
        $administracao,
        $contabilidade,
        $psicologia,
        $servico_social,
        $sistema_informacao,
        $primeiro_emprego,
        $assistente_administrativo,
        $assistente_financeiro,
        $atendimento_balconista,
        $auxiliar_cozinha,
        $auxiliar_producao,
        $baba,
        $bombeiro,
        $cabelereira,
        $camareira,
        $changueiro,
        $copeira,
        $cozinheiro,
        $cuidadora,
        $decorador,
        $depiladora,
        $detetizacao,
        $domestica,
        $eletricista,
        $embalador,
        $encanador,
        $estagiario,
        $esteticista,
        $faxineira_diarista,
        $fiscal_loja,
        $frentista,
        $garcon,
        $gerente_loja,
        $jardinagem,
        $lavadeira,
        $limpador_de_piscina,
        $marcineiro,
        $manicure,
        $massagista,
        $mecanico,
        $monitor,
        $montador_moveis,
        $motoboy,
        $motorista,
        $operador_caixa,
        $passadeira,
        $pedreiro,
        $pintor,
        $porteiro,
        $recepcionista,
        $safrista,
        $secretaria,
        $seguranca,
        $servente_pedreiro,
        $servico_gerais,
        $supervisor,
        $telefonista,
        $vendedor,
        $vigia
    )
    {


        //inserção no banco
        try {
            $sql = "UPDATE mcu_sysjob_vagas SET vaga=:vaga, descricao=:descricao, empresa=:empresa, ";
            $sql .= "sexo=:sexo, cnh=:cnh, pcd=:pcd, alfabetizado=:alfabetizado, a1a4=:a1a4, a5a8=:a5a8, ensino_medio=:ensino_medio, superior=:superior, administracao=:administracao, contabilidade=:contabilidade, psicologia=:psicologia, servico_social=:servico_social, sistema_informacao=:sistema_informacao, primeiro_emprego=:primeiro_emprego, assistente_administrativo=:assistente_administrativo, assistente_financeiro=:assistente_financeiro, atendimento_balconista=:atendimento_balconista, auxiliar_cozinha=:auxiliar_cozinha, auxiliar_producao=:auxiliar_producao, baba=:baba, bombeiro=:bombeiro, cabelereira=:cabelereira, camareira=:camareira, changueiro=:changueiro, copeira=:copeira, cozinheiro=:cozinheiro, cuidadora=:cuidadora, decorador=:decorador, depiladora=:depiladora, detetizacao=:detetizacao, domestica=:domestica, eletricista=:eletricista, embalador=:embalador, encanador=:encanador, estagiario=:estagiario, esteticista=:esteticista, faxineira_diarista=:faxineira_diarista, fiscal_loja=:fiscal_loja, frentista=:frentista, garcon=:garcon, gerente_loja=:gerente_loja, jardinagem=:jardinagem, lavadeira=:lavadeira, limpador_de_piscina=:limpador_de_piscina, marcineiro=:marcineiro, manicure=:manicure, massagista=:massagista, mecanico=:mecanico, monitor=:monitor, montador_moveis=:montador_moveis, motoboy=:motoboy, motorista=:motorista, operador_caixa=:operador_caixa, passadeira=:passadeira, pedreiro=:pedreiro, pintor=:pintor, porteiro=:porteiro, recepcionista=:recepcionista, safrista=:safrista, secretaria=:secretaria, seguranca=:seguranca, servente_pedreiro=:servente_pedreiro, servico_gerais=:servico_gerais, supervisor=:supervisor, telefonista=:telefonista, vendedor=:vendedor, vigia=:vigia ";
            $sql .= "WHERE id=:id";
            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindValue(":vaga", $vaga);
            $atualiza->bindValue(":descricao", $descricao);
            $atualiza->bindValue(":empresa", $empresa);
            $atualiza->bindValue(":sexo", $sexo);
            $atualiza->bindValue(":cnh", $cnh);
            $atualiza->bindValue(":pcd", $pcd);
            $atualiza->bindValue(":alfabetizado", $alfabetizado);
            $atualiza->bindValue(":a1a4", $a1a4);
            $atualiza->bindValue(":a5a8", $a5a8);
            $atualiza->bindValue(":ensino_medio", $ensino_medio);
            $atualiza->bindValue(":superior", $superior);
            $atualiza->bindValue(":administracao", $administracao);
            $atualiza->bindValue(":contabilidade", $contabilidade);
            $atualiza->bindValue(":psicologia", $psicologia);
            $atualiza->bindValue(":servico_social", $servico_social);
            $atualiza->bindValue(":sistema_informacao", $sistema_informacao);
            $atualiza->bindValue(":primeiro_emprego", $primeiro_emprego);
            $atualiza->bindValue(":assistente_administrativo", $assistente_administrativo);
            $atualiza->bindValue(":assistente_financeiro", $assistente_financeiro);
            $atualiza->bindValue(":atendimento_balconista", $atendimento_balconista);
            $atualiza->bindValue(":auxiliar_cozinha", $auxiliar_cozinha);
            $atualiza->bindValue(":auxiliar_producao", $auxiliar_producao);
            $atualiza->bindValue(":baba", $baba);
            $atualiza->bindValue(":bombeiro", $bombeiro);
            $atualiza->bindValue(":cabelereira", $cabelereira);
            $atualiza->bindValue(":camareira", $camareira);
            $atualiza->bindValue(":changueiro", $changueiro);
            $atualiza->bindValue(":copeira", $copeira);
            $atualiza->bindValue(":cozinheiro", $cozinheiro);
            $atualiza->bindValue(":cuidadora", $cuidadora);
            $atualiza->bindValue(":decorador", $decorador);
            $atualiza->bindValue(":depiladora", $depiladora);
            $atualiza->bindValue(":detetizacao", $detetizacao);
            $atualiza->bindValue(":domestica", $domestica);
            $atualiza->bindValue(":eletricista", $eletricista);
            $atualiza->bindValue(":embalador", $embalador);
            $atualiza->bindValue(":encanador", $encanador);
            $atualiza->bindValue(":estagiario", $estagiario);
            $atualiza->bindValue(":esteticista", $esteticista);
            $atualiza->bindValue(":faxineira_diarista", $faxineira_diarista);
            $atualiza->bindValue(":fiscal_loja", $fiscal_loja);
            $atualiza->bindValue(":frentista", $frentista);
            $atualiza->bindValue(":garcon", $garcon);
            $atualiza->bindValue(":gerente_loja", $gerente_loja);
            $atualiza->bindValue(":jardinagem", $jardinagem);
            $atualiza->bindValue(":lavadeira", $lavadeira);
            $atualiza->bindValue(":limpador_de_piscina", $limpador_de_piscina);
            $atualiza->bindValue(":marcineiro", $marcineiro);
            $atualiza->bindValue(":manicure", $manicure);
            $atualiza->bindValue(":massagista", $massagista);
            $atualiza->bindValue(":mecanico", $mecanico);
            $atualiza->bindValue(":monitor", $monitor);
            $atualiza->bindValue(":montador_moveis", $montador_moveis);
            $atualiza->bindValue(":motoboy", $motoboy);
            $atualiza->bindValue(":motorista", $motorista);
            $atualiza->bindValue(":operador_caixa", $operador_caixa);
            $atualiza->bindValue(":passadeira", $passadeira);
            $atualiza->bindValue(":pedreiro", $pedreiro);
            $atualiza->bindValue(":pintor", $pintor);
            $atualiza->bindValue(":porteiro", $porteiro);
            $atualiza->bindValue(":recepcionista", $recepcionista);
            $atualiza->bindValue(":safrista", $safrista);
            $atualiza->bindValue(":secretaria", $secretaria);
            $atualiza->bindValue(":seguranca", $seguranca);
            $atualiza->bindValue(":servente_pedreiro", $servente_pedreiro);
            $atualiza->bindValue(":servico_gerais", $servico_gerais);
            $atualiza->bindValue(":supervisor", $supervisor);
            $atualiza->bindValue(":telefonista", $telefonista);
            $atualiza->bindValue(":vendedor", $vendedor);
            $atualiza->bindValue(":vigia", $vigia);
            $atualiza->bindValue(":id", $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        if (isset($atualiza)) {
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vvaga&id={$id}");
            exit();

        } else {
            if (empty($fsh)) {

                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}

?>