<?php
class Empresa{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarempresanova($empresa,$endereco,$contato){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_sysjob_empresas ";
                $sql.="(id, empresa, endereco, contato)";
                $sql.=" VALUES ";
                $sql.="(NULL, :empresa, :endereco, :contato)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":empresa", $empresa);
                $insere->bindValue(":endereco", $endereco);
                $insere->bindValue(":contato", $contato);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarempresaeditar($id,$empresa,$endereco,$contato){
        //inserção no banco
        try{
            $sql="UPDATE mcu_sysjob_empresas SET empresa=:empresa, endereco=:endereco, contato=:contato WHERE id=:id";

            global $pdo;
            $up=$pdo->prepare($sql);
            $up->bindValue(":empresa", $empresa);
            $up->bindValue(":endereco", $endereco);
            $up->bindValue(":contato", $contato);
            $up->bindValue(":id", $id);
            $up->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($up)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>