<?php
class SalvarFormacao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarformacaonovo(
        $pessoa_id,
        $titulo,
        $escola,
        $periodo,
        $descricao,
        $concluido
    ){
        //tratamento das variaveis
  //      $titulo=ucwords(strtoupper($titulo));
        ////    $escola=ucwords(strtoupper($escola));

        
                try {
                    $sql = "INSERT INTO mcu_sysjob_formacao ";
                    $sql .= "(id, pessoa_id, titulo, escola, periodo, descricao, concluido)";
                    $sql .= " VALUES ";
                    $sql .= "(NULL, :pessoa_id, :titulo, :escola, :periodo, :descricao, :concluido)";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":pessoa_id", $pessoa_id);
                    $insere->bindValue(":titulo", $titulo);
                    $insere->bindValue(":escola", $escola);
                    $insere->bindValue(":periodo", $periodo);
                    $insere->bindValue(":descricao", $descricao);
                    $insere->bindValue(":concluido", $concluido);

                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro:' . $error_msg->getMessage();
                }



        if(isset($insere)){
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"2",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa_id}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }//da function
}//d class
?>