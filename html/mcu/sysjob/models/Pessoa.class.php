<?php
class Pessoa{
    public function fncsalvarpessoaedicao(
        $id,
        $id_prof,
        $nome,
        $nascimento,
        $sexo,
        $estado_civil,
        $cpf,
        $rg,
        $cnh,
        $ctps,
        $endereco,
        $numero,
        $bairro,
        $cidade,
        $referencia,
        $email,
        $telefone1,
        $telefone2,
        $objetivo,
        $ultimo_salario,
        $pretensao_salarial,
        $informacoes_complementares,
        $pcd,
        $alfabetizado,
        $a1a4,
        $a5a8,
        $ensino_medio,
        $superior,
        $administracao,
        $contabilidade,
        $psicologia,
        $servico_social,
        $sistema_informacao,
        $primeiro_emprego,
        $assistente_administrativo,
        $assistente_financeiro,
        $atendimento_balconista,
        $auxiliar_cozinha,
        $auxiliar_producao,
        $baba,
        $bombeiro,
        $cabelereira,
        $camareira,
        $changueiro,
        $copeira,
        $cozinheiro,
        $cuidadora,
        $decorador,
        $depiladora,
        $detetizacao,
        $domestica,
        $eletricista,
        $embalador,
        $encanador,
        $estagiario,
        $esteticista,
        $faxineira_diarista,
        $fiscal_loja,
        $frentista,
        $garcon,
        $gerente_loja,
        $jardinagem,
        $lavadeira,
        $limpador_de_piscina,
        $marcineiro,
        $manicure,
        $massagista,
        $mecanico,
        $monitor,
        $montador_moveis,
        $motoboy,
        $motorista,
        $operador_caixa,
        $passadeira,
        $pedreiro,
        $pintor,
        $porteiro,
        $recepcionista,
        $safrista,
        $secretaria,
        $seguranca,
        $servente_pedreiro,
        $servico_gerais,
        $supervisor,
        $telefonista,
        $vendedor,
        $vigia
    ){

        try{
            $sql="SELECT * FROM mcu_sysjob_pessoas WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

                try {
                $sql ="UPDATE mcu_sysjob_pessoas SET ";
                $sql .="nome=:nome, nascimento=:nascimento, sexo=:sexo, estado_civil=:estado_civil, cpf=:cpf, rg=:rg, cnh=:cnh, ctps=:ctps, ";
                $sql .="endereco=:endereco, numero=:numero, bairro=:bairro, cidade=:cidade, referencia=:referencia, email=:email, ";
                $sql .="telefone1=:telefone1, telefone2=:telefone2, objetivo=:objetivo, ultimo_salario=:ultimo_salario, ";
                $sql .="pretensao_salarial=:pretensao_salarial, informacoes_complementares=:informacoes_complementares, ";
                $sql .="pcd=:pcd, alfabetizado=:alfabetizado, a1a4=:a1a4, a5a8=:a5a8, ensino_medio=:ensino_medio, superior=:superior, ";
                $sql .="administracao=:administracao, contabilidade=:contabilidade, psicologia=:psicologia, servico_social=:servico_social, ";
                $sql .="sistema_informacao=:sistema_informacao, primeiro_emprego=:primeiro_emprego, assistente_administrativo=:assistente_administrativo, assistente_financeiro=:assistente_financeiro, atendimento_balconista=:atendimento_balconista, auxiliar_cozinha=:auxiliar_cozinha, auxiliar_producao=:auxiliar_producao, baba=:baba, bombeiro=:bombeiro, cabelereira=:cabelereira, camareira=:camareira, changueiro=:changueiro, copeira=:copeira, cozinheiro=:cozinheiro, cuidadora=:cuidadora, decorador=:decorador, depiladora=:depiladora, detetizacao=:detetizacao, domestica=:domestica, eletricista=:eletricista, embalador=:embalador, encanador=:encanador, estagiario=:estagiario, esteticista=:esteticista, faxineira_diarista=:faxineira_diarista, fiscal_loja=:fiscal_loja, frentista=:frentista, garcon=:garcon, gerente_loja=:gerente_loja, jardinagem=:jardinagem, lavadeira=:lavadeira, limpador_de_piscina=:limpador_de_piscina, marcineiro=:marcineiro, manicure=:manicure, massagista=:massagista, mecanico=:mecanico, monitor=:monitor, montador_moveis=:montador_moveis, motoboy=:motoboy, motorista=:motorista, operador_caixa=:operador_caixa, passadeira=:passadeira, pedreiro=:pedreiro, pintor=:pintor, porteiro=:porteiro, recepcionista=:recepcionista, safrista=:safrista, secretaria=:secretaria, seguranca=:seguranca, servente_pedreiro=:servente_pedreiro, servico_gerais=:servico_gerais, supervisor=:supervisor, telefonista=:telefonista, vendedor=:vendedor, vigia=:vigia ";
                $sql .="WHERE id=:id";
            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindValue(":nome", $nome);
                    $atualiza->bindValue(":nascimento", $nascimento);
                    $atualiza->bindValue(":sexo", $sexo);
                    $atualiza->bindValue(":estado_civil", $estado_civil);
                    $atualiza->bindValue(":cpf", $cpf);
                    $atualiza->bindValue(":rg", $rg);
                    $atualiza->bindValue(":cnh", $cnh);
                    $atualiza->bindValue(":ctps", $ctps);
                    $atualiza->bindValue(":endereco", $endereco);
                    $atualiza->bindValue(":numero", $numero);
                    $atualiza->bindValue(":bairro", $bairro);
                    $atualiza->bindValue(":cidade", $cidade);
                    $atualiza->bindValue(":referencia", $referencia);
                    $atualiza->bindValue(":email", $email);
                    $atualiza->bindValue(":telefone1", $telefone1);
                    $atualiza->bindValue(":telefone2", $telefone2);
                    $atualiza->bindValue(":objetivo", $objetivo);
                    $atualiza->bindValue(":ultimo_salario", $ultimo_salario);
                    $atualiza->bindValue(":pretensao_salarial", $pretensao_salarial);
                    $atualiza->bindValue(":informacoes_complementares", $informacoes_complementares);
                    $atualiza->bindValue(":pcd", $pcd);
                    $atualiza->bindValue(":alfabetizado", $alfabetizado);
                    $atualiza->bindValue(":a1a4", $a1a4);
                    $atualiza->bindValue(":a5a8", $a5a8);
                    $atualiza->bindValue(":ensino_medio", $ensino_medio);
                    $atualiza->bindValue(":superior", $superior);
                    $atualiza->bindValue(":administracao", $administracao);
                    $atualiza->bindValue(":contabilidade", $contabilidade);
                    $atualiza->bindValue(":psicologia", $psicologia);
                    $atualiza->bindValue(":servico_social", $servico_social);
                    $atualiza->bindValue(":sistema_informacao", $sistema_informacao);
                    $atualiza->bindValue(":primeiro_emprego", $primeiro_emprego);
                    $atualiza->bindValue(":assistente_administrativo", $assistente_administrativo);
                    $atualiza->bindValue(":assistente_financeiro", $assistente_financeiro);
                    $atualiza->bindValue(":atendimento_balconista", $atendimento_balconista);
                    $atualiza->bindValue(":auxiliar_cozinha", $auxiliar_cozinha);
                    $atualiza->bindValue(":auxiliar_producao", $auxiliar_producao);
                    $atualiza->bindValue(":baba", $baba);
                    $atualiza->bindValue(":bombeiro", $bombeiro);
                    $atualiza->bindValue(":cabelereira", $cabelereira);
                    $atualiza->bindValue(":camareira", $camareira);
                    $atualiza->bindValue(":changueiro", $changueiro);
                    $atualiza->bindValue(":copeira", $copeira);
                    $atualiza->bindValue(":cozinheiro", $cozinheiro);
                    $atualiza->bindValue(":cuidadora", $cuidadora);
                    $atualiza->bindValue(":decorador", $decorador);
                    $atualiza->bindValue(":depiladora", $depiladora);
                    $atualiza->bindValue(":detetizacao", $detetizacao);
                    $atualiza->bindValue(":domestica", $domestica);
                    $atualiza->bindValue(":eletricista", $eletricista);
                    $atualiza->bindValue(":embalador", $embalador);
                    $atualiza->bindValue(":encanador", $encanador);
                    $atualiza->bindValue(":estagiario", $estagiario);
                    $atualiza->bindValue(":esteticista", $esteticista);
                    $atualiza->bindValue(":faxineira_diarista", $faxineira_diarista);
                    $atualiza->bindValue(":fiscal_loja", $fiscal_loja);
                    $atualiza->bindValue(":frentista", $frentista);
                    $atualiza->bindValue(":garcon", $garcon);
                    $atualiza->bindValue(":gerente_loja", $gerente_loja);
                    $atualiza->bindValue(":jardinagem", $jardinagem);
                    $atualiza->bindValue(":lavadeira", $lavadeira);
                    $atualiza->bindValue(":limpador_de_piscina", $limpador_de_piscina);
                    $atualiza->bindValue(":marcineiro", $marcineiro);
                    $atualiza->bindValue(":manicure", $manicure);
                    $atualiza->bindValue(":massagista", $massagista);
                    $atualiza->bindValue(":mecanico", $mecanico);
                    $atualiza->bindValue(":monitor", $monitor);
                    $atualiza->bindValue(":montador_moveis", $montador_moveis);
                    $atualiza->bindValue(":motoboy", $motoboy);
                    $atualiza->bindValue(":motorista", $motorista);
                    $atualiza->bindValue(":operador_caixa", $operador_caixa);
                    $atualiza->bindValue(":passadeira", $passadeira);
                    $atualiza->bindValue(":pedreiro", $pedreiro);
                    $atualiza->bindValue(":pintor", $pintor);
                    $atualiza->bindValue(":porteiro", $porteiro);
                    $atualiza->bindValue(":recepcionista", $recepcionista);
                    $atualiza->bindValue(":safrista", $safrista);
                    $atualiza->bindValue(":secretaria", $secretaria);
                    $atualiza->bindValue(":seguranca", $seguranca);
                    $atualiza->bindValue(":servente_pedreiro", $servente_pedreiro);
                    $atualiza->bindValue(":servico_gerais", $servico_gerais);
                    $atualiza->bindValue(":supervisor", $supervisor);
                    $atualiza->bindValue(":telefonista", $telefonista);
                    $atualiza->bindValue(":vendedor", $vendedor);
                    $atualiza->bindValue(":vigia", $vigia);

            $atualiza->bindValue(":id", $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        }else{
            //msg de erro para o usuario
            $flash="Ops, nao há essa pessoa cadastrado  em nosso sistema!!";
            echo "<div class=\"alert alert-danger alert-dismissible text-center\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Fechar\">
                <span aria-hidden=\"true\">&times;</span>
                </button>
                <p><strong>Erro!!</strong>
                <a href=\"#\" class=\"alert-link\">{$flash}</a>
                </p>
                </div>";

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}&sucesso=s");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//end function


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvarpessoanovo(
        $id_prof,
        $nome,
        $nascimento,
        $sexo,
        $estado_civil,
        $cpf,
        $rg,
        $cnh,
        $ctps,
        $endereco,
        $numero,
        $bairro,
        $cidade,
        $referencia,
        $email,
        $telefone1,
        $telefone2,
        $objetivo,
        $ultimo_salario,
        $pretensao_salarial,
        $informacoes_complementares,
        $pcd,
        $alfabetizado,
        $a1a4,
        $a5a8,
        $ensino_medio,
        $superior,
        $administracao,
        $contabilidade,
        $psicologia,
        $servico_social,
        $sistema_informacao,
        $primeiro_emprego,
        $assistente_administrativo,
        $assistente_financeiro,
        $atendimento_balconista,
        $auxiliar_cozinha,
        $auxiliar_producao,
        $baba,
        $bombeiro,
        $cabelereira,
        $camareira,
        $changueiro,
        $copeira,
        $cozinheiro,
        $cuidadora,
        $decorador,
        $depiladora,
        $detetizacao,
        $domestica,
        $eletricista,
        $embalador,
        $encanador,
        $estagiario,
        $esteticista,
        $faxineira_diarista,
        $fiscal_loja,
        $frentista,
        $garcon,
        $gerente_loja,
        $jardinagem,
        $lavadeira,
        $limpador_de_piscina,
        $marcineiro,
        $manicure,
        $massagista,
        $mecanico,
        $monitor,
        $montador_moveis,
        $motoboy,
        $motorista,
        $operador_caixa,
        $passadeira,
        $pedreiro,
        $pintor,
        $porteiro,
        $recepcionista,
        $safrista,
        $secretaria,
        $seguranca,
        $servente_pedreiro,
        $servico_gerais,
        $supervisor,
        $telefonista,
        $vendedor,
        $vigia
    ){
        //tratamento das variaveis
        $nome=ucwords(strtolower($nome));
       // $endereco=ucwords(strtoupper($endereco));
        $bairro=ucwords(strtolower($bairro));
        $cidade=ucwords(strtolower($cidade));
        $email=strtolower($email);

        if($nascimento==""){
            $nascimento="1000-01-01";
        }



        
                try {
                    $sql = "INSERT INTO mcu_sysjob_pessoas ";
                    $sql .= "(id, data_cadastro, nome, nascimento, sexo, estado_civil, cpf, rg, cnh, ctps, endereco, numero, bairro, cidade, referencia, email, telefone1, telefone2, objetivo, ultimo_salario, pretensao_salarial, informacoes_complementares, pcd, alfabetizado, a1a4, a5a8, ensino_medio, superior, administracao, contabilidade, psicologia, servico_social, sistema_informacao, primeiro_emprego, assistente_administrativo, assistente_financeiro, atendimento_balconista, auxiliar_cozinha, auxiliar_producao, baba, bombeiro, cabelereira, camareira, changueiro, copeira, cozinheiro, cuidadora, decorador, depiladora, detetizacao, domestica, eletricista, embalador, encanador, estagiario, esteticista, faxineira_diarista, fiscal_loja, frentista, garcon, gerente_loja, jardinagem, lavadeira, limpador_de_piscina, marcineiro, manicure, massagista, mecanico, monitor, montador_moveis, motoboy, motorista, operador_caixa, passadeira, pedreiro, pintor, porteiro, recepcionista, safrista, secretaria, seguranca, servente_pedreiro, servico_gerais, supervisor, telefonista, vendedor, vigia) ";
                    $sql .= " VALUES ";
                    $sql .= "(NULL, CURRENT_TIMESTAMP, :nome, :nascimento, :sexo, :estado_civil, :cpf, :rg, :cnh, :ctps, :endereco, :numero, ";
                    $sql .= ":bairro, :cidade, :referencia, :email, :telefone1, :telefone2, :objetivo, :ultimo_salario, :pretensao_salarial, ";
                    $sql .= ":informacoes_complementares, :pcd, :alfabetizado, :a1a4, :a5a8, :ensino_medio, :superior, :administracao, ";
                    $sql .= ":contabilidade, :psicologia, :servico_social, :sistema_informacao, :primeiro_emprego, :assistente_administrativo, :assistente_financeiro, :atendimento_balconista, :auxiliar_cozinha, :auxiliar_producao, :baba, :bombeiro, :cabelereira, :camareira, :changueiro, :copeira, :cozinheiro, :cuidadora, :decorador, :depiladora, :detetizacao, :domestica, :eletricista, :embalador, :encanador, :estagiario, :esteticista, :faxineira_diarista, :fiscal_loja, :frentista, :garcon, :gerente_loja, :jardinagem, :lavadeira, :limpador_de_piscina, :marcineiro, :manicure, :massagista, :mecanico, :monitor, :montador_moveis, :motoboy, :motorista, :operador_caixa, :passadeira, :pedreiro, :pintor, :porteiro, :recepcionista, :safrista, :secretaria, :seguranca, :servente_pedreiro, :servico_gerais, :supervisor, :telefonista, :vendedor, :vigia )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":nascimento", $nascimento);
                    $insere->bindValue(":sexo", $sexo);
                    $insere->bindValue(":estado_civil", $estado_civil);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":cnh", $cnh);
                    $insere->bindValue(":ctps", $ctps);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":cidade", $cidade);
                    $insere->bindValue(":referencia", $referencia);
                    $insere->bindValue(":email", $email);
                    $insere->bindValue(":telefone1", $telefone1);
                    $insere->bindValue(":telefone2", $telefone2);
                    $insere->bindValue(":objetivo", $objetivo);
                    $insere->bindValue(":ultimo_salario", $ultimo_salario);
                    $insere->bindValue(":pretensao_salarial", $pretensao_salarial);
                    $insere->bindValue(":informacoes_complementares", $informacoes_complementares);
                    $insere->bindValue(":pcd", $pcd);
                    $insere->bindValue(":alfabetizado", $alfabetizado);
                    $insere->bindValue(":a1a4", $a1a4);
                    $insere->bindValue(":a5a8", $a5a8);
                    $insere->bindValue(":ensino_medio", $ensino_medio);
                    $insere->bindValue(":superior", $superior);
                    $insere->bindValue(":administracao", $administracao);
                    $insere->bindValue(":contabilidade", $contabilidade);
                    $insere->bindValue(":psicologia", $psicologia);
                    $insere->bindValue(":servico_social", $servico_social);
                    $insere->bindValue(":sistema_informacao", $sistema_informacao);
                    $insere->bindValue(":primeiro_emprego", $primeiro_emprego);
                    $insere->bindValue(":assistente_administrativo", $assistente_administrativo);
                    $insere->bindValue(":assistente_financeiro", $assistente_financeiro);
                    $insere->bindValue(":atendimento_balconista", $atendimento_balconista);
                    $insere->bindValue(":auxiliar_cozinha", $auxiliar_cozinha);
                    $insere->bindValue(":auxiliar_producao", $auxiliar_producao);
                    $insere->bindValue(":baba", $baba);
                    $insere->bindValue(":bombeiro", $bombeiro);
                    $insere->bindValue(":cabelereira", $cabelereira);
                    $insere->bindValue(":camareira", $camareira);
                    $insere->bindValue(":changueiro", $changueiro);
                    $insere->bindValue(":copeira", $copeira);
                    $insere->bindValue(":cozinheiro", $cozinheiro);
                    $insere->bindValue(":cuidadora", $cuidadora);
                    $insere->bindValue(":decorador", $decorador);
                    $insere->bindValue(":depiladora", $depiladora);
                    $insere->bindValue(":detetizacao", $detetizacao);
                    $insere->bindValue(":domestica", $domestica);
                    $insere->bindValue(":eletricista", $eletricista);
                    $insere->bindValue(":embalador", $embalador);
                    $insere->bindValue(":encanador", $encanador);
                    $insere->bindValue(":estagiario", $estagiario);
                    $insere->bindValue(":esteticista", $esteticista);
                    $insere->bindValue(":faxineira_diarista", $faxineira_diarista);
                    $insere->bindValue(":fiscal_loja", $fiscal_loja);
                    $insere->bindValue(":frentista", $frentista);
                    $insere->bindValue(":garcon", $garcon);
                    $insere->bindValue(":gerente_loja", $gerente_loja);
                    $insere->bindValue(":jardinagem", $jardinagem);
                    $insere->bindValue(":lavadeira", $lavadeira);
                    $insere->bindValue(":limpador_de_piscina", $limpador_de_piscina);
                    $insere->bindValue(":marcineiro", $marcineiro);
                    $insere->bindValue(":manicure", $manicure);
                    $insere->bindValue(":massagista", $massagista);
                    $insere->bindValue(":mecanico", $mecanico);
                    $insere->bindValue(":monitor", $monitor);
                    $insere->bindValue(":montador_moveis", $montador_moveis);
                    $insere->bindValue(":motoboy", $motoboy);
                    $insere->bindValue(":motorista", $motorista);
                    $insere->bindValue(":operador_caixa", $operador_caixa);
                    $insere->bindValue(":passadeira", $passadeira);
                    $insere->bindValue(":pedreiro", $pedreiro);
                    $insere->bindValue(":pintor", $pintor);
                    $insere->bindValue(":porteiro", $porteiro);
                    $insere->bindValue(":recepcionista", $recepcionista);
                    $insere->bindValue(":safrista", $safrista);
                    $insere->bindValue(":secretaria", $secretaria);
                    $insere->bindValue(":seguranca", $seguranca);
                    $insere->bindValue(":servente_pedreiro", $servente_pedreiro);
                    $insere->bindValue(":servico_gerais", $servico_gerais);
                    $insere->bindValue(":supervisor", $supervisor);
                    $insere->bindValue(":telefonista", $telefonista);
                    $insere->bindValue(":vendedor", $vendedor);
                    $insere->bindValue(":vigia", $vigia);

                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro:' . $error_msg->getMessage();
                }



        if(isset($insere)){
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                $sql = "SELECT Max(id) FROM mcu_sysjob_pessoas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];

                header("Location: index.php?pg=Vpessoa&id={$maid}&sucesso=s");
                exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }//da function
}//d class
?>