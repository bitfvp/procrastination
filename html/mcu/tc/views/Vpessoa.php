<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
    <div class="row">
        <div class="col-md-8">
            <?php include_once("includes/pessoacabecalhotop.php"); ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mt-2">
                        <div class="card-header bg-primary text-dark">
                            Acesse pelo <?php echo $env->env_url;?>mcu/curso
                        </div>
                        <div class="card-body">

                            <div class="mb-2">
                                <?php
                                foreach (fnccursolist() as $item){
                                    echo "<a href='?pg=Vpessoa&id={$_GET['id']}&aca=habcurso&idcurso={$item['id']}' class='btn btn-info m-2'>Habilitar {$item['curso']}</a>";
                                }
                                ?>
                            </div>
                            <table class="table table-sm table-striped">
                                <thead>
                                <tr>
                                    <td>CURSO</td>
                                    <td>ETAPA</td>
                                    <td>DATA</td>
                                    <td>AÇÕES</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sql = "SELECT * FROM mcu_curso_inscrito WHERE pessoa=?";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->bindParam(1,$_GET['id']);
                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                $incricoes = $consulta->fetchAll();
                                $sql=null;
                                $consulta=null;

                                foreach ($incricoes as $item){
                                    echo "<tr>";
                                    echo "<td>".fncgetcurso($item['curso'])['curso']."</td>";
                                    echo "<td>{$item['etapa']}</td>";
                                    echo "<td>".datahoraBanco2data($item['data'])."</td>";
                                    echo "<td>"
                                        . "<a href='?pg=Vpessoa&id={$_GET['id']}&aca=resetacurso&idcurso={$item['curso']}' class='btn btn-warning' title='Resetar'>"
                                        . "<i class='fa fa-sync-alt'></i>"
                                        . "</a>  "
                                        . "<a href='?pg=Vpessoa&id={$_GET['id']}&aca=excluircurso&idcurso={$item['curso']}' class='btn btn-danger' title='Excluir'>"
                                        . "<i class='fa fa-times'></i>"
                                        . "</a>"
                                        . "</td>";
                                    echo "</tr>";
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <?php include_once ("includes/sectionmenulateral.php");?>
        </div>

    </div>



</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>