<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar etapa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="etapasave";
    $etapa=fncgetetapa($_GET['id']);
    $curso=$etapa['curso'];
    $maiorordem=$etapa['ordem'];

}else{
    $a="etapanew";
    $curso=$_GET['idcurso'];
    $sql = "SELECT Max(ordem) FROM mcu_curso_etapa where curso=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$curso);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $maiorordem = $consulta->fetch();
    $maiorordem=$maiorordem[0]+1;
    $sql=null;
    $consulta=null;
}


?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcurso&id={$_GET['idcurso']}&aca={$a}"; ?>" method="post" enctype="multipart/form-data">
        <h3 class="form-cadastro-heading">Cadastro de etapas</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $etapa['id']; ?>"/>
                <input autocomplete="off" id="curso" placeholder="curso" type="hidden" class="form-control" name="curso" value="<?php echo $curso; ?>"/>
                <label for="ordem">ORDEM:</label>
                <input autocomplete="off" id="ordem" placeholder="ordem" type="number" class="form-control" name="ordem" value="<?php echo $maiorordem; ?>"/>
            </div>

            <div class="col-md-6">
                <label for="titulo">TITULO:</label>
                <input autocomplete="off" id="titulo" placeholder="titulo" type="text" class="form-control" name="titulo" value="<?php echo $etapa['titulo']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="tipo">TIPO:</label>
                <select name="tipo" id="tipo" class="form-control">
                    <option selected="" value="<?php if ($etapa['tipo'] == "") {$z = 1;echo $z;} else {echo $etapa['tipo'];} ?>">
                        <?php
                        switch ($etapa['tipo']){
                            case 1:
                                echo "Leitura";
                                break;
                            case 2:
                                echo "Supervisionada";
                                break;
                            default:
                                echo "Leitura";
                                break;
                        }
                        ?>
                    </option>
                    <option value="1">Leitura</option>
                    <option value="2">Supervisionada</option>
                </select>
            </div>

            <div class="col-md-8">
                <label for="imagem">IMAGEM:</label>
                <textarea id="imagem" class="form-control" rows="7" name="imagem"><?php echo $etapa['imagem']; ?></textarea>
            </div>
            <div class="col-md-4">
                <label for="imagem">IMAGEM:</label>
                <img src="<?php echo $etapa['imagem']; ?>" alt="" class="form-control" style="max-height: 180px">
            </div>

            <div class="col-md-6">
                <div class="custom-file mt-2">
                    <input id="arquivo" type="file" class="custom-file-input" name="arquivo" value=""/>
                    <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                </div>
            </div>
            <div class="col-md-6">
                <h6 class="text-danger mt-4">Aceitamos o envio de jpg, jpeg, png com tamnho de até 800KBs</h6>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>