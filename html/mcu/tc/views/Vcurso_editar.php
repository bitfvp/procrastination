<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Curso-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="cursosave";
    $curso=fncgetcurso($_GET['id']);
}else{
    $a="cursonew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcurso_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de cursos</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $curso['id']; ?>"/>
                <label for="curso">CURSO:</label>
                <input autocomplete="off" id="curso" placeholder="curso" type="text" class="form-control" name="curso" value="<?php echo $curso['curso']; ?>"/>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>