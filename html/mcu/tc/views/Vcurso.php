<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_65"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Curso-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $curso=fncgetcurso($_GET['id']);
}else{
    echo "HOUVE ALGUM ERRO";
}
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do curso
                </div>
                <div class="card-body">
                    <blockquote class="blockquote blockquote-info">
                        <header>CURSO: <strong class="text-info"><?php echo $curso['curso']; ?>&nbsp;&nbsp;</strong></header>
                        <h5 class="d-none">
                            exemple: <strong class="text-info">info</strong>
                        </h5>
                    </blockquote>
                    <a href="index.php?pg=Vcurso_editar&id=<?php echo $_GET['id']; ?>" class="btn btn-lg btn-block btn-success">EDITAR CURSO</a>
                </div>
            <!-- fim da col md 4 -->
            </div>
        </div>
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Etapas do curso
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vetapa_editar&idcurso=<?php echo $_GET['id'];?>" class="btn btn btn-success btn-block col-md-6">
                        NOVA ETAPA
                    </a>
                    <br>

                    <table class="table table-sm table-striped table-hover">
                        <thead class="">
                        <tr>
                            <td>ORDEM</td>
                            <td>TITULO</td>
                            <td>TIPO</td>
                            <td>EDITAR</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach (fncetapalist($_GET['id']) as $item){
                            if ($item['tipo']==1){
                                $tipo='<strong class="text-info">Leitura</strong>';
                            }else{
                                $tipo='<strong class="text-danger">Supervisionada</strong>';
                            }
                            echo "<tr>";
                            echo "<td>{$item['ordem']}</td>";
                            echo "<td>{$item['titulo']}</td>";
                            echo "<td>{$tipo}</td>";
                            echo "<td><a href='index.php?pg=Vetapa_editar&idcurso={$_GET['id']}&id={$item['id']}'> Editar</a></td>";
                            echo "<tr>";
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>