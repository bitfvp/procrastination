<div class="container">
    <form action="index.php" method="get" class="col-md-6" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por pessoa..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />

        </div>
    </form>

        <a href="index.php?pg=Vpessoaeditar" class="btn btn-info btn-lg btn-block col-md-6 float-right">
            NOVO CADASTRO
        </a>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>
</div>


<div class="container">
    <table class="table table-striped table-hover table-sm">
        <thead>
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">CPF</th>
            <th scope="col">RG</th>
            <th scope="col">AÇÕES</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination float-right">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$anterior}'><span aria-hidden='true'>&laquo; Anterior</a></li> ";
                        }
                        echo "|";
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>

                        <?php echo $tr;?> pessoa(s) listada(s)
                    </ul>
                </nav>

            </th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        if(isset($_GET['sca']) and $_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        // vamos criar a visualização
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $cpf = $dados["cpf"];
            $rg = $dados["rg"];


            ?>
            <tr>
                <th scope="row" id="<?php echo $id;  ?>">
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id; ?>" title="Ver pessoa">
                        <?php
                        if($_GET['sca']!="") {
                            $sta = CSA;
                            $nnn = $nome;
                            $nn = explode(CSA, $nnn);
                            $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                            echo $n;
                        }else{
                            echo $nome;
                        }
                        ?>
                    </a>
                </th>

                <td>
                    <?php
                        if (isset($_GET['scb']) and $_GET['scb'] != "") {
                            $stb = CSB;
                            $ccc = $cpf;
                            $cc = explode(CSB, $ccc);
                            $c = implode("<span class='text-danger'>{$stb}</span>", $cc);
                            echo $c;
                        } else {
                            echo mask($cpf,'###.###.###_##');
                        }
                    ?></td>
                <td>
                    <?php
                        if (isset($_GET['scc']) and $_GET['scc'] != "") {
                            $stc = CSC;
                            $rrr = $rg;
                            $rr = explode(CSC, $rrr);
                            $r = implode("<span class='text-danger'>{$stc}</span>", $rr);
                            echo $r;
                        } else {
                            echo $rg;
                        }
                    ?>
                </td>

                <td>
                    <a href="index.php?pg=Vpessoaeditar&id=<?php echo $id; ?>" title="Edite os dados dessa pessoa" class="btn btn-sm btn-outline-primary fa fa-pen"></a>
                </td>


            </tr>



            <?php
        }
        ?>
        </tbody>
    </table>
</div>