<?php
if($startactiona==1 && $aca=="habcurso" && isset($_GET['idcurso']) && is_numeric($_GET['idcurso'])){
    $pessoa=$_GET["id"];
    $curso=$_GET["idcurso"];

    $sql = "SELECT id FROM mcu_curso_inscrito WHERE pessoa=? and curso=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$pessoa);
    $consulta->bindParam(2,$curso);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cont = $consulta->rowCount();
    $sql=null;
    $consulta=null;

    if ($cont==0){
        $salvar= new Inscricao();
        $salvar->fnchabilitacurso($pessoa,$curso);
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Essa pessoa já está cadastrada nesse curso!!",
            "type"=>"warning",
        ];
        header("Location: ?pg=Vpessoa&id={$pessoa}");
        exit();
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($startactiona==1 && $aca=="excluircurso" && isset($_GET['idcurso']) && is_numeric($_GET['idcurso'])){
    $pessoa=$_GET["id"];
    $curso=$_GET["idcurso"];

    $sql = "SELECT id FROM mcu_curso_inscrito WHERE pessoa=? and curso=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$pessoa);
    $consulta->bindParam(2,$curso);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cont = $consulta->rowCount();
    $sql=null;
    $consulta=null;

    if ($cont!=0){
        $salvar= new Inscricao();
        $salvar->fncexcluicurso($pessoa,$curso);
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Essa pessoa não tem esse curso!!",
            "type"=>"warning",
        ];
        header("Location: ?pg=Vpessoa&id={$pessoa}");
        exit();
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($startactiona==1 && $aca=="resetacurso" && isset($_GET['idcurso']) && is_numeric($_GET['idcurso'])){
    $pessoa=$_GET["id"];
    $curso=$_GET["idcurso"];

    $sql = "SELECT id FROM mcu_curso_inscrito WHERE pessoa=? and curso=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$pessoa);
    $consulta->bindParam(2,$curso);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cont = $consulta->rowCount();
    $sql=null;
    $consulta=null;

    if ($cont!=0){
        $salvar= new Inscricao();
        $salvar->fncresetacurso($pessoa,$curso);
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Essa pessoa não tem esse curso!!",
            "type"=>"warning",
        ];
        header("Location: ?pg=Vpessoa&id={$pessoa}");
        exit();
    }
}
