<?php
function fncetapalist($idcurso){
    $sql = "SELECT * FROM mcu_curso_etapa where curso=? ORDER BY ordem";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$idcurso);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $etapalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $etapalista;
}

function fncgetetapa($id){
    $sql = "SELECT * FROM mcu_curso_etapa WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getetapa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getetapa;
}
?>