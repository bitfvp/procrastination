<?php
class Inscricao{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnchabilitacurso($pessoa,$curso){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_curso_inscrito ";
                $sql.="(id, pessoa, curso, etapa)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :curso, 1)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $pessoa);
                $insere->bindValue(":curso", $curso);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro no curso realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncexcluicurso($pessoa,$curso){

        //inserção no banco
        try{
            $sql="DELETE FROM mcu_curso_inscrito WHERE pessoa=:pessoa and curso=:curso";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":curso", $curso);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Exclusão do curso realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncresetacurso($pessoa,$curso){

        //inserção no banco
        try{
            $sql="UPDATE `mcu_curso_inscrito` SET etapa=1 where pessoa=:pessoa and curso=:curso";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":pessoa", $pessoa);
            $insere->bindValue(":curso", $curso);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização do curso realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vpessoa&id={$pessoa}");
            exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
?>