<?php
class Etapa{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncetapanew($curso,$ordem,$titulo,$tipo,$imagem){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_curso_etapa ";
                $sql.="(id, curso, ordem, titulo, tipo, imagem)";
                $sql.=" VALUES ";
                $sql.="(NULL, :curso, :ordem, :titulo, :tipo, :imagem)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":curso", $curso);
                $insere->bindValue(":ordem", $ordem);
                $insere->bindValue(":titulo", $titulo);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":imagem", $imagem);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcurso&id={$curso}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncetapaedit( $id,$curso,$ordem,$titulo,$tipo,$imagem){

        //inserção no banco
        try{
            $sql="UPDATE mcu_curso_etapa SET curso=:curso, ordem=:ordem, titulo=:titulo, tipo=:tipo, imagem=:imagem WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":curso", $curso);
            $insere->bindValue(":ordem", $ordem);
            $insere->bindValue(":titulo", $titulo);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":imagem", $imagem);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcurso&id={$curso}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>