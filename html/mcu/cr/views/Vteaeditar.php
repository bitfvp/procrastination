<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_54"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Pessoa Editar-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_p_tea WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoa = $consulta->fetch();

    $sql=null;
    $consulta=null;
}
?>

<main class="container"><!--todo conteudo-->

	<form class="form-signin" action="index.php?pg=Vtea&id=<?php echo $_GET['id'];?>&aca=savetea" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="Salvar"/>
            </div>
		</div>
		<hr>
    <div class="row">
        <div class="col-md-6">
            <label for="">DOC:</label>
            <input autocomplete="off" id="doc" type="text" class="form-control" name="doc" value="<?php echo $pessoa['doc']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="">Responsável:</label>
            <input autocomplete="off" id="responsavel" type="text" class="form-control" name="responsavel" value="<?php echo $pessoa['responsavel']; ?>"/>
        </div>
        <div class="col-md-6">
            <label for="">Telefone:</label>
            <input autocomplete="off" id="telefone" type="text" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
            <script>
                $(document).ready(function(){
                    $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                });
            </script>
        </div>
    </div>

	</form>

	</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>