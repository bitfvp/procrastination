<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_51"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Aguardando Envio idoso-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">


    <!--    inicio de apae-->
    <div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Passe livre Apae aguardando impressão
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_p_apae \n"
                    . "WHERE (mcu_p_apae.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vacarteira" target="_blank" class="btn btn-block btn-success mb-2"> IMPRIMIR PASSES</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Va&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_apae_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há passes aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
    </div>

    <!--    fim de apae-->

	<div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Passe livre idoso aguardando impressão
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_p_idoso \n"
                    . "WHERE (mcu_p_idoso.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vicarteira" target="_blank" class="btn btn-block btn-success mb-2"> IMPRIMIR PASSES</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Vi&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_idoso_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há passes aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
	</div>
<!--    fim de idoso-->

    <div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Credencial idoso aguardando impressão
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_cr_idoso \n"
                    . "WHERE (mcu_cr_idoso.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vcr_idoso_estacionamento" target="_blank" class="btn btn-block btn-success mb-2"> IMPRIMIR ESTACIONAMENTO</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Vcr_idoso&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_cr_idoso_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há credenciais aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
    </div>
    <!--    fim de cr_idoso-->

    <div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Credencial pcd aguardando impressão
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_cr_pcd \n"
                    . "WHERE (mcu_cr_pcd.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vcr_pcd_estacionamento" target="_blank" class="btn btn-block btn-success mb-2"> IMPRIMIR ESTACIONAMENTO</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Vcr_pcd&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_cr_pcd_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há credenciais aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
    </div>
    <!--    fim de cr_pcd-->


    <!--    inicio de fibro-->
    <div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Credencial de fibromialgia aguardando impressão
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_p_fibro \n"
                    . "WHERE (mcu_p_fibro.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vfcarteira" target="_blank" class="btn btn-success mb-2"> CARTEIRA</a>
                <a href="index.php?pg=Vfestacionamento" target="_blank" class="btn btn- btn-success mb-2"> ESTACIONAMENTO</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Vf&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_fibro_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há passes aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
    </div>


    <!--    inicio de TEa-->
    <div class="col-md-6">
        <div class="card mb-2">
            <div class="card-header bg-info text-light">
                Credencial de TEA
            </div>
            <div class="card-body">
                <?php
                $sql = "SELECT * \n"
                    . "FROM mcu_p_tea \n"
                    . "WHERE (mcu_p_tea.impressao)=1 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lista = $consulta->fetchAll();
                $listaCont = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                ?>
                <a href="index.php?pg=Vteacarteira" target="_blank" class="btn btn-success mb-2"> CARTEIRA</a>
                <a href="index.php?pg=Vteaestacionamento" target="_blank" class="btn btn-success mb-2"> ESTACIONAMENTO</a>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>
                            NOME
                        </th>
                        <th>
                            AÇÃO
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($listaCont!=0){
                        $cont=0;
                        foreach ($lista as $lt) {
                            $cont++;
                            ?>
                            <tr class="<?php echo ($cont>4)? "bg-danger" : "";?>">
                                <td><a href="index.php?pg=Vtea&id=<?php echo $lt['pessoa'];?>"><?php echo fncgetpessoa($lt['pessoa'])['nome'];?></a></td>
                                <td><a href='?pg=Vpasse&aca=bot_tea_imprime2&id=<?php echo $lt['pessoa'];?>' class='btn btn-danger btn-sm'>Desativar</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        echo "<tr><td colspan='2' class='text-success'>Não há passes aguardando para imprimir!!</td></tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer text-danger text-center">
                São geradas apenas 4 passes por vez.
            </div>
        </div>
    </div>
	
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>