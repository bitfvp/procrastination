<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_50"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Carteira-".$env->env_titulo;
$css="teaestacionamento";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * "
    . "FROM mcu_p_fibro "
    . "WHERE (((mcu_p_fibro.impressao)=1)) limit 0,4";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carteiras = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>

<div class="container">
    <?php
    $cont=0;
    foreach ($carteiras as $carteira){
    $pe=fncgetpessoa($carteira['pessoa']);
    $cont++;
        if ($cont>1){
            echo "<div style='page-break-before:always;'>&nbsp</div>";
        }
    ?>
            <div class="todo">
                <div class="blocoA">
                    <img src="<?php echo $env->env_estatico; ?>img/estacionamento1.png" alt="">
                </div>
                <div class="blocoB">
                    <div class="topo">
                        <div class="logo">
                            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" alt="">
                        </div>
                        <div class="topo_info">
                            <h2>PREFEITURA MUNICIPAL DE MANHUAÇU</h2>
                            <h3>SECRETARIA MUNICIPAL DO TRABALHO<br>E DESENVOLVIMENTO SOCIAL</h3>
                        </div>
                    </div>
                    <h1 class="doc">ESTACIONAMENTO VAGA ESPECIAL</h1>
                    <h5 class="lei">CONFORME LEI MUNICIPALNº 3940 DE 02 DE MAIO DE 2019</h5>
                    <h3 class="reg">Nº DO REGISTRO: 0000<?php echo $carteira['carteira']; ?>-F</h3>
                    <hr class="hr1">
                    <h4 class="emissao">DATA DE EMISSÃO: <?php echo date('d/m/Y'); ?></h4>
                    <h4 class="validade">DATA DE VALIDADE: <?php echo date('d/m/');
                        $zzz=date('Y')+5;
                        echo $zzz;
                        ?></h4>
                    <h4 class="info2">CREDENCIAL EMITIDA EM FAVOR DA PESSOA <br>PORTADORA DE FIBROMIALGIA</h4>

                </div>
            </div>

        <div class="todo topomaismargem">
            <div class="blocoC">
                <h4>NOME DO BENEFICIÁRIO: <?php echo strtoupper($pe['nome']); ?></h4>
                <h4>REGRAS DE UTILIZAÇÃO</h4>
                <h5>1. A autorização concedida por meio deste cartão somente terá validade se o mesmo for apresentado
                no original e preencher as seguintes condições:</h5>
                <h5 class="paragrafo">1.1. Estiver colocado sobre o painel do veículo, com frente voltada para cima;</h5>
                <h5 class="paragrafo">1.2. For apresentado à autoridade de trânsito ou aos seus agentes, sempre que solicitado. </h5>
                <h5>2. Este cartão de autorização poderá ser recolhido e o ato da autorização suspenso ou cassado, a
                    qualquer tempo, a critério do órgão de trânsito, especialmente se verificada irregularidade em sua
                    utilização, considerando-se como tal, dentre outros: </h5>
                <h5 class="paragrafo">2.1. O empréstimo do cartão a terceiros; </h5>
                <h5 class="paragrafo">2.2. O uso de cópia do cartão, efetuada por qualquer processo; </h5>
                <h5 class="paragrafo">2.3. O porte do cartão com rasuras ou falsificado; </h5>
                <h5 class="paragrafo">2.4. O uso do cartão em desacordo com as disposições nele contidas ou na legislação
                    pertinente, especialmente se constatado pelo agente que o veículo por ocasião da
                    utilização da vaga especial, não serviu para o transporte do deficiente físico;</h5>
                <h5 class="paragrafo">2.5. O uso do cartão com a validade vencida. </h5>
                <h5>3. A presente autorização somente é válida para estacionar nas vagas devidamente sinalizadas com o
                    Símbolo Internacional de Acesso, especialmente criadas pelo órgão de trânsito para esse fim. </h5>
                <h5>4. Esta autorização também permite o uso em vagas de Estacionamento Rotativo Regulamentado,
                    gratuito ou pago, sinalizadas com o Símbolo Internacional de Acesso, sendo obrigatória a
                    utilização conjunta do Cartão do Estacionamento, bem como a obediência às suas normas de
                    utilização.</h5>
                <h5>5. O desrespeito ao disposto neste cartão de autorização, bem como às demais regras de trânsito e a
                    sinalização local, sujeitará o infrator as medidas administrativas, penalidades e pontuações
                    previstas em lei.</h5>
            </div>
        </div>
        <?php
    }?>
</div>

</body>
</html>