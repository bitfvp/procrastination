<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_52"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Pessoa Editar-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_p_pcd WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoa = $consulta->fetch();

    $sql=null;
    $consulta=null;
}
?>

<main class="container"><!--todo conteudo-->

	<form class="form-signin" action="index.php?pg=Vp&id=<?php echo $_GET['id'];?>&aca=savepcd" method="post">
        <div class="row">
            <div class="col-md-6">
                <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block" value="Salvar"/>
            </div>
		</div>
		<hr>

	<div class="row">
			<div class="col-md-3">
			<label   for="possui_acomp" >Possui Acompanhante</label>
		<select name="possui_acomp" id="possui_acomp" class="form-control">
			// vamos criar a visualização
			<option selected="" value="<?php if($pessoa['possui_acomp']==""){$z=0; echo $z;}else{ echo $pessoa['possui_acomp'];} ?>">
				<?php
				if($pessoa['possui_acomp']==0){echo"Não";}
				if($pessoa['possui_acomp']==1){echo"Sim";} ?>
			</option>
			<option value="0">Não</option>
			<option value="1">Sim</option>
		</select>
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-5">
		<label for="">Acompanhante 1</label><input autocomplete="off" id="acompanhante_1" type="text" class="form-control" name="acompanhante_1" value="<?php echo $pessoa['acompanhante_1']; ?>"/>
	</div>
		<div class="col-md-5">
		<label for="">Acompanhante 2</label><input autocomplete="off" id="acompanhante_2" type="text" class="form-control" name="acompanhante_2" value="<?php echo $pessoa['acompanhante_2']; ?>"/>
	</div>
	</div>

	<hr>
	<div class="row">
			<div class="col-md-3">
			<label   for="aprovado" >Aprovado</label>
		<select name="aprovado" id="aprovado" class="form-control">
			// vamos criar a visualização
			<option selected="" value="<?php if($pessoa['aprovado']==""){$z=0; echo $z;}else{ echo $pessoa['aprovado'];} ?>">
				<?php
				if($pessoa['aprovado']==0){echo"Aguardando";}
				if($pessoa['aprovado']==1){echo"Sim";}
				if($pessoa['aprovado']==2){echo"Negado";} ?>
			</option>
			<option value="0">Aguardando</option>
			<option value="1">Sim</option>
			<option value="2">Negado</option>
		</select>
	</div>
	</div>
	

	</form>

	</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>