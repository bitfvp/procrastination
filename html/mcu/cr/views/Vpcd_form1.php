<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_52"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Formulário 1 dados-".$env->env_titulo;
$css="pcdform1";

include_once("{$env->env_root}includes/head.php");

$idc=$_GET['id'];
$sql = "SELECT * \n"
    . "FROM mcu_p_pcd \n"
    . "WHERE (((mcu_p_pcd.pessoa)=:pessoa))";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa", $idc);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carteira = $consulta->fetch();
    $sql=null;
    $consulta=null;
    $pes=fncgetpessoa($carteira['pessoa']);
?>

<div class="container">

    <div class="hender">
        <div class="h_imagem">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg"  alt="">
        </div>
        <div class="h_titulo">
        <br><br><br><br><br>
            <h3>PREFEITURA MUNICIPAL DE MANHUAÇU</h3>
            <h4>SECRETARIA MUNICIPAL DE TRABALHO E DESENVOLVIMENTO SOCIAL</h4>
        </div>
    </div>
    <div class="codd"><h3>Processo: A<?php echo $carteira['carteira']; ?></h3></div>

    <div class="main">
        <h3>REQUERIMENTO DA CARTEIRA DE PASSE LIVRE - PESSOA COM DEFICIÊNCIA</h3>
        <hr><hr>
        <h4>Nome do(a) beneficiário : <strong><?php echo $pes['nome']; ?></strong></h4>
        <hr>
        <h4>Data de nascimento : <strong><?php echo datahoraBanco2data($pes['nascimento'])." <i>".Calculo_Idade($pes['nascimento'])." anos</i>"; ?></strong></h4>
        <hr>
        <h4>Telefone : <strong><?php echo $pes['telefone']; ?></strong></h4>
        <hr>
        <h4>RG : <strong><?php echo $pes['rg']; ?></strong></h4>
        <hr>
        <h4>CPF : <strong><?php echo mask($pes['cpf'],'###.###.###-##'); ?></strong></h4>
        <hr>
        <h4>Pai : <strong><?php echo $pes['pai']; ?></strong></h4>
        <hr>
        <h4>Mãe : <strong><?php echo $pes['mae']; ?></strong></h4>
        <hr>
        <h4>Endereço : <strong><?php echo $pes['endereco']; ?>   <?php echo $pes['numero']; ?></strong></h4>
        <hr>
        <h4>Bairro : <strong>
        <?php fncgetbairro($carteira['bairro']);
        echo fncgetbairro($pes['bairro'])['bairro'];
        ?>
        </strong></h4>
        <hr>
    </div>

    <div class="main2">
    <h3>O formulário deve conter os documentos abaixo, para emissão da carteira de passe livre municipal</h3>
    <hr>
        <h4>1.   Cópia do documento de identidade;</h4>
        <h4>2.   Cópia do CPF;</h4>
        <h4>3.   Cópia do comprovante atualizado de endereço (luz ou água);</h4>
        <h4>4.   Laudo médico comprovando  deficiência, carimbado, assinado e datado pelo médico, com identificação do CRM;</h4>
        <h4>5.   CADUNICO-Folha resumo, cadastro deve ser feito no setor do cadastro unico, (Monsenhor Gonzalez 484, segundo andar, centro Mannhuaçu-MG);</h4>
        <h4>6.   Formulário da SMTDS preenchido e assinado pelo profissional responsável, credenciado pelo municipio, pela elaboração do laudo médico-pericial.</h4>
    </div>
        <br>
        <br>
        <br>
        <div class="fot">
            <h3><?php echo date('d/m/Y'); ?></h3>
            <h4>Afirmo, sob pena de ser responsabilizado civil e criminalmente, que as informações acima são verdadeiras.</h4>
            <h4>Nestes termos peço deferimento.</h4>
        </div>

        <div class="foot">
            <h4>_____________________________________________________________________</h4>
            <h4>
                Assinatura da pessoa com deficência e/ou seu representante legal.
            </h4>
        </div>
        
</div>

</body>
</html>