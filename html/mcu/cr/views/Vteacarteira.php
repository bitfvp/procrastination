<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_50"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Carteira-".$env->env_titulo;
$css="teacarteira";
include_once("{$env->env_root}includes/head.php");

$sql = "SELECT * "
    . "FROM mcu_p_tea "
    . "WHERE (((mcu_p_tea.impressao)=1)) limit 0,4";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carteiras = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>

<div class="container">
    <?php
    foreach ($carteiras as $carteira){
    $pe=fncgetpessoa($carteira['pessoa']);
    ?>

    <div class="ladoa">
        <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" style="width: 56px; height: 56px; float: left;" alt="">
        <h4 style="float: left; text-align: center; "> Prefeitura Municipal de Manhuaçu</h4>
        <h6 style="float: left; text-align: center; ">Secretaria Municipal do Trabalho e Desenvolvimento Social</h6>
        <br><br>
        <h6 style="float: left; text-align: center; "><strong>CIPTEA - Carteira de Identificação da Pessoa <BR>Com Transtorno do Espectro Autista</strong></h6>
        <div class="c_numero">#<?php echo $carteira['carteira']; ?></div>
        <div class="foto"></div>
        <div class="principal">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" alt="">
            <h4>Nome: <strong><?php echo strtoupper($pe['nome']); ?></strong></h4>
            <h4>CPF: <strong><?php echo $pe['cpf']; ?></strong></h4>
            <h4>RG: <strong><?php echo $pe['rg']; ?></strong></h4>
            <h4>Nasc.: <strong><?php echo dataBanco2data($pe['nascimento']); ?></strong></h4>
            <h5>End.: <strong><?php echo strtoupper($pe['endereco']); if ($pe['numero']!=0){echo " ".$pe['numero'];}else{echo " S/N";}  ?></strong></h5>
            <h5>Bairro:
                <strong>
                    <?php
                    $cadbairro=fncgetbairro($pe['bairro']);
                    echo $cadbairro['bairro'];
                    ?>
                </strong>
            </h5>

        </div>
        <div class="fot">
        <div class="emissao">Emissão: <?php echo date('d/m/Y'); ?>
            <br>
            Validade: <?php echo date('d/m/');
            $zzz=date('Y')+5;
            echo $zzz;
             ?>
        </div>
        <div class="assisA">
        <div class="assis">
        Assistente Social
        </div>
        </div>

        <div class="secA">
        <div class="sec">
        Secretaria Mun.Ass.Social
        </div>
        </div>

        </div>
    </div>
    <!-- //////////////////////////////////// -->
    <div class="ladob">
        <br>
        <h4>AO PORTADOR É DADO O DIREITO DE</h4>
        <h4>ATENDIMENTO PREFERENCIAL DURANTE TODO HORÁRIO DE EXPEDIENTE.</h4>
        <h6>DE ACORDO COM A LEI MUNICIPAL<br> N.º 4042 DE 8 DE JUNHO DE 2020</h6>
        <br>
        <h6>A EMPRESA PODERÁ SOLICITAR PARA COMPROVAÇÃO UM DOCUMENTO DO USUÁRIO</h6>
        <br>
        <h3><strong>USO PESSOAL E INTRANSFERÍVEL</strong></h3>
        <hr>
        <h6>Responsável:</h6>
        <h5>
            <?php echo strtoupper($carteira['responsavel']); ?>
            <br>
            <?php echo strtoupper($carteira['doc']); ?>
            <br>
            <?php echo strtoupper($carteira['telefone']); ?>
        </h5>
    </div>
        <hr style="width: 100%;">
        <?php
    }?>
</div>

</body>
</html>