<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_51"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Aguardando Envio idoso-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-8">
<?php
$sql = "SELECT mcu_p_idoso.id, mcu_p_idoso.pessoa, mcu_p_idoso.carteira, mcu_pessoas.nome, mcu_p_idoso.foto, mcu_p_idoso.envio\n"
    . "FROM mcu_pessoas INNER JOIN mcu_p_idoso ON mcu_pessoas.id = mcu_p_idoso.pessoa\n"
    . "WHERE (((mcu_p_idoso.foto)=1) AND ((mcu_p_idoso.envio)=0))";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$lista = $consulta->fetchAll();
$listaCont = $consulta->rowCount();
$sql=null;
$consulta=null;

if ($listaCont!=0){
foreach ($lista as $lt) {
    echo "<div class=\"well\">";
    echo "Numero do Pedido: A". $lt['carteira'];
    echo "<br>Nome: ". $lt['nome']."<br>";
    echo "<a href='?pg=Viaguardando&aca=idosoenviado&id={$lt['pessoa']}' class='btn btn-danger'>";
    echo "Enviar ";
    echo "</a>";
    echo "</div>";
}
}else{
    echo "<h1>Não há carteiras aguardando para enviar!!</h1>";
}
?>

	</div>
	<div class="col-md-4">
    </div>
	
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>