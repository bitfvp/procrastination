<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_52"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Status PCD-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped table-sm">
            <thead class="thead-dark">
            <tr>
                <th>Pedido</th>
                <th>Nome</th>
                <th>Aprovado</th>
                <th>possui acomp.</th>
                <th>Foto tirada</th>
                <th>Enviado</th>
                <th>Status cartão</th>
            </tr>
            </thead>
            <tbody>
<?php
$sql = "SELECT\n"
    . "mcu_p_pcd.id, mcu_p_pcd.pessoa, mcu_pessoas.nome, mcu_p_pcd.carteira, mcu_p_pcd.aprovado, mcu_p_pcd.possui_acomp, mcu_p_pcd.foto, mcu_p_pcd.envio, mcu_p_pcd.cartao_feito\n"
    . "FROM\n"
    . "mcu_pessoas\n"
    . "INNER JOIN mcu_p_pcd ON mcu_pessoas.id = mcu_p_pcd.pessoa\n"
    . "ORDER BY\n"
    . "mcu_p_pcd.carteira DESC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$lista = $consulta->fetchAll();
$sql=null;
$consulta=null;

foreach ($lista as $lt) {
    echo "<tr>";
    echo "<td>{$lt['carteira']}</td>";
    echo "<td><a href='index.php?pg=Vp&id={$lt['pessoa']}'>{$lt['nome']}</a></td>";
    if ($lt['aprovado']==0){echo "<td class='text-muted bg-info'>Aguardando...</td>";}
    if ($lt['aprovado']==1){echo "<td class='text-muted bg-success'>Sim</td>";}
    if ($lt['aprovado']==2){echo "<td class='text-muted bg-danger'>Ocorreu um problema!!</td>";}

    echo ($lt['possui_acomp']==1) ? "<td class='text-muted bg-success'>Sim</td>": "<td class='text-muted bg-info'>Não</td>";
    echo ($lt['foto']==1) ? "<td class='text-muted bg-success'>Sim</td>": "<td class='text-muted bg-info'>Não</td>";
    echo ($lt['envio']==1) ? "<td class='text-muted bg-success'>Sim</td>": "<td class='text-muted bg-info'>Não</td>";

    if ($lt['cartao_feito']==0){echo "<td class='text-muted bg-info'>Aguardando...</td>";}
    if ($lt['cartao_feito']==1){echo "<td class='text-muted bg-success'>Confeccionado</td>";}
    if ($lt['cartao_feito']==2){echo "<td class='text-muted bg-danger'>Ocorreu um problema!!</td>";}

    echo "</tr>";
}
?>
            </tbody>
        </table>
	</div>






    <?php

        $sql = "SELECT
	COUNT(id) AS total,
	YEAR (mcu_p_pcd.cadastro)
FROM
	mcu_p_pcd
GROUP BY
	YEAR (mcu_p_pcd.cadastro)
ORDER BY
	mcu_p_pcd.cadastro DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $qcrano = $consulta->fetchAll();
        $sql=null;
        $consulta=null;


        $sql = "SELECT
	COUNT(id) AS total,
	YEAR (mcu_p_pcd.cadastro),
	MONTH (mcu_p_pcd.cadastro)
FROM
	mcu_p_pcd
GROUP BY
	YEAR (mcu_p_pcd.cadastro),
	MONTH (mcu_p_pcd.cadastro)
ORDER BY
	mcu_p_pcd.cadastro DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $qcrmes = $consulta->fetchAll();
        $sql=null;
        $consulta=null;


    ?>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-info text-light">
                Histórico de novas aberturas de processo
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>PERÍODO</th>
                        <th>QUANTIDADE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    foreach($qcrano as $dados){
                        echo "<tr>";
                        echo "<td>".utf8_encode(strftime('%Y', strtotime("{$dados[1]}-01-01")))."</td>";
                        echo "<td>{$dados[0]} Novos</td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>

                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>PERÍODO</th>
                        <th>QUANTIDADE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $contar=0;
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    foreach($qcrmes as $dados){
                        echo "<tr>";
                        echo "<td>".utf8_encode(strftime('%B de %Y', strtotime("{$dados[1]}-{$dados[2]}-01")))."</td>";
                        echo "<td>{$dados[0]} Novos</td>";
                        echo "</tr>";
                        $contar+=$dados[0];
                    }
                    ?>
                    </tbody>
                </table>
                <div class="card-footer text-info text-right">
                    Total:<?php echo $contar;?>
                </div>
            </div>
        </div>
    </div>





    <?php

//    $sql = "SELECT
//Count( DISTINCT(mcu_p_pcd_at.pessoa)) AS total,YEAR(mcu_p_pcd_at.`data`), MONTH(mcu_p_pcd_at.`data`)
//FROM
//mcu_p_pcd_at
//WHERE (((mcu_p_pcd_at.descricao)='Processo Iniciado') OR ((mcu_p_pcd_at.descricao)='Processo Reiniciado')  )
//GROUP BY
//	YEAR (mcu_p_pcd_at.`data`),
//	MONTH (mcu_p_pcd_at.`data`)
//ORDER BY
//	mcu_p_pcd_at.`data` DESC ";
//    global $pdo;
//    $consulta = $pdo->prepare($sql);
//    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
//    $qcrmes = $consulta->fetchAll();
//    $sql=null;
//    $consulta=null;

    $sql = "SELECT 
Count( DISTINCT(mcu_p_pcd_at.pessoa)) AS total,YEAR(mcu_p_pcd_at.`data`)
FROM
mcu_p_pcd_at
WHERE (((mcu_p_pcd_at.descricao)='Processo Iniciado') OR ((mcu_p_pcd_at.descricao)='Processo Reiniciado')  ) 
GROUP BY
	YEAR (mcu_p_pcd_at.`data`)
ORDER BY
	mcu_p_pcd_at.`data` DESC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $qcrano = $consulta->fetchAll();
    $sql=null;
    $consulta=null;



    ?>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-info text-light">
                Histórico de aberturas de processo (novos+renovações)
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>PERÍODO</th>
                        <th>QUANTIDADE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                $contar=0;
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    foreach($qcrano as $dados){
                        echo "<tr>";
                        echo "<td>".utf8_encode(strftime('%Y', strtotime("{$dados[1]}-01-01")))."</td>";
                        echo "<td>{$dados[0]} Novos</td>";
                        echo "</tr>";
                $contar+=$dados[0];
                    }
                    ?>
                    </tbody>
                </table>
                <div class="card-footer text-info text-right">
                    Total:<?php echo $contar;?>
                </div>

<!--                <table class="table table-sm">-->
<!--                    <thead>-->
<!--                    <tr>-->
<!--                        <th>PERÍODO</th>-->
<!--                        <th>QUANTIDADE</th>-->
<!--                    </tr>-->
<!--                    </thead>-->
<!--                    <tbody>-->
<!--                    --><?php
//                    $contar=0;
//                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
//                    date_default_timezone_set('America/Sao_Paulo');
//                    foreach($qcrmes as $dados){
//                        echo "<tr>";
//                        echo "<td>".utf8_encode(strftime('%B de %Y', strtotime("{$dados[1]}-{$dados[2]}-01")))."</td>";
//                        echo "<td>{$dados[0]} Novos</td>";
//                        echo "</tr>";
//                        $contar+=$dados[0];
//                    }
//                    ?>
<!--                    </tbody>-->
<!--                </table>-->
<!--                <div class="card-footer text-info text-right">-->
<!--                    Total:--><?php //echo $contar;?>
<!--                </div>-->
            </div>
        </div>
    </div>



	
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>