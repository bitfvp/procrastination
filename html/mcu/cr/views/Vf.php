<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_51"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-8">
        <?php
        include_once ("includes/pessoa_cabecalho.php");
        include_once ("includes/fibro/fibro_cabecalho.php");

        if (!empty($processo)) {
            include_once("includes/fibro/fibro_atform.php");
            include_once("includes/fibro/fibro_at.php");
        }
        ?>

	</div>
	<div class="col-md-4">

        <?php
        if (!empty($processo)){
            include_once ("includes/fibro/fibro_arquivosform.php");
            include_once ("includes/fibro/fibro_arquivos.php");
        }
        ?>
    </div>
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>