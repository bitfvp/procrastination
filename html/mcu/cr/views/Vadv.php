<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_53"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}


$page="Advertências-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-7">
            <?php include_once ("includes/pessoa_cabecalho.php");

            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT *\n"
                        . "FROM mcu_p_adv \n"
                        . "WHERE \n"
                        . "mcu_p_adv.pessoa = ?\n"
                        . "ORDER BY\n"
                        . "mcu_p_adv.`data` DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $ativi = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>

            <div id="pointofview" class="card">
                <div class="card-header bg-info text-light">
                    Histórico de movimentações
                </div>
                <div class="card-body">
                    <h5>
                        <?php
                        foreach ($ativi as $at) {
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-info">
                                “<strong class="text-success"><?php echo $at['descricao']; ?></strong>”
                                <br>
                                <strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>

                            </blockquote>
                            <?php
                        }
                        ?>
                    </h5>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Formulário de advertências
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vadv&aca=newadv&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000"
                                          class="form-control" rows="4" name="descricao"></textarea>
                                <span id="cont">1000</span>/1000
                            </div>
                        </div>
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>