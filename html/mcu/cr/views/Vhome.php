<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '1200;URL={$env->env_url_mod}'>";
include_once("includes/topo.php");
?>

<style>
    .ul-pac-man {
        font-family: "Roboto";
        font-size: 13px;
        line-height: 1.5em;
        margin: 5px 0 15px;
        padding: 0;
    }
    .ul-pac-man li {
        list-style: none;
        position: relative;
        padding: 0 0 0 20px;
    }

    ul.pac-man li::before {
        content: "";
        position: absolute;
        left: 0px;
        top: 5px;
        width: 0px;
        height: 0px;
        border-right: 5px solid transparent;
        border-top: 5px solid #ff6600;
        border-left: 5px solid #ff6600;
        border-bottom: 5px solid #ff6600;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }



</style>

<div class="modal fade bd-example-modal-lg" id="alerta" tabindex="-1" role="dialog" aria-labelledby="modalalertaLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg bg-dark" role="document">
        <div class="modal-content">
            <div id="mmsgs" class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="text-center text-warning"><i class="fa fa-exclamation-triangle"></i> Serviços e requisitos</h4>
                <h5 class="pl-5">
                    <ul class="ul-pac-man pac-man">
                        <li>IDOSO (azul 60-64 anos)
                            <ul class="pac-man">
                                <li>xerox rg, cpf,comprovante de residência</li>
                                <li>cadastro único (cadúnico)</li>
                                <li>1 foto 3x4</li>
                                <li>--entrega em 15 dias</li>
                            </ul>
                        </li>
                        <li>IDOSO (cartão 65 anos)
                            <ul class="pac-man">
                                <li>originais rg, comprovante de residência</li>
                                <li>--entrega em 30 dias</li>
                            </ul>
                        </li>
                        <li>IDOSO FEDERAL
                            <ul class="pac-man">
                                <li>cadúnico+90 dias, 1 foto 3X4</li>
                                <li>--entrega em 7 dias</li>
                            </ul>
                        </li>
                        <li>PCD
                            <ul class="pac-man">
                                <li>Cadúnico de antemão</li>
                                <li>Originais da identidade, cpf, comprovante de residência</li>
                                <li>Abertura de processo e renovação</li>
                            </ul>
                        </li>
                        <li>FIBROMIALGIA
                            <ul class="pac-man">
                                <li>xerox da identidade, cpf, comprovante de residência, título de eleitor</li>
                                <li>laudo médico ou atestado cid.10 m79-7</li>
                                <li>1 foto 3x4 </li>
                                <li>--entrega em 7 dias</li>
                            </ul>
                        </li>
                        <li>TEA
                            <ul class="pac-man">
                                <li>xerox do rg e cpf do titular</li>
                                <li>xerox do rg e cpf do representante legal</li>
                                <li>xerox do comprovante de residência</li>
                                <li>xerox do laudo médico</li>
                                <li>1 foto 3x4</li>
                                <li>--entrega em 30 dias</li>
                            </ul>
                        </li>
                    </ul>
                </h5>
            </div>
        </div>
    </div>
</div>

<?php if (isset($_SESSION['tempcontalerta']) and $_SESSION['tempcontalerta']!=1){?>
<script>
    $('#alerta').modal('toggle');
</script>
<?php

    $_SESSION['tempcontalerta']=1;
}?>


<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    APAE
                </div>
                <div class="card-body">
                    <p>
                    <table class="table table-stripe table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT mcu_p_apae.id, mcu_p_apae.carteira, mcu_p_apae.pessoa, mcu_pessoas.nome\n"
                            . "FROM\n"
                            . "	mcu_pessoas\n"
                            . "INNER JOIN mcu_p_apae ON mcu_pessoas.id = mcu_p_apae.pessoa\n"
                            . "WHERE mcu_p_apae.foto = 1 AND mcu_p_apae.envio = 1 AND mcu_p_apae.cartao_feito = 0\n"
                            . "ORDER BY\n"
                            . "	mcu_pessoas.nome ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lista = $consulta->fetchAll();
                        $listaCont = $consulta->rowCount();
                        $sql=null;
                        $consulta=null;

                        if ($listaCont!=0){
                            foreach ($lista as $lt) {
                                echo "<tr>";
                                echo "<td>";
                                echo "<a href=\"";
                                echo $env->env_url. "mcu/passelivre/index.php?pg=Va&id=". $lt['pessoa']."\">";
                                echo $lt['nome'];
                                echo "</a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        }else{
                            echo "<tr><td class='text-danger'>Não há pedidos aguardando resposta!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>
        </div><!-- .col-md-04 -->

        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    IDOSO
                </div>
                <div class="card-body">
                    <p>
                    <table class="table table-stripe table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT mcu_p_idoso.id, mcu_p_idoso.carteira, mcu_p_idoso.pessoa,mcu_pessoas.nome\n"
                            . "FROM mcu_p_idoso INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_p_idoso.pessoa \n"
                            . "WHERE\n"
                            . "mcu_p_idoso.foto = 1 AND mcu_p_idoso.envio = 1 AND mcu_p_idoso.segunda_via = 0 AND mcu_p_idoso.cartao_feito = 0\n"
                            . "ORDER BY\n"
                            . "mcu_pessoas.nome ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lista = $consulta->fetchAll();
                        $listaCont = $consulta->rowCount();
                        $sql=null;
                        $consulta=null;

                        if ($listaCont!=0){
                            foreach ($lista as $lt) {
                                echo "<tr>";
                                echo "<td>";
                                echo "<a href=\"";
                                echo $env->env_url. "mcu/passelivre/index.php?pg=Vi&id=". $lt['pessoa']."\">";
                                echo $lt['nome'];
                                echo "</a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        }else{
                            echo "<tr><td class='text-danger'>Não há pedidos aguardando resposta!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    IDOSO SEGUNDA VIA
                </div>
                <div class="card-body">
                    <p>
                    <table class="table table-stripe table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT mcu_p_idoso.id, mcu_p_idoso.carteira, mcu_p_idoso.pessoa,mcu_pessoas.nome\n"
                            . "FROM mcu_p_idoso INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_p_idoso.pessoa \n"
                            . "WHERE\n"
                            . "mcu_p_idoso.foto = 1 AND mcu_p_idoso.envio = 1 AND mcu_p_idoso.segunda_via = 1 AND mcu_p_idoso.cartao_feito = 0\n"
                            . "ORDER BY\n"
                            . "mcu_pessoas.nome ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lista = $consulta->fetchAll();
                        $listaCont = $consulta->rowCount();
                        $sql=null;
                        $consulta=null;

                        if ($listaCont!=0){
                            foreach ($lista as $lt) {
                                echo "<tr>";
                                echo "<td>";
                                echo "<a href=\"";
                                echo $env->env_url. "mcu/passelivre/index.php?pg=Vi&id=". $lt['pessoa']."\">";
                                echo $lt['nome'];
                                echo "</a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        }else{
                            echo "<tr><td class='text-danger'>Não há pedidos aguardando resposta!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>
        </div><!-- .col-md-04 -->

        <div class="col-md-4">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    PCD
                </div>
                <div class="card-body">
                    <p>
                    <table class="table table-stripe table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT mcu_p_pcd.id, mcu_p_pcd.carteira, mcu_p_pcd.pessoa,mcu_pessoas.nome\n"
                            . "FROM mcu_p_pcd INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_p_pcd.pessoa \n"
                            . "WHERE\n"
                            . "mcu_p_pcd.foto = 1 AND mcu_p_pcd.envio = 1 AND mcu_p_pcd.segunda_via = 0 AND mcu_p_pcd.cartao_feito = 0 AND mcu_p_pcd.aprovado = 1\n"
                            . "ORDER BY\n"
                            . "mcu_pessoas.nome ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lista = $consulta->fetchAll();
                        $listaCont = $consulta->rowCount();
                        $sql=null;
                        $consulta=null;

                        if ($listaCont!=0){
                            foreach ($lista as $lt) {
                                echo "<tr>";
                                echo "<td>";
                                echo "<a href=\"";
                                echo $env->env_url. "mcu/passelivre/index.php?pg=Vp&id=". $lt['pessoa']."\">";
                                echo $lt['nome'];
                                echo "</a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        }else{
                            echo "<tr><td class='text-danger'>Não há pedidos aguardando resposta!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    PCD SEGUNDA VIA
                </div>
                <div class="card-body">
                    <p>
                    <table class="table table-stripe table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT mcu_p_pcd.id, mcu_p_pcd.carteira, mcu_p_pcd.pessoa,mcu_pessoas.nome\n"
                            . "FROM mcu_p_pcd INNER JOIN mcu_pessoas ON mcu_pessoas.id = mcu_p_pcd.pessoa \n"
                            . "WHERE\n"
                            . "mcu_p_pcd.foto = 1 AND mcu_p_pcd.envio = 1 AND mcu_p_pcd.segunda_via = 1 AND mcu_p_pcd.cartao_feito = 0 AND mcu_p_pcd.aprovado = 1\n"
                            . "ORDER BY\n"
                            . "mcu_pessoas.nome ASC";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                        $lista = $consulta->fetchAll();
                        $listaCont = $consulta->rowCount();
                        $sql=null;
                        $consulta=null;

                        if ($listaCont!=0){
                            foreach ($lista as $lt) {
                                echo "<tr>";
                                echo "<td>";
                                echo "<a href=\"";
                                echo $env->env_url. "mcu/passelivre/index.php?pg=Vp&id=". $lt['pessoa']."\">";
                                echo $lt['nome'];
                                echo "</a>";
                                echo "</td>";
                                echo "</tr>";
                            }
                        }else{
                            echo "<tr><td class='text-danger'>Não há pedidos aguardando resposta!!</td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                    </p>
                </div>
            </div>
        </div><!-- .col-md-04 -->

    </div> <!-- .row -->
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>