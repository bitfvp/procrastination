<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_52"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Formulário 2 dados-".$env->env_titulo;
$css="pcdform2";

include_once("{$env->env_root}includes/head.php");

$idc=$_GET['id'];
$sql = "SELECT * \n"
    . "FROM mcu_p_pcd \n"
    . "WHERE (((mcu_p_pcd.pessoa)=:pessoa))";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa", $idc);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carteira = $consulta->fetch();    

    $sql=null;
    $consulta=null;
    $pes=fncgetpessoa($carteira['pessoa']);
?>

<div class="container">
    <div class="hender">
        <div class="h_imagem">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg"  alt="">
        </div>
        <div class="h_titulo">
        <br><br><br><br><br>
            <h3>PREFEITURA MUNICIPAL DE MANHUAÇU</h3>
            <h4>SECRETARIA MUNICIPAL DE TRABALHO E DESENVOLVIMENTO SOCIAL</h4>
        </div>
    </div>
    <div class="codd"><h3>Processo: A<?php echo $carteira['carteira']; ?></h3></div>

    <div class="main">
        <h3>DADOS - PESSOA COM DEFICIÊNCIA</h3>
        <hr><hr>
        <h4>Nome do(a) beneficiário : <strong><?php echo $pes['nome'];?></strong></h4>
        <hr>
        <h4>Data de nascimento : <strong><?php echo datahoraBanco2data($pes['nascimento'])." <i>".Calculo_Idade($pes['nascimento'])." anos</i>"; ?></strong></h4>
        <hr>
        <h4>RG : <strong><?php echo $pes['rg']; ?></strong></h4>
        <hr>
        <h4>CPF : <strong><?php echo mask($pes['cpf'],'###.###.###-##'); ?></strong></h4>
    </div>

    <div class="enunciado">
    <h3>EXAME MÉDICO: DIAGNÓSTICO - PESSOA COM DEFICIÊNCIA</h3>
    <hr>
    </div>

    <DIV class="laudo">
    <div class="info"><h6>Laudo médico: Descrever detalhadamente e de forma legível, a deficiência diagnosticada no paciente, que gere incapacidade para o desempenho de atividade</h6></div>
    <br><br><br><br><br><br>
    <div class="acomp"><h3>ACOMPANHANTE: (<strong class="invisi">&nbsp;&nbsp;&nbsp;</strong>)SIM(<strong class="invisi">&nbsp;&nbsp;&nbsp;</strong>)NÃO </h3></div>        
    </DIV>

    <div class="enunciado">
    <h4><strong>Atesto que o(a) requerente, para fins de gratuidade</strong> no sistema de transporte coletivo municipal de passageiros, foi diagnosticado com a deficiência abaixo especificada:</h4>
    </div>

    <div class="mainn">
    <hr>
    <br>
        <h3>(   ) DEFICIÊNCIA FÍSICA: CID-10:_______________________________________________</h3>
        
        <h3>(   ) DEFICIÊNCIA AUDITIVA: CID-10:____________________________________________</h3>
        
        <h3>(   ) DEFICIÊNCIA MENTAL: CID-10:_____________________________________________</h3>
        
        <h3>(   ) DEFICIÊNCIA VISUAL: CID-10:______________________________________________</h3>

        <h3>(   ) DEFICIÊNCIA MÚLTIPLA: CID-10:___________________________________________</h3>
        
    </div>




        <div class="foot">
            <br>
            <br>
            <h4>_____________________________________________________________________</h4>
            <h4>
                Data de expedição do atestado
            </h4>
            <br>
            <br>
            <br>
            <br>
            <h4>_____________________________________________________________________</h4>
            <h4>
                Carimbo c/ CRM
            </h4>
        </div>
        
</div>

</body>
</html>