<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_52"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Formulário 3 pericial-".$env->env_titulo;
$css="pcdform3";

include_once("{$env->env_root}includes/head.php");

$idc=$_GET['id'];
$sql = "SELECT * \n"
    . "FROM mcu_p_pcd \n"
    . "WHERE (((mcu_p_pcd.pessoa)=:pessoa))";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":pessoa", $idc);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carteira = $consulta->fetch();    

    $sql=null;
    $consulta=null;
    $pes=fncgetpessoa($carteira['pessoa'])
?>

<div class="container">
    <!-- //////////////////////////////// -->
    <div class="hender">
        <div class="h_imagem">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg"  alt="">
        </div>
        <div class="h_titulo">
        <br><br><br><br><br>
            <h3>PREFEITURA MUNICIPAL DE MANHUAÇU</h3>
            <h4>SECRETARIA MUNICIPAL DE TRABALHO E DESENVOLVIMENTO SOCIAL</h4>
        </div>
    </div>
    <div class="codd"><h3>Processo Numero:A<?php echo $carteira['carteira']; ?></h3></div>

    <div class="main">
        <h4>Nome do(a) beneficiário : <strong><?php echo $pes['nome'];?></strong></h4>
        <hr>
        <h4>Data de Nascimento : <strong><?php echo datahoraBanco2data($pes['nascimento'])." <i>".Calculo_Idade($pes['nascimento'])." anos</i>"; ?></strong></h4>
        <hr>
        <h4>RG : <strong><?php echo $pes['rg']; ?></strong></h4>
        <hr>
        <h4>CPF : <strong><?php echo mask($pes['cpf'],'###.###.###-##'); ?></strong></h4>
    </div>

<div class="enunciado">
    <h3>TIPO DE DEFICIÊNCIA:</h3>
</div>

<div class="contem">
<h3>
    <div class="five">(<strong class="invisi">&nbsp;&nbsp;</strong>) Física</div>
    <div class="five">(<strong class="invisi">&nbsp;&nbsp;</strong>) Auditiva</div>
    <div class="five">(<strong class="invisi">&nbsp;&nbsp;</strong>) Visual</div>
    <div class="five">(<strong class="invisi">&nbsp;&nbsp;</strong>) Mental</div>
    <div class="five">(<strong class="invisi">&nbsp;&nbsp;</strong>) Múltipla</div>
</h3>
</div>

    <div class="enunciado">
    <h3>RELATÓRIO DE EQUIPE MULTIDISCIPLINAR-PERICIAL</h3>
    <h4>Para fins de gratuidade no sistema de transporte coletivo municipal de passageiros</h4>
    </div>

    <DIV class="laudo">
    <div class="info"><h6>Descrever, detalhadamente e de forma legível, o resultado da avaliação</h6></div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <div class="acomp"><h3>(<strong class="invisi">&nbsp;&nbsp;</strong>)APTO(<strong class="invisi">&nbsp;&nbsp;</strong>)INAPTO </h3><h3>ACOMPANHANTE: (<strong class="invisi">&nbsp;&nbsp;</strong>)SIM(<strong class="invisi">&nbsp;&nbsp;</strong>)NÃO </h3></div>
    </DIV>




        <div class="foot">
            <h4>_____________________________________________________________________</h4>
            <h4>
                Data de expedição do atestado
            </h4>
            <br>
            <br>
            <br>
            <h4>_____________________________________________________________________</h4>
            <h4>
                Carimbo c/ CRM
            </h4>
            <br>
            <br>
            <br>
            <h4>___________________________________<strong class="invisi">&nbsp;&nbsp;</strong>__________________________________</h4>
            <h4>
                Auxiliar Administrativo<strong class="invisi">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>Assistente Social
            </h4>
        </div>
        
</div>

</body>
</html>