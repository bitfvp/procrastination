<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Lista de agendamentos para perícia médica-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>

<?php
$sql = "SELECT\n"
    . " mcu_p_pcd_p.id,\n"
    . " mcu_pessoas.id as id_pessoa,\n"
    . " mcu_pessoas.nome,\n"
    . " mcu_p_pcd_p.carteira,\n"
    . " mcu_bairros.bairro,\n"
    . " mcu_pessoas.telefone,\n"
    . " mcu_p_pcd_p.data_pericia,\n"
    . " mcu_p_pcd_p.hora_pericia,\n"
    . " mcu_p_pcd_p.obs,\n"
    . " mcu_p_pcd_p.avisado,\n"
    . " mcu_p_pcd_p.feito \n"
    . "FROM\n"
    . "mcu_p_pcd\n"
    . "INNER JOIN mcu_p_pcd_p ON mcu_p_pcd.carteira = mcu_p_pcd_p.carteira\n"
    . "INNER JOIN mcu_pessoas ON mcu_p_pcd.pessoa = mcu_pessoas.id\n"
    . "INNER JOIN mcu_bairros ON mcu_bairros.id = mcu_pessoas.bairro \n"
    . "ORDER BY\n"
    . "mcu_p_pcd_p.id DESC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$lista = $consulta->fetchAll();
$sql=null;
$consulta=null;
?>
<main class='container'>
    <h3>Cadastros de marcação de perícias médicas para passe livre municipal PCD</h3>
    <hr class="hrgrosso">
    <div class="row">
        <div class="col-md-3">
            <a href="index.php?pg=Vpericiaeditar" class="btn btn-success mb-2" target=""><i class="fas fa-plus"></i> NOVA MARCAÇÃO</a>
        </div>
    </div>
    <div class="row">
        <table class="table table-sm table-striped table-hover table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th class="text-center">Processo</th>
                <th class="text-center">Bairro</th>
                <th class="text-center">Telefone</th>
                <th class="text-center">Perícia</th>
                <th class="text-center">Obs</th>
                <th class="text-center">Avisado</th>
                <th class="text-center">Feito</th>
                <th class="text-center">Ações</th>
            </tr>
            </thead>
            <tbody>
                <?php
                foreach ($lista as $lt) {
                    if ($lt['data_pericia']==null || $lt['data_pericia']==null){
                        $d_pericia="<td class='text-danger text-center'><i class='badge badge-danger'>Não marcado</i></td>";
                    }else{
                        $d_pericia="<td class='text-center'>".dataBanco2data($lt['data_pericia'])." ".$lt['hora_pericia']."</td>";
                    }
                    if ($lt['avisado']==0){
                        $avi = "<td class='text-center'><i class='badge badge-danger'>Não</i></td>";
                    }else{
                        $avi = "<td class='text-center text-success'><i class='badge badge-success'>Sim</i></td>";
                    }
                    if ($lt['feito']==0){
                        $fei = "<td class='text-center text-danger'><i class='badge badge-danger'>Não</i></td>";
                    }else{
                        $fei = "<td class='text-center text-success'><i class='badge badge-success'>Sim</i></td>";
                    }
                    if ($lt['feito']!=0 && $lt['avisado']!=0){
                        $bgg = "bg-warning";
                    }else{
                        $bgg = " ";
                    }
                    ?>
                    <tr class="<?php echo $bgg;?>">
                        <td class="text-center"><?php echo $lt['id'];?></td>
                        <td class=""><a href='index.php?pg=Vp&id=<?php echo $lt['id_pessoa'];?>' target="_blank"><?php echo $lt['nome'];?></a></td>
                        <td class="text-center">A<?php echo $lt['carteira'];?></td>
                        <td class="text-center"><?php echo $lt['bairro'];?></td>
                        <td class="text-center"><?php echo $lt['telefone'];?></td>
                        <?php echo $d_pericia;?>
                        <td class="text-center"><?php echo $lt['obs'];?></td>
                        <?php echo $avi;?>
                        <?php echo $fei;?>
                        <td class="px-0" style="width: 115px">
                            <div class="row mx-0 px-0">
                                <div class="col-md-3 offset-1">
                                    <a href="index.php?pg=Vpericiaeditar&id=<?php echo $lt['id']; ?>" title="Editar entrega" class="btn btn-sm btn-outline-primary fa fa-pen"></a>
                                </div>

                                <?php if ($lt['avisado']==0){?>
                                <div class="col-md-3 offset-1">
                                    <a href='index.php?pg=Vpericialista&id=<?php echo $lt['id']; ?>&id_pessoa=<?php echo $lt['id_pessoa']; ?>&aca=controlavisado' title='Click para marcar que foi avisado' class='btn btn-sm btn-outline-info fas fa-phone text-info'></a>
                                </div>
                                <?php
                                }

                                if ($lt['feito']==0){
                                ?>
                                <div class="col-md-3 offset-1">
                                    <a href='index.php?pg=Vpericialista&id=<?php echo $lt['id']; ?>&id_pessoa=<?php echo $lt['id_pessoa']; ?>&aca=controlfeito' title='Click para marcar que foi feito a perícia' class='btn btn-sm btn-outline-success fas fa-clipboard-check text-success'></a>
                                </div>
                                    <?php
                                }
                                ?>

                                <div class="col-md-3 offset-1">
                                    <a href='index.php?pg=Vpericialista&id=<?php echo $lt['id']; ?>&id_pessoa=<?php echo $lt['id_pessoa']; ?>&aca=controlapagar' title='Click para apagar a perícia' class='btn btn-sm btn-outline-danger fas fa-trash-alt text-danger'></a>
                                </div>

                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>