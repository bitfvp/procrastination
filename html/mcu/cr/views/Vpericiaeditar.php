<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Contato-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="periciasave";
    $pericia=fncgetpericia($_GET['id']);
    $visi=" readonly='true' ";
    $visi2=" disabled ";
}else{
    $a="pericianew";
    $visi="  ";
    $visi2="  ";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="index.php?pg=Vhome&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="form-group col-md-2">
                <label>Código:</label>
                <input id="id" type="text" readonly="true" class="form-control disabled" name="id" value="<?php echo $pericia['id']; ?>"/>
            </div>
            <div class="form-group col-md-2">
                <label for="carteira">Processo:</label>
                <input <?php echo $visi;?> id="carteira" type="text" class="form-control <?php echo $visi2;?>" name="carteira" value="<?php echo $pericia['carteira']; ?>"/>
            </div>

            <div class="form-group col-md-3">
                <label for="data_pericia">Data da pericia:</label>
                <input id="data_pericia" type="date" class="form-control" name="data_pericia" value="<?php echo $pericia['data_pericia']; ?>"/>
            </div>
            <div class="form-group col-md-3">
                <label for="hora_pericia">hora da pericia:</label>
                <input id="hora_pericia" type="time" class="form-control" name="hora_pericia" value="<?php echo $pericia['hora_pericia']; ?>"/>
            </div>

            <div class="form-group col-md-12">
                <label for="obs">Observação:</label>
                <textarea id="obs" onkeyup="limite_textarea(this.value,255,obs,'cont')" maxlength="255" autofocus class="form-control" rows="3" name="obs"><?php echo $pericia['obs']; ?></textarea>
                <span id="cont">255</span>/255
            </div>

            <div class="form-group col-md-12">
                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR MARCAÇÃO"/>
            </div>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>