<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_49"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{//senao vai executar abaixo
            if ($allow["allow_51"]!=1){
                header("Location: {$env->env_url}?pg=Vlogin");
                exit();
            }
        }
    }
}

$page="Status cr pcd-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
	<div class="col-md-12">
        <table class="table table-striped table-sm">
            <thead class="thead-dark">
            <tr>
                <th>Pedido</th>
                <th>Nome</th>
            </tr>
            </thead>
            <tbody>
<?php
$sql = "SELECT\n"
    . "mcu_cr_pcd.id, mcu_cr_pcd.pessoa, mcu_pessoas.nome, mcu_cr_pcd.carteira\n"
    . "FROM\n"
    . "mcu_pessoas\n"
    . "INNER JOIN mcu_cr_pcd ON mcu_pessoas.id = mcu_cr_pcd.pessoa\n"
    . "ORDER BY\n"
    . "mcu_cr_pcd.carteira DESC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$lista = $consulta->fetchAll();
$sql=null;
$consulta=null;

foreach ($lista as $lt) {
    echo "<tr>";
    echo "<td>{$lt['carteira']}</td>";
    echo "<td><a href='index.php?pg=Vcr_pcd&id={$lt['pessoa']}'>{$lt['nome']}</a></td>";
    echo "</tr>";
}
?>
            </tbody>
        </table>
	</div>
	
</div>
	


</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>