<?php
class Pericia{
    public function fncpericianew($carteira,$data_pericia,$hora_pericia,$obs){
        //tratamento das variaveis

        //inserção no banco
        try{
            $sql="INSERT INTO mcu_p_pcd_p ";
            $sql.="(id, carteira, data_pericia, hora_pericia, obs)";
            $sql.=" VALUES ";
            $sql.="(NULL, :carteira, :data_pericia, :hora_pericia, :obs)";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":carteira", $carteira);
            $insere->bindValue(":data_pericia", $data_pericia);
            $insere->bindValue(":hora_pericia", $hora_pericia);
            $insere->bindValue(":obs", $obs);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    public function fncpericiaedit($id,$carteira,$data_pericia,$hora_pericia,$obs){


        try{
            $sql="SELECT * FROM mcu_p_pcd_p WHERE id=:id and carteira=:carteira";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->bindValue(":carteira", $carteira);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

                try {
                $sql="UPDATE mcu_p_pcd_p SET data_pericia=:data_pericia, hora_pericia=:hora_pericia, obs=:obs WHERE id=:id";
            global $pdo;
            $atualiza=$pdo->prepare($sql);
                    $atualiza->bindValue(":data_pericia", $data_pericia);
                    $atualiza->bindValue(":hora_pericia", $hora_pericia);
                    $atualiza->bindValue(":obs", $obs);

            $atualiza->bindValue(":id", $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        }else{
            //msg de erro para o usuario;
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"danger",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
//                header("Location: index.php?pg=Vp&id={$pessoa}");
//                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//end function


}//d class