<?php
class Pcd{
    public function fncpcd($pessoa,$possui_acomp,$acompanhante_1,$acompanhante_2,$aprovado){
        //tratamento das variaveis
        $acompanhante_1=ucwords(strtoupper($acompanhante_1));
        $acompanhante_2=ucwords(strtoupper($acompanhante_2));

        try{
            $sql="SELECT * FROM mcu_p_pcd WHERE pessoa=:pessoa";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":pessoa", $pessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

                try {
                $sql="UPDATE mcu_p_pcd SET possui_acomp=:possui_acomp, acompanhante_1=:acompanhante_1, acompanhante_2=:acompanhante_2, aprovado=:aprovado WHERE pessoa=:pessoa";
            global $pdo;
            $atualiza=$pdo->prepare($sql);
                    $atualiza->bindValue(":possui_acomp", $possui_acomp);
                    $atualiza->bindValue(":acompanhante_1", $acompanhante_1);
                    $atualiza->bindValue(":acompanhante_2", $acompanhante_2);
                    $atualiza->bindValue(":aprovado", $aprovado);

            $atualiza->bindValue(":pessoa", $pessoa);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        }else{
            //msg de erro para o usuario;
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"danger",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vp&id={$pessoa}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//end function


}//d class
?>