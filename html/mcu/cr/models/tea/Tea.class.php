<?php
class Tea{
    public function fnctea($pessoa,$doc,$responsavel,$telefone){
        //tratamento das variaveis
        $doc=strtoupper($doc);
        $responsavel=ucwords(strtolower($responsavel));

        try{
            $sql="SELECT * FROM mcu_p_tea WHERE pessoa=:pessoa";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":pessoa", $pessoa);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

                try {
                $sql="UPDATE mcu_p_tea SET doc=:doc, responsavel=:responsavel, telefone=:telefone WHERE pessoa=:pessoa";
            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindValue(":doc", $doc);
            $atualiza->bindValue(":responsavel", $responsavel);
            $atualiza->bindValue(":telefone", $telefone);
            $atualiza->bindValue(":pessoa", $pessoa);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        }else{
            //msg de erro para o usuario;
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"danger",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vtea&id={$pessoa}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//end function


}//d class