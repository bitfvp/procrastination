<?php
class Adv
{
    public function fncadvnew($id,$id_prof,$descricao)
    {

        //inserção no banco
        try {
            $sql = "INSERT INTO mcu_p_adv ";
            $sql .= "(id, pessoa, data, profissional, descricao)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, CURRENT_TIMESTAMP, :profissional, :descricao)";

            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $id);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($insere)) {
            /////////////////////////////////////////////////////
            //criar log
            //reservado ao log
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Advertência Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vadv&id={$id}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }

}

?>