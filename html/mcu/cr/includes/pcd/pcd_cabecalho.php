<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_p_pcd WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $processo = $consulta->fetch();
    $sql=null;
    $consulta=null;

}else{
    $_SESSION['fsh']=[
        "flash"=>"Ação abortada devido a falta de identificação do cadastro!!",
        "type"=>"danger",
    ];
    header("Location: ?pg=Vhome");
    exit();
}
?>
<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Ações
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <h5>
                <?php
                if (empty($processo)){
                    echo "<a class='btn btn-primary' href='?pg=Vp&aca=novapcd&id=".$_GET['id']."' title=''>";
                    echo "Pedido de carteira";
                    echo "</a>";
                }else{
                ?>
                Número do pedido:<strong class="text-info"><?php echo $processo['carteira']; ?>&nbsp;&nbsp;</strong>
                <br>
                Possui acompanhante:
                <strong class="text-info"><?php
                    if($processo['possui_acomp']==0){echo"Não";}
                    if($processo['possui_acomp']==1){echo"Sim";} ?>&nbsp;&nbsp;
                </strong>

                Aprovado:
                <strong class="text-info"><?php
                if($processo['aprovado']==0){echo"Aguardando";}
                if($processo['aprovado']==1){echo"Sim";}
                if($processo['aprovado']==2){echo"Negado";}
                ?>&nbsp;&nbsp;
                </strong>

                Foto tirada:
                <strong class="text-info"><?php
                if($processo['foto']==0){echo"Não";}
                if($processo['foto']==1){echo"Sim";} ?>&nbsp;&nbsp;
                </strong>

                    Segunda via:
                    <strong class="text-info"><?php if($processo['segunda_via']==1){echo"Sim";}else{echo "Não";} ?>&nbsp;&nbsp;</strong>

                Enviado:
                <strong class="text-info"><?php
                if($processo['envio']==0){echo"Não";}
                if($processo['envio']==1){echo"Sim";} ?>&nbsp;&nbsp;
                </strong>
                <br>
                Status do cartão magnético:
                <strong class="text-info"><?php
                if($processo['cartao_feito']==0){echo"Aguardando...";}
                if($processo['cartao_feito']==1){echo"Confeccionado";}
                if($processo['cartao_feito']==2){echo"Ocorreu um problema!!";}
                ?>&nbsp;&nbsp;
                </strong>

                Observação da empresa:<strong class="text-info"><?php echo $processo['obs_uniao']; ?>&nbsp;&nbsp;</strong>
                <br>
                <div class="btn-group my-2">
                    <a class="btn btn-success" href="index.php?pg=Vpcdeditar&id=<?php echo $_GET['id'];?>" title="">
                        Editar
                    </a>
                    <a class="btn btn-warning" href="index.php?pg=Vpcd_form1&id=<?php echo $_GET['id'];?>" target="_blank" title="">
                        Formulário dados
                    </a>
                    <a class="btn btn-warning" href="index.php?pg=Vpcd_form2&id=<?php echo $_GET['id'];?>" target="_blank" title="">
                        Formulário atestado
                    </a>
                    <a class="btn btn-warning" href="index.php?pg=Vpcd_form3&id=<?php echo $_GET['id'];?>" target="_blank" title="">
                        Formulário pericial
                    </a>
                </div>
                    <div class="btn-group">
                            <?php
                            if ($processo['foto']==0){
                                echo "<a class='btn btn-danger' href='?pg=Vp&aca=pcdfototirada&id=".$_GET['id']."' title=''>";
                                echo "Fotos tiradas";
                                echo "</a>";
                            }

                        if ($processo['segunda_via']!=1){
                            echo "<a class='btn btn-warning' href='?pg=Vp&acb=pcdsegundavia&id=".$_GET['id']."' title=''>";
                            echo "Segunda via";
                            echo "</a>";
                        }
                        ?>
                        <a class="btn btn-danger" href="index.php?pg=Vp&id=<?php echo $_GET['id'];?>&acb=pcdreset" title="">
                            Reset do processo
                        </a>
                    </div>
                    <?php
                }//fim de if de existe pedido
                ?>
            </h5>
        </blockquote>
    </div>
</div>