<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_pessoas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoa = $consulta->fetch();

    $sql=null;
    $consulta=null;
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ação abortada devido a falta de identificação do cadastro!!",
        "type"=>"danger",
    ];
    header("Location: ?pg=Vhome");
    exit();
}
?>
    <div class="card mb-2">
        <div class="card-header bg-info text-light">
            <a href="?pg=Vpessoa&id=<?php echo $_GET['id'];?>" class="fa fa-backward text-danger">VOLTAR</a>
        </div>
        <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <h5>
                Nome:<strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
                RG:
                <strong class="text-info"><?php
                if($pessoa['rg']!="") {
                echo "<span class='text-info '>";
                    echo mask($pessoa['rg'],'###.###.###');
                echo "</span>";
                }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
                }
                ?></strong>
                CPF:
                <strong class="text-info"><?php
                    if($pessoa['cpf']!="") {
                        echo "<span class='text-info '>";
                        echo mask($pessoa['cpf'],'###.###.###-##');
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?></strong>
                Nascimento:
                <strong class="text-info"><?php
                if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                echo "<span class='text-info'>";
                echo dataBanco2data ($pessoa['nascimento']);
                if(Calculo_Idade($pessoa['nascimento'])>=60){
                    echo " <i class='text-danger'>".Calculo_Idade($pessoa['nascimento'])." anos </i>";
                }else{
                    echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos </i>";
                }
                echo "</span>";
                }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
                }
                ?>
                </strong>
                Endereço:
                <strong class="text-info"><?php
                if($pessoa['endereco']!=""){
                echo "<span class='text-info'>";
                echo $pessoa['endereco'];
                echo "</span>";
                }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
                }
                ?></strong>&nbsp;&nbsp;
                Número:
                <strong class="text-info"><?php
                    if ($pessoa['numero']==0){
                        echo "<span class='text-info'>";
                        echo "s/n";
                        echo "</span>";
                    }else{
                        echo "<span class='text-info'>";
                        echo $pessoa['numero'];
                        echo "</span>";
                    }
                ?></strong>&nbsp;&nbsp;
                Bairro
                <strong class="text-info"><?php
                if ($pessoa['bairro'] != "0") {
                $cadbairro=fncgetbairro($pessoa['bairro']);
                echo $cadbairro['bairro'];
                } else {
                echo "<span class='text-warning'>[---]</span>";
                }
                ?></strong>&nbsp;&nbsp;
                Telefone:
                <strong class="text-info"><?php
                if($pessoa['telefone']!="") {
                echo "<span class='text-info'>";
                echo $pessoa['telefone'];
                echo "</span>";
                }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
                }
                ?></strong>
                Mãe:
                <strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;
                </strong>

                Pai:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>

                <br>
                <div class="btn-group">
                    <a class="btn btn-success" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
                        Editar Pessoa
                    </a>
                </div>
            </h5>
            <footer class="blockquote-footer">
                Mantenha atualizado
            </footer>
        </blockquote>
    </div>
</div>