<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    // Captura os dados do cliente solicitado
    $sql = "SELECT * FROM mcu_cr_pcd WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_GET['id']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $processo = $consulta->fetch();
    $sql=null;
    $consulta=null;

}else{
    $_SESSION['fsh']=[
        "flash"=>"Ação abortada devido a falta de identificação do cadastro!!",
        "type"=>"danger",
    ];
    header("Location: ?pg=Vhome");
    exit();
}
?>
<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Ações
    </div>
    <div class="card-body">
        <blockquote class="blockquote blockquote-info">
            <h5>
            <?php
            if (empty($processo)){
                echo "<a class='btn btn-primary' href='?pg=Vcr_pcd&aca=novacrpcd&id=".$_GET['id']."' title=''>";
                echo "Pedir credencial";
                echo "</a>";
            }else{
            ?>
                Número do pedido:<strong class="text-info"><?php echo $processo['carteira']; ?>&nbsp;&nbsp;</strong>
                <br>


                <br>
                <div class="btn-group mt-2">
                    <?php
                    ($processo['impressao']==0)? $xx="btn-outline-info text-danger": $xx="btn-info";
                    ($processo['impressao']==0)? $xy="Impressão da credencial desativada": $xy="Impressão da credencial ativada";
                    ?>
                    <a class="btn <?php echo $xx;?>" href="index.php?pg=Vcr_pcd&id=<?php echo $_GET['id'];?>&aca=bot_cr_pcd_imprime" title="">
                        <?php echo $xy;?>
                    </a>
                </div>
                <?php
            }//fim de if de existe pedido
            ?>
            </h5>
        </blockquote>
    </div>
</div>