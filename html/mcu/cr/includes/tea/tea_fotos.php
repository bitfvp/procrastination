<div class="row">
    <div class="col-md-4">
        <blockquote class="blockquote blockquote-info">
            Titular:
            <div class="row">
                <?php
                //$URL_ATUAL= "$_SERVER[REQUEST_URI]";
                if ($processo['carteira']!=0){
                    $sql = "SELECT * FROM `mcu_p_tea_dados` where carteira='{$processo['carteira']}' and tipo='titular' ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $dados = $consulta->fetchAll();//$total[0]
                    $sql = null;
                    $consulta = null;

                    foreach ($dados as $dado){
                        $status="";
                        if ($dado['status']==0){
                            $status="bg-danger-light ";
                        }
                        $link="../../dados/mcu/p_tea/fotos/" . $dado['carteira'] . "/" . $dado['tipo'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                        $caminho=$link;
                        echo "<div class='col-md-6 text-center {$status}'>";
                        echo "<a href=" . $link . " target='_blank'>";
                        echo "<img src=" . $caminho . " alt='...'  class='img-fluid img-thumbnail'>";
                        echo "</a>";
                        echo "<a href=" . $link . " target='_blank' title='View larger' class='fas fa-search-plus btn btn-sm'></a>";
                        if ($dado['status']==0){
                            $us=fncgetusuario($dado['delete_prof']);
                            echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                        }else{
                            $act="index.php?pg=Vtea&id=".$_GET['id'] ."&aca=apagaft_tea&cart=". $dado['carteira'] . "&ft=". $dado['arquivo'];
                            echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                        }
                        echo "</div>";
                    }
                }
                ?>
            </div>
            <footer class="blockquote-footer text-danger">
                Click no arquivo pra acessar
            </footer>
        </blockquote>
    </div>



    <div class="col-md-4">
        <blockquote class="blockquote blockquote-info">
            Responsável¹:<br><h6><strong class="text-info"><?php echo $processo['responsavel']; ?>&nbsp;&nbsp;</strong></h6>
            <div class="row">
                <?php
                //$URL_ATUAL= "$_SERVER[REQUEST_URI]";
                if ($processo['carteira']!=0){
                    $sql = "SELECT * FROM `mcu_p_tea_dados` where carteira='{$processo['carteira']}' and tipo='responsavel' ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->execute();
                    $dados = $consulta->fetchAll();//$total[0]
                    $sql = null;
                    $consulta = null;

                    foreach ($dados as $dado){
                        $status="";
                        if ($dado['status']==0){
                            $status="bg-danger-light ";
                        }
                        $link="../../dados/mcu/p_tea/fotos/" . $dado['carteira'] . "/" . $dado['tipo'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                        $caminho=$link;
                        echo "<div class='col-md-6 text-center {$status}'>";
                        echo "<a href=" . $link . " target='_blank'>";
                        echo "<img src=" . $caminho . " alt='...'  class='img-fluid img-thumbnail'>";
                        echo "</a>";
                        echo "<a href=" . $link . " target='_blank' title='View larger' class='fas fa-search-plus btn btn-sm'></a>";
                        if ($dado['status']==0){
                            $us=fncgetusuario($dado['delete_prof']);
                            echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                        }else{
                            $act="index.php?pg=Vtea&id=".$_GET['id'] ."&aca=apagaft_tea&cart=". $dado['carteira'] . "&ft=". $dado['arquivo'];
                            echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                        }
                        echo "</div>";
                    }
                }
                ?>
            </div>
        </blockquote>
    </div>

</div>