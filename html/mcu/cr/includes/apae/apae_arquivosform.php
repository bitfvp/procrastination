<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Envio de fotos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Va&aca=apaenovafoto&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="titular" type="file" class="custom-file-input" name="titular[]" value="" multiple/>
                        <label class="custom-file-label" for="titular">Fotos do titular...</label>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="acompanhante_1" type="file" class="custom-file-input" name="acompanhante_1[]" value="" multiple/>
                        <label class="custom-file-label" for="acompanhante_1">Fotos do acompanhante¹...</label>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="acompanhante_2" type="file" class="custom-file-input" name="acompanhante_2[]" value="" multiple/>
                        <label class="custom-file-label" for="acompanhante_2">Fotos do acompanhante²...</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="ENVIAR FOTOS"/>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card mb-2">
    <div class="card-header bg-info text-light">
        Envio de arquivos
    </div>
    <div class="card-body">
        <form action="index.php?pg=Va&aca=apaenovoarquivo&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="custom-file">
                        <input id="arquivos" type="file" class="custom-file-input" name="arquivos[]" value="" multiple/>
                        <label class="custom-file-label" for="arquivos">Arquivos do titular...</label>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-block" value="ENVIAR ARQUIVOS"/>
                </div>
            </div>
        </form>
    </div>
</div>