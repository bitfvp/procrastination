<?php
//metodo de marcar como avisado
if ($startactiona == 1 && $aca == "controlavisado") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"]) && isset($_GET["id_pessoa"])) {
        //dados

        try {
            $sql = "UPDATE mcu_p_pcd_p ";
            $sql .= "SET avisado='1'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $_GET["id"]);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Alterado!!",
            "type"=>"success",
        ];

        $descricao="Foi indicado que foi de alguma forma avisado ao titular sobre a data e hora da perícia marcada";

            //executa classe cadastro
            $salvar= new Pcd_At();
            $salvar->fncpcdatnew($_GET["id_pessoa"],$_SESSION["id"],$descricao);

//////////////////////////////////////////////////////////////////////////////

        header("Location: index.php?pg=Vpericialista");
        exit();

    }
}


//metodo de marcar como feito a pericia
if ($startactiona == 1 && $aca == "controlfeito") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"]) && isset($_GET["id_pessoa"])) {
        //dados

        try {
            $sql = "UPDATE mcu_p_pcd_p ";
            $sql .= "SET feito='1'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $_GET["id"]);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Alterado!!",
            "type"=>"success",
        ];

        $descricao="Foi confirmado que o titular da carteira fez a perícia  médica";

        //executa classe cadastro
        $salvar= new Pcd_At();
        $salvar->fncpcdatnew($_GET["id_pessoa"],$_SESSION["id"],$descricao);

//////////////////////////////////////////////////////////////////////////////

        header("Location: index.php?pg=Vpericialista");
        exit();

    }
}

//metodo de apagar pericia
if ($startactiona == 1 && $aca == "controlapagar") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"]) && isset($_GET["id_pessoa"])) {
        //dados

        try {
            $sql = "DELETE FROM `mcu_p_pcd_p` WHERE `mcu_p_pcd_p`.`id` = ? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $_GET["id"]);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Apagado!!",
            "type"=>"success",
        ];
//////////////////////////////////////////////////////////////////////////////

        header("Location: index.php?pg=Vpericialista");
        exit();

    }
}