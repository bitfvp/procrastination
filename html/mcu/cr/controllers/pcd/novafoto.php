<?php
//metodo de acao cadastro de usuario
if ($startactiona == 1 && $aca == "pcdnovafoto") {
    $id = $_GET["id"];

    $sql = "SELECT * FROM mcu_p_pcd WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cs = $consulta->fetch();
    $cart = $cs['carteira'];
    $sql = null;
    $consulta = null;
    if ($cart != 0) {
        //verificar se pasta existe
        if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/')) {
            //pasta existe
        } else {
            //cria pasta
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/');
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/');
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/');
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2');
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos');
        }
        if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/')) {
        } else {
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/');
        }
        if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/')) {
        } else {
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/');
        }
        if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/')) {
        } else {
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/');
        }
        if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/')) {
        } else {
            mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/');
        }

        require_once("{$env->env_root}models/Upin.class.php");
        #Instanciamos a classe Upload:
        $Upin = new Upin;
        // verifica se foi enviado um arquivo
        $fillle = $_FILES['titular']['name'];
        if (isset($_FILES['titular']['name']) && $fillle[0] != null) {//if principal
            $Upin->get(
                '../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/', //Pasta de uploads (previamente criada)
                $_FILES["titular"]["name"], //Pega o nome dos arquivos, altere apenas
                10, //Tamanho máximo
                "jpeg,png,jpg,gif", //Extensões permitidas
                "titular", //Atributo name do input file
                1 //Mudar o nome? 1 = sim, 0 = não
            );
            $Upin->run();
        }

        // verifica se foi enviado um arquivo
        $fillle = $_FILES['acompanhante_1']['name'];
        if (isset($_FILES['acompanhante_1']['name']) && $fillle[0] != null) {//if principal
            $Upin->get(
                '../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/', //Pasta de uploads (previamente criada)
                $_FILES["acompanhante_1"]["name"], //Pega o nome dos arquivos, altere apenas
                10, //Tamanho máximo
                "jpeg,png,jpg,gif", //Extensões permitidas
                "acompanhante_1", //Atributo name do input file
                1 //Mudar o nome? 1 = sim, 0 = não
            );
            $Upin->run();
        }//if primario

        // verifica se foi enviado um arquivo
        $fillle = $_FILES['acompanhante_2']['name'];
        if (isset($_FILES['acompanhante_2']['name']) && $fillle[0] != null) {//if principal
            $Upin->get(
                '../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/', //Pasta de uploads (previamente criada)
                $_FILES["acompanhante_2"]["name"], //Pega o nome dos arquivos, altere apenas
                10, //Tamanho máximo
                "jpeg,png,jpg,gif", //Extensões permitidas
                "acompanhante_2", //Atributo name do input file
                1 //Mudar o nome? 1 = sim, 0 = não
            );
            $Upin->run();
        }

        ////base de dados dados
        ///
        $types = array( 'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf', 'txt' );
        if(is_dir("../../dados/mcu/p_pcd/fotos/".$cart."/")){
//        echo "A Pasta Existe";

            //titular
            if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/titular/") ) {
                while ( $entry = readdir( $handle ) ) {
                    $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                    if( in_array( $ext, $types ) ){
                        $arquivoo = explode(".", $entry);//$entry é o arquivo
                        $extencao = end($arquivoo);
                        echo $arquivoo[0]."=====".$extencao;
                        $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        $total = $consulta->fetch();//$total[0]
                        $sql = null;
                        $consulta = null;
                        if($total[0]==0){
                            /////////////////////////////Fazer a inclusao
                            $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                ." VALUES "
                                ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'titular');";
                            global $pdo;
                            $update = $pdo->prepare($sql);
                            $update->execute();
                            $sql = null;
                            $update = null;
                        }else {//se for diferente é por que já esta cadastrado
                            echo "Já existe na base";
                        }
                    }
                }
                closedir($handle);
            }
            //titular

            //acompanhante_1
            if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/acompanhante_1/") ) {
                while ( $entry = readdir( $handle ) ) {
                    $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                    if( in_array( $ext, $types ) ){
                        $arquivoo = explode(".", $entry);//$entry é o arquivo
                        $extencao = end($arquivoo);
                        echo $arquivoo[0]."=====".$extencao;
                        $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        $total = $consulta->fetch();//$total[0]
                        $sql = null;
                        $consulta = null;
                        if($total[0]==0){
                            /////////////////////////////Fazer a inclusao
                            $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                ." VALUES "
                                ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_1');";
                            global $pdo;
                            $update = $pdo->prepare($sql);
                            $update->execute();
                            $sql = null;
                            $update = null;
                        }else {//se for diferente é por que já esta cadastrado
                            echo "Já existe na base";
                        }
                    }
                }
                closedir($handle);
            }
            //acompanhante_1

            //acompanhante_2
            if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/acompanhante_2/") ) {
                while ( $entry = readdir( $handle ) ) {
                    $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                    if( in_array( $ext, $types ) ){
                        $arquivoo = explode(".", $entry);//$entry é o arquivo
                        $extencao = end($arquivoo);
                        echo $arquivoo[0]."=====".$extencao;
                        $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        $total = $consulta->fetch();//$total[0]
                        $sql = null;
                        $consulta = null;
                        if($total[0]==0){
                            /////////////////////////////Fazer a inclusao
                            $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                ." VALUES "
                                ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_2');";
                            global $pdo;
                            $update = $pdo->prepare($sql);
                            $update->execute();
                            $sql = null;
                            $update = null;
                        }else {//se for diferente é por que já esta cadastrado
                            echo "Já existe na base";
                        }
                    }
                }
                closedir($handle);
            }
            //acompanhante_2

            //arquivos
            if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/arquivos/") ) {
                while ( $entry = readdir( $handle ) ) {
                    $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                    if( in_array( $ext, $types ) ){
                        $arquivoo = explode(".", $entry);//$entry é o arquivo
                        $extencao = end($arquivoo);
                        echo $arquivoo[0]."=====".$extencao;
                        $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        $total = $consulta->fetch();//$total[0]
                        $sql = null;
                        $consulta = null;
                        if($total[0]==0){
                            /////////////////////////////Fazer a inclusao
                            $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                ." VALUES "
                                ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'arquivos');";
                            global $pdo;
                            $update = $pdo->prepare($sql);
                            $update->execute();
                            $sql = null;
                            $update = null;
                        }else {//se for diferente é por que já esta cadastrado
                            echo "Já existe na base";
                        }
                    }
                }
                closedir($handle);
            }
            //arquivos


        }//fim
        ///
        ////
        $_SESSION['fsh'] = [
            "flash" => "salvo!",
            "type" => "success",
        ];
        header("Location: index.php?pg=Vp&id={$id}");
        exit();
    } else {
        $_SESSION['fsh'] = [
            "flash" => "Essa pessoa ainda nâo tem o nummero da carteira!",
            "type" => "danger",
        ];
    }
}//fim









/////////////////////////////////////////////////////////////////////////////////////////
if ($startactiona == 1 && $aca == "pcdnovoarquivo") {
    $id = $_GET["id"];

    $sql = "SELECT * FROM mcu_p_pcd WHERE pessoa=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cs = $consulta->fetch();
    $cart = $cs['carteira'];
    $sql = null;
    $consulta = null;

    if ($cart != 0) {
        // verifica se foi enviado um arquivo
        $fillle = $_FILES['arquivos']['name'];
        if (isset($_FILES['arquivos']['name']) && $fillle[0] != null) {//if principal
            //verificar se pasta existe
            if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/')) {
                //pasta existe
            } else {
                //cria pasta
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/');
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/');
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/');
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/');
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/');
            }
            if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/')) {
            } else {
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/titular/');
            }
            if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/')) {
            } else {
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_1/');
            }
            if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/')) {
            } else {
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/acompanhante_2/');
            }
            if (is_dir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/')) {
            } else {
                mkdir('../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/');
            }

            require_once("{$env->env_root}models/Upin.class.php");
            #Instanciamos a classe Upload:
            $Upin = new Upin;


            $Upin->get(
                '../../dados/mcu/p_pcd/fotos/' . $cart . '/arquivos/', //Pasta de uploads (previamente criada)
                $_FILES["arquivos"]["name"], //Pega o nome dos arquivos, altere apenas
                10, //Tamanho máximo em megas
                "jpeg,png,jpg,gif,JPG,JPEG,PNG,GIF", //Extensões permitidas
                "arquivos", //Atributo name do input file
                1 //Mudar o nome? 1 = sim, 0 = não
            );
            $Upin->run();

            ////base de dados dados
            ///
            $types = array( 'png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf', 'txt' );
            if(is_dir("../../dados/mcu/p_pcd/fotos/".$cart."/")){
//        echo "A Pasta Existe";

                //titular
                if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/titular/") ) {
                    while ( $entry = readdir( $handle ) ) {
                        $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                        if( in_array( $ext, $types ) ){
                            $arquivoo = explode(".", $entry);//$entry é o arquivo
                            $extencao = end($arquivoo);
                            echo $arquivoo[0]."=====".$extencao;
                            $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute();
                            $total = $consulta->fetch();//$total[0]
                            $sql = null;
                            $consulta = null;
                            if($total[0]==0){
                                /////////////////////////////Fazer a inclusao
                                $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                    ." VALUES "
                                    ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'titular');";
                                global $pdo;
                                $update = $pdo->prepare($sql);
                                $update->execute();
                                $sql = null;
                                $update = null;
                            }else {//se for diferente é por que já esta cadastrado
                                echo "Já existe na base";
                            }
                        }
                    }
                    closedir($handle);
                }
                //titular

                //acompanhante_1
                if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/acompanhante_1/") ) {
                    while ( $entry = readdir( $handle ) ) {
                        $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                        if( in_array( $ext, $types ) ){
                            $arquivoo = explode(".", $entry);//$entry é o arquivo
                            $extencao = end($arquivoo);
                            echo $arquivoo[0]."=====".$extencao;
                            $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute();
                            $total = $consulta->fetch();//$total[0]
                            $sql = null;
                            $consulta = null;
                            if($total[0]==0){
                                /////////////////////////////Fazer a inclusao
                                $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                    ." VALUES "
                                    ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_1');";
                                global $pdo;
                                $update = $pdo->prepare($sql);
                                $update->execute();
                                $sql = null;
                                $update = null;
                            }else {//se for diferente é por que já esta cadastrado
                                echo "Já existe na base";
                            }
                        }
                    }
                    closedir($handle);
                }
                //acompanhante_1

                //acompanhante_2
                if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/acompanhante_2/") ) {
                    while ( $entry = readdir( $handle ) ) {
                        $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                        if( in_array( $ext, $types ) ){
                            $arquivoo = explode(".", $entry);//$entry é o arquivo
                            $extencao = end($arquivoo);
                            echo $arquivoo[0]."=====".$extencao;
                            $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute();
                            $total = $consulta->fetch();//$total[0]
                            $sql = null;
                            $consulta = null;
                            if($total[0]==0){
                                /////////////////////////////Fazer a inclusao
                                $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                    ." VALUES "
                                    ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'acompanhante_2');";
                                global $pdo;
                                $update = $pdo->prepare($sql);
                                $update->execute();
                                $sql = null;
                                $update = null;
                            }else {//se for diferente é por que já esta cadastrado
                                echo "Já existe na base";
                            }
                        }
                    }
                    closedir($handle);
                }
                //acompanhante_2

                //arquivos
                if ( $handle = opendir("../../dados/mcu/p_pcd/fotos/".$cart."/arquivos/") ) {
                    while ( $entry = readdir( $handle ) ) {
                        $ext = strtolower( pathinfo( $entry, PATHINFO_EXTENSION) );
                        if( in_array( $ext, $types ) ){
                            $arquivoo = explode(".", $entry);//$entry é o arquivo
                            $extencao = end($arquivoo);
                            echo $arquivoo[0]."=====".$extencao;
                            $sql = "SELECT COUNT(`id`) FROM `mcu_p_pcd_dados` where carteira='{$cart}' and arquivo='{$arquivoo[0]}'";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->execute();
                            $total = $consulta->fetch();//$total[0]
                            $sql = null;
                            $consulta = null;
                            if($total[0]==0){
                                /////////////////////////////Fazer a inclusao
                                $sql = "INSERT INTO `mcu_p_pcd_dados` (`id`, `data_cadastro`, `status`,  `carteira`, `arquivo`, `extensao`, `tipo`)"
                                    ." VALUES "
                                    ."(NULL, CURRENT_TIMESTAMP, '1', '{$cart}', '{$arquivoo[0]}', '{$extencao}', 'arquivos');";
                                global $pdo;
                                $update = $pdo->prepare($sql);
                                $update->execute();
                                $sql = null;
                                $update = null;
                            }else {//se for diferente é por que já esta cadastrado
                                echo "Já existe na base";
                            }
                        }
                    }
                    closedir($handle);
                }
                //arquivos


            }//fim
            ///
            ////
            $_SESSION['fsh'] = [
                "flash" => "salvo!",
                "type" => "success",
            ];

            header("Location: index.php?pg=Vp&id={$id}");
            exit();
        } else {
            $_SESSION['fsh'] = [
                "flash" => "Não foi enviado nenhum arquivo!",
                "type" => "danger",
            ];
        }

    } else {
        $_SESSION['fsh'] = [
            "flash" => "Essa pessoa ainda nâo tem o nummero da carteira!",
            "type" => "danger",
        ];

    }
}//fim