<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="novaapae"){
$id=$_GET["id"];

//consulta os registros dessa pessoa
    $sql = "SELECT * FROM mcu_p_apae WHERE pessoa=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $result = $consulta->fetch();
            $sql=null;
            $consulta=null;

            //se nao tiver registro
            if (empty($result)) {

                //seleciona o maior numero de carteira
                $sql = "SELECT Max(carteira) FROM mcu_p_apae";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mxv = $consulta->fetch();
                $sql=null;
                $consulta=null;
                $maxv=$mxv[0];

                //recebe o proximo numero
                $maxv++;

                //nova carteira
                try{
                    $sql="INSERT INTO mcu_p_apae ";
                    $sql .= "(id, pessoa, carteira, possui_acomp, foto, envio)";
                    $sql .= " VALUES ";
                    $sql .= "(NULL, :pessoa, :carteira, 0, 0, 0)";
                    global $pdo;
                    $insere=$pdo->prepare($sql);
                    $insere->bindValue(":pessoa", $id);
                    $insere->bindValue(":carteira", $maxv);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro'. $error_msg->getMessage();
                }

                //verificar se pasta existe
                if (is_dir('../../dados/mcu/p_apae/fotos/'.$maxv.'/')) {
                    //pasta existe
                }else{
                    //cria pasta
                 mkdir('../../dados/mcu/p_apae/fotos/'.$maxv.'/');
                 mkdir('../../dados/mcu/p_apae/fotos/'.$maxv.'/titular/');
                 mkdir('../../dados/mcu/p_apae/fotos/'.$maxv.'/acompanhante_1/');
                 mkdir('../../dados/mcu/p_apae/fotos/'.$maxv.'/acompanhante_2/');
                 mkdir('../../dados/mcu/p_apae/fotos/'.$maxv.'/arquivos/');
                }


                try {
                    $sql = "INSERT INTO mcu_p_apae_at ";
                    $sql .= "(id, pessoa, data, profissional, descricao)";
                    $sql .= " VALUES ";
                    $sql .= "(NULL, :pessoa, CURRENT_TIMESTAMP, :profissional, :descricao)";
                    $id_prof=$_SESSION['id'];
                    $descricao="Processo Iniciado";
                    /////////////////

                    ///////////////////
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":pessoa", $id);
                    $insere->bindValue(":profissional", $id_prof);
                    $insere->bindValue(":descricao", $descricao);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }

                /// atualiza pessoa com passe livre
                try{
                    $sql="UPDATE mcu_pessoas SET ";
                    $sql.="passe_livre='1' ";
                    $sql.=" WHERE id=:pessoa";
                    global $pdo;
                    $atuali=$pdo->prepare($sql);
                    $atuali->bindValue(":pessoa", $id);
                    $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro '. $error_msg->getMessage();
                }

                $_SESSION['fsh']=[
                    "flash"=>"Carteira OK",
                    "type"=>"success",
                ];
                header("Location: index.php?pg=Va&id={$id}");
                exit();


            }else{
                $_SESSION['fsh']=[
                    "flash"=>"já tem carteira",
                    "type"=>"info",
                ];
            }

}
?>