<?php
if($startactionb==1 && $acb=='apaereset'){
    // Recebe o id do cliente do cliente via GET
    if(isset($_GET['id'])) {
        $id =$_GET['id'];
        //existe um id e se ele é numérico
        if (!empty($id) && is_numeric($id)) {
            // Captura os dados do cliente solicitado
            $sql = "UPDATE `mcu_p_apae` SET  `foto` = '0', `envio` = '0', `cartao_feito`='0', `obs_uniao`=NULL WHERE `mcu_p_apae`.`pessoa` = ?";
            global $pdo;
            $updat = $pdo->prepare($sql);
            $updat->bindParam(1, $id);
            $updat->execute(); global $LQ; $LQ->fnclogquery($sql);
            $sql=null;
            $updat=null;
        }
        try {
            $sql = "INSERT INTO mcu_p_apae_at ";
            $sql .= "(id, pessoa, data, profissional, descricao)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :pessoa, CURRENT_TIMESTAMP, :profissional, :descricao)";
            $id_prof=$_SESSION['id'];
            $descricao="Processo Reiniciado";
            /////////////////

            ///////////////////
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":pessoa", $id);
            $insere->bindValue(":profissional", $id_prof);
            $insere->bindValue(":descricao", $descricao);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Reset de Processo Feito com Sucesso",
            "type"=>"info",
        ];
        header("Location: index.php?pg=Va&id={$id}");
        exit();
    }

}
?>