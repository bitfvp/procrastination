<?php
//desativar foto pcd
if($startactiona==1 && $aca=="apagaft_p"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_p_pcd_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto pcd

//desativar foto apae
if($startactiona==1 && $aca=="apagaft_a"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_p_apae_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto apae




//desativar foto idoso
if($startactiona==1 && $aca=="apagaft_i"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_p_idoso_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto idoso




//desativar foto fibro
if($startactiona==1 && $aca=="apagaft_f"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_p_fibro_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto fibro



//desativar foto tea
if($startactiona==1 && $aca=="apagaft_tea"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_p_tea_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto tea



//desativar foto cr_idoso
if($startactiona==1 && $aca=="apagaft_cr_idoso"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_cr_idoso_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto cr_idoso


//desativar foto cr_pcd
if($startactiona==1 && $aca=="apagaft_cr_pcd"){
    $pg=$_GET['pg'];
    $id=$_GET['id'];
    $prof=$_SESSION['id'];
    $cart=$_GET["cart"];
    $ft=$_GET["ft"];
    //começa a validação
    //se algum campo vazia retorna a msg
    if(empty($id) or empty($ft) or empty($pg) or empty($cart)){
        $_SESSION['fsh']=[
            "flash"=>"há algum erro",
            "type"=>"warning",
        ];
        header("Location: index.php");
        exit();
    }else{
        try {
            $sql = "UPDATE `mcu_cr_pcd_dados` SET `status` = '0', delete_prof=:prof WHERE carteira = :carteira and arquivo= :arquivo ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":prof", $prof);
            $exclui->bindValue(":carteira", $cart);
            $exclui->bindValue(":arquivo", $ft);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }
        $_SESSION['fsh']=[
            "flash"=>"Arquivo Desativada Com Sucesso",
            "type"=>"success",
        ];
        header("Location: ?pg={$pg}&id={$id}");
        exit();
    }
}
//desativar foto cr_pcd
