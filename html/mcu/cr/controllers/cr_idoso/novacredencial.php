<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="novacridoso"){
    $id=$_GET["id"];

    //consulta os registros dessa pessoa
    $sql = "SELECT nascimento FROM mcu_pessoas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $nasc = $consulta->fetch();
    $sql=null;
    $consulta=null;

    if (Calculo_Idade($nasc[0])>=1){

        //consulta os registros dessa pessoa
        $sql = "SELECT * FROM mcu_cr_idoso WHERE pessoa=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $id);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $result = $consulta->fetch();
        $sql=null;
        $consulta=null;

                //se nao tiver registro
        if (empty($result)) {
                    //seleciona o maior numero de carteira
            $sql = "SELECT Max(carteira) FROM mcu_cr_idoso";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mxv = $consulta->fetch();
            $sql=null;
            $consulta=null;
            $maxv=$mxv[0];

                    //recebe o proximo numero
            $maxv++;

                    //nova carteira
            try{
                $sql="INSERT INTO mcu_cr_idoso ";
                $sql .= "(id, pessoa, carteira)";
                $sql .= " VALUES ";
                $sql .= "(NULL, :pessoa, :carteira)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $id);
                $insere->bindValue(":carteira", $maxv);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
                }
                    //verificar se pasta existe
            if (is_dir('../../dados/mcu/cr_idoso/fotos/'.$maxv.'/')) {
                //pasta existe
            }else{
                        //cria pasta
                mkdir('../../dados/mcu/cr_idoso/fotos/'.$maxv.'/');
                mkdir('../../dados/mcu/cr_idoso/fotos/'.$maxv.'/arquivos/');
                }


                try {
                $sql = "INSERT INTO mcu_cr_idoso_at ";
                $sql .= "(id, pessoa, data, profissional, descricao)";
                $sql .= " VALUES ";
                $sql .= "(NULL, :pessoa, CURRENT_TIMESTAMP, :profissional, :descricao)";
                $id_prof=$_SESSION['id'];
                $descricao="Processo Iniciado";
                /////////////////
                    /// ///////////////////
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":pessoa", $id);
                    $insere->bindValue(":profissional", $id_prof);
                    $insere->bindValue(":descricao", $descricao);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }

                    /// atualiza pessoa com passe livre

            try{
                $sql="UPDATE mcu_pessoas SET ";
                $sql.="passe_livre='1' ";
                $sql.=" WHERE id=:pessoa";
                global $pdo;
                $atuali=$pdo->prepare($sql);
                $atuali->bindValue(":pessoa", $id);
                $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }
            $_SESSION['fsh']=[
                "flash"=>"credencial pedida",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcr_idoso&id={$id}");
            exit();



        }else{
            $_SESSION['fsh']=[
                "flash"=>"já tem credencial",
                "type"=>"info",
            ];
            header("Location: index.php?pg=Vcr_idoso&id={$id}");
            exit();
        }

    }else{
        $_SESSION['fsh']=[
            "flash"=>"Essa pessoa ainda não tem idade pra credencial de fibro!!",
            "type"=>"danger",
        ];
        header("Location: index.php?pg=Vcr_idoso&id={$id}");
        exit();
    }
}