<?php
function fncibgelist(){
    $sql = "SELECT * FROM tbl_ibge ORDER BY cidade ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ibgelista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $ibgelista;
}

function fncgetibge($id){
    $sql = "SELECT * FROM tbl_ibge WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getibge = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getibge;
}
?>