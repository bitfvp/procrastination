<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="kmnew"){

    //Receber os dados do formulário
    $arquivo_tmp = $_FILES['arquivo']['tmp_name'];

//    ler todo o arquivo para um array
    $dados = file($arquivo_tmp);

    $lll=0;
//Ler os dados do array
    foreach($dados as $linha){
        $lll++;//linha
        $erro=0;//erro
        $msg_conteudo="";//se precisar mostrar a linha que deu erro
        $msg_error="";//se precisar mostrar o erro

        //Retirar acentos e ç
        $linha = remover_caracter($linha);
//        coloca maiusculo
        $linha=strtoupper($linha);

        //isola km
        $linha = str_replace(array("KM"), " KM ", $linha);
        //Retirar os espaços em branco no inicio e no final da string
        $linha = trim($linha);
        //retira as ponto
        $linha = str_replace(array("."), ' ', $linha);
        //retira as #####
        $linha = str_replace(array("#"), '', $linha);
        //substitui a primeira chave
        $linha = str_replace(array("["), '', $linha);


        //        Retirar espacos seguidos
        $linha = preg_replace('!\s+!', ' ', $linha);

//        remove caracteres especiais
        $linha = preg_replace('/[\x00-\x1F\x7F]/u', '', $linha);

        //remove bom
        $bom = pack('H*','EFBBBF');
        $linha = preg_replace("/^$bom/", '', $linha);

        //se precisar mostrar a linha que deu erro
        $msg_conteudo.="linha ".$lll." - ".$linha;

        //ver se contem ], se não tiver é uma quebra de linha
        if (mb_strpos($linha, ']') !== false) {
            $linhaerro=0;
            //contem
        }else{
            $linhaerro=1;
            //não contem
        }

        //separa a informacao em dois [0]data [1]conteudo
        $data_conteudo = explode(']', $linha);
        $periodo= $data_conteudo[0];

        $periodo=datahoraData2Banco($periodo);//-----------------------------------------------------
        $hora = date('H', strtotime($periodo));


            $data_conteudo[1] = trim($data_conteudo[1]);
            $data_conteudo[1] = str_replace(array(":"), ']', $data_conteudo[1]);
            //separa a informacao em dois [0]pessoa que lancou  [1]conteudo
            $motorista_conteudo = explode(']', $data_conteudo[1]);
            $motorista= strtoupper($motorista_conteudo[0]);//-----------------------------------------------------


            $motorista_conteudo[1] = trim($motorista_conteudo[1]);
            //separa a informacao em varios divididos por espaco
            $conteudo = explode(' ', $motorista_conteudo[1]);
//        $conteudo = json_encode($conteudo);

            $placa= $conteudo[0];//-----------------------------------------------------
            $tipo= $conteudo[1];//-----------------------------------------------------
            $km= $conteudo[2];//-----------------------------------------------------
            $descricao="";
            for ($i = 1; $i <= 100; $i++) {
                $o=$i+2;
                if (isset($conteudo[$o])){
                    $descricao.=$conteudo[$o]." ";//-----------------------------------------------------
                }
            }



            $sql = "SELECT * FROM mcu_frota_motorista WHERE identificacao=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $motorista);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $getmotorista = $consulta->fetch();
            $getmotorista_cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            if ($getmotorista_cont>0){
                $motorista=$getmotorista['id']."";
            }else{
                $erro=1;
                $msg_error.="<i class='text-danger'>motorista</i> ";
            }

            $sql = "SELECT * FROM mcu_frota_carro WHERE placa=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $placa);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $getcarro = $consulta->fetch();
            $getcarro_cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            if ($getcarro_cont>0){
                $placa = $getcarro['id']."";
            }else{
                $erro=1;
                $msg_error.="<i class='text-danger'>placa</i> ";
            }


//tipo
            if ($tipo=="KM" or $tipo=="OLEO"){
                if ($tipo=="KM"){
                    $tipo=1;
                }
                if ($tipo=="OLEO"){
                    $tipo=2;
                }
            }else{
                $erro=1;
                $msg_error.="<i class='text-danger'>tipo</i> ";
            }

            //kilometragem
            if (is_numeric($km) and $km>0){
                $km=$km;
            }else{
                $erro=1;
                $msg_error.="<i class='text-danger'>kilometragem</i> ";
            }



            if ($erro==1){
                echo $msg_conteudo. "    ";
                echo $msg_error."<br>";
            }else{

                $aux = $motorista.$placa.$km.$hora;
                $aux = sha1($aux . "555");//-----------------------------------------------------

                $sql = "SELECT * FROM mcu_frota_km WHERE aux=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $aux);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $getkm_cont = $consulta->rowCount();
                $sql = null;
                $consulta = null;
                if ($getkm_cont>0 or $linhaerro==1){
                    //se ja tiver no banco passa direto
//            echo $msg_conteudo. "    ";
//            echo $msg_error="<i class='text-info'>Existe</i><br> ";
                }else {
                    //se ainda não existe na base

                    //        //executa classe cadastro
                    $salvar= new Km();
                    $salvar->fnckmnew($motorista,$placa,$tipo,$periodo,$aux,$km,$descricao);
                    echo $lll. "    ";
                    echo "<i class='text-success'>Feito</i><br>";

                }




            }



    }
//    header("Location: index.php?pg=Vkm_lista");
//    exit();

}
?>