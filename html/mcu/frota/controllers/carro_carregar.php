<?php
function fnccarrolist(){
    $sql = "SELECT * FROM mcu_frota_carro ORDER BY carro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $carrolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $carrolista;
}

function fncgetcarro($id){
    $sql = "SELECT * FROM mcu_frota_carro WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcarro = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcarro;
}
?>