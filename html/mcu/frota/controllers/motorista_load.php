<?php
//gerado pelo geracode
function fncmotoristalist(){
    $sql = "SELECT * FROM mcu_frota_motorista ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $motoristalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $motoristalista;
}

function fncgetmotorista($id){
    $sql = "SELECT * FROM mcu_frota_motorista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getmcu_frota_motorista = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getmcu_frota_motorista;
}
?>