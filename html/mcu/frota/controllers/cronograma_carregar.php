<?php
function fnccronogramalist(){
    $sql = "SELECT * FROM mcu_frota_cronograma ORDER BY data desc ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cronogramalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cronogramalista;
}

function fncgetcronograma($id){
    $sql = "SELECT * FROM mcu_frota_cronograma WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcronograma = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcronograma;
}
?>