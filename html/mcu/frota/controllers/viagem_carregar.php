<?php
function fncviagemlist(){
    $sql = "SELECT * FROM mcu_frota_viagem ORDER BY id desc ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $viagemlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $viagemlista;
}

function fncgetviagem($id){
    $sql = "SELECT * FROM mcu_frota_viagem WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getviagem = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getviagem;
}
?>