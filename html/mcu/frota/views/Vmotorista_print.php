<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="relatório-abastecimentos-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_GET['sca']." 00:00:01";
    $final = $_GET['scb']." 23:59:59";

    $sql = "SELECT * "
        . "FROM mcu_frota_km  "
        . "WHERE  mcu_frota_km.cod_motorista = ? and "
        . "mcu_frota_km.`periodo` >= ? AND "
        . "mcu_frota_km.`periodo` <= ? "
        ."ORDER BY "
        ."`periodo` ASC";
    global $pdo;
    $cons = $pdo->prepare($sql);
    $cons->bindParam(1, $_GET['id']);
    $cons->bindParam(2, $inicial);
    $cons->bindParam(3, $final);
    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
    $kms = $cons->fetchAll();
    $sql=null;
    $consulta=null;

    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h3>Relatório de lançamento de Quilometragem por período </h3>
        <h5>Período: <?php echo dataBanco2data($_GET['sca'])." à ".dataBanco2data($_GET['scb']);?></h5>
        <h6>Motorista: <?php echo strtoupper(fncgetmotorista($_GET['id'])['motorista']);?> </h6>
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th>CARRO</th>
                <th>PERIODO</th>
                <th>TIPO</th>
                <th>KM</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($kms as $dados){
                $id = $dados["id"];
                $carro = fncgetcarro($dados["cod_carro"]);
                $data_ts=datahoraBanco2data($dados["data_ts"]);
                $periodo=datahoraBanco2data($dados["periodo"]);
                $tipo=$dados["tipo"];
                if ($tipo==1){
                    $tipo="<i class=''>KM</i>";
                }
                if ($tipo==2){
                    $tipo="<i class=''>óleo</i>";
                }
                $kilometro=$dados["kilometro"];
                $descricao=$dados["descricao"];
                ?>

                <tr>
                    <td><a href="index.php?pg=Vcarro&id=<?php echo $carro['id']; ?>"><?php echo $carro['carro']." - ".$carro['placa']; ?></a></td>
                    <td><?php echo $periodo; ?></td>
                    <td><?php echo $tipo; ?></td>
                    <td><?php echo $kilometro; ?></td>
                    <td>
                        <i class="badge badge-warning">
                            <strong><?php echo $id; ?></strong>
                        </i>
                    </td>
                </tr>
                <tr>
                    <td colspan="5"><?php echo $descricao; ?></td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>
        <br>
        <div class="row text-center">
            <div class="col-12">
                <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
            </div>
            <div class="col-12">
                <strong> Concordo com os dados prescritos acima e dou fé.</strong>
            </div>
        </div>
        <br>
        <br>
        <div class="row text-center">
            <div class="col-12">
                <h4>_______________________</h4>
                <h4>
                    Assinatura
                </h4>
            </div>
        </div>
    </div>
</main>
</body>
</html>