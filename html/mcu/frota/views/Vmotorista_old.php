<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Motorista Historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $motorista=fncgetmotorista($_GET['id']);
}else{
    echo "HOUVE ALGUM ERRO";
}
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do motorista
                </div>
                <div class="card-body">
                    <blockquote class="blockquote blockquote-info">
                        <header>MOTORISTA: <strong class="text-info"><?php echo strtoupper($motorista['motorista']); ?>&nbsp;&nbsp;</strong></header>

                        <h5>
                            CPF: <strong class="text-info"><?php echo mask($motorista['cpf'],'###.###.###_##'); ?></strong>
                        </h5>
                        <h5>
                            NASCIMENTO: <strong class="text-info"><?php echo dataBanco2data($motorista['nascimento']); ?></strong>
                        </h5>
                </blockquote>
            </div>
        </div>

        <div class="col-md-12">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-viagens-tab" data-toggle="pill" href="#pills-viagens" role="tab" aria-controls="pills-viagens" aria-selected="true">Viagens</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-abastecimentos-tab" data-toggle="pill" href="#pills-abastecimentos" role="tab" aria-controls="pills-abastecimentos" aria-selected="false">Abastecimentos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-km-tab" data-toggle="pill" href="#pills-km" role="tab" aria-controls="pills-km" aria-selected="false">Lançamentos</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <?php
                $sql = "SELECT * "
                    . "FROM mcu_frota_viagem  "
                    . "WHERE  cod_motorista = ? "
                    ."ORDER BY "
                    ."`data` DESC";
                global $pdo;
                $cons = $pdo->prepare($sql);
                $cons->bindParam(1, $_GET['id']);
                $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                $viagens = $cons->fetchAll();
                $sql=null;
                $consulta=null;
                ?>
                <div class="tab-pane fade show active" id="pills-viagens" role="tabpanel" aria-labelledby="pills-viagens-tab">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                        <tr>
                            <th>CARRO</th>
                            <th>DATA</th>
                            <th>DESTINO</th>
                            <th>MOTIVO</th>
                            <th>PASSAGEIROS</th>
                            <th>EDITAR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // vamos criar a visualização
                        foreach($viagens as $dados){
                            $id = $dados["id"];
                            $carro = fncgetcarro($dados["cod_carro"]);
                            $data=$dados["data"];
                            $destino=fncgetibge($dados["destino"]);
                            $motivo=$dados["motivo"];
                            $passageiros=$dados["passageiros"];
                            ?>
                            <tr>
                                <td><a href="index.php?pg=Vcarro&id=<?php echo $carro['id']; ?>"><?php echo $carro['carro']; ?></a></td>
                                <td>
                                <?php
                                echo dataBanco2data($data);
                                ?>
                                </td>
                                <td><?php echo utf8_encode($destino['cidade']." ".$destino['uf']); ?></td>
                                <td><?php echo $motivo; ?></td>
                                <td><?php echo $passageiros; ?></td>
                                <td>
                                    <?php
                                    if ($allow["allow_2"]==1){ ?>
                                        <a href="index.php?pg=Vviagem_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                                        <?php
                                    }else{
                                        echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                                    }
                                    ?>

                                    <i class="badge badge-warning float-right">
                                        <strong><?php echo $id; ?></strong>
                                    </i>
                                </td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-abastecimentos" role="tabpanel" aria-labelledby="pills-abastecimentos-tab">
                    <?php
                    $sql = "SELECT * "
                        . "FROM mcu_frota_abastecimento  "
                        . "WHERE  mcu_frota_abastecimento.cod_motorista = ? "
                        ."ORDER BY "
                        ."mcu_frota_abastecimento.`data` DESC";
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $abastecimentos = $cons->fetchAll();
                    $sql=null;
                    $consulta=null;
                    ?>
                    <table id="tabela" class="table table-striped table-sm table-hover">
                        <thead class="bg-primary">
                        <tr>
                            <th>CARRO</th>
                            <th>DATA</th>
                            <th>QUANTIDADE</th>
                            <th>VALOR</th>
                            <th>KM</th>
                            <th>EDITAR</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // vamos criar a visualização
                        foreach ($abastecimentos as $dados) {
                            $id = $dados["id"];
                            $carro=fncgetcarro($dados["cod_carro"]);
                            $data= dataBanco2data($dados["data"]);
                            $quantidade=$dados["quantidade"];
                            $valor=$dados["valor"];
                            $km=$dados["km"];
                            ?>

                            <tr>
                                <td><a href="index.php?pg=Vcarro&id=<?php echo $carro['id']; ?>"><?php echo $carro['carro']; ?></a></td>
                                <td><?php echo $data; ?></td>
                                <td><?php echo $quantidade; ?> Lts</td>
                                <td>R$ <?php echo $valor; ?></td>
                                <td><?php echo $km; ?></td>
                                <td>
                                    <?php
                                    if ($allow["allow_2"]==1){ ?>
                                        <a href="index.php?pg=Vabastecimento_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                                        <?php
                                    }else{
                                        echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                                    }
                                    ?>
                                    <i class="badge badge-warning float-right">
                                        <strong><?php echo $id; ?></strong>
                                    </i>
                                </td>
                            </tr>

                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-km" role="tabpanel" aria-labelledby="pills-km-tab">
                    <?php
                    $sql = "SELECT * "
                        . "FROM mcu_frota_km  "
                        . "WHERE  cod_motorista = ? "
                        ."ORDER BY "
                        ."`id` DESC";
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $kms = $cons->fetchAll();
                    $sql=null;
                    $consulta=null;
                    ?>
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                        <tr>
                            <th>CARRO</th>
                            <th>PERIODO</th>
                            <th>TIPO</th>
                            <th>KM</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($kms as $dados){
                            $id = $dados["id"];
                            $carro = fncgetcarro($dados["cod_carro"]);
                            $data_ts=datahoraBanco2data($dados["data_ts"]);
                            $periodo=datahoraBanco2data($dados["periodo"]);
                            $tipo=$dados["tipo"];
                            if ($tipo==1){
                                $tipo="<i class=''>KM</i>";
                            }
                            if ($tipo==2){
                                $tipo="<i class=''>óleo</i>";
                            }
                            $kilometro=$dados["kilometro"];
                            $descricao=$dados["descricao"];
                            ?>

                            <tr>
                                <td><a href="index.php?pg=Vcarro&id=<?php echo $carro['id']; ?>"><?php echo $carro['carro']; ?></a></td>
                                <td><?php echo $periodo; ?></td>
                                <td><?php echo $tipo; ?></td>
                                <td><?php echo $kilometro; ?></td>
                                <td>
                                    <i class="badge badge-warning">
                                        <strong><?php echo $id; ?></strong>
                                    </i>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><?php echo $descricao; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>