<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Motorista Historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<?php
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $motorista=fncgetmotorista($_GET['id']);
}else{
    echo "HOUVE ALGUM ERRO";
}
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-2">
                <div class="card-header bg-info text-light">
                    Dados do motorista
                </div>
                <div class="card-body">
                    <blockquote class="blockquote blockquote-info">
                        <header>MOTORISTA: <strong class="text-info"><?php echo strtoupper($motorista['motorista']); ?>&nbsp;&nbsp;</strong></header>

                        <h5>
                            CPF: <strong class="text-info"><?php echo mask($motorista['cpf'],'###.###.###_##'); ?></strong>
                        </h5>
                        <h5>
                            NASCIMENTO: <strong class="text-info"><?php echo dataBanco2data($motorista['nascimento']); ?></strong>
                        </h5>
                </blockquote>
            </div>
        </div>

        <div class="col-md-12">
            <a href="index.php?pg=Vmotorista&id=<?php echo $_GET['id'];?>" class="btn btn-sm btn-info">Kilometragem</a>
            <a href="#" class="btn btn-sm btn-outline-info">Viagens</a>
            <a href="#" class="btn btn-sm btn-outline-info">Abastecimento</a>

            <hr>
            <form action="index.php" method="get" target="_blank">
                <div class="input-group mb-3 col-md-6 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-warning" type="submit"><i class="fas fa-print"></i></button>
                    </div>
                    <input name="pg" value="Vmotorista_print" hidden/>
                    <input name="id" value="<?php echo $_GET['id'];?>" hidden/>
                    <input type="date" name="sca" id="sca" autocomplete="off" class="form-control" value="" required />
                    <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="" required />
                </div>
            </form>

                    <?php
                    $sql = "SELECT * "
                        . "FROM mcu_frota_km  "
                        . "WHERE  cod_motorista = ? "
                        ."ORDER BY "
                        ."`periodo` DESC";
                    global $pdo;
                    $cons = $pdo->prepare($sql);
                    $cons->bindParam(1, $_GET['id']);
                    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $kms = $cons->fetchAll();
                    $sql=null;
                    $consulta=null;
                    ?>
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                        <tr>
                            <th>CARRO</th>
                            <th>PERIODO</th>
                            <th>TIPO</th>
                            <th>KM</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($kms as $dados){
                            $id = $dados["id"];
                            $carro = fncgetcarro($dados["cod_carro"]);
                            $data_ts=datahoraBanco2data($dados["data_ts"]);
                            $periodo=datahoraBanco2data($dados["periodo"]);
                            $tipo=$dados["tipo"];
                            if ($tipo==1){
                                $tipo="<i class=''>KM</i>";
                            }
                            if ($tipo==2){
                                $tipo="<i class=''>óleo</i>";
                            }
                            $kilometro=$dados["kilometro"];
                            $descricao=$dados["descricao"];
                            ?>

                            <tr>
                                <td><a href="index.php?pg=Vcarro&id=<?php echo $carro['id']; ?>"><?php echo $carro['carro']; ?></a></td>
                                <td><?php echo $periodo; ?></td>
                                <td><?php echo $tipo; ?></td>
                                <td><?php echo $kilometro; ?></td>
                                <td>
                                    <i class="badge badge-warning">
                                        <strong><?php echo $id; ?></strong>
                                    </i>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><?php echo $descricao; ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>


        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>