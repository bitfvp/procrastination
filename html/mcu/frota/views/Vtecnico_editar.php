<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Tecnico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="tecnicosave";
    $tecnico=fncgettecnico($_GET['id']);
}else{
    $a="tecniconew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vtecnico_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de tecnicos</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" name="id" value="<?php echo $tecnico['id']; ?>"/>
                <label for="nome">NOME:</label>
                <input autocomplete="off" id="nome" placeholder="nome" type="text" class="form-control" name="nome" value="<?php echo $tecnico['nome']; ?>"/>
            </div>
        </div>

        <div class="row">

            <div class="col-md-5">
                <label for="segunda1">segunda1</label>
                <select name="segunda1" id="segunda1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['segunda1'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['segunda1'];
                    } ?>">
                        <?php
                        if ($tecnico['segunda1'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['segunda1'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="segunda2">segunda2</label>
                <select name="segunda2" id="segunda2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['segunda2'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['segunda2'];
                    } ?>">
                        <?php
                        if ($tecnico['segunda2'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['segunda2'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="terca1">terca1</label>
                <select name="terca1" id="terca1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['terca1'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['terca1'];
                    } ?>">
                        <?php
                        if ($tecnico['terca1'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['terca1'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="terca2">terca2</label>
                <select name="terca2" id="terca2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['terca2'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['terca2'];
                    } ?>">
                        <?php
                        if ($tecnico['terca2'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['terca2'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="quarta1">quarta1</label>
                <select name="quarta1" id="quarta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['quarta1'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['quarta1'];
                    } ?>">
                        <?php
                        if ($tecnico['quarta1'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['quarta1'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="quarta2">quarta2</label>
                <select name="quarta2" id="quarta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['quarta2'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['quarta2'];
                    } ?>">
                        <?php
                        if ($tecnico['quarta2'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['quarta2'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="quinta1">quinta1</label>
                <select name="quinta1" id="quinta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['quinta1'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['quinta1'];
                    } ?>">
                        <?php
                        if ($tecnico['quinta1'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['quinta1'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="quinta2">quinta2</label>
                <select name="quinta2" id="quinta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['quinta2'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['quinta2'];
                    } ?>">
                        <?php
                        if ($tecnico['quinta2'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['quinta2'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="sexta1">sexta1</label>
                <select name="sexta1" id="sexta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['sexta1'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['sexta1'];
                    } ?>">
                        <?php
                        if ($tecnico['sexta1'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['sexta1'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="sexta2">sexta2</label>
                <select name="sexta2" id="sexta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($tecnico['sexta2'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $tecnico['sexta2'];
                    } ?>">
                        <?php
                        if ($tecnico['sexta2'] == 0) {
                            echo "Não";
                        }
                        if ($tecnico['sexta2'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>



            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
        </div>
    </form>


</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>