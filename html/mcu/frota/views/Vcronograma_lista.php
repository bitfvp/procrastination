<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de cronogramas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_frota_cronograma ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_frota_cronograma ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY data desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Listagem de cronograma</h2>
    <hr>
    <div class="row mb-2">
        <div class="col-md-6">
            <a href="index.php?pg=Vcronograma_editar" class="btn btn-success btn-block float-left">
                NOVO
            </a>
        </div>
        <div class="col-md-6">
            <a href="index.php?pg=Vcronograma" target="_blank" class="btn btn-info btn-block float-right">
                IMPRIMIR
            </a>
        </div>
    </div>




    <table class="table table-striped table-hover table-sm ">
        <thead class="thead-dark">
            <tr>
                <th>DATA</th>
                <th>MOTORISTA</th>
                <th>ATIVO</th>
                <th>EDITAR</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $data = datahoraBanco2data($dados["data"]);
            $motorista=fncgetmotorista($dados["motorista"])['motorista'];
            $ativo=$dados["ativo"];
            ?>
            <tr>
                <td>
                    <?php echo $data; ?>
                </td>
                <td>
                    <?php echo $motorista; ?>
                    <a data-toggle="collapse" title="listar" data-target="#accordion<?php echo $id; ?>" class="btn btn-sm btn-info fa fa-list clickable float-left m-1"></a>
                </td>
                <td>
                    <?php
                    if ($ativo==1){
                        echo "<i class='text-success fa fa-hand-peace'>Ativo</i>";
                    }else{
                        echo "<i class='text-danger fa fa-thumbs-down'>Desativado</i>";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($allow["allow_2"]==1){ ?>
                    <a href="index.php?pg=Vcronograma_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                        <?php
                    }else{
                        echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                    }
                    ?>
                </td>
            </tr>


            <tr id='accordion<?php echo $id;?>' class='collapse'>
                <td colspan='8' class="bg-secondary">
                    <div class="row bg-<?php echo $statuss;?>">
                        <div class="col-md-2">
                            <ul>
                                Manhã
                            </ul>
                            <ul>
                                Tarde
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <?php echo fncgettecnico($dados["segunda1"])['nome'];?>
                            </ul>
                            <ul>
                                <?php echo fncgettecnico($dados["segunda2"])['nome'];?>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <?php echo fncgettecnico($dados["terca1"])['nome'];?>
                            </ul>
                            <ul>
                                <?php echo fncgettecnico($dados["terca2"])['nome'];?>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <?php echo fncgettecnico($dados["quarta1"])['nome'];?>
                            </ul>
                            <ul>
                                <?php echo fncgettecnico($dados["quarta2"])['nome'];?>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <?php echo fncgettecnico($dados["quinta1"])['nome'];?>
                            </ul>
                            <ul>
                                <?php echo fncgettecnico($dados["quinta2"])['nome'];?>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <ul>
                                <?php echo fncgettecnico($dados["sexta1"])['nome'];?>
                            </ul>
                            <ul>
                                <?php echo fncgettecnico($dados["sexta2"])['nome'];?>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>



            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
