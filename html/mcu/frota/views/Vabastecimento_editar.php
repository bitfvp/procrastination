<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar abastecimento-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="abastecimentosave";
    $abastecimento=fncgetabastecimento($_GET['id']);
}else{
    $a="abastecimentonew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vabastecimento_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de abastecimentos</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $abastecimento['id']; ?>"/>
                <label for="data">DATA:</label>
                <input autocomplete="off" autofocus id="data" type="date" class="form-control" name="data" value="<?php echo $abastecimento['data']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="cod_motorista">MOTORISTA:</label>
                <select name="cod_motorista" required="true" id="cod_motorista" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $abastecimento['cod_motorista'];?>">
                        <?php
                        $motoristaid=$abastecimento['cod_motorista'];
                        $motorista=fncgetmotorista($motoristaid);
                        echo $motorista['nome'];
                        ?>
                    </option>
                    <?php
                    foreach(fncmotoristalist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <label for="cod_carro">CARRO:</label>
                <select name="cod_carro" required="true" id="cod_carro" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $abastecimento['cod_carro'];?>">
                        <?php
                        $carroid=$abastecimento['cod_carro'];
                        $carro=fncgetcarro($carroid);
                        echo $carro['carro'];
                        ?>
                    </option>
                    <?php
                    foreach(fnccarrolist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['carro']." ".$item['placa']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <label for="quantidade">QUANTIDADE:(use virgula)</label>
                <input autocomplete="off"  id="quantidade" placeholder="quantidade" type="number" step="0.01" class="form-control" name="quantidade" value="<?php echo $abastecimento['quantidade']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="valor">VALOR:(use virgula)</label>
                <input autocomplete="off"  id="valor" placeholder="valor" type="number" step="0.01" class="form-control" name="valor" value="<?php echo $abastecimento['valor']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="km">KM:</label>
                <input autocomplete="off"  id="km" placeholder="km" type="number" class="form-control" name="km" value="<?php echo $abastecimento['km']; ?>"/>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
        </div>
    </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>