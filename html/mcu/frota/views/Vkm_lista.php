<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de lançamentos de KM-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");


if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_frota_km WHERE periodo LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_frota_km ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY periodo desc LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY periodo desc LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

<main class="container"><!--todo conteudo-->
    <h2>Importação de KM</h2>
    <hr>



    <form class="form-signin" action="index.php?pg=Vkm_lista&aca=kmnew" method="post" enctype="multipart/form-data">
         <div class="row">
             <div class="col-md-6 set">
                 <label for="arquivo">Arquivo:</label>
                 <input type="file" name="arquivo" class="form-control" >
             </div>
         </div>
            <div class="col-md-10">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block my-2" />
            </div>
        </div>
    </form>



    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vkm_lista" hidden/>
            <input type="date" name="sca" id="sca" autocomplete="off" class="form-control" aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>

    <table class="table table-striped table-hover table-sm">
        <thead>
            <tr>
                <th>MOTORISTA</th>
                <th>CARRO</th>
                <th>PERÍODO</th>
                <th>TIPO</th>
                <th>KM</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $motorista = fncgetmotorista($dados["cod_motorista"]);
            $carro = fncgetcarro($dados["cod_carro"]);
            $data_ts=datahoraBanco2data($dados["data_ts"]);
            $tipo=$dados["tipo"];
            if ($tipo==1){
                $tipo="<i class=''>KM</i>";
            }
            if ($tipo==2){
                $tipo="<i class=''>óleo</i>";
            }
            $periodo=datahoraBanco2data($dados["periodo"]);
            $kilometro=$dados["kilometro"];
            $descricao=$dados["descricao"];
            ?>

            <tr>
                <td><a href="index.php?pg=Vmotorista&id=<?php echo $dados['cod_motorista']; ?>"><?php echo strtoupper($motorista['motorista']); ?></a></td>
                <td><a href="index.php?pg=Vcarro&id=<?php echo $dados['cod_carro']; ?>"><?php echo $carro['carro']; ?></a></td>
                <td><?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = $periodo;
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo $periodo;
                    }
                    ?>
                </td>
                <td><?php echo $tipo; ?></td>
                <td><?php echo $kilometro; ?></td>
                <td>
                    <i class="badge badge-warning">
                        <strong><?php echo $id; ?></strong>
                    </i>
                </td>
            </tr>
            <tr>
                <td colspan="6"><?php echo $descricao; ?></td>
            </tr>

	<?php
        }
        ?>
</tbody>
</table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
