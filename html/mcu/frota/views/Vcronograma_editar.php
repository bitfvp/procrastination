<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Cronograma-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="cronogramasave";
    $cronograma=fncgetcronograma($_GET['id']);
}else{
    $a="cronogramanew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcronograma_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de cronogramas</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" name="id" value="<?php echo $cronograma['id']; ?>"/>
                <label for="ativo">ativo</label>
                <select name="ativo" id="ativo" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['ativo'] == "") {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['ativo'];
                    } ?>">
                        <?php
                        if ($cronograma['ativo'] == 0) {
                            echo "Não";
                        }
                        if ($cronograma['ativo'] == 1) {
                            echo "Sim";
                        } ?>
                    </option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="motorista">MOTORISTA:</label>
                <select name="motorista" required="true" id="motorista" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $cronograma['motorista'];?>">
                        <?php
                        echo fncgetmotorista($cronograma['motorista'])['motorista'];
                        ?>
                    </option>
                    <?php
                    foreach(fncmotoristalist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['motorista']; ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="row">

            <div class="col-md-5">
                <label for="segunda1">Segunda-Feira Manhã</label>
                <select name="segunda1" id="segunda1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['segunda1'] == "" or $cronograma['segunda1'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['segunda1'];
                    } ?>">
                        <?php

                        if ($cronograma['segunda1'] =="" or $cronograma['segunda1'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['segunda1'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("segunda1") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                    <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="segunda2">Segunda-Feira Tarde</label>
                <select name="segunda2" id="segunda2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['segunda2'] == "" or $cronograma['segunda2'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['segunda2'];
                    } ?>">
                        <?php
                        if ($cronograma['segunda2'] == "" or $cronograma['segunda2'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['segunda2'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("segunda2") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="terca1">Terca-Feira Manhã</label>
                <select name="terca1" id="terca1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['terca1'] == "" or $cronograma['terca1'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['terca1'];
                    } ?>">
                        <?php
                        if ($cronograma['terca1'] == "" or $cronograma['terca1'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['terca1'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("terca1") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="terca2">Terca-Feira Tarde</label>
                <select name="terca2" id="terca2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['terca2'] == "" or $cronograma['terca2'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['terca2'];
                    } ?>">
                        <?php
                        if ($cronograma['terca2'] == "" or $cronograma['terca2'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['terca2'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("terca2") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="quarta1">Quarta-Feira Manhã</label>
                <select name="quarta1" id="quarta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['quarta1'] == "" or $cronograma['quarta1'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['quarta1'];
                    } ?>">
                        <?php
                        if ($cronograma['quarta1'] == "" or $cronograma['quarta1'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['quarta1'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("quarta1") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="quarta2">Quarta-Feira Tarde</label>
                <select name="quarta2" id="quarta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['quarta2'] == "" or $cronograma['quarta2'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['quarta2'];
                    } ?>">
                        <?php
                        if ($cronograma['quarta2'] == "" or $cronograma['quarta2'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['quarta2'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("quarta2") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="quinta1">Quinta-Feira Manhã</label>
                <select name="quinta1" id="quinta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['quinta1'] == "" or $cronograma['quinta1'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['quinta1'];
                    } ?>">
                        <?php
                        if ($cronograma['quinta1'] == "" or $cronograma['quinta1'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['quinta1'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("quinta1") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="quinta2">Quinta-Feira Tarde</label>
                <select name="quinta2" id="quinta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['quinta2'] == "" or $cronograma['quinta2'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['quinta2'];
                    } ?>">
                        <?php
                        if ($cronograma['quinta2'] == "" or $cronograma['quinta2'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['quinta2'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("quinta2") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>

            <div class="col-md-5">
                <label for="sexta1">Sexta-Feira Manhã</label>
                <select name="sexta1" id="sexta1" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['sexta1'] == "" or $cronograma['sexta1'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['sexta1'];
                    } ?>">
                        <?php
                        if ($cronograma['sexta1'] == "" or $cronograma['sexta1'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['sexta1'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("sexta1") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>
            <div class="col-md-5">
                <label for="sexta2">Sexta-Feira Tarde</label>
                <select name="sexta2" id="sexta2" class="form-control">// vamos criar a visualização
                    <option selected="" value="<?php if ($cronograma['sexta2'] == "" or $cronograma['sexta2'] ==0) {
                        $z = 0;
                        echo $z;
                    } else {
                        echo $cronograma['sexta2'];
                    } ?>">
                        <?php
                        if ($cronograma['sexta2'] == "" or $cronograma['sexta2'] ==0){
                            echo "Vazio";
                        }else{
                            echo fncgettecnico($cronograma['sexta2'])['nome'];
                        }
                        ?>
                    </option>
                    <?php
                    foreach(fnctecnicolistfilto("sexta2") as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                        <?php
                    }
                    ?>
                    <option value="0">Vazio</option>
                </select>
            </div>



            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
        </div>
    </form>


</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>