<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Carro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="motoristaupdate";
    $motorista=fncgetmotorista($_GET['id']);
}else{
    $a="motoristainsert";
}
?>
<div class="container"><!--todo conteudo-->
    <h2>Cadastro de motorista</h2>
    <hr>
    <form class="form-signin" action="<?php echo "index.php?pg=Vmotorista_lista&aca={$a}"; ?>" method="post">
        <div class="row">
            <div class="col-md-8">
                <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $motorista['id']; ?>"/>
                <label for="nome">NOME</label>
                <input autocomplete="off" autofocus id="motorista" type="text" class="form-control" name="motorista" value="<?php echo $motorista['motorista']; ?>"/>
            </div>
            <script type="text/javascript">
                $(function(){
                    var campo = $("#motorista");
                    campo.keyup(function(e){
                        e.preventDefault();
                        campo.val($(this).val().toLowerCase());
                    });
                });

            </script>
            <div class="col-md-4">
                <label for="identificacao">IDENTIFICAÇÃO</label>
                <input autocomplete="off" id="identificacao" type="text" class="form-control" name="identificacao" value="<?php echo $motorista['identificacao']; ?>"/>
            </div>
            <div class="col-md-6">
                <label for="nascimento">NASCIMENTO</label>
                <input id="nascimento" type="date" class="form-control" name="nascimento" value="<?php echo $motorista['nascimento'];?>"/>
            </div>
            <div class="col-md-6">
                <label for="cpf">CPF</label>
                <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $motorista['cpf']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-12">
                <input type="submit" id="salvar" name="salvar" class="btn btn-success btn-block mt-2" value="SALVAR"/>
            </div>

        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>