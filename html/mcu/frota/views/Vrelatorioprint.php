<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="relatório-abastecimentos-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial'];
    $final = $_POST['data_final'];

    $sql = "SELECT "
        . "mcu_frota_carro.carro, "
        . "Sum(mcu_frota_abastecimento.quantidade) as quantidade, "
        . "Sum(mcu_frota_abastecimento.valor) as valor "
        . "FROM "
        . "mcu_frota_carro "
        . "INNER JOIN mcu_frota_abastecimento ON mcu_frota_carro.id = mcu_frota_abastecimento.cod_carro "
        . "WHERE "
        . "mcu_frota_abastecimento.`data` >= :inicial AND "
        . "mcu_frota_abastecimento.`data` <= :final "
        . "GROUP BY "
        . "mcu_frota_carro.carro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $porcarro = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

    $sql = "SELECT "
        . "tbl_users.nome, "
        . "Sum(mcu_frota_abastecimento.quantidade) as quantidade, "
        . "Sum(mcu_frota_abastecimento.valor) as valor "
        . "FROM "
        . "mcu_frota_abastecimento "
        . "INNER JOIN tbl_users ON tbl_users.id = mcu_frota_abastecimento.cod_motorista "
        . "WHERE "
        . "mcu_frota_abastecimento.`data` >= :inicial AND "
        . "mcu_frota_abastecimento.`data` <= :final "
        . "GROUP BY "
        . "tbl_users.nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pormotorista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;

    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h3>Relatório de abastecimentos por periodo </h3>

        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>CARRO&nbsp;</td>
                <td>LITROS&nbsp;</td>
                <td>VALOR&nbsp;</td>
                <td>MEDIA/VALOR&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($porcarro as $pc) {
                $litros = number_format($pc['quantidade'], 2, '.', ',');
                $valor=number_format($pc['valor'], 2, '.', ',');
                ?>
            <tr>
                <td><?php echo $pc['carro']; ?>&nbsp;</td>
                <td><?php echo $litros; ?>&nbsp;</td>
                <td><?php echo "R$ ".$valor; ?>&nbsp;</td>
                <td><?php echo "R$ ".$pc['valor'] / $pc['quantidade']; ?>&nbsp;</td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <hr>

        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <td>MOTORISTA&nbsp;</td>
                <td>LITROS&nbsp;</td>
                <td>VALOR&nbsp;</td>
                <td>MEDIA/VALOR&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($pormotorista as $pc) {
                $litros = number_format($pc['quantidade'], 2, '.', ',');
                $valor=number_format($pc['valor'], 2, '.', ',');
                ?>
                <tr>
                    <td><?php echo $pc['nome']; ?>&nbsp;</td>
                    <td><?php echo $litros; ?>&nbsp;</td>
                    <td><?php echo "R$ ".$valor; ?>&nbsp;</td>
                    <td><?php echo "R$ ".$pc['valor'] / $pc['quantidade']; ?>&nbsp;</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</main>
</body>
</html>