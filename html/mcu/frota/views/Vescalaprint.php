<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Escala-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
$mes = $_POST['mes'];
$dias = $_POST['dias'];
$semana = $_POST['semana'];


$ordema=$_POST["ordema"];
$ordema = preg_replace('/\r?\n/', ' ', $ordema);
$ordema = str_replace(' ', ',', $ordema);
$ordema = str_replace(',,', ',', $ordema);
$ordema = str_replace(',,,', ',', $ordema);
$ordema=explode(',', $ordema);
$ordematemp=0;
$ordemacount = count($ordema);
$ordemacount = $ordemacount-1;


$ordemb=$_POST["ordemb"];
$ordemb = preg_replace('/\r?\n/', ' ', $ordemb);
$ordemb = str_replace(' ', ',', $ordemb);
$ordemb = str_replace(',,', ',', $ordemb);
$ordemb = str_replace(',,,', ',', $ordemb);
$ordemb=explode(',', $ordemb);
$ordembtemp=0;
$ordembcount = count($ordema);
//$ordembcount = $ordembcount-1;
?>
<!--começa o conteudo-->
<main class="container text-uppercase">
    <h2 class="text-center">Escala <?php echo $mes;?></h2>
    <table class="table table-bordered text-center">
        <thead>
        <tr>
            <td colspan="2" class="bg-info">DOMINGO</td>
            <td colspan="2" class="bg-warning">SEGUNDA</td>
            <td colspan="2" class="bg-warning">TERÇA</td>
            <td colspan="2" class="bg-warning">QUARTA</td>
            <td colspan="2" class="bg-warning">QUINTA</td>
            <td colspan="2" class="bg-info">SEXTA</td>
            <td colspan="2" class="bg-info">SÁBADO</td>
        </tr>
        </thead>

        <?php
        for ($dia = 1; $dia <= $dias; $dia++) {
//            echo $dia;
//            echo "<br>";
//
//            echo retornadiasemana($semana);

                if (($dia==1 and $semana==1) or ($semana==1)){
                    echo "<tr>\n";
                }
                if ($dia==1 and $semana==2){
                    echo "<tr>\n";
                    echo "<td colspan='2'></td>\n";
                }
                if ($dia==1 and $semana==3){
                    echo "<tr>\n";
                    echo "<td colspan='4'></td>\n";
                }
                if ($dia==1 and $semana==4){
                    echo "<tr>\n";
                    echo "<td colspan='6'></td>\n";
                }
                if ($dia==1 and $semana==5){
                    echo "<tr>\n";
                    echo "<td colspan='8'></td>\n";
                }
                if ($dia==1 and $semana==6){
                    echo "<tr>\n";
                    echo "<td colspan='10'></td>\n";
                }
                if ($dia==1 and $semana==7){
                    echo "<tr>\n";
                    echo "<td colspan='12'></td>\n";
                }


                if ($semana>1 and $semana<6){
                    //meio da semana
                    $texto = $ordema[$ordematemp];
                    if ($ordematemp==$ordemacount){
                        $ordematemp=0;
                    }else{
                        $ordematemp++;
                    }
                    $cor="bg-warning";
                }else{
                    //fim de semana

                    $texto = $ordemb[$ordembtemp];
                    if ($semana==1){
                        if ($ordembtemp==$ordembcount){
                            $ordembtemp=0;
                        }else{
                            $ordembtemp++;
                        }
                    }
                    $cor="bg-info";
                }

            echo "<td class='{$cor}'>\n";
                echo $dia;
            echo "</td>\n";

            echo "<td class=''>\n";
                echo $texto;
            echo "</td>\n";







            if ($dia==$dias or $semana==7){
                echo "</tr>\n\n";
            }



            if ($semana<7){
                $semana++;
            }else{
                $semana=1;
            }


        }

        ?>


    </table>



</main>
</body>
</html>