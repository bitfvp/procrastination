<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Lista de Motoristas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from mcu_frota_motorista WHERE motorista LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from mcu_frota_motorista ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY motorista LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY motorista LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container"><!--todo conteudo-->
    <h2>Listagem de motoristas</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vmotorista_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por veículo..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <?php if ($allow["allow_2"]==1){ ?>
    <a href="index.php?pg=Vmotorista_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO MOTORISTA
    </a>
    <?php }?>
    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <table class="table table-striped table-hover table-sm">
        <thead>
            <tr>
                <th>MOTORISTA</th>
                <th>CPF</th>
                <th>NASCIMENTO</th>
                <th>IDENTIFICAÇÃO</th>
                <th>STATUS</th>
                <th>EDITAR</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $motorista = strtoupper($dados["motorista"]);
            $cpf = $dados["cpf"];
            $nascimento = $dados["nascimento"];
            $identificacao = $dados["identificacao"];
            $status = $dados["status"];
            ?>
            <tr>
                <td>
                    <a href="index.php?pg=Vmotorista&id=<?php echo $id; ?>">
                    <?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = $motorista;
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo $motorista;
                    }
                    ?>
                    </a>
                </td>
                <td><?php echo $cpf; ?></td>
                <td><?php echo dataBanco2data($nascimento); ?></td>
                <td><?php echo $identificacao; ?></td>
                <td>
                    <?php
                    if ($status==1){
                        echo "<i class='text-success fa fa-hand-peace'>Ativo</i>";
                    }else{
                        echo "<i class='text-danger fa fa-thumbs-down'>Desativado</i>";
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if ($allow["allow_2"]==1){ ?>
                    <a href="index.php?pg=Vmotorista_editar&id=<?php echo $id; ?>"><span class="fa fa-pen"></span></a>
                        <?php
                    }else{
                        echo "<i class='fa fa-ban' title='você não tem permissão pra editar'></i>";
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
