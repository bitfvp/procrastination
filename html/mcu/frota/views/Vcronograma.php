<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}

$page="Cronogramas de motoristas-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

    $sql = "select * from mcu_frota_cronograma \n"
        . "WHERE  ativo=1 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cronogramas = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid text-uppercase">
<?php
foreach ($cronogramas as $crono){
    echo "<h4 class='text-center'>".strtoupper(fncgetmotorista($crono['motorista'])['motorista'])."</h4>"
    ?>
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Manhã</th>
                <th>Tarde</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Segunda-feira</td>
                <td><?php echo strtoupper(fncgettecnico($crono['segunda1'])['nome']);?></td>
                <td><?php echo strtoupper(fncgettecnico($crono['segunda2'])['nome']);?></td>
            </tr>
            <tr>
                <td>Terça-feira</td>
                <td><?php echo strtoupper(fncgettecnico($crono['terca1'])['nome']);?></td>
                <td><?php echo strtoupper(fncgettecnico($crono['terca2'])['nome']);?></td>
            </tr>
            <tr>
                <td>Quarta-feira</td>
                <td><?php echo strtoupper(fncgettecnico($crono['quarta1'])['nome']);?></td>
                <td><?php echo strtoupper(fncgettecnico($crono['quarta2'])['nome']);?></td>
            </tr>
            <tr>
                <td>Quinta-feira</td>
                <td><?php echo strtoupper(fncgettecnico($crono['quinta1'])['nome']);?></td>
                <td><?php echo strtoupper(fncgettecnico($crono['quinta2'])['nome']);?></td>
            </tr>
            <tr>
                <td>Sexta-feira</td>
                <td><?php echo strtoupper(fncgettecnico($crono['sexta1'])['nome']);?></td>
                <td><?php echo strtoupper(fncgettecnico($crono['sexta2'])['nome']);?></td>
            </tr>
        </tbody>
    </table>
    <hr>
    <?php
}
?>

</div>
</body>
</html>