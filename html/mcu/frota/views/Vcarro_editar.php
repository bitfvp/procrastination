<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Carro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="carrosave";
    $carro=fncgetcarro($_GET['id']);
}else{
    $a="carronew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcarro_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de veículos</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $carro['id']; ?>"/>
                <label for="carro">DESCRIÇÃO:</label>
                <input autocomplete="off" id="carro" placeholder="carro" type="text" class="form-control" name="carro" value="<?php echo $carro['carro']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="placa">PLACA:</label>
                <input autocomplete="off" maxlength="8" id="placa" placeholder="placa" type="text" class="form-control" name="placa" value="<?php echo $carro['placa']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#placa').mask('AAA-0A00', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-5">
                <label for="renavam">RENAVAM:</label>
                <input autocomplete="off" maxlength="11" id="renavam" placeholder="renavam" type="text" class="form-control" name="renavam" value="<?php echo $carro['renavam']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="chassi">CHASSI:</label>
                <input autocomplete="off" maxlength="17" id="chassi" placeholder="chassi" type="text" class="form-control" name="chassi" value="<?php echo $carro['chassi']; ?>"/>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>