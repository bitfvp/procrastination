<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">

<div class="row">
    <div class="col-md-8 offset-2">

        <div class="card">
            <div class="card-header bg-info text-light">
            Escala
            </div>
            <div class="card-body">
                <form action="index.php?pg=Vescalaprint" method="post" target="_blank">
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="GERAR"/>

                    <div class="form-group">
                        <label for="mes">Mẽs:</label>
                        <input id="mes" type="text" class="form-control" name="mes" value="" required/>
                    </div>

                    <div class="form-group">
                        <label for="dias">Dias no mẽs:</label>
                        <input id="dias" type="number" class="form-control" name="dias" value="30" max="31" min="28" required/>
                    </div>

                    <div class="form-group">
                        <label for="semana">Mês começa em:</label>
                        <select name="semana" id="semana" class="form-control">
                            <option selected="" value="2">SEGUNDA-FEIRA</option>
                            <option value="1">DOMINGO</option>
                            <option value="2">SEGUNDA-FEIRA</option>
                            <option value="3">TERÇA-FEIRA</option>
                            <option value="4">QUARTA-FEIRA</option>
                            <option value="5">QUINTA-FEIRA</option>
                            <option value="6">SEXTA-FEIRA</option>
                            <option value="7">SÁBADO</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ordema" class="form-label">Ordem A</label>
                        <textarea required name="ordema" id="ordema" class="form-control" cols="30" rows="4"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="ordemb" class="form-label">Ordem B</label>
                        <textarea required name="ordemb" id="ordemb" class="form-control" cols="30" rows="4"></textarea>
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>