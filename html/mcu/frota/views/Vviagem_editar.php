<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar viagem-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="viagemsave";
    $viagem=fncgetviagem($_GET['id']);
}else{
    $a="viagemnew";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vviagem_lista&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de viagem</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $viagem['id']; ?>"/>
                <label for="data">DATA:</label>
                <input autocomplete="off" autofocus id="data" type="date" class="form-control" name="data" value="<?php echo $viagem['data']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="cod_motorista">MOTORISTA:</label>
                <select name="cod_motorista" required="true" id="cod_motorista" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $viagem['cod_motorista'];?>">
                        <?php
                        $motorista=fncgetmotorista($viagem['cod_motorista']);
                        echo $motorista['nome'];
                        ?>
                    </option>
                    <?php
                    foreach(fncmotoristalist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <label for="cod_carro">CARRO:</label>
                <select name="cod_carro" required="true" id="cod_carro" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $viagem['cod_carro'];?>">
                        <?php
                        $carro=fncgetcarro($viagem['cod_carro']);
                        echo $carro['carro'];
                        ?>
                    </option>
                    <?php
                    foreach(fnccarrolist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['carro']." ".$item['placa']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <label for="destino">DESTINO:</label>
                <select name="destino" required="true" id="destino" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $viagem['destino'];?>">
                        <?php
                        $destinoid=$viagem['destino'];
                        $destino=fncgetibge($destinoid);
                        echo utf8_encode($destino['cidade']." ".$destino['uf']);
                        ?>
                    </option>
                    <?php
                    foreach(fncibgelist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo utf8_encode($item['cidade'])." ".$item['uf']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <label for="motivo">MOTIVO:</label>
                <input autocomplete="off"  id="motivo" placeholder="motivo" type="text" step="0.01" class="form-control" name="motivo" value="<?php echo $viagem['motivo']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="passageiros">PASSAGEIROS:</label>
                <input autocomplete="off"  id="passageiros" placeholder="passageiros" type="text" step="0.01" class="form-control" name="passageiros" value="<?php echo $viagem['passageiros']; ?>"/>
            </div>

            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>