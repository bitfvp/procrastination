<?php
class Abastecimento{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncabastecimentonew($cod_motorista,$cod_carro,$data,$quantidade,$valor,$km){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_frota_abastecimento ";
                $sql.="(id, cod_motorista, cod_carro, data, quantidade, valor, km)";
                $sql.=" VALUES ";
                $sql.="(NULL, :cod_motorista, :cod_carro, :data, :quantidade, :valor, :km)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":cod_motorista", $cod_motorista);
                $insere->bindValue(":cod_carro", $cod_carro);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":quantidade", $quantidade);
                $insere->bindValue(":valor", $valor);
                $insere->bindValue(":km", $km);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vabastecimento_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncabastecimentoedit($id,$cod_motorista,$cod_carro,$data,$quantidade,$valor,$km){

        //inserção no banco
        try{
            $sql="UPDATE mcu_frota_abastecimento SET cod_motorista=:cod_motorista, cod_carro=:cod_carro, data=:data, quantidade=:quantidade, valor=:valor, km=:km WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":cod_motorista", $cod_motorista);
            $insere->bindValue(":cod_carro", $cod_carro);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":km", $km);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vabastecimento_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>