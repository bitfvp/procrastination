<?php
class Viagem{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncviagemnew($cod_motorista,$cod_carro,$data,$destino,$motivo,$passageiros){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_frota_viagem ";
                $sql.="(id, cod_motorista, cod_carro, data, destino, motivo, passageiros)";
                $sql.=" VALUES ";
                $sql.="(NULL, :cod_motorista, :cod_carro, :data, :destino, :motivo, :passageiros)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":cod_motorista", $cod_motorista);
                $insere->bindValue(":cod_carro", $cod_carro);
                $insere->bindValue(":data", $data);
                $insere->bindValue(":destino", $destino);
                $insere->bindValue(":motivo", $motivo);
                $insere->bindValue(":passageiros", $passageiros);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vviagem_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncviagemedit($id,$cod_motorista,$cod_carro,$data,$destino,$motivo,$passageiros){

        //inserção no banco
        try{
            $sql="UPDATE mcu_frota_viagem SET cod_motorista=:cod_motorista, cod_carro=:cod_carro, data=:data, destino=:destino, motivo=:motivo, passageiros=:passageiros WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":cod_motorista", $cod_motorista);
            $insere->bindValue(":cod_carro", $cod_carro);
            $insere->bindValue(":data", $data);
            $insere->bindValue(":destino", $destino);
            $insere->bindValue(":motivo", $motivo);
            $insere->bindValue(":passageiros", $passageiros);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vviagem_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>