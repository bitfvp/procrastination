<?php
class Carro{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccarronew( $carro,$placa, $renavam, $chassi){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_frota_carro ";
                $sql.="(id, carro, placa, renavam, chassi)";
                $sql.=" VALUES ";
                $sql.="(NULL, :carro, :placa, :renavam, :chassi)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":carro", $carro);
                $insere->bindValue(":placa", $placa);
                $insere->bindValue(":renavam", $renavam);
                $insere->bindValue(":chassi", $chassi);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcarro_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccarroedit( $id,$carro,$placa,$renavam,$chassi){

        //inserção no banco
        try{
            $sql="UPDATE mcu_frota_carro SET carro=:carro, placa=:placa, renavam=:renavam, chassi=:chassi WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":carro", $carro);
            $insere->bindValue(":placa", $placa);
            $insere->bindValue(":renavam", $renavam);
            $insere->bindValue(":chassi", $chassi);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcarro_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>