<?php
class Tecnico{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnctecniconew( $nome,$segunda1,$segunda2,$terca1,$terca2,$quarta1,$quarta2,$quinta1,$quinta2,$sexta1,$sexta2){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_frota_tecnico ";
                $sql.="(id, nome, segunda1, segunda2, terca1, terca2, quarta1, quarta2, quinta1, quinta2, sexta1, sexta2)";
                $sql.=" VALUES ";
                $sql.="(NULL, :nome, :segunda1, :segunda2, :terca1, :terca2, :quarta1, :quarta2, :quinta1, :quinta2, :sexta1, :sexta2)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":segunda1", $segunda1);
                $insere->bindValue(":segunda2", $segunda2);
                $insere->bindValue(":terca1", $terca1);
                $insere->bindValue(":terca2", $terca2);
                $insere->bindValue(":quarta1", $quarta1);
                $insere->bindValue(":quarta2", $quarta2);
                $insere->bindValue(":quinta1", $quinta1);
                $insere->bindValue(":quinta2", $quinta2);
                $insere->bindValue(":sexta1", $sexta1);
                $insere->bindValue(":sexta2", $sexta2);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vtecnico_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnctecnicoedit( $id,$nome,$segunda1,$segunda2,$terca1,$terca2,$quarta1,$quarta2,$quinta1,$quinta2,$sexta1,$sexta2){

        //inserção no banco
        try{
            $sql="UPDATE mcu_frota_tecnico SET nome=:nome, segunda1=:segunda1, segunda2=:segunda2, terca1=:terca1, terca2=:terca2, quarta1=:quarta1, quarta2=:quarta2, quinta1=:quinta1, quinta2=:quinta2, sexta1=:sexta1, sexta2=:sexta2 WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":nome", $nome);
            $insere->bindValue(":segunda1", $segunda1);
            $insere->bindValue(":segunda2", $segunda2);
            $insere->bindValue(":terca1", $terca1);
            $insere->bindValue(":terca2", $terca2);
            $insere->bindValue(":quarta1", $quarta1);
            $insere->bindValue(":quarta2", $quarta2);
            $insere->bindValue(":quinta1", $quinta1);
            $insere->bindValue(":quinta2", $quinta2);
            $insere->bindValue(":sexta1", $sexta1);
            $insere->bindValue(":sexta2", $sexta2);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vtecnico_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>