<?php
class Km{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnckmnew($motorista,$carro,$tipo,$periodo,$aux,$kilometro,$descricao){

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_frota_km ";
                $sql.="(id, cod_motorista, cod_carro, tipo, periodo, aux, kilometro, descricao)";
                $sql.=" VALUES ";
                $sql.="(NULL, :cod_motorista, :cod_carro, :tipo, :periodo, :aux, :kilometro, :descricao)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":cod_motorista", $motorista);
                $insere->bindValue(":cod_carro", $carro);
                $insere->bindValue(":tipo", $tipo);
                $insere->bindValue(":periodo", $periodo);
                $insere->bindValue(":aux", $aux);
                $insere->bindValue(":kilometro", $kilometro);
                $insere->bindValue(":descricao", $descricao);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
?>