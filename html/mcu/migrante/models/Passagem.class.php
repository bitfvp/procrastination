<?php
class Passagem{
    public function fncpassagemnew($at_migrante,$at_profissional,$at_cidade_anterior,$at_destino,$at_acolhimento_provisorio,$at_encaminhamento,$at_obs, $at_matriz){
        //
        try{
            $sql = "SELECT migrante_encaminhamento.id, migrante_encaminhamento.valor\n"
                . "FROM migrante_encaminhamento\n"
                . "WHERE (((migrante_encaminhamento.id)=?))";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam("1", $at_encaminhamento);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $encaminhamentoarray = $consulta->fetch();
            $at_valor_passagem=$encaminhamentoarray['valor'];
            $sql=null;
            $consulta=null;
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

            //inserção no banco
            try{
                $sql = "INSERT INTO `migrante_passagem` ";
                $sql .= "(`id`, `migrante`, `data`, `cidade_anterior`, `destino`, `acolhimento_provisorio`, `encaminhamento`, `valor_passagem`, `obs`, `profissional`, `imprimir`, `entregue`, `matriz`) ";
                $sql .= "VALUES ";
                $sql .= "(NULL, :migrante, CURRENT_TIMESTAMP, :cidade_anterior, :destino, :acolhimento_provisorio, :encaminhamento, :valor_passagem, :obs, :profissional, '0', '0', :matriz)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":migrante", $at_migrante);
                $insere->bindValue(":cidade_anterior", $at_cidade_anterior);
                $insere->bindValue(":destino", $at_destino);
                $insere->bindValue(":acolhimento_provisorio", $at_acolhimento_provisorio);
                $insere->bindValue(":encaminhamento", $at_encaminhamento);
                $insere->bindValue(":valor_passagem", $at_valor_passagem);
                $insere->bindValue(":obs", $at_obs);
                $insere->bindValue(":profissional", $at_profissional);
                $insere->bindValue(":matriz", $at_matriz);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($insere)){
            /////////////////////////////////////////////////////
            //criar log

            //soma cont_ranking pra pessoa
            try{
                $sql="UPDATE migrante SET cont_rank=cont_rank+1,ultima_passagem=now() WHERE id=:id";
                global $pdo;
                $atualizarankpessoa=$pdo->prepare($sql);
                $atualizarankpessoa->bindValue(":id", $at_migrante);
                $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro rank pessoa '. $error_msg->getMessage();
            }

            //contagem de passagens
            try{
                $sql="UPDATE migrante_encaminhamento SET quantidade=quantidade-1 WHERE id=:id";
                global $pdo;
                $atualizarankpessoa=$pdo->prepare($sql);
                $atualizarankpessoa->bindValue(":id", $at_encaminhamento);
                $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro quant passagem '. $error_msg->getMessage();
            }

            ////////////////////////////////////////////////////////////////////////////


            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
                header("Location: index.php?pg=Vpassagem&id={$at_migrante}");
                exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////


    public function fncpassagemdelete($profissional_id,$pessoa_id,$ticket_id,$encaminhamento_id){
        try {
            $sql = "DELETE FROM `migrante_passagem` WHERE id = :ticket_id and profissional = :profissional_id";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":ticket_id", $ticket_id);
            $exclui->bindValue(":profissional_id", $profissional_id);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        //contagem de passagens
        try{
            $sql="UPDATE migrante_encaminhamento SET quantidade=quantidade+1 WHERE id=:id";
            global $pdo;
            $atualizarankpessoa=$pdo->prepare($sql);
            $atualizarankpessoa->bindValue(":id", $encaminhamento_id);
            $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro quant passagem '. $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Passagem apagada com successo!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vpassagem&id={$_GET['id']}");
        exit();
    }//fim da fnc delete

}
?>