<?php
class Encaminhamento{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncencaminhamentonew($encaminhamento,$valor,$horario,$quantidade,$matriz){

            //inserção no banco
            try{
                $sql="INSERT INTO migrante_encaminhamento ";
                $sql.="(id, encaminhamento, valor, horario, quantidade, matriz)";
                $sql.=" VALUES ";
                $sql.="(NULL, :encaminhamento, :valor, :horario, :quantidade, :matriz)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":encaminhamento", $encaminhamento);
                $insere->bindValue(":valor", $valor);
                $insere->bindValue(":horario", $horario);
                $insere->bindValue(":quantidade", $quantidade);
                $insere->bindValue(":matriz", $matriz);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncencaminhamentoedit($id,$encaminhamento,$valor,$horario,$quantidade){

        //inserção no banco
        try{
            $sql="UPDATE migrante_encaminhamento SET encaminhamento=:encaminhamento, valor=:valor, horario=:horario, quantidade=:quantidade WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":encaminhamento", $encaminhamento);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":horario", $horario);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>