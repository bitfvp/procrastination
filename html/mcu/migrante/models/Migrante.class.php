<?php
class Migrante{
    public function fncmigranteedit($id,$id_prof,$nome,$nome_social,$sexo,$nascimento,$cpf,$rg,$uf_rg,$bo,$cart_trabalho,$cn,$c_casamento,$titulo_eleitor,$raca_cor,$mae,$pai,$alfabetizado,$estado_civil,$origem,$contato_familiar,$profissao,$obs,$alcool,$cigarro,$maconha,$crack,$deficiencia,$deficiencia_desc){
        //tratamento das variaveis
        if($alfabetizado==""){
            $alfabetizado="0";
        }
        if($nascimento==""){
            $nascimento="1900-01-01";
        }
        try{
            $sql="SELECT * FROM migrante WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        $consulta=null;
        if($contar!=0){
            //inserção no banco
            try{
                $sql="UPDATE migrante SET nome=:nome, nome_social=:nome_social, sexo=:sexo, cpf=:cpf, rg=:rg, uf_rg=:uf_rg, bo=:bo, cart_trabalho=:cart_trabalho, cn=:cn, c_casamento=:c_casamento, ";
                $sql.="titulo_eleitor=:titulo_eleitor, nascimento=:nascimento, raca_cor=:raca_cor, mae=:mae, pai=:pai, alfabetizado=:alfabetizado, estado_civil=:estado_civil, ";
                $sql.="origem=:origem, contato_familiar=:contato_familiar, profissao=:profissao, obs=:obs, alcool=:alcool, cigarro=:cigarro, maconha=:maconha, crack=:crack, ";
                $sql.="deficiencia=:deficiencia, deficiencia_desc=:deficiencia_desc  ";
                $sql.= "WHERE id=:id ";
                global $pdo;
                $atualiza=$pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":nome_social", $nome_social);
                $atualiza->bindValue(":sexo", $sexo);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":uf_rg", $uf_rg);
                $atualiza->bindValue(":bo", $bo);
                $atualiza->bindValue(":cart_trabalho", $cart_trabalho);
                $atualiza->bindValue(":cn", $cn);
                $atualiza->bindValue(":c_casamento", $c_casamento);
                $atualiza->bindValue(":titulo_eleitor", $titulo_eleitor);
                $atualiza->bindValue(":nascimento", $nascimento);
                $atualiza->bindValue(":raca_cor", $raca_cor);
                $atualiza->bindValue(":mae", $mae);
                $atualiza->bindValue(":pai", $pai);
                $atualiza->bindValue(":alfabetizado", $alfabetizado);
                $atualiza->bindValue(":estado_civil", $estado_civil);
                $atualiza->bindValue(":origem", $origem);
                $atualiza->bindValue(":contato_familiar", $contato_familiar);
                $atualiza->bindValue(":profissao", $profissao);
                $atualiza->bindValue(":obs", $obs);
                $atualiza->bindValue(":alcool", $alcool);
                $atualiza->bindValue(":cigarro", $cigarro);
                $atualiza->bindValue(":maconha", $maconha);
                $atualiza->bindValue(":crack", $crack);

                $atualiza->bindValue(":deficiencia", $deficiencia);
                $atualiza->bindValue(":deficiencia_desc", $deficiencia_desc);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erroooo'. $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];
        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log

            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}");
                exit();
        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }





















    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncmigrantenew($id_prof, $nome,$nome_social,$sexo,$nascimento,$cpf,$rg,$uf_rg,$bo,$cart_trabalho,$cn,$c_casamento,$titulo_eleitor,$raca_cor,$mae,$pai,$alfabetizado,$estado_civil,$origem,$contato_familiar,$profissao,$obs,$alcool,$cigarro,$maconha,$crack,$deficiencia,$deficiencia_desc){
        //tratamento das variaveis
        if($alfabetizado==""){
            $alfabetizado="0";
        }
        if($sexo==""){
            $sexo="0";
        }
        if($nascimento==""){
            $nascimento="1000-01-01";
        }

        try{
            $sql="SELECT * FROM ";
            $sql.="migrante ";
            $sql.="WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();

        if(($contarcpf==0)or ($cpf=="")){
            //inserção no banco
            try{
                $sql="INSERT INTO migrante ";
                $sql.="(id, data_cadastro, nome, nome_social, sexo, cpf, rg, uf_rg, bo, cart_trabalho, cn, c_casamento, titulo_eleitor, nascimento, raca_cor, mae, pai, alfabetizado, estado_civil, origem, contato_familiar, profissao, obs, alcool, cigarro, maconha, crack, deficiencia, deficiencia_desc)";
                $sql.=" VALUES ";
                $sql.="(NULL, CURRENT_TIMESTAMP, :nome, :nome_social, :sexo, :cpf, :rg, :uf_rg, :bo, :cart_trabalho, :cn, :c_casamento, :titulo_eleitor, :nascimento,  :raca_cor, :mae, :pai,  :alfabetizado, :estado_civil, :origem, :contato_familiar, :profissao, :obs, :alcool, :cigarro, :maconha, :crack, :deficiencia, :deficiencia_desc)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":nome_social", $nome_social);
                $insere->bindValue(":sexo", $sexo);
                $insere->bindValue(":cpf", $cpf);
                $insere->bindValue(":rg", $rg);
                $insere->bindValue(":uf_rg", $uf_rg);
                $insere->bindValue(":bo", $bo);
                $insere->bindValue(":cart_trabalho", $cart_trabalho);
                $insere->bindValue(":cn", $cn);
                $insere->bindValue(":c_casamento", $c_casamento);
                $insere->bindValue(":titulo_eleitor", $titulo_eleitor);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":raca_cor", $raca_cor);
                $insere->bindValue(":mae", $mae);
                $insere->bindValue(":pai", $pai);
                $insere->bindValue(":alfabetizado", $alfabetizado);
                $insere->bindValue(":estado_civil", $estado_civil);
                $insere->bindValue(":origem", $origem);
                $insere->bindValue(":contato_familiar", $contato_familiar);
                $insere->bindValue(":profissao", $profissao);
                $insere->bindValue(":obs", $obs);
                $insere->bindValue(":alcool", $alcool);
                $insere->bindValue(":cigarro", $cigarro);
                $insere->bindValue(":maconha", $maconha);
                $insere->bindValue(":crack", $crack);
                $insere->bindValue(":deficiencia", $deficiencia);
                $insere->bindValue(":deficiencia_desc", $deficiencia_desc);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!! confira com calma pois essa pessoa que você está cadastrando pode já existir",
                "type"=>"warning",
            ];
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM migrante";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;
                $maid=$mid[0];

                    header("Location: index.php?pg=Vpessoa&id={$maid}");
                    exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];


            }
        }

    }
}
?>