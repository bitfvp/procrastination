<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

ob_start();
session_start();

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/procrastination/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/procrastination/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

include_once("{$_ENV['ENV_ROOT']}includes/funcoes.php");

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}


if(isset($_REQUEST['term'])){
    // Prepare a select statement
    $sql = "SELECT * FROM tbl_ibge WHERE cidade LIKE '%$_REQUEST[term]%' limit 0,15";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $licidade = $consulta->fetchall();
    $contcidade = $consulta->rowCount();
    $sql=null;
    $consulta=null;

            if($contcidade > 0){
                // Fetch result rows as an associative array
                foreach ($licidade as $lc){
                    echo utf8_encode("<p>" . $lc["cidade"]."-" . $lc["uf"] ."</p>");
                }
            } else{
                echo "<p>Sem combinações encontradas</p>";
            }
    }
?>