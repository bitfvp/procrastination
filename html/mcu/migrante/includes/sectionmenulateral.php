<section class="sidebar-offcanvas" id="sidebar">
    <div class="list-group">
        <a href="index.php?pg=Vat&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-folder"></i> PASTA DO USUÁRIO</a>
        <a href="index.php?pg=Vpassagem&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-bus"></i> PASSAGEM</a>
        <a href="index.php?pg=Valb_freq&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fa fa-clipboard-list"></i> HISTÓRICO NO ALBERGUE</a>
        <a href="#" data-toggle="modal" data-target="#modalhorarios" class="list-group-item"><i class="fa fa-clock"></i> HORARIOS</a>
        <a href="https://www.google.com/maps/d/viewer?mid=1FVhubn7vhbt7DGIzKkX7dW5-Epg&ll=-20.268836697342863%2C-41.6521984307891&z=8" target="_blank" class="list-group-item"><i class="fa fa-map"></i>    MAPA</a>
    </div>
</section>

<script type="application/javascript">
    var offset = $('#sidebar').offset().top;
    var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
    $(document).on('scroll', function () {
        if (offset <= $(window).scrollTop()) {
            $meuMenu.addClass('fixarmenu');
        } else {
            $meuMenu.removeClass('fixarmenu');
        }
    });
</script>


<!--modalhorarios-->
<!-- Modal -->
<div class="modal bd-example-modal-lg" id="modalhorarios" tabindex="-1" role="dialog" aria-labelledby="modalhorariosLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mx-auto" id="modalhorariosLabel">Horários de ônibus</h5>
                <a href="index.php?pg=Vencaminhamentoeditar" class="btn btn-success">NOVO</a>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-sm table-borderless">

                            <?php
                            $sql = "SELECT * FROM migrante_encaminhamento where matriz=? order by encaminhamento";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$_SESSION['matriz']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $encaminhamentolista = $consulta->fetchAll();
                            $sql=null;
                            $consulta=null;
                            foreach ($encaminhamentolista as $lista){
                                if ($lista['quantidade'] < 5){
                                    $cor="danger";
                                }
                                if ($lista['quantidade'] >= 5) {
                                    $cor = "warning";
                                }
                                if ($lista['quantidade'] >= 10) {
                                    $cor = "success";
                                }
                                echo "<tr>";
                                    echo "<td>R$".number_format($lista['valor'], 2)."</td>";
                                    echo "<td><i class='fa fa-map-marker text-info'></i> ".$lista['encaminhamento']."</td>";
                                    echo "<td title='restam:' class='badge badge-".$cor."'>".$lista['quantidade']."</td>";
                                    echo "<td>".$lista['horario']."</td>";
                                    echo "<td><a href='index.php?pg=Vencaminhamentoeditar&id=".$lista['id']."' class='fa fa-pen'></a></td>";
                                echo "</tr>";
                            }
                            ?>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--fim de modalhorarios-->
