<?php
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetmigrante($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<div class="container-fluid">
    <h3>DADOS DO USUÁRIO</h3>
    <blockquote class="blockquote blockquote-info">
        <header>
            NOME:
            <strong class="text-info"><?php echo $pessoa['nome']; ?>&nbsp;&nbsp;</strong>
        </header>
        <h6>
        NOME SOCIAL:<strong class="text-info"><?php echo $pessoa['nome_social']; ?>&nbsp;&nbsp;</strong>
            SEXO:
            <?php
            if($pessoa['sexo']!="" and $pessoa['sexo']!="0") {
                echo "<span class='text-info'>";
                if($pessoa['sexo']==0){echo"Selecione...";}
                if($pessoa['sexo']==1){echo"Feminino";}
                if($pessoa['sexo']==2){echo"Masculino";}
                if($pessoa['sexo']==3){echo"Indefinido";}
                echo "</span>";
            }else{
                echo "<span class='text-muted'>";
                echo "[---]";
                echo "</span>";
            }
            ?>
        </strong>
            NASCIMENTO:
            <strong class="text-info"><?php
                if($pessoa['nascimento']!="1900-01-01" and $pessoa['nascimento']!="" and $pessoa['nascimento']!="1000-01-01") {
                    echo "<span class='text-info'>";
                    echo dataBanco2data ($pessoa['nascimento']);
                    echo " <i class='text-success'>".Calculo_Idade($pessoa['nascimento'])." anos</i>";
                    echo "</span>";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
            <hr>

            CPF:
            <strong class="text-info"><?php
                if($pessoa['cpf']!="") {
                    echo "<span class='text-info'>";
                    echo mask($pessoa['cpf'],'###.###.###-##');
                    echo "</span>";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?></strong>
            RG:
            <strong class="text-info"><?php
                if($pessoa['rg']!="") {
                    echo "<span class='text-info'>";
                    echo mask($pessoa['rg'],'###.###.###');
                    echo "</span>";
                }else{
                    echo "<span class='text-muted'>";
                    echo "[---]";
                    echo "</span>";
                }
                ?></strong>
            UF (RG):<strong class="text-info"><?php echo $pessoa['uf_rg']; ?>&nbsp;&nbsp;</strong>
            BO:
            <?php
            if($pessoa['bo']!="") {
                echo "<strong class='text-info'>";
                echo $pessoa['bo'];
            }else{
                echo "<strong class='text-muted'>";
                echo "[---]";} ?>
            </strong>
        
        <i title="CARTEIRA DE TRABALHO E PREVIDENCIA SOCIAL">CTPS</i>:
        <?php
        if($pessoa['cart_trabalho']!="") {
        echo "<strong class='text-info'>";
        echo $pessoa['cart_trabalho'];
        
        }else{
        echo "<strong class='text-muted'>";
        echo "[---]";
        
        }
        ?>
        </strong>
        
        <i title="CERTIDÃO DE NASCIMENTO">CN</i>:
        <?php
        if($pessoa['cn']!="") {
        echo "<strong class='text-info'>";
        echo $pessoa['cn'];
        
        }else{
        echo "<strong class='text-muted'>";
        echo "[---]";
        
        }
        ?>
        </strong>
        
        <i title="CERTIDÃO DE CASAMENTO">CC</i>:
        <?php
        if($pessoa['c_casamento']!="") {
        echo "<strong class='text-info'>";
        echo $pessoa['c_casamento'];
        
        }else{
        echo "<strong class='text-muted'>";
        echo "[---]";
        
        }
        ?>
        </strong>

        TITULO DE ELEITOR:
        <?php
        if($pessoa['titulo_eleitor']!="") {
        echo "<strong class='text-info'>";
        echo $pessoa['titulo_eleitor'];
        
        }else{
        echo "<strong class='text-muted'>";
        echo "[---]";
        
        }
        ?>
        </strong>
        
        RAÇA/COR:
        <strong class="text-info"><?php
        if(($pessoa['raca_cor']==0)or($pessoa['raca_cor']=="")){echo"Selecione...";}
        if($pessoa['raca_cor']==1){echo"Branca";}
        if($pessoa['raca_cor']==2){echo"Negra";}
        if($pessoa['raca_cor']==3){echo"Amarela";}
        if($pessoa['raca_cor']==4){echo"Parda";}
        if($pessoa['raca_cor']==5){echo"Indigena";} ?>&nbsp;&nbsp;
        </strong>

        PAI:<strong class="text-info"><?php echo $pessoa['pai']; ?>&nbsp;&nbsp;</strong>
            MÃE:<strong class="text-info"><?php echo $pessoa['mae']; ?>&nbsp;&nbsp;</strong>
            <hr>
        ALFABETIZADO:
        <strong class="text-info"><?php
        if($pessoa['alfabetizado']==0){echo"Não";}
        if($pessoa['alfabetizado']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

        ESTADO CIVIL:
        <strong class="text-info"><?php
        if($pessoa['estado_civil']==0){echo"Solteiro(a)";}
        if($pessoa['estado_civil']==1){echo"Casado(a)";}
        if($pessoa['estado_civil']==2){echo"Divorciado(a)";}
        if($pessoa['estado_civil']==3){echo"Uniao Estavel";}
        if($pessoa['estado_civil']==4){echo"Viuvo(a)";}
        ?>&nbsp;&nbsp;
        </strong>

        ORIGEM:<strong class="text-info"><?php echo $pessoa['origem']; ?>&nbsp;&nbsp;</strong>
        CONTATO FAMILIAR:<strong class="text-info"><?php echo $pessoa['contato_familiar']; ?>&nbsp;&nbsp;</strong>
        PROFISSÃO:<strong class="text-info"><?php echo $pessoa['profissao']; ?>&nbsp;&nbsp;</strong>
        OBS:<strong class="text-info"><?php echo $pessoa['obs']; ?>&nbsp;&nbsp;</strong>
        <hr>
        ALCOOL:
        <strong class="text-info"><?php
        if($pessoa['alcool']==0){echo"Não";}
        if($pessoa['alcool']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

        CIGARRO:
        <strong class="text-info"><?php
        if($pessoa['cigarro']==0){echo"Não";}
        if($pessoa['cigarro']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

        MACONHA:
        <strong class="text-info"><?php
        if($pessoa['maconha']==0){echo"Não";}
        if($pessoa['maconha']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

        CRACK:
        <strong class="text-info"><?php
        if($pessoa['crack']==0){echo"Não";}
        if($pessoa['crack']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

        <hr>
            POSSUI DEFICIÊNCIA:
            <strong class="text-info"><?php
                if($pessoa['deficiencia']==0){echo"Não";}
                if($pessoa['deficiencia']==1){echo"Sim";} ?>&nbsp;&nbsp;</strong>

            DESCRIÇÃO DA DEFICIÊNCIA:
            <strong class="text-info"><?php
                if($pessoa['deficiencia_desc']!="") {
                    echo "<span class='text-info'>";
                    echo $pessoa['deficiencia_desc'];
                    echo "</span>";
                }else{
                }
                ?></strong>
        </h6>
        <footer class="blockquote-footer">
            Mantenha atualizado</strong>&nbsp;&nbsp;
        </footer>
    </blockquote>
</div>

<?php
if ($allow["admin"]==1) {?>
<div class="row">
    <div class="form-group col-md-6">
        <h4>Juntar</h4>
        <hr>
        <form action="index.php?pg=Vpessoa&id=<?php echo $_GET['id']; ?>&aca=juntamigrante" method="post">
            <input id="outro" type="number" required class="form-control input-sm" name="outro" value="0"/>
            <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
        </form>
    </div>
</div>
    <?php
}
?>

<a  class="btn btn-success btn-block" href="?pg=Vpessoaeditar&id=<?php echo $_GET['id']; ?>" title="Edite os dados dessa pessoa">
    EDITAR PESSOA
</a>