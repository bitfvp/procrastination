<main class="container">
    <h2>Busca por relato</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-12 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vbusca_relatos" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por relato..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <br>
    <table class="table table-stripe table-hover table-condensed table-striped">
        <tfoot>
        <tr>
            <th scope="row" colspan="1">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc - 1;
                        $proximo = $pc + 1;
                        if ($pc > 1) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca_relatos&sca={$_GET['sca']}&pgn={$anterior}'><span aria-hidden=\"true\">&laquo; Anterior</a></li> ";
                        }
                        if ($pc < $tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vbusca_relatos&sca={$_GET['sca']}&pgn={$proximo}'>Próximo &#187;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="1" class="text-right text-info"><?php echo $todos->rowCount(); ?> Registro(s) listado(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <?php
        $sta = strtoupper($_GET['sca']);
        define('CSA', $sta);//TESTE
        while ($dados = $limite->fetch()){
        $id = $dados["id"];
        $prof = $dados["profissional"];
        $data = datahoraBanco2data($dados["data"]);
        $descricao = strtoupper($dados["descricao"]);
        ?>
        <tr>
            <td scope="row" id="<?php echo $id; ?>" colspan="2">
                <blockquote class="blockquote blockquote-info">
                    <h5>
                            <i class="fa fa-quote-left fa-sm "></i>
                            <strong class="text-success">
                                <?php
                                if ($_GET['sca'] != "") {
                                    $sta = CSA;
                                    $nnn = $descricao;
                                    $nn = explode(CSA, $nnn);
                                    $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                                    echo $n;
                                } else {
                                    echo $descricao;
                                }
                                ?>
                            </strong>
                            <i class="fa fa-quote-right fa-sm"></i>
                        <br>
                        <strong class="text-info" ><?php echo $data; ?>&nbsp;&nbsp;</strong>

                    <span class="badge badge-pill badge-info float-right"><strong><?php echo $id; ?></strong></span>
                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($prof);
                        echo $us['nome'];
                        echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                        ?>
                    </footer>
                    </h5>
                </blockquote>

            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</main>