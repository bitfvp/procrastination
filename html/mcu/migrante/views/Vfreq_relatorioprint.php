<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_63"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
        }
    }
}

$page="Relatorio de pernoites-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$iniciala=$_POST['data_inicial'];
$inicial=$iniciala." 00:00:00";
$finala=$_POST['data_final'];
$final=$finala." 23:59:59";
$user=$_SESSION['id'];

$sql = "SELECT * \n"
    . "FROM migrante_alb_freq \n"
    . "WHERE ((data>=:inicial) And (data<=:final) and matriz=1)\n"
    . "ORDER BY pessoa, data";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

?>
	<main class="container-fluid">
        <h3>Relatório de pernoite e jartar</h3>
        <?php
        $x=0;
        echo '<h5>Período de '.dataBanco2data($iniciala).' à '.dataBanco2data($finala).'</h5>';
        ?>
        <table class="table table-striped table-hover table-sm">
         <thead>
            <tr>
                <td>PESSOA&nbsp;</td>
                <td>SEXO&nbsp;</td>
                <td>PROFISSIONAL&nbsp;</td>
                <td>DATA&nbsp;</td>
                <td>TIPO&nbsp;</td>
            </tr>
         </thead>
        <?php
        $pernoite=0;
        $pernoite_feminino=0;
        $pernoite_masculino=0;
        $pernoite_indefinido=0;
        $jantar=0;
        $jantar_feminino=0;
        $jantar_masculino=0;
        $jantar_indefinido=0;

        foreach($ati as $at){
            $mig=fncgetmigrante($at['pessoa']);
            if($at['tipo']==1){
                $pernoite++;
                $tipo="Pernoite";
                switch ($mig['sexo']){
                    case 1:
                        $sexo="Feminino <i class='fa fa-venus'></i>";
                        $pernoite_feminino++;
                        break;
                    case 2:
                        $sexo="Masculino <i class='fa fa-mars'></i>";
                        $pernoite_masculino++;
                        break;
                    default:
                        $sexo="Indefinido <i class='fa fa-venus-mars'></i>";
                        $pernoite_indefinido++;
                        break;
                }
            }
            if($at['tipo']==2){
                $jantar++;
                $tipo="Jantar";
                switch ($mig['sexo']){
                    case 1:
                        $sexo="Feminino <i class='fa fa-venus'></i>";
                        $jantar_feminino++;
                        break;
                    case 2:
                        $sexo="Masculino <i class='fa fa-mars'></i>";
                        $jantar_masculino++;
                        break;
                    default:
                        $sexo="Indefinido <i class='fa fa-venus-mars'></i>";
                        $jantar_indefinido++;
                        break;
                }
            }
            ?>
            <tbody>
            <tr>
                <td><?php echo $mig['nome'];?>&nbsp;</td>
                <td><?php echo $sexo;?>&nbsp;</td>
                <td><?php echo fncgetusuario($at['profissional'])['nome'];?>&nbsp;</td>
                <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
                <td><?php echo $tipo; ?>&nbsp;</td>
            </tr>
            </tbody>
            <?php
        }

        echo "<tfoot>";
        echo "<tr>";
        echo "<td>Pernoite: ".$pernoite."</td>";
        echo "<td>Feminino: ".$pernoite_feminino."</td>";
        echo "<td>Masculino: ".$pernoite_masculino."</td>";
        echo "<td colspan='2'>Indefinido: ".$pernoite_indefinido."</td>";
        echo "</tr>";

        echo "<tr>";
        echo "<td>Jantar: ".$jantar."</td>";
        echo "<td>Feminino: ".$jantar_feminino."</td>";
        echo "<td>Masculino: ".$jantar_masculino."</td>";
        echo "<td colspan='2'>Indefinido: ".$jantar_indefinido."</td>";
        echo "</tr>";

        echo "</tfoot>";

        echo '</table>';
        ?>
        </main>
    </body>
</html>