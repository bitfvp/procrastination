<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '1200;URL={$env->env_url_mod}'>";
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-5">

            <div class="card">
                <div class="card-header bg-danger text-light">
                    10 Usuários mais ativos
                </div>
                <div class="card-body">
                    <table class="table table-hover table-sm">
                        <tbody>
                        <?php
                        $ccont=0;
                        foreach (fnctopmigrante() as $rk){
                            $ccont+=1;

                            if($ccont==1){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/gold.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont==2){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/silver.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont==3){
                                echo "<tr>";
                                echo "<td>";
                                echo "<img src='{$env->env_estatico}img/bronze.png' alt=''>";
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                            if($ccont>3){
                                echo "<tr>";
                                echo "<td  class='text-center'>";
                                echo $ccont;
                                echo "</td>";
                                echo "<td><a href='index.php?pg=Vpessoa&id={$rk['id']}' title='Ver pessoa'>{$rk['nome']}</a></td>";
                                echo "<td>{$rk['cont_rank']}</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

            </div>

            </div> <!-- /..col-md-4 -->
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>

    </div> <!-- /.row -->

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>