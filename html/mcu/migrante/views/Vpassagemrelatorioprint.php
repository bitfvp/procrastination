<?php
ini_set('display_errors',1);
            ini_set('display_startup_erros',1);
            error_reporting(E_ALL);
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_62"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Relatorio de Passagens-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$iniciala=$_POST['data_inicial'];
$inicial=$iniciala." 00:00:00";
$finala=$_POST['data_final'];
$final=$finala." 23:59:59";
$ordem=$_POST['ordem'];
    $sql = "SELECT * FROM migrante_passagem "
        ."WHERE (((migrante_passagem.data)>=:inicial) And ((migrante_passagem.data)<=:final) AND (entregue=1) AND migrante_passagem.matriz=:matriz) ORDER BY {$ordem}";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->bindValue(":matriz",$_SESSION['matriz']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
///////////////////////////////////////////////////////////

?>  

<main class="container">
    <h3>Relatório de passagens</h3>
        <h4>Período de <?php echo dataBanco2data($iniciala);?> à <?php echo dataBanco2data($finala); ?></h4>

        <table class="table table-hover table-sm table-striped">
            <tr>
                <td>DATA</td>
                <td>PESSOA</td>
                <td>SEXO</td>
                <td>IDADE</td>
                <td>ENCAMINHAMENTO</td>
                <td>VALOR</td>
            </tr>
            <?php
            $acont=0;
            $acontvalor=0;
            $masculino=0;
            $feminino=0;
            $indefinido=0;
            foreach($ati as $at){
                $pes=fncgetmigrante($at['migrante']);
                switch ($pes['sexo']){
                    case 1:
                        $sexo="Feminino";
                        $feminino++;
                        break;
                    case 2:
                        $sexo="Masculino";
                        $masculino++;
                        break;
                    default:
                        $sexo="Indefinido";
                        $indefinido++;
                        break;
                }
                ?>
                <tr>
                    <td><?php echo dataRetiraHora($at['data']); ?></td>
                    <td><?php echo $pes['nome'];?></td>
                    <td>
                        <?php
                        switch ($pes['sexo']){
                            case 1:
                                echo '<i class="fa fa-venus"></i>';
                                break;
                            case 2:
                                echo '<i class="fa fa-mars"></i>';
                                break;
                            default:
                                echo '<i class="fa fa-angry fa-2x"></i>';
                                break;
                        }
                        ?>
                    </td>
                    <td><?php
                        $idade=Calculo_Idade($pes['nascimento']);
                        if ($idade>110){
                            echo "<i class='fa fa-dizzy fa-2x'></i>";
                        }else{
                            echo $idade." anos";
                        }
                         ?></td>
                    <td><?php echo fncgetmigr_encam($at['encaminhamento'])['encaminhamento']; ?></td>
                    <td><?php echo "R$ ".number_format($at['valor_passagem'], 2); $acont+=1; $acontvalor+=$at['valor_passagem']; ?></td>
                </tr>
                <?php } ?>
        </table>
    <h4><?php echo $acont; ?> passagens, valor total: R$ <?php echo number_format($acontvalor, 2); ?></h4>
    <h4><?php echo $masculino; ?> do sexo masculino, <?php echo $feminino; ?> do sexo feminino e <?php echo $indefinido; ?> indefinidos</h4>
    <?php $acont=0; ?>

            <fieldset>
                <div id="piechart" style="width: auto; height: 900px;"></div>
            </fieldset>
</main>
</body>
</html>
<?php
$sqlgrf = "SELECT migrante_encaminhamento.encaminhamento,  COUNT(migrante_encaminhamento.encaminhamento) as contadora\n"
    . "FROM migrante_encaminhamento INNER JOIN migrante_passagem ON migrante_encaminhamento.id = migrante_passagem.encaminhamento\n"
    . "WHERE (\n"
    . "((migrante_passagem.data)>:inicial) And ((migrante_passagem.data)<=:final) AND (entregue=1) And migrante_passagem.matriz=:matriz\n"
    . " )\n"
    . "GROUP BY migrante_encaminhamento.encaminhamento ";
global $pdo;
$consulta = $pdo->prepare($sqlgrf);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->bindValue(":matriz",$_SESSION['matriz']);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$graf = $consulta->fetchAll();
$sql=null;
$consulta=null;
?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- //////////////////////////////////////////////////////////////////:: -->
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
      //montando o array com os dados
      var data = google.visualization.arrayToDataTable([
          ['Lugar', 'Quant'],
          <?php
          foreach ($graf as $gf) {
            echo '[\''.$gf['contadora'].' '.$gf['encaminhamento'].'\',  '.$gf['contadora'].'],'."\n";
        }
        ?>
        ]);
       //opções para o gráfico pizza
               var options3 = {
                  title: 'Grafico torta de passagens por período',
                   is3D: true,
               };
        //instanciando e desenhando para o gráfico pizza
                var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                chart.draw(data, options3); 
    }
</script>