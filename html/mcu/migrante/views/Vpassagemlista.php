<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_61"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Impressão de Passagens-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

<?php

try{
    $sql = "SELECT migrante_passagem.id, migrante.nome, tbl_users.nome AS profissional, migrante_encaminhamento.encaminhamento, migrante_passagem.data, migrante_passagem.valor_passagem, migrante_passagem.imprimir, migrante_passagem.entregue\n"
        . "FROM tbl_users INNER JOIN (migrante_encaminhamento INNER JOIN (migrante INNER JOIN migrante_passagem ON migrante.id = migrante_passagem.migrante) ON migrante_encaminhamento.id = migrante_passagem.encaminhamento) ON tbl_users.id = migrante_passagem.profissional\n"
        . "WHERE (((migrante_passagem.entregue)=0) and (migrante_passagem.matriz=?))\n"
        . "ORDER BY migrante_passagem.data DESC";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->bindParam(1, $_SESSION['matriz']);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$dados=$todos->fetchAll();
$tr=$todos->rowCount();// verifica o número total de registros
?>
<!--        //começalista-->

<a href="index.php?pg=Vpassagemcomprovante" class="btn btn-lg btn-success btn-block" target="_blank">
    IMPRIMIR COMPROVANTE
</a>
    <table class="table table-stripe table-hover table-condensed table-striped">
        <thead>
        <tr>
            <th scope="col">NOME</th>
            <th scope="col">ENCAMINHAMENTO</th>
            <th scope="col">VALOR</th>
            <th scope="col">DATA</th>
            <th scope="col">PROFISSIONAL</th>
            <th scope="col">IMPRIMIR</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="3">
            </th>
            <td colspan="3" class="text-warning text-right"><?php echo $tr;?> Passagem(s) encontrada(s)</td>
        </tr>
        </tfoot>
        <?php
        foreach ($dados as $pa){
            ?>
        <tbody>
        <tr>
            <th scope="row" id="<?php echo $id;  ?>">
                <?php echo $pa['nome']; ?>
                <a href="<?php echo "index.php?pg=Vpassagemlista&id={$pa['id']}&aca=entregapassagem"; ?>" class="btn btn-sm btn-info float-right">FINALIZAR</a>
            </th>
            <td><?php echo $pa['encaminhamento']; ?></td>
            <td><?php echo "R$".number_format($pa['valor_passagem'], 2); ?></td>
            <td><?php echo datahoraBanco2data($pa['data']); ?></td>
            <td><?php $prof=explode(" ", $pa['profissional']); echo $prof[0]; ?></td>
            <td class="">
                <?php
                if($pa['imprimir']==0){
                    echo "<a href='index.php?pg=Vpassagemlista&id={$pa['id']}&aca=imprimirpassagemsim' title='Click para alterar para sim'><span class='fa fa-thumbs-down text-danger' aria-hidden='true'></span>";
                    echo"Não";
                    echo "</a>";
                }
                if($pa['imprimir']==1){
                    echo "<a href='index.php?pg=Vpassagemlista&id={$pa['id']}&aca=imprimirpassagemnao' title='Click para alterar para não'><span class='fa fa-thumbs-up text-success' aria-hidden='true'></span>";
                    echo"Sim";
                    echo "</a>";
                }
                ?>
            </td>
        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>