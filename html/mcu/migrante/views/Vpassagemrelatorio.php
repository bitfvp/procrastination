<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_62"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Relatorio de Passagens-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- =============================começa conteudo======================================= -->
            <div class="card">
                <div class="card-header bg-info text-light">
                    Relatório de passagens
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vpassagemrelatorioprint" method="post" target="_blank">
                        <input type="submit" class="btn btn-lg btn-success btn-block mb-2" value="Gerar relatório"/>
                        <div class="form-group">
                            <label for="data_inicial">Data inicial:</label>
                            <input id="data_inicial" type="date" class="form-control" name="data_inicial" required/>
                        </div>
                        <div class="form-group">
                            <label for="data_final">Data final:</label>
                            <input id="data_final" type="date" class="form-control" name="data_final" required/>
                        </div>
                        <div class="form-group">
                            <label for="ordem">Ordem:</label>
                            <select name="ordem" id="ordem" class="form-control">
                                // vamos criar a visualização
                                <option selected="" value="data">Data</option>
                                <option value="migrante">Migrante</option>
                                <option value="encaminhamento">Encaminhamento</option>
                            </select>
                        </div>

                    </form>
                </div>
            </div>
<!-- =============================fim conteudo======================================= -->       
    </div>  
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>