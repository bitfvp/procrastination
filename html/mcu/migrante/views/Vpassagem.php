<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"] != "1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
} else {
    if ($_SESSION["matriz"] != 1) {
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    } else {
//        validação das permissoes
        if ($allow["allow_57"] != 1) {
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        } else {
            if ($allow["allow_60"] != 1) {
                header("Location: {$env->env_url_mod}");
                exit();
            } else {
                //ira abrir
            }
        }
    }
}


$page = "Histórico de passagens-" . $env->env_titulo;
$css = "style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#serch1 input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings(".result");
            if (inputVal.length) {
                $.get("includes/ibge_cidade.php", {term: inputVal}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdown.html(data);
                });
            } else {
                resultDropdown.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".result p", function () {
            $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
            $(this).parent(".result").empty();
        });
    });
</script>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <?php include_once("includes/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= --><?php
            if ($allow["allow_61"] == 1) {?>
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Lançamento de passagens
                    </div>
                    <div class="card-body">
                        <form action="index.php?pg=Vpassagem&aca=newpassagem&id=<?php echo $_GET['id']; ?>" method="post">
                            <div class="row">
                                <div class="form-group col-md-6" id="serch1">
                                    <label for="cidade_anterior">Cidade anterior:</label>
                                    <input id="cidade_anterior" autocomplete="off" type="text" class="form-control" name="cidade_anterior" value=""/>
                                    <div class="result"></div>
                                </div>

                                <div class="form-group col-md-6" id="serch1">
                                    <label for="destino">Destino:</label>
                                    <input id="destino" autocomplete="off" type="text" class="form-control" name="destino" value=""/>
                                    <div class="result"></div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="acolhimento_provisorio">Albergue:</label>
                                    <select name="acolhimento_provisorio" id="acolhimento_provisorio" class="form-control" required>
                                        <option selected="" value="">Selecione...</option>
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="encaminhamento">Encaminhamento:</label>
                                    <select name="encaminhamento" id="encaminhamento" class="form-control" required>
                                        // vamos criar a visualização
                                        <option selected="" value="">Selecione...</option>
                                        <?php
                                        foreach (fncmigr_encam_lista() as $ativ) {?>
                                            <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['encaminhamento']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="obs">Obs:</label>
                                    <textarea id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="5" name="obs"></textarea>
                                    <span id="cont">250</span>/250
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR NOVA PASSAGEM"/>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <?php
            }
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * \n"
                        . "FROM migrante_passagem \n"
                        . "WHERE (((migrante_passagem.migrante)=?))\n"
                        . "ORDER BY migrante_passagem.data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $hist = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>

            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <a href="#" onclick="alert('funcao nao disponivel ainda')" target="_blank">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão do prontuário</span>
                    </a>
                    <h6><?php
                        $total = 0;
                        foreach ($hist as $at) {
                            if ($at['matriz']==$_SESSION['matriz']){
                                $cor="info";
                            }else{
                                $cor="danger";
                            }
                            $matriz=fncgetmatriz($at['matriz']);
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                Em:<strong class="text-warning"><?php echo $matriz['cidade']; ?>&nbsp;&nbsp;</strong class="text-info">
                                <br>
                                Encaminhamento:<strong class="text-warning"><?php echo fncgetmigr_encam($at['encaminhamento'])['encaminhamento']; ?>&nbsp;&nbsp;</strong class="text-info">
                                <br>
                                Data:<strong class="text-success"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong class="text-info">

                                Valor:
                                <strong class="text-success">
                                    <?php echo " R$" . number_format($at['valor_passagem'], 2); ?>&nbsp&nbsp
                                </strong>
                                <br>
                                Cidade anterior:
                                <strong class="text-success"><?php echo $at['cidade_anterior']; ?>
                                    &nbsp;&nbsp;</strong class="text-info">
                                <br>
                                Destino:
                                <strong class="text-success"><?php echo $at['destino']; ?>
                                    &nbsp;&nbsp;</strong class="text-info">
                                <br>
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i>
                                    <strong class="text-success"><?php echo $at['obs']; ?></strong class="text-info">
                                    <i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                                <?php
                                if ($at['profissional'] == $_SESSION['id'] and $at['entregue']!='1') {
                                    ?>
                                    <div class="dropdown show">
                                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apagar
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vpassagem&id={$_GET['id']}&aca=excluirpassagem&ticket_id={$at['id']}&encaminhamento_id={$at['encaminhamento']}"; ?>">Apagar</a>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </blockquote>
                            <?php
                            $total += $at['valor_passagem'];
                        }

                        ?>
                    </h6>
                </div>
                <footer class="card-footer text-center text-warning">
                    <?php echo "Valor total: R$" . number_format($total, 2); ?>
                </footer>
            </div>

            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>