<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_61"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Impressão de Comprovante-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");
// Recebe

$sql = "SELECT migrante_passagem.id, migrante_passagem.imprimir, migrante_passagem.entregue, migrante.nome, migrante_encaminhamento.encaminhamento, migrante_passagem.valor_passagem\n"
    . "FROM migrante_encaminhamento INNER JOIN (migrante INNER JOIN migrante_passagem ON migrante.id = migrante_passagem.migrante) ON migrante_encaminhamento.id = migrante_passagem.encaminhamento\n"
    . "WHERE (((migrante_passagem.imprimir)=1) AND ((migrante_passagem.entregue)=0) AND ((migrante_passagem.matriz)=?))";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $_SESSION['matriz']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $passagens = $consulta->fetchAll();
    $contpassagens= $consulta->rowCount();
    $sql=null;
    $consulta=null;
?>
<br><br><br>
<main class="container">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
                <h3><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h3>
            <H6><strong>Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</strong></H6>
            <h5><strong>SECRETARIA MUNICIPAL DO TRABALHO E DESENVOLVIMENTO SOCIAL</strong></h5>
            <h6><strong>A Viação Rio Doce, solicito a(s) passagem(s) listadas abaixo, e autorizamos o fornecimento ao(s) beneficiário(s).</strong></h6>
        </div>
    </div>
    <div class="row">
        <div class="col-12 px-5">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th class="text-center">QUANTIDADE</th>
                        <th>BENEFICIÁRIO</th>
                        <th>ENCAMINHAMENTO</th>
                        <th>VALOR DO TICKET</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total=0;
                    if ($contpassagens!=0){
                        foreach ($passagens as $ps){
                            echo "<tr>";
                            echo "<th class='text-center'>1</th>";
                            echo "<th>{$ps['nome']}</th>";
                            echo "<th>{$ps['encaminhamento']}</th>";
                            echo "<th>";
                            echo "R$ ".number_format($ps['valor_passagem'], 2);
                            echo "</th>";
                            echo "</tr>";
                            $total+=$ps['valor_passagem'];
                        }

                    }else{
                        echo "<tr>";
                        echo "<td><br></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td><br></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "</tr>";
                        $total=0;
                    }

                    ?>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="text-right pr-3">Manhuaçu, <?php
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo strftime('%A, %d de %B de %Y', strtotime('today'));
                        ?>
                    </th>
                    <th>Total R$ <?php
                        if ($contpassagens!=0){
                            echo number_format($total, 2);
                        }
                        ?>
                    </th>
                </tr>
                </tfoot>
        </table>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h2 class="text-center">RESPONSÁVEL:__________________</h2>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>
<br><br><br><br>
<br><br><br><br>
<main class="container">
    <div class="row">
        <div class="col-2 pr-0">
            <img src="<?php echo $env->env_estatico; ?>img/mcu.jpg" class="img-thumbnail border-0"/>
        </div>
        <div class="col-10 text-center pl-0 pr-5 pt-4">
            <h3><strong>PREFEITURA MUNICIPAL DE MANHUAÇU</strong></h3>
            <H6><strong>Praça Cordovil Pinto Coelho, 460 - Manhuaçu - Minas Gerais</strong></H6>
            <h5><strong>SECRETARIA MUNICIPAL DO TRABALHO E DESENVOLVIMENTO SOCIAL</strong></h5>
            <h6><strong>A Viação Rio Doce, solicito a(s) passagem(s) listadas abaixo, e autorizamos o fornecimento ao(s) beneficiário(s).</strong></h6>
        </div>
    </div>
    <div class="row">
        <div class="col-12 px-5">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th class="text-center">QUANTIDADE</th>
                    <th>BENEFICIÁRIO</th>
                    <th>ENCAMINHAMENTO</th>
                    <th>VALOR DO TICKET</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total=0;
                if ($contpassagens!=0){
                    foreach ($passagens as $ps){
                        echo "<tr>";
                        echo "<th class='text-center'>1</th>";
                        echo "<th>{$ps['nome']}</th>";
                        echo "<th>{$ps['encaminhamento']}</th>";
                        echo "<th>";
                        echo "R$ ".number_format($ps['valor_passagem'], 2);
                        echo "</th>";
                        echo "</tr>";
                        $total+=$ps['valor_passagem'];
                    }

                }else{
                    echo "<tr>";
                    echo "<td><br></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td><br></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "</tr>";
                    $total=0;
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="text-right pr-3">Manhuaçu, <?php
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo strftime('%A, %d de %B de %Y', strtotime('today'));
                        ?>
                    </th>
                    <th>Total R$ <?php
                        if ($contpassagens!=0){
                            echo number_format($total, 2);
                        }
                        ?>
                    </th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h2 class="text-center">RESPONSÁVEL:__________________</h2>
            <h6 class="float-right"> FlavioW<i class="fa fa-cogs"></i>rks</h6>
        </div>
    </div>
</main>
</body>
</html>