<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_58"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}


$page="Pasta do usuário-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-3">
            <?php include_once("includes/pessoacabecalhoside.php"); ?>
        </div>

        <div class="col-md-6">
            <!-- =============================começa conteudo======================================= -->

            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamento de atividades
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vat&aca=newat&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="data">Data:</label>
                                <input id="data" type="date" class="form-control input-sm" name="data"
                                       value="<?php echo date("Y-m-d"); ?>"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="restricao">Restrição:</label><select name="restricao" id="restricao" class="form-control input-sm">
                                    // vamos criar a visualização
                                    <option selected="" value="0">Aberto</option>
                                    <option value="1">Confidencial</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="atividade">Atividade:</label>
                                <select name="atividade" id="atividade" class="form-control input-sm">
                                    // vamos criar a visualização
                                    <option selected="" value="0">Selecione...</option>
                                    <?php
                                    foreach (fncmi_atlista() as $ativ) {?>
                                        <option value="<?php echo $ativ['id']; ?>"><?php echo $ativ['atividade']; ?></option>
                                    <?php
                                    } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="descricao">Descrição:</label>
                                <textarea id="descricao" onkeyup="limite_textarea(this.value,2000,descricao,'cont')" maxlength="2000" class="form-control" rows="7" name="descricao"></textarea>
                                <span id="cont">2000</span>/2000
                            </div>
                            <div class="form-group col-md-12">
                                <div class="custom-file">
                                    <input id="arquivo" type="file" class="custom-file-input" name="arquivo[]" value="" multiple/>
                                    <label class="custom-file-label" for="arquivo">Escolha o arquivo...</label>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <?php
            // Recebe
            if (isset($_GET['id'])) {
                $at_idpessoa = $_GET['id'];
                //existe um id e se ele é numérico
                if (!empty($at_idpessoa) && is_numeric($at_idpessoa)) {
                    // Captura os dados do cliente solicitado
                    $sql = "SELECT * \n"
                        . "FROM migrante_at \n"
                        . "WHERE (pessoa=?)\n"
                        . "ORDER BY data DESC";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1, $at_idpessoa);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $hist = $consulta->fetchAll();
                    $sql = null;
                    $consulta = null;
                }
            }
            ?>

            <div class="card mt-2">
                <div id="pointofview" class="card-header bg-info text-light">
                    Histórico do usuário
                </div>
                <div class="card-body">
                    <a href="#" onclick="alert('Funcao ainda não disponvel')">
                        <span class="fa fa-print text-warning" aria-hidden="true"> Impressão do prontuário</span>
                    </a>
                    <h6>
                        <?php
                        foreach ($hist as $at) {
                            if ($at['matriz']==$_SESSION['matriz']){
                                $cor="info";
                            }else{
                                $cor="danger";
                            }
                            ?>
                            <hr>
                            <blockquote class="blockquote blockquote-<?php echo $cor; ?>">
                                <p>
                                    <i class="fa fa-quote-left fa-sm "></i><?php
                                    if($at['restricao']=="1"){
                                        if($at['profissional']==$_SESSION['id']){
                                            echo "<i class='text-danger fa fa-user-secret' title='Confidenciais e outros usuários não poderão visualizar'></i> ".$at['descricao'];
                                        }else{
                                            echo "<i class='text-danger fa fa-user-secret'></i> Confidencial - contate o profissional responsável por essa atividade...";
                                        }
                                    }else{
                                        echo "<strong class='text-success'>{$at['descricao']}</strong>";
                                    }
                                    ?><i class="fa fa-quote-right fa-sm"></i>
                                </p>
                                <strong class="text-info"><?php echo dataRetiraHora($at['data']); ?>&nbsp;&nbsp;</strong>
                                Atividade:<strong class="text-info"><?php echo fncgetmi_at($at['atividade'])['atividade']; ?>&nbsp&nbsp</strong>
                                <br>
                                <span class="badge badge-pill badge-warning float-right"><strong><?php echo $at['id']; ?></strong></span>
                                <footer class="blockquote-footer">
                                    <?php
                                    $us=fncgetusuario($at['profissional']);
                                    echo $us['nome'];
                                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                                    ?>
                                </footer>
                                <footer class="blockquote-footer">
                                    <?php
                                    $matriz=fncgetmatriz($at['matriz']);
                                    echo $matriz['cidade'];
                                    ?>
                                </footer>
                                <?php
                                if (($at['profissional'] == $_SESSION['id']) and (Expiradatahora($at['data'], 1)==1)) {?>
                                    <div class="dropdown show">
                                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Apagar
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">Não</a>
                                            <a class="dropdown-item bg-danger" href="<?php echo "?pg=Vat&id={$_GET['id']}&aca=excluirat&id_at={$at['id']}"; ?>">Apagar</a>
                                        </div>
                                    </div>
                                    <h6>*</h6>
                                    <?php
                                }
                                ?>
                            </blockquote>
                            <?php
                            if ($at['id'] != 0) {

                                $files = glob("../../dados/migrante/atividades/" . $at['id'] . "/*.*");
                                for ($i = 0; $i < count($files); $i++) {
                                    $num = $files[$i];
                                    $extencao = explode(".", $num);
                                    //ultima posicao do array
                                    $ultimo = end($extencao);
                                    switch ($ultimo) {
                                        case "docx":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "doc":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xls":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";

                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xlsx":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "pdf":
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        default:
                                            echo "<div class='row'>";
                                            echo "<div class='col-md-10'>";
                                            echo "<a href=" . $num . " target='_blank' >";
                                            echo "<img src=" . $num . " alt='...' class='img-thumbnail img-fluid'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;
                                    }
                                }
                            }
                            echo "<hr>";
                        }
                        ?>
                    </h6>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>