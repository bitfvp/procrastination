<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_63"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatos relatório-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<style media=all>
    hr{
        margin-top:0.5rem;
        margin-bottom:0.5rem;
        border:0;
        border-top:1px solid rgba(0,0,0,.1)
    }
</style>
<main class="container-fluid">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";
    $sql = "SELECT * "
        . "FROM migrante_alb_relato "
        . "WHERE ((data>=:inicial)and(data<:final) and matriz=1) "
        . "ORDER BY "
        . "`data` ASC ";

    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ativi = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h1>Relatório de atividades no albergue</h1>

            <?php
            $total = 0;
            foreach ($ativi as $at) {
                ?>
                <h6><i class="fa fa-quote-left fa-sm "></i><?php echo $at['descricao']; ?><i class="fa fa-quote-right fa-sm"></i></h6>
                <h6>
                    <?php
                    $us=fncgetusuario($at['profissional']);
                    echo $us['nome'];
                    echo " (".fncgetprofissao($us['profissao'])['profissao'].")";
                    ?>&nbsp;
                    <?php echo datahoraBanco2data($at['data']); ?>&nbsp;
                </h6>
                <hr>

                <?php
                $total++;
            }
            ?>

        <h4>Total de atividades: <?php echo $total; ?></h4>

        <fieldset>
            <div id="piechart" style="width: auto; height: 900px;"></div>
        </fieldset>
    </div>


</main>
</body>
</html>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['prof', 'Quant'],
            <?php
            $inicial = $_POST['data_inicial']." 00:00:01";
            $final = $_POST['data_final']." 23:59:59";
            $sql = "SELECT "
                . "Count(migrante_alb_relato.id) AS contadora, "
                . "tbl_users.nome AS profissional "
                . "FROM "
                . "migrante_alb_relato "
                . "INNER JOIN tbl_users ON tbl_users.id = migrante_alb_relato.profissional "
                . "WHERE "
                . "migrante_alb_relato.`data` >= :inicial AND "
                . "migrante_alb_relato.`data` < :final "
                . " and migrante_alb_relato.matriz=1 "
                . "GROUP BY\n "
                . "tbl_users.nome "
                . "ORDER BY "
                . "contadora DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":inicial", $inicial);
            $consulta->bindValue(":final", $final);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($graf as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['profissional'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);



        //opções para o gráfico pizza
        var options3 = {
            title: 'Grafico torta de atividade por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>