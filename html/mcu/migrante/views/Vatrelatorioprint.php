<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            if ($allow["allow_59"]!=1){
                header("Location: {$env->env_url_mod}");
                exit();
            }else{
                //ira abrir
            }
        }
    }
}

$page="Relatorio de Atividades-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe
$iniciala=$_POST['data_inicial'];
$inicial=$iniciala." 00:00:00";
$finala=$_POST['data_final'];
$final=$finala." 23:59:59";
$user=$_SESSION['id'];

$sql = "SELECT * \n"
    . "FROM migrante_at \n"
    . "WHERE (((migrante_at.data)>=:inicial And (migrante_at.data)<=:final) And migrante_at.matriz=:matriz)\n"
    . "ORDER BY migrante_at.profissional, migrante_at.atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
$consulta->bindValue(":matriz",$_SESSION['matriz']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ati = $consulta->fetchAll();
    $sql=null;
    $consulta=null;

?>
	<main class="container">
        <h2>Relatório de atividades</h2>
        <?php
        $x=0;
        echo '<h4>Período de '.dataBanco2data($iniciala).' à '.dataBanco2data($finala).'</h4>';
        ?>
        <table class="table table-striped table-hover table-sm">
            <tr>
                <td>PESSOA</td>
                <td>SEXO</td>
                <td>PROFISSIONAL</td>
                <td>DATA</td>
                <td>ATIVIDADE</td>
            </tr>
        <?php
        $acont=0;
        $a=$ati[0]['atividade'];
        foreach($ati as $at){
            $pes=fncgetmigrante($at['pessoa']);
            if($a!=$at['atividade']){
                $a=$at['atividade'];
                echo "<tr>";
                echo "<td colspan='5'>Total:".$acont."&nbsp;</td>";
                echo "</tr>";
                $acont=0;
            }

            ?>
            <tr>
                <td><?php echo $pes['nome'];?>&nbsp;</td>
                <td>
                    <?php
                    switch ($pes['sexo']){
                        case 1:
                            echo '<i class="fa fa-venus"></i>';
                            break;
                        case 2:
                            echo '<i class="fa fa-mars"></i>';
                            break;
                        default:
                            echo '<i class="fa fa-venus-mars"></i>';
                            break;
                    }
                    ?>
                </td>
                <td><?php echo fncgetusuario($at['profissional'])['nome'];?>&nbsp;</td>
                <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
                <td><?php echo fncgetmi_at($at['atividade'])['atividade']; ?>&nbsp;</td>
            </tr>
            <?php
            $acont+=1;
        }
        echo "<tr>";
        echo "<td colspan='5'>Total:".$acont."&nbsp;</td>";
        echo "</tr>";
        echo '</table>';
        ?>
        </main>
    </body>
</html>