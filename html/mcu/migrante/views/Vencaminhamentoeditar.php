<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_57"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Encaminhamento-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="encaminhamentosave";
    $encaminhamento=fncgetmigr_encam($_GET['id']);
}else{
    $a="encaminhamentonew";
}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vhome&aca={$a}"; ?>" method="post">
        <h3 class="form-cadastro-heading">Cadastro de encaminhamento</h3>
        <hr>
        <div class="row">
            <div class="col-md-5">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $encaminhamento['id']; ?>"/>
                <label for="encaminhamento">ENCAMINHAMENTO:</label>
                <input autocomplete="off" id="encaminhamento" placeholder="encaminhamento" type="text" class="form-control" name="encaminhamento" value="<?php echo $encaminhamento['encaminhamento']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="valor">VALOR R$:</label>
                <input  id="valor" placeholder="valor" type="number" class="form-control" name="valor" min='0' step="0.01" value="<?php echo $encaminhamento['valor']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="horario">HORÁRIOS:</label>
                <input autocomplete="off" id="horario" placeholder="horarios" type="text" class="form-control" name="horario" value="<?php echo $encaminhamento['horario']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="quantidade">QUANTIDADE:</label>
                <input id="valor" placeholder="valor" type="number" class="form-control" name="quantidade" min='0' step="1" value="<?php echo $encaminhamento['quantidade']; ?>"/>
            </div>
            <div class="col-md-12">
                <input type="submit" value="SALVAR" class="btn btn-success my-2" />
            </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>