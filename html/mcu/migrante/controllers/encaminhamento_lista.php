<?php
function fncmigr_encam_lista(){
    $sql = "SELECT * FROM migrante_encaminhamento where matriz=? ORDER BY encaminhamento";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$_SESSION['matriz']);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $encam_lista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $encam_lista;
}

function fncgetmigr_encam($id){
    $sql = "SELECT * FROM migrante_encaminhamento WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $get_encam = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $get_encam;
}
?>