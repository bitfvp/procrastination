<?php
function fncmi_atlista(){
    $sql = "SELECT * FROM migrante_atlista ORDER BY atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $atlista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $atlista;
}

function fncgetmi_at($id){
    $sql = "SELECT * FROM migrante_atlista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getat = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getat;
}