<?php
//metodo de acao exclusao de atividade
if ($startactiona == 1 && $aca == "excluirmigrante") {
    $profissional = $_SESSION["id"];
    $pessoa = $_GET["id"];


    if ((isset($profissional) and is_numeric($profissional) and $profissional==1) and (isset($pessoa) and is_numeric($pessoa))) {

        //passagem
        try {
            $sql = "DELETE FROM `migrante_passagem` WHERE migrante = :migrante ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":migrante", $pessoa);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        //at
        try {
            $sql = "DELETE FROM `migrante_at` WHERE pessoa = :migrante ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":migrante", $pessoa);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        //frequencia
        try {
            $sql = "DELETE FROM `migrante_alb_freq` WHERE pessoa = :migrante ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":migrante", $pessoa);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        //migrante
        try {
            $sql = "DELETE FROM `migrante` WHERE id = :migrante ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":migrante", $pessoa);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Exclusão de atividade realizado com sucesso!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vbusca");
        exit();
    }
}