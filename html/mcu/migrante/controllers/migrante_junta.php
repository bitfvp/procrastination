<?php
//metodo de acao exclusao de atividade
if ($startactiona == 1 && $aca == "juntamigrante") {

    $profissional = $_SESSION["id"];
    $pessoa = $_GET["id"];
    $outro = $_POST["outro"];


    if ((isset($profissional) and is_numeric($profissional) and $profissional==1) and (isset($pessoa) and is_numeric($pessoa)) and (isset($outro) and is_numeric($outro) and $outro!=0)) {

        //passagem
        try {
            $sql = "UPDATE migrante_passagem SET migrante=:pessoa  WHERE migrante = :outro ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":pessoa", $pessoa);
            $exclui->bindValue(":outro", $outro);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        //at
        try {
            $sql = "UPDATE migrante_at SET pessoa=:pessoa WHERE pessoa = :outro ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":pessoa", $pessoa);
            $exclui->bindValue(":outro", $outro);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        //frequencia
        try {
            $sql = "UPDATE `migrante_alb_freq` SET pessoa=:pessoa WHERE pessoa = :outro ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":pessoa", $pessoa);
            $exclui->bindValue(":outro", $outro);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }


        //migrante
        try {
            $sql = "DELETE FROM `migrante` WHERE id = :migrante ";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":migrante", $outro);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Junção de atividade realizado com sucesso!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vbusca");
        exit();
    }
}