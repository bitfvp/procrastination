<?php 
//metodo de selecionar
if($startactiona==1 && $aca=="imprimirpassagemsim"){
    if(isset($_GET["id"])){
        $id=$_GET["id"];
        try{
            $sql="UPDATE migrante_passagem ";
            $sql.="SET imprimir='1'";
            $sql.=" WHERE id=? ";

            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
    }
    $_SESSION['fsh']=[
        "flash"=>"Alterado!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vpassagemlista");
    exit();
}
//metodo de selecionar
if($startactiona==1 && $aca=="imprimirpassagemnao"){
    if(isset($_GET["id"])){
        $id=$_GET["id"];
        try{
            $sql="UPDATE migrante_passagem ";
            $sql.="SET imprimir='0'";
            $sql.=" WHERE id=? ";

            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
    }
    $_SESSION['fsh']=[
        "flash"=>"Alterado!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vpassagemlista");
    exit();
}
//metodo de selecionar
if($startactiona==1 && $aca=="entregapassagem"){
    if(isset($_GET["id"])){
        //dados
        $id=$_GET["id"];

        try{
            $sql="UPDATE migrante_passagem ";
            $sql.="SET entregue='1'";
            $sql.=" WHERE id=? ";

            global $pdo;
            $atualiza=$pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
    }
    $_SESSION['fsh']=[
        "flash"=>"Passagem entregue ao beneficiario!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vpassagemlista");
    exit();
}
?>