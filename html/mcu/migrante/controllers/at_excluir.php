<?php
//metodo de acao exclusao de atividade
if ($startactiona == 1 && $aca == "excluirat") {
    $profissional = $_SESSION["id"];
    $pessoa = $_GET["id"];
    $at_id = $_GET["id_at"];

    if ((isset($profissional) and is_numeric($profissional)) and (isset($at_id) and is_numeric($at_id))) {
        try {
            $sql = "DELETE FROM `migrante_at` WHERE id = :at_id and profissional = :profissional";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":at_id", $at_id);
            $exclui->bindValue(":profissional", $profissional);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Exclusão de atividade realizado com sucesso!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vat&id={$pessoa}");
        exit();
    }
}