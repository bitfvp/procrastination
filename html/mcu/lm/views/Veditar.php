<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

        <form class="form-signin" action="index.php?pg=Vhome&aca=novolm" method="post">
            <div class="row">
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success btn-block" value="SALVAR"/>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-7">
                    <label for="conta">Conta:</label>
                    <select name="conta" id="conta" class="form-control">
                        // vamos criar a visualização
                        <option selected="" value="0"  class="text-info">escolha...</option>
                        <option value="1" class="text-danger">primaria</option>
                        <option value="2" class="text-danger">segunda</option>
                        <option value="3" class="text-danger">terceira</option>
                        <option value="4" class="text-danger">quarta</option>
                        <option value="5" class="text-danger">quinta</option>
                        <option value="6" class="text-danger">sexta</option>
                        <option value="7" class="text-danger">setima</option>
                        <option value="8" class="text-danger">oitava</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="comida">comida:(use virgula)</label>
                    <input autocomplete="off"  id="comida" placeholder="comida" type="number" step="0.1" class="form-control" name="comida" value=""/>
                </div>
                <div class="col-md-6">
                    <label for="pedra">pedra:</label>
                    <input autocomplete="off"  id="pedra" placeholder="pedra" type="number" step="0.1" class="form-control" name="pedra" value=""/>
                </div>
                <div class="col-md-6">
                    <label for="madeira">madeira:</label>
                    <input autocomplete="off"  id="madeira" placeholder="madeira" type="number" step="0.1" class="form-control" name="madeira" value=""/>
                </div>
                <div class="col-md-6">
                    <label for="minerio">minerio:</label>
                    <input autocomplete="off"  id="minerio" placeholder="minerio" type="number" step="0.1" class="form-control" name="minerio" value=""/>
                </div>
                <div class="col-md-6">
                    <label for="ouro">ouro:</label>
                    <input autocomplete="off"  id="ouro" placeholder="ouro" type="number" step="0.1" class="form-control" name="ouro" value=""/>
                </div>
                <div class="col-md-6">
                    <label for="poder">poder:</label>
                    <input autocomplete="off"  id="poder" placeholder="poder" type="number" step="0.1" class="form-control" name="poder" value=""/>
                </div>
            </div>
        </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>