<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["admin"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '60;URL={$env->env_url_mod}'>";
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-3">
            <a href="index.php?pg=Veditar" class="btn btn-lg btn-success btn-block">
                NOVO
            </a>
        </div>
    </div>

    <fieldset>
        <div id="curve_chart1" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart2" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart3" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart4" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart5" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart6" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart7" style="width: auto; height: 450px;"></div>
    </fieldset>
    <fieldset>
        <div id="curve_chart8" style="width: auto; height: 450px;"></div>
    </fieldset>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 1 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'primeira',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart1'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 2 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'segunda',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart2'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 3 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'terceira',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart3'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 4 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'quarta',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart4'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 5 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'quinta',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart5'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 6 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'sexta',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart6'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 7 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'setima',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart7'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['PERIODO', 'COMIDA', 'PEDRA', 'MADEIRA', 'MINERIO', 'OURO', 'PODER'],
            <?php
            // Recebe
            $sql = "SELECT * FROM lm WHERE lm.conta = 8 ORDER BY lm.`data` DESC LIMIT 0, 10";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $graf = $consulta->fetchAll();
            $sql = null;
            $consulta = null;

            foreach ($graf as $gf) {
                $dd=dataRetiraHora($gf['data']);
                $dia=explode('/', $dd);
                echo "['".$dia[0]."', ".$gf['comida'].", ".$gf['pedra'].", ".$gf['madeira'].", ".$gf['minerio'].", ".$gf['ouro'].", ".$gf['poder']."]," . "\n";
            }
            ?>
        ]);

        var options = {
            title: 'oitava',
            curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart8'));

        chart.draw(data, options);
    }
</script>

