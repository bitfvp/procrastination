<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_74"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Editar Oficios-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
        <form class="form-signin" action="index.php?pg=Vhome&aca=salvaroficionovo" method="post">
            <input type="submit" class="btn btn-success btn-block" value="Salvar"/><br>
            <fieldset>
                <label  for="numero_oficio">Numero do oficio:</label><input autocomplete="off" id="numero_oficio" autofocus type="text" class="form-control" name="numero_oficio" placeholder="Digite o numero do oficio..." />
                <label  for="origem">Origem:</label><input autocomplete="off" id="origem" type="text" class="form-control" name="origem" placeholder="Digite a origem do oficio..." />
                <label  for="encaminhamento">Encaminhamento:</label><input autocomplete="off" id="encaminhamento" type="text" class="form-control" name="encaminhamento" placeholder="Digite o encaminhamento do oficio..." />
                <label for="obs">Observação:</label><textarea rows="8" id="obs" onkeyup="limite_textarea(this.value)" maxlength="10000" class="form-control" name="obs" ></textarea>

        </form>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>