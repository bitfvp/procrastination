<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_74"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Meus Oficios-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

<div class="row">
    <div class="col-md-12">
<!-- =============================começa conteudo======================================= -->
	<?php
    // Recebe
        $id_user =$_SESSION['id'];
        //existe um id e se ele é numérico
        if (!empty($id_user) && is_numeric($id_user)) {
            // Captura os dados do cliente solicitado
$sql = "SELECT mcu_oficio.id, mcu_oficio.data, mcu_oficio.numero_oficio, mcu_oficio.origem, mcu_oficio.encaminhamento, mcu_oficio.obs, mcu_oficio.profissional AS id_prof, tbl_users.nome AS profissional\n"
    . "FROM tbl_users INNER JOIN mcu_oficio ON tbl_users.id = mcu_oficio.profissional\n"
    . "WHERE (((mcu_oficio.profissional)=?))\n"
    . "ORDER BY mcu_oficio.data DESC LIMIT 0,100";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $id_user);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mat = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
        }
    ?>
<div class="panel panel-info">
<div class="panel-heading">
<h3 class="panel-title">Meu Hitorico de Oficios</h3>
</div>
<div class="panel-body">
<p>
<h5>
<?php
            foreach($mat as $at){
                ?>
                <hr>


                    <blockquote>
                        &nbsp;Numero Oficio:&nbsp;<strong class="text-info"><?php echo $at['numero_oficio']; ?></strong>
                        &nbsp;Origem:&nbsp;<strong class="text-info"><?php echo $at['origem']; ?></strong>
                        &nbsp;Encaminhamento:&nbsp;<strong class="text-info"><?php echo $at['encaminhamento']; ?></strong>
                        &nbsp;Obs:&nbsp;<strong class="text-info"><?php echo $at['obs']; ?></strong>
                        &nbsp;Data:&nbsp;<strong class="text-info"><?php echo datahoraBanco2data($at['data']); ?>&nbsp;&nbsp;</strong>
                    </blockquote>
                <?php } ?>
</h5>
</p>
</div>
</div>
            
<!-- =============================fim conteudo======================================= -->       
    </div>  
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
</html>