<?php
include_once("includes/header.php");
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_74"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }else{
            ///broco chave
            foreach($_REQUEST as $___opt => $___val){
                $$___opt = $___val;
            }
            if(empty($pg)){
                include_once("views/Vhome.php");
            }else{
                $file="views/".$pg.".php";
                if(file_exists($file)){
                    include_once($file);
                }else{
                    include_once('views/404.php');
                }
            }
            //fim do bloco chave
        }
    }
}

?>