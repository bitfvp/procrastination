<?php
//starts
ob_start();
session_start();
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();


include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();

require "{$env->env_root}vendor/autoload.php";
use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
//echo $os->getName();
$device = new Device();
$language = new Language();

include_once("{$env->env_root}models/LogQuery.class.php");//conecção com o db
$LQ = new LogQuery();

//get das actions
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

include_once ("{$env->env_root}includes/funcoes.php");//funcoes
////troca tema
include_once("{$env->env_root}controllers/trocatema.php");
////logout
include_once("{$env->env_root}controllers/logout.php");

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

//valida se há manutenção
include_once("{$env->env_root}controllers/validaManutencao.php");

//chat msg
include_once("{$env->env_root}models/mcu/Chat_Msg.class.php");
include_once("{$env->env_root}controllers/mcu/chat_msg.php");

/* inicio do Bloco dedidado*/
if (isset($_SESSION['logado']) and $_SESSION['logado']=="1"){

    include_once("models/Salvaroficio.class.php");
//visita
    include_once("controllers/oficionovo.php");

}
/* fim do bloco dedicado*/


//metodo de checar usuario
include_once("{$env->env_root}controllers/confirmUser.php");