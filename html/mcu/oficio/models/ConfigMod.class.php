<?php //globais
include_once("../../../models/Env.class.php");

Class ConfigMod extends Env {
    public $env_root_mod;
    public $env_url_mod;
    public $env_mod_nome;
    public $env_titulo;

    function __construct(){
        parent::__construct();
        $this->env_root_mod= $this->env_root.$this->env_htmlpasta."mcu/oficio/";
        $this->env_url_mod= $this->env_url."mcu/oficio/";
        $this->env_mod_nome="oficio ";
        $this->env_titulo=$this->env_nome . $this->env_mod_nome;
    }


}