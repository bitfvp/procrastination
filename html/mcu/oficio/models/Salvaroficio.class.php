<?php
class SalvarOficio
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsalvaroficionovo($id_prof, $numero_oficio, $origem, $encaminhamento, $obs)
    {
        //tratamento das variaveis

        //inserção no banco
        try {
            $sql = "INSERT INTO oficio ";
            $sql .= "(id, data, numero_oficio, origem, encaminhamento, obs, profissional)";
            $sql .= " VALUES ";
            $sql .= "(NULL, CURRENT_TIMESTAMP, :numero_oficio, :origem, :encaminhamento, :obs, :profissional)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":numero_oficio", $numero_oficio);
            $insere->bindValue(":origem", $origem);
            $insere->bindValue(":encaminhamento", $encaminhamento);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":profissional", $id_prof);
            $insere->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        if (isset($insere)) {
            /////////////////////////////////////////////////////
            $_SESSION['fsh'] = [
                "flash" => "Cadastro realizado com sucesso!!",
                "type" => "success",
            ];

        } else {
            if (empty($flash)) {
                if (empty($_SESSION['fsh'])) {
                    $_SESSION['fsh'] = [
                        "flash" => "Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type" => "danger",
                    ];

                }
            }
        }
    }
}