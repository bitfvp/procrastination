<?php
class Contato{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccontatonew( $nome,$telefone){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_agenda ";
                $sql.="(id, nome, telefone, cadastro)";
                $sql.=" VALUES ";
                $sql.="(NULL, :nome, :telefone, :cadastro)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":telefone", $telefone);
                $insere->bindValue(":cadastro", $_SESSION["id"]);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccontatoedit( $id,$nome,$telefone){
        //tratamento das variaveis

        //inserção no banco
        try{
            $sql="UPDATE mcu_agenda SET nome=:nome, telefone=:telefone, cadastro=:cadastro WHERE mcu_agenda.id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":nome", $nome);
            $insere->bindValue(":telefone", $telefone);
            $insere->bindValue(":cadastro", $_SESSION["id"]);
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }
}
?>