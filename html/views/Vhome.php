<?php
$page="".$env->env_titulo;
$css="home";
include_once("{$env->env_root}includes/head.php");
?>
    <main>
        <header class="container-fluid">
            <div class="row">
                <div class="col-md-12 title">
                    <img class="homeimg" src="<?php echo $env->env_estatico; ?>img/syssocial1.png" alt="">
                    <h2 class="heading">Informação Gerencial</h2>
                    <p>Projetos modulares para deixar os processos mais simples </p>
                    <a href="<?php echo $env->env_url."index.php?pg=Vlogin"; ?>" class="butgo">Entrar!</a>
                </div>
            </div>
        </header>

        <footer class="container-fluid">
            <p>
                <i class="fa fa-paint-brush"></i>
                <strong class="red">+</strong>
                <i class="fa fa-code"></i>
                <strong class="red">+</strong>
                <i class="fa fa-cloud"></i>
                <a href="https://www.facebook.com/flavioworks" class="muted" target="_blank" title="Construimos, testamos e implementamos em dias, não meses.">
                    FlavioW<i class="fa fa-cogs"></i>rks
                </a>
                <?php echo date("Y"); ?>
            </p>
        </footer>

    </main>
</html>