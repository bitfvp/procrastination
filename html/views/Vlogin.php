<?php
$page="Login-".$env->env_titulo;
$css="login_register";
if(isset($_SESSION['logado'])){
    switch ($_SESSION['matriz']){
        case 1:
            header("Location: {$env->env_url}mcu");
            exit();
            break;
        case 2:
            header("Location: {$env->env_url}caf");
            exit();
            break;
        case 3:
            header("Location: {$env->env_url}mpref");
            exit();
            break;
        case 4:
            header("Location: {$env->env_url}smem");
            exit();
            break;
        default:
            echo"<META HTTP-EQUIV=REFRESH CONTENT = '10;URL={$env->env_url}'>";
            $_SESSION['fsh']=[
                "flash"=>"Você não está lotado em lugar nenhum! Favor entrar em contato com o administrador",
                "type"=>"danger",
            ];
            killSession();
            break;

    }
}
include_once("{$env->env_root}includes/head.php");
?>

<div class="container-fluid">
    <main class="container  font3">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card text-center">
                    <div class="card-header border-0">
                        <a href="index.php"><img class="img-responsive img-fluid" src="<?php echo $env->env_estatico; ?>img/syssocial1.png" alt="" title="<?php echo $env->env_nome; ?>"/></a>
                    </div>
                    <div class="card-body">
                        <form action="index.php?pg=Vlogin&aca=logar" method="post">
                            <input autofocus type="text" name="sysemail" id="sysemail"  placeholder="Usuário" minlength="5" maxlength="60" autocomplete="on" class="form-control form-control-lg mb-1" required>
                            <input type="password" name="syssenha" id="syssenha"  placeholder="Entre com sua senha" minlength="5" maxlength="30" autocomplete="off" class="form-control form-control-lg " required>
                            <button type="submit" class="btn btn-block btn-lg button-anon-pen mt-2"> <span>ENTRAR</span> </button>
                        </form>
                    </div>
                    <div class="card-footer text-muted bg-white border-0">
                        <small class="">
                            <div id="counter-area"><span id="counter" title="acessos"></span></div>
                            <!--chama msg recebida-->
                            <script type="text/javascript">
                                $.ajaxSetup({cache: false});
                                $(document).ready(function () {
                                    $('#counter').load('includes/login_acessos.php');
                                });
                                $(document).ready(function () {
                                    setInterval(function () {
                                        $('#counter').load('includes/login_acessos.php')
                                    }, 30000);
                                });
                            </script>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div> <!-- /container -->

<div id='stars'></div>
<div id='stars2'></div>
<!--<div id='stars3'></div>-->

<?php include_once("{$env->env_root}includes/footer.php"); ?>

</body>
</html>