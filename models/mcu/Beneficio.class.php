<?php
class Beneficio{
    public function fncbenew($cb_pessoa,$cb_beneficio,$cb_condicao,$cb_quantidade,$cb_data_pedido,$cb_profissional,$cb_descricao,$cb_entregue,$cb_data_entrega,$cb_quem_recebeu){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_beneficio ";
                $sql.="(id, pessoa, beneficio, condicao, quantidade, data_pedido, profissional, descricao, entregue, data_entrega, quem_recebeu)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :beneficio, :condicao, :quantidade, :data_pedido, :profissional, :descricao, :entregue, :data_entrega, :quem_recebeu)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $cb_pessoa);
                $insere->bindValue(":beneficio", $cb_beneficio);
                $insere->bindValue(":condicao", $cb_condicao);
                $insere->bindValue(":quantidade", $cb_quantidade);
                $insere->bindValue(":data_pedido", $cb_data_pedido);
                $insere->bindValue(":profissional", $cb_profissional);
                $insere->bindValue(":descricao", $cb_descricao);
                $insere->bindValue(":entregue", $cb_entregue);
                $insere->bindValue(":data_entrega", $cb_data_entrega);
                $insere->bindValue(":quem_recebeu", $cb_quem_recebeu);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($insere)){
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($cb_pessoa,$_SESSION['id'],"Novo beneficio",3,1);
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Benefício cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
                ];
                header("Location: ?pg=Vbe&id={$cb_pessoa}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim de fnc create

    public function fncbedelete($cb_id,$prof_id,$pessoa_id){
        try {
            $sql = "DELETE FROM `mcu_beneficio` WHERE id = :cb_id and profissional = :prof_id and entregue <> 1";
            global $pdo;
            $exclui = $pdo->prepare($sql);
            $exclui->bindValue(":cb_id", $cb_id);
            $exclui->bindValue(":prof_id", $prof_id);
            $exclui->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro:' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Benefício excluido com sucesso",
            "type"=>"success",
        ];
        //reservado para log
        global $LL; $LL->fnclog($pessoa_id,$_SESSION['id'],"Apagar beneficio",3,4);
        ////////////////////////////////////////////////////////////////////////////
        header("Location: ?pg=Vbe&id={$_GET['id']}");
        exit();

    }//fim da fnc delete


//funcão de editar a entrega
    public function fncbeedit($cb_id,$cb_entregue,$cb_data_entrega,$cb_quem_recebeu){

        //inserção no banco
        try{
            $sql="UPDATE mcu_beneficio SET ";
            $sql.="entregue=:entregue, data_entrega=:data_entrega, quem_recebeu=:quem_recebeu";
            $sql.=" WHERE id=:id";

            global $pdo;
            $atuali=$pdo->prepare($sql);
            $atuali->bindValue(":entregue", $cb_entregue);
            $atuali->bindValue(":data_entrega", $cb_data_entrega);
            $atuali->bindValue(":quem_recebeu", $cb_quem_recebeu);
            $atuali->bindValue(":id",$cb_id);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }


        if(isset($atuali)){
            /////////////////////////////////////////////////////
            //reservado para log
            //buscar cod da pessoa conforme o codigo
            $sql = "SELECT pessoa FROM mcu_beneficio WHERE id=? ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $cb_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $cbbb = $consulta->fetch();
            $sql=null;
            $consulta=null;

            global $LL; $LL->fnclog($cbbb['0'],$_SESSION['id'],"Edicao de entrega de beneficio",4,3);
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Cesta básica atualizada com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}//fim de classe