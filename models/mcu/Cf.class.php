<?php

class Cf
{
    public function fncnovcf($id_pessoa, $token)
    {

        //inserção no banco
        try {
            $sql = "UPDATE mcu_pessoas SET ";
            $sql .= "cod_familiar=:cod_familiar";
            $sql .= " WHERE id=:id";

            global $pdo;
            $atuali = $pdo->prepare($sql);
            $atuali->bindValue(":cod_familiar", $token);
            $atuali->bindValue(":id", $id_pessoa);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($atuali)) {
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id_pessoa}");
            exit();

        } else {
            if (empty($_SESSION['fsh'])) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function addmembro($id_pessoa,$cf,$id_membro)
    {

        //inserção no banco
        try {
            $sql = "UPDATE mcu_pessoas SET ";
            $sql .= "cod_familiar=:cod_familiar";
            $sql .= " WHERE id=:id";

            global $pdo;
            $atuali = $pdo->prepare($sql);
            $atuali->bindValue(":cod_familiar", $cf);
            $atuali->bindValue(":id", $id_membro);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($atuali)) {
            //////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Adicionada a familia!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id_pessoa}");
            exit();

        } else {
            if (empty($_SESSION['fsh'])) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncexccf($id_pessoa,$id_membro)
    {

        //inserção no banco
        try {
            $sql = "UPDATE mcu_pessoas SET ";
            $sql .= "cod_familiar=null";
            $sql .= " WHERE id=:id";

            global $pdo;
            $atuali = $pdo->prepare($sql);
            $atuali->bindValue(":id", $id_membro);
            $atuali->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if (isset($atuali)) {
            //////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Membro Removido!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id_pessoa}");
            exit();

        } else {
            if (empty($_SESSION['fsh'])) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}

?>