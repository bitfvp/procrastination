<?php
class Log{
    public function fnclog($pessoa,$profissional,$descricao,$atividade,$atividade_tipo){
        //criar log
            $descricaolog="apuracao de entrega de CB da pessoa x";
            $codlog="4";
            try{
                $sql="INSERT INTO mcu_log ";
                $sql.="(id, pessoa, profissional,  data, descricao, atividade, atividade_tipo )";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :profissional, CURRENT_TIMESTAMP, :descricao, :atividade, :atividade_tipo )";
                global $pdo;
                $inserelog=$pdo->prepare($sql);
                $inserelog->bindValue(":pessoa", $pessoa);
                $inserelog->bindValue(":profissional", $profissional);
                $inserelog->bindValue(":descricao", $descricao);
                $inserelog->bindValue(":atividade", $atividade);
                $inserelog->bindValue(":atividade_tipo", $atividade_tipo);
                $inserelog->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro log '. $error_msg->getMessage();
            }

            //soma cont_ranking pra pessoa
            try{
                $sql="UPDATE mcu_pessoas SET cont_rank=cont_rank+1 WHERE mcu_pessoas.id=:id";
                global $pdo;
                $atualizarankpessoa=$pdo->prepare($sql);
                $atualizarankpessoa->bindValue(":id", $pessoa);
                $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro rank pessoa '. $error_msg->getMessage();
            }

            //soma cont_ranking pro profissional
            try{
                $sql="UPDATE tbl_users SET cont_rank=cont_rank+1 WHERE tbl_users.id=:id";
                global $pdo;
                $atualizarankpessoa=$pdo->prepare($sql);
                $atualizarankpessoa->bindValue(":id", $profissional);
                $atualizarankpessoa->execute(); global $LQ; $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro rank profissional '. $error_msg->getMessage();
            }

	}

}

?>