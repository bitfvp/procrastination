<?php
class Pessoa{
    public function fncpessoaedit(
        $id,
        $nome,
        $nome_social,
        $sexo,
        $nascimento,
        $cpf,
        $rg,
        $uf_rg,
        $nis,
        $ctps,
        $cod_familiar,
        $responsavel_familiar,
        $parentesco,
        $endereco,
        $numero,
        $bairro,
        $referencia,
        $telefone,
        $raca_cor,
        $agente_saude,
        $mae,
        $pai,
        $possui_veiculo,
        $paga_aluguel,
        $possui_propriedade,
        $alfabetizado,
        $renda,
        $bpc,
        $deficiencia,
        $deficiencia_desc,
        $curso,
        $horario
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $agente_saude=remover_caracter(ucwords(strtolower($agente_saude)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        try{
            $sql="SELECT * FROM ";
                $sql.="mcu_pessoas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE mcu_pessoas ";
                $sql.="SET ";
                $sql .= "nome=:nome, 
                nome_social=:nome_social, 
                sexo=:sexo, 
                nascimento=:nascimento,
                cpf=:cpf, 
                rg=:rg, 
                uf_rg=:uf_rg, 
                nis=:nis, 
                ctps=:ctps,
                cod_familiar=:cod_familiar,
                responsavel_familiar=:responsavel_familiar, 
                parentesco=:parentesco,  
                endereco=:endereco, 
                numero=:numero, 
                bairro=:bairro, 
                referencia=:referencia, 
                telefone=:telefone, 
                raca_cor=:raca_cor, 
                agente_saude=:agente_saude,
                mae=:mae, 
                pai=:pai, 
                possui_veiculo=:possui_veiculo, 
                paga_aluguel=:paga_aluguel, 
                possui_propriedade=:possui_propriedade, 
                alfabetizado=:alfabetizado, 
                renda=:renda, 
                bpc=:bpc, 
                deficiencia=:deficiencia,
                deficiencia_desc=:deficiencia_desc,
                curso=:curso,
                horario=:horario
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":nome_social", $nome_social);
                $atualiza->bindValue(":sexo", $sexo);
                $atualiza->bindValue(":nascimento", $nascimento);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":uf_rg", $uf_rg);
                $atualiza->bindValue(":nis", $nis);
                $atualiza->bindValue(":ctps", $ctps);
                $atualiza->bindValue(":cod_familiar", $cod_familiar);
                $atualiza->bindValue(":responsavel_familiar", $responsavel_familiar);
                $atualiza->bindValue(":parentesco", $parentesco);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":referencia", $referencia);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":raca_cor", $raca_cor);
                $atualiza->bindValue(":agente_saude", $agente_saude);
                $atualiza->bindValue(":mae", $mae);
                $atualiza->bindValue(":pai", $pai);
                $atualiza->bindValue(":possui_veiculo", $possui_veiculo);
                $atualiza->bindValue(":paga_aluguel", $paga_aluguel);
                $atualiza->bindValue(":possui_propriedade", $possui_propriedade);
                $atualiza->bindValue(":alfabetizado", $alfabetizado);
                $atualiza->bindValue(":renda", $renda);
                $atualiza->bindValue(":bpc", $bpc);
                $atualiza->bindValue(":deficiencia", $deficiencia);
                $atualiza->bindValue(":deficiencia_desc", $deficiencia_desc);
                $atualiza->bindValue(":curso", $curso);
                $atualiza->bindValue(":horario", $horario);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vpessoa&id={$id}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew(
                                  $nome,
                                  $nome_social,
                                  $sexo,
                                  $nascimento,
                                  $cpf,
                                  $rg,
                                  $uf_rg,
                                  $nis,
                                  $ctps,
                                  $cod_familiar,
                                  $responsavel_familiar,
                                  $parentesco,
                                  $endereco,
                                  $numero,
                                  $bairro,
                                  $referencia,
                                  $telefone,
                                  $raca_cor,
                                  $agente_saude,
                                  $mae,
                                  $pai,
                                  $possui_veiculo,
                                  $paga_aluguel,
                                  $possui_propriedade,
                                  $alfabetizado,
                                  $renda,
                                  $bpc,
                                  $deficiencia,
                                  $deficiencia_desc,
                                  $curso,
                                  $horario
    ){
        //tratamento das variaveis
        $nome=remover_caracter(ucwords(strtolower($nome)));
        $nome_social=remover_caracter(ucwords(strtolower($nome_social)));
        $agente_saude=remover_caracter(ucwords(strtolower($agente_saude)));
        $uf_rg=remover_caracter(ucwords(strtoupper($uf_rg)));
        $mae=remover_caracter(ucwords(strtolower($mae)));
        $pai=remover_caracter(ucwords(strtolower($pai)));

        if($sexo==""){
            $sexo="0";
        }
        if($responsavel_familiar==""){
            $responsavel_familiar="0";
        }
        if($nascimento==""){
            $nascimento="1000-01-01";
        }
        if($bairro==""){
            $bairro="0";
        }
        try{
            $sql="SELECT count('id') FROM ";
                $sql.="mcu_pessoas";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->fetch();
        $contarcpf = $contarcpf[0];
        try{
            $sql="SELECT count('id') FROM ";
                $sql.="mcu_pessoas";
            $sql.=" WHERE rg=:rg";
            global $pdo;
            $consultarg=$pdo->prepare($sql);
            $consultarg->bindValue(":rg", $rg);
            $consultarg->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarrg=$consultarg->fetch();
        $contarrg = $contarrg[0];
        try{
            $sql="SELECT count('id') FROM ";
                $sql.="mcu_pessoas";
            $sql.=" WHERE nis=:nis";
            global $pdo;
            $consultanis=$pdo->prepare($sql);
            $consultanis->bindValue(":nis", $nis);
            $consultanis->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarnis=$consultanis->fetch();
        $contarnis = $contarnis[0];

        if(($contarnis==0)or ($nis=="")){
        if(($contarrg==0)or ($rg=="")){
        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO mcu_pessoas ";
                    $sql .= "(id,
                    data_cadastro,
                    resp_cadastro,
                    nome,
                    nome_social,
                    sexo,
                    nascimento,
                    cpf,
                    rg,
                    uf_rg,
                    nis,
                    ctps,
                    cod_familiar,
                    responsavel_familiar,
                    parentesco,
                    endereco,
                    numero,
                    bairro,
                    referencia,
                    telefone,
                    raca_cor,
                    agente_saude,
                    mae,
                    pai,
                    possui_veiculo,
                    paga_aluguel,
                    possui_propriedade,
                    alfabetizado,
                    renda,
                    bpc,
                    deficiencia,
                    deficiencia_desc,
                    curso,
                    horario
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    CURRENT_TIMESTAMP,
                    :resp_cadastro,
                    :nome,
                    :nome_social,
                    :sexo,
                    :nascimento,
                    :cpf,
                    :rg,
                    :uf_rg,
                    :nis,
                    :ctps,
                    :cod_familiar,
                    :responsavel_familiar,
                    :parentesco,
                    :endereco,
                    :numero,
                    :bairro,
                    :referencia,
                    :telefone,
                    :raca_cor,
                    :agente_saude,
                    :mae,
                    :pai,
                    :possui_veiculo,
                    :paga_aluguel,
                    :possui_propriedade,
                    :alfabetizado,
                    :renda,
                    :bpc,
                    :deficiencia,
                    :deficiencia_desc,
                    :curso,
                    :horario
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":resp_cadastro", $_SESSION['id']);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":nome_social", $nome_social);
                    $insere->bindValue(":sexo", $sexo);
                    $insere->bindValue(":nascimento", $nascimento);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":uf_rg", $uf_rg);
                    $insere->bindValue(":nis", $nis);
                    $insere->bindValue(":ctps", $ctps);
                    $insere->bindValue(":cod_familiar", $cod_familiar);
                    $insere->bindValue(":responsavel_familiar", $responsavel_familiar);
                    $insere->bindValue(":parentesco", $parentesco);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":referencia", $referencia);
                    $insere->bindValue(":telefone", $telefone);
                    $insere->bindValue(":raca_cor", $raca_cor);
                    $insere->bindValue(":agente_saude", $agente_saude);
                    $insere->bindValue(":mae", $mae);
                    $insere->bindValue(":pai", $pai);
                    $insere->bindValue(":possui_veiculo", $possui_veiculo);
                    $insere->bindValue(":paga_aluguel", $paga_aluguel);
                    $insere->bindValue(":possui_propriedade", $possui_propriedade);
                    $insere->bindValue(":alfabetizado", $alfabetizado);
                    $insere->bindValue(":renda", $renda);
                    $insere->bindValue(":bpc", $bpc);
                    $insere->bindValue(":deficiencia", $deficiencia);
                    $insere->bindValue(":deficiencia_desc", $deficiencia_desc);
                    $insere->bindValue(":curso", $curso);
                    $insere->bindValue(":horario", $horario);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse RG!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse nis!!",
                "type"=>"warning",
            ];
        }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="mcu_pessoas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vpessoa&id={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncnewduplicata($pessoa){
        //tratamento das variaveis



                    //inserção no banco
                    try {
                        $sql="INSERT INTO mcu_pessoas_duplicadas ";
                        $sql .= "(id,pessoa,profissional
                    )";
                        $sql .= " VALUES ";
                        $sql .= "(NULL, :pessoa, :profissional)";
                        global $pdo;
                        $insere = $pdo->prepare($sql);
                        $insere->bindValue(":pessoa", $pessoa);
                        $insere->bindValue(":profissional", $_SESSION['id']);
                        $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                    } catch (PDOException $error_msg) {
                        echo 'Erro' . $error_msg->getMessage();
                    }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Obrigado!!",
                "type"=>"success",
            ];


            header("Location: index.php?pg=Vpessoa&id={$_GET['id']}");
            exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }

    }



}
