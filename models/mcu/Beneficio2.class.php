<?php
class Beneficio2{
    public function fncbenew2($cb_pessoa,$cb_beneficio,$cb_condicao,$cb_quantidade,$cb_data_pedido,$cb_profissional,$cb_descricao,$cb_entregue,$cb_data_entrega,$cb_quem_recebeu){
        //tratamento das variaveis

            //inserção no banco
            try{
                $sql="INSERT INTO mcu_beneficio ";
                $sql.="(id, pessoa, beneficio, condicao, quantidade, data_pedido, profissional, descricao, entregue, data_entrega, quem_recebeu)";
                $sql.=" VALUES ";
                $sql.="(NULL, :pessoa, :beneficio, :condicao, :quantidade, :data_pedido, :profissional, :descricao, :entregue, :data_entrega, :quem_recebeu)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":pessoa", $cb_pessoa);
                $insere->bindValue(":beneficio", $cb_beneficio);
                $insere->bindValue(":condicao", $cb_condicao);
                $insere->bindValue(":quantidade", $cb_quantidade);
                $insere->bindValue(":data_pedido", $cb_data_pedido);
                $insere->bindValue(":profissional", $cb_profissional);
                $insere->bindValue(":descricao", $cb_descricao);
                $insere->bindValue(":entregue", $cb_entregue);
                $insere->bindValue(":data_entrega", $cb_data_entrega);
                $insere->bindValue(":quem_recebeu", $cb_quem_recebeu);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }


        if(isset($insere)){
            /////////////////////////////////////////////////////
            //reservado para log
            global $LL; $LL->fnclog($cb_pessoa,$_SESSION['id'],"Novo beneficio",3,1);
            ////////////////////////////////////////////////////////////////////////////

            $_SESSION['fsh']=[
                "flash"=>"Benefício cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
                ];
                header("Location: ?pg=Vbe2&id={$cb_pessoa}");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim de fnc create


}//fim de classe