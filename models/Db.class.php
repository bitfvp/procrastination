<?php
//inclui a classe a ser herdada
//Env possui as configuracoes de ambientes mais abrangentes
//e tudo depende delas
include_once("{$env->env_root}/models/Env.class.php");


class Db extends Env {
    // Variável que guarda a conexão PDO.
    private $dbHost;
    private $dbName;
    private $dbUser;
    private $dbPass;
    private $dbDriver;
    private $sis_titulo;
    private $sis_email;
    protected static $db;
    // Private construct - garante que a classe só possa ser instanciada internamente.

    protected function __construct(){
        # Informações sobre o banco de dados:
        parent::__construct();

            $this->dbHost =$_ENV['ENV_BD_IP'];
            $this->dbName=$_ENV['ENV_BD_BANCO'];
            $this->dbUser =$_ENV['ENV_BD_USUARIO'];
            $this->dbPass =$_ENV['ENV_BD_SENHA'];


        $this->dbDriver= "mysql";
        # Informações sobre o sistema:
        $this->sis_titulo = $_ENV['ENV_NOME'];
        $this->sis_email = "flavioworks@live.com";
        try {
            # Atribui o objeto PDO à variável $db.
            self::$db = new PDO("$this->dbDriver:host=$this->dbHost; dbname=$this->dbName", $this->dbUser, $this->dbPass);
            # Garante que o PDO lance exceções durante erros.
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            # Garante que os dados sejam armazenados com codificação UFT-8.
//            self::$db->exec('SET NAMES utf8');
            self::$db->exec('SET NAMES latin1');
            //no futuro fazer uma conversao para utf8, e nas tabelas converter os dados com update com codigo abaixo
            //SET descricao = convert(cast(convert(descricao using  latin1) as binary) using utf8)

        }
        catch (PDOException $e) {
            # Envia um e-mail para o e-mail oficial do sistema, em caso de erro de conexão.
           // mail($sistema_email, "PDOException em $sistema_titulo", $e->getMessage());
            # Então não carrega nada mais da página.
            die("Connection Error: " . $e->getMessage());
        }
    }
    # Método estático - acessível sem instanciação.
    public static function conn(){
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$db)
        {
            new Db();
        }
        # Retorna a conexão.
        return self::$db;
    }
}
