<?php
//classe primaria
class Env{
    //determina se esta local ou em producao web
    //$_ENV['ENV'];

    //diretorio raiz (em alguns servers "web" "htdocs" "html")
    public $env_htmlpasta;

    //determina camminho no ponto abaixo da raiz
    public $env_root;

    //determina url na raiz
    public $env_url;

    //determina caminho url onde fica as images css e js imbarcados
    public $env_estatico;

    //nome da aplicacao
    public $env_nome;

    protected function __construct() {
        $this->env_htmlpasta="html/";
        $this->env_root=$_ENV['ENV_ROOT'];
        $this->env_url=$_ENV['ENV_URL'];
        $this->env_estatico=$this->env_url.$_ENV['ENV_ESTATICO'];

        //rodar sem dns
        if ($_SERVER['HTTP_HOST']=="174.138.119.106"){
            $this->env_url=$_ENV['ENV_URL_IP'];
            $this->env_estatico=$this->env_url.$_ENV['ENV_ESTATICO'];
        }

        $this->env_nome=$_ENV['ENV_NOME'];
    }

}
