<?php
class Login{
    public function fnclogar($email, $senha, $dispositivo, $sistema_operacional, $navegador, $senhatest ){

        global $W_O_R_D_S;
        $W_O_R_D_S->fncwords_posicoes();

        //verifica se o banco tem o usuario e senha
	    try{
            $sql="select * from tbl_users WHERE (email=? or nick=?) AND senha=? limit 1";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $email);
            $consulta->bindParam(2, $email);
            $consulta->bindParam(3, $senha);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
	    }catch ( PDOException $error_msg){
	        echo 'Erroff'. $error_msg->getMessage();
        }
        //verifica se existe >0 registros
        if( $consulta->rowCount()!=0){
			$dados=$consulta->fetch();
			//verifica se esta ativo
			if($dados['status']==1){
                $_SESSION['id']=$dados['id'];
				$_SESSION['nome']=$dados['nome'];
                $_SESSION['matriz']=$dados['matriz'];
                $_SESSION['theme']=$dados['theme'];
                //setcookie('logado',1);//, time()+3600*24
                $_SESSION['logado']="1";
				$log=1;

				//seta o token
                $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                $tm2=$tml;
                $newtm = $tm2+"3600";
                $tokenId = sha1($tm2);
                $_SESSION['tokenId'] = $tokenId;
                $_SESSION['tokenTime'] = $newtm;


                $sql = "insert into tbl_token ";
                $sql .= "(id, token, token_time, user, dispositivo, sistema_operacional, navegador) values(NULL, :token, :time, :user, :dispositivo, :sistema_operacional, :navegador) ";
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":token", $tokenId);
                $insere->bindValue(":time", $newtm);
                $insere->bindValue(":dispositivo", $dispositivo);
                $insere->bindValue(":sistema_operacional", $sistema_operacional);
                $insere->bindValue(":navegador", $navegador);
                $insere->bindValue(":user", $dados['id']);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                $sql = null;
                $consulta = null;

			}else{
                $_SESSION['fsh']=[
			        "flash"=>"Aguarde nossa Aprovação",
                    "type"=>"info",
                    ];
			}
		}else{
            //catalogar erros ao entrar
            $descricao=$email." --*-- ".$senhatest;
            $sql = "insert into tbl_tent_acessos ";
            $sql .= "(id, data, dispositivo, sistema_operacional, navegador, descricao) values "
                ."(NULL, CURRENT_TIMESTAMP, :dispositivo, :sistema_operacional, :navegador, :descricao) ";
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":dispositivo", $dispositivo);
            $insere->bindValue(":sistema_operacional", $sistema_operacional);
            $insere->bindValue(":navegador", $navegador);
            $insere->bindValue(":descricao", $descricao);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            $sql = null;
            $consulta = null;
        }
		if(isset($log)){
		    $sorte=rand(1,8);
            switch ($sorte){
                case 1:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo ao sistema que você nunca soube que queria",
                        "type"=>"success",
                    ];
                    break;
                case 2:
                    $_SESSION['fsh']=[
                        "flash"=>"olá, preparamos tudo para você ter uma ótima experiência",
                        "type"=>"success",
                    ];
                    break;
                case 3:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo.",
                        "type"=>"success",
                    ];
                    break;
                case 4:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, se estava bom ontem, hoje está melhor",
                        "type"=>"success",
                    ];
                    break;
                case 5:
                    $_SESSION['fsh']=[
                        "flash"=>"oi, fique a vontade, qualquer dificuldade procure o coordenador ou o suporte",
                        "type"=>"success",
                    ];
                    break;
                case 6:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, que bom que está com a gente",
                        "type"=>"success",
                    ];
                    break;
                case 7:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, toda a informação aqui",
                        "type"=>"success",
                    ];
                    break;
                case 8:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo, estamos prontos pra começar",
                        "type"=>"success",
                    ];
                    break;
                default:
                    $_SESSION['fsh']=[
                        "flash"=>"Bem-vindo",
                        "type"=>"success",
                    ];
                    break;
            }


		}else{
			if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops, erro ao entrar, digite e-mail e senha corretamente!",
                    "type"=>"danger",
                ];
			}
		}

		
	}

}
