<?php
class Words{


    public function fncwords_posicoes(){
        try{
            $sql="SELECT * FROM words ORDER BY cont DESC LIMIT 0,1999";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $posicoes=$consulta->fetchAll();

        $contadora=0;
        foreach ($posicoes as $pos){
            if ($pos['posicao']!=$contadora){
                try{
                    $sql = "UPDATE words SET ";
                    $sql .= "pos_anterior = posicao, posicao=:contadora";
                    $sql .= " WHERE id=:id";
                    global $pdo;
                    $consulta=$pdo->prepare($sql);
                    $consulta->bindValue(":contadora", $contadora);
                    $consulta->bindValue(":id", $pos['id']);
                    $consulta->execute();
                }catch ( PDOException $error_msg){
                    echo 'Erroff'. $error_msg->getMessage();
                }
            }
            $contadora++;
        }

    }

    public function fncwords_new($cons){

        //verifica se tem numero
        if (is_numeric(filter_var($cons, FILTER_SANITIZE_NUMBER_INT))){
//            echo "contem numeros";
        }else{
            try{
                $sql="INSERT INTO words( word )VALUES( :cons )";
                global $pdo;
                $consulta=$pdo->prepare($sql);
                $consulta->bindValue(":cons", $cons);
                $consulta->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }
        }
    }


    public function fncwords_cont($cons){

        $array = array('que','com', 'para', 'pra', 'dos', 'das', 'nao', 'nos', 'ser', 'sua', 'seu', 'fui', 'nas', 'foi', 'fez', 'faz', 'faz', 'ela', 'ele', 'tem', 'uma', 'dia', 'por', 'ter', 'elas', 'eles', 'seus', 'seus', 'suas', 'esta', 'por', 'pelo', 'pois');
        $ver = array_search($cons, $array,true);
        if (is_numeric($ver)){
//            echo is_null($ver);
//            echo "esta na lista de descarte";
        }else{
//            echo "nao esta na lista de descarte";
            try{
                $sql = "UPDATE words SET ";
                $sql .= "cont=cont+1";
                $sql .= " WHERE word=:word";
                global $pdo;
                $consulta=$pdo->prepare($sql);
                $consulta->bindValue(":word", $cons);
                $consulta->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }
        }



    }

    public function fncwords_verifica($cons){

        try{
            $sql="SELECT word as quantidade from words where word=:cons";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":cons", $cons);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        return $consulta->rowCount();
    }


    public function fncwords($cons){

        //words encontrasse desativado
//        $arr = explode(' ', $cons);
//
//        foreach ($arr as $arra){
//            $arra=strtolower(remover_caracter(limpadocumento($arra)));
//            if (strlen($arra)>2){
//                $verifica=$this->fncwords_verifica($arra);
//                if ($verifica==0){
//                    $this->fncwords_new($arra);
//                }else{
//                    $this->fncwords_cont($arra);
//                }
//            }
//        }
        //words encontrasse desativado

//        $tttttttttttttttttt= $this->fncwords_posicoes();

	}



}

?>