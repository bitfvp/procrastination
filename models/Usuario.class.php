<?php 
class Usuario{
	public function fncnewusuario(
        $nome,
        $status,
        $nick,
        $email,
        $sexo,
        $nascimento,
        $mae,
        $pai,
        $cpf,
        $rg,
        $rg_expedicao,
        $rg_emissor,
        $pis,
        $titulo,
        $zona,
        $secao,
        $endereco,
        $bairro,
        $cep,
        $telefone1,
        $telefone2,
        $escolaridade,
        $estado_civil,
        $profissao,
        $setor,
        $admissao,
        $fim_exercicio,
        $completo,
        $matriz
    ){
		//tratamento das variaveis
		$senha=sha1("12345678"."1010011010");

        if($matriz==""){
            $matriz="0";
        }
        if($nascimento==""){
            $nascimento="1900-01-01";
        }
		//valida se ja ha um usuario cadastrado
        try{
            $sql="SELECT * FROM tbl_users WHERE email=:email";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":email", $email);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

		$contar=$consulta->rowCount();
		if($contar==0){
			//inserção no banco
            try{
                $sql="INSERT INTO tbl_users("
                    ."nome, status, nick, email, senha, sexo, nascimento, mae, pai, cpf, rg, rg_expedicao, rg_emissor, pis, titulo, zona, secao, endereco, bairro, cep, telefone1, telefone2, escolaridade, estado_civil, profissao, setor, admissao, fim_exercicio, completo, matriz"
                    .")VALUES("
                    .":nome, :status, :nick, :email, :senha, :sexo, :nascimento, :mae, :pai, :cpf, :rg, :rg_expedicao, :rg_emissor, :pis, :titulo, :zona, :secao, :endereco, :bairro, :cep, :telefone1, :telefone2, :escolaridade, :estado_civil, :profissao, :setor, :admissao, :fim_exercicio, :completo, :matriz"
                    .")";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":nick", $nick);
                $insere->bindValue(":email", $email);
                $insere->bindValue(":senha", $senha);
                $insere->bindValue(":sexo", $sexo);
                $insere->bindValue(":nascimento", $nascimento);
                $insere->bindValue(":mae", $mae);
                $insere->bindValue(":pai", $pai);
                $insere->bindValue(":cpf", $cpf);
                $insere->bindValue(":rg", $rg);
                $insere->bindValue(":rg_expedicao", $rg_expedicao);
                $insere->bindValue(":rg_emissor", $rg_emissor);
                $insere->bindValue(":pis", $pis);
                $insere->bindValue(":titulo", $titulo);
                $insere->bindValue(":zona", $zona);
                $insere->bindValue(":secao", $secao);
                $insere->bindValue(":endereco", $endereco);
                $insere->bindValue(":bairro", $bairro);
                $insere->bindValue(":cep", $cep);
                $insere->bindValue(":telefone1", $telefone1);
                $insere->bindValue(":telefone2", $telefone2);
                $insere->bindValue(":escolaridade", $escolaridade);
                $insere->bindValue(":estado_civil", $estado_civil);
                $insere->bindValue(":profissao", $profissao);
                $insere->bindValue(":setor", $setor);
                $insere->bindValue(":admissao", $admissao);
                $insere->bindValue(":fim_exercicio", $fim_exercicio);
                $insere->bindValue(":completo", $completo);
                $insere->bindValue(":matriz", $matriz);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

		}else{
			//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já há um usuario cadastrado com esse email em nosso sistema!!!!",
                "type"=>"danger",
            ];

		}

			if(isset($insere)){

		    //seleciona o ultimo usuario cadastrado
                $sql = "SELECT Max(id) FROM tbl_users";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;
                $maid=$mid[0];

                //busca matriz
                $sql = "SELECT matriz FROM `tbl_users` WHERE id={$maid}";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mma = $consulta->fetch();
                $sql=null;
                $consulta=null;
                $mmatriz=$mma['matriz'];

                //cria allow
                if ($mmatriz==1){$sql="INSERT INTO `mcu_allow` (`id`, `pessoa`) VALUES (NULL, :pessoa);";}
                if ($mmatriz==2){$sql="INSERT INTO `caf_allow` (`id`, `pessoa`) VALUES (NULL, :pessoa);";}
                if ($mmatriz==3){$sql="INSERT INTO `mpref_allow` (`id`, `pessoa`) VALUES (NULL, :pessoa);";}
                if ($mmatriz==4){$sql="INSERT INTO `smem_allow` (`id`, `pessoa`) VALUES (NULL, :pessoa);";}
                    global $pdo;
                    $insere=$pdo->prepare($sql);
                    $insere->bindValue(":pessoa", $maid);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);

                $_SESSION['fsh']=[
                    "flash"=>"Cadastro realizado com sucesso!!",
                    "type"=>"success",
                ];
                
			}else{
				if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                    $_SESSION['fsh']=[
                        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                        "type"=>"danger",
                    ];
				}
			}
	}
//==================================================================================================

//=========================================================================================
    public function fncusuarioedit(
        $pessoa,
        $nome,
        $status,
        $nick,
        $email,
        $sexo,
        $nascimento,
        $mae,
        $pai,
        $cpf,
        $rg,
        $rg_expedicao,
        $rg_emissor,
        $pis,
        $titulo,
        $zona,
        $secao,
        $endereco,
        $bairro,
        $cep,
        $telefone1,
        $telefone2,
        $escolaridade,
        $estado_civil,
        $profissao,
        $setor,
        $admissao,
        $fim_exercicio,
        $completo,
        $matriz
        )
    {
        try{
            $sql = "UPDATE tbl_users SET "
                ."nome = :nome, "
                ."status = :status, "
                ."nick = :nick, "
                ."email = :email, "
                ."sexo = :sexo, "
                ."nascimento = :nascimento, "
                ."mae = :mae, "
                ."pai = :pai, "
                ."cpf = :cpf, "
                ."rg = :rg, "
                ."rg_expedicao = :rg_expedicao, "
                ."rg_emissor = :rg_emissor, "
                ."pis = :pis, "
                ."titulo = :titulo, "
                ."zona = :zona, "
                ."secao = :secao, "
                ."endereco = :endereco, "
                ."bairro = :bairro, "
                ."cep = :cep, "
                ."telefone1 = :telefone1, "
                ."telefone2 = :telefone2, "
                ."escolaridade = :escolaridade, "
                ."estado_civil = :estado_civil, "
                ."profissao = :profissao, "
                ."setor = :setor, "
                ."admissao = :admissao, "
                ."fim_exercicio = :fim_exercicio, "
                ."completo = :completo, "
                ."matriz = :matriz "
                ."WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":nome", $nome);
            $insere2->bindValue(":status", $status);
            $insere2->bindValue(":nick", $nick);
            $insere2->bindValue(":email", $email);
            $insere2->bindValue(":sexo", $sexo);
            $insere2->bindValue(":nascimento", $nascimento);
            $insere2->bindValue(":mae", $mae);
            $insere2->bindValue(":pai", $pai);
            $insere2->bindValue(":cpf", $cpf);
            $insere2->bindValue(":rg", $rg);
            $insere2->bindValue(":rg_expedicao", $rg_expedicao);
            $insere2->bindValue(":rg_emissor", $rg_emissor);
            $insere2->bindValue(":pis", $pis);
            $insere2->bindValue(":titulo", $titulo);
            $insere2->bindValue(":zona", $zona);
            $insere2->bindValue(":secao", $secao);
            $insere2->bindValue(":endereco", $endereco);
            $insere2->bindValue(":bairro", $bairro);
            $insere2->bindValue(":cep", $cep);
            $insere2->bindValue(":telefone1", $telefone1);
            $insere2->bindValue(":telefone2", $telefone2);
            $insere2->bindValue(":escolaridade", $escolaridade);
            $insere2->bindValue(":estado_civil", $estado_civil);
            $insere2->bindValue(":profissao", $profissao);
            $insere2->bindValue(":setor", $setor);
            $insere2->bindValue(":admissao", $admissao);
            $insere2->bindValue(":fim_exercicio", $fim_exercicio);
            $insere2->bindValue(":completo", $completo);
            $insere2->bindValue(":matriz", $matriz);
            $insere2->bindValue(":id", $pessoa);
            $insere2->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
        $descricao="alteração no registro";
        try{
            $sql="INSERT INTO tbl_users_log ";
            $sql.="(id, user, profissional,  data, descricao )";
            $sql.=" VALUES ";
            $sql.="(NULL, :user, :profissional, CURRENT_TIMESTAMP, :descricao )";
            global $pdo;
            $inserelog=$pdo->prepare($sql);
            $inserelog->bindValue(":user", $pessoa);
            $inserelog->bindValue(":profissional", $_SESSION['id']);
            $inserelog->bindValue(":descricao", $descricao);
            $inserelog->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro log '. $error_msg->getMessage();
        }

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
/// //==================================================================================================
///
///
//=========================================================================================
    public function fncusuarioedit2(
        $pessoa,
        $nome,
        $sexo,
        $nascimento,
        $mae,
        $pai,
        $cpf,
        $rg,
        $rg_expedicao,
        $rg_emissor,
        $pis,
        $titulo,
        $zona,
        $secao,
        $endereco,
        $bairro,
        $cep,
        $telefone1,
        $telefone2,
        $escolaridade,
        $estado_civil,
        $profissao,
        $setor,
        $admissao,
        $fim_exercicio,
        $completo,
        $matriz
    )
    {
        try{
            $sql = "UPDATE tbl_users SET "
                ."nome = :nome, "
                ."sexo = :sexo, "
                ."nascimento = :nascimento, "
                ."mae = :mae, "
                ."pai = :pai, "
                ."cpf = :cpf, "
                ."rg = :rg, "
                ."rg_expedicao = :rg_expedicao, "
                ."rg_emissor = :rg_emissor, "
                ."pis = :pis, "
                ."titulo = :titulo, "
                ."zona = :zona, "
                ."secao = :secao, "
                ."endereco = :endereco, "
                ."bairro = :bairro, "
                ."cep = :cep, "
                ."telefone1 = :telefone1, "
                ."telefone2 = :telefone2, "
                ."escolaridade = :escolaridade, "
                ."estado_civil = :estado_civil, "
                ."profissao = :profissao, "
                ."setor = :setor, "
                ."admissao = :admissao, "
                ."fim_exercicio = :fim_exercicio, "
                ."completo = :completo, "
                ."matriz = :matriz "
                ."WHERE id = :id";
            global $pdo;
            $insere2=$pdo->prepare($sql);
            $insere2->bindValue(":nome", $nome);
            $insere2->bindValue(":sexo", $sexo);
            $insere2->bindValue(":nascimento", $nascimento);
            $insere2->bindValue(":mae", $mae);
            $insere2->bindValue(":pai", $pai);
            $insere2->bindValue(":cpf", $cpf);
            $insere2->bindValue(":rg", $rg);
            $insere2->bindValue(":rg_expedicao", $rg_expedicao);
            $insere2->bindValue(":rg_emissor", $rg_emissor);
            $insere2->bindValue(":pis", $pis);
            $insere2->bindValue(":titulo", $titulo);
            $insere2->bindValue(":zona", $zona);
            $insere2->bindValue(":secao", $secao);
            $insere2->bindValue(":endereco", $endereco);
            $insere2->bindValue(":bairro", $bairro);
            $insere2->bindValue(":cep", $cep);
            $insere2->bindValue(":telefone1", $telefone1);
            $insere2->bindValue(":telefone2", $telefone2);
            $insere2->bindValue(":escolaridade", $escolaridade);
            $insere2->bindValue(":estado_civil", $estado_civil);
            $insere2->bindValue(":profissao", $profissao);
            $insere2->bindValue(":setor", $setor);
            $insere2->bindValue(":admissao", $admissao);
            $insere2->bindValue(":fim_exercicio", $fim_exercicio);
            $insere2->bindValue(":completo", $completo);
            $insere2->bindValue(":matriz", $matriz);
            $insere2->bindValue(":id", $pessoa);
            $insere2->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        $descricao="alteração no registro";
        try{
            $sql="INSERT INTO tbl_users_log ";
            $sql.="(id, user, profissional,  data, descricao )";
            $sql.=" VALUES ";
            $sql.="(NULL, :user, :profissional, CURRENT_TIMESTAMP, :descricao )";
            global $pdo;
            $inserelog=$pdo->prepare($sql);
            $inserelog->bindValue(":user", $pessoa);
            $inserelog->bindValue(":profissional", $_SESSION['id']);
            $inserelog->bindValue(":descricao", $descricao);
            $inserelog->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro log '. $error_msg->getMessage();
        }

        if(isset($insere2)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }
/// //==================================================================================================

//=========================================================================================
    public function fncnewsenha($id,$senha){
        //tratamento das variaveis
        $senha=sha1($senha."1010011010");

        //atualização no banco
        try{
            $sql="UPDATE tbl_users SET senha=:senha ";
            $sql.="WHERE id=:id";
            global $pdo;
            $at=$pdo->prepare($sql);
            $at->bindValue(":id", $id);
            $at->bindValue(":senha", $senha);
            $at->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if(isset($at)){
            $_SESSION['fsh']=[
                "flash"=>"Atualização de senha realizado com sucesso!!",
                "type"=>"success",
                "error"=>"No proximo login use a nova senha cadastrada",
            ];
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }
//==================================================================================================

//=========================================================================================
    public function fncresetsenha($id){
        //inserção no banco
        try{
            $sql = "UPDATE tbl_users SET senha = :novasenha WHERE id = :id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":novasenha", "c8b27e713ebfe1e38f991c4462360a9b11242db9");
            $insere->bindValue(":id", $id);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Senha resetada com sucesso!! ",
                "type"=>"success",
                "error"=>"<h2>Nova senha: 12345678</h2>"
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador ",
                    "type"=>"danger",
                ];

            }
        }
    }
    //==================================================================================================

//=========================================================================================
}
?>